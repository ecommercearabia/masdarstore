/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomerinterestscustomaddon.jalo;

import com.masdar.masdarcustomerinterestscustomaddon.constants.MasdarcustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarcustomerinterestscustomaddonManager extends GeneratedMasdarcustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarcustomerinterestscustomaddonManager.class.getName() );
	
	public static final MasdarcustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarcustomerinterestscustomaddonManager) em.getExtension(MasdarcustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
