/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponsamplescustomaddon.jalo;

import com.masdar.masdarcustomercouponsamplescustomaddon.constants.MasdarcustomercouponsamplescustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarcustomercouponsamplescustomaddonManager extends GeneratedMasdarcustomercouponsamplescustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarcustomercouponsamplescustomaddonManager.class.getName() );
	
	public static final MasdarcustomercouponsamplescustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarcustomercouponsamplescustomaddonManager) em.getExtension(MasdarcustomercouponsamplescustomaddonConstants.EXTENSIONNAME);
	}
	
}
