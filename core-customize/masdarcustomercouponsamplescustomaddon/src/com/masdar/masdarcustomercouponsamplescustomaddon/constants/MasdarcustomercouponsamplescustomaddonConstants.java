/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponsamplescustomaddon.constants;

/**
 * Masdarcustomercouponsamplescustomaddon constants
 */
public final class MasdarcustomercouponsamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarcustomercouponsamplescustomaddon";

	private MasdarcustomercouponsamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
