/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarconsignmenttrackingcustomocc.jalo;

import com.masdar.masdarconsignmenttrackingcustomocc.constants.MasdarconsignmenttrackingcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarconsignmenttrackingcustomoccManager extends GeneratedMasdarconsignmenttrackingcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarconsignmenttrackingcustomoccManager.class.getName() );
	
	public static final MasdarconsignmenttrackingcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarconsignmenttrackingcustomoccManager) em.getExtension(MasdarconsignmenttrackingcustomoccConstants.EXTENSIONNAME);
	}
	
}
