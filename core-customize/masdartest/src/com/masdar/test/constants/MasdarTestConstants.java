/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.test.constants;

/**
 * 
 */
public class MasdarTestConstants extends GeneratedMasdarTestConstants
{

	public static final String EXTENSIONNAME = "masdartest";

}
