/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomerinterestscustomocc.jalo;

import com.masdar.masdarcustomerinterestscustomocc.constants.MasdarcustomerinterestscustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarcustomerinterestscustomoccManager extends GeneratedMasdarcustomerinterestscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarcustomerinterestscustomoccManager.class.getName() );
	
	public static final MasdarcustomerinterestscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarcustomerinterestscustomoccManager) em.getExtension(MasdarcustomerinterestscustomoccConstants.EXTENSIONNAME);
	}
	
}
