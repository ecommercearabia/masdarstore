/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomerinterestscustomocc.constants;

/**
 * Global class for all masdarcustomerinterestscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class MasdarcustomerinterestscustomoccConstants extends GeneratedMasdarcustomerinterestscustomoccConstants
{
	public static final String EXTENSIONNAME = "masdarcustomerinterestscustomocc"; //NOSONAR

	private MasdarcustomerinterestscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
