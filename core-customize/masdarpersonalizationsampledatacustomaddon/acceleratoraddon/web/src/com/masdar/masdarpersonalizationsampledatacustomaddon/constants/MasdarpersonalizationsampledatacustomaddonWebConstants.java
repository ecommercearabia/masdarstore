/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationsampledatacustomaddon.constants;

/**
 * Global class for all Masdarpersonalizationsampledatacustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarpersonalizationsampledatacustomaddonWebConstants // NOSONAR
{
	private MasdarpersonalizationsampledatacustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
