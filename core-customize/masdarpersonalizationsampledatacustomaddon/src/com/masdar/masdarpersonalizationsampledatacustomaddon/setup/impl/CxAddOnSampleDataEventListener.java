/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  de.hybris.platform.addonsupport.setup.impl.GenericAddOnSampleDataEventListener
 *  de.hybris.platform.servicelayer.event.events.AbstractEvent
 *  de.hybris.platform.util.Config
 */
package com.masdar.masdarpersonalizationsampledatacustomaddon.setup.impl;

import de.hybris.platform.addonsupport.setup.impl.GenericAddOnSampleDataEventListener;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.util.Config;


public class CxAddOnSampleDataEventListener extends GenericAddOnSampleDataEventListener
{
	@Override
	protected void onEvent(final AbstractEvent event)
	{
		if (Config.getBoolean("masdarpersonalizationsampledatacustomaddon.import.active", true))
		{
			super.onEvent(event);
		}
	}
}

