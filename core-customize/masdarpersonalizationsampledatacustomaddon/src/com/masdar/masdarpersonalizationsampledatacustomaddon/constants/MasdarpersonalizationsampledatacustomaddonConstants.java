/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationsampledatacustomaddon.constants;

/**
 * Global class for all Masdarpersonalizationsampledatacustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class MasdarpersonalizationsampledatacustomaddonConstants extends GeneratedMasdarpersonalizationsampledatacustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarpersonalizationsampledatacustomaddon";

	private MasdarpersonalizationsampledatacustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
