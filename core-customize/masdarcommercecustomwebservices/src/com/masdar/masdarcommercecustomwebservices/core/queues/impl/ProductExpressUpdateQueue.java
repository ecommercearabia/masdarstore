/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommercecustomwebservices.core.queues.impl;

import com.masdar.masdarcommercecustomwebservices.core.queues.data.ProductExpressUpdateElementData;


/**
 * Queue for {@link com.masdar.masdarcommercecustomwebservices.core.queues.data.ProductExpressUpdateElementData}
 */
public class ProductExpressUpdateQueue extends AbstractUpdateQueue<ProductExpressUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
