/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommercecustomwebservices.core.queues.impl;

import com.masdar.masdarcommercecustomwebservices.core.queues.data.OrderStatusUpdateElementData;


/**
 * Queue for {@link com.masdar.masdarcommercecustomwebservices.core.queues.data.OrderStatusUpdateElementData}
 */
public class OrderStatusUpdateQueue extends AbstractUpdateQueue<OrderStatusUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
