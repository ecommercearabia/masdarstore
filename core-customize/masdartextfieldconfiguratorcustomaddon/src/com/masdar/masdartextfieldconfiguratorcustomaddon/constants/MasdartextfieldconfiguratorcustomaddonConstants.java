/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.masdartextfieldconfiguratorcustomaddon.constants;

/**
 * Global class for all Masdartextfieldconfiguratorcustomaddon constants. You can add global constants for your extension
 * into this class.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public final class MasdartextfieldconfiguratorcustomaddonConstants extends GeneratedMasdartextfieldconfiguratorcustomaddonConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "masdartextfieldconfiguratorcustomaddon";

	private MasdartextfieldconfiguratorcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
