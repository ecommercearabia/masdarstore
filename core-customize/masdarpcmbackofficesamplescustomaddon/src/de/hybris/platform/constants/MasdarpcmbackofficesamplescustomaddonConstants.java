/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package de.hybris.platform.constants;

/**
 * Global class for all Masdarpcmbackofficesamplescustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class MasdarpcmbackofficesamplescustomaddonConstants extends GeneratedMasdarpcmbackofficesamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarpcmbackofficesamplescustomaddon";

	private MasdarpcmbackofficesamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
