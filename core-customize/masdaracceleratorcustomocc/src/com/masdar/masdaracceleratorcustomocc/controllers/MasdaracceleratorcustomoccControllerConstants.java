/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaracceleratorcustomocc.controllers;

public interface MasdaracceleratorcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
