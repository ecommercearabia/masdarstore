/**
 *
 */
package com.masdar.masdarcommercialregistrationbackoffice.renderer;

import de.hybris.platform.omsbackoffice.renderers.NestedAttributeUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CompanyRegistrationInfoNestedAttributeUtils extends NestedAttributeUtils
{



	@Override
	public Object invokePropertyAsMethod(final Object object, final String propertyName)
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		if (object == null)
		{
			return null;
		}
		final Class objectClass = object.getClass();
		final Method getter = objectClass.getMethod(this.propertyNameToGetter(propertyName));
		return getter.invoke(object, (Object[]) null);
	}
}
