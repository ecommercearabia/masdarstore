/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.constants;

/**
 * Global class for all Masdarproductfacades constants. You can add global constants for your extension into this class.
 */
public final class MasdarproductfacadesConstants extends GeneratedMasdarproductfacadesConstants
{
	public static final String EXTENSIONNAME = "masdarproductfacades";

	private MasdarproductfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarproductfacadesPlatformLogo";
}
