/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantOptionsProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.masdarproduct.model.UnitVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultVariantOptionsGroceryProductPopulator extends SearchResultVariantOptionsProductPopulator
{
	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}

	private static final Logger LOG = Logger.getLogger(SearchResultVariantOptionsGroceryProductPopulator.class);

	@Override
	protected List<VariantOptionData> getVariantOptions(final List<SearchResultValueData> variants,
			final Set<String> variantTypeAttributes, final String rollupProperty)
	{
		final List<VariantOptionData> variantOptions = new ArrayList<>();

		for (final SearchResultValueData variant : variants)
		{
			final VariantOptionData variantOption = new VariantOptionData();
			variantOption.setCode((String) getValue(variant, CODE_VARIANT_PROPERTY));
			final Double priceValue = this.<Double> getValue(variant, PRICE_VALUE_VARIANT_PROPERTY);
			if (priceValue != null)
			{
				final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY,
						BigDecimal.valueOf(priceValue.doubleValue()), getCommonI18NService().getCurrentCurrency());
				variantOption.setPriceData(priceData);
			}

			variantOption.setUrl((String) getValue(variant, URL_VARIANT_PROPERTY));
			variantOption.setVariantOptionQualifiers(getVariantOptionQualifiers(variant, variantTypeAttributes, rollupProperty));

			populateUnitOfMeasure(variant, variantOption);

			variantOptions.add(variantOption);
		}
		return variantOptions;
	}

	protected void populateUnitOfMeasure(final SearchResultValueData variant, final VariantOptionData variantOption)
	{
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(this.<String> getValue(variant, "code"));

			if (productModel instanceof UnitVariantProductModel)
			{
				variantOption.setUnitOfMeasure(((UnitVariantProductModel) productModel).getUnitVariant());
				variantOption.setUnitOfMeasureDescription(((UnitVariantProductModel) productModel).getUnitVariantDescription());
				variantOption.setUnitColor(((UnitVariantProductModel) productModel).getUnitColor());
				variantOption.setUnitSize(((UnitVariantProductModel) productModel).getUnitSize());
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// DO NOTHING
		}
	}

	@Override
	protected void populatePrices(final SearchResultValueData source, final ProductData target)
	{
		// Pull the volume prices flag
		final Boolean volumePrices = this.<Boolean> getValue(source, "volumePrices");
		target.setVolumePricesFlag(volumePrices == null ? Boolean.FALSE : volumePrices);

		// Pull the price value for the current currency
		final Double priceValue = this.<Double> getValue(source, "priceValue");

		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(productCode);
			if (productModel != null)
			{
				populatePriceUsingproductModel(productModel, target);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}

	}

	public void populatePriceUsingproductModel(final ProductModel productModel, final ProductData productData)
	{
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCustomCommercePriceService().getWebPriceForProductWithTax(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCustomCommercePriceService().getWebPriceForProductWithTax(productModel);
		}

		if (info != null)
		{
			final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
			productData.setPrice(priceData);
		}
		else
		{
			productData.setPurchasable(Boolean.FALSE);
		}
	}
}
