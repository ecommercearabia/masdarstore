/**
 *
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;


/**
 * @author mbaker
 *
 */
public class ProductPriceWithTaxPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	protected CustomCommercePriceService getCommercePriceService()
	{
		return customCommercePriceService;
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (Objects.isNull(source) || Objects.isNull(target))
		{
			return;
		}
		target.setPriceWithTax(getPrice(source, true));
		target.setPriceWithoutTax(getPrice(source, false));


	}


	protected PriceData getPrice(final ProductModel productModel, final boolean withTax)
	{
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProductWithTax(productModel, withTax);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getWebPriceForProductWithTax(productModel, withTax);
		}

		if (info != null)
		{
			return getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
		}

		return null;
	}



}
