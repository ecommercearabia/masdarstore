/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductVolumePricesPopulator implements Populator<SearchResultValueData, ProductData>
{
	private static final Logger LOG = Logger.getLogger(SearchResultProductVolumePricesPopulator.class);

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productVolumePricesPopulator")
	private Populator<ProductModel, ProductData> productVolumePricesPopulator;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		populateVolumePrices(source, target);
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	protected void populateVolumePrices(final SearchResultValueData source, final ProductData target)
	{
		try
		{
			final ProductModel productModel = productService.getProductForCode(target.getCode());
			if (productModel != null)
			{
				productVolumePricesPopulator.populate(productModel, target);
			}
		}
		catch (final Exception ex)
		{
			LOG.error("Exception occurred while populating volume prices", ex);
			return;
		}
	}
}
