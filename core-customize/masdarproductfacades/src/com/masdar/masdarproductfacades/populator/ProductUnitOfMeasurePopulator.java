/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import com.masdar.masdarproduct.model.UnitVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductUnitOfMeasurePopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source instanceof UnitVariantProductModel)
		{
			final UnitVariantProductModel variant = (UnitVariantProductModel) source;
			target.setUnitOfMeasure(variant.getUnitOfMeasure());
			target.setUnitOfMeasureDescription(variant.getUnitVariantDescription());
			target.setUnitSize(variant.getUnitSize());
			target.setUnitColor(variant.getUnitColor());
			//target.setUnit(variant.getUnitVariantDescription());
		}
	}

}
