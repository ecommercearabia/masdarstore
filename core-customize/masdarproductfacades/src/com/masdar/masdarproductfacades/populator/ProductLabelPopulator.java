package com.masdar.masdarproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductLabelPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private static final String PRODUCT_LABEL_B2B_Site_Channel_ATTR = "MasdarClassification/1.0/MasdarHiddenClassification.productlabel";
	private static final String PRODUCT_LABEL_B2C_Site_Channel_ATTR = "Masdarb2cClassification/1.0/MasdarHiddenb2cClassification.productlabel";


	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateClassification(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateClassification(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features == null)
		{
			return;
		}

		final SiteChannel currentSiteChannel = getCurrentSiteChannel();
		final Feature collectionFeatureLabel = SiteChannel.B2B.equals(getCurrentSiteChannel())
				? features.getFeatureByCode(PRODUCT_LABEL_B2B_Site_Channel_ATTR)
				: features.getFeatureByCode(PRODUCT_LABEL_B2C_Site_Channel_ATTR);
		if (collectionFeatureLabel == null || collectionFeatureLabel.getValue() == null)
		{
			return;
		}
		target.setProductLabel(collectionFeatureLabel.getValue().getValue().toString());

	}

	private SiteChannel getCurrentSiteChannel()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return currentSite != null && SiteChannel.B2B.equals(currentSite.getChannel()) ? SiteChannel.B2B : SiteChannel.B2C;
	}

}