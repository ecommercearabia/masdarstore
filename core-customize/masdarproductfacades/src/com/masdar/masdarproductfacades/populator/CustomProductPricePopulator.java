/**
 *
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;


/**
 * @author monzer
 *
 */
public class CustomProductPricePopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductPricePopulator<SOURCE, TARGET>
{

	private CustomCommercePriceService commercePriceService;


	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProductWithTax(productModel, true);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}

		if (info != null)
		{
			final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
			productData.setPrice(priceData);
		}
		else
		{
			productData.setPurchasable(Boolean.FALSE);
		}

		populateForPickup(productModel, productData);
	}

	/**
	 *
	 */
	private void populateForPickup(final SOURCE productModel, final TARGET productData)
	{
		productData.setEnabledForPickup(productModel.isPickupInStoreFlag());
	}

	/**
	 * @return the commercePriceService
	 */
	@Override
	public CustomCommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}


	/**
	 * @param commercePriceService
	 *           the commercePriceService to set
	 */
	public void setCommercePriceService(final CustomCommercePriceService commercePriceService)
	{
		this.commercePriceService = commercePriceService;
	}

}
