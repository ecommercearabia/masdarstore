package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPriceRangePopulator.PriceRangeComparator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.core.service.CustomPriceService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductTaxPopulator implements Populator<SearchResultValueData, ProductData>
{
	private static final Logger LOG = Logger.getLogger(SearchResultProductTaxPopulator.class);

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productPriceWithTaxPopulator")
	private Populator<ProductModel, ProductData> productPriceWithTaxPopulator;

	@Resource(name = "productDiscountWithTaxPopulator")
	private Populator<ProductModel, ProductData> productDiscountWithTaxPopulator;
	@Resource(name = "customProductVolumePricesWithTaxPopulator")
	private Populator<ProductModel, ProductData> productVolumePricesWithTaxPopulator;



	@Resource(name = "customPriceService")
	private CustomPriceService priceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(productCode);
			if (productModel != null)
			{
				getProductDiscountWithTaxPopulator().populate(productModel, target);
				getProductPriceWithTaxPopulator().populate(productModel, target);
				getProductVolumePricesWithTaxPopulator().populate(productModel, target);
				target.setPriceRangeWithTax(getPriceRange(productModel, true));
				target.setPriceRangeWithoutTax(getPriceRange(productModel, false));
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}
	}

	protected PriceRangeData getPriceRange(final ProductModel productModel, final boolean tax)
	{
		if (productModel != null)
		{
			// make sure you have the baseProduct because variantProducts won't have other variants
			final ProductModel baseProduct;
			if (productModel instanceof VariantProductModel)
			{
				final VariantProductModel variant = (VariantProductModel) productModel;
				baseProduct = variant.getBaseProduct();
			}
			else
			{
				baseProduct = productModel;
			}

			final Collection<VariantProductModel> variants = baseProduct.getVariants();
			final List<PriceInformation> allPricesInfos = new ArrayList<PriceInformation>();

			// collect all price infos
			for (final VariantProductModel variant : variants)
			{
				final List<PriceInformation> priceInformationsForProduct = tax
						? getPriceService().getPriceInformationsForProductWithTax(variant)
						: getPriceService().getPriceInformationsForProduct(variant);
				allPricesInfos.addAll(priceInformationsForProduct);
				if (CollectionUtils.isEmpty(priceInformationsForProduct) && CollectionUtils.isNotEmpty(variant.getVariants()))
				{
					variant.getVariants().forEach(v -> findPriceStrategyAndAddAll(v, allPricesInfos, tax));
				}
			}

			// sort the list
			Collections.sort(allPricesInfos, PriceRangeComparator.INSTANCE);


			final PriceRangeData priceRange = new PriceRangeData();
			// get the min and max
			if (!allPricesInfos.isEmpty())
			{
				final PriceData min = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(0).getPriceValue().getValue()),
						allPricesInfos.get(0).getPriceValue().getCurrencyIso());

				final PriceData max = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getValue()),
						allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getCurrencyIso());

				priceRange.setMinPrice(min);
				priceRange.setMaxPrice(max);

			}

			return priceRange;

		}
		return null;

	}

	private void findPriceStrategyAndAddAll(final VariantProductModel variant, final List<PriceInformation> allPricesInfos,
			final boolean tax)
	{
		allPricesInfos.addAll(tax ? getPriceService().getPriceInformationsForProductWithTax(variant)
				: getPriceService().getPriceInformationsForProduct(variant));
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @return the productPriceWithTaxPopulator
	 */
	protected Populator<ProductModel, ProductData> getProductPriceWithTaxPopulator()
	{
		return productPriceWithTaxPopulator;
	}

	/**
	 * @return the productDiscountWithTaxPopulator
	 */
	protected Populator<ProductModel, ProductData> getProductDiscountWithTaxPopulator()
	{
		return productDiscountWithTaxPopulator;
	}

	/**
	 * @return the priceService
	 */
	protected CustomPriceService getPriceService()
	{
		return priceService;
	}

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}



	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @return the productVolumePricesWithTaxPopulator
	 */
	protected Populator<ProductModel, ProductData> getProductVolumePricesWithTaxPopulator()
	{
		return productVolumePricesWithTaxPopulator;
	}



}