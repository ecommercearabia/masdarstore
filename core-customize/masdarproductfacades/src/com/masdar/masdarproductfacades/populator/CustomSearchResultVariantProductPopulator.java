/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPriceRangePopulator.PriceRangeComparator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.core.service.CustomPriceService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomSearchResultVariantProductPopulator extends SearchResultVariantProductPopulator
{
	private static final Logger LOG = Logger.getLogger(CustomSearchResultVariantProductPopulator.class);


	@Resource(name = "customPriceService")
	private CustomPriceService priceService;

	@Resource(name = "priceService")
	private PriceService cpriceService;

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}


	@Override
	protected void populateStock(final SearchResultValueData source, final ProductData target)
	{
		final String stockLevelStatus = this.<String> getValue(source, "stockLevelStatus");
		try
		{
			// In case of low stock then make a call to the stock service to determine if in or out of stock.
			// In this case (low stock) it is ok to load the product from the DB and do the real stock check
			final ProductModel productModel = getProductService().getProductForCode(target.getCode());
			if (productModel != null)
			{
				target.setStock(getStockConverter().convert(productModel));
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// If the product is no longer visible to the customergroup then this exception can be thrown

			// We can't remove the product from the results, but we can mark it as out of stock
			target.setStock(getStockLevelStatusConverter().convert(StockLevelStatus.OUTOFSTOCK));
		}
	}

	@Override
	protected List<ImageData> createImageData(final SearchResultValueData source)
	{
		final List<ImageData> result = new ArrayList<>();

		addImageData(source, "thumbnail", result);
		addImageData(source, "product", result);
		addImageData(source, "store", result);
		addImageData(source, "zoom", result);

		return result;
	}

	@Override
	protected void populatePrices(final SearchResultValueData source, final ProductData target)
	{
		// Pull the volume prices flag
		final Boolean volumePrices = this.<Boolean> getValue(source, "volumePrices");
		target.setVolumePricesFlag(volumePrices == null ? Boolean.FALSE : volumePrices);

		// Pull the price value for the current currency
		final Double priceValue = this.<Double> getValue(source, "priceValue");

		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(productCode);
			if (productModel != null)
			{
				populatePriceUsingproductModel(productModel, target);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}

	}

	public void populatePriceUsingproductModel(final ProductModel productModel, final ProductData productData)
	{

		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCustomCommercePriceService().getWebPriceForProductWithTax(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCustomCommercePriceService().getWebPriceForProductWithTax(productModel);
		}

		if (info != null)
		{
			final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
			productData.setPrice(priceData);
		}
		else
		{
			productData.setPurchasable(Boolean.FALSE);
		}
	}

	/**
	 * Set price range for the {@link ProductData}. If there is no priceRange in the source, no {@link PriceRangeData}
	 * will be set in the target.
	 *
	 * @param source
	 *           The {@link SearchResultValueData} containing the priceRange.
	 * @param target
	 *           The {@link ProductData} to be modified.
	 */
	@Override
	protected void setPriceRange(final SearchResultValueData source, final ProductData target)
	{

		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(productCode);
			if (productModel != null)
			{
				populatePriceRange(productModel, target);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}
	}


	protected void populatePriceRange(final ProductModel productModel, final ProductData productData)
	{
		if (productModel != null && productData != null)
		{
			// make sure you have the baseProduct because variantProducts won't have other variants
			final ProductModel baseProduct;
			if (productModel instanceof VariantProductModel)
			{
				final VariantProductModel variant = (VariantProductModel) productModel;
				baseProduct = variant.getBaseProduct();
			}
			else
			{
				baseProduct = productModel;
			}

			final Collection<VariantProductModel> variants = baseProduct.getVariants();
			final List<PriceInformation> allPricesInfos = new ArrayList<PriceInformation>();

			// collect all price infos
			for (final VariantProductModel variant : variants)
			{
				final List<PriceInformation> priceInformationsForProduct = getPriceService()
						.getPriceInformationsForProductWithTax(variant);
				allPricesInfos.addAll(priceInformationsForProduct);
				if (CollectionUtils.isEmpty(priceInformationsForProduct) && CollectionUtils.isNotEmpty(variant.getVariants()))
				{
					variant.getVariants().forEach(v -> findPriceStrategyAndAddAll(v, allPricesInfos));
				}
			}

			// sort the list
			Collections.sort(allPricesInfos, PriceRangeComparator.INSTANCE);


			final PriceRangeData priceRange = new PriceRangeData();
			// get the min and max
			if (!allPricesInfos.isEmpty())
			{
				final PriceData min = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(0).getPriceValue().getValue()),
						allPricesInfos.get(0).getPriceValue().getCurrencyIso());

				final PriceData max = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getValue()),
						allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getCurrencyIso());

				priceRange.setMinPrice(min);
				priceRange.setMaxPrice(max);

			}

			productData.setPriceRange(priceRange);

		}

	}

	private void findPriceStrategyAndAddAll(final VariantProductModel variant, final List<PriceInformation> allPricesInfos)
	{
		allPricesInfos.addAll(getPriceService().getPriceInformationsForProductWithTax(variant));
	}


	/**
	 * @return the priceService
	 */
	public CustomPriceService getPriceService()
	{
		return priceService;
	}


}
