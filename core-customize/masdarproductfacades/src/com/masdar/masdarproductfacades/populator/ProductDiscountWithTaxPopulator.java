package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.DiscountPriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.service.CustomCommercePriceService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductDiscountWithTaxPopulator extends ProductDiscountPopulator
{

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;




	@Override
	public void populate(final ProductModel productModel, final ProductData productData)
	{
		if (Objects.isNull(productModel) || Objects.isNull(productData))
		{
			return;
		}

		productData.setDiscountWithTax(getDiscountPrice(productModel, true));
		productData.setDiscountWithoutTax(getDiscountPrice(productModel, false));
	}

	/**
	 *
	 */
	private DiscountPriceData getDiscountPrice(final ProductModel productModel, final boolean tax)
	{
		final Optional<BigDecimal> percentageValue = getPromotionPercentageValue(productModel);
		final PriceInformation info = getCustomCommercePriceService().getWebPriceForProductWithTax(productModel, tax);
		if (percentageValue.isPresent() && !Objects.isNull(info))
		{
			final double priceVal = convertPrice(info.getPriceValue().getValue());
			final double savingVal = percentageValue.get().doubleValue() * priceVal / 100;
			return createDiscountPriceData(priceVal, savingVal, info, PriceDataType.BUY);
		}
		else
		{
			final Optional<BigDecimal> originalPrice = getOriginalPriceValue(productModel);
			if (originalPrice.isPresent())
			{
				return createDiscountPriceData(originalPrice.get().doubleValue(),
						originalPrice.get().doubleValue() - convertPrice(info.getPriceValue().getValue()), info, PriceDataType.BUY);
			}
		}
		return null;

	}

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}


}
