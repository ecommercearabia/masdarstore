/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.acceleratorfacades.product.converters.populator.ProductVolumePricesPopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import com.masdar.core.service.CustomPriceService;


/**
 * Populator for product volume prices.
 */
public class CustomProductVolumePricesWithTaxPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
		ProductVolumePricesPopulator<SOURCE, TARGET>
{

	@Resource(name = "customPriceService")
	private CustomPriceService customPriceService;

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		if (Objects.isNull(productModel) || Objects.isNull(productData))
		{
			return;
		}

		productData.setVolumePricesWithTax(getVolumePrices(productModel, true));
		productData.setVolumePricesWithoutTax(getVolumePrices(productModel, false));
	}

	private List<PriceData> getVolumePrices(final SOURCE productModel, final boolean tax)
	{
		final List<PriceInformation> pricesInfos = tax ? getCustomPriceService().getPriceInformationsForProductWithTax(productModel)
				: getCustomPriceService().getPriceInformationsForProduct(productModel);
		if (pricesInfos == null || pricesInfos.size() < 2)
		{
			return Collections.<PriceData> emptyList();
		}
		else
		{
			final List<PriceData> volPrices = createPrices(productModel, pricesInfos);

			// Sort the list into quantity order
			Collections.sort(volPrices, VolumePriceComparator.INSTANCE);

			// Set the max quantities
			for (int i = 0; i < volPrices.size() - 1; i++)
			{
				volPrices.get(i).setMaxQuantity(Long.valueOf(volPrices.get(i + 1).getMinQuantity().longValue() - 1));
			}

			return volPrices;
		}
	}

	/**
	 * @return the customPriceService
	 */
	protected CustomPriceService getCustomPriceService()
	{
		return customPriceService;
	}

}
