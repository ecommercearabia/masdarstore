package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;

import java.util.List;

import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductLabelPopulator implements Populator<SearchResultValueData, ProductData>
{


	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		target.setProductLabel(getValue(source, "ProductLabel", String.class));
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName, final Class<T> clazz)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		final Object object = source.getValues().get(propertyName);
		if (clazz.isInstance(object))
		{
			return (T) object;
		}

		if (object instanceof List<?>)
		{
			return (T) ((List<?>) object).get(0);
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}
}
