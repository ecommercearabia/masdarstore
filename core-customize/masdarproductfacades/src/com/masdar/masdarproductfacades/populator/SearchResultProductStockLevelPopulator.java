/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.util.Assert;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductStockLevelPopulator implements Populator<SearchResultValueData, ProductData>
{
	@Resource(name = "stockConverter")
	private Converter<ProductModel, StockData> stockConverter;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "stockLevelStatusConverter")
	private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;



	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		populateStock(source, target);

		try
		{
			final ProductModel product = getProductService().getProduct(target.getCode());
			populateStockForSelectedCity(product, target);
			populateStockForSelectedPOS(product, target);
			populateStock(source, target);

		}
		catch (final UnknownIdentifierException ex)
		{
			target.setSelectedLocationStock(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK));
			target.setSelectedPOSStock(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK));

		}
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	protected void populateStock(final SearchResultValueData source, final ProductData target)
	{
		try
		{
			// In case of low stock then make a call to the stock service to determine if in or out of stock.
			// In this case (low stock) it is ok to load the product from the DB and do the real stock check
			final ProductModel productModel = productService.getProductForCode(target.getCode());
			if (productModel != null)
			{
				target.setStock(stockConverter.convert(productModel));
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			// If the product is no longer visible to the customergroup then this exception can be thrown

			// We can't remove the product from the results, but we can mark it as out of stock
			target.setStock(stockLevelStatusConverter.convert(StockLevelStatus.OUTOFSTOCK));
		}
	}

	/**
	 *
	 */
	private void populateStockForSelectedPOS(final ProductModel source, final ProductData target)
	{

		final StockData stockData = new StockData();

		stockData.setStockLevel(getCustomCommerceStockService().getStockLevelForProductAndPOSByCurrentCustomer(source));
		stockData.setStockLevelStatus(getCustomCommerceStockService().getStockLevelStatusForProductAndPOSByCurrentCustomer(source));
		stockData.setStockThreshold(getThresholdValue(source));

		target.setSelectedPOSStock(stockData);
	}


	/**
	 *
	 */
	private void populateStockForSelectedCity(final ProductModel source, final ProductData target)
	{
		final StockData stockData = new StockData();

		stockData.setStockLevel(getCustomCommerceStockService().getStockLevelForProductAndCityByCurrentCustomer(source));
		stockData
				.setStockLevelStatus(getCustomCommerceStockService().getStockLevelStatusForProductAndCityByCurrentCustomer(source));
		stockData.setStockThreshold(getThresholdValue(source));

		target.setSelectedLocationStock(stockData);

	}


	protected Integer getThresholdValue(final ProductModel productModel)
	{
		final Map<CategoryModel, Integer> thresholdOnCategory = new HashMap<>();
		final Set<CategoryModel> processedCategories = new HashSet<>();

		final Collection<CategoryModel> productSupercategories = new HashSet<>();
		productSupercategories.addAll(productModel.getSupercategories());

		if (productModel instanceof VariantProductModel)
		{
			final ProductModel baseProduct = ((VariantProductModel) productModel).getBaseProduct();
			final Collection<CategoryModel> baseProductSupercategories = baseProduct.getSupercategories();
			if (!CollectionUtils.isEmpty(baseProductSupercategories))
			{
				productSupercategories.addAll(baseProductSupercategories);
			}
		}

		for (final CategoryModel category : productSupercategories)
		{
			getThresholdfromOneTree(category, processedCategories, thresholdOnCategory);
		}

		if (MapUtils.isEmpty(thresholdOnCategory))
		{
			return getBaseSiteService().getCurrentBaseSite().getDefaultStockLevelThreshold();
		}
		else
		{
			for (final CategoryModel processedCategory : processedCategories)
			{
				if (thresholdOnCategory.containsKey(processedCategory))
				{
					thresholdOnCategory.remove(processedCategory);
				}
			}

			final Collection<Integer> thresholdToCompare = thresholdOnCategory.values();
			return Collections.min(thresholdToCompare);
		}
	}

	protected void getThresholdfromOneTree(final CategoryModel currentCategory, final Set<CategoryModel> processedCategories,
			final Map<CategoryModel, Integer> thresholdOnCategory)
	{

		if (currentCategory.getStockLevelThreshold() != null)
		{
			final Collection<CategoryModel> allSupercategories = getCategoryService()
					.getAllSupercategoriesForCategory(currentCategory);
			if (!CollectionUtils.isEmpty(allSupercategories))
			{
				processedCategories.addAll(allSupercategories);
			}
			thresholdOnCategory.put(currentCategory, currentCategory.getStockLevelThreshold());
			return;
		}

		final Collection<CategoryModel> supercategories = currentCategory.getSupercategories();

		if (!CollectionUtils.isEmpty(supercategories))
		{
			for (final CategoryModel category : supercategories)
			{
				getThresholdfromOneTree(category, processedCategories, thresholdOnCategory);
			}
		}
	}




	/**
	 *
	 */
	private void populateStock(final ProductModel source, final ProductData target)
	{
		final StockData stockData = getStockConverter().convert(source);
		target.setStock(stockData);

	}


	/**
	 * @return the stockConverter
	 */
	protected Converter<ProductModel, StockData> getStockConverter()
	{
		return stockConverter;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	/**
	 * @return the baseSiteService
	 */
	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}


	/**
	 * @return the categoryService
	 */
	protected CategoryService getCategoryService()
	{
		return categoryService;
	}


	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}



}
