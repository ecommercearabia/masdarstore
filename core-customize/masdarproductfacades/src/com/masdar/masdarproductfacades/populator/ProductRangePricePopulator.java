/**
 *
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPriceRangePopulator.PriceRangeComparator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.core.service.CustomPriceService;


/**
 * @author monzer
 *
 */
public class ProductRangePricePopulator implements Populator<ProductModel, ProductData>

{

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "customPriceService")
	private CustomPriceService priceService;

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;


	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (Objects.isNull(source) || Objects.isNull(target))
		{
			return;
		}

		target.setPriceRangeWithTax(getPriceRange(source, true));
		target.setPriceRangeWithoutTax(getPriceRange(source, false));


	}


	protected PriceRangeData getPriceRange(final ProductModel productModel, final boolean tax)
	{
		if (productModel != null)
		{
			// make sure you have the baseProduct because variantProducts won't have other variants
			final ProductModel baseProduct;
			if (productModel instanceof VariantProductModel)
			{
				final VariantProductModel variant = (VariantProductModel) productModel;
				baseProduct = variant.getBaseProduct();
			}
			else
			{
				baseProduct = productModel;
			}

			final Collection<VariantProductModel> variants = baseProduct.getVariants();
			final List<PriceInformation> allPricesInfos = new ArrayList<PriceInformation>();

			// collect all price infos
			for (final VariantProductModel variant : variants)
			{
				final List<PriceInformation> priceInformationsForProduct = tax
						? getPriceService().getPriceInformationsForProductWithTax(variant)
						: getPriceService().getPriceInformationsForProduct(variant);
				allPricesInfos.addAll(priceInformationsForProduct);
				if (CollectionUtils.isEmpty(priceInformationsForProduct) && CollectionUtils.isNotEmpty(variant.getVariants()))
				{
					variant.getVariants().forEach(v -> findPriceStrategyAndAddAll(v, allPricesInfos, tax));
				}
			}

			// sort the list
			Collections.sort(allPricesInfos, PriceRangeComparator.INSTANCE);


			final PriceRangeData priceRange = new PriceRangeData();
			// get the min and max
			if (!allPricesInfos.isEmpty())
			{
				final PriceData min = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(0).getPriceValue().getValue()),
						allPricesInfos.get(0).getPriceValue().getCurrencyIso());

				final PriceData max = getPriceDataFactory().create(PriceDataType.FROM,
						BigDecimal.valueOf(allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getValue()),
						allPricesInfos.get(allPricesInfos.size() - 1).getPriceValue().getCurrencyIso());

				priceRange.setMinPrice(min);
				priceRange.setMaxPrice(max);

			}

			return priceRange;

		}
		return null;

	}

	private void findPriceStrategyAndAddAll(final VariantProductModel variant, final List<PriceInformation> allPricesInfos,
			final boolean tax)
	{
		allPricesInfos.addAll(tax ? getPriceService().getPriceInformationsForProductWithTax(variant)
				: getPriceService().getPriceInformationsForProduct(variant));
	}





	/**
	 * @return the priceService
	 */
	public CustomPriceService getPriceService()
	{
		return priceService;
	}

	/**
	 * @return the customCommercePriceService
	 */
	public CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}


	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

}
