/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.masdar.core.service.CustomCommercePriceService;



/**
 * @author husam.dababneh@erabia.com
 */
public class ProductLowestAndSellingPricePopulator implements Populator<ProductModel, ProductData>
{


	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;
	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService commercePriceService;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}
	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}
	@Override
	public void populate(final ProductModel source , final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(source.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProductWithTax(source, true);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(source);
		}

		if (info == null)
		{
			return;
		}

		final PriceData lowestPriceData = getPriceData(source.getLowestPrice(), priceType, info);
		final PriceData sellingPriceData = getPriceData(source.getSellingPrice(), priceType, info);

		target.setLowestPrice(lowestPriceData);
		target.setSellingPrice(sellingPriceData);

	}

	/**
	 * @param value
	 * @param priceType
	 * @param info
	 * @return PriceData
	 */
	private PriceData getPriceData(final double value, final PriceDataType priceType, final PriceInformation info)
	{
		return getPriceDataFactory().create(priceType, BigDecimal.valueOf(value), info.getPriceValue().getCurrencyIso());
	}


}
