package com.masdar.masdarproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductLabelsPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private static final String PRODUCT_LABEL_B2B_SITE_CHANNEL_ATTR = "MasdarClassification/1.0/MasdarHiddenClassification.productlabel";
	//	private static final String PRODUCT_LABEL_B2C_SITE_CHANNEL_ATTR = "Masdarb2cClassification/1.0/MasdarHiddenb2cClassification.productlabel";


	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateLabels(source, target);
		}
	}

	/**
	 *
	 */
	private void populateLabels(final ProductModel source, final ProductData target)
	{
		final Set<ProductModel> variantsProducts = getVariantsProducts(source);

		variantsProducts.add(source);
		final Set<String> productLabels = new HashSet<>();

		for (final ProductModel productModel : variantsProducts)
		{
			productLabels.addAll(getProductLabels(productModel));
		}

		target.setProductLabels(new ArrayList<String>(productLabels));

	}

	private SiteChannel getCurrentSiteChannel()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return currentSite != null && SiteChannel.B2B.equals(currentSite.getChannel()) ? SiteChannel.B2B : SiteChannel.B2C;
	}


	private void getVariantsProducts(final ProductModel product, final Set<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);
		}
	}

	private Set<ProductModel> getVariantsProducts(final ProductModel product)
	{
		if (product == null)
		{
			return Collections.emptySet();
		}
		final Set<ProductModel> products = new HashSet<>();

		getVariantsProducts(product, products);
		return products;
	}

	private List<String> getProductLabels(final ProductModel source)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features == null)
		{
			return Collections.emptyList();
		}


		final Feature collectionFeatureLabel = features.getFeatureByCode(PRODUCT_LABEL_B2B_SITE_CHANNEL_ATTR);

		if (collectionFeatureLabel == null || CollectionUtils.isEmpty(collectionFeatureLabel.getValues()))
		{
			return Collections.emptyList();
		}

		final List<FeatureValue> values = collectionFeatureLabel.getValues();

		final List<String> labels = new ArrayList<>();

		for (final FeatureValue featureValue : values)
		{
			labels.add(featureValue.getValue().toString());
		}

		return labels;

	}
}