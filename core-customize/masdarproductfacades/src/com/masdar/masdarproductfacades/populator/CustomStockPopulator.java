/**
 *
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.converters.populator.StockPopulator;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Objects;

import javax.annotation.Resource;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * @author mbaker
 *
 */
public class CustomStockPopulator extends StockPopulator<ProductModel, StockData>
{
	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void populate(final ProductModel productModel, final StockData stockData) throws ConversionException
	{
		if (productModel.isDeliveryFlag())
		{
			stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
			stockData.setStockLevel(Long.valueOf(0));
			return;
		}
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		if (currentBaseSite == null || !currentBaseSite.isCustomerLocationEnabled() || !isCitySelected())
		{
			super.populate(productModel, stockData);
			return;
		}

		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		if (!isStockSystemEnabled(baseStore))
		{
			stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
			stockData.setStockLevel(Long.valueOf(0));
			return;
		}
		stockData.setStockLevel(getCustomCommerceStockService().getStockLevelForProductAndCityByCurrentCustomer(productModel));
		stockData.setStockLevelStatus(
				getCustomCommerceStockService().getStockLevelStatusForProductAndCityByCurrentCustomer(productModel));
		stockData.setStockThreshold(getThresholdValue(productModel));

	}

	/**
	 *
	 */
	protected boolean isCitySelected()
	{
		final UserModel currentUser = getUserService().getCurrentUser();

		return Objects.nonNull(currentUser) && Objects.nonNull(currentUser.getSelectedCity());

	}

	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}


}
