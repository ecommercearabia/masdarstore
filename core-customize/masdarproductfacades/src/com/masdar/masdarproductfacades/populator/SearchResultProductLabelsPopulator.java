package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductLabelsPopulator implements Populator<SearchResultValueData, ProductData>
{
	@Resource(name = "productLabelsPopulator")
	private Populator<ProductModel, ProductData> productLabelsPopulator;

	@Resource(name = "productService")
	private ProductService productService;

	private static final Logger LOG = Logger.getLogger(SearchResultProductLabelsPopulator.class);

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		// Pull the values directly from the SearchResult object
		//		target.setProductLabel(this.<String> getValue(source, "ProductLabel"));

		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			if (productModel != null)
			{
				productLabelsPopulator.populate(productModel, target);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}
}
