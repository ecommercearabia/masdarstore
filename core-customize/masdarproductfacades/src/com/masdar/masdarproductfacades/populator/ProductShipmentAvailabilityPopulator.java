/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author monzer
 */
public class ProductShipmentAvailabilityPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "stockPopulator")
	private Populator<ProductModel, StockData> stockPopulator;


	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (source == null || target == null)
		{
			return;
		}


		target.setStockAvailableForDelivery(isStockDataAvailableForDelivery(source));
		target.setStockAvailableForPickup(isVariantsProductsEnabledForPickup(source)); // for now
	}

	private boolean isVariantsProductsEnabledForPickup(final ProductModel product)
	{
		final List<ProductModel> variantsProducts = getVariantsProducts(product);

		return variantsProducts.stream().anyMatch(variant -> variant.isPickupInStoreFlag());
	}

	protected boolean isStockDataAvailableForDelivery(final ProductModel product)
	{
		final StockData stock = new StockData();
		final List<ProductModel> variantsProducts = getVariantsProducts(product);

		for (final ProductModel p : variantsProducts)
		{
			getStockPopulator().populate(p, stock);
			if (!StockLevelStatus.OUTOFSTOCK.equals(stock.getStockLevelStatus()) && !p.isDeliveryFlag())
			{
				return true;
			}
		}
		return false;
	}

	private void getVariantsProducts(final ProductModel product, final List<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);
		}
	}

	protected List<ProductModel> getVariantsProducts(final ProductModel product)
	{
		if (product == null)
		{
			return Collections.emptyList();
		}
		final List<ProductModel> products = new ArrayList<>();

		getVariantsProducts(product, products);
		return products;
	}

	/**
	 * @return the stockPopulator
	 */
	public Populator<ProductModel, StockData> getStockPopulator()
	{
		return stockPopulator;
	}

}
