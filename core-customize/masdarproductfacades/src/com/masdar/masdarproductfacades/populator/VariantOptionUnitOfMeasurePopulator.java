/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.masdarproduct.model.UnitVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionUnitOfMeasurePopulator implements Populator<VariantProductModel, VariantOptionData>
{

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{
		if (source instanceof UnitVariantProductModel)
		{
			final UnitVariantProductModel variant = (UnitVariantProductModel) source;
			target.setUnitOfMeasure(variant.getUnitOfMeasure());
			target.setUnitOfMeasureDescription(variant.getUnitVariantDescription());
			target.setUnitColor(variant.getUnitColor());
			target.setUnitSize(variant.getUnitSize());
			populateIsActiveOnB2C(variant, target);
		}
	}

	/**
	 *
	 */
	private void populateIsActiveOnB2C(final UnitVariantProductModel variant, final VariantOptionData target)
	{
		target.setActiveOnB2C(isActiveOnB2C(variant));
	}

	private void getVariantsProducts(final ProductModel product, final List<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);
		}
	}

	private List<ProductModel> getVariantsProducts(final ProductModel product)
	{
		if (product == null)
		{
			return Collections.emptyList();
		}
		final List<ProductModel> products = new ArrayList<>();

		getVariantsProducts(product, products);
		return products;
	}

	private boolean isActiveOnB2C(final ProductModel productModel)
	{
		if (productModel == null)
		{
			return false;
		}

		final List<ProductModel> variantsProducts = getVariantsProducts(productModel);
		if (CollectionUtils.isEmpty(variantsProducts))
		{
			return productModel.isActiveOnB2C();
		}

		for (final ProductModel product : variantsProducts)
		{
			if (product.isActiveOnB2C())
			{
				return true;
			}
		}


		return false;

	}

}
