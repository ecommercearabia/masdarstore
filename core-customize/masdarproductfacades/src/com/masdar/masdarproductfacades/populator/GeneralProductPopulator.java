package com.masdar.masdarproductfacades.populator;


import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import com.masdar.masdarproduct.model.UnitVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class GeneralProductPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		target.setActiveOnB2C(source.isActiveOnB2C());

		target.setEan(source.getEan());

		populateUnitOfMeasure(source, target);
	}

	/**
	 *
	 */
	private void populateUnitOfMeasure(final ProductModel source, final ProductData target)
	{

		if (source instanceof UnitVariantProductModel)
		{
			target.setUnitOfMeasure(((UnitVariantProductModel) source).getUnitVariant());
		}

	}


}