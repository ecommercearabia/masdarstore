package com.masdar.masdarproductfacades.populator;


import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductStockPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	@Resource(name = "stockConverter")
	private Converter<ProductModel, StockData> stockConverter;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (getBaseSiteService().getCurrentBaseSite() == null)
		{
			return;
		}
		populateStockForSelectedCity(source, target);
		populateStockForSelectedPOS(source, target);
		populateStock(source, target);
	}


	/**
	 *
	 */
	private void populateStock(final ProductModel source, final ProductData target)
	{
		final StockData stockData = getStockConverter().convert(source);
		target.setStock(stockData);
	}


	/**
	 *
	 */
	private void populateStockForSelectedPOS(final ProductModel source, final ProductData target)
	{
		final StockData stockData = new StockData();

		if (!source.isPickupInStoreFlag())
		{
			stockData.setStockLevel(0L);
			stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
		}
		else
		{
			stockData.setStockLevel(getCustomCommerceStockService().getStockLevelForProductAndPOSByCurrentCustomer(source));
			stockData
					.setStockLevelStatus(getCustomCommerceStockService().getStockLevelStatusForProductAndPOSByCurrentCustomer(source));
			stockData.setStockThreshold(getThresholdValue(source));
		}

		target.setSelectedPOSStock(stockData);
	}


	/**
	 *
	 */
	private void populateStockForSelectedCity(final ProductModel source, final ProductData target)
	{
		final StockData stockData = new StockData();

		stockData.setStockLevel(getCustomCommerceStockService().getStockLevelForProductAndCityByCurrentCustomer(source));
		stockData
				.setStockLevelStatus(getCustomCommerceStockService().getStockLevelStatusForProductAndCityByCurrentCustomer(source));
		stockData.setStockThreshold(getThresholdValue(source));

		target.setSelectedLocationStock(stockData);
	}


	protected Integer getThresholdValue(final ProductModel productModel)
	{
		final Map<CategoryModel, Integer> thresholdOnCategory = new HashMap<>();
		final Set<CategoryModel> processedCategories = new HashSet<>();

		final Collection<CategoryModel> productSupercategories = new HashSet<>();
		productSupercategories.addAll(productModel.getSupercategories());

		if (productModel instanceof VariantProductModel)
		{
			final ProductModel baseProduct = ((VariantProductModel) productModel).getBaseProduct();
			final Collection<CategoryModel> baseProductSupercategories = baseProduct.getSupercategories();
			if (!CollectionUtils.isEmpty(baseProductSupercategories))
			{
				productSupercategories.addAll(baseProductSupercategories);
			}
		}

		for (final CategoryModel category : productSupercategories)
		{
			getThresholdfromOneTree(category, processedCategories, thresholdOnCategory);
		}

		if (MapUtils.isEmpty(thresholdOnCategory))
		{
			return getBaseSiteService().getCurrentBaseSite().getDefaultStockLevelThreshold();
		}
		else
		{
			for (final CategoryModel processedCategory : processedCategories)
			{
				if (thresholdOnCategory.containsKey(processedCategory))
				{
					thresholdOnCategory.remove(processedCategory);
				}
			}

			final Collection<Integer> thresholdToCompare = thresholdOnCategory.values();
			return Collections.min(thresholdToCompare);
		}
	}

	protected void getThresholdfromOneTree(final CategoryModel currentCategory, final Set<CategoryModel> processedCategories,
			final Map<CategoryModel, Integer> thresholdOnCategory)
	{

		if (currentCategory.getStockLevelThreshold() != null)
		{
			final Collection<CategoryModel> allSupercategories = getCategoryService()
					.getAllSupercategoriesForCategory(currentCategory);
			if (!CollectionUtils.isEmpty(allSupercategories))
			{
				processedCategories.addAll(allSupercategories);
			}
			thresholdOnCategory.put(currentCategory, currentCategory.getStockLevelThreshold());
			return;
		}

		final Collection<CategoryModel> supercategories = currentCategory.getSupercategories();

		if (!CollectionUtils.isEmpty(supercategories))
		{
			for (final CategoryModel category : supercategories)
			{
				getThresholdfromOneTree(category, processedCategories, thresholdOnCategory);
			}
		}
	}


	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	/**
	 * @return the baseSiteService
	 */
	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}


	/**
	 * @return the categoryService
	 */
	protected CategoryService getCategoryService()
	{
		return categoryService;
	}


	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}


	/**
	 * @return the stockConverter
	 */
	protected Converter<ProductModel, StockData> getStockConverter()
	{
		return stockConverter;
	}
}