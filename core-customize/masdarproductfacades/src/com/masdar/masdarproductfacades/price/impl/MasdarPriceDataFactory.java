package com.masdar.masdarproductfacades.price.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


/**
 * @author husam.dababaneh@erabia.com
 *
 */
public class MasdarPriceDataFactory extends de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory
{

	@Override
	protected DecimalFormat adjustSymbol(final DecimalFormat format, final CurrencyModel currencyModel)
	{
		final String symbol = currencyModel.getSymbolLoc();
		if (symbol != null)
		{
			final DecimalFormatSymbols symbols = format.getDecimalFormatSymbols(); // does cloning
			final String iso = currencyModel.getIsocode();
			boolean changed = false;
			if (!iso.equalsIgnoreCase(symbols.getInternationalCurrencySymbol()))
			{
				symbols.setInternationalCurrencySymbol(iso);
				changed = true;
			}
			if (!symbol.equals(symbols.getCurrencySymbol()))
			{
				symbols.setCurrencySymbol(symbol);
				changed = true;
			}
			if (changed)
			{
				format.setDecimalFormatSymbols(symbols);
			}

			fixPostFixAndPrefixForArabic(format);
		}
		return format;
	}

	private void fixPostFixAndPrefixForArabic(final DecimalFormat format)
	{
		final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
		if (currentLanguage != null && currentLanguage.getIsocode().equals("ar"))
		{
			format.setPositiveSuffix(" " + format.getPositivePrefix());
			format.setPositivePrefix("");
		}
	}

}