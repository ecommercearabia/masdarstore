/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaraccountsummarycustomaddon.document;

public interface Range
{

	Object getMinBoundary();

	Object getMaxBoundary();
}
