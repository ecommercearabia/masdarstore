/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaraccountsummarycustomaddon.document.service;

import de.hybris.platform.servicelayer.search.SearchResult;

import com.masdar.masdaraccountsummarycustomaddon.model.B2BDocumentTypeModel;


/**
 * Provides services for the B2BDocument type
 */
public interface B2BDocumentTypeService
{
	/**
	 * Gets all document types.
	 * 
	 * @return a SearchResult<B2BDocumentTypeModel> containing document types.
	 */
	SearchResult<B2BDocumentTypeModel> getAllDocumentTypes();
}
