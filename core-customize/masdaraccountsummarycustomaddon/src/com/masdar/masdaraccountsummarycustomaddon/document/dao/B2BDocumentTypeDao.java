/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaraccountsummarycustomaddon.document.dao;


import de.hybris.platform.servicelayer.search.SearchResult;

import com.masdar.masdaraccountsummarycustomaddon.model.B2BDocumentTypeModel;


public interface B2BDocumentTypeDao
{

	/**
	 * Gets all document types.
	 * 
	 * @return a SearchResult<B2BDocumentTypeModel> containing a list of all document types.
	 */
	SearchResult<B2BDocumentTypeModel> getAllDocumentTypes();
}
