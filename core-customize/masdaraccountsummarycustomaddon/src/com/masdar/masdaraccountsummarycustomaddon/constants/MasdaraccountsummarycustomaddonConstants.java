/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaraccountsummarycustomaddon.constants;

/**
 * Global class for all Masdaraccountsummarycustomaddon constants. You can add global constants for your extension into this class.
 */
public final class MasdaraccountsummarycustomaddonConstants extends GeneratedMasdaraccountsummarycustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdaraccountsummarycustomaddon";
	public static final String ACCOUNT_SUMMARY_UNIT_TREE_PAGE = "addon:/masdaraccountsummarycustomaddon/cms/accountsummaryunittreecomponent";
	public static final String ACCOUNT_STATUS_DOCUMENTLIST = "addon:/masdaraccountsummarycustomaddon/pages/documents";
	public static final String ACCOUNT_STATUS_COMPONENTVIEW = "addon:/masdaraccountsummarycustomaddon/cms/accountsummaryaccountstatuscomponent";

	public static final String PAYORUSEINTERFACENAME = "PayableOrUsableInterfaceActive";

	public static final String DEFAULT_SORT = "asc";
	public static final String DEFAULT_PAGE_SIZE = "10";
	public static final String DEFAULT_PAGE = "0";
	public static final String ERROR_ATTRIBUTE = "error";
	public static final String ACCOUNT_STATUS_CMS_PAGE = "accountstatus";
	public static final String ACCOUNT_STATUS_DETAIL_CMS_PAGE = "accountstatusdetail";
	public static final String SEARCH_STATUS_OPEN = "status_open";
	public static final String SEARCH_STATUS_CLOSED = "status_closed";
	public static final String SEARCH_BY_DOCUMENTNUMBER = "documentNumber";

	public static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";

	public static final String ACCOUNT_SUMMARY_UNIT_CMS_PAGE = "accountsummaryunittree";
	public static final String ACCOUNT_SUMMARY_UNIT_DETAILS_LIST_CMS_PAGE = "accountsummaryunitdetailslist";

	public static final String DEFAULT_SORT_CODE_PROP = "accountsummary.documents.defaultSortCode";

	private MasdaraccountsummarycustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
