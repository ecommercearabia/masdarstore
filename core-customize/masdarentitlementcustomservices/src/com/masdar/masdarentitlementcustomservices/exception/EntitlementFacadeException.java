/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarentitlementcustomservices.exception;

public class EntitlementFacadeException extends Exception
{
	/**
	 * @param message
	 */
	public EntitlementFacadeException(final String message)
	{
		super(message);
	}
}
