/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
/**
 * Default implementation of entitlement facade and some related things.
 */
package com.masdar.masdarentitlementcustomservices.entitlement.impl;
