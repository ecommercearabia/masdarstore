/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarentitlementcustomservices.constants;

/**
 * Global class for all Masdarentitlementcustomservices constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings({ "PMD", "squid:CallToDeprecatedMethod" })
public final class MasdarentitlementcustomservicesConstants extends GeneratedMasdarentitlementcustomservicesConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "masdarentitlementcustomservices";

	private MasdarentitlementcustomservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
