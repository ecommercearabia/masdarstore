/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarentitlementcustomservices.interceptor.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import com.masdar.masdarentitlementcustomservices.enums.EntitlementTimeUnit;
import com.masdar.masdarentitlementcustomservices.model.ProductEntitlementModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Test;

@IntegrationTest
public class ProductEntitlementInitDefaultsInterceptorTest extends ServicelayerTest
{
	@Resource
	private ModelService modelService;

	@Test
	public void shouldApplyDefaultsForEndDate()
	{
		final ProductEntitlementModel model = new ProductEntitlementModel();
		modelService.initDefaults(model);
		assertEquals(EntitlementTimeUnit.MONTH, model.getTimeUnit());
		assertEquals(1, (int) model.getTimeUnitStart());
		assertNull(model.getTimeUnitDuration());
	}
}
