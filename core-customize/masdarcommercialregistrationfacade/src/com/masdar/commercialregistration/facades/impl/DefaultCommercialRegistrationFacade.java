/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.facades.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.facades.CommercialRegistrationFacade;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.service.CommercialRegistrationService;


/**
 *
 */
public class DefaultCommercialRegistrationFacade implements CommercialRegistrationFacade
{


	@Resource(name = "commercialRegistrationConverter")
	private Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfoData> commercialRegistrationConverter;

	@Resource(name = "commercialRegistrationService")
	private CommercialRegistrationService commercialRegistrationService;

	@Resource(name = "commercialRegistrationThirdPartyConverter")
	private Converter<CompanyRegistrationInfo, CompanyRegistrationInfoData> commercialRegistrationThirdPartyConverter;


	@Resource(name = "userService")
	private UserService userService;

	@Override
	public Optional<CompanyRegistrationInfoData> getCompanyCrInfo(final B2BCustomerModel customer)
	{
		Preconditions.checkArgument(customer != null, "B2BCustomerModel is null");

		if (customer.getCompanyRegistrationInfo() == null)
		{
			Optional.empty();
		}

		return Optional.ofNullable(getCommercialRegistrationConverter().convert(customer.getCompanyRegistrationInfo()));
	}

	@Override
	public Optional<CompanyRegistrationInfoData> getCompanyCrInfoByCurrentCustomer()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser == null)
		{
			throw new IllegalArgumentException("currentUser is null");
		}
		if (!(currentUser instanceof B2BCustomerModel))
		{
			throw new IllegalArgumentException("currentUser is not B2BCustomerModel");
		}
		return getCompanyCrInfo((B2BCustomerModel) getUserService().getCurrentUser());
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the commercialRegistrationService
	 */
	protected CommercialRegistrationService getCommercialRegistrationService()
	{
		return commercialRegistrationService;
	}

	/**
	 * @return the commercialRegistrationConverter
	 */
	protected Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfoData> getCommercialRegistrationConverter()
	{
		return commercialRegistrationConverter;
	}


	/**
	 * @return the commercialRegistrationThirdPartyConverter
	 */
	protected Converter<CompanyRegistrationInfo, CompanyRegistrationInfoData> getCommercialRegistrationThirdPartyConverter()
	{
		return commercialRegistrationThirdPartyConverter;
	}

	@Override
	public Optional<CompanyRegistrationInfoData> getCompanyCrInfoByCurrentBaseStore(final String crNumber)
	{
		Preconditions.checkArgument(!crNumber.isBlank(), "crNumber is null");
		try
		{
			final Optional<CompanyRegistrationInfo> companyRegistrationInfo = getCommercialRegistrationService()
					.getCompanyCrInfoByCurrentStore(crNumber);

			if (companyRegistrationInfo.isEmpty())
			{
				return Optional.empty();
			}
			return Optional.ofNullable(getCommercialRegistrationThirdPartyConverter().convert(companyRegistrationInfo.get()));

		}
		catch (final CommercialRegistrationException e)
		{
			return Optional.empty();
		}
	}


}
