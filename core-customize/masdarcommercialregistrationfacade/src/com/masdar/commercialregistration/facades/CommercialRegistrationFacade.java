/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.facades;

import de.hybris.platform.b2b.model.B2BCustomerModel;

import java.util.Optional;

import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;

/**
 *
 */
public interface CommercialRegistrationFacade
{

	Optional<CompanyRegistrationInfoData> getCompanyCrInfo(final B2BCustomerModel customer);

	Optional<CompanyRegistrationInfoData> getCompanyCrInfoByCurrentCustomer();

	Optional<CompanyRegistrationInfoData> getCompanyCrInfoByCurrentBaseStore(final String crNumber);

}
