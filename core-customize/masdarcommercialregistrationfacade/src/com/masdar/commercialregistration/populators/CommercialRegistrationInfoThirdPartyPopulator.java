/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrBusinessType;
import com.masdar.commercialregistration.beans.CrCancellation;
import com.masdar.commercialregistration.beans.CrCompany;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.beans.CrIsic;
import com.masdar.commercialregistration.beans.CrLocation;
import com.masdar.commercialregistration.beans.CrStatus;
import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.data.CrActivitiesData;
import com.masdar.commercialregistration.data.CrBusinessTypeData;
import com.masdar.commercialregistration.data.CrCalendarTypeData;
import com.masdar.commercialregistration.data.CrCancellationData;
import com.masdar.commercialregistration.data.CrCompanyData;
import com.masdar.commercialregistration.data.CrFiscalYearData;
import com.masdar.commercialregistration.data.CrIsicData;
import com.masdar.commercialregistration.data.CrLocationData;
import com.masdar.commercialregistration.data.CrStatusData;


/**
 *
 */
public class CommercialRegistrationInfoThirdPartyPopulator
		implements Populator<CompanyRegistrationInfo, CompanyRegistrationInfoData>
{

	@Override
	public void populate(final CompanyRegistrationInfo source, final CompanyRegistrationInfoData target) throws ConversionException
	{
		if (source == null)
		{
			return;
		}
		target.setCrName(source.getCrName());
		target.setCrNumber(source.getCrNumber());
		target.setCrEntityNumber(source.getCrEntityNumber());
		target.setIssueDate(source.getIssueDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setCrMainNumber(source.getCrMainNumber());

		populateBusinessType(source.getBusinessType(), target);
		populateFiscalYear(source.getFiscalYear(), target);
		populateStatus(source.getStatus(), target);
		populateCancellation(source.getCancellation(), target);
		populateLocation(source.getLocation(), target);
		populateCompany(source.getCompany(), target);
		populateActivities(source.getActivities(), target);

	}

	private List<CrIsicData> getIsicData(final List<CrIsic> list)
	{
		if (list == null)
		{
			return null;
		}

		final List<CrIsicData> data = new ArrayList<>(list.size());

		for (final CrIsic source : list)
		{
			final CrIsicData d = new CrIsicData();
			d.setId(source.getId());
			d.setName(source.getName());
			d.setNameEN(source.getNameEN());
			data.add(d);
		}

		return data;
	}

	/**
	 *
	 */
	private void populateActivities(final CrActivities crActivities, final CompanyRegistrationInfoData target)
	{
		CrActivitiesData activities = null;
		if (crActivities != null)
		{
			activities = new CrActivitiesData();
			activities.setDescription(crActivities.getDescription());
			activities.setIsic(getIsicData(crActivities.getIsic()));
		}
		target.setActivities(activities);
	}

	/**
	 *
	 */
	private void populateCompany(final CrCompany crCompany, final CompanyRegistrationInfoData target)
	{
		CrCompanyData company = null;
		if (crCompany != null)
		{
			company = new CrCompanyData();
			company.setPeriod(crCompany.getPeriod());
			company.setEndDate(crCompany.getEndDate());
			company.setStartDate(crCompany.getStartDate());
		}
		target.setCompany(company);
	}

	/**
	 *
	 */
	private void populateLocation(final CrLocation crLocation, final CompanyRegistrationInfoData target)
	{
		CrLocationData location = null;
		if (crLocation != null)
		{
			location = new CrLocationData();
			location.setId(crLocation.getId());
			location.setName(crLocation.getName());
		}
		target.setLocation(location);
	}

	/**
	 *
	 */
	private void populateCancellation(final CrCancellation crCancellation, final CompanyRegistrationInfoData target)
	{
		CrCancellationData cancellation = null;
		if (crCancellation != null)
		{
			cancellation = new CrCancellationData();
			cancellation.setDate(crCancellation.getDate());
			cancellation.setReason(crCancellation.getReason());
		}
		target.setCancellation(cancellation);
	}


	/**
	 *
	 */
	private void populateStatus(final CrStatus crStatus, final CompanyRegistrationInfoData target)
	{
		CrStatusData status = null;
		if (crStatus != null)
		{
			status = new CrStatusData();
			status.setId(crStatus.getId());
			status.setName(crStatus.getName());
			status.setNameEn(crStatus.getNameEn());
		}
		target.setStatus(status);
	}

	/**
	 *
	 */
	private void populateFiscalYear(final CrFiscalYear crFiscalYear, final CompanyRegistrationInfoData target)
	{
		CrFiscalYearData fiscalYearData = null;
		if (crFiscalYear != null)
		{
			fiscalYearData = new CrFiscalYearData();
			final CrCalendarTypeData crCalendarTypeData = new CrCalendarTypeData();
			crCalendarTypeData.setId(crFiscalYear.getCalendarType().getId());
			crCalendarTypeData.setName(crFiscalYear.getCalendarType().getName());
			fiscalYearData.setCalendarType(crCalendarTypeData);
			fiscalYearData.setDay(crFiscalYear.getDay());
			fiscalYearData.setMonth(crFiscalYear.getMonth());
		}
		target.setFiscalYear(fiscalYearData);
	}

	/**
	 *
	 */
	private void populateBusinessType(final CrBusinessType source, final CompanyRegistrationInfoData target)
	{
		final CrBusinessTypeData businessTypeData = new CrBusinessTypeData();
		businessTypeData.setId(source.getId());
		businessTypeData.setName(source.getName());
		target.setBusinessType(businessTypeData);
	}

}
