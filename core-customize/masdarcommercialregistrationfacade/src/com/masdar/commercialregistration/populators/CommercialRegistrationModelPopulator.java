/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrBusinessType;
import com.masdar.commercialregistration.beans.CrCancellation;
import com.masdar.commercialregistration.beans.CrCompany;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.beans.CrIsic;
import com.masdar.commercialregistration.beans.CrLocation;
import com.masdar.commercialregistration.beans.CrStatus;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrBusinessTypeModel;
import com.masdar.commercialregistration.model.CrCalendarTypeModel;
import com.masdar.commercialregistration.model.CrCancellationModel;
import com.masdar.commercialregistration.model.CrCompanyModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;
import com.masdar.commercialregistration.model.CrIsicModel;
import com.masdar.commercialregistration.model.CrLocationModel;
import com.masdar.commercialregistration.model.CrStatusModel;


/**
 *
 */
public class CommercialRegistrationModelPopulator implements Populator<CompanyRegistrationInfo, CompanyRegistrationInfoModel>
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void populate(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
			throws ConversionException
	{
		target.setCode(source.getCrNumber());
		target.setCrName(source.getCrName());
		target.setCrNumber(source.getCrNumber());
		target.setCrEntityNumber(source.getCrEntityNumber());
		target.setIssueDate(source.getIssueDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setCrMainNumber(source.getCrMainNumber());

		populateBusinessType(source.getBusinessType(), target);
		populateFiscalYear(source.getFiscalYear(), target);
		populateStatus(source.getStatus(), target);
		populateCancellation(source.getCancellation(), target);
		populateLocation(source.getLocation(), target);
		populateCompany(source.getCompany(), target);
		populateActivities(source.getActivities(), target);



	}

	private List<CrIsicModel> getIsicData(final List<CrIsic> list)
	{
		if (list == null)
		{
			return null;
		}

		final List<CrIsicModel> data = new ArrayList<>(list.size());

		for (final CrIsic source : list)
		{
			final CrIsicModel d = modelService.create(CrIsicModel.class);
			d.setId(source.getId());
			d.setName(source.getName());
			d.setNameEN(source.getNameEN());
			data.add(d);
		}

		return data;
	}

	/**
	 *
	 */
	private void populateActivities(final CrActivities crActivities, final CompanyRegistrationInfoModel target)
	{
		final CrActivitiesModel activities = modelService.create(CrActivitiesModel.class);
		if (crActivities != null)
		{
			activities.setLongDescription(crActivities.getDescription());
			activities.setIsic(getIsicData(crActivities.getIsic()));
		}
		target.setActivities(activities);
	}

	/**
	 *
	 */
	private void populateCompany(final CrCompany crCompany, final CompanyRegistrationInfoModel target)
	{
		final CrCompanyModel company = modelService.create(CrCompanyModel.class);
		if (crCompany != null)
		{
			company.setPeriod(crCompany.getPeriod());
			company.setEndDate(crCompany.getEndDate());
			company.setStartDate(crCompany.getStartDate());
		}
		target.setCompany(company);
	}

	/**
	 *
	 */
	private void populateLocation(final CrLocation crLocation, final CompanyRegistrationInfoModel target)
	{
		final CrLocationModel location = modelService.create(CrLocationModel.class);
		if (location != null)
		{
			location.setId(crLocation.getId());
			location.setName(crLocation.getName());
		}
		target.setLocation(location);
	}

	/**
	 *
	 */
	private void populateCancellation(final CrCancellation crCancellation, final CompanyRegistrationInfoModel target)
	{
		final CrCancellationModel cancellation = modelService.create(CrCancellationModel.class);
		if (crCancellation != null)
		{
			cancellation.setDate(crCancellation.getDate());
			cancellation.setReason(crCancellation.getReason());
		}
		target.setCancellation(cancellation);
	}


	/**
	 *
	 */
	private void populateStatus(final CrStatus crStatus, final CompanyRegistrationInfoModel target)
	{
		final CrStatusModel status = modelService.create(CrStatusModel.class);
		if (status != null)
		{
			status.setId(crStatus.getId());
			status.setName(crStatus.getName());
			status.setNameEn(crStatus.getNameEn());
		}
		target.setStatus(status);
	}

	/**
	 *
	 */
	private void populateFiscalYear(final CrFiscalYear crFiscalYear, final CompanyRegistrationInfoModel target)
	{

		final CrFiscalYearModel model = modelService.create(CrFiscalYearModel.class);
		final CrCalendarTypeModel crCalendarType = modelService.create(CrCalendarTypeModel.class);
		if (crFiscalYear != null)
		{
			crCalendarType.setId(crFiscalYear.getCalendarType() == null ? 0 : crFiscalYear.getCalendarType().getId());
			crCalendarType.setName(crFiscalYear.getCalendarType() == null ? "" : crFiscalYear.getCalendarType().getName());
			model.setMonth(crFiscalYear.getMonth());
			model.setDay(crFiscalYear.getDay());
		}
		model.setCalendarType(crCalendarType);

		target.setFiscalYear(model);
	}

	/**
	 *
	 */
	private void populateBusinessType(final CrBusinessType crBusinessType, final CompanyRegistrationInfoModel target)
	{
		final CrBusinessTypeModel model = modelService.create(CrBusinessTypeModel.class);
		if (crBusinessType != null)
		{
			model.setId(crBusinessType.getId());
			model.setName(crBusinessType.getName());
		}
		target.setBusinessType(model);
	}

}
