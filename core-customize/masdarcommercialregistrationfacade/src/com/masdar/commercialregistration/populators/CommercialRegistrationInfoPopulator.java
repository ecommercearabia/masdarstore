/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.data.CrActivitiesData;
import com.masdar.commercialregistration.data.CrBusinessTypeData;
import com.masdar.commercialregistration.data.CrCalendarTypeData;
import com.masdar.commercialregistration.data.CrCancellationData;
import com.masdar.commercialregistration.data.CrCompanyData;
import com.masdar.commercialregistration.data.CrFiscalYearData;
import com.masdar.commercialregistration.data.CrIsicData;
import com.masdar.commercialregistration.data.CrLocationData;
import com.masdar.commercialregistration.data.CrStatusData;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrBusinessTypeModel;
import com.masdar.commercialregistration.model.CrCancellationModel;
import com.masdar.commercialregistration.model.CrCompanyModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;
import com.masdar.commercialregistration.model.CrIsicModel;
import com.masdar.commercialregistration.model.CrLocationModel;
import com.masdar.commercialregistration.model.CrStatusModel;


/**
 *
 */
public class CommercialRegistrationInfoPopulator implements Populator<CompanyRegistrationInfoModel, CompanyRegistrationInfoData>
{

	@Override
	public void populate(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfoData target)
			throws ConversionException
	{
		if (source == null)
		{
			return;
		}
		target.setCrName(source.getCrName());
		target.setCrNumber(source.getCrNumber());
		target.setCrEntityNumber(source.getCrEntityNumber());
		target.setIssueDate(source.getIssueDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setCrMainNumber(source.getCrMainNumber());

		populateBusinessType(source.getBusinessType(), target);
		populateFiscalYear(source.getFiscalYear(), target);
		populateStatus(source.getStatus(), target);
		populateCancellation(source.getCancellation(), target);
		populateLocation(source.getLocation(), target);
		populateCompany(source.getCompany(), target);
		populateActivities(source.getActivities(), target);

	}

	private List<CrIsicData> getIsicData(final List<CrIsicModel> list)
	{
		if (list == null)
		{
			return null;
		}

		final List<CrIsicData> data = new ArrayList<>(list.size());

		for (final CrIsicModel source : list)
		{
			final CrIsicData d = new CrIsicData();
			d.setId(source.getId());
			d.setName(source.getName());
			d.setNameEN(source.getNameEN());
			data.add(d);
		}

		return data;
	}

	/**
	 *
	 */
	private void populateActivities(final CrActivitiesModel crActivitiesModel, final CompanyRegistrationInfoData target)
	{
		if (crActivitiesModel == null)
		{
			return;
		}
		final CrActivitiesData activities = new CrActivitiesData();
		activities.setDescription(crActivitiesModel.getLongDescription());
		activities.setIsic(getIsicData(crActivitiesModel.getIsic()));
		target.setActivities(activities);
	}

	/**
	 *
	 */
	private void populateCompany(final CrCompanyModel crCompanyModel, final CompanyRegistrationInfoData target)
	{
		if (crCompanyModel == null)
		{
			return;
		}
		final CrCompanyData company = new CrCompanyData();
		company.setPeriod(crCompanyModel.getPeriod());
		company.setEndDate(crCompanyModel.getEndDate());
		company.setStartDate(crCompanyModel.getStartDate());
		target.setCompany(company);
	}

	/**
	 *
	 */
	private void populateLocation(final CrLocationModel crLocationModel, final CompanyRegistrationInfoData target)
	{
		if (crLocationModel == null)
		{
			return;
		}
		final CrLocationData location = new CrLocationData();
		location.setId(crLocationModel.getId());
		location.setName(crLocationModel.getName());
		target.setLocation(location);
	}

	/**
	 *
	 */
	private void populateCancellation(final CrCancellationModel crCancellationModel, final CompanyRegistrationInfoData target)
	{
		if (crCancellationModel == null)
		{
			return;
		}
		final CrCancellationData cancellation = new CrCancellationData();
		cancellation.setDate(crCancellationModel.getDate());
		cancellation.setReason(crCancellationModel.getReason());
		target.setCancellation(cancellation);
	}


	/**
	 *
	 */
	private void populateStatus(final CrStatusModel crStatusModel, final CompanyRegistrationInfoData target)
	{
		if (crStatusModel == null)
		{
			return;
		}
		final CrStatusData status = new CrStatusData();
		status.setId(crStatusModel.getId());
		status.setName(crStatusModel.getName());
		status.setNameEn(crStatusModel.getNameEn());
		target.setStatus(status);
	}

	/**
	 *
	 */
	private void populateFiscalYear(final CrFiscalYearModel crFiscalYearModel, final CompanyRegistrationInfoData target)
	{
		if (crFiscalYearModel == null)
		{
			return;
		}
		final CrFiscalYearData fiscalYearData = new CrFiscalYearData();
		final CrCalendarTypeData crCalendarTypeData = new CrCalendarTypeData();
		crCalendarTypeData.setId(crFiscalYearModel.getCalendarType().getId());
		crCalendarTypeData.setName(crFiscalYearModel.getCalendarType().getName());
		fiscalYearData.setCalendarType(crCalendarTypeData);
		fiscalYearData.setDay(crFiscalYearModel.getDay());
		fiscalYearData.setMonth(crFiscalYearModel.getMonth());

		target.setFiscalYear(fiscalYearData);
	}

	/**
	 *
	 */
	private void populateBusinessType(final CrBusinessTypeModel source, final CompanyRegistrationInfoData target)
	{
		if (source == null)
		{
			return;
		}
		final CrBusinessTypeData businessTypeData = new CrBusinessTypeData();
		businessTypeData.setId(source.getId());
		businessTypeData.setName(source.getName());
		target.setBusinessType(businessTypeData);
	}

}
