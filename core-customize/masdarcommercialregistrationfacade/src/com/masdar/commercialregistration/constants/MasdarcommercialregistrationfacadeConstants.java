/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.constants;

/**
 * Global class for all Masdarcommercialregistrationfacade constants. You can add global constants for your extension into this class.
 */
public final class MasdarcommercialregistrationfacadeConstants extends GeneratedMasdarcommercialregistrationfacadeConstants
{
	public static final String EXTENSIONNAME = "masdarcommercialregistrationfacade";

	private MasdarcommercialregistrationfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
