/**
 *
 */
package com.masdar.masdarcronjobs.jobs;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;


/**
 * @author mbaker
 *
 */
public class PaymentB2BCancelInvoicesJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(PaymentB2BCancelInvoicesJob.class);

	@Resource(name = "paymentB2BOrderService")
	private PaymentB2BOrderService paymentB2BOrderService;

	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final List<OrderModel> failedOrders = paymentB2BOrderService
				.getOrderByPaymentProviderStatusAndPaymentProviderTransactionType(PaymentProviderStatus.FAILED,
						PaymentProviderTransactionType.FULL_CANCEL);
		if (!CollectionUtils.isEmpty(failedOrders))
		{
			for (final AbstractOrderModel order : failedOrders)
			{
				paymentB2BContext.cancelOrder(order);
			}

		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
