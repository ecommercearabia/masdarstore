/**
 *
 */
package com.masdar.masdarcronjobs.jobs;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;


/**
 * @author mbaker
 *
 */
public class PaymentB2BUpdateStatusJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PaymentB2BUpdateStatusJob.class);


	@Resource(name = "paymentB2BOrderService")
	private PaymentB2BOrderService paymentB2BOrderService;

	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;



	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final Set<OrderModel> orderByStatus = paymentB2BOrderService
				.getOrderByStatusAndPaymentB2BInvoiceCreatedSuccessfully(OrderStatus.PENDING, true).stream()
				.filter(OrderModel.class::isInstance).map(OrderModel.class::cast).collect(Collectors.toSet());

		if (CollectionUtils.isEmpty(orderByStatus))
		{
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}

		for (final AbstractOrderModel order : orderByStatus)
		{
			try
			{
				paymentB2BContext.updatePaymentStatus(order);
			}
			catch (final Exception ex)
			{
				LOG.error(ex.getMessage());
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}


}
