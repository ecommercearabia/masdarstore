/**
 *
 */
package com.masdar.masdarcronjobs.jobs.esal;

import de.hybris.platform.b2b.process.approval.model.B2BApprovalProcessModel;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.Ordermanagement.constants.MasdarOrdermanagementConstants;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;


/**
 * @author mbaker
 *
 */
public class ESALReCreateFailedOrderJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ESALReCreateFailedOrderJob.class);


	@Resource(name = "paymentB2BOrderService")
	private PaymentB2BOrderService paymentB2BOrderService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Resource(name = "timeService")
	private TimeService timeService;


	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final List<AbstractOrderModel> orderByPaymentProviderStatus = paymentB2BOrderService
				.getOrderByStatusAndPaymentB2BInvoiceCreatedSuccessfully(OrderStatus.PENDING, false);

		if (CollectionUtils.isEmpty(orderByPaymentProviderStatus))
		{
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		for (final AbstractOrderModel abstractOrderModel : orderByPaymentProviderStatus)
		{
			if (isExceedDueDate(abstractOrderModel))
			{
				buildCancelRequest(abstractOrderModel);
			}
			else
			{
				reCreateInvoice(abstractOrderModel);
			}
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * @param abstractOrderModel
	 */
	private void reCreateInvoice(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel instanceof OrderModel
				&& CollectionUtils.isNotEmpty(((OrderModel) abstractOrderModel).getOrderProcess()))
		{
			final Optional<OrderProcessModel> process = ((OrderModel) abstractOrderModel).getOrderProcess().stream()
					.filter(p -> !(p instanceof B2BApprovalProcessModel)).findFirst();
			if (process.isPresent())
			{
				final String eventId = process.get().getCode() + "_" + MasdarOrdermanagementConstants.ORDER_ACTION_EVENT_NAME;
				final BusinessProcessEvent event = BusinessProcessEvent.builder(eventId).withChoice("createESALInvoice").build();
				businessProcessService.triggerEvent(event);
			}

		}

	}

	protected boolean isExceedDueDate(final AbstractOrderModel order)
	{
		final Date currentTime = timeService.getCurrentTime();
		final Date creationtime = order.getCreationtime();
		final BaseStoreModel store = order.getStore();
		final int esalInvoiceDueDate = store.getEsalInvoiceDueDate();
		final long diffInMillies = Math.abs(currentTime.getTime() - creationtime.getTime());
		final long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		return esalInvoiceDueDate < diff;
	}

	protected OrderCancelRequest buildCancelRequest(final AbstractOrderModel order)
	{
		if (order != null && order instanceof OrderModel)
		{
			final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();

			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				final OrderCancelEntry orderCancelEntry = new OrderCancelEntry(entry);
				orderCancelEntry.setCancelReason(CancelReason.OTHER);
				orderCancelEntries.add(orderCancelEntry);
			}


			final OrderCancelRequest orderCancelRequest = new OrderCancelRequest((OrderModel) order, orderCancelEntries);
			orderCancelRequest.setCancelReason(CancelReason.OTHER);
			orderCancelRequest.setNotes("");
			return orderCancelRequest;
		}
		return null;
	}

}
