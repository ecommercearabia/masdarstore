/**
 *
 */
package com.masdar.masdarcronjobs.jobs.esal;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.event.PaymentReminderEvent;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;


/**
 * @author mbaker
 *
 */
public class ESALOrderCancelJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ESALOrderCancelJob.class);


	@Resource(name = "paymentB2BOrderService")
	private PaymentB2BOrderService paymentB2BOrderService;

	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "timeService")
	private TimeService timeService;

	@Resource(name = "eventService")
	private EventService eventService;


	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final List<AbstractOrderModel> orderByStatus = paymentB2BOrderService
				.getOrderByStatusAndPaymentB2BInvoiceCreatedSuccessfully(OrderStatus.PENDING, true);
		if (CollectionUtils.isEmpty(orderByStatus))
		{
			LOG.info("No pending orders");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}

		for (final AbstractOrderModel order : orderByStatus)
		{
			LOG.info("Calculate Remaining days for {}", order.getCode());
			final long numberOfRemainingDays = getNumberOfRemainingDays(order);
			LOG.info("Remaining days for {} is {}", order.getCode(), numberOfRemainingDays);
			if (numberOfRemainingDays <= 0)
			{
				LOG.info("Start Cancellation for {}", order.getCode());
				requectOrderCancellation(order);
			}
			else
			{
				LOG.info("Send Reminder Email for {}", order.getCode());
				sendReminderEmail((OrderModel) order);
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 *
	 */
	private void sendReminderEmail(final OrderModel orderModel)
	{
		if (CollectionUtils.isNotEmpty(orderModel.getOrderProcess()))
		{
			final Optional<OrderProcessModel> process = orderModel.getOrderProcess().stream().findFirst();
			if (process.isPresent())
			{
				eventService.publishEvent(new PaymentReminderEvent(process.get()));
			}
		}

	}

	/**
	 * @param order
	 */
	protected void requectOrderCancellation(final AbstractOrderModel order)
	{
		if (!orderCancelService.isCancelPossible((OrderModel) order, userService.getCurrentUser(), false, false).isAllowed())
		{
			LOG.error("Cancel is not Allowed for order {}", order.getCode());
			return;
		}
		final OrderCancelRequest buildCancelRequest = buildCancelRequest(order);
		if (buildCancelRequest != null)
		{
			try
			{
				orderCancelService.requestOrderCancel(buildCancelRequest, userService.getCurrentUser());
				LOG.info("Order Cancelation started for {}", order.getCode());
			}
			catch (final Exception e)
			{
				LOG.error("Exception {} while cnacnellig order {}", e.getMessage(), order.getCode());
			}
		}

	}

	protected OrderCancelRequest buildCancelRequest(final AbstractOrderModel order)
	{
		if (order instanceof OrderModel)
		{
			final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();

			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				final OrderCancelEntry orderCancelEntry = new OrderCancelEntry(entry);
				orderCancelEntry.setCancelReason(CancelReason.OTHER);
				orderCancelEntries.add(orderCancelEntry);
			}


			final OrderCancelRequest orderCancelRequest = new OrderCancelRequest((OrderModel) order, orderCancelEntries);
			orderCancelRequest.setCancelReason(CancelReason.OTHER);
			orderCancelRequest.setNotes("");
			return orderCancelRequest;
		}
		return null;
	}

	protected long getNumberOfRemainingDays(final AbstractOrderModel order)
	{
		final Date currentTime = timeService.getCurrentTime();
		if (Objects.isNull(order.getInvoiceCreationDate()))
		{
			return 0;
		}
		final Date creationtime = order.getInvoiceCreationDate();
		final BaseStoreModel store = order.getStore();
		final int esalInvoiceDueDate = Objects.nonNull(order.getEsalInvoiceDueDatePeriod()) ? order.getEsalInvoiceDueDatePeriod()
				: store.getEsalInvoiceDueDate();
		final long diffInMillies = Math.abs(currentTime.getTime() - creationtime.getTime());
		final long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		return esalInvoiceDueDate - diff;
	}
}
