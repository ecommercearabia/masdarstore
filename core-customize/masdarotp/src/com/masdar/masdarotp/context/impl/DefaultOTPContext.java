/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.core.service.MobilePhoneService;
import com.masdar.masdarotp.context.OTPContext;
import com.masdar.masdarotp.context.OTPProviderContext;
import com.masdar.masdarotp.entity.SmsForm;
import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.exception.OTPException;
import com.masdar.masdarotp.exception.TokenInvalidatedException;
import com.masdar.masdarotp.exception.enums.OTPExceptionType;
import com.masdar.masdarotp.model.OTPProviderModel;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;
import com.masdar.masdarotp.service.OTPVerificationTokenService;
import com.masdar.masdarotp.strategy.OTPStrategy;


/**
 * The Class DefaultOTPContext.
 *
 * @author mnasro
 * @author monzer
 * @author husamd
 */
public class DefaultOTPContext implements OTPContext
{
	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(DefaultOTPContext.class);
	private static final String OTP_STRATEGY_NOT_FOUND = "strategy not found";


	private static final String OTP_PROVIDER_IS_DISABLED = "OTP provider is disabled";
	private static final String CMS_SITE_MODEL_MUSTN_T_BE_NULL = "cmsSiteModel mustn't be null";
	private static final String OTP_PROVIDER_CONFIG_IS_UNAVAILABLE = "OTP provider config is unavailable";
	private static final String CUSTOMER_MODEL_MUSTN_T_BE_NULL = "customerModel mustn't be null";
	private static final String MOBILE_NUMBER_MUSTN_T_BE_NULL = "mobileNumber mustn't be null";


	@Resource(name = "otpProviderContext")
	private OTPProviderContext otpProviderContext;

	@Resource(name = "otpVerificationTokenService")
	private OTPVerificationTokenService otpVerificationTokenService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "otpStrategyMap")
	private Map<Class<?>, OTPStrategy> otpStrategyMap;


	protected Optional<OTPStrategy> getStrategy(final Class<?> providerClass)
	{
		final OTPStrategy strategy = getOtpStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, OTP_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	@Override
	public boolean isEnabledByCurrentSite(final OTPVerificationTokenType type)
	{
		return isEnabled(type, getCmsSiteService().getCurrentSite());
	}

	@Override
	public boolean isEnabled(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (cmsSiteModel == null || type == null)
		{
			return false;
		}
		final String otpProviderType = getOTPProviderType(type, cmsSiteModel);
		try
		{
			final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getProvider(cmsSiteModel, otpProviderType);
			return otpProviderModel.isPresent();
		}
		catch (final NotSupportedException e)
		{
			return false;
		}
	}

	private Optional<OTPProviderModel> getOTPProvider(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (type == null)
		{
			return Optional.empty();
		}

		final String otpProviderType = getOTPProviderType(type, cmsSiteModel);

		if (otpProviderType == null)
		{
			return Optional.empty();
		}

		return getOtpProviderContext().getProvider(cmsSiteModel, otpProviderType);
	}

	/**
	 *
	 */
	private String getOTPProviderType(final OTPVerificationTokenType type, final CMSSiteModel cmsSiteModel)
	{
		if (type == null)
		{
			return null;
		}
		switch (type)
		{
			case LOGIN:
			case CHECKOUT_LOGIN:
			case REGISTRATION:
			case CHECKOUT_REGISTRATION:
			case UPDATE_PROFILE:
				return cmsSiteModel.getRegistrationOTPProvider();
			default:
				return null;

		}

	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCode(final OTPVerificationTokenType type, final String countryisoCode,
			final String mobileNumber, final Object data, final CustomerModel customerModel, final CMSSiteModel cmsSiteModel)
			throws OTPException
	{
		Preconditions.checkArgument(Objects.nonNull(type), "type mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), MOBILE_NUMBER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(Objects.nonNull(data), "data mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(customerModel), CUSTOMER_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(Objects.nonNull(cmsSiteModel), CMS_SITE_MODEL_MUSTN_T_BE_NULL);

		if (!isEnabled(type, cmsSiteModel))
		{
			throw new OTPException(OTPExceptionType.DISABLED, OTP_PROVIDER_IS_DISABLED);
		}

		final Optional<OTPProviderModel> otpProvider = getOTPProvider(type, cmsSiteModel);

		if (otpProvider.isEmpty())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, OTP_PROVIDER_CONFIG_IS_UNAVAILABLE);
		}

		final boolean sendOTPCode = getStrategy(otpProvider.get().getClass()).get().sendOTPCode(countryisoCode, mobileNumber,
				otpProvider.get());
		if (sendOTPCode)
		{
			return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, mobileNumber, customerModel);
		}
		else
		{
			throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "send OTP is failed - service unavailable");
		}
	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSite(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data, final CustomerModel customerModel)
			throws OTPException
	{
		return sendOTPCode(type, countryisoCode, mobileNumber, data, customerModel, getCmsSiteService().getCurrentSite());
	}

	@Override
	public Optional<OTPVerificationTokenModel> sendOTPCodeByCurrentSiteAndCustomer(final OTPVerificationTokenType type,
			final String countryisoCode, final String mobileNumber, final Object data) throws OTPException
	{
		return sendOTPCodeByCurrentSite(type, countryisoCode, mobileNumber, data, getCurrentCustomer());
	}

	@Override
	public boolean verifyCode(final String token, final String countryisoCode, final String mobileNumber, final String code,
			final CustomerModel customerModel, final CMSSiteModel cmsSiteModel) throws OTPException, TokenInvalidatedException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(token), "token mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode mustn't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), MOBILE_NUMBER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "code mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(customerModel), CUSTOMER_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(Objects.nonNull(cmsSiteModel), CMS_SITE_MODEL_MUSTN_T_BE_NULL);

		final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = getOtpVerificationTokenService().getToken(token);

		if (otpVerificationTokenModel.isEmpty())
		{
			throw new TokenInvalidatedException("token[" + token + "] is not found!");
		}

		final OTPVerificationTokenType type = otpVerificationTokenModel.get().getType();
		if (!isEnabled(type, cmsSiteModel))
		{
			throw new OTPException(OTPExceptionType.DISABLED, OTP_PROVIDER_IS_DISABLED);
		}

		final Optional<OTPProviderModel> otpProvider = getOTPProvider(type, cmsSiteModel);

		if (otpProvider.isEmpty())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, OTP_PROVIDER_CONFIG_IS_UNAVAILABLE);
		}

		getOtpVerificationTokenService().verifyToken(token, customerModel);
		return getStrategy(otpProvider.get().getClass()).get().verifyCode(countryisoCode, mobileNumber, code, otpProvider.get());
	}

	@Override
	public boolean verifyCodeByCurrentSite(final String token, final String countryisoCode, final String mobileNumber,
			final String code, final CustomerModel customerModel) throws OTPException, TokenInvalidatedException
	{
		return verifyCode(token, countryisoCode, mobileNumber, code, customerModel, getCmsSiteService().getCurrentSite());
	}

	@Override
	public boolean verifyCodeByCurrentSiteAndCustomer(final String token, final String countryisoCode, final String mobileNumber,
			final String code) throws OTPException, TokenInvalidatedException
	{
		return verifyCodeByCurrentSite(token, countryisoCode, mobileNumber, code, getCurrentCustomer());
	}


	protected CustomerModel getCurrentCustomer()
	{
		final UserModel currentUser = getCurrentUser();

		return (!(currentUser instanceof CustomerModel)) ? null : (CustomerModel) currentUser;
	}

	private UserModel getCurrentUser()
	{
		return getUserService().getCurrentUser();
	}

	protected Map<Class<?>, OTPStrategy> getOtpStrategyMap()
	{
		return otpStrategyMap;
	}

	protected OTPProviderContext getOtpProviderContext()
	{
		return otpProviderContext;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	public OTPVerificationTokenService getOtpVerificationTokenService()
	{
		return otpVerificationTokenService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Override
	public boolean verifyToken(final String token, final CustomerModel customerModel) throws TokenInvalidatedException
	{
		return getOtpVerificationTokenService().verifyToken(token, customerModel);
	}

	@Override
	public boolean verifyTokenByCurrentCustomer(final String token) throws TokenInvalidatedException
	{
		return verifyToken(token, getCurrentCustomer());
	}

	@Override
	public Optional<OTPVerificationTokenModel> getToken(final String token, final CustomerModel customerModel)
			throws TokenInvalidatedException
	{
		return verifyToken(token, customerModel) ? getOtpVerificationTokenService().getToken(token) : Optional.empty();
	}

	@Override
	public Optional<OTPVerificationTokenModel> getTokenByCurrentCustomer(final String token) throws TokenInvalidatedException
	{
		return getToken(token, getCurrentCustomer());
	}

	@Override
	public void removeToken(final String token, final CustomerModel customerModel)
	{
		getOtpVerificationTokenService().removeToken(token);
	}

	@Override
	public void removeTokenByCurrentCustomer(final String token)
	{
		removeToken(token, getCurrentCustomer());

	}

	@Override
	public Optional<OTPVerificationTokenModel> generateToken(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode, final CustomerModel customerModel)
	{
		return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, customerModel);
	}

	@Override
	public Optional<OTPVerificationTokenModel> generateTokenCurrentCustomer(final OTPVerificationTokenType type, final Object data,
			final String countryisoCode)
	{
		return getOtpVerificationTokenService().generateToken(type, data, countryisoCode, getCurrentCustomer());

	}

	@Override
	public boolean verifyCodeByCurrentSiteAndCustomer(final String token, final String code)
			throws OTPException, TokenInvalidatedException
	{
		final Optional<OTPVerificationTokenModel> verificationTokenModel = getOtpVerificationTokenService().getToken(token);
		if (verificationTokenModel.isEmpty())
		{
			throw new TokenInvalidatedException();
		}
		return verifyCodeByCurrentSite(token, verificationTokenModel.get().getMobileCountry(),
				verificationTokenModel.get().getMobileNumber(), code, getCurrentCustomer());
	}


	@Override
	public boolean sendSMSMessageByCurrentSiteAndCustomer(final String message) throws OTPException
	{
		return sendSMSMessage(getCurrentCustomer(), cmsSiteService.getCurrentSite(), message);
	}

	@Override
	public boolean sendSMSMessage(final CustomerModel customerModel, final CMSSiteModel cmsSiteModel, final String message)
			throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(message), "message mustn't be null");
		Preconditions.checkArgument(Objects.nonNull(customerModel), CUSTOMER_MODEL_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(Objects.nonNull(cmsSiteModel), CMS_SITE_MODEL_MUSTN_T_BE_NULL);

		if (!isEnabled(OTPVerificationTokenType.LOGIN, cmsSiteModel))
		{
			throw new OTPException(OTPExceptionType.DISABLED, OTP_PROVIDER_IS_DISABLED);
		}

		final Optional<OTPProviderModel> otpProvider = getOTPProvider(OTPVerificationTokenType.LOGIN, cmsSiteModel);

		if (otpProvider.isEmpty())
		{
			throw new OTPException(OTPExceptionType.OTP_CONFIG_UNAVAILABLE, OTP_PROVIDER_CONFIG_IS_UNAVAILABLE);
		}

		final String mobileNumber = customerModel.getMobileNumber();

		final boolean sendSMSMessage = getStrategy(otpProvider.get().getClass()).get().sendSMSMessage(mobileNumber, message,
				otpProvider.get());
		if (sendSMSMessage)
		{
			return true;
		}

		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, "send OTP is failed - service unavailable");

	}


	private String replace(final String text, final String newString)
	{

		return text + " " + newString;

	}

	private String replace(final String message, final Map<String, String> replacements)
	{
		if (message == null || message.trim().isBlank() || replacements == null || replacements.isEmpty())
		{
			return message;
		}

		final Pattern pattern = Pattern.compile("\\{\\d*\\}");
		final Matcher matcher = pattern.matcher(message);
		final StringBuilder builder = new StringBuilder();
		int i = 0;
		while (matcher.find())
		{
			final String replacement = replacements.get(matcher.group());
			builder.append(message.substring(i, matcher.start()));
			if (replacement == null)
			{
				builder.append(matcher.group(0));
			}
			else
			{
				builder.append(replacement);
			}
			i = matcher.end();
		}
		if (builder.toString().length() - 1 != i)
		{
			builder.append(message.substring(i));
		}
		return builder.toString();
	}

	@Override
	public boolean sendShippingConfirmationSMSMessage(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, "consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "order in the consignmentModel is null");
		Preconditions.checkArgument(consignmentModel.getOrder().getStore() != null, "Store in the consignmentModel is null");
		final BaseStoreModel store = consignmentModel.getOrder().getStore();
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) consignmentModel.getOrder().getSite();
		if (store == null || !store.isEnableConsignmentShippedMessage())
		{
			LOG.info("DefaultOTPContext : Order Shipment SMS Message is disabled");
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{
			String message = StringUtils.isBlank(store.getConsignmentShippedMessage())
					? "Dear {0} Your order {1} is (Delivered/Pickup) and we hope to see again soon."
					: store.getConsignmentShippedMessage();

			final Map<String, String> map = new HashMap<>();
			final CustomerModel customerModel = (CustomerModel) consignmentModel.getOrder().getUser();
			map.put("{0}", customerModel.getDisplayName());
			map.put("{1}", consignmentModel.getOrder().getCode());
			message = replace(message, map);

			final String mobileNumber = getMobileNumber(customerModel, consignmentModel.getOrder());

			consignmentModel.setSmsShippingConfirmationRequest(new SmsForm(mobileNumber, "Masdar", message).toString());
			getStrategy(otpProviderModel.get().getClass()).get().sendSMSMessage(mobileNumber, message, otpProviderModel.get());

			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext::324", e);
			return false;
		}
	}

	protected String getMobileNumber(final CustomerModel customerModel, final AbstractOrderModel abstractOrderModel)
	{
		final String deliveryAddressMobileNumber = abstractOrderModel.getDeliveryAddress() != null
				&& StringUtils.isNotBlank(abstractOrderModel.getDeliveryAddress().getMobile())
						? abstractOrderModel.getDeliveryAddress().getMobile()
						: null;

		if (StringUtils.isNotBlank(deliveryAddressMobileNumber))
		{
			return getNormalizedMobileNumber(abstractOrderModel.getDeliveryAddress().getMobileCountry().getIsocode(),
					abstractOrderModel.getDeliveryAddress().getMobile());
		}
		else
		{
			return getNormalizedMobileNumber(customerModel.getMobileCountry().getIsocode(), customerModel.getMobileNumber());
		}
	}


	protected String getNormalizedMobileNumber(final String countryisocode, final String mobileNumber)
	{

		Optional<String> normalizedPhoneNumber = Optional.empty();

		if (!StringUtils.isEmpty(countryisocode) && !StringUtils.isEmpty(mobileNumber))
		{
			normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(countryisocode, mobileNumber);
		}
		return normalizedPhoneNumber.isPresent() ? normalizedPhoneNumber.get() : mobileNumber;
	}

	@Override
	public boolean sendOrderCompletedSMSMessage(final OrderModel orderModel)
	{
		Preconditions.checkArgument(orderModel != null, "OrderModel is null");
		Preconditions.checkArgument(orderModel.getSite() != null, "Site is null");

		final BaseStoreModel store = orderModel.getStore();
		final CMSSiteModel cmsSiteModel = (CMSSiteModel) orderModel.getSite();
		if (store == null || !store.isEnableOrderCompletedMessage())
		{
			LOG.info("[{}] : Order Shipment SMS Message is disabled", this.getClass().getSimpleName());
			return false;
		}
		final Optional<OTPProviderModel> otpProviderModel = getOtpProviderContext().getSendSMSProvider(cmsSiteModel);

		Preconditions.checkArgument(otpProviderModel.isPresent(), "otpProviderModel is null");
		try
		{
			String message = StringUtils.isBlank(store.getOrderCompletedMessage())
					? "Dear {0} Your order {1} is (Delivered/Pickup) and we hope to see again soon."
					: store.getOrderCompletedMessage();

			final Map<String, String> map = new HashMap<>();
			final CustomerModel customerModel = (CustomerModel) orderModel.getUser();
			map.put("{0}", customerModel.getDisplayName());
			map.put("{1}", orderModel.getCode());
			message = replace(message, map);

			final String mobileNumber = getMobileNumber(customerModel, orderModel);

			getStrategy(otpProviderModel.get().getClass()).get().sendSMSMessage(mobileNumber, message, otpProviderModel.get());

			return true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred sending SMS DefaultOTPContext::324", e);
			return false;
		}
	}

}
