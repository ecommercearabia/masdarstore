/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.constants;

/**
 * Global class for all Masdarotp constants. You can add global constants for your extension into this class.
 */
public final class MasdarotpConstants extends GeneratedMasdarotpConstants
{
	public static final String EXTENSIONNAME = "masdarotp";

	private MasdarotpConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarotpPlatformLogo";
}
