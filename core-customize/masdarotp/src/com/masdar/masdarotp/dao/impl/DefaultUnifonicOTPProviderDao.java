/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.dao.impl;

import com.masdar.masdarotp.dao.OTPProviderDao;
import com.masdar.masdarotp.model.UnifonicOTPProviderModel;


/**
 * @author Husam Dababneh
 *
 *         The Class DefaultUnifonicOTPProviderDao.
 */
public class DefaultUnifonicOTPProviderDao extends DefaultOTPProviderDao implements OTPProviderDao
{

	/**
	 * Instantiates a new default Unifonic OTP provider dao.
	 */
	public DefaultUnifonicOTPProviderDao()
	{
		super(UnifonicOTPProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return UnifonicOTPProviderModel._TYPECODE;
	}

}
