/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.dao.impl;

import com.masdar.masdarotp.dao.OTPProviderDao;
import com.masdar.masdarotp.model.EtisalatOTPProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultEtisalatOTPProviderDao.
 */
public class DefaultEtisalatOTPProviderDao extends DefaultOTPProviderDao implements OTPProviderDao
{

	/**
	 * Instantiates a new default Etisalat OTP provider dao.
	 */
	public DefaultEtisalatOTPProviderDao()
	{
		super(EtisalatOTPProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EtisalatOTPProviderModel._TYPECODE;
	}

}
