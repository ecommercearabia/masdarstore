/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.service;

import com.masdar.masdarotp.exception.TokenInvalidatedException;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;


/**
 *
 */
public interface OTPVerificationTokenService
{

	public Optional<OTPVerificationTokenModel> generateToken(OTPVerificationTokenType type, Object data,
			final String countryisoCode, final String mobileNumber,
			final CustomerModel customerModel);

	public Optional<OTPVerificationTokenModel> generateToken(OTPVerificationTokenType type, Object data,
			final String countryisoCode, final CustomerModel customerModel);

	public Optional<OTPVerificationTokenModel> getToken(String token);

	public boolean verifyToken(String token,
			final CustomerModel customerModel) throws TokenInvalidatedException;

	public void removeToken(String token);

	public void removeToken(String token, CustomerModel customerModel);


}
