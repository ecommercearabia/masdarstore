package com.masdar.masdarotp.unifonic.service;

import com.masdar.masdarotp.unifonic.exception.UnifonicException;


public interface UnifonicService
{
	boolean sendVerificationCode(final String baseUrl, final String appSid, final String auth, final String to,
			final String channel, final String locale, final String length) throws UnifonicException;

	boolean verifyCode(final String baseUrl, final String appSid, final String auth, final String to, final String channel,
			final String code) throws UnifonicException;


	boolean sendSMSMessage(final String baseUrl, final String appSid, final String senderId, final String auth, final String to,
			final String message) throws UnifonicException;
}
