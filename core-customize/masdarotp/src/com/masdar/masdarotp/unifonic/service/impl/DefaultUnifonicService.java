package com.masdar.masdarotp.unifonic.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masdar.masdarotp.unifonic.beans.UnifonicResponse;
import com.masdar.masdarotp.unifonic.exception.UnifonicException;
import com.masdar.masdarotp.unifonic.exception.type.UnifonicErrorCodeType;
import com.masdar.masdarotp.unifonic.exception.type.UnifonicExceptionType;
import com.masdar.masdarotp.unifonic.service.UnifonicService;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;



public class DefaultUnifonicService implements UnifonicService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultUnifonicService.class);
	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	private static final String API_UNIFONIC_END_POINT_START = "start";
	private static final String API_UNIFONIC_END_POINT_CHECK = "check";
	private static final String X_AUTHENTICATE_APP_ID_KEY = "x-authenticate-app-id";
	private static final String X_AUTHENTICATE_APP_ID_VALUE = "AP506599ef5751427c97790c52f322310a";
	private static final String AUTHORIZATION_KEY = "Authorization";
	private static final String ACCEPT_KEY = "Accept";
	private static final String ACCEPT_VALUE = "application/json";
	private static final String CHANNEL_KEY = "channel";
	private static final String CONTENT_TYPE_KEY = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded";
	private static final String CODE_KEY = "code";
	private static final String TO_KEY = "to";
	private static final String LOCALE_KEY = "locale";
	private static final String LENGTH_KEY = "length";

	private static final String SMS_PUBLIC_ID_KEY = "PublicId";
	private static final String SMS_SECRET_KEY = "Secret";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.otpservices.service.VerificationService#sendVerificationCode(java. lang.String, java.lang.String)
	 */
	@Override
	public boolean sendVerificationCode(final String baseUrl, final String appSid, final String auth, final String to,
			final String channel, final String locale, final String length) throws UnifonicException
	{
		final String URL = baseUrl + API_UNIFONIC_END_POINT_START;

		final Map<String, String> header = new HashMap<>(4);
		header.put(X_AUTHENTICATE_APP_ID_KEY, appSid);
		header.put(AUTHORIZATION_KEY, auth);
		header.put(ACCEPT_KEY, ACCEPT_VALUE);
		header.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);

		final String mobileNumber = !to.startsWith("00") ? "00" + to : to;

		final Map<String, Object> fields = new HashMap<>(4);
		fields.put(TO_KEY, mobileNumber);
		fields.put(CHANNEL_KEY, channel);
		fields.put(LOCALE_KEY, locale);
		fields.put(LENGTH_KEY, length);


		HttpResponse<String> response = null;

		try
		{
			LOG.info("Sending OTP Code to {}", mobileNumber);
			response = Unirest.post(URL).headers(header).fields(fields).asString();
			final UnifonicResponse responseObject = GSON.fromJson(response.getBody(), UnifonicResponse.class);


			if (!response.isSuccess() || responseObject == null || responseObject.getError() != null)
			{
				throw new UnifonicException(response.getStatusText(), UnifonicExceptionType.UNKNOWN_ERROR_CODE, response.getBody());
			}

			return true;
		}
		catch (final UnirestException e)
		{
			throw new UnifonicException(e.getMessage(), UnifonicExceptionType.INTERNAL_ERROR, "");
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.otpservices.service.VerificationService#verifyCode(java.lang. String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean verifyCode(final String baseUrl, final String appSid, final String auth, final String to, final String channel,
			final String code) throws UnifonicException
	{
		final String URL = baseUrl + API_UNIFONIC_END_POINT_CHECK;
		final Map<String, String> header = new HashMap<>(4);
		header.put(X_AUTHENTICATE_APP_ID_KEY, X_AUTHENTICATE_APP_ID_VALUE);
		header.put(AUTHORIZATION_KEY, auth);
		header.put(ACCEPT_KEY, ACCEPT_VALUE);
		header.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);

		final String mobileNumber = !to.startsWith("00") ? "00" + to : to;

		final Map<String, Object> fields = new HashMap<>(3);
		fields.put(TO_KEY, mobileNumber);
		fields.put(CHANNEL_KEY, channel);
		fields.put(CODE_KEY, code);

		HttpResponse<String> response = null;

		try
		{
			LOG.info("Checking OTP Code for {}", mobileNumber);
			response = Unirest.post(URL).headers(header).fields(fields).asString();

			final UnifonicResponse responseObject = GSON.fromJson(response.getBody(), UnifonicResponse.class);
			if (!response.isSuccess() || responseObject == null || responseObject.getError() != null)
			{
				throw new UnifonicException(response.getStatusText(), UnifonicExceptionType.UNKNOWN_ERROR_CODE, response.getBody());
			}

			if (UnifonicErrorCodeType.CORRECT.getCode() != responseObject.getErrorCode())
			{
				throw new UnifonicException(responseObject.getResponseStatus(), UnifonicExceptionType.INTERNAL_ERROR,
						responseObject.getResponseStatus());
			}
			return true;

		}
		catch (final UnirestException e)
		{
			throw new UnifonicException(e.getMessage(), UnifonicExceptionType.INTERNAL_ERROR, "");
		}

	}



	@Override
	public boolean sendSMSMessage(final String baseUrl, final String appSid, final String senderId, final String auth,
			final String to, final String message) throws UnifonicException
	{
		final Map<String, Object> params = new HashMap<>(9);
		params.put("AppSid", appSid); //"WnUh7F4zlEZPBI7SoHHfAC7nFYqu8W");
		params.put("SenderID", senderId); //"Masdar");
		params.put("Body", message);
		params.put("Recipient", to);
		params.put("responseType", "JSON");
		params.put("CorrelationID", "CorrelationID");
		params.put("baseEncode", "true");
		params.put("statusCallback", "sent");
		params.put("async", "true");

		final Map<String, String> headers = new HashMap<>(2);
		headers.put(ACCEPT_KEY, ACCEPT_VALUE);
		headers.put(AUTHORIZATION_KEY, auth);

		final String url = baseUrl
				+ "?AppSid={AppSid}&SenderID={SenderID}&Body={Body}&Recipient={Recipient}&responseType={responseType}&CorrelationID={CorrelationID}&baseEncode={baseEncode}&statusCallback={statusCallback}&async={async}";

		final HttpResponse<String> response = Unirest.post(url).routeParam(params).headers(headers).body("").asString();

		if (!response.isSuccess())
		{
			throw new UnifonicException("Request: " + "", UnifonicExceptionType.UNIFONIC_ERROR, response.getBody());
		}


		//		GSON.fromJson(response.getBody(), UnifonicSMSResponse.class);

		return true;
	}

}
