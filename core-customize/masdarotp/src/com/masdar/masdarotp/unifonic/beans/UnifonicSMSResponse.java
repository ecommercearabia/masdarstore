package com.masdar.masdarotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UnifonicSMSResponse
{
	@Expose
	@SerializedName("success")
	private boolean success;
	@Expose
	@SerializedName("message")
	private String message;
	@Expose
	@SerializedName("errorCode")
	private String errorCode;
	@Expose
	@SerializedName("MessageID")
	private String messageID;
	@Expose
	@SerializedName("CorrelationID")
	private String correlationID;

	protected boolean isSuccess()
	{
		return success;
	}

	protected void setSuccess(final boolean success)
	{
		this.success = success;
	}

	protected String getMessage()
	{
		return message;
	}

	protected void setMessage(final String message)
	{
		this.message = message;
	}

	protected String getErrorCode()
	{
		return errorCode;
	}

	protected void setErrorCode(final String errorCode)
	{
		this.errorCode = errorCode;
	}

	protected String getMessageID()
	{
		return messageID;
	}

	protected void setMessageID(final String messageID)
	{
		this.messageID = messageID;
	}

	protected String getCorrelationID()
	{
		return correlationID;
	}

	protected void setCorrelationID(final String correlationID)
	{
		this.correlationID = correlationID;
	}



}
