package com.masdar.masdarotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UnifonicResponse
{
	@Expose
	@SerializedName("to")
	private String to;
	@Expose
	@SerializedName("length")
	private int length;
	@Expose
	@SerializedName("channel")
	private String channel;
	@Expose
	@SerializedName("locale")
	private String locale;
	@Expose
	@SerializedName("code_type")
	private String codeType;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("response_status")
	private String responseStatus;
	@Expose
	@SerializedName("error_code")
	private int errorCode;
	@Expose
	@SerializedName("error")
	private String error;

	/**
	 * @return the to
	 */
	public String getTo()
	{
		return to;
	}

	/**
	 * @param to
	 *           the to to set
	 */
	public void setTo(final String to)
	{
		this.to = to;
	}

	/**
	 * @return the length
	 */
	public int getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *           the length to set
	 */
	public void setLength(final int length)
	{
		this.length = length;
	}

	/**
	 * @return the channel
	 */
	public String getChannel()
	{
		return channel;
	}

	/**
	 * @param channel
	 *           the channel to set
	 */
	public void setChannel(final String channel)
	{
		this.channel = channel;
	}

	/**
	 * @return the locale
	 */
	public String getLocale()
	{
		return locale;
	}

	/**
	 * @param locale
	 *           the locale to set
	 */
	public void setLocale(final String locale)
	{
		this.locale = locale;
	}

	/**
	 * @return the codeType
	 */
	public String getCodeType()
	{
		return codeType;
	}

	/**
	 * @param codeType
	 *           the codeType to set
	 */
	public void setCodeType(final String codeType)
	{
		this.codeType = codeType;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * @return the responseStatus
	 */
	public String getResponseStatus()
	{
		return responseStatus;
	}

	/**
	 * @param responseStatus
	 *           the responseStatus to set
	 */
	public void setResponseStatus(final String responseStatus)
	{
		this.responseStatus = responseStatus;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * @param errorCode
	 *           the errorCode to set
	 */
	public void setErrorCode(final int errorCode)
	{
		this.errorCode = errorCode;
	}

	/**
	 * @return the error
	 */
	public String getError()
	{
		return error;
	}

	/**
	 * @param error
	 *           the error to set
	 */
	public void setError(final String error)
	{
		this.error = error;
	}

}
