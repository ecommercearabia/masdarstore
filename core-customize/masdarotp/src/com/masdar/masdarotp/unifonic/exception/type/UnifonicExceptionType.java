/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.unifonic.exception.type;

import java.util.Arrays;


/**
 *
 */
public enum UnifonicExceptionType
{
	UNIFONIC_ERROR("002", "Unifonic Error"), INTERNAL_ERROR("1000", "Internal Error"), UNKNOWN_ERROR_CODE("1001",
			"Uknown Error Code");

	private final String code;
	private final String msg;

	/**
	 *
	 */
	private UnifonicExceptionType(final String code, final String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	public static UnifonicExceptionType getExceptionTypeFromCode(final String code)
	{
		return Arrays.asList(UnifonicExceptionType.values()).stream().filter(e -> e.getCode().equals(code)).findFirst()
				.orElse(UNKNOWN_ERROR_CODE);
	}


}
