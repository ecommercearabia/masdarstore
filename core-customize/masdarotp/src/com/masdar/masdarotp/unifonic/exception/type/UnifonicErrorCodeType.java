/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarotp.unifonic.exception.type;

/**
 *
 */
public enum UnifonicErrorCodeType
{
	CORRECT(101, "correct"), INCORRECT(107, "incorrect"), ATTEMPTS_EXCEEDED(108, "attempts_exceeded"), CODE_EXPIRED(109,
			"code_expired"), ALREADY_VERIFIED(110, "already_verified");

	private final int code;
	private final String msg;


	/**
	 *
	 */
	private UnifonicErrorCodeType(final int code, final String msg)
	{
		this.code = code;
		this.msg = msg;
	}


	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}


	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}


}
