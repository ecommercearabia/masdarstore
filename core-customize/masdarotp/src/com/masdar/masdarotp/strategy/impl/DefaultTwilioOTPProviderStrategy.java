package com.masdar.masdarotp.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.masdarotp.model.OTPProviderModel;
import com.masdar.masdarotp.model.TwilioOTPProviderModel;
import com.masdar.masdarotp.service.OTPProviderService;
import com.masdar.masdarotp.strategy.OTPProviderStrategy;


/**
 * @author mnasro
 *
 *         The Class DefaultTwilioOTPProviderStrategy.
 */
public class DefaultTwilioOTPProviderStrategy implements OTPProviderStrategy
{

	/** The otp provider service. */
	@Resource(name = "otpProviderService")
	private OTPProviderService otpProviderService;

	/**
	 * Gets the OTP provider service.
	 *
	 * @return the OTP provider service
	 */
	protected OTPProviderService getOTPProviderService()
	{
		return otpProviderService;
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getOTPProviderService().getActive(cmsSiteUid, TwilioOTPProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getOTPProviderService().getActive(cmsSiteModel, TwilioOTPProviderModel.class);
	}

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite()
	{
		return getOTPProviderService().getActiveProviderByCurrentSite(TwilioOTPProviderModel.class);
	}
}

