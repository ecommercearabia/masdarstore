package com.masdar.masdarotp.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.masdarotp.model.OTPProviderModel;
import com.masdar.masdarotp.model.UnifonicOTPProviderModel;
import com.masdar.masdarotp.service.OTPProviderService;
import com.masdar.masdarotp.strategy.OTPProviderStrategy;


/**
 * @author Husam Dababneh
 *
 *         The Class DefaultTwilioOTPProviderStrategy.
 */
public class DefaultUnifonicOTPProviderStrategy implements OTPProviderStrategy
{

	/** The otp provider service. */
	@Resource(name = "otpProviderService")
	private OTPProviderService otpProviderService;

	/**
	 * Gets the OTP provider service.
	 *
	 * @return the OTP provider service
	 */
	protected OTPProviderService getOTPProviderService()
	{
		return otpProviderService;
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getOTPProviderService().getActive(cmsSiteUid, UnifonicOTPProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getOTPProviderService().getActive(cmsSiteModel, UnifonicOTPProviderModel.class);
	}

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite()
	{
		return getOTPProviderService().getActiveProviderByCurrentSite(UnifonicOTPProviderModel.class);
	}
}

