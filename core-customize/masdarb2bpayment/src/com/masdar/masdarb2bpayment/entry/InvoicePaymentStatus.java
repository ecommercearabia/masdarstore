/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.entry;



/**
 *
 */
public class InvoicePaymentStatus
{

	private String invoiceNumber;


	private String sadadBillsID;


	private String billerId;


	private double totalPaidAmount;


	private double outstandingAmount;


	private String paymentStatus;


	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}


	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}


	/**
	 * @return the sadadBillsID
	 */
	public String getSadadBillsID()
	{
		return sadadBillsID;
	}


	/**
	 * @param sadadBillsID the sadadBillsID to set
	 */
	public void setSadadBillsID(final String sadadBillsID)
	{
		this.sadadBillsID = sadadBillsID;
	}


	/**
	 * @return the billerId
	 */
	public String getBillerId()
	{
		return billerId;
	}


	/**
	 * @param billerId the billerId to set
	 */
	public void setBillerId(final String billerId)
	{
		this.billerId = billerId;
	}


	/**
	 * @return the totalPaidAmount
	 */
	public double getTotalPaidAmount()
	{
		return totalPaidAmount;
	}


	/**
	 * @param totalPaidAmount the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(final double totalPaidAmount)
	{
		this.totalPaidAmount = totalPaidAmount;
	}


	/**
	 * @return the outstandingAmount
	 */
	public double getOutstandingAmount()
	{
		return outstandingAmount;
	}


	/**
	 * @param outstandingAmount the outstandingAmount to set
	 */
	public void setOutstandingAmount(final double outstandingAmount)
	{
		this.outstandingAmount = outstandingAmount;
	}


	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus()
	{
		return paymentStatus;
	}


	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(final String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}



}
