/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.entry;

/**
 *
 */
public class InvoiceCancelResult
{
	private String result;

	private Object errors;

	public Object getErrors()
	{
		return errors;
	}

	public void setErrors(final Object errors)
	{
		this.errors = errors;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}
}
