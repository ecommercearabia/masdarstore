/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.masdarb2bpayment.esal.beans.LineItem;


/**
 *
 */
public class EsalLineItemPopulator implements Populator<AbstractOrderEntryModel, LineItem>
{

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	private static final double EPSILON = 0.01d;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}



	@Override
	public void populate(final AbstractOrderEntryModel source, final LineItem target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");


		if (!Objects.isNull(source.getProduct()))
		{
			target.setCode(source.getProduct().getCode());
			target.setDescriptionArabic(source.getProduct().getDescription());
			target.setDescriptionEnglish(source.getProduct().getDescription());
			target.setUomArabic(source.getProduct().getUnit().getCode());
			target.setUomEnglish(source.getProduct().getUnit().getCode());
		}

		double basePrice = 0;
		double totalPrice = 0;
		double totalPriceWithTax = 0;
		double taxValue = 0.0;
		double taxAppliedValue = 0.0;
		double netDiscount = 0.0;

		if (source.getBasePrice() != null)
		{
			basePrice = createPrice(source, source.getBasePrice()).getValue().doubleValue();
		}
		if (source.getTotalPrice() != null)
		{
			totalPrice = createPrice(source, source.getTotalPrice()).getValue().doubleValue();
		}

		if (!CollectionUtils.isEmpty(source.getTaxValues()))
		{
			for (final TaxValue value : source.getTaxValues())
			{
				taxValue += value.getValue();
				taxAppliedValue += value.getAppliedValue();
			}
		}
		if (!CollectionUtils.isEmpty(source.getDiscountValues()))
		{
			for (final DiscountValue value : source.getDiscountValues())
			{
				netDiscount += value.getAppliedValue();
			}
		}
		basePrice -= netDiscount / source.getQuantity();
		netDiscount = 0;

		final double amount = basePrice * source.getQuantity();
		totalPriceWithTax = source.getTotalPrice() + taxAppliedValue;
		target.setPrice(formatBasePriceDouble(basePrice));
		target.setForeignPrice(formatBasePriceDouble(basePrice));
		target.setAmount(formatDouble(amount));
		target.setDiscountAmount(netDiscount);
		target.setPriceAfterVat(formatDouble(totalPriceWithTax));
		target.setAmountAfterDiscount(formatDouble(totalPrice));
		target.setQuantity(source.getQuantity());
		target.setVatPercent(Math.ceil(taxValue));
		target.setTotalVat(formatDouble(taxAppliedValue));
		if (amount != 0)
		{
			target.setDiscountPercent(Math.ceil((netDiscount / amount) * 100));
		}
		else
		{
			target.setDiscountPercent(0.0);
		}

	}


	protected PriceData createPrice(final AbstractOrderEntryModel orderEntry, final Double val)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
				orderEntry.getOrder().getCurrency());
	}

	protected Double getBasePrice(final AbstractOrderEntryModel source)
	{
		if (source != null && source.getProduct() != null)
		{
			final PriceDataType priceType;
			final PriceInformation info;
			if (CollectionUtils.isEmpty(source.getProduct().getVariants()))
			{
				priceType = PriceDataType.BUY;
				info = getCustomCommercePriceService().getWebPriceForProductWithTax(source.getProduct(), true);
			}
			else
			{
				priceType = PriceDataType.FROM;
				info = getCustomCommercePriceService().getFromPriceForProduct(source.getProduct());
			}

			if (info != null && info.getPriceValue() != null)
			{
				return info.getPriceValue().getValue();
			}
		}
		return source.getBasePrice();
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.##");
		final String format = df.format(val);
		return Double.valueOf(format);
	}


	protected double formatBasePriceDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.####");
		final String format = df.format(val);
		return Double.valueOf(format);
	}






}
