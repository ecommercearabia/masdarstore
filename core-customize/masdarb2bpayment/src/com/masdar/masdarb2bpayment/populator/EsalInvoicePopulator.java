/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.populator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.LineItem;


/**
 *
 */
public class EsalInvoicePopulator implements Populator<AbstractOrderModel, Invoice>
{

	@Resource(name = "esalLineItemConverter")
	private Converter<AbstractOrderEntryModel, LineItem> esalLineItemConverter;

	@Resource(name = "cartConverter")
	private Converter<CartModel, CartData> cartConverter;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Resource(name = "quoteConverter")
	private Converter<QuoteModel, QuoteData> quoteConverter;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;


	private static final Logger LOG = LoggerFactory.getLogger(EsalInvoicePopulator.class);
	protected static final String DATE_FORMAT = "yyyy-MM-dd";

	@Override
	public void populate(final AbstractOrderModel source, final Invoice target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setInvoiceNumber(source.getCode());
		target.setInvoiceIssueDate(getFormattedDate(new Date()));
		final Date dueDate = getDueDate(source);
		target.setDueDate(getFormattedDate(dueDate));
		target.setDateOfDelivery(null);
		if (!Objects.isNull(source.getCurrency()))
		{
			target.setCurrency(source.getCurrency().getIsocode());
			target.setForeignCurrency(source.getCurrency().getIsocode());
		}

		final double totalPrice = createPrice(source, source.getTotalPrice()).getValue().doubleValue();
		target.setInvoiceReference(null);
		target.setRemarksArabic(null);
		target.setRemarksEnglish(null);

		target.setNarrationArabic(null);
		target.setNarrationEnglish(null);

		target.setShipmentRefNumber(null);
		target.setSalespersonEnglish(null);
		target.setSalespersonArabic(null);
		target.setSelfAccountForTax(false);

		target.setTotalBeforeVAT(totalPrice);
		target.setTotalVAT(source.getTotalTax());
		target.setGrandTotal(totalPrice + source.getTotalTax());
		target.setAdvanceAmount(null);
		target.setOutstandingAmount(null);

		target.setPercentOfCompletion(null);
		target.setMilestonePayments(null);

		populateEsalLineItems(source, target);
		addDeliveryFees(source, target.getLineItems());
	}

	/**
	 *
	 */
	private void addDeliveryFees(final AbstractOrderModel source, final List<LineItem> items)
	{
		if (source.getDeliveryCost() == 0)
		{
			return;
		}
		final Double deliveryCost = source.getDeliveryCost();
		final double totalPrice = source.getTotalPrice();
		final double basePrices = getBasePricesWithDiscount(source);
		final double percentage = ((totalPrice + source.getTotalTax()) / (basePrices + source.getDeliveryCost())) - 1.0;

		final LineItem lineItem = new LineItem();
		lineItem.setAmount(deliveryCost);
		lineItem.setPrice(deliveryCost);
		lineItem.setForeignPrice(deliveryCost);
		lineItem.setCode("Delivery_Fees");
		lineItem.setQuantity(1l);
		lineItem.setVatPercent(Math.ceil(formatDouble(percentage * 100)));
		lineItem.setTotalVat(formatDouble(deliveryCost * percentage));
		lineItem.setDiscountAmount(0d);
		lineItem.setDiscountPercent(0d);
		lineItem.setPriceAfterVat(formatDouble(createPrice(source, deliveryCost * (1 + percentage)).getValue().doubleValue()));
		lineItem.setAmountAfterDiscount(createPrice(source, deliveryCost).getValue().doubleValue());
		lineItem.setDescriptionArabic("Delivery fees");
		lineItem.setDescriptionEnglish("Delivery fees");
		lineItem.setUomArabic("Q");
		lineItem.setUomEnglish("Q");
		items.add(lineItem);

	}

	/**
	 *
	 */
	private double getBasePricesWithDiscount(final AbstractOrderModel source)
	{
		double basePrice = 0;
		if (!CollectionUtils.isEmpty(source.getEntries()))
		{
			for (final var entry : source.getEntries())
			{
				double discount = 0;
				for (final DiscountValue discountValue : entry.getDiscountValues())
				{
					discount += discountValue.getAppliedValue();
				}

				basePrice += (entry.getBasePrice() * entry.getQuantity()) - discount;


			}

		}
		return basePrice;
	}

	/**
	 *
	 */
	private void populateEsalLineItems(final AbstractOrderModel source, final Invoice target)
	{

		if (!CollectionUtils.isEmpty(source.getEntries()))
		{
			target.setLineItems(esalLineItemConverter.convertAll(source.getEntries()));
		}



	}



	protected String getFormattedDate(final Date date)
	{
		if (Objects.isNull(date))
		{
			return null;
		}
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);

		return simpleDateFormat.format(date);


	}

	protected PriceData createPrice(final AbstractOrderModel order, final Double val)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()), order.getCurrency());
	}

	protected double formatDouble(final double value)
	{
		final DecimalFormat df = new DecimalFormat("#.#");
		return Double.valueOf(df.format(value));
	}

	protected Date getDueDate(final AbstractOrderModel order)
	{
		final Date creationtime = new Date();

		final BaseStoreModel store = order.getStore();
		final int esalInvoiceDueDate = store.getEsalInvoiceDueDate();
		final Calendar c = Calendar.getInstance();
		c.setTime(creationtime);
		c.add(Calendar.DATE, esalInvoiceDueDate);
		return c.getTime();
	}
}
