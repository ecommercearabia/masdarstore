/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.masdarb2bpayment.esal.beans.Customer;
import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.core.service.MobilePhoneService;


/**
 *
 */
public class EsalCustomerPopulator implements Populator<AbstractOrderModel, Invoice>
{

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Override
	public void populate(final AbstractOrderModel source, final Invoice target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");


		if (Objects.isNull(source.getUser()) || !(source.getUser() instanceof CustomerModel))
		{
			return;
		}
		final Customer customer = getEsalCustomer((CustomerModel) source.getUser(), source);

		target.setCustomer(customer);
	}


	protected Customer getEsalCustomer(final CustomerModel customer, final AbstractOrderModel source)
	{
		final Customer esalCustomer = new Customer();
		final String pk = customer.getPk().getLongValueAsString();
		esalCustomer.setCode(pk);
		esalCustomer.setNameArabic(customer.getName());
		esalCustomer.setVatRegisterationNumber(null);
		esalCustomer.setEmail(customer.getContactEmail());
		String mobileNumber = "+966";
		if (!Objects.isNull(customer.getMobileCountry()) && !Objects.isNull(customer.getMobileCountry().getIsocode()))
		{
			final String mobileCountry = customer.getMobileCountry().getIsocode();
			final Optional<String> number = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					customer.getMobileNumber());
			mobileNumber = number.isEmpty() ? mobileNumber : "+".concat(number.get());
		}
		esalCustomer.setContactNo(mobileNumber);
		final AddressModel deliveryAddress = source.getDeliveryAddress();
		if (!Objects.isNull(deliveryAddress))
		{
			esalCustomer.setAddressArabic1(deliveryAddress.getLine1());
			esalCustomer.setAddressArabic2(deliveryAddress.getLine2());
			esalCustomer.setAddressEnglish1(deliveryAddress.getLine1());
			esalCustomer.setAddressEnglish2(deliveryAddress.getLine2());
			if (!Objects.isNull(deliveryAddress.getCity()))
			{
				esalCustomer.setCityEnglish(deliveryAddress.getCity().getName());
				esalCustomer.setCityArabic(deliveryAddress.getCity().getName());
			}

			esalCustomer.setProdLineEnglish(null);
			esalCustomer.setProdLineArabic(null);
		}

		return esalCustomer;
	}

}
