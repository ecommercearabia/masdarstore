/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.ProfitMargin;




/**
 *
 */
public class EsalProfitMarginPopulator implements Populator<AbstractOrderModel, Invoice>
{

	@Override
	public void populate(final AbstractOrderModel source, final Invoice target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final ProfitMargin profitMargin = getEsalProfit(source);
		target.setProfitMargin(profitMargin);

	}

	/**
	 *
	 */
	protected ProfitMargin getEsalProfit(final AbstractOrderModel source)
	{
		final ProfitMargin profitMargin = new ProfitMargin();
		profitMargin.setApplied(false);
		profitMargin.setProcedureArabic(null);
		profitMargin.setProcedureEnglish(null);
		return profitMargin;
	}

}
