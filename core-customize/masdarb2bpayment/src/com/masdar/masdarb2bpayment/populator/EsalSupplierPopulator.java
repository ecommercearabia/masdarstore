/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.Supplier;




/**
 *
 */
public class EsalSupplierPopulator implements Populator<AbstractOrderModel, Invoice>
{

	@Override
	public void populate(final AbstractOrderModel source, final Invoice target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		final Supplier supplier = getEsalSupplier(source);
		target.setSupplier(supplier);

	}

	/**
	 *
	 */
	protected Supplier getEsalSupplier(final AbstractOrderModel source)
	{
		final Supplier supplier = new Supplier();
		supplier.setBankSwiftCode(null);
		supplier.setBranchArabic(null);
		supplier.setBranchEnglish(null);
		supplier.setCityArabic(null);
		supplier.setCityEnglish(null);
		supplier.setIban(null);
		supplier.setId(null);
		supplier.setLocalityArabic(null);
		supplier.setLocalityEnglish(null);
		supplier.setPlantArabic(null);
		supplier.setPlantEnglish(null);
		supplier.setRegionArabic(null);
		supplier.setRegionEnglish(null);
		return supplier;
	}


}
