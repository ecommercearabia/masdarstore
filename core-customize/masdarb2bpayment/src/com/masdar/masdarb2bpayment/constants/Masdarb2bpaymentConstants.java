package com.masdar.masdarb2bpayment.constants;

/**
 * The Class Masdarb2bpaymentConstants.
 */
@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class Masdarb2bpaymentConstants extends GeneratedMasdarb2bpaymentConstants
{
	
	/** The Constant EXTENSIONNAME. */
	public static final String EXTENSIONNAME = "masdarb2bpayment";
	
	/**
	 * Instantiates a new masdarb 2 bpayment constants.
	 */
	private Masdarb2bpaymentConstants()
	{
		//empty
	}
	
	
}
