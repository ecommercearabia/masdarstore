/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Interface PaymentB2BProviderService.
 */
public interface PaymentB2BProviderService
{

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @param providerClass the provider class
	 * @return the optional
	 */
	public Optional<PaymentB2BProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	public Optional<PaymentB2BProviderModel> getActiveProvider(String baseStoreUid, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	public Optional<PaymentB2BProviderModel> getActiveProvider(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass the provider class
	 * @return the active provider by current base store
	 */
	public Optional<PaymentB2BProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass);

}
