/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.masdar.core.enums.WebServiceRequestStatus;

/**
 *
 */
public interface PaymentB2BWebServiceRecordService
{
	public void addWebhookRecord(Object requestBody, WebServiceRequestStatus requestStatus, String failureReason,
			AbstractOrderModel order);

	public void addApiRecord(Object requestBody, Object responseBody, WebServiceRequestStatus requestStatus, String failureReason,
			AbstractOrderModel order);
}
