/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;


/**
 *
 */
public interface PaymentB2BOrderService
{

	public Optional<OrderModel> getOrderByInvoiceId(String invoiceId);

	public Optional<OrderModel> getOrderBySadadBillId(String sadadBillId);

	public List<OrderModel> getOrderByPaymentProviderStatusAndPaymentProviderTransactionType(PaymentProviderStatus status,
			PaymentProviderTransactionType paymentProviderTransactionType);

	public List<AbstractOrderModel> getOrderByPaymentProviderStatus(final PaymentProviderStatus status);

	public List<AbstractOrderModel> getOrderByStatusAndPaymentB2BInvoiceCreatedSuccessfully(final OrderStatus status,
			boolean invoiceCreated);

}
