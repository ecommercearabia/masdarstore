/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.masdar.masdarb2bpayment.esal.beans.Invoice;


/**
 *
 */
public interface ESALCreditNotesRecordService
{
	public void addRecord(AbstractOrderModel order, Invoice invoice);
}
