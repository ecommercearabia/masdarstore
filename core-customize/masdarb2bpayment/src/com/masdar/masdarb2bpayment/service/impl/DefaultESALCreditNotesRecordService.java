/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.service.ESALCreditNotesRecordService;
import com.masdar.masdarpaymentB2Bprovider.model.ESALCreditNoteRecordModel;


/**
 *
 */
public class DefaultESALCreditNotesRecordService implements ESALCreditNotesRecordService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultESALCreditNotesRecordService.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void addRecord(final AbstractOrderModel order, final Invoice invoice)
	{
		if (Objects.isNull(order))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("order is null.");
			LOG.error(ex.getMessage());
			throw ex;
		}
		if (Objects.isNull(invoice))
		{
			final IllegalArgumentException ex = new IllegalArgumentException("invoce is null.");
			LOG.error(ex.getMessage());
			throw ex;
		}

		final ESALCreditNoteRecordModel record = getEsalCreditNotesRecord(invoice);
		record.setOrder(order);
		modelService.save(record);
	}


	/**
	 *
	 */
	private ESALCreditNoteRecordModel getEsalCreditNotesRecord(final Invoice invoice)
	{
		final ESALCreditNoteRecordModel record = new ESALCreditNoteRecordModel();
		record.setCreationDate(new Date());
		record.setInvoiceId(invoice.getInvoiceNumber());
		record.setInvoiceReferance(invoice.getInvoiceReference());
		return record;
	}

}
