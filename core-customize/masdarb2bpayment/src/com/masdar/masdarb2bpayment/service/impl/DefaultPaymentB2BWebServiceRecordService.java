/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.masdar.core.enums.WebServiceRecordType;
import com.masdar.core.enums.WebServiceRequestStatus;
import com.masdar.masdarb2bpayment.model.PaymentB2BWebServiceRecordModel;
import com.masdar.masdarb2bpayment.service.PaymentB2BWebServiceRecordService;

/**
 *
 */
public class DefaultPaymentB2BWebServiceRecordService implements PaymentB2BWebServiceRecordService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPaymentB2BWebServiceRecordService.class);

	@Resource(name = "modelService")
	private ModelService modelService;


	@Override
	public void addWebhookRecord(final Object requestBody, final WebServiceRequestStatus requestStatus, final String failureReason,
			final AbstractOrderModel order)

	{
		final var record = getRecord(requestBody, null, requestStatus, WebServiceRecordType.WEB_HOOK, failureReason, order);
		modelService.save(record);
		LOG.info("a webhook record {} has been created.", record);

	}

	@Override
	public void addApiRecord(final Object requestBody, final Object responseBody, final WebServiceRequestStatus requestStatus,
			final String failureReason, final AbstractOrderModel order)
	{
		final var record = getRecord(requestBody, responseBody, requestStatus, WebServiceRecordType.API, failureReason, order);
		modelService.save(record);
		LOG.info("an API record {} has been created.", record);

	}

	protected PaymentB2BWebServiceRecordModel getRecord(final Object requestBody, final Object responseBody,
			final WebServiceRequestStatus requestStatus, final WebServiceRecordType type, final String failureReason,
			final AbstractOrderModel order) {
		final var record = new PaymentB2BWebServiceRecordModel();

		final Gson gson = new Gson();
		record.setRequestBody(gson.toJson(requestBody));
		record.setRequestStatus(requestStatus);
		record.setResponseBody(gson.toJson(responseBody));
		record.setType(type);
		record.setCreationtime(new Date());
		record.setFailureReasons(failureReason);
		if (!Objects.isNull(order))
		{
			record.setOrder(order);
		}
		return record;
	}
}
