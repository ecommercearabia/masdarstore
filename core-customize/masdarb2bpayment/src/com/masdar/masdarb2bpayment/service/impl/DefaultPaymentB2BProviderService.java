/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.masdar.masdarb2bpayment.dao.PaymentB2BProviderDAO;
import com.masdar.masdarb2bpayment.service.PaymentB2BProviderService;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Class DefaultPaymentB2BProviderService.
 */
public class DefaultPaymentB2BProviderService implements PaymentB2BProviderService
{

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant BASESTORE_UID_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null or empty";

	/** The Constant BASESTORE_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	/** The Constant PROVIDER_DAO_NOT_FOUND. */
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";

	/** The payment B 2 b provider dao map. */
	@Resource(name = "paymentB2BProviderDaoMap")
	private Map<Class<?>, PaymentB2BProviderDAO> paymentB2BProviderDaoMap;

	/**
	 * Gets the payment B 2 b provider dao map.
	 *
	 * @return the payment B 2 b provider dao map
	 */
	protected Map<Class<?>, PaymentB2BProviderDAO> getPaymentB2BProviderDaoMap()
	{
		return paymentB2BProviderDaoMap;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @param providerClass the provider class
	 * @return the optional
	 */
	@Override
	public Optional<PaymentB2BProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderDAO> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProvider(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseStoreUid), BASESTORE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderDAO> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProvider(baseStoreUid);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel,
			final Class<?> providerClass)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderDAO> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass the provider class
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderDAO> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProviderByCurrentBaseStore();
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass the provider class
	 * @return the dao
	 */
	protected Optional<PaymentB2BProviderDAO> getDao(final Class<?> providerClass)
	{
		final PaymentB2BProviderDAO dao = getPaymentB2BProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}
}
