/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.service.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.masdarb2bpayment.dao.PaymentB2BOrderDAO;
import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;

/**
 *
 */
public class DefaultPaymentB2BOrderService implements PaymentB2BOrderService
{
	@Resource(name = "paymentB2BOrderDAO")
	private PaymentB2BOrderDAO paymentB2BOrderDAO;

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPaymentB2BOrderService.class);

	@Override
	public List<AbstractOrderModel> getOrderByPaymentProviderStatus(final PaymentProviderStatus status)
	{
		Preconditions.checkArgument(!Objects.isNull(status), "status is null");
		return paymentB2BOrderDAO.findByPaymentProviderStatus(status);
	}

	@Override
	public List<AbstractOrderModel> getOrderByStatusAndPaymentB2BInvoiceCreatedSuccessfully(final OrderStatus status,
			final boolean invoiceCreated)
	{
		Preconditions.checkArgument(!Objects.isNull(status), "status is null");
		return paymentB2BOrderDAO.findByStatusAndPaymentB2BInvoiceCreatedSuccessfully(status, invoiceCreated);
	}

	@Override
	public Optional<OrderModel> getOrderByInvoiceId(final String invoiceId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(invoiceId), "invoiceId is empty");
		return paymentB2BOrderDAO.findByInvoiceId(invoiceId);
	}


	@Override
	public Optional<OrderModel> getOrderBySadadBillId(final String sadadBillId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sadadBillId), "sadadBillId is empty");
		return paymentB2BOrderDAO.findBySadadBillId(sadadBillId);
	}

	@Override
	public List<OrderModel> getOrderByPaymentProviderStatusAndPaymentProviderTransactionType(final PaymentProviderStatus status,
			final PaymentProviderTransactionType paymentProviderTransactionType)
	{
		Preconditions.checkArgument(!Objects.isNull(status), "status is null");
		Preconditions.checkArgument(!Objects.isNull(paymentProviderTransactionType), "paymentProviderTransactionType is null");
		return paymentB2BOrderDAO.findByPaymentProviderStatusAndPaymentProviderTransactionType(status,
				paymentProviderTransactionType);
	}


}
