package com.masdar.masdarb2bpayment.exception;

import com.masdar.masdarb2bpayment.exception.type.PaymentB2BExceptionType;


public class PaymentB2BException extends Exception
{

	private final PaymentB2BExceptionType type;

	private final Exception exception;

	public PaymentB2BException(final PaymentB2BExceptionType type, final String msg, final Exception exception)
	{
		super(msg);
		this.type = type;
		this.exception = exception;
	}

	public PaymentB2BExceptionType getType()
	{
		return type;
	}

	/**
	 * @return the exception
	 */
	public Exception getException()
	{
		return exception;
	}



}
