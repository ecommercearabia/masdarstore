/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.exception.type;

/**
 *
 */
public enum PaymentB2BExceptionType
{
	ESAL_PAYMENT_EXCEPTION("esal exception");

	final String msg;

	/**
	 *
	 */
	private PaymentB2BExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

}
