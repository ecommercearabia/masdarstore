/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.context;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Interface LoyaltyProgramProviderContext.
 */
public interface PaymentB2BProviderContext
{

	/**
	 * Gets the provider.
	 *
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<PaymentB2BProviderModel> getProvider(CheckoutPaymentType paymentType);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<PaymentB2BProviderModel> getProvider(String baseStoreUid, CheckoutPaymentType paymentType);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<PaymentB2BProviderModel> getProvider(BaseStoreModel baseStoreModel, CheckoutPaymentType paymentType);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the provider
	 */
	/*
	 * public Optional<PaymentB2BProviderModel> getProvider(BaseStoreModel baseStoreModel);
	 */}
