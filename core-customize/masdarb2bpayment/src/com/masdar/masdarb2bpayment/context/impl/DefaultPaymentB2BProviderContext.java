/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.context.impl;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.masdar.masdarb2bpayment.context.PaymentB2BProviderContext;
import com.masdar.masdarb2bpayment.strategy.PaymentB2BProviderStrategy;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Class DefaultPaymentB2BProviderContext.
 */
public class DefaultPaymentB2BProviderContext implements PaymentB2BProviderContext
{

	/** The payment B 2 B provider map. */
	@Resource(name = "paymentB2BProviderStrategyMap")
	private Map<CheckoutPaymentType, PaymentB2BProviderStrategy> paymentB2BProviderMap;

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_TYPE_MUSTN_T_BE_NULL = "payment type mustn't be null";

	/** The Constant BASESTORE_UID_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null";

	/** The Constant BASESTORE_MODEL_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant BASE_STORE_DEFAULT_PROVIDER. */
	private static final String BASE_STORE_DEFAULT_PROVIDER = "EsalPaymentB2BProvider";

	/**
	 * Gets the payment B 2 B provider map.
	 *
	 * @return the payment B 2 B provider map
	 */
	public Map<CheckoutPaymentType, PaymentB2BProviderStrategy> getPaymentB2BProviderMap()
	{
		return paymentB2BProviderMap;
	}

	/**
	 * Gets the provider.
	 *
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getProvider(final CheckoutPaymentType paymentType)
	{
		Preconditions.checkArgument(paymentType != null, PAYMENT_TYPE_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderStrategy> strategy = getStrategy(paymentType);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentBaseStore();
	}

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getProvider(final String baseStoreUid, final CheckoutPaymentType paymentType)
	{
		Preconditions.checkArgument(paymentType != null, PAYMENT_TYPE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreUid != null, BASESTORE_UID_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderStrategy> strategy = getStrategy(paymentType);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreUid);
	}

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getProvider(final BaseStoreModel baseStoreModel,
			final CheckoutPaymentType paymentType)
	{
		Preconditions.checkArgument(paymentType != null, PAYMENT_TYPE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BProviderStrategy> strategy = getStrategy(paymentType);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreModel);
	}



	/**
	 * Gets the strategy.
	 *
	 * @param providerClass the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentB2BProviderStrategy> getStrategy(final CheckoutPaymentType paymentType)
	{
		final PaymentB2BProviderStrategy strategy = getPaymentB2BProviderMap().get(paymentType);

		return Optional.ofNullable(strategy);
	}
}
