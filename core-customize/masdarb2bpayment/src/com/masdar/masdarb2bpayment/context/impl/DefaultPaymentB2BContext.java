/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.context.impl;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.context.PaymentB2BProviderContext;
import com.masdar.masdarb2bpayment.entry.InvoiceCancelResult;
import com.masdar.masdarb2bpayment.entry.InvoicePaymentStatus;
import com.masdar.masdarb2bpayment.exception.PaymentB2BException;
import com.masdar.masdarb2bpayment.strategy.PaymentB2BStrategy;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Class DefaultPaymentB2BContext.
 */
public class DefaultPaymentB2BContext implements PaymentB2BContext
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPaymentB2BContext.class);

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data can not be null";

	/** The Constant PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL. */
	private static final String PAYMENT_TYPE_MODEL_MUST_NOT_BE_NULL = "payment type must not be null";

	/** The Constant PAYMENT_STRATEGY_NOT_FOUND. */
	private static final String PAYMENT_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";

	private static final String PAYMENT_PROVIDERS_MUSTN_T_BE_EMPTY = "paymentProviders mustn't be empty";

	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";

	private static final String ABSTRACTORDERS_MUSTN_T_BE_EMPTY = "abstractOrder mustn't be empty";

	private static final String BASESTORE_MUSTN_T_BE_NULL = "basestore mustn't be null";

	/** The payment provider context. */
	@Resource(name = "paymentB2BProviderContext")
	private PaymentB2BProviderContext paymentB2BProviderContext;

	/** The payment strategy map. */
	@Resource(name = "paymentB2BStrategyMap")
	private Map<Class<?>, PaymentB2BStrategy> paymentB2BStrategyMap;



	/**
	 * @return the paymentB2BStrategyMap
	 */
	protected Map<Class<?>, PaymentB2BStrategy> getPaymentB2BStrategyMap()
	{
		return paymentB2BStrategyMap;
	}

	protected Optional<PaymentB2BStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentB2BStrategy strategy = getPaymentB2BStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PAYMENT_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	@Override
	public String createInvoice(final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(abstractOrderModel);
		return createInvoice(paymentB2BProvider, abstractOrderModel);
	}

	@Override
	public void createInvoices(final BaseStoreModel baseStoreModel, final CheckoutPaymentType paymentType,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentType != null, PAYMENT_TYPE_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentB2BProviderModel> paymentB2BProvider = paymentB2BProviderContext.getProvider(baseStoreModel,
				paymentType);
		if (!paymentB2BProvider.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		}
		createInvoices(paymentB2BProvider.get(), abstractOrderModels);

	}

	@Override
	public Optional<InvoiceCancelResult> cancelInvoice(final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(abstractOrderModel);
		return cancelInvoice(paymentB2BProvider, abstractOrderModel);
	}

	@Override
	public Optional<InvoiceCancelResult> cancelInvoices(final BaseStoreModel baseStoreModel, final CheckoutPaymentType paymentType,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		final Optional<PaymentB2BProviderModel> paymentB2BProvider = paymentB2BProviderContext.getProvider(baseStoreModel,
				paymentType);
		if (!paymentB2BProvider.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		}
		return cancelInvoices(paymentB2BProvider.get(), abstractOrderModels);
	}

	@Override
	public Optional<InvoicePaymentStatus> getPaymentStatus(final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(abstractOrderModel);

		return getPaymentStatus(paymentB2BProvider, abstractOrderModel);
	}

	@Override
	public String createInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel.getStore() != null, BASESTORE_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);

		}
		return strategy.get().createInvoice(paymentB2BProviderModel, abstractOrderModel);
	}

	@Override
	public void createInvoices(final PaymentB2BProviderModel paymentB2BProviderModel,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);


		if (CollectionUtils.isEmpty(abstractOrderModels))
		{
			throw new IllegalArgumentException(ABSTRACTORDERS_MUSTN_T_BE_EMPTY);
		}

		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		strategy.get().createInvoices(paymentB2BProviderModel, abstractOrderModels);

	}

	@Override
	public Optional<InvoiceCancelResult> cancelInvoice(final PaymentB2BProviderModel paymentB2BProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDERS_MUSTN_T_BE_EMPTY);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, BASESTORE_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{

			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		return strategy.get().cancelInvoice(paymentB2BProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<InvoiceCancelResult> cancelInvoices(final PaymentB2BProviderModel paymentB2BProviderModel,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		return strategy.get().cancelInvoices(paymentB2BProviderModel, abstractOrderModels);
	}

	@Override
	public Optional<InvoicePaymentStatus> getPaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel.getStore() != null, BASESTORE_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		return strategy.get().getPaymentStatus(paymentB2BProviderModel, abstractOrderModel);

	}

	@Override
	public void updateInvoice(final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(abstractOrderModel);
		updateInvoice(paymentB2BProvider, abstractOrderModel);
	}

	@Override
	public void updateInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel.getStore() != null, BASESTORE_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		strategy.get().updateInvoice(paymentB2BProviderModel, abstractOrderModel);
	}

	@Override
	public boolean isPaid(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		return strategy.get().isPaid(paymentB2BProviderModel, order);
	}

	@Override
	public boolean isPaid(final AbstractOrderModel order)
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(order);
		return isPaid(paymentB2BProvider, order);
	}

	@Override
	public void updatePaymentStatus(final AbstractOrderModel order)
	{
		final PaymentB2BProviderModel paymentB2BProvider = getPaymentProvider(order);
		updatePaymentStatus(paymentB2BProvider, order);

	}

	@Override
	public void updatePaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		strategy.get().updatePaymentStatus(paymentB2BProviderModel, order);

	}

	@Override
	public boolean cancelOrder(final AbstractOrderModel order)
	{
		final PaymentB2BProviderModel paymentB2BProviderModel = getPaymentProvider(order);
		return cancelOrder(paymentB2BProviderModel, order);
	}


	@Override
	public boolean cancelOrder(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final Optional<PaymentB2BStrategy> strategy = getStrategy(paymentB2BProviderModel.getClass());
		if (!strategy.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_STRATEGY_NOT_FOUND);
		}
		return strategy.get().cancelOrder(paymentB2BProviderModel, order);

	}


	/**
	 * @return
	 *
	 */
	protected PaymentB2BProviderModel getPaymentProvider(final AbstractOrderModel order)
	{
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order.getStore() != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order.getPaymentType() != null, PAYMENT_TYPE_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentB2BProviderModel> paymentB2BProvider = paymentB2BProviderContext.getProvider(order.getStore(),
				order.getPaymentType());
		if (!paymentB2BProvider.isPresent())
		{
			throw new IllegalArgumentException(PAYMENT_PROVIDER_MUSTN_T_BE_NULL);

		}
		return paymentB2BProvider.get();

	}




}
