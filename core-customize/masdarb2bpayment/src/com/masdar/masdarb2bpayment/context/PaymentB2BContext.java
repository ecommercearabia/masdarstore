/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.context;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarb2bpayment.entry.InvoiceCancelResult;
import com.masdar.masdarb2bpayment.entry.InvoicePaymentStatus;
import com.masdar.masdarb2bpayment.exception.PaymentB2BException;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Interface PaymentB2BContext.
 */
public interface PaymentB2BContext
{

	/**
	 * Creates the invoice.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public String createInvoice(AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	public String createInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException;
	/**
	 * Creates the invoices.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModels
	 *           the abstract order models
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public void createInvoices(BaseStoreModel baseStoreModel, CheckoutPaymentType paymentType,
			List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException;

	public void updateInvoice(AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	public void updateInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException;

	/**
	 * Cancel invoice.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoiceCancelResult> cancelInvoice(AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	/**
	 * Cancel invoices.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModels
	 *           the abstract order models
	 * @return the optional
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoiceCancelResult> cancelInvoices(BaseStoreModel baseStoreModel, CheckoutPaymentType paymentType,
			List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException;

	/**
	 * Gets the payment status.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment status
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoicePaymentStatus> getPaymentStatus(AbstractOrderModel abstractOrderModel) throws PaymentB2BException;



	/**
	 * Creates the invoices.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModels
	 *           the abstract order models
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public void createInvoices(PaymentB2BProviderModel paymentB2BProviderModel, List<AbstractOrderModel> abstractOrderModels)
			throws PaymentB2BException;

	/**
	 * Cancel invoice.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoiceCancelResult> cancelInvoice(final PaymentB2BProviderModel paymentB2BProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	/**
	 * Cancel invoices.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModels
	 *           the abstract order models
	 * @return the optional
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoiceCancelResult> cancelInvoices(PaymentB2BProviderModel paymentB2BProviderModel,
			List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException;

	/**
	 * Gets the payment status.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment status
	 * @throws PaymentB2BException
	 *            the payment B 2 B exception
	 */
	public Optional<InvoicePaymentStatus> getPaymentStatus(PaymentB2BProviderModel paymentB2BProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentB2BException;


	public boolean isPaid(PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);

	public boolean isPaid(AbstractOrderModel order);

	public void updatePaymentStatus(AbstractOrderModel order);

	public void updatePaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);

	public boolean cancelOrder(AbstractOrderModel order);

	public boolean cancelOrder(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);



	/*
	 * public boolean checkPaymentTypeForAction(PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel
	 * order);
	 *
	 * public boolean checkPaymentTypeForAction(AbstractOrderModel order);
	 */
}
