/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum ServiceType
{
	CIP("CIP"), ELCT("ELCT"), PHON("PHON"), GSM("GSM"), LLIN("LLIN"), INSR("INSR"), BKSV("BKSV"), GOVT("GOVT"), MED("MED"), CCRD(
			"CCRD"), UTIL("UTIL"), PCIN("PCIN"), EXAM("EXAM");

	private String value;

	/**
	 *
	 */
	private ServiceType(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}

}
