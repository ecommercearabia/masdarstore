/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 *
 */
public enum CancelStatus
{
	SUCCESS("success");

	private String code;

	/**
	 *
	 */
	private CancelStatus(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the code
	 */
	protected String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	protected void setCode(final String code)
	{
		this.code = code;
	}


}
