/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum PaymentStatus
{
	PMT_NEW("pmt_new"), PMT_UPDATED("pmt_updated"), PMT_ANY("pmt_any"), PMT_DUPLICATE("pmt_duplicate"), PMT_REVERSAL(
			"pmt_reversal"), PMT_TRANSF("pmt_transf"), PMT_NOT_TRANSF("pmt_not_transf"), PMT_NOT_ADVICED(
					"pmt_not_adviced"), PMT_RECON("pmt_recon"), PMT_ALREADY_RECON("pmt_already_recon"), PMT_NOT_RECON(
							"pmt_not_recon"), PMT_NOT_IN_BANK("pmt_not_in_bank"), PMT_NOT_IN_SADAD("pmt_not_in_sadad"), PMT_MIS_MATCH(
									"pmt_mis_match"), PMT_COMPLETED("pmt_completed"), PMT_NOT_COMPLETED(
											"pmt_not_completed"), PMT_BILLER_MISTMATCH("pmt_biller_mistmatch");

	private String value;

	/**
	 *
	 */
	private PaymentStatus(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}

}
