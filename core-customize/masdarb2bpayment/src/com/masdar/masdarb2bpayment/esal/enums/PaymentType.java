/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum PaymentType
{
	POST("POST"), RCHG("RCHG"), RNEW("RNEW"), RDEP("RDEP"), RADV("RADV");

	private String value;

	/**
	 *
	 */
	private PaymentType(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}


}
