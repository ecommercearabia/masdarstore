/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum SeverityType
{
	INFO("INFO"), WARNING("WARNING"), ERROR("ERROR");

	private String value;

	/**
	 *
	 */
	private SeverityType(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}

}
