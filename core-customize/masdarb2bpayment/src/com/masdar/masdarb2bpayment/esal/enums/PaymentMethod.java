/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum PaymentMethod
{
	CASH("CASH"), CCARD("CCARD"), EFT("EFT"), ACTDEB("ACTDEB");

	private String value;

	/**
	 *
	 */
	private PaymentMethod(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}

}
