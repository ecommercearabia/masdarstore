/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum OfficialIdType
{
	NAT("NAT"), IQA("IQA"), BIS("BIS"), ACT("ACT"), SID("SID"), BTL("BTL"), BED("BED"), BIE("BIE"), SED("SED"), PAS("PAS");

	private String value;

	/**
	 *
	 */
	private OfficialIdType(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}



}
