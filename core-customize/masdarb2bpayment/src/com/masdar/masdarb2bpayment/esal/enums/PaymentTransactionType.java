/**
 *
 */
package com.masdar.masdarb2bpayment.esal.enums;

/**
 * @author mbaker
 *
 */
public enum PaymentTransactionType
{
	SPTN("SPTN"), BLRPTN("BLRPTN"), BNKPTN("BNKPTN"), BNKRVL("BNKRVL");

	private String value;

	/**
	 *
	 */
	private PaymentTransactionType(final String value)
	{
		this.value = value;
	}

	/**
	 * @return the value
	 */
	protected String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	protected void setValue(final String value)
	{
		this.value = value;
	}
}
