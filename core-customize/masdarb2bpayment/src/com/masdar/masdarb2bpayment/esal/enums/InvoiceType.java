package com.masdar.masdarb2bpayment.esal.enums;

public enum InvoiceType {

	INVOICE("I"), CREDIT_NOTE("C"), DEBIT_NOTE("D");

	private final String code;

	private InvoiceType(final String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
