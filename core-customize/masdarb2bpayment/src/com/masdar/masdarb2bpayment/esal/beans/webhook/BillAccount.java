/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class BillAccount
{
	@SerializedName("billingAcct")
	private String billingAccount;

	@SerializedName("billNumber")
	private String billNumber;

	/**
	 * @return the billingAccount
	 */
	public String getBillingAccount()
	{
		return billingAccount;
	}

	/**
	 * @param billingAccount
	 *           the billingAccount to set
	 */
	public void setBillingAccount(final String billingAccount)
	{
		this.billingAccount = billingAccount;
	}

	/**
	 * @return the billNumber
	 */
	public String getBillNumber()
	{
		return billNumber;
	}

	/**
	 * @param billNumber
	 *           the billNumber to set
	 */
	public void setBillNumber(final String billNumber)
	{
		this.billNumber = billNumber;
	}

}
