/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.sadad;

import java.util.List;


/**
 * @author mbaker
 *
 */
public class SadadBillIdRequest
{

	private String supplierId;

	private List<SadadInvoice> invoices;

	/**
	 * @return the supplierId
	 */
	public String getSupplierId()
	{
		return supplierId;
	}

	/**
	 * @param supplierId
	 *           the supplierId to set
	 */
	public void setSupplierId(final String supplierId)
	{
		this.supplierId = supplierId;
	}

	/**
	 * @return the invoices
	 */
	public List<SadadInvoice> getInvoices()
	{
		return invoices;
	}

	/**
	 * @param invoices
	 *           the invoices to set
	 */
	public void setInvoices(final List<SadadInvoice> invoices)
	{
		this.invoices = invoices;
	}


}
