package com.masdar.masdarb2bpayment.esal.beans;

public class ProfitMargin {

	private boolean applied;
	private String procedureArabic;
	private String procedureEnglish;

	public boolean isApplied() {
		return applied;
	}

	public void setApplied(final boolean applied) {
		this.applied = applied;
	}

	public String getProcedureArabic() {
		return procedureArabic;
	}

	public void setProcedureArabic(final String procedureArabic) {
		this.procedureArabic = procedureArabic;
	}

	public String getProcedureEnglish() {
		return procedureEnglish;
	}

	public void setProcedureEnglish(final String procedureEnglish) {
		this.procedureEnglish = procedureEnglish;
	}

}
