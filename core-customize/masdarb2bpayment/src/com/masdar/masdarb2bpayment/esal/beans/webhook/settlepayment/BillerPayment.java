/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.Payor;
/**
 * @author mbaker
 *
 */
public class BillerPayment
{
	@SerializedName("pmtTransId")
	private List<PaymentTransaction> pyamentTransactions;

	@SerializedName("custId")
	private Payor customer;

	@SerializedName("pmtStatus")
	private PaymentStatus paymentStatus;

	@SerializedName("pmtInfo")
	private PaymentInfo paymentInfo;

	/**
	 * @return the pyamentTransactions
	 */
	public List<PaymentTransaction> getPyamentTransactions()
	{
		return pyamentTransactions;
	}

	/**
	 * @param pyamentTransactions
	 *           the pyamentTransactions to set
	 */
	public void setPyamentTransactions(final List<PaymentTransaction> pyamentTransactions)
	{
		this.pyamentTransactions = pyamentTransactions;
	}

	/**
	 * @return the customer
	 */
	public Payor getCustomer()
	{
		return customer;
	}

	/**
	 * @param customer
	 *           the customer to set
	 */
	public void setCustomer(final Payor customer)
	{
		this.customer = customer;
	}

	/**
	 * @return the paymentStatus
	 */
	public PaymentStatus getPaymentStatus()
	{
		return paymentStatus;
	}

	/**
	 * @param paymentStatus
	 *           the paymentStatus to set
	 */
	public void setPaymentStatus(final PaymentStatus paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	/**
	 * @return the paymentInfo
	 */
	public PaymentInfo getPaymentInfo()
	{
		return paymentInfo;
	}

	/**
	 * @param paymentInfo
	 *           the paymentInfo to set
	 */
	public void setPaymentInfo(final PaymentInfo paymentInfo)
	{
		this.paymentInfo = paymentInfo;
	}


}
