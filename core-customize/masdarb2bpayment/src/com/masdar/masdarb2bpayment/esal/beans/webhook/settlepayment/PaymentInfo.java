/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import de.hybris.platform.payment.methods.PaymentMethod;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.Payor;
import com.masdar.masdarb2bpayment.esal.enums.PaymentType;
import com.masdar.masdarb2bpayment.esal.enums.ServiceType;


/**
 * @author mbaker
 *
 */
public class PaymentInfo
{
	@SerializedName("curAmt")
	private double amount;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@SerializedName("prcDt")
	private Date processingDate;

	@JsonFormat(pattern = "yyyy-mm-dd")
	@SerializedName("dueDt")
	private Date dueDate;

	private String bankId;

	private String branchCode;

	private String districtCode;

	private String accessChannel;

	@SerializedName("pmtMethod")
	private PaymentMethod paymentMethod;

	@SerializedName("pmtType")
	private PaymentType paymentType;

	@SerializedName("chkDigit")
	private Payor checkDigit;

	@SerializedName("serviceType")
	private ServiceType serviceType;

	@SerializedName("pmtRefInfo")
	private String paymentReferranceInfo;

	@SerializedName("includeInfoPlus")
	private InfoPlus includeInfoPlus;

	@SerializedName("beneficiaryId")
	private Payor beneficiary;

	@SerializedName("phoneNum")
	private PhoneNumber phoneNumber;

	@SerializedName("accountId")
	private BillAccount accountId;

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the processingDate
	 */
	public Date getProcessingDate()
	{
		return processingDate;
	}

	/**
	 * @param processingDate
	 *           the processingDate to set
	 */
	public void setProcessingDate(final Date processingDate)
	{
		this.processingDate = processingDate;
	}

	/**
	 * @return the dueDate
	 */
	public Date getDueDate()
	{
		return dueDate;
	}

	/**
	 * @param dueDate
	 *           the dueDate to set
	 */
	public void setDueDate(final Date dueDate)
	{
		this.dueDate = dueDate;
	}

	/**
	 * @return the bankId
	 */
	public String getBankId()
	{
		return bankId;
	}

	/**
	 * @param bankId
	 *           the bankId to set
	 */
	public void setBankId(final String bankId)
	{
		this.bankId = bankId;
	}

	/**
	 * @return the branchCode
	 */
	public String getBranchCode()
	{
		return branchCode;
	}

	/**
	 * @param branchCode
	 *           the branchCode to set
	 */
	public void setBranchCode(final String branchCode)
	{
		this.branchCode = branchCode;
	}

	/**
	 * @return the districtCode
	 */
	public String getDistrictCode()
	{
		return districtCode;
	}

	/**
	 * @param districtCode
	 *           the districtCode to set
	 */
	public void setDistrictCode(final String districtCode)
	{
		this.districtCode = districtCode;
	}

	/**
	 * @return the accessChannel
	 */
	public String getAccessChannel()
	{
		return accessChannel;
	}

	/**
	 * @param accessChannel
	 *           the accessChannel to set
	 */
	public void setAccessChannel(final String accessChannel)
	{
		this.accessChannel = accessChannel;
	}

	/**
	 * @return the paymentMethod
	 */
	public PaymentMethod getPaymentMethod()
	{
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *           the paymentMethod to set
	 */
	public void setPaymentMethod(final PaymentMethod paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentType getPaymentType()
	{
		return paymentType;
	}

	/**
	 * @param paymentType
	 *           the paymentType to set
	 */
	public void setPaymentType(final PaymentType paymentType)
	{
		this.paymentType = paymentType;
	}

	/**
	 * @return the checkDigit
	 */
	public Payor getCheckDigit()
	{
		return checkDigit;
	}

	/**
	 * @param checkDigit
	 *           the checkDigit to set
	 */
	public void setCheckDigit(final Payor checkDigit)
	{
		this.checkDigit = checkDigit;
	}

	/**
	 * @return the serviceType
	 */
	public ServiceType getServiceType()
	{
		return serviceType;
	}

	/**
	 * @param serviceType
	 *           the serviceType to set
	 */
	public void setServiceType(final ServiceType serviceType)
	{
		this.serviceType = serviceType;
	}

	/**
	 * @return the paymentReferranceInfo
	 */
	public String getPaymentReferranceInfo()
	{
		return paymentReferranceInfo;
	}

	/**
	 * @param paymentReferranceInfo
	 *           the paymentReferranceInfo to set
	 */
	public void setPaymentReferranceInfo(final String paymentReferranceInfo)
	{
		this.paymentReferranceInfo = paymentReferranceInfo;
	}

	/**
	 * @return the includeInfoPlus
	 */
	public InfoPlus getIncludeInfoPlus()
	{
		return includeInfoPlus;
	}

	/**
	 * @param includeInfoPlus
	 *           the includeInfoPlus to set
	 */
	public void setIncludeInfoPlus(final InfoPlus includeInfoPlus)
	{
		this.includeInfoPlus = includeInfoPlus;
	}

	/**
	 * @return the beneficiary
	 */
	public Payor getBeneficiary()
	{
		return beneficiary;
	}

	/**
	 * @param beneficiary
	 *           the beneficiary to set
	 */
	public void setBeneficiary(final Payor beneficiary)
	{
		this.beneficiary = beneficiary;
	}

	/**
	 * @return the phoneNumber
	 */
	public PhoneNumber getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	public void setPhoneNumber(final PhoneNumber phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}




}
