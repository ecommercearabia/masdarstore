/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import java.util.List;

import com.google.gson.annotations.SerializedName;
/**
 * @author mbaker
 *
 */
public class BranchPayment
{
	@SerializedName("branchCode")
	private String branchCode;

	@SerializedName("curAmt")
	private double amount;

	@SerializedName("pmtRec")
	private List<BillerPayment> billerPayments;

	/**
	 * @return the branchCode
	 */
	public String getBranchCode()
	{
		return branchCode;
	}

	/**
	 * @param branchCode
	 *           the branchCode to set
	 */
	public void setBranchCode(final String branchCode)
	{
		this.branchCode = branchCode;
	}

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the billerPayments
	 */
	public List<BillerPayment> getBillerPayments()
	{
		return billerPayments;
	}

	/**
	 * @param billerPayments
	 *           the billerPayments to set
	 */
	public void setBillerPayments(final List<BillerPayment> billerPayments)
	{
		this.billerPayments = billerPayments;
	}




}
