/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class PaymentNotificationRequest
{

	@SerializedName("payment")
	private PaymentNotification payment;

	/**
	 * @return the payment
	 */
	public PaymentNotification getPayment()
	{
		return payment;
	}

	/**
	 * @param payment
	 *           the payment to set
	 */
	public void setPayment(final PaymentNotification payment)
	{
		this.payment = payment;
	}


}
