/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.Payor;


/**
 * @author mbaker
 *
 */
public class PaymentReferrance
{
	@SerializedName("billingAcct")
	private String billingAccount;

	@SerializedName("serviceCode")
	private String serviceCode;

	@SerializedName("chkDigit")
	private Payor checkDigit;

	/**
	 * @return the billingAccount
	 */
	public String getBillingAccount()
	{
		return billingAccount;
	}

	/**
	 * @param billingAccount
	 *           the billingAccount to set
	 */
	public void setBillingAccount(final String billingAccount)
	{
		this.billingAccount = billingAccount;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode()
	{
		return serviceCode;
	}

	/**
	 * @param serviceCode
	 *           the serviceCode to set
	 */
	public void setServiceCode(final String serviceCode)
	{
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the checkDigit
	 */
	public Payor getCheckDigit()
	{
		return checkDigit;
	}

	/**
	 * @param checkDigit
	 *           the checkDigit to set
	 */
	public void setCheckDigit(final Payor checkDigit)
	{
		this.checkDigit = checkDigit;
	}

}
