/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification;



import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.BankDetails;
import com.masdar.masdarb2bpayment.esal.beans.webhook.Payor;
import com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment.PaymentStatus;
import com.masdar.masdarb2bpayment.esal.enums.PaymentMethod;


/**
 * @author mbaker
 *
 */
public class PaymentNotification
{
	@SerializedName("bankDetails")
	private BankDetails bankDetails;

	@SerializedName("billerId")
	private String billerId;

	@SerializedName("curAmt")
	private double curAmt;

	@SerializedName("pmtRefInfo")
	private String pmtRefInfo;

	@SerializedName("payor")
	private Payor payor;

	@SerializedName("prcDt")
	private String prcDt;

	@SerializedName("dueDt")
	private String dueDt;

	@SerializedName("efftDt")
	private String efftDt;

	@SerializedName("sadadPmtId")
	private String sadadPmtId;

	@SerializedName("bankPmtId")
	private String bankPmtId;

	@SerializedName("bankReversalId")
	private String bankReversalId;

	@SerializedName("pmtMethod")
	private PaymentMethod pmtMethod;

	@SerializedName("proxyPayor")
	private Payor proxyPayor;

	@SerializedName("status")
	private PaymentStatus status;

	@SerializedName("prePaidPaymentRef")
	private PrePaidPaymentReferrance prePaidPaymentRef;

	@SerializedName("postPaidPaymentRef")
	private PostPaidPaymentReferrance postPaidPaymentRef;

	/**
	 * @return the bankDetails
	 */
	public BankDetails getBankDetails()
	{
		return bankDetails;
	}

	/**
	 * @param bankDetails
	 *           the bankDetails to set
	 */
	public void setBankDetails(final BankDetails bankDetails)
	{
		this.bankDetails = bankDetails;
	}

	/**
	 * @return the billerId
	 */
	public String getBillerId()
	{
		return billerId;
	}

	/**
	 * @param billerId
	 *           the billerId to set
	 */
	public void setBillerId(final String billerId)
	{
		this.billerId = billerId;
	}

	/**
	 * @return the curAmt
	 */
	public double getCurAmt()
	{
		return curAmt;
	}

	/**
	 * @param curAmt
	 *           the curAmt to set
	 */
	public void setCurAmt(final double curAmt)
	{
		this.curAmt = curAmt;
	}

	/**
	 * @return the pmtRefInfo
	 */
	public String getPmtRefInfo()
	{
		return pmtRefInfo;
	}

	/**
	 * @param pmtRefInfo
	 *           the pmtRefInfo to set
	 */
	public void setPmtRefInfo(final String pmtRefInfo)
	{
		this.pmtRefInfo = pmtRefInfo;
	}

	/**
	 * @return the payor
	 */
	public Payor getPayor()
	{
		return payor;
	}

	/**
	 * @param payor
	 *           the payor to set
	 */
	public void setPayor(final Payor payor)
	{
		this.payor = payor;
	}

	/**
	 * @return the prcDt
	 */
	public String getPrcDt()
	{
		return prcDt;
	}

	/**
	 * @param prcDt
	 *           the prcDt to set
	 */
	public void setPrcDt(final String prcDt)
	{
		this.prcDt = prcDt;
	}

	/**
	 * @return the dueDt
	 */
	public String getDueDt()
	{
		return dueDt;
	}

	/**
	 * @param dueDt
	 *           the dueDt to set
	 */
	public void setDueDt(final String dueDt)
	{
		this.dueDt = dueDt;
	}

	/**
	 * @return the efftDt
	 */
	public String getEfftDt()
	{
		return efftDt;
	}

	/**
	 * @param efftDt
	 *           the efftDt to set
	 */
	public void setEfftDt(final String efftDt)
	{
		this.efftDt = efftDt;
	}

	/**
	 * @return the sadadPmtId
	 */
	public String getSadadPmtId()
	{
		return sadadPmtId;
	}

	/**
	 * @param sadadPmtId
	 *           the sadadPmtId to set
	 */
	public void setSadadPmtId(final String sadadPmtId)
	{
		this.sadadPmtId = sadadPmtId;
	}

	/**
	 * @return the bankPmtId
	 */
	public String getBankPmtId()
	{
		return bankPmtId;
	}

	/**
	 * @param bankPmtId
	 *           the bankPmtId to set
	 */
	public void setBankPmtId(final String bankPmtId)
	{
		this.bankPmtId = bankPmtId;
	}

	/**
	 * @return the bankReversalId
	 */
	public String getBankReversalId()
	{
		return bankReversalId;
	}

	/**
	 * @param bankReversalId
	 *           the bankReversalId to set
	 */
	public void setBankReversalId(final String bankReversalId)
	{
		this.bankReversalId = bankReversalId;
	}

	/**
	 * @return the pmtMethod
	 */
	public PaymentMethod getPmtMethod()
	{
		return pmtMethod;
	}

	/**
	 * @param pmtMethod
	 *           the pmtMethod to set
	 */
	public void setPmtMethod(final PaymentMethod pmtMethod)
	{
		this.pmtMethod = pmtMethod;
	}

	/**
	 * @return the proxyPayor
	 */
	public Payor getProxyPayor()
	{
		return proxyPayor;
	}

	/**
	 * @param proxyPayor
	 *           the proxyPayor to set
	 */
	public void setProxyPayor(final Payor proxyPayor)
	{
		this.proxyPayor = proxyPayor;
	}

	/**
	 * @return the status
	 */
	public PaymentStatus getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final PaymentStatus status)
	{
		this.status = status;
	}

	/**
	 * @return the prePaidPaymentRef
	 */
	public PrePaidPaymentReferrance getPrePaidPaymentRef()
	{
		return prePaidPaymentRef;
	}

	/**
	 * @param prePaidPaymentRef
	 *           the prePaidPaymentRef to set
	 */
	public void setPrePaidPaymentRef(final PrePaidPaymentReferrance prePaidPaymentRef)
	{
		this.prePaidPaymentRef = prePaidPaymentRef;
	}

	/**
	 * @return the postPaidPaymentRef
	 */
	public PostPaidPaymentReferrance getPostPaidPaymentRef()
	{
		return postPaidPaymentRef;
	}

	/**
	 * @param postPaidPaymentRef
	 *           the postPaidPaymentRef to set
	 */
	public void setPostPaidPaymentRef(final PostPaidPaymentReferrance postPaidPaymentRef)
	{
		this.postPaidPaymentRef = postPaidPaymentRef;
	}

}
