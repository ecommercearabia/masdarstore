/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class BillAccount
{
	@SerializedName("billingAcct")
	private String billingAccount;

	@SerializedName("billerId")
	private String billerId;

	/**
	 * @return the billingAccount
	 */
	public String getBillingAccount()
	{
		return billingAccount;
	}

	/**
	 * @param billingAccount
	 *           the billingAccount to set
	 */
	public void setBillingAccount(final String billingAccount)
	{
		this.billingAccount = billingAccount;
	}

	/**
	 * @return the billerId
	 */
	public String getBillerId()
	{
		return billerId;
	}

	/**
	 * @param billerId
	 *           the billerId to set
	 */
	public void setBillerId(final String billerId)
	{
		this.billerId = billerId;
	}


}
