/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.Payor;


/**
 * @author mbaker
 *
 */
public class PrePaidPaymentReferrance extends PaymentReferrance
{

	@SerializedName("beneficiaryPhoneNum")
	private String beneficiaryPhoneNum;

	@SerializedName("beneficiaryId")
	private Payor beneficiaryId;

	/**
	 * @return the beneficiaryPhoneNum
	 */
	public String getBeneficiaryPhoneNum()
	{
		return beneficiaryPhoneNum;
	}

	/**
	 * @param beneficiaryPhoneNum
	 *           the beneficiaryPhoneNum to set
	 */
	public void setBeneficiaryPhoneNum(final String beneficiaryPhoneNum)
	{
		this.beneficiaryPhoneNum = beneficiaryPhoneNum;
	}

	/**
	 * @return the beneficiaryId
	 */
	public Payor getBeneficiaryId()
	{
		return beneficiaryId;
	}

	/**
	 * @param beneficiaryId
	 *           the beneficiaryId to set
	 */
	public void setBeneficiaryId(final Payor beneficiaryId)
	{
		this.beneficiaryId = beneficiaryId;
	}



}
