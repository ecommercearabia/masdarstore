/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class SettlementReportRequest
{

	@SerializedName("status")
	private Status status;

	@SerializedName("rqUID")
	private String requestUID;

	@SerializedName("billerReconUploadRq")
	private BillerReconUploadRequest billerReconUploadRequest;

	/**
	 * @return the status
	 */
	public Status getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final Status status)
	{
		this.status = status;
	}

	/**
	 * @return the requestUID
	 */
	public String getRequestUID()
	{
		return requestUID;
	}

	/**
	 * @param requestUID
	 *           the requestUID to set
	 */
	public void setRequestUID(final String requestUID)
	{
		this.requestUID = requestUID;
	}

	/**
	 * @return the billerReconUploadRequest
	 */
	public BillerReconUploadRequest getBillerReconUploadRequest()
	{
		return billerReconUploadRequest;
	}

	/**
	 * @param billerReconUploadRequest
	 *           the billerReconUploadRequest to set
	 */
	public void setBillerReconUploadRequest(final BillerReconUploadRequest billerReconUploadRequest)
	{
		this.billerReconUploadRequest = billerReconUploadRequest;
	}


}
