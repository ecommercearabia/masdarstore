/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import java.util.List;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class BankPayment
{
	@SerializedName("bankId")
	private String bankId;

	@SerializedName("curAmt")
	private double amount;

	@SerializedName("pmtBranchRec")
	private List<BranchPayment> branchPayments;

	/**
	 * @return the bankId
	 */
	public String getBankId()
	{
		return bankId;
	}

	/**
	 * @param bankId
	 *           the bankId to set
	 */
	public void setBankId(final String bankId)
	{
		this.bankId = bankId;
	}

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the branchPayments
	 */
	public List<BranchPayment> getBranchPayments()
	{
		return branchPayments;
	}

	/**
	 * @param branchPayments the branchPayments to set
	 */
	public void setBranchPayments(final List<BranchPayment> branchPayments)
	{
		this.branchPayments = branchPayments;
	}

}
