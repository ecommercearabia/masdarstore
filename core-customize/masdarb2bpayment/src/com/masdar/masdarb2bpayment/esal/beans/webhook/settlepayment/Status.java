/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.enums.SeverityType;


/**
 * @author mbaker
 *
 */
public class Status
{
	@SerializedName("statusCode")
	private long statusCode;

	@SerializedName("shortDesc")
	private String shortDescription;

	@SerializedName("severityType")
	private SeverityType severityType;

	/**
	 * @return the statusCode
	 */
	public long getStatusCode()
	{
		return statusCode;
	}

	/**
	 * @param statusCode
	 *           the statusCode to set
	 */
	public void setStatusCode(final long statusCode)
	{
		this.statusCode = statusCode;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *           the shortDescription to set
	 */
	public void setShortDescription(final String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the severityType
	 */
	public SeverityType getSeverityType()
	{
		return severityType;
	}

	/**
	 * @param severityType
	 *           the severityType to set
	 */
	public void setSeverityType(final SeverityType severityType)
	{
		this.severityType = severityType;
	}

}
