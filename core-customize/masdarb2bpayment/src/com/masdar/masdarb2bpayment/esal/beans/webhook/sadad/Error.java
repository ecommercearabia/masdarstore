/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.sadad;

/**
 * @author mbaker
 *
 */
public class Error
{
	private String code;

	private String status;

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}


}
