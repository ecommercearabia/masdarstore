package com.masdar.masdarb2bpayment.esal.beans;

import java.util.List;


public class Invoice {

	private String invoiceType;
	private Customer customer;
	private String invoiceNumber;
	private String invoiceIssueDate;
	private String dueDate;
	private String dateOfDelivery;
	private String currency;
	private String foreignCurrency;
	private String invoiceReference;
	private String remarksArabic;
	private String remarksEnglish;
	private String narrationArabic;
	private String narrationEnglish;
	private String shipmentRefNumber;
	private String salespersonEnglish;
	private String salespersonArabic;
	private Boolean selfAccountForTax;
	private Double totalBeforeVAT;
	private Double totalVAT;
	private Double grandTotal;
	private Double advanceAmount;
	private Double outstandingAmount;
	private Double percentOfCompletion;
	private Boolean milestonePayments;
	private Supplier supplier;
	private ProfitMargin profitMargin;
	private List<LineItem> lineItems;

	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(final String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(final String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceIssueDate()
	{
		return invoiceIssueDate;
	}

	public void setInvoiceIssueDate(final String invoiceIssueDate)
	{
		this.invoiceIssueDate = invoiceIssueDate;
	}

	public String getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(final String dueDate)
	{
		this.dueDate = dueDate;
	}

	public String getDateOfDelivery()
	{
		return dateOfDelivery;
	}

	public void setDateOfDelivery(final String dateOfDelivery)
	{
		this.dateOfDelivery = dateOfDelivery;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(final String currency) {
		this.currency = currency;
	}
	public String getForeignCurrency() {
		return foreignCurrency;
	}
	public void setForeignCurrency(final String foreignCurrency) {
		this.foreignCurrency = foreignCurrency;
	}
	public String getInvoiceReference() {
		return invoiceReference;
	}
	public void setInvoiceReference(final String invoiceReference) {
		this.invoiceReference = invoiceReference;
	}
	public String getRemarksArabic() {
		return remarksArabic;
	}
	public void setRemarksArabic(final String remarksArabic) {
		this.remarksArabic = remarksArabic;
	}
	public String getRemarksEnglish() {
		return remarksEnglish;
	}
	public void setRemarksEnglish(final String remarksEnglish) {
		this.remarksEnglish = remarksEnglish;
	}
	public String getNarrationArabic() {
		return narrationArabic;
	}
	public void setNarrationArabic(final String narrationArabic) {
		this.narrationArabic = narrationArabic;
	}
	public String getNarrationEnglish() {
		return narrationEnglish;
	}
	public void setNarrationEnglish(final String narrationEnglish) {
		this.narrationEnglish = narrationEnglish;
	}
	public String getShipmentRefNumber() {
		return shipmentRefNumber;
	}
	public void setShipmentRefNumber(final String shipmentRefNumber) {
		this.shipmentRefNumber = shipmentRefNumber;
	}
	public String getSalespersonEnglish() {
		return salespersonEnglish;
	}
	public void setSalespersonEnglish(final String salespersonEnglish) {
		this.salespersonEnglish = salespersonEnglish;
	}
	public String getSalespersonArabic() {
		return salespersonArabic;
	}
	public void setSalespersonArabic(final String salespersonArabic) {
		this.salespersonArabic = salespersonArabic;
	}
	public Boolean getSelfAccountForTax() {
		return selfAccountForTax;
	}
	public void setSelfAccountForTax(final Boolean selfAccountForTax) {
		this.selfAccountForTax = selfAccountForTax;
	}
	public Double getTotalBeforeVAT() {
		return totalBeforeVAT;
	}
	public void setTotalBeforeVAT(final Double totalBeforeVAT) {
		this.totalBeforeVAT = totalBeforeVAT;
	}
	public Double getTotalVAT() {
		return totalVAT;
	}
	public void setTotalVAT(final Double totalVAT) {
		this.totalVAT = totalVAT;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(final Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(final Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public Double getOutstandingAmount() {
		return outstandingAmount;
	}
	public void setOutstandingAmount(final Double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	public Double getPercentOfCompletion() {
		return percentOfCompletion;
	}
	public void setPercentOfCompletion(final Double percentOfCompletion) {
		this.percentOfCompletion = percentOfCompletion;
	}
	public Boolean getMilestonePayments() {
		return milestonePayments;
	}
	public void setMilestonePayments(final Boolean milestonePayments) {
		this.milestonePayments = milestonePayments;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(final Supplier supplier) {
		this.supplier = supplier;
	}
	public ProfitMargin getProfitMargin() {
		return profitMargin;
	}
	public void setProfitMargin(final ProfitMargin profitMargin) {
		this.profitMargin = profitMargin;
	}
	public List<LineItem> getLineItems() {
		return lineItems;
	}
	public void setLineItems(final List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	@Override
	public String toString()
	{
		return "Invoice [invoiceType=" + invoiceType + ", customer=" + customer + ", invoiceNumber=" + invoiceNumber
				+ ", invoiceIssueDate=" + invoiceIssueDate + ", dueDate=" + dueDate + ", dateOfDelivery=" + dateOfDelivery
				+ ", currency=" + currency + ", foreignCurrency=" + foreignCurrency + ", invoiceReference=" + invoiceReference
				+ ", remarksArabic=" + remarksArabic + ", remarksEnglish=" + remarksEnglish + ", narrationArabic=" + narrationArabic
				+ ", narrationEnglish=" + narrationEnglish + ", shipmentRefNumber=" + shipmentRefNumber + ", salespersonEnglish="
				+ salespersonEnglish + ", salespersonArabic=" + salespersonArabic + ", selfAccountForTax=" + selfAccountForTax
				+ ", totalBeforeVAT=" + totalBeforeVAT + ", totalVAT=" + totalVAT + ", grandTotal=" + grandTotal + ", advanceAmount="
				+ advanceAmount + ", outstandingAmount=" + outstandingAmount + ", percentOfCompletion=" + percentOfCompletion
				+ ", milestonePayments=" + milestonePayments + ", supplier=" + supplier + ", profitMargin=" + profitMargin
				+ ", lineItems=" + lineItems + "]";
	}




}
