/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * @author mbaker
 *
 */
public class BillerReconUploadRequest
{

	@SerializedName("prcDt")
	private String prcDt;

	@SerializedName("collectPmtAmt")
	private double collectPaymentAmount;

	@SerializedName("reconPmtAmt")
	private double reconciledPaymentAmount;

	@SerializedName("unReconPmtAmt")
	private double unconciledPaymentAmount;

	@SerializedName("transFees")
	private double transactionFees;

	@SerializedName("pmtBankRec")
	private List<BankPayment> bankPayments;

	/**
	 * @return the prcDt
	 */
	public String getPrcDt()
	{
		return prcDt;
	}

	/**
	 * @param prcDt
	 *           the prcDt to set
	 */
	public void setPrcDt(final String prcDt)
	{
		this.prcDt = prcDt;
	}

	/**
	 * @return the collectPaymentAmount
	 */
	public double getCollectPaymentAmount()
	{
		return collectPaymentAmount;
	}

	/**
	 * @param collectPaymentAmount
	 *           the collectPaymentAmount to set
	 */
	public void setCollectPaymentAmount(final double collectPaymentAmount)
	{
		this.collectPaymentAmount = collectPaymentAmount;
	}

	/**
	 * @return the reconciledPaymentAmount
	 */
	public double getReconciledPaymentAmount()
	{
		return reconciledPaymentAmount;
	}

	/**
	 * @param reconciledPaymentAmount
	 *           the reconciledPaymentAmount to set
	 */
	public void setReconciledPaymentAmount(final double reconciledPaymentAmount)
	{
		this.reconciledPaymentAmount = reconciledPaymentAmount;
	}

	/**
	 * @return the unconciledPaymentAmount
	 */
	public double getUnconciledPaymentAmount()
	{
		return unconciledPaymentAmount;
	}

	/**
	 * @param unconciledPaymentAmount
	 *           the unconciledPaymentAmount to set
	 */
	public void setUnconciledPaymentAmount(final double unconciledPaymentAmount)
	{
		this.unconciledPaymentAmount = unconciledPaymentAmount;
	}

	/**
	 * @return the transactionFees
	 */
	public double getTransactionFees()
	{
		return transactionFees;
	}

	/**
	 * @param transactionFees
	 *           the transactionFees to set
	 */
	public void setTransactionFees(final double transactionFees)
	{
		this.transactionFees = transactionFees;
	}

	/**
	 * @return the bankPayments
	 */
	public List<BankPayment> getBankPayments()
	{
		return bankPayments;
	}

	/**
	 * @param bankPayments
	 *           the bankPayments to set
	 */
	public void setBankPayments(final List<BankPayment> bankPayments)
	{
		this.bankPayments = bankPayments;
	}


}
