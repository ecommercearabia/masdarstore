/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook;

/**
 * @author mbaker
 *
 */
public class BankDetails
{

	private String bankId;

	private String branchCode;

	private String districtCode;

	private String accessChannel;

	/**
	 * @return the bankId
	 */
	public String getBankId()
	{
		return bankId;
	}

	/**
	 * @param bankId
	 *           the bankId to set
	 */
	public void setBankId(final String bankId)
	{
		this.bankId = bankId;
	}

	/**
	 * @return the branchCode
	 */
	public String getBranchCode()
	{
		return branchCode;
	}

	/**
	 * @param branchCode
	 *           the branchCode to set
	 */
	public void setBranchCode(final String branchCode)
	{
		this.branchCode = branchCode;
	}

	/**
	 * @return the districtCode
	 */
	public String getDistrictCode()
	{
		return districtCode;
	}

	/**
	 * @param districtCode
	 *           the districtCode to set
	 */
	public void setDistrictCode(final String districtCode)
	{
		this.districtCode = districtCode;
	}

	/**
	 * @return the accessChannel
	 */
	public String getAccessChannel()
	{
		return accessChannel;
	}

	/**
	 * @param accessChannel
	 *           the accessChannel to set
	 */
	public void setAccessChannel(final String accessChannel)
	{
		this.accessChannel = accessChannel;
	}

}
