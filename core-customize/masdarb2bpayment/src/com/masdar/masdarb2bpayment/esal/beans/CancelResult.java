package com.masdar.masdarb2bpayment.esal.beans;

public class CancelResult {

	private String result;

	private Object errors;

	public Object getErrors() {
		return errors;
	}

	public void setErrors(final Object errors) {
		this.errors = errors;
	}

	public String getResult() {
		return result;
	}

	public void setResult(final String result) {
		this.result = result;
	}

}
