/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.enums.PaymentTransactionType;


/**
 * @author mbaker
 *
 */
public class PaymentTransaction
{
	@SerializedName("pmtId")
	private String paymentId;

	@SerializedName("pmtIdType")
	private PaymentTransactionType paymentIdType;

	/**
	 * @return the paymentId
	 */
	public String getPaymentId()
	{
		return paymentId;
	}

	/**
	 * @param paymentId
	 *           the paymentId to set
	 */
	public void setPaymentId(final String paymentId)
	{
		this.paymentId = paymentId;
	}

	/**
	 * @return the paymentIdType
	 */
	public PaymentTransactionType getPaymentIdType()
	{
		return paymentIdType;
	}

	/**
	 * @param paymentIdType
	 *           the paymentIdType to set
	 */
	public void setPaymentIdType(final PaymentTransactionType paymentIdType)
	{
		this.paymentIdType = paymentIdType;
	}

}
