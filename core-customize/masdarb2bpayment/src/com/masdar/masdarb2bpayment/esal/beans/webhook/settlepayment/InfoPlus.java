/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class InfoPlus
{

	@SerializedName("incPaymentRanges")
	private boolean incPaymentRanges;

	/**
	 * @return the incPaymentRanges
	 */
	public boolean isIncPaymentRanges()
	{
		return incPaymentRanges;
	}

	/**
	 * @param incPaymentRanges
	 *           the incPaymentRanges to set
	 */
	public void setIncPaymentRanges(final boolean incPaymentRanges)
	{
		this.incPaymentRanges = incPaymentRanges;
	}



}
