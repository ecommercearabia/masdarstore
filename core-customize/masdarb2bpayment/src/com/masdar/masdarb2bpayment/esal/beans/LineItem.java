package com.masdar.masdarb2bpayment.esal.beans;

public class LineItem {

	private Integer serialNumber;
	private String code;
	private String descriptionArabic;
	private String descriptionEnglish;
	private String uomArabic;
	private String uomEnglish;
	private Double price;
	private Double foreignPrice;
	private Long quantity;
	private Double amount;
	private Double discountPercent;
	private Double discountAmount;
	private Double amountAfterDiscount;
	private Double vatPercent;
	private Double totalVat;
	private Double priceAfterVat;
	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(final Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCode() {
		return code;
	}
	public void setCode(final String code) {
		this.code = code;
	}
	public String getDescriptionArabic() {
		return descriptionArabic;
	}
	public void setDescriptionArabic(final String descriptionArabic) {
		this.descriptionArabic = descriptionArabic;
	}
	public String getDescriptionEnglish() {
		return descriptionEnglish;
	}
	public void setDescriptionEnglish(final String descriptionEnglish) {
		this.descriptionEnglish = descriptionEnglish;
	}
	public String getUomArabic() {
		return uomArabic;
	}
	public void setUomArabic(final String uomArabic) {
		this.uomArabic = uomArabic;
	}
	public String getUomEnglish() {
		return uomEnglish;
	}
	public void setUomEnglish(final String uomEnglish) {
		this.uomEnglish = uomEnglish;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(final Double price) {
		this.price = price;
	}

	public Double getForeignPrice()
	{
		return foreignPrice;
	}

	public void setForeignPrice(final Double foreignPrice)
	{
		this.foreignPrice = foreignPrice;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(final Double amount) {
		this.amount = amount;
	}
	public Double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(final Double discountPercent) {
		this.discountPercent = discountPercent;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(final Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getAmountAfterDiscount() {
		return amountAfterDiscount;
	}
	public void setAmountAfterDiscount(final Double amountAfterDiscount) {
		this.amountAfterDiscount = amountAfterDiscount;
	}

	/**
	 * @return the vatPercent
	 */
	public Double getVatPercent()
	{
		return vatPercent;
	}

	/**
	 * @param vatPercent
	 *           the vatPercent to set
	 */
	public void setVatPercent(final Double vatPercent)
	{
		this.vatPercent = vatPercent;
	}
	public Double getTotalVat() {
		return totalVat;
	}
	public void setTotalVat(final Double totalVat) {
		this.totalVat = totalVat;
	}
	public Double getPriceAfterVat() {
		return priceAfterVat;
	}
	public void setPriceAfterVat(final Double priceAfterVat) {
		this.priceAfterVat = priceAfterVat;
	}

	@Override
	public String toString()
	{
		return "LineItem [serialNumber=" + serialNumber + ", code=" + code + ", descriptionArabic=" + descriptionArabic
				+ ", descriptionEnglish=" + descriptionEnglish + ", uomArabic=" + uomArabic + ", uomEnglish=" + uomEnglish
				+ ", price=" + price + ", foreignPrice=" + foreignPrice + ", quantity=" + quantity + ", amount=" + amount
				+ ", discountPercent=" + discountPercent + ", discountAmount=" + discountAmount + ", amountAfterDiscount="
				+ amountAfterDiscount + ", vatPercent=" + vatPercent + ", totalVat=" + totalVat + ", priceAfterVat=" + priceAfterVat
				+ "]";
	}

}
