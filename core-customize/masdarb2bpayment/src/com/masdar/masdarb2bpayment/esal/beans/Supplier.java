package com.masdar.masdarb2bpayment.esal.beans;

public class Supplier {

	private String id;
	private String iban;
	private String bankSwiftCode;
	private String regionEnglish;
	private String regionArabic;
	private String cityEnglish;
	private String cityArabic;
	private String localityEnglish;
	private String localityArabic;
	private String branchEnglish;
	private String branchArabic;
	private String plantEnglish;
	private String plantArabic;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(final String iban) {
		this.iban = iban;
	}

	public String getBankSwiftCode() {
		return bankSwiftCode;
	}

	public void setBankSwiftCode(final String bankSwiftCode) {
		this.bankSwiftCode = bankSwiftCode;
	}

	public String getRegionEnglish() {
		return regionEnglish;
	}

	public void setRegionEnglish(final String regionEnglish) {
		this.regionEnglish = regionEnglish;
	}

	public String getRegionArabic() {
		return regionArabic;
	}

	public void setRegionArabic(final String regionArabic) {
		this.regionArabic = regionArabic;
	}

	public String getCityEnglish() {
		return cityEnglish;
	}

	public void setCityEnglish(final String cityEnglish) {
		this.cityEnglish = cityEnglish;
	}

	public String getCityArabic() {
		return cityArabic;
	}

	public void setCityArabic(final String cityArabic) {
		this.cityArabic = cityArabic;
	}

	public String getLocalityEnglish() {
		return localityEnglish;
	}

	public void setLocalityEnglish(final String localityEnglish) {
		this.localityEnglish = localityEnglish;
	}

	public String getLocalityArabic() {
		return localityArabic;
	}

	public void setLocalityArabic(final String localityArabic) {
		this.localityArabic = localityArabic;
	}

	public String getBranchEnglish() {
		return branchEnglish;
	}

	public void setBranchEnglish(final String branchEnglish) {
		this.branchEnglish = branchEnglish;
	}

	public String getBranchArabic() {
		return branchArabic;
	}

	public void setBranchArabic(final String branchArabic) {
		this.branchArabic = branchArabic;
	}

	public String getPlantEnglish() {
		return plantEnglish;
	}

	public void setPlantEnglish(final String plantEnglish) {
		this.plantEnglish = plantEnglish;
	}

	public String getPlantArabic() {
		return plantArabic;
	}

	public void setPlantArabic(final String plantArabic) {
		this.plantArabic = plantArabic;
	}

	@Override
	public String toString()
	{
		return "Supplier [id=" + id + ", iban=" + iban + ", bankSwiftCode=" + bankSwiftCode + ", regionEnglish=" + regionEnglish
				+ ", regionArabic=" + regionArabic + ", cityEnglish=" + cityEnglish + ", cityArabic=" + cityArabic
				+ ", localityEnglish=" + localityEnglish + ", localityArabic=" + localityArabic + ", branchEnglish=" + branchEnglish
				+ ", branchArabic=" + branchArabic + ", plantEnglish=" + plantEnglish + ", plantArabic=" + plantArabic + "]";
	}

}
