package com.masdar.masdarb2bpayment.esal.beans;

public class Customer {

	private String code;
	private String nameArabic;
	private String nameEnglish;
	private String addressArabic1;
	private String addressEnglish1;
	private String addressArabic2;
	private String addressEnglish2;
	private String vatRegistrationNumber;
	private String branch;
	private String email;
	private String regionEnglish;
	private String regionArabic;
	private String cityEnglish;
	private String cityArabic;
	private String localityEnglish;
	private String localityArabic;
	private String branchArabic;
	private String plantEnglish;
	private String plantArabic;
	private String segmentEnglish;
	private String segmentArabic;
	private String divisionEnglish;
	private String divisionArabic;
	private String prodLineEnglish;
	private String prodLineArabic;
	private String contactNo;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getNameArabic() {
		return nameArabic;
	}

	public void setNameArabic(final String nameArabic) {
		this.nameArabic = nameArabic;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(final String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getAddressArabic1() {
		return addressArabic1;
	}

	public void setAddressArabic1(final String addressArabic1) {
		this.addressArabic1 = addressArabic1;
	}

	public String getAddressEnglish1() {
		return addressEnglish1;
	}

	public void setAddressEnglish1(final String addressEnglish1) {
		this.addressEnglish1 = addressEnglish1;
	}

	public String getAddressArabic2() {
		return addressArabic2;
	}

	public void setAddressArabic2(final String addressArabic2) {
		this.addressArabic2 = addressArabic2;
	}

	public String getAddressEnglish2() {
		return addressEnglish2;
	}

	public void setAddressEnglish2(final String addressEnglish2) {
		this.addressEnglish2 = addressEnglish2;
	}

	public String getVatRegisterationNumber() {
		return vatRegistrationNumber;
	}

	public void setVatRegisterationNumber(final String vatRegistrationNumber)
	{
		this.vatRegistrationNumber = vatRegistrationNumber;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(final String branch) {
		this.branch = branch;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getRegionEnglish() {
		return regionEnglish;
	}

	public void setRegionEnglish(final String regionEnglish) {
		this.regionEnglish = regionEnglish;
	}

	public String getRegionArabic() {
		return regionArabic;
	}

	public void setRegionArabic(final String regionArabic) {
		this.regionArabic = regionArabic;
	}

	public String getCityEnglish() {
		return cityEnglish;
	}

	public void setCityEnglish(final String cityEnglish) {
		this.cityEnglish = cityEnglish;
	}

	public String getCityArabic() {
		return cityArabic;
	}

	public void setCityArabic(final String cityArabic) {
		this.cityArabic = cityArabic;
	}

	public String getLocalityEnglish() {
		return localityEnglish;
	}

	public void setLocalityEnglish(final String localityEnglish) {
		this.localityEnglish = localityEnglish;
	}

	public String getLocalityArabic() {
		return localityArabic;
	}

	public void setLocalityArabic(final String localityArabic) {
		this.localityArabic = localityArabic;
	}

	public String getBranchArabic() {
		return branchArabic;
	}

	public void setBranchArabic(final String branchArabic) {
		this.branchArabic = branchArabic;
	}

	public String getPlantEnglish() {
		return plantEnglish;
	}

	public void setPlantEnglish(final String plantEnglish) {
		this.plantEnglish = plantEnglish;
	}

	public String getPlantArabic() {
		return plantArabic;
	}

	public void setPlantArabic(final String plantArabic) {
		this.plantArabic = plantArabic;
	}

	public String getSegmentEnglish() {
		return segmentEnglish;
	}

	public void setSegmentEnglish(final String segmentEnglish) {
		this.segmentEnglish = segmentEnglish;
	}

	public String getSegmentArabic() {
		return segmentArabic;
	}

	public void setSegmentArabic(final String segmentArabic) {
		this.segmentArabic = segmentArabic;
	}

	public String getDivisionEnglish() {
		return divisionEnglish;
	}

	public void setDivisionEnglish(final String divisionEnglish) {
		this.divisionEnglish = divisionEnglish;
	}

	public String getDivisionArabic() {
		return divisionArabic;
	}

	public void setDivisionArabic(final String divisionArabic) {
		this.divisionArabic = divisionArabic;
	}

	public String getProdLineEnglish() {
		return prodLineEnglish;
	}

	public void setProdLineEnglish(final String prodLineEnglish) {
		this.prodLineEnglish = prodLineEnglish;
	}

	public String getProdLineArabic() {
		return prodLineArabic;
	}

	public void setProdLineArabic(final String prodLineArabic) {
		this.prodLineArabic = prodLineArabic;
	}

	/**
	 * @return the vatRegistrationNumber
	 */
	public String getVatRegistrationNumber()
	{
		return vatRegistrationNumber;
	}

	/**
	 * @param vatRegistrationNumber
	 *           the vatRegistrationNumber to set
	 */
	public void setVatRegistrationNumber(final String vatRegistrationNumber)
	{
		this.vatRegistrationNumber = vatRegistrationNumber;
	}

	/**
	 * @return the contactNo
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * @param contactNo
	 *           the contactNo to set
	 */
	public void setContactNo(final String contactNo)
	{
		this.contactNo = contactNo;
	}

	@Override
	public String toString()
	{
		return "Customer [code=" + code + ", nameArabic=" + nameArabic + ", nameEnglish=" + nameEnglish + ", addressArabic1="
				+ addressArabic1 + ", addressEnglish1=" + addressEnglish1 + ", addressArabic2=" + addressArabic2
				+ ", addressEnglish2=" + addressEnglish2 + ", vatRegistrationNumber=" + vatRegistrationNumber + ", branch=" + branch
				+ ", email=" + email + ", regionEnglish=" + regionEnglish + ", regionArabic=" + regionArabic + ", cityEnglish="
				+ cityEnglish + ", cityArabic=" + cityArabic + ", localityEnglish=" + localityEnglish + ", localityArabic="
				+ localityArabic + ", branchArabic=" + branchArabic + ", plantEnglish=" + plantEnglish + ", plantArabic="
				+ plantArabic + ", segmentEnglish=" + segmentEnglish + ", segmentArabic=" + segmentArabic + ", divisionEnglish="
				+ divisionEnglish + ", divisionArabic=" + divisionArabic + ", prodLineEnglish=" + prodLineEnglish
				+ ", prodLineArabic=" + prodLineArabic + ", contactNo=" + contactNo + "]";
	}


}
