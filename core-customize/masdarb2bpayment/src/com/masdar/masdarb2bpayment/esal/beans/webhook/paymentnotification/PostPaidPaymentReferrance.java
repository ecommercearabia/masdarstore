/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification;

import com.google.gson.annotations.SerializedName;
import com.masdar.masdarb2bpayment.esal.beans.webhook.BillAccount;


/**
 * @author mbaker
 *
 */
public class PostPaidPaymentReferrance extends PaymentReferrance
{

	@SerializedName("billNumber")
	private String billNumber;

	@SerializedName("billNumberWithAccount")
	private BillAccount billAccount;

	@SerializedName("billCycle")
	private String billCycle;

	/**
	 * @return the billNumber
	 */
	public String getBillNumber()
	{
		return billNumber;
	}

	/**
	 * @param billNumber the billNumber to set
	 */
	public void setBillNumber(final String billNumber)
	{
		this.billNumber = billNumber;
	}

	/**
	 * @return the billAccount
	 */
	public BillAccount getBillAccount()
	{
		return billAccount;
	}

	/**
	 * @param billAccount the billAccount to set
	 */
	public void setBillAccount(final BillAccount billAccount)
	{
		this.billAccount = billAccount;
	}

	/**
	 * @return the billCycle
	 */
	public String getBillCycle()
	{
		return billCycle;
	}

	/**
	 * @param billCycle the billCycle to set
	 */
	public void setBillCycle(final String billCycle)
	{
		this.billCycle = billCycle;
	}

}
