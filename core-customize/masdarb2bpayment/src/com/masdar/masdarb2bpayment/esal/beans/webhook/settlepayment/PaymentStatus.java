/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class PaymentStatus
{
	@SerializedName("pmtStatusCode")
	private String paymentStatusCode;

	@SerializedName("efftDt")
	private String effectiveDate;

	@SerializedName("status")
	private Status status;

	/**
	 * @return the paymentStatusCode
	 */
	public String getPaymentStatusCode()
	{
		return paymentStatusCode;
	}

	/**
	 * @param paymentStatusCode
	 *           the paymentStatusCode to set
	 */
	public void setPaymentStatusCode(final String paymentStatusCode)
	{
		this.paymentStatusCode = paymentStatusCode;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate()
	{
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *           the effectiveDate to set
	 */
	public void setEffectiveDate(final String effectiveDate)
	{
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the status
	 */
	public Status getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final Status status)
	{
		this.status = status;
	}



}
