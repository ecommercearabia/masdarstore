package com.masdar.masdarb2bpayment.esal.beans;

public class AuthorizationData {

	private Token authorization;

	private Token refresh;

	public Token getAuthorization() {
		return authorization;
	}

	public void setAuthorization(final Token authorization) {
		this.authorization = authorization;
	}

	public Token getRefresh() {
		return refresh;
	}

	public void setRefresh(final Token refresh) {
		this.refresh = refresh;
	}

}


