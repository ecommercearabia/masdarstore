/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.sadad;

import java.util.List;


/**
 * @author mbaker
 *
 */
public class SadadInvoice
{
	private String invoiceNumber;

	private String status;

	private String sadadBillsId;

	private List<Error> errors;

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * @return the sadadBillsId
	 */
	public String getSadadBillsId()
	{
		return sadadBillsId;
	}

	/**
	 * @param sadadBillsId
	 *           the sadadBillsId to set
	 */
	public void setSadadBillsId(final String sadadBillsId)
	{
		this.sadadBillsId = sadadBillsId;
	}

	/**
	 * @return the errors
	 */
	public List<Error> getErrors()
	{
		return errors;
	}

	/**
	 * @param errors
	 *           the errors to set
	 */
	public void setErrors(final List<Error> errors)
	{
		this.errors = errors;
	}


}
