package com.masdar.masdarb2bpayment.esal.beans;

import com.google.gson.annotations.SerializedName;

public class PaymentStatus {

	@SerializedName("invoiceNumber")
	private String invoiceNumber;

	@SerializedName("sadadBillsID")
	private String sadadBillsID;

	@SerializedName("billerId")
	private String billerId;

	@SerializedName("totalPaidAmount")
	private double totalPaidAmount;

	@SerializedName("outstandingAmount")
	private double outstandingAmount;

	@SerializedName("paymentStatus")
	private String paymentStatus;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(final String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSadadBillsID() {
		return sadadBillsID;
	}

	public void setSadadBillsID(final String sadadBillsID) {
		this.sadadBillsID = sadadBillsID;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(final String billerId) {
		this.billerId = billerId;
	}

	public double getTotalPaidAmount() {
		return totalPaidAmount;
	}

	public void setTotalPaidAmount(final double totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	public double getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(final double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(final String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

}
