/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment;

import com.google.gson.annotations.SerializedName;


/**
 * @author mbaker
 *
 */
public class PhoneNumber
{

	@SerializedName("beneficiaryPhoneNum")
	private String beneficiaryPhoneNumber;

	/**
	 * @return the beneficiaryPhoneNumber
	 */
	public String getBeneficiaryPhoneNumber()
	{
		return beneficiaryPhoneNumber;
	}

	/**
	 * @param beneficiaryPhoneNumber
	 *           the beneficiaryPhoneNumber to set
	 */
	public void setBeneficiaryPhoneNumber(final String beneficiaryPhoneNumber)
	{
		this.beneficiaryPhoneNumber = beneficiaryPhoneNumber;
	}



}
