/**
 *
 */
package com.masdar.masdarb2bpayment.esal.beans.webhook;

import com.masdar.masdarb2bpayment.esal.enums.OfficialIdType;


/**
 * @author mbaker
 *
 */
public class Payor
{
	private String officialId;

	private OfficialIdType officialIdType;

	/**
	 * @return the officialId
	 */
	public String getOfficialId()
	{
		return officialId;
	}

	/**
	 * @param officialId
	 *           the officialId to set
	 */
	public void setOfficialId(final String officialId)
	{
		this.officialId = officialId;
	}

	/**
	 * @return the officialIdType
	 */
	public OfficialIdType getOfficialIdType()
	{
		return officialIdType;
	}

	/**
	 * @param officialIdType
	 *           the officialIdType to set
	 */
	public void setOfficialIdType(final OfficialIdType officialIdType)
	{
		this.officialIdType = officialIdType;
	}

}
