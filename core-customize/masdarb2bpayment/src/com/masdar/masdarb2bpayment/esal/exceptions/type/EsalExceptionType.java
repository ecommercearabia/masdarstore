package com.masdar.masdarb2bpayment.esal.exceptions.type;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum EsalExceptionType {

	NOT_AUTHORIZED("401", "not authorized"),
	BAD_REQUET("400", "One or more errors occurred while processing the request"),
	INTERNAL_SERVER_ERROR("500", "Internal Server error"),UNKOWN_EXCEPTION_CODE("0","unkown exception code");

	private final String code;

	private final String msg;

	private static Map<String, EsalExceptionType> map = new HashMap<>();

	static {
		for (final EsalExceptionType esalExceptionType : EsalExceptionType.values()) {
			map.put(esalExceptionType.code, esalExceptionType);
		}
	}

	private EsalExceptionType(final String code, final String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public static Optional<EsalExceptionType> getExceptionFromCode(final String code) {
		return Optional.ofNullable(map.get(code));
	}

}
