package com.masdar.masdarb2bpayment.esal.exceptions;

import com.masdar.masdarb2bpayment.esal.exceptions.type.EsalExceptionType;

public class EsalException extends Exception{

	private final EsalExceptionType type;

	private final Object data;

	public EsalException(final EsalExceptionType type, final String msg, final Object data) {
		super(msg);
		this.type = type;
		this.data = data;
	}

	public EsalExceptionType getType() {
		return type;
	}

	public Object getData() {
		return data;
	}

}
