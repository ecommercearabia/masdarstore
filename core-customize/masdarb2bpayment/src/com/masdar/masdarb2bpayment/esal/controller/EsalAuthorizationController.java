package com.masdar.masdarb2bpayment.esal.controller;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.masdar.masdarb2bpayment.esal.beans.AuthorizationData;
import com.masdar.masdarb2bpayment.esal.constants.ESALConstant;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.services.ESALAuthorizationService;

@RestController
@RequestMapping("/auth")
public class EsalAuthorizationController {

	@Resource(name = "esalAuthorizationService")
	private ESALAuthorizationService esalAuthorizationService;

	@PostMapping
	public Object login(@RequestParam(name = "username", required = true)
	final String username,
			@RequestParam(name = "password", required = true) final String password) {

		Optional<AuthorizationData> token;
		try {
			token = esalAuthorizationService.getToken(ESALConstant.BASE_URL, username, password);
		} catch (final EsalException e) {

			return e.getLocalizedMessage();
		}
		if (!token.isEmpty())
		{
			return token.get();
		}

		return null;
	}

	@PostMapping("/check")
	public boolean checkToken(@RequestParam(name = "token", required = true) final String token) {
		return esalAuthorizationService.isTokenValid(ESALConstant.BASE_URL, token);
	}
}
