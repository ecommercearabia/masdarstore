package com.masdar.masdarb2bpayment.esal.controller;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.masdar.masdarb2bpayment.esal.beans.CancelResult;
import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.PaymentStatus;
import com.masdar.masdarb2bpayment.esal.constants.ESALConstant;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.services.ESALInvoiceService;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/invoices")
public class ESALInvoiceController {

	@Resource(name = "esalInvoiceService")
	private ESALInvoiceService esalInvoiceService;

	@PostMapping
	public Object createInvoice(@RequestParam(name = "username", required = true)
	final String username,
			@RequestParam(name = "password", required = true) final String password,
			@RequestParam(name = "id", required = false) final String id,
			@RequestParam(name = "supplierId", required = true) final String supplierId,
			@ApiParam(name = "Invoice", type = "Invoice", required = true) @RequestBody final Invoice invoice) {
		try {
			esalInvoiceService.createInvoice(ESALConstant.BASE_URL, username, password, id, supplierId, invoice);
			return null;
		} catch (final EsalException e) {
			return e.getMessage();
		}
	}

	@PostMapping("/cancel")
	public Object cancelInvoices(@RequestParam(name = "username", required = true) final String username,
			@RequestParam(name = "password", required = true) final String password,
			@RequestParam(name = "supplierId", required = true) final String supplierId,
			@ApiParam(name = "Invoices", type = "List", required = true) @RequestBody final List<String> invoices) {
		Optional<CancelResult> cancelResult;
		try {
			cancelResult = esalInvoiceService.cancelInvoices(ESALConstant.BASE_URL, username, password, supplierId, invoices);
		} catch (final EsalException e) {
			return e.getMessage();
		}
		return cancelResult.isPresent() ? cancelResult.get() : null;
	}

	@PostMapping("/{invoiceNumber}/cancel")
	public Object cancelInvoice(@RequestParam(name = "username", required = true) final String username,
			@RequestParam(name = "password", required = true) final String password,
			@RequestParam(name = "supplierId", required = true) final String supplierId,
			@PathVariable(name = "invoiceNumber") final String invoiceNumber) {
		Optional<CancelResult> cancelResult = Optional.empty();
		try {
			cancelResult = esalInvoiceService.cancelInvoice(ESALConstant.BASE_URL, username, password, supplierId, invoiceNumber);
		} catch (final EsalException e) {
			return e.getMessage();
		}
		return cancelResult.isPresent() ? cancelResult.get() : null;
	}

	@PostMapping("/{invoiceNumber}/credit-notes")
	public Object createCreditNotes(@RequestParam(name = "username", required = true)
	final String username, @RequestParam(name = "password", required = true)
	final String password, @RequestParam(name = "id", required = false)
	final String id, @RequestParam(name = "supplierId", required = true)
	final String supplierId, @RequestParam(name = "invoiceReferrance", required = true)
	final String invoiceReferrance, @ApiParam(name = "Invoice", type = "Invoice", required = true)
	@RequestBody
	final Invoice invoice)
	{
		try
		{
			esalInvoiceService.createCreditNote(ESALConstant.BASE_URL, username, password, id, supplierId, invoiceReferrance,
					invoice);
			return null;
		}
		catch (final EsalException e) {
			return e.getMessage();
		}
	}
	@GetMapping("/{invoiceNumber}/status")
	public Object getInvoiceStatus(@RequestParam(name = "username", required = true) final String username,
			@RequestParam(name = "password", required = true) final String password,
			@PathVariable(name = "invoiceNumber") final String invoiceNumber) {
		Optional<PaymentStatus> paymentStatus;
		try {
			paymentStatus = esalInvoiceService.getPaymentStatus(ESALConstant.BASE_URL, username, password, invoiceNumber);
		} catch (final EsalException e) {
			return ((Exception) (e.getData())).getMessage();
		}

		return paymentStatus.isPresent() ? paymentStatus.get() : null;
	}
}
