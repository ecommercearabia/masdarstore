package com.masdar.masdarb2bpayment.esal.services.impl;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Optional;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.exceptions.type.EsalExceptionType;
import com.masdar.masdarb2bpayment.esal.services.ESALWebApiService;


public class DefaultESALWebApiService implements ESALWebApiService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultESALWebApiService.class);

	private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	private static final String AUTHORIZATION_TYPE = "Bearer ";

	@Override
	public <T, K> Object httpPOST(final String url, final K body, final String token, final Class<T> clazz) throws EsalException
	{
		LOG.info("start httpPost method");
		MultiValueMap<String, String> authHeader = null;

		if (token != null)
		{
			authHeader = getAuthHeader(token);
		}

		final HttpEntity<K> bodyAndHeaders = new HttpEntity<>(body, authHeader);
		ResponseEntity<?> response = null;
		try
		{
			LOG.info("httpPost: send request");
			final HttpComponentsClientHttpRequestFactory requestFactory = getRequestFactory();
			final RestTemplate restTemplate = new RestTemplate(requestFactory);
			response = restTemplate.postForEntity(url, bodyAndHeaders, clazz);
			logData(url, body, token, response, authHeader);

			if (response.getStatusCodeValue() >= 200 && response.getStatusCodeValue() < 300)
			{
				return response.getBody();
			}
		}
		catch (final Exception ex)
		{
			logData(url, body, token, response, authHeader);
			LOG.error("httpPost: exception {}", ex.getMessage());
			error(ex);
		}
		return null;
	}

	/**
	 * @param <K>
	 *
	 */
	private <K> void logData(final String url, final K body, final String token, final ResponseEntity<?> response,
			final MultiValueMap<String, String> authHeader)
	{
		final Gson gson = new Gson();
		LOG.info("Sending Request...");
		final String bodyStr = (body instanceof String) ? (String) body : gson.toJson(body);
		LOG.info("Request:\n url = {}, requestBody = {}, requestHeader = {}, response = {},token = {}", url, bodyStr, authHeader,
				Objects.nonNull(response) ? gson.toJson(response.getBody()) : "", token);


	}

	protected void error(final Exception ex) throws EsalException
	{
		Optional<EsalExceptionType> exceptionFromCode = Optional.empty();

		if (ex instanceof RestClientResponseException)
		{
			exceptionFromCode = EsalExceptionType
					.getExceptionFromCode(String.valueOf(((RestClientResponseException) ex).getRawStatusCode()));
		}



		if (exceptionFromCode.isPresent())
		{
			throw new EsalException(exceptionFromCode.get(), exceptionFromCode.get().getMsg(), ex);
		}

		throw new EsalException(EsalExceptionType.UNKOWN_EXCEPTION_CODE, EsalExceptionType.UNKOWN_EXCEPTION_CODE.getMsg(), ex);
	}

	protected MultiValueMap<String, String> getAuthHeader(final String token)
	{
		final MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
		header.add(AUTHORIZATION_HEADER_KEY, AUTHORIZATION_TYPE.concat(token));
		return header;
	}

	@Override
	public <T> Object httpGet(final String url, final MultiValueMap<String, String> params, final String token,
			final Class<T> clazz) throws EsalException
	{
		LOG.info("start httpGet method");
		MultiValueMap<String, String> authHeader = null;
		if (token != null)
		{
			authHeader = getAuthHeader(token);
		}

		final HttpEntity<Object> headers = new HttpEntity<>(authHeader);
		final UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).queryParams(params).build();

		ResponseEntity<?> response = null;
		try
		{
			LOG.info("httpGet: send request");
			final HttpComponentsClientHttpRequestFactory requestFactory = getRequestFactory();
			final RestTemplate restTemplate = new RestTemplate(requestFactory);
			response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, headers, clazz);
			if (response.getStatusCodeValue() >= 200 && response.getStatusCodeValue() < 300)
			{
				return response.getBody();
			}
		}
		catch (HttpClientErrorException | HttpServerErrorException | KeyManagementException | NoSuchAlgorithmException
				| KeyStoreException ex)
		{
			LOG.error("httpGet: exception {}", ex);
			error(ex);
		}
		return null;
	}

	protected HttpComponentsClientHttpRequestFactory getRequestFactory()
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException
	{
		final TrustStrategy acceptingTrustStrategy = (final X509Certificate[] chain, final String authType) -> true;

		final SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
				.build();

		final SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		final CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();

		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);

		return requestFactory;
	}
}
