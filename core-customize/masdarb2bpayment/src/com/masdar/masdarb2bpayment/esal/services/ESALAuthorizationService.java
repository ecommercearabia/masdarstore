package com.masdar.masdarb2bpayment.esal.services;

import java.util.Optional;

import com.masdar.masdarb2bpayment.esal.beans.AuthorizationData;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;

public interface ESALAuthorizationService {

	public Optional<AuthorizationData> getToken(String baseURL, String username, String password) throws EsalException;

	public boolean isTokenValid(String baseURL, String token);

}
