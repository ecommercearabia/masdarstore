/**
*
*/
package com.masdar.masdarb2bpayment.esal.services.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.enums.WebServiceRequestStatus;
import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification.PaymentNotification;
import com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification.PaymentNotificationRequest;
import com.masdar.masdarb2bpayment.esal.beans.webhook.sadad.SadadBillIdRequest;
import com.masdar.masdarb2bpayment.esal.beans.webhook.sadad.SadadInvoice;
import com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment.SettlementReportRequest;
import com.masdar.masdarb2bpayment.esal.services.EsalClientService;
import com.masdar.masdarb2bpayment.service.PaymentB2BOrderService;
import com.masdar.masdarb2bpayment.service.PaymentB2BWebServiceRecordService;




/**
 * @author mbaker
 *
 */
public class DefaultEsalClientService implements EsalClientService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultEsalClientService.class);
	private static final String INVALID_REQUEST_MSG = "invalid request";
	private static final String INVALID_INVOICE_MSG = "invalid invoice";
	private static final String INVALID_PAYMENT_NOTIFICATION_MSG = "invalid payment notifictaion";
	private static final String INVALID_SADAD_BILL_ID_MSG = "invalid sadad bill Id";
	private static final String UNKOWN_ERROR_MSG = "unkown error";
	private static final String INVALID_AMOUNT_MSG = "invalid amount";
	private static final String SADAD_BILL_ID_ALREADY_EXISTS_MSG = "sadad bill has already assigned to another order.";


	@Resource(name = "paymentB2BOrderService")
	private PaymentB2BOrderService paymentB2BOrderService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "paymentB2BWebServiceRecordService")
	private PaymentB2BWebServiceRecordService paymentB2BWebServiceRecordService;


	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;

	@Override
	public void saveSadadBillId(final SadadBillIdRequest sadadBillIdRequest)
	{
		if (Objects.isNull(sadadBillIdRequest) || Collections.isEmpty(sadadBillIdRequest.getInvoices()))
		{
			paymentB2BWebServiceRecordService.addWebhookRecord(sadadBillIdRequest, WebServiceRequestStatus.FAILED,
					INVALID_REQUEST_MSG, null);

			throw new IllegalArgumentException(INVALID_REQUEST_MSG);
		}

		final List<SadadInvoice> invoices = sadadBillIdRequest.getInvoices();

		for (final SadadInvoice invoice : invoices)
		{

			final Optional<OrderModel> orderByCode = paymentB2BOrderService.getOrderByInvoiceId(invoice.getInvoiceNumber());
			if (orderByCode.isEmpty())
			{
				continue;
			}
			final OrderModel orderModel = orderByCode.get();
			paymentB2BWebServiceRecordService.addWebhookRecord(sadadBillIdRequest, WebServiceRequestStatus.SUCCESS, null,
					orderModel);
			paymentB2BContext.updatePaymentStatus(orderModel);
		}

	}

	@Override
	public void updatePayment(final PaymentNotificationRequest paymentNotificationRequest)
	{
		if (Objects.isNull(paymentNotificationRequest) || Objects.isNull(paymentNotificationRequest.getPayment()))
		{
			paymentB2BWebServiceRecordService.addWebhookRecord(paymentNotificationRequest, WebServiceRequestStatus.FAILED,
					INVALID_REQUEST_MSG, null);
			throw new IllegalArgumentException(INVALID_PAYMENT_NOTIFICATION_MSG);

		}

		final PaymentNotification payment = paymentNotificationRequest.getPayment();
		if (Strings.isBlank(payment.getPmtRefInfo()))
		{
			paymentB2BWebServiceRecordService.addWebhookRecord(paymentNotificationRequest, WebServiceRequestStatus.FAILED,
					INVALID_SADAD_BILL_ID_MSG, null);
			throw new IllegalArgumentException(INVALID_SADAD_BILL_ID_MSG);
		}

		final String invoiceId = payment.getPmtRefInfo();
		final Optional<OrderModel> orderBySadadBillId = paymentB2BOrderService.getOrderByInvoiceId(invoiceId);
		if (orderBySadadBillId.isEmpty())
		{
			paymentB2BWebServiceRecordService.addWebhookRecord(paymentNotificationRequest, WebServiceRequestStatus.FAILED,
					INVALID_SADAD_BILL_ID_MSG, null);
			return;
		}

		final OrderModel orderModel = orderBySadadBillId.get();
		paymentB2BWebServiceRecordService.addWebhookRecord(paymentNotificationRequest, WebServiceRequestStatus.SUCCESS, null,
				orderModel);
		paymentB2BContext.updatePaymentStatus(orderModel);
	}



	@Override
	public void settlementReport(final SettlementReportRequest settlementReportRequest)
	{
		paymentB2BWebServiceRecordService.addWebhookRecord(settlementReportRequest, WebServiceRequestStatus.SUCCESS, null, null);
	}

}
