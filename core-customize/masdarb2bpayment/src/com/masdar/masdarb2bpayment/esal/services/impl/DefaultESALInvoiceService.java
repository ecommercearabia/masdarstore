package com.masdar.masdarb2bpayment.esal.services.impl;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;
import com.masdar.masdarb2bpayment.esal.beans.AuthorizationData;
import com.masdar.masdarb2bpayment.esal.beans.CancelResult;
import com.masdar.masdarb2bpayment.esal.beans.Customer;
import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.LineItem;
import com.masdar.masdarb2bpayment.esal.beans.PaymentStatus;
import com.masdar.masdarb2bpayment.esal.enums.InvoiceType;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.services.ESALAuthorizationService;
import com.masdar.masdarb2bpayment.esal.services.ESALInvoiceService;
import com.masdar.masdarb2bpayment.esal.services.ESALWebApiService;
import com.masdar.masdarb2bpayment.service.PaymentB2BWebServiceRecordService;


public class DefaultESALInvoiceService implements ESALInvoiceService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultESALInvoiceService.class);


	private final String INVOICE_ID_KEY = "id";
	private final String INVOICE_SUPPLIER_ID_KEY = "supplierId";
	private final String INVOICES_KEY = "invoices";
	private final String INVOICE_NUMBER_KEY = "invoiceNumber";
	private final String CREATE_INVOICE_PATH = "v2/uploadInvoices";
	private final String CANCEL_INVOICE_PATH = "v1/invoice/cancel";
	private final String STATUS_INVOICE_PATH = "v1/paymentStatus?";
	private final String SUPPLIER_ID_ERROR_MSG = "supplierId  is null";

	@Resource(name = "esalWebApiService")
	private ESALWebApiService esalWebApiService;

	@Resource(name = "esalAuthorizationService")
	private ESALAuthorizationService esalAuthorizationService;

	@Resource(name = "paymentB2BWebServiceRecordService")
	private PaymentB2BWebServiceRecordService paymentB2BWebServiceRecordService;

	@Override
	public String createInvoice(final String baseURL, final String username, final String password, final String id,
			final String supplierId, final Invoice invoice) throws EsalException
	{
		createInvoices(baseURL, username, password, id, supplierId, List.of(invoice));
		return invoice.getInvoiceNumber();
	}

	@Override
	public void createInvoices(final String baseURL, final String username, final String password, final String id,
			final String supplierId, final List<Invoice> invoices) throws EsalException
	{
		validate(invoices);
		if (Strings.isBlank(supplierId))
		{
			throw new IllegalArgumentException(SUPPLIER_ID_ERROR_MSG);
		}

		validate(baseURL, username, password);

		final String token = getValidToken(baseURL, username, password);

		final Map<String, Object> data = new LinkedHashMap<>();
		data.put(INVOICE_ID_KEY, id);
		data.put(INVOICE_SUPPLIER_ID_KEY, supplierId);
		data.put(INVOICES_KEY, invoices);
		final String url = baseURL + CREATE_INVOICE_PATH;

		esalWebApiService.httpPOST(url, data, token, Void.class);


	}

	@Override
	public void createCreditNote(final String baseURL, final String username, final String password, final String id,
			final String supplierId, final String invoiceReferrance, final Invoice invoice) throws EsalException
	{
		validate(invoice);
		if (Strings.isBlank(invoiceReferrance))
		{
			throw new IllegalArgumentException("invoice referrance is null or empty");
		}
		if (Strings.isBlank(supplierId))
		{
			throw new IllegalArgumentException(SUPPLIER_ID_ERROR_MSG);
		}

		invoice.setInvoiceReference(invoiceReferrance);
		invoice.setInvoiceType(InvoiceType.CREDIT_NOTE.getCode());

		createInvoices(baseURL, username, password, id, supplierId, List.of(invoice));

	}

	/**
	 *
	 */
	private void validate(final String baseURL, final String username, final String password)
	{
		if (Strings.isBlank(baseURL))
		{
			throw new IllegalArgumentException("baseURL is null");
		}

		if (Strings.isBlank(username))
		{
			throw new IllegalArgumentException("username is null");
		}

		if (Strings.isBlank(password))
		{
			throw new IllegalArgumentException("password is null");
		}

	}

	@Override
	public Optional<CancelResult> cancelInvoice(final String baseURL, final String username, final String password,
			final String supplierId, final String invoiceNumbers) throws EsalException
	{
		return cancelInvoices(baseURL, username, password, supplierId, List.of(invoiceNumbers));
	}

	@Override
	public Optional<CancelResult> cancelInvoices(final String baseURL, final String username, final String password,
			final String supplierId, final List<String> invoiceNumbers) throws EsalException
	{
		if (Strings.isBlank(supplierId))
		{
			LOG.error(SUPPLIER_ID_ERROR_MSG);
			throw new IllegalArgumentException(SUPPLIER_ID_ERROR_MSG);
		}
		validate(baseURL, username, password);
		final String token = getValidToken(baseURL, username, password);
		final List<Invoice> invoices = getInvoices(invoiceNumbers);
		final Map<String, Object> data = new LinkedHashMap<>();
		data.put(INVOICE_SUPPLIER_ID_KEY, supplierId);
		data.put(INVOICES_KEY, invoices);

		final String url = baseURL + CANCEL_INVOICE_PATH;
		final CancelResult result = (CancelResult) esalWebApiService.httpPOST(url, data, token, CancelResult.class);
		return Optional.of(result);

	}

	@Override
	public Optional<PaymentStatus> getPaymentStatus(final String baseURL, final String username, final String password,
			final String invoiceNumber) throws EsalException
	{
		if (Strings.isBlank(invoiceNumber))
		{
			LOG.error("invoiceNumber is empty");
			throw new IllegalArgumentException("invoiceNumber is empty");
		}
		validate(baseURL, username, password);
		final String token = getValidToken(baseURL, username, password);
		final String url = baseURL + STATUS_INVOICE_PATH;
		final MultiValueMap<String, String> data = new LinkedMultiValueMap<>();
		data.add(INVOICE_NUMBER_KEY, invoiceNumber);

		final List<Object> result = (List<Object>) esalWebApiService.httpGet(url, data, token, List.class);
		if (CollectionUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final Object object = result.get(0);
		final PaymentStatus invoicePaymentStatus = new Gson().fromJson(object.toString(), PaymentStatus.class);
		return Optional.ofNullable(invoicePaymentStatus);
	}

	private void validate(final List<Invoice> invoices)
	{
		if (CollectionUtils.isEmpty(invoices))
		{
			LOG.error("invoices list is empty");
			throw new IllegalArgumentException("invoices list is empty");
		}
		for (final Invoice invoice : invoices)
		{
			validate(invoice);
		}

	}

	private List<Invoice> getInvoices(final List<String> invoiceNumbers)
	{
		if (CollectionUtils.isEmpty(invoiceNumbers))
		{
			LOG.error("invoiceNumbers list is empty");
			throw new IllegalArgumentException("invoiceNumbers list is empty");
		}

		final List<Invoice> invoices = new LinkedList<>();

		for (final String invoiceNumber : invoiceNumbers)
		{
			final Invoice invoice = new Invoice();
			invoice.setInvoiceNumber(invoiceNumber);
			invoices.add(invoice);
		}

		return invoices;
	}

	protected void validate(final Invoice invoice)
	{
		if (Objects.isNull(invoice))
		{
			LOG.error("invoice is null");
			throw new IllegalArgumentException("invoice is null");
		}
		if (Strings.isBlank(invoice.getInvoiceNumber()))
		{
			LOG.error("invoice number is null");
			throw new IllegalArgumentException("invoice number is null");
		}
		if (Objects.isNull(invoice.getInvoiceIssueDate()))
		{
			LOG.error("invoice issue date is null");
			throw new IllegalArgumentException("invoice issue date is null");
		}
		if (Objects.isNull(invoice.getDueDate()))
		{
			LOG.error("invoice due date is null");
			throw new IllegalArgumentException("invoice due date is null");
		}
		if (Strings.isBlank(invoice.getCurrency()))
		{
			LOG.error("invoice currency is null");
			throw new IllegalArgumentException("invoice currency is null");
		}
		if (Objects.isNull(invoice.getTotalBeforeVAT()))
		{
			LOG.error("invoice total before VAT is null");
			throw new IllegalArgumentException("invoice total before VAT is null");
		}
		if (Objects.isNull(invoice.getTotalVAT()))
		{
			LOG.error("invoice total vat is null");
			throw new IllegalArgumentException("invoice total vat is null");
		}
		if (Objects.isNull(invoice.getGrandTotal()))
		{
			LOG.error("invoice grand total is null");
			throw new IllegalArgumentException("invoice grand total is null");
		}
		//
		if (Objects.isNull(invoice.getInvoiceType()))
		{
			invoice.setInvoiceType(InvoiceType.INVOICE.getCode());
		}
		validate(invoice.getCustomer());
		validate(invoice.getLineItems(), invoice.getForeignCurrency());

	}

	protected void validate(final List<LineItem> lineItems, final String foreignCurrency)
	{
		if (CollectionUtils.isEmpty(lineItems))
		{
			LOG.error("Line items is null or empty");
			throw new IllegalArgumentException("Line items is null or empty");
		}
		int serial = 1;
		for (final LineItem it : lineItems)
		{
			it.setSerialNumber(serial++);
			if (Objects.isNull(it.getCode()))
			{
				LOG.error("lineItem code is null");
				throw new IllegalArgumentException("lineItem code is null");
			}
			if (Strings.isBlank(it.getDescriptionArabic()))
			{
				LOG.error("lineItem arabic description is null");
				throw new IllegalArgumentException("lineItem arabic description is null");
			}

			if (Strings.isBlank(it.getUomArabic()))
			{
				LOG.error("lineItem arabic unit of measure is null");
				throw new IllegalArgumentException("lineItem arabic unit of measure is null");
			}
			if (Strings.isBlank(it.getUomEnglish()))
			{
				LOG.error("lineItem english unit of measure is null");
				throw new IllegalArgumentException("lineItem english unit of measure is null");
			}
			if (Objects.isNull(it.getPrice()))
			{
				LOG.error("lineItem price is null");
				throw new IllegalArgumentException("lineItem price is null");
			}
			if (!Strings.isBlank(foreignCurrency) && Objects.isNull(it.getForeignPrice()))
			{
				LOG.error("foreignCurrency quantity is null");
				throw new IllegalArgumentException("foreignCurrency quantity is null");
			}
			if (Objects.isNull(it.getQuantity()))
			{
				LOG.error("lineItem quantity is null");
				throw new IllegalArgumentException("lineItem quantity is null");
			}
			if (Objects.isNull(it.getAmount()))
			{
				LOG.error("lineItem amount is null");
				throw new IllegalArgumentException("lineItem amount is null");
			}
			if (Objects.isNull(it.getDiscountPercent()))
			{
				LOG.error("lineItem discount percent is null");
				throw new IllegalArgumentException("lineItem discount percent is null");
			}
			if (Objects.isNull(it.getDiscountAmount()))
			{
				LOG.error("lineItem discount amount is null");
				throw new IllegalArgumentException("lineItem discount amount is null");
			}

			if (Objects.isNull(it.getAmountAfterDiscount()))
			{
				LOG.error("lineItem amount after discount is null");
				throw new IllegalArgumentException("lineItem amount after discount is null");
			}
			if (Objects.isNull(it.getVatPercent()))
			{
				LOG.error("lineItem vat percent is null");
				throw new IllegalArgumentException("lineItem vat percent is null");
			}
			if (Objects.isNull(it.getTotalVat()))
			{
				LOG.error("lineItem total vat is null");
				throw new IllegalArgumentException("lineItem total vat is null");
			}
			if (Objects.isNull(it.getPriceAfterVat()))
			{
				LOG.error("lineItem price after vat is null");
				throw new IllegalArgumentException("lineItem price after vat is null");
			}
		}

	}

	protected void validate(final Customer customer)
	{
		if (Objects.isNull(customer))
		{
			LOG.error("customer is null");
			throw new IllegalArgumentException("customer is null");
		}
		if (Strings.isBlank(customer.getNameArabic()))
		{
			LOG.error("customer arabic name is empty");
			throw new IllegalArgumentException("customer arabic name is empty");
		}
		if (Strings.isBlank(customer.getAddressArabic1()))
		{
			LOG.error("customer arabic address 1 is empty");
			throw new IllegalArgumentException("customer arabic address 1 is empty");
		}
		if (Strings.isBlank(customer.getEmail()))
		{
			LOG.error("customer email is null");
			throw new IllegalArgumentException("customer email is empty");
		}

		if (Strings.isBlank(customer.getContactNo()))
		{
			LOG.error("customer contact number is empty");
			throw new IllegalArgumentException("customer contact number is empty");
		}

	}

	protected String getValidToken(final String baseURL, final String username, final String password) throws EsalException
	{
		Optional<AuthorizationData> authData = Optional.empty();
		try
		{
			authData = esalAuthorizationService.getToken(baseURL, username, password);
		}
		catch (final EsalException e)
		{
			LOG.error(e.getMessage(), e);
			throw e;
		}
		if (authData.isEmpty())
		{
			LOG.info("authorization data is empty");
			return null;
		}
		final String token = authData.get().getAuthorization().getToken();
		if (esalAuthorizationService.isTokenValid(baseURL, token))
		{
			/* LOG.info("token {} is valid", token); */
			return token;
		}
		return null;
	}

}
