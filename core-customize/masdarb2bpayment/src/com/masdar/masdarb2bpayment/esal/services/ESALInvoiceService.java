package com.masdar.masdarb2bpayment.esal.services;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarb2bpayment.esal.beans.CancelResult;
import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.PaymentStatus;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;

public interface ESALInvoiceService {

	public String createInvoice(String baseURL, String username, String password, String id, String supplierId, Invoice invoice)
			throws EsalException;

	public void createInvoices(String baseURL, String username, String password, String id, String supplierId,
			List<Invoice> invoices)
			throws EsalException;

	public void createCreditNote(String baseURL, String username, String password, String id, String supplierId,
			String invoiceReferrance,
			Invoice invoice) throws EsalException;


	public Optional<CancelResult> cancelInvoice(String baseURL, String username, String password, String supplierId,
			String invoiceNumbers) throws EsalException;

	public Optional<CancelResult> cancelInvoices(String baseURL, String username, String password, String supplierId,
			List<String> invoiceNumbers) throws EsalException;

	public Optional<PaymentStatus> getPaymentStatus(String baseURL, String username, String password, String invoiceNumbers)
			throws EsalException;

}
