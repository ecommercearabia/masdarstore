package com.masdar.masdarb2bpayment.esal.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.masdar.masdarb2bpayment.esal.beans.AuthorizationData;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.exceptions.type.EsalExceptionType;
import com.masdar.masdarb2bpayment.esal.services.ESALAuthorizationService;
import com.masdar.masdarb2bpayment.esal.services.ESALWebApiService;


@Service
public class DefaultESALAuthorizationService implements ESALAuthorizationService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultESALAuthorizationService.class);

	@Resource(name = "esalWebApiService")
	private ESALWebApiService esalWebApiService;

	private static final String USER_NAME_KEY = "username";
	private static final String PASSWORD_KEY = "password";
	private static final String CHECK_TOKEN_KEY = "token=";
	private static final String BASE_LINK_ERROR_MSG = "baseURL is null or empty";
	private static final String LOGIN_PATH = "cas/login";
	private static final String CHECK_TOKEN_PATH = "cas-token/check";

	@Override
	public Optional<AuthorizationData> getToken(final String baseURL, final String username, final String password)
			throws EsalException
	{
		LOG.info("start getToken method");
		if (Strings.isBlank(username))
		{
			throw new IllegalArgumentException("username is null or empty");
		}
		if (Strings.isBlank(password))
		{
			throw new IllegalArgumentException("password is null or empty");
		}
		if (Strings.isBlank(baseURL))
		{
			throw new IllegalArgumentException(BASE_LINK_ERROR_MSG);
		}

		LOG.info("getToken: prepare params");
		final Map<String, Object> data = new HashMap<>();
		data.put(USER_NAME_KEY, username);
		data.put(PASSWORD_KEY, password);
		final String url = baseURL + LOGIN_PATH;
		final AuthorizationData authData = (AuthorizationData) esalWebApiService.httpPOST(url, data, null, AuthorizationData.class);
		return Optional.ofNullable(authData);
	}

	@Override
	public boolean isTokenValid(final String baseURL, final String token)
	{
		LOG.info("start isTokenValid method");
		if (Strings.isBlank(token))
		{
			throw new IllegalArgumentException("token is null or empty");
		}
		if (Strings.isBlank(baseURL))
		{
			throw new IllegalArgumentException(BASE_LINK_ERROR_MSG);
		}

		final String url = baseURL + CHECK_TOKEN_PATH;
		final String data = CHECK_TOKEN_KEY.concat(token);
		try
		{
			esalWebApiService.httpPOST(url, data, null, String.class);
			return true;
		}
		catch (final EsalException ex)
		{
			LOG.error("isTokenValid: error with ex = {}", ex);
			if (ex.getType() == EsalExceptionType.NOT_AUTHORIZED)
			{
				return false;
			}
		}
		return false;
	}



}
