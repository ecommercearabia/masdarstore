package com.masdar.masdarb2bpayment.esal.services;

import org.springframework.util.MultiValueMap;

import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;

public interface ESALWebApiService {

	public <T,K> Object httpPOST(String url, K body, String token, Class<T> clazz) throws EsalException;

	public <T> Object httpGet(String url, MultiValueMap<String, String> params, String token, Class<T> clazz) throws EsalException;

}
