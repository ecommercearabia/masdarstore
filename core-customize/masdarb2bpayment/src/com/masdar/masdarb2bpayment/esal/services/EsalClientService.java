/**
 *
 */
package com.masdar.masdarb2bpayment.esal.services;

import com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification.PaymentNotificationRequest;
import com.masdar.masdarb2bpayment.esal.beans.webhook.sadad.SadadBillIdRequest;
import com.masdar.masdarb2bpayment.esal.beans.webhook.settlepayment.SettlementReportRequest;



/**
 * @author mbaker
 *
 */
public interface EsalClientService
{

	public void saveSadadBillId(SadadBillIdRequest sadadBillIdRequest);

	public void updatePayment(PaymentNotificationRequest paymentNotificationRequest);

	public void settlementReport(SettlementReportRequest settlementReportRequest);

}
