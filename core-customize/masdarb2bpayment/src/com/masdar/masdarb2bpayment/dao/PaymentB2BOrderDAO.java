/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.dao;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;


/**
 *
 */
public interface PaymentB2BOrderDAO
{
	public Optional<OrderModel> findByInvoiceId(String invoiceId);

	public Optional<OrderModel> findBySadadBillId(String sadadBillId);

	public List<OrderModel> findByPaymentProviderStatusAndPaymentProviderTransactionType(final PaymentProviderStatus status,
			final PaymentProviderTransactionType paymentProviderTransactionType);

	public List<AbstractOrderModel> findByPaymentProviderStatus(PaymentProviderStatus status);

	public List<AbstractOrderModel> findByStatusAndPaymentB2BInvoiceCreatedSuccessfully(OrderStatus status,
			boolean paymentB2BInvoiceCreatedSuccessfully);




}
