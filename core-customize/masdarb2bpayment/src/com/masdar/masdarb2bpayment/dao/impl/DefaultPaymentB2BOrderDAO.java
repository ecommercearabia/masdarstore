/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.dao.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.masdarb2bpayment.dao.PaymentB2BOrderDAO;
import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;


/**
 *
 */
public class DefaultPaymentB2BOrderDAO implements PaymentB2BOrderDAO
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The base select query. */
	private static final String BASE_SELECT_QUERY = "SELECT {p:" + OrderModel.PK + "} " + "FROM {" + OrderModel._TYPECODE
			+ " AS p} ";

	/** The code key. */
	private static final String ORDER_STATUS_KEY = "status";

	private static final String PAYMENT_PROVIDER_STATUS_KEY = "paymentProviderStatus";

	private static final String PAYMENT_B2B_INVOICE_CREATED_SUCCESSFULLY = "paymentB2BInvoiceCreatedSuccessfully";

	private static final String PAYMENT_B2B_KEY = "paymentB2bInvoiceId";

	private static final String SADAD_BILL_ID_KEY = "sadadBillId";

	private static final String PAYMENT_PROVIDER_TRANSACTION_TYPE_KEY = "paymentProviderTransactionType";


	@Override
	public List<AbstractOrderModel> findByPaymentProviderStatus(final PaymentProviderStatus status)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.PAYMENTPROVIDERSTATUS + "}=?"
				+ PAYMENT_PROVIDER_STATUS_KEY + " AND {p:" + OrderModel.ORIGINALVERSION + "} IS NULL ";
		;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(PAYMENT_PROVIDER_STATUS_KEY, status);
		return flexibleSearchService.<AbstractOrderModel> search(query).getResult();
	}


	@Override
	public List<AbstractOrderModel> findByStatusAndPaymentB2BInvoiceCreatedSuccessfully(final OrderStatus status,
			final boolean paymentB2BInvoiceCreatedSuccessfully)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.STATUS + "}=?" + ORDER_STATUS_KEY + " AND {p:"
				+ OrderModel.PAYMENTB2BINVOICECREATEDSUCCESSFULLY + "}=?" + PAYMENT_B2B_INVOICE_CREATED_SUCCESSFULLY + " AND {p:"
				+ OrderModel.ORIGINALVERSION + "} IS NULL ";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(ORDER_STATUS_KEY, status);
		query.addQueryParameter(PAYMENT_B2B_INVOICE_CREATED_SUCCESSFULLY, paymentB2BInvoiceCreatedSuccessfully);
		return flexibleSearchService.<AbstractOrderModel> search(query).getResult();
	}


	@Override
	public Optional<OrderModel> findByInvoiceId(final String invoiceId)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.PAYMENTB2BINVOICEID + "}=?" + PAYMENT_B2B_KEY
				+ " AND {p:" + OrderModel.ORIGINALVERSION + "} IS NULL ";
		;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(PAYMENT_B2B_KEY, invoiceId);
		final List<OrderModel> result = flexibleSearchService.<OrderModel> search(query).getResult();
		return Optional.ofNullable(CollectionUtils.isEmpty(result) ? null : result.get(0));

	}

	@Override
	public Optional<OrderModel> findBySadadBillId(final String sadadBillId)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.SADADBILLID + "}=?" + SADAD_BILL_ID_KEY
				+ " AND {p:" + OrderModel.ORIGINALVERSION + "} IS NULL ";
		;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(SADAD_BILL_ID_KEY, sadadBillId);
		final List<OrderModel> result = flexibleSearchService.<OrderModel> search(query).getResult();
		return Optional.ofNullable(CollectionUtils.isEmpty(result) ? null : result.get(0));
	}

	@Override
	public List<OrderModel> findByPaymentProviderStatusAndPaymentProviderTransactionType(final PaymentProviderStatus status,
			final PaymentProviderTransactionType paymentProviderTransactionType)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.PAYMENTPROVIDERSTATUS + "}=?"
				+ PAYMENT_PROVIDER_STATUS_KEY + " AND {p:" + OrderModel.PAYMENTPROVIDERTRANSACTIONTYPE + "}=?"
				+ PAYMENT_PROVIDER_TRANSACTION_TYPE_KEY + " AND {p:" + OrderModel.ORIGINALVERSION + "} IS NULL ";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(PAYMENT_PROVIDER_STATUS_KEY, status);
		query.addQueryParameter(PAYMENT_PROVIDER_TRANSACTION_TYPE_KEY, paymentProviderTransactionType);
		return flexibleSearchService.<OrderModel> search(query).getResult();
	}

}
