/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.dao.impl;

import com.masdar.masdarPaymentB2Cprovider.model.EsalPaymentB2CProviderModel;
import com.masdar.masdarb2bpayment.dao.PaymentB2BProviderDAO;


/**
 * The Class DefaultEsalPaymentB2CProviderDao.
 */
public class DefaultEsalPaymentB2CProviderDao extends DefaultPaymentB2BProviderDao implements PaymentB2BProviderDAO
{

	/**
	 * Instantiates a new default ESAL payment B 2 b provider dao.
	 */
	public DefaultEsalPaymentB2CProviderDao()
	{
		super(EsalPaymentB2CProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EsalPaymentB2CProviderModel._TYPECODE;
	}

}
