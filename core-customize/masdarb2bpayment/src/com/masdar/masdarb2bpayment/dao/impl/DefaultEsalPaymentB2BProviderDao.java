/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.dao.impl;

import com.masdar.masdarPaymentB2Bprovider.model.EsalPaymentB2BProviderModel;
import com.masdar.masdarb2bpayment.dao.PaymentB2BProviderDAO;


/**
 * The Class DefaultESALPaymentB2BProviderDao.
 */
public class DefaultEsalPaymentB2BProviderDao extends DefaultPaymentB2BProviderDao implements PaymentB2BProviderDAO
{

	/**
	 * Instantiates a new default ESAL payment B 2 b provider dao.
	 */
	public DefaultEsalPaymentB2BProviderDao()
	{
		super(EsalPaymentB2BProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EsalPaymentB2BProviderModel._TYPECODE;
	}

}
