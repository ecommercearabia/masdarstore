/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.masdarPaymentB2Cprovider.model.EsalPaymentB2CProviderModel;
import com.masdar.masdarb2bpayment.service.PaymentB2BProviderService;
import com.masdar.masdarb2bpayment.strategy.PaymentB2BProviderStrategy;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Class DefaultEsalPaymentB2CProviderStrategy.
 */
public class DefaultEsalPaymentB2CProviderStrategy implements PaymentB2BProviderStrategy
{

	/** The payment B 2 b provider service. */
	@Resource(name = "paymentB2BProviderService")
	private PaymentB2BProviderService paymentB2BProviderService;

	/**
	 * Gets the payment B 2 b provider service.
	 *
	 * @return the payment B 2 b provider service
	 */
	public PaymentB2BProviderService getPaymentB2BProviderService()
	{
		return paymentB2BProviderService;
	}


	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getPaymentB2BProviderService().getActiveProvider(baseStoreUid, EsalPaymentB2CProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{

		return getPaymentB2BProviderService().getActiveProvider(baseStoreModel, EsalPaymentB2CProviderModel.class);

	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<PaymentB2BProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getPaymentB2BProviderService().getActiveProviderByCurrentBaseStore(EsalPaymentB2CProviderModel.class);
	}


}
