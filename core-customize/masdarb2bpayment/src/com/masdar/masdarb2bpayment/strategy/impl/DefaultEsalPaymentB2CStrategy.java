/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.strategy.impl;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpStatusCodeException;

import com.google.common.base.Preconditions;
import com.masdar.core.enums.WebServiceRequestStatus;
import com.masdar.core.event.FullPaymentEvent;
import com.masdar.core.event.SadadBillIdEvent;
import com.masdar.core.order.service.CustomOrderService;
import com.masdar.masdarPaymentB2Cprovider.model.EsalPaymentB2CProviderModel;
import com.masdar.masdarb2bpayment.entry.InvoiceCancelResult;
import com.masdar.masdarb2bpayment.entry.InvoicePaymentStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;
import com.masdar.masdarb2bpayment.esal.beans.CancelResult;
import com.masdar.masdarb2bpayment.esal.beans.Invoice;
import com.masdar.masdarb2bpayment.esal.beans.PaymentStatus;
import com.masdar.masdarb2bpayment.esal.enums.CancelStatus;
import com.masdar.masdarb2bpayment.esal.exceptions.EsalException;
import com.masdar.masdarb2bpayment.esal.exceptions.type.EsalExceptionType;
import com.masdar.masdarb2bpayment.esal.services.ESALInvoiceService;
import com.masdar.masdarb2bpayment.exception.PaymentB2BException;
import com.masdar.masdarb2bpayment.exception.type.PaymentB2BExceptionType;
import com.masdar.masdarb2bpayment.service.ESALCreditNotesRecordService;
import com.masdar.masdarb2bpayment.service.PaymentB2BProviderService;
import com.masdar.masdarb2bpayment.service.PaymentB2BWebServiceRecordService;
import com.masdar.masdarb2bpayment.strategy.PaymentB2BStrategy;
import com.masdar.masdarpaymentB2Bprovider.model.ESALCreditNoteRecordModel;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Class DefaultEsalPaymentB2CStrategy.
 */
public class DefaultEsalPaymentB2CStrategy implements PaymentB2BStrategy
{
	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */

	private static final Logger LOG = LoggerFactory.getLogger(DefaultEsalPaymentB2CStrategy.class);
	private static final String PAID_STATUS = "PAID";

	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";

	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";

	private static final String BASE_STORE_MUSTN_T_BE_NULL = "baseStore mustn't be null";

	private static final String ABSTRACTORDERS_MUSTN_T_BE_EMPTY = "abstractOrders mustn't be null";

	private static final String UNIQUE_ID = "Id_";

	@Resource(name = "esalCreditNotesRecordService")
	private ESALCreditNotesRecordService esalCreditNotesRecordService;

	@Resource(name = "paymentB2BWebServiceRecordService")
	private PaymentB2BWebServiceRecordService paymentB2BWebServiceRecordService;

	/** The payment B 2 b provider service. */
	@Resource(name = "paymentB2BProviderService")
	private PaymentB2BProviderService paymentB2BProviderService;

	@Resource(name = "esalInvoiceService")
	private ESALInvoiceService esalInvoiceService;

	@Resource(name = "esalInvoiceConverter")
	private Converter<AbstractOrderModel, Invoice> esalInvoiceConverter;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;


	@Override
	public String createInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}

		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;
		final Invoice invoice = esalInvoiceConverter.convert(abstractOrderModel);

		try
		{
			final String invoiceId = esalInvoiceService.createInvoice(providerModel.getBaseUrl(), providerModel.getUsername(),
					providerModel.getPassword(), UNIQUE_ID + invoice.getInvoiceNumber(), providerModel.getSuplierId(), invoice);
			paymentB2BWebServiceRecordService.addApiRecord(invoice, null, WebServiceRequestStatus.SUCCESS, null, abstractOrderModel);
			if (Objects.nonNull(abstractOrderModel.getStore()))
			{
				abstractOrderModel.setEsalInvoiceDueDatePeriod(abstractOrderModel.getStore().getEsalInvoiceDueDate());
			}
			abstractOrderModel.setPaymentType(CheckoutPaymentType.ESAL_B2C);
			abstractOrderModel.setPaymentB2BInvoiceCreatedSuccessfully(true);
			abstractOrderModel.setPaymentB2BInvoiceId(invoiceId);
			abstractOrderModel.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.NOTPAID);
			abstractOrderModel.setInvoiceCreationDate(new Date());
			modelService.save(abstractOrderModel);
			return invoiceId;
		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			paymentB2BWebServiceRecordService.addApiRecord(invoice, e.getData(), WebServiceRequestStatus.FAILED, e.getMessage(),
					abstractOrderModel);
			throw exception;
		}
		catch (final Exception e)
		{
			paymentB2BWebServiceRecordService.addApiRecord(invoice, e, WebServiceRequestStatus.FAILED, e.getMessage(),
					abstractOrderModel);
			throw e;
		}

	}

	@Override
	public void createInvoices(final PaymentB2BProviderModel paymentB2BProviderModel,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		if (CollectionUtils.isEmpty(abstractOrderModels))
		{
			throw new IllegalArgumentException(ABSTRACTORDERS_MUSTN_T_BE_EMPTY);
		}
		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}
		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;
		final List<Invoice> invoices = esalInvoiceConverter.convertAll(abstractOrderModels);

		try
		{
			esalInvoiceService.createInvoices(providerModel.getBaseUrl(), providerModel.getUsername(), providerModel.getPassword(),
					UNIQUE_ID, providerModel.getSuplierId(), invoices);

		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			throw exception;
		}

	}

	@Override
	public Optional<InvoicePaymentStatus> getPaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}
		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;
		final Optional<PaymentStatus> paymentStatus;
		try
		{
			paymentStatus = esalInvoiceService.getPaymentStatus(providerModel.getBaseUrl(), providerModel.getUsername(),
					providerModel.getPassword(), abstractOrderModel.getPaymentB2BInvoiceId());
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), paymentStatus.get(),
					WebServiceRequestStatus.SUCCESS, null, abstractOrderModel);

		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), e.getData(),
					WebServiceRequestStatus.FAILED, e.getMessage(), abstractOrderModel);

			throw exception;
		}
		catch (final Exception e)
		{
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), e,
					WebServiceRequestStatus.FAILED, e.getMessage(), abstractOrderModel);
			throw e;
		}


		return Optional.ofNullable(populatInvoicePaymentStatus(paymentStatus));
	}


	/**
	 *
	 */
	protected InvoicePaymentStatus populatInvoicePaymentStatus(final Optional<PaymentStatus> paymentStatus)
	{
		if (paymentStatus.isEmpty())
		{
			return null;
		}
		final InvoicePaymentStatus invoicePaymentStatus = new InvoicePaymentStatus();
		invoicePaymentStatus.setBillerId(paymentStatus.get().getBillerId());
		invoicePaymentStatus.setSadadBillsID(paymentStatus.get().getSadadBillsID());
		invoicePaymentStatus.setOutstandingAmount(paymentStatus.get().getOutstandingAmount());
		invoicePaymentStatus.setPaymentStatus(paymentStatus.get().getPaymentStatus());
		invoicePaymentStatus.setTotalPaidAmount(paymentStatus.get().getTotalPaidAmount());
		return invoicePaymentStatus;
	}

	@Override
	public Optional<InvoiceCancelResult> cancelInvoice(final PaymentB2BProviderModel paymentB2BProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}
		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;

		Optional<CancelResult> cancelInvoice = Optional.empty();
		try
		{
			cancelInvoice = esalInvoiceService.cancelInvoice(providerModel.getBaseUrl(), providerModel.getUsername(),
					providerModel.getPassword(), providerModel.getSuplierId(), abstractOrderModel.getCode());
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), cancelInvoice.get(),
					WebServiceRequestStatus.SUCCESS, null, abstractOrderModel);
		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), e.getData(),
					WebServiceRequestStatus.FAILED, e.getMessage(), abstractOrderModel);
			throw exception;
		}
		catch (final Exception e)
		{
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), e,
					WebServiceRequestStatus.FAILED, e.getMessage(), abstractOrderModel);
		}

		return Optional.ofNullable(populateCancelInvoiceResult(cancelInvoice));
	}


	/**
	 *
	 */
	protected InvoiceCancelResult populateCancelInvoiceResult(final Optional<CancelResult> cancelInvoice)
	{
		if (cancelInvoice.isEmpty())
		{
			return null;
		}
		final InvoiceCancelResult invoiceCancelResult = new InvoiceCancelResult();
		invoiceCancelResult.setErrors(cancelInvoice.get().getErrors());
		invoiceCancelResult.setResult(cancelInvoice.get().getResult());
		return invoiceCancelResult;
	}


	@Override
	public Optional<InvoiceCancelResult> cancelInvoices(final PaymentB2BProviderModel paymentB2BProviderModel,
			final List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		if (CollectionUtils.isEmpty(abstractOrderModels))
		{
			throw new IllegalArgumentException(ABSTRACTORDERS_MUSTN_T_BE_EMPTY);
		}

		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}
		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;

		final List<String> invoiceNumbers = abstractOrderModels.stream().map(AbstractOrderModel::getCode)
				.collect(Collectors.toList());

		Optional<CancelResult> cancelInvoice;

		try
		{
			cancelInvoice = esalInvoiceService.cancelInvoices(providerModel.getBaseUrl(), providerModel.getUsername(),
					providerModel.getPassword(), providerModel.getSuplierId(), invoiceNumbers);
		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			throw exception;
		}

		return Optional.ofNullable(populateCancelInvoiceResult(cancelInvoice));
	}

	@Override
	public void updateInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		if (!(paymentB2BProviderModel instanceof EsalPaymentB2CProviderModel))
		{
			throw new NotSupportedException("");
		}

		final EsalPaymentB2CProviderModel providerModel = (EsalPaymentB2CProviderModel) paymentB2BProviderModel;
		final Invoice invoice = esalInvoiceConverter.convert(abstractOrderModel);
		final List<ESALCreditNoteRecordModel> esalCreditNoteRecords = abstractOrderModel.getEsalCreditNoteRecords();
		invoice.setInvoiceNumber(invoice.getInvoiceNumber() + "_cn_" + (esalCreditNoteRecords.size() + 1));
		final String invoiceReferrance = abstractOrderModel.getPaymentB2BInvoiceId();

		try
		{
			esalInvoiceService.createCreditNote(providerModel.getBaseUrl(), providerModel.getUsername(), providerModel.getPassword(),
					UNIQUE_ID + invoice.getInvoiceNumber(), providerModel.getSuplierId(), invoiceReferrance, invoice);
			esalCreditNotesRecordService.addRecord(abstractOrderModel, invoice);
			paymentB2BWebServiceRecordService.addApiRecord(invoice, null, WebServiceRequestStatus.SUCCESS, null, abstractOrderModel);

		}
		catch (final EsalException e)
		{
			final PaymentB2BException exception = new PaymentB2BException(PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION,
					PaymentB2BExceptionType.ESAL_PAYMENT_EXCEPTION.getMsg(), e);
			paymentB2BWebServiceRecordService.addApiRecord(invoice, e.getData(), WebServiceRequestStatus.FAILED, e.getMessage(),
					abstractOrderModel);
			throw exception;
		}
		catch (final Exception e)
		{
			paymentB2BWebServiceRecordService.addApiRecord(abstractOrderModel.getPaymentB2BInvoiceId(), e,
					WebServiceRequestStatus.FAILED, e.getMessage(), abstractOrderModel);
			throw e;
		}
	}

	@Override
	public boolean isPaid(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		if (!isPaymentB2B(order))
		{
			LOG.error("payment is not B2B");
			return false;
		}
		Optional<InvoicePaymentStatus> paymentStatus;
		try
		{
			paymentStatus = getPaymentStatus(paymentB2BProviderModel, order);
		}
		catch (final PaymentB2BException e)
		{
			LOG.error(e.getMessage());
			return false;
		}

		if (paymentStatus.isEmpty())
		{
			LOG.error("payment status is empty");
			return false;
		}

		final InvoicePaymentStatus invoicePaymentStatus = paymentStatus.get();

		return (PAID_STATUS.equals(invoicePaymentStatus.getPaymentStatus())
				&& order.getTotalPrice() == invoicePaymentStatus.getTotalPaidAmount()
				&& invoicePaymentStatus.getOutstandingAmount() == 0);
	}

	protected boolean isPaymentB2B(final AbstractOrderModel order)
	{
		if (Objects.isNull(order))
		{
			return false;
		}
		return (CheckoutPaymentType.ESAL_B2C.equals(order.getPaymentType()));

	}

	@Override
	public void updatePaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		try
		{
			final Optional<InvoicePaymentStatus> paymentStatus = getPaymentStatus(paymentB2BProviderModel, order);
			if (paymentStatus.isEmpty())
			{
				LOG.error("payment status is empty");
				return;
			}
			modifyOrderAttributes(order, paymentStatus.get());
			final Optional<AbstractOrderModel> orderByCodeAndOrderStatus = customOrderService
					.getOrderByCodeAndOrderStatus(order.getCode(), OrderStatus.CREATED);

			if (orderByCodeAndOrderStatus.isPresent())
			{
				orderByCodeAndOrderStatus.get().setPaymentStatus(order.getPaymentStatus());
				orderByCodeAndOrderStatus.get().setSadadBillId(order.getSadadBillId());
				modelService.save(orderByCodeAndOrderStatus.get());
			}

		}
		catch (final PaymentB2BException e)
		{

			if (e.getException() instanceof EsalException
					&& EsalExceptionType.BAD_REQUET.equals(((EsalException) (e.getException())).getType())
					&& Strings.isBlank(order.getSadadBillId()))
			{
				order.setPaymentB2BInvoiceId(Strings.EMPTY);
				order.setPaymentB2BInvoiceCreatedSuccessfully(false);
				order.setInvoiceCreationDate(null);
			}

			LOG.error(e.getMessage());
		}
		modelService.save(order);
	}

	/**
	 *
	 */
	private void modifyOrderAttributes(final AbstractOrderModel order, final InvoicePaymentStatus paymentStatus)
	{

		order.setPaymentB2BPaidAmount(paymentStatus.getTotalPaidAmount());
		order.setSadadBillId(paymentStatus.getSadadBillsID());
		sendSadadEmail((OrderModel) order);
		if (order.getTotalPrice() <= order.getPaymentB2BPaidAmount()
				|| PAID_STATUS.equalsIgnoreCase(paymentStatus.getPaymentStatus()))
		{
			order.setPaymentStatus(de.hybris.platform.core.enums.PaymentStatus.PAID);
			order.setStatus(OrderStatus.READY);
			sendPaymentNotificationEmail((OrderModel) order);
		}

	}

	public boolean cancelOrder(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(paymentB2BProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(order != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		if (isPaid(paymentB2BProviderModel, order))
		{
			LOG.info("cannot cancel invoice {} because it's paid", order.getPaymentB2BInvoiceId());
			return false;
		}

		if (PaymentProviderTransactionType.FULL_CANCEL.equals(order.getPaymentProviderTransactionType()))
		{
			return fullCancel(paymentB2BProviderModel, order);
		}
		else
		{
			return partialCancel(paymentB2BProviderModel, order);
		}

	}


	protected boolean partialCancel(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{

		final NotSupportedException ex = new NotSupportedException("ESAL partial cancellation is not supported.");
		LOG.error("Process: exception = {}", ex.getMessage());
		throw ex;
	}

	protected boolean fullCancel(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		Optional<InvoiceCancelResult> cancelInvoice;
		try
		{
			cancelInvoice = cancelInvoice(paymentB2BProviderModel, order);
			if (cancelInvoice.isPresent() && CancelStatus.SUCCESS.toString().equalsIgnoreCase(cancelInvoice.get().getResult()))
			{
				order.setPaymentProviderStatus(PaymentProviderStatus.SUCCESS);
				return true;
			}
		}
		catch (final PaymentB2BException e)
		{
			LOG.error("Process: exception = {}", e.getException().getMessage());
		}
		order.setPaymentProviderStatus(PaymentProviderStatus.FAILED);
		return false;
	}


	protected boolean isInvoiceExist(final PaymentB2BProviderModel paymentB2BProviderModel, final AbstractOrderModel order)
	{
		try
		{
			final Optional<InvoicePaymentStatus> paymentStatus = getPaymentStatus(paymentB2BProviderModel, order);
			return paymentStatus.isPresent();
		}
		catch (final PaymentB2BException e1)
		{
			final Exception exception = e1.getException();
			if ((exception instanceof EsalException) && ((EsalException) exception).getData() instanceof HttpStatusCodeException)
			{
				final HttpStatusCodeException httpException = (HttpStatusCodeException) ((EsalException) exception).getData();
				if (httpException.getStatusCode() == HttpStatus.BAD_REQUEST)
				{
					order.setPaymentProviderStatus(PaymentProviderStatus.SUCCESS);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 *
	 */
	private void sendPaymentNotificationEmail(final OrderModel orderModel)
	{
		if (orderModel.isPaymentUpdateEmailSent())
		{
			return;
		}

		if (!CollectionUtils.isEmpty(orderModel.getOrderProcess()))
		{
			final Optional<OrderProcessModel> process = orderModel.getOrderProcess().stream().findFirst();
			if (process.isPresent())
			{
				eventService.publishEvent(new FullPaymentEvent(process.get()));
				orderModel.setPaymentUpdateEmailSent(true);
				modelService.save(orderModel);
			}
		}
	}

	/**
	 *
	 */
	private void sendSadadEmail(final OrderModel orderModel)
	{
		if (orderModel.isSadadBillIdEmailSent())
		{
			return;
		}
		if (!CollectionUtils.isEmpty(orderModel.getOrderProcess()))
		{
			final Optional<OrderProcessModel> process = orderModel.getOrderProcess().stream().findFirst();
			if (process.isPresent())
			{
				eventService.publishEvent(new SadadBillIdEvent(process.get()));
				orderModel.setSadadBillIdEmailSent(true);
				modelService.save(orderModel);
			}
		}
	}

}

