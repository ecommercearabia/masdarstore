package com.masdar.masdarb2bpayment.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarb2bpayment.entry.InvoiceCancelResult;
import com.masdar.masdarb2bpayment.entry.InvoicePaymentStatus;
import com.masdar.masdarb2bpayment.exception.PaymentB2BException;
import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 *
 *
 */
public interface PaymentB2BStrategy
{

	public String createInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException;

	public void updateInvoice(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel abstractOrderModel)
			throws PaymentB2BException;

	public void createInvoices(final PaymentB2BProviderModel paymentProviderModel,
			List<AbstractOrderModel> abstractOrderModels)
			throws PaymentB2BException;

	public Optional<InvoiceCancelResult> cancelInvoice(final PaymentB2BProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	public Optional<InvoiceCancelResult> cancelInvoices(final PaymentB2BProviderModel paymentProviderModel,
			List<AbstractOrderModel> abstractOrderModels) throws PaymentB2BException;

	public Optional<InvoicePaymentStatus> getPaymentStatus(final PaymentB2BProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentB2BException;

	public boolean isPaid(PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);

	public void updatePaymentStatus(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);

	public boolean cancelOrder(final PaymentB2BProviderModel paymentB2BProviderModel, AbstractOrderModel order);

}