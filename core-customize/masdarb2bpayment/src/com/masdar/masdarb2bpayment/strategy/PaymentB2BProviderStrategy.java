/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bpayment.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdarpaymentB2Bprovider.model.PaymentB2BProviderModel;


/**
 * The Interface PaymentB2BProviderStrategy.
 */
public interface PaymentB2BProviderStrategy
{

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	public Optional<PaymentB2BProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	public Optional<PaymentB2BProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	public Optional<PaymentB2BProviderModel> getActiveProviderByCurrentBaseStore();
}
