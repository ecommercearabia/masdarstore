/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationpromotionssampledatacustomaddon.controllers;

/**
 */
public interface MasdarpersonalizationpromotionssampledatacustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
