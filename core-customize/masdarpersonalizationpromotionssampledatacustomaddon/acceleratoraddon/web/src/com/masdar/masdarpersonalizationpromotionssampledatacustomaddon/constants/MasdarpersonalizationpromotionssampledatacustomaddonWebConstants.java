/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationpromotionssampledatacustomaddon.constants;

/**
 * Global class for all Masdarpersonalizationpromotionssampledatacustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarpersonalizationpromotionssampledatacustomaddonWebConstants // NOSONAR
{
	private MasdarpersonalizationpromotionssampledatacustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
