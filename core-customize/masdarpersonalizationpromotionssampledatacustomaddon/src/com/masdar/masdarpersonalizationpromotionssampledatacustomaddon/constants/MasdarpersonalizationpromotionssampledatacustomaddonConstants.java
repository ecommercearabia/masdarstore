/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationpromotionssampledatacustomaddon.constants;

/**
 * Global class for all Masdarpersonalizationpromotionssampledatacustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class MasdarpersonalizationpromotionssampledatacustomaddonConstants extends GeneratedMasdarpersonalizationpromotionssampledatacustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarpersonalizationpromotionssampledatacustomaddon";

	private MasdarpersonalizationpromotionssampledatacustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
