/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarstorecreditfacades.constants;

/**
 * Global class for all Masdarstorecreditfacades constants. You can add global constants for your extension into this class.
 */
public final class MasdarstorecreditfacadesConstants extends GeneratedMasdarstorecreditfacadesConstants
{
	public static final String EXTENSIONNAME = "masdarstorecreditfacades";

	private MasdarstorecreditfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarstorecreditfacadesPlatformLogo";
}
