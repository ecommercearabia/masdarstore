/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdartimeslot.constants;

/**
 * Global class for all Masdartimeslot constants. You can add global constants for your extension into this class.
 */
public final class MasdartimeslotConstants extends GeneratedMasdartimeslotConstants
{
	public static final String EXTENSIONNAME = "masdartimeslot";

	private MasdartimeslotConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
