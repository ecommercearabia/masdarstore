/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdartimeslot.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.masdar.core.area.service.AreaService;
import com.masdar.core.model.AreaModel;
import com.masdar.masdartimeslot.constants.GeneratedMasdartimeslotConstants.Enumerations.TimeSlotConfigType;
import com.masdar.masdartimeslot.dao.TimeSlotDao;
import com.masdar.masdartimeslot.model.PeriodModel;
import com.masdar.masdartimeslot.model.TimeSlotInfoModel;
import com.masdar.masdartimeslot.model.TimeSlotModel;
import com.masdar.masdartimeslot.service.TimeSlotService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotService implements TimeSlotService
{
	private static final String ZONE_DELIVERY_MODE_ILLEGAL_ARGS = "zoneDeliveryModeCode must not be null or empty.";
	private static final String AREA_ILLEGAL_ARGS = "areaCode must not be null or empty.";
	private static final String PERIOD_CODE_ILLEGAL_ARGS = "periodCode must not be null or empty.";

	@Resource(name = "deliveryService")
	private DeliveryService deliveryService;

	@Resource(name = "timeSlotDao")
	private TimeSlotDao timeSlotDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "areaService")
	private AreaService areaService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public Optional<TimeSlotModel> getActive(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);

		final DeliveryModeModel zoneDeliveryModeModel = deliveryService.getDeliveryModeForCode(zoneDeliveryModeCode);

		if (zoneDeliveryModeModel instanceof ZoneDeliveryModeModel)
		{
			return Optional.ofNullable(((ZoneDeliveryModeModel) zoneDeliveryModeModel).getTimeSlot());
		}
		return Optional.ofNullable(null);
	}

	@Override
	public boolean isTimeSlotEnabled(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);
		final ZoneDeliveryModeModel deliveryMode = (ZoneDeliveryModeModel) deliveryService
				.getDeliveryModeForCode(zoneDeliveryModeCode);
		return deliveryMode != null && deliveryMode.getTimeSlot() != null && deliveryMode.getTimeSlot().isActive();
	}

	@Override
	public boolean isTimeSlotEnabledByArea(final String areaCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(areaCode), AREA_ILLEGAL_ARGS);
		final Optional<AreaModel> optional = areaService.get(areaCode);
		return optional.isPresent() && optional.get().getTimeSlot() != null && optional.get().getTimeSlot().isActive();
	}

	@Override
	public int getNumberOfOrdersByTimeSlot(final BaseStoreModel store, final String start, final String end, final String date,
			final String timezone)
	{
		return timeSlotDao.get(store, start, end, date, timezone).size();
	}

	@Override
	public List<OrderModel> getOrdersByInterval(final BaseStoreModel store, final String timezone, final long interval)
	{
		return timeSlotDao.get(store, timezone, interval);
	}

	@Override
	public void saveTimeSlotInfo(final TimeSlotInfoModel info, final CartModel cart, final String date, final LocalTime start)
	{
		Optional<TimeSlotModel> timeSlot;
		if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType().name())
				&& cart.getDeliveryAddress() != null && cart.getDeliveryAddress().getArea() != null)
		{
			timeSlot = getActiveByArea(cart.getDeliveryAddress().getArea().getCode());
		}
		else
		{
			timeSlot = getActive(cart.getDeliveryMode().getCode());
		}

		Preconditions.checkArgument(timeSlot.isPresent(), "Cannot Find timeslot");

		final String timezone = timeSlot.get().getTimezone();
		info.setTimezone(timezone);

		final ZonedDateTime startDateTime = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				start, ZoneId.of(timezone));

		info.setStartDate(Date.from(startDateTime.toInstant()));
		info.setStartHour(startDateTime.getHour());
		info.setStartMinute(startDateTime.getMinute());

		modelService.save(info);
		modelService.refresh(info);
		cart.setTimeSlotInfo(info);
		modelService.save(cart);
	}

	@Override
	public Optional<TimeSlotModel> getActiveByArea(final String areaCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(areaCode), AREA_ILLEGAL_ARGS);
		final Optional<AreaModel> optional = areaService.get(areaCode);
		if (optional.isPresent())
		{
			return Optional.ofNullable(optional.get().getTimeSlot());
		}
		return Optional.ofNullable(null);
	}

	@Override
	public Optional<PeriodModel> getPeriod(final String periodCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(periodCode), PERIOD_CODE_ILLEGAL_ARGS);
		return timeSlotDao.findPeriod(periodCode);
	}
}
