package com.masdar.commercialregistration.service.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.dao.CommercialRegistrationProviderDao;
import com.masdar.commercialregistration.dao.CompanyRegistrationInfoDao;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.exception.type.CommercialRegistrationExceptionType;
import com.masdar.commercialregistration.model.CommercialRegistrationProviderModel;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.service.CommercialRegistrationService;
import com.masdar.commercialregistration.service.WathqService;



/**
 * The Class DefaultCommercialRegistrationService.
 */
public class DefaultCommercialRegistrationService implements CommercialRegistrationService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCommercialRegistrationService.class);

	/** The Constant CODE_IS_NULL_OR_EMPTY. */
	private static final String CODE_IS_NULL_OR_EMPTY = "code is null or empty";

	/** The Constant CR_IS_NULL_OR_EMPTY. */
	private static final String CR_IS_NULL_OR_EMPTY = "cr is null or empty";

	/** The commercial registration provider dao. */
	@Resource(name = "commercialRegistrationProviderDao")
	private CommercialRegistrationProviderDao commercialRegistrationProviderDao;

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The company registration info dao. */
	@Resource(name = "companyRegistrationInfoDao")
	private CompanyRegistrationInfoDao companyRegistrationInfoDao;

	/** The wathq service. */
	@Resource(name = "wathqService")
	private WathqService wathqService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "companyRegistrationInfoToBeanConverter")
	private Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfo> companyRegistrationInfoToBeanConverter;


	/**
	 * @return the companyRegistrationInfoToBeanConverter
	 */
	protected Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfo> getCompanyRegistrationInfoToBeanConverter()
	{
		return companyRegistrationInfoToBeanConverter;
	}


	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	@Resource(name = "companyRegistrationInfoToModelConverter")
	private Converter<CompanyRegistrationInfo, CompanyRegistrationInfoModel> companyRegistrationInfoToModelConverter;

	/**
	 * @return the companyRegistrationInfoToModelConverter
	 */
	protected Converter<CompanyRegistrationInfo, CompanyRegistrationInfoModel> getCompanyRegistrationInfoToModelConverter()
	{
		return companyRegistrationInfoToModelConverter;
	}


	/**
	 * Gets the base store service.
	 *
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * Gets the wathq service.
	 *
	 * @return the wathqService
	 */
	protected WathqService getWathqService()
	{
		return wathqService;
	}

	/**
	 * Gets the commercial registration provider dao.
	 *
	 * @return the commercialRegistrationProviderDao
	 */
	protected CommercialRegistrationProviderDao getCommercialRegistrationProviderDao()
	{
		return commercialRegistrationProviderDao;
	}

	/**
	 * Gets the company registration info dao.
	 *
	 * @return the companyRegistrationInfoDao
	 */
	protected CompanyRegistrationInfoDao getCompanyRegistrationInfoDao()
	{
		return companyRegistrationInfoDao;
	}

	/**
	 * Gets the company cr info by current store.
	 *
	 * @param cr
	 *           the cr
	 * @return the company cr info by current store
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	public Optional<CompanyRegistrationInfo> getCompanyCrInfoByCurrentStore(final String cr) throws CommercialRegistrationException
	{
		return getCompanyCrInfo(cr, getBaseStoreService().getCurrentBaseStore());
	}

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @param store
	 *           the store
	 * @return the company cr info
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	@Override
	public Optional<CompanyRegistrationInfo> getCompanyCrInfo(final String cr, final BaseStoreModel store)
			throws CommercialRegistrationException
	{
		final Optional<CommercialRegistrationProviderModel> providerByCurrentBaseStore = getCommercialRegistrationProviderDao()
				.getProviderByStore(store);
		if (providerByCurrentBaseStore.isEmpty())
		{
			throw new CommercialRegistrationException("Provider Is null", CommercialRegistrationExceptionType.INTERNAL_ERROR);
		}

		return getCompanyCrInfo(cr, providerByCurrentBaseStore.get());
	}

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @param provider
	 *           the provider
	 * @return the company cr info
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	protected Optional<CompanyRegistrationInfo> getCompanyCrInfo(final String cr,
			final CommercialRegistrationProviderModel provider) throws CommercialRegistrationException
	{

		Preconditions.checkArgument(StringUtils.isNoneEmpty(cr), CR_IS_NULL_OR_EMPTY);

		final Optional<CompanyRegistrationInfoModel> companyRegistrationInfoModel = getCompanyCrInfoModel(cr);

		if (companyRegistrationInfoModel.isPresent())
		{
			LOG.info("Company Registration Info Model with cr: {} found and returned successfully", cr);

			return Optional.of(getCompanyRegistrationInfoToBeanConverter().convert(companyRegistrationInfoModel.get()));
		}
		LOG.info("Company Registration Info Model with cr: {} not found", cr);
		if (provider == null || provider.getBaseURL() == null || provider.getApiKey() == null)
		{
			LOG.error("Commercial Registration Provider data is not valid");

			throw new CommercialRegistrationException("Commercial Registration Provider data is not valid["
					+ CommercialRegistrationExceptionType.INTERNAL_ERROR.getMsg() + "]",
					CommercialRegistrationExceptionType.INTERNAL_ERROR);
		}

		final Optional<CompanyRegistrationInfo> companyCrInfo = getWathqService().getCompanyCrInfo(cr, provider.getBaseURL(),
				provider.getApiKey());


		if (companyCrInfo.isPresent())
		{
			saveCompanyRegistrationInfo(companyCrInfo.get(), cr);
		}

		return companyCrInfo;
	}

	/**
	 * Save company registration info.
	 *
	 * @param companyRegistrationInfo
	 *           the company registration info
	 * @param cr
	 */
	private void saveCompanyRegistrationInfo(final CompanyRegistrationInfo companyRegistrationInfo, final String cr)
	{
		try
		{
			CompanyRegistrationInfoModel companyRegistrationInfoModel = getModelService().create(CompanyRegistrationInfoModel.class);

			companyRegistrationInfoModel = getCompanyRegistrationInfoToModelConverter().convert(companyRegistrationInfo,
					companyRegistrationInfoModel);
			getModelService().saveAll(companyRegistrationInfoModel);
			LOG.info("Company Registration Info saved  successfully");

		}
		catch (final Exception e)
		{
			LOG.error("Company Registration Info Model not saved[" + e.getMessage() + "]");
		}
	}

	/**
	 * Gets the company cr info.
	 *
	 * @param code
	 *           the code
	 * @return the company cr info
	 */
	@Override
	public Optional<CompanyRegistrationInfoModel> getCompanyCrInfoModel(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_IS_NULL_OR_EMPTY);
		return getCompanyRegistrationInfoDao().getByCodeOrEntityNumber(code);
	}


	/**
	 * @param companyCr
	 * @param baseStore
	 * @return
	 * @throws CommercialRegistrationException
	 */
	public CompanyRegistrationInfoModel checkAndCreateCompanyCrInfoModel(final String companyCr, final BaseStoreModel baseStore)
			throws CommercialRegistrationException
	{
		Optional<CompanyRegistrationInfoModel> companyCrInfoModel = this.getCompanyCrInfoModel(companyCr);
		if (companyCrInfoModel.isPresent())
		{
			return companyCrInfoModel.get();
		}

		final Optional<CompanyRegistrationInfo> companyCrInfo = this.getCompanyCrInfo(companyCr, baseStore);

		if (companyCrInfo.isPresent() && !Strings.isBlank(companyCrInfo.get().getCrNumber()))
		{

			companyCrInfoModel = this.getCompanyCrInfoModel(companyCrInfo.get().getCrNumber());
			if (companyCrInfoModel.isPresent())
			{
				return companyCrInfoModel.get();
			}
		}

		throw new CommercialRegistrationException("Provider Is null", CommercialRegistrationExceptionType.INTERNAL_ERROR);
	}

}
