/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.service.impl;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.exception.type.CommercialRegistrationExceptionType;
import com.masdar.commercialregistration.service.WathqService;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


/**
 * The Class DefaultWathqService.
 */
public class DefaultWathqService implements WathqService
{

	/** The Constant CR_IS_NULL_OR_EMPTY. */
	private static final String CR_IS_NULL_OR_EMPTY = "cr is null or empty";

	/** The Constant BASEURL_IS_NULL_OR_EMPTY. */
	private static final String BASEURL_IS_NULL_OR_EMPTY = "baseURL is null or empty";

	/** The Constant APIKEY_IS_NULL_OR_EMPTY. */
	private static final String APIKEY_IS_NULL_OR_EMPTY = "apiKey is null or empty";

	/** The Constant GSON. */
	private static final Gson GSON = new GsonBuilder().create();

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @param baseURL
	 *           the base URL
	 * @param apiKey
	 *           the api key
	 * @return the company cr info
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	@Override
	public Optional<CompanyRegistrationInfo> getCompanyCrInfo(final String cr, final String baseURL, final String apiKey)
			throws CommercialRegistrationException
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(cr), CR_IS_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseURL), BASEURL_IS_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNoneEmpty(apiKey), APIKEY_IS_NULL_OR_EMPTY);

		final String URL = baseURL + "/info/" + cr;
		try
		{
			final String data = Unirest.get(URL).header("apiKey", apiKey).asString()
					.getBody();
			final CompanyRegistrationInfo a = GSON.fromJson(data, CompanyRegistrationInfo.class);

			if (a.getCode() != null)
			{
				throw new CommercialRegistrationException(a.getMessage(),
						CommercialRegistrationExceptionType.getExceptionFromCode(a.getCode()));
			}

			return Optional.ofNullable(a);
		}
		catch (final UnirestException e)
		{
			throw new CommercialRegistrationException(e.getMessage(), CommercialRegistrationExceptionType.INTERNAL_ERROR);
		}
	}

}
