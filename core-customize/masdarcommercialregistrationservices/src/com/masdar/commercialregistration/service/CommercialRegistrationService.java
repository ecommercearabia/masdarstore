package com.masdar.commercialregistration.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;



/**
 * The Interface CommercialRegistrationService.
 */
public interface CommercialRegistrationService
{

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @param store
	 *           the store
	 * @return the company cr info
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	Optional<CompanyRegistrationInfo> getCompanyCrInfo(String cr, final BaseStoreModel store)
			throws CommercialRegistrationException;

	/**
	 * Gets the company cr info by current store.
	 *
	 * @param cr
	 *           the cr
	 * @return the company cr info by current store
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	Optional<CompanyRegistrationInfo> getCompanyCrInfoByCurrentStore(String cr) throws CommercialRegistrationException;

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @return the company cr info
	 */
	Optional<CompanyRegistrationInfoModel> getCompanyCrInfoModel(String cr);

	CompanyRegistrationInfoModel checkAndCreateCompanyCrInfoModel(final String companyCr, final BaseStoreModel baseStore)
			throws CommercialRegistrationException;
}
