/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.service;

import java.util.Optional;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;



/**
 * The Interface WathqService.
 */
public interface WathqService
{

	/**
	 * Gets the company cr info.
	 *
	 * @param cr
	 *           the cr
	 * @param baseURL
	 *           the base URL
	 * @param apiKey
	 *           the api key
	 * @return the company cr info
	 * @throws CommercialRegistrationException
	 *            the commercial registration exception
	 */
	Optional<CompanyRegistrationInfo> getCompanyCrInfo(String cr,String baseURL,String apiKey)
			throws CommercialRegistrationException;
}
