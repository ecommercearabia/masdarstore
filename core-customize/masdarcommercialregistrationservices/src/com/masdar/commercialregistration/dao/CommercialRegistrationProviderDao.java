/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.commercialregistration.model.CommercialRegistrationProviderModel;


/**
 * The Interface CommercialRegistrationProviderDao.
 */
public interface CommercialRegistrationProviderDao
{

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active provider
	 */
	public Optional<CommercialRegistrationProviderModel> getProviderByStore(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	public Optional<CommercialRegistrationProviderModel> getProviderByCurrentBaseStore();

}
