package com.masdar.commercialregistration.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.masdar.commercialregistration.dao.CompanyRegistrationInfoDao;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;


/**
 * The Class DefaultCompanyRegistrationInfoDao.
 *
 * @author tuqa
 */
public class DefaultCompanyRegistrationInfoDao extends DefaultGenericDao<CompanyRegistrationInfoModel>
		implements CompanyRegistrationInfoDao
{
	/**
	 *
	 */
	public DefaultCompanyRegistrationInfoDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Instantiates a new default area dao.
	 */
	public DefaultCompanyRegistrationInfoDao()
	{
		super(CompanyRegistrationInfoModel._TYPECODE);
	}

	@Override
	public Optional<CompanyRegistrationInfoModel> get(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(CompanyRegistrationInfoModel.CODE, code);
		final List<CompanyRegistrationInfoModel> find = find(params);
		return find.stream().findFirst();
	}

	@Override
	public List<CompanyRegistrationInfoModel> getAll()
	{
		return find();
	}

	@Override
	public Optional<CompanyRegistrationInfoModel> getByCodeOrEntityNumber(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("select {c.pk} ");
		query.append("from {CompanyRegistrationInfo as c}");
		query.append("where");
		query.append("    {c.code}           = ?code ");
		query.append(" or {c.crNumber}       = ?crNumber ");
		query.append(" or {c.crEntityNumber} = ?crEntityNumber ");
		//		query.append(" or {c.crMainNumber}   = ?crMainNumber ");


		params.put("code", code);
		params.put("crNumber", code);
		params.put("crEntityNumber", code);
		//		params.put("crMainNumber", code);

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		final SearchResult<CompanyRegistrationInfoModel> result = getFlexibleSearchService().search(flexibleSearchQuery);


		if (result.getCount() >= 1)
		{
			return Optional.ofNullable(result.getResult().get(0));
		}

		return Optional.empty();
	}
}
