/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.dao.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Optional;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.masdar.commercialregistration.dao.CommercialRegistrationProviderDao;
import com.masdar.commercialregistration.model.CommercialRegistrationProviderModel;


/**
 *
 */
public class DefaultCommercialRegistrationProviderDao implements CommercialRegistrationProviderDao
{

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Override
	public Optional<CommercialRegistrationProviderModel> getProviderByStore(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "BaseStore Cannot Be null");

		return Optional.ofNullable(baseStoreModel.getCommercialRegistrationProvider());
	}

	@Override
	public Optional<CommercialRegistrationProviderModel> getProviderByCurrentBaseStore()
	{
		Preconditions.checkArgument(getBaseStoreService().getCurrentBaseStore() != null, "Current BaseStore Cannot Be null");
		return getProviderByStore(getBaseStoreService().getCurrentBaseStore());
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}



}
