/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.dao;

import java.util.List;
import java.util.Optional;

import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;



/**
 * The Interface CompanyRegistrationInfoDao.
 *
 * @author tuqa
 */
public interface CompanyRegistrationInfoDao
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<CompanyRegistrationInfoModel> get(String code);

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<CompanyRegistrationInfoModel> getAll();


	/**
	 * Gets CompanyRegistrationInfoModel
	 *
	 * @param code
	 *           crNumber or Entity Number
	 *
	 * @return the all
	 */
	public Optional<CompanyRegistrationInfoModel> getByCodeOrEntityNumber(String code);

}
