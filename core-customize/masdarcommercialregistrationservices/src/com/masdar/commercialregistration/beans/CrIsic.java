
package com.masdar.commercialregistration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrIsic {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("nameEN")
	@Expose
	private String nameEN;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameEN() {
		return nameEN;
	}

	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

}
