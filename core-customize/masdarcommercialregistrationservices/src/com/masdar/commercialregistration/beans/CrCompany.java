
package com.masdar.commercialregistration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrCompany {

	@SerializedName("period")
	@Expose
	private String period;
	@SerializedName("startDate")
	@Expose
	private String startDate;
	@SerializedName("endDate")
	@Expose
	private String endDate;

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
