
package com.masdar.commercialregistration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CompanyRegistrationInfo
{

	@SerializedName("code")
	@Expose
	private String code;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("crName")
	@Expose
	private String crName;
	@SerializedName("crNumber")
	@Expose
	private String crNumber;
	@SerializedName("crEntityNumber")
	@Expose
	private String crEntityNumber;
	@SerializedName("issueDate")
	@Expose
	private String issueDate;
	@SerializedName("expiryDate")
	@Expose
	private String expiryDate;
	@SerializedName("crMainNumber")
	@Expose
	private String crMainNumber;
	@SerializedName("businessType")
	@Expose
	private CrBusinessType businessType;
	@SerializedName("fiscalYear")
	@Expose
	private CrFiscalYear fiscalYear;
	@SerializedName("status")
	@Expose
	private CrStatus status;
	@SerializedName("cancellation")
	@Expose
	private CrCancellation cancellation;
	@SerializedName("location")
	@Expose
	private CrLocation location;
	@SerializedName("company")
	@Expose
	private CrCompany company;
	@SerializedName("activities")
	@Expose
	private CrActivities activities;

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the crName
	 */
	public String getCrName()
	{
		return crName;
	}

	/**
	 * @param crName
	 *           the crName to set
	 */
	public void setCrName(final String crName)
	{
		this.crName = crName;
	}

	/**
	 * @return the crNumber
	 */
	public String getCrNumber()
	{
		return crNumber;
	}

	/**
	 * @param crNumber
	 *           the crNumber to set
	 */
	public void setCrNumber(final String crNumber)
	{
		this.crNumber = crNumber;
	}

	/**
	 * @return the crEntityNumber
	 */
	public String getCrEntityNumber()
	{
		return crEntityNumber;
	}

	/**
	 * @param crEntityNumber
	 *           the crEntityNumber to set
	 */
	public void setCrEntityNumber(final String crEntityNumber)
	{
		this.crEntityNumber = crEntityNumber;
	}

	/**
	 * @return the issueDate
	 */
	public String getIssueDate()
	{
		return issueDate;
	}

	/**
	 * @param issueDate
	 *           the issueDate to set
	 */
	public void setIssueDate(final String issueDate)
	{
		this.issueDate = issueDate;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate()
	{
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *           the expiryDate to set
	 */
	public void setExpiryDate(final String expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the crMainNumber
	 */
	public String getCrMainNumber()
	{
		return crMainNumber;
	}

	/**
	 * @param crMainNumber
	 *           the crMainNumber to set
	 */
	public void setCrMainNumber(final String crMainNumber)
	{
		this.crMainNumber = crMainNumber;
	}

	/**
	 * @return the businessType
	 */
	public CrBusinessType getBusinessType()
	{
		return businessType;
	}

	/**
	 * @param businessType
	 *           the businessType to set
	 */
	public void setBusinessType(final CrBusinessType businessType)
	{
		this.businessType = businessType;
	}

	/**
	 * @return the fiscalYear
	 */
	public CrFiscalYear getFiscalYear()
	{
		return fiscalYear;
	}

	/**
	 * @param fiscalYear
	 *           the fiscalYear to set
	 */
	public void setFiscalYear(final CrFiscalYear fiscalYear)
	{
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the status
	 */
	public CrStatus getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final CrStatus status)
	{
		this.status = status;
	}

	/**
	 * @return the cancellation
	 */
	public CrCancellation getCancellation()
	{
		return cancellation;
	}

	/**
	 * @param cancellation
	 *           the cancellation to set
	 */
	public void setCancellation(final CrCancellation cancellation)
	{
		this.cancellation = cancellation;
	}

	/**
	 * @return the location
	 */
	public CrLocation getLocation()
	{
		return location;
	}

	/**
	 * @param location
	 *           the location to set
	 */
	public void setLocation(final CrLocation location)
	{
		this.location = location;
	}

	/**
	 * @return the company
	 */
	public CrCompany getCompany()
	{
		return company;
	}

	/**
	 * @param company
	 *           the company to set
	 */
	public void setCompany(final CrCompany company)
	{
		this.company = company;
	}

	/**
	 * @return the activities
	 */
	public CrActivities getActivities()
	{
		return activities;
	}

	/**
	 * @param activities
	 *           the activities to set
	 */
	public void setActivities(final CrActivities activities)
	{
		this.activities = activities;
	}


}
