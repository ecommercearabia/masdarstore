
package com.masdar.commercialregistration.beans;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrActivities {

	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("isic")
	@Expose
	private List<CrIsic> isic = null;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CrIsic> getIsic() {
		return isic;
	}

	public void setIsic(List<CrIsic> isic) {
		this.isic = isic;
	}

}
