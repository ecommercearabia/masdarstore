
package com.masdar.commercialregistration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrFiscalYear {

	@SerializedName("month")
	@Expose
	private int month;
	@SerializedName("day")
	@Expose
	private int day;
	@SerializedName("calendarType")
	@Expose
	private CrCalendarType calendarType;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public CrCalendarType getCalendarType() {
		return calendarType;
	}

	public void setCalendarType(CrCalendarType calendarType) {
		this.calendarType = calendarType;
	}

}
