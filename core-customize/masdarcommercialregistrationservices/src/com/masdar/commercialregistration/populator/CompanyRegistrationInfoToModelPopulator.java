/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

/**
 *
 */
/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrBusinessType;
import com.masdar.commercialregistration.beans.CrCancellation;
import com.masdar.commercialregistration.beans.CrCompany;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.beans.CrLocation;
import com.masdar.commercialregistration.beans.CrStatus;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrBusinessTypeModel;
import com.masdar.commercialregistration.model.CrCancellationModel;
import com.masdar.commercialregistration.model.CrCompanyModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;
import com.masdar.commercialregistration.model.CrLocationModel;
import com.masdar.commercialregistration.model.CrStatusModel;



/**
 * The Class CompanyRegistrationInfoToModelPopulator.
 */
public class CompanyRegistrationInfoToModelPopulator implements Populator<CompanyRegistrationInfo, CompanyRegistrationInfoModel>
{
	@Resource(name = "crActivitiesToModelConverter")
	private Converter<CrActivities, CrActivitiesModel> crActivitiesToModelConverter;

	/**
	 * @return the crActivitiesToModelConverter
	 */
	protected Converter<CrActivities, CrActivitiesModel> getCrActivitiesToModelConverter()
	{
		return crActivitiesToModelConverter;
	}

	@Resource(name = "crFiscalYearToModelConverter")
	private Converter<CrFiscalYear, CrFiscalYearModel> crFiscalYearToModelConverter;

	/**
	 * @return the crFiscalYearToModelConverter
	 */
	protected Converter<CrFiscalYear, CrFiscalYearModel> getCrFiscalYearToModelConverter()
	{
		return crFiscalYearToModelConverter;
	}

	/** The cr company converter. */
	@Resource(name = "crCompanyToModelConverter")
	private Converter<CrCompany, CrCompanyModel> crCompanyToModelConverter;

	/** The cr business converter. */
	@Resource(name = "crBusinessTypeToModelConverter")
	private Converter<CrBusinessType, CrBusinessTypeModel> crBusinessTypeToModelConverter;

	/** The cr cancellation converter. */
	@Resource(name = "crCancellationToModelConverter")
	private Converter<CrCancellation, CrCancellationModel> crCancellationToModelConverter;

	@Resource(name = "crStatusToModelConverter")
	private Converter<CrStatus, CrStatusModel> crStatusToModelConverter;

	@Resource(name = "crLocationToModelConverter")
	private Converter<CrLocation, CrLocationModel> crLocationToModelConverter;



	/**
	 * @return the crLocationToModelConverter
	 */
	protected Converter<CrLocation, CrLocationModel> getCrLocationToModelConverter()
	{
		return crLocationToModelConverter;
	}

	/**
	 * @return the crStatusToModelConverter
	 */
	protected Converter<CrStatus, CrStatusModel> getCrStatusToModelConverter()
	{
		return crStatusToModelConverter;
	}

	/**
	 * @return the crCompanyToModelConverter
	 */
	protected Converter<CrCompany, CrCompanyModel> getCrCompanyToModelConverter()
	{
		return crCompanyToModelConverter;
	}

	/**
	 * @return the crBusinessTypeToModelConverter
	 */
	protected Converter<CrBusinessType, CrBusinessTypeModel> getCrBusinessTypeToModelConverter()
	{
		return crBusinessTypeToModelConverter;
	}

	/**
	 * @return the crCancellationToModelConverter
	 */
	protected Converter<CrCancellation, CrCancellationModel> getCrCancellationToModelConverter()
	{
		return crCancellationToModelConverter;
	}

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCrNumber());
		target.setCrName(source.getCrName());
		target.setCrNumber(source.getCrNumber());
		target.setCrEntityNumber(source.getCrEntityNumber());
		target.setIssueDate(source.getIssueDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setCrMainNumber(source.getCrMainNumber());
		populateBusinessType(source, target);
		populateFiscalYear(source, target);
		populateCRStatus(source, target);
		populateCancellation(source, target);
		populateLocation(source, target);
		populateCompany(source, target);
		populateActivities(source, target);

	}


	/**
	 *
	 */
	private void populateLocation(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		if (source.getLocation() == null)
		{
			return;
		}

		CrLocationModel crLocationModel = target.getLocation();
		if (crLocationModel == null)
		{
			crLocationModel = getModelService().create(CrLocationModel.class);
		}

		target.setLocation(getCrLocationToModelConverter().convert(source.getLocation(), crLocationModel));

	}

	/**
	 *
	 */
	private void populateFiscalYear(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{

		if (source.getFiscalYear() == null)
		{
			return;
		}

		CrFiscalYearModel fiscalYear = target.getFiscalYear();
		if (fiscalYear == null)
		{
			fiscalYear = getModelService().create(CrFiscalYearModel.class);
		}

		target.setFiscalYear(getCrFiscalYearToModelConverter().convert(source.getFiscalYear(), fiscalYear));
	}

	/**
	 *
	 */
	private void populateActivities(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{

		if (source.getActivities() == null)
		{
			return;
		}

		CrActivitiesModel activities = target.getActivities();
		if (activities == null)
		{
			activities = getModelService().create(CrActivitiesModel.class);
		}

		target.setActivities(getCrActivitiesToModelConverter().convert(source.getActivities(), activities));

	}

	/**
	 *
	 */
	private void populateCRStatus(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		if (source.getStatus() == null)
		{
			return;
		}

		CrStatusModel status = target.getStatus();
		if (status == null)
		{
			status = getModelService().create(CrStatusModel.class);
		}

		final CrStatusModel crStatusModel = getCrStatusToModelConverter().convert(source.getStatus(), status);
		target.setStatus(crStatusModel);

	}

	/**
	 *
	 */
	private void populateCompany(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		if (source.getCompany() == null)
		{
			return;
		}

		CrCompanyModel company = target.getCompany();
		if (company == null)
		{
			company = getModelService().create(CrCompanyModel.class);
		}

		final CrCompanyModel companyModel = getCrCompanyToModelConverter().convert(source.getCompany(), company);
		target.setCompany(companyModel);

	}




	/**
	 *
	 */
	private void populateBusinessType(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		if (source.getBusinessType() == null)
		{
			return;
		}

		CrBusinessTypeModel businessType = target.getBusinessType();
		if (businessType == null)
		{
			businessType = getModelService().create(CrBusinessTypeModel.class);
		}

		final CrBusinessTypeModel crBusinessTypeModel = getCrBusinessTypeToModelConverter().convert(source.getBusinessType(),
				businessType);
		target.setBusinessType(crBusinessTypeModel);

	}




	/**
	 *
	 */
	private void populateCancellation(final CompanyRegistrationInfo source, final CompanyRegistrationInfoModel target)
	{
		if (source.getCancellation() == null)
		{
			return;
		}

		CrCancellationModel cancellation = target.getCancellation();
		if (cancellation == null)
		{
			cancellation = getModelService().create(CrCancellationModel.class);
		}

		final CrCancellationModel crCancellationModel = getCrCancellationToModelConverter().convert(source.getCancellation(),
				cancellation);
		target.setCancellation(crCancellationModel);
	}


}

