/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrCompany;
import com.masdar.commercialregistration.model.CrCompanyModel;


/**
 *
 */
public class CrCompanyToModelPopulator implements Populator<CrCompany, CrCompanyModel>
{

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrCompany source, final CrCompanyModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setEndDate(source.getEndDate());
		target.setPeriod(source.getPeriod());
		target.setStartDate(source.getStartDate());
	}


}
