/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrCancellation;
import com.masdar.commercialregistration.model.CrCancellationModel;



/**
 * The Class CrCancellationPopulator.
 */
public class CrCancellationToModelPopulator implements Populator<CrCancellation, CrCancellationModel>
{

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrCancellation source, final CrCancellationModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setReason(source.getReason());
		target.setDate(source.getDate());

	}

}
