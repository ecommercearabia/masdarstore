/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator.info;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrCalendarType;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.model.CrCalendarTypeModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;


/**
 *
 */
public class CrFiscalYearToBeanPopulator implements Populator<CrFiscalYearModel, CrFiscalYear>
{
	@Resource(name = "crCalendarToBeanConverter")
	private Converter<CrCalendarTypeModel, CrCalendarType> crCalendarToBeanConverter;

	/**
	 * @return the crCalendarToBeanConverter
	 */
	protected Converter<CrCalendarTypeModel, CrCalendarType> getCrCalendarToBeanConverter()
	{
		return crCalendarToBeanConverter;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrFiscalYearModel source, final CrFiscalYear target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDay(source.getDay());
		target.setMonth(source.getMonth());

		populateCalendar(source, target);
	}

	/**
	 * Populate calendar.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populateCalendar(final CrFiscalYearModel source, final CrFiscalYear target)
	{

		if (source.getCalendarType() == null)
		{
			return;
		}
		target.setCalendarType(getCrCalendarToBeanConverter().convert(source.getCalendarType()));
	}


}
