/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator.info;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrIsic;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrIsicModel;


/**
 * The Class CrActivitiesToModelPopulator.
 */
public class CrActivitiesToBeanPopulator implements Populator<CrActivitiesModel, CrActivities>
{
	@Resource(name = "crIsicToBeanConverter")
	private Converter<CrIsicModel, CrIsic> crIsicToBeanConverter;

	/**
	 * @return the crIsicToBeanConverter
	 */
	protected Converter<CrIsicModel, CrIsic> getCrIsicToBeanConverter()
	{
		return crIsicToBeanConverter;
	}


	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrActivitiesModel source, final CrActivities target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDescription(source.getDescription());
		populateIsic(source, target);
	}

	/**
	 *
	 */
	private void populateIsic(final CrActivitiesModel source, final CrActivities target)
	{
		final List<CrIsicModel> isic = source.getIsic();
		if (CollectionUtils.isEmpty(isic))
		{
			return;
		}

		final List<CrIsic> isicModelList = new ArrayList<>();
		for (final CrIsicModel crIsic : isic)
		{
			isicModelList.add(getCrIsicToBeanConverter().convert(crIsic));
		}

		target.setIsic(isicModelList);
	}
}
