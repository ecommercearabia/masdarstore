/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator.info;

/**
 *
 */
/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrBusinessType;
import com.masdar.commercialregistration.beans.CrCancellation;
import com.masdar.commercialregistration.beans.CrCompany;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.beans.CrLocation;
import com.masdar.commercialregistration.beans.CrStatus;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrBusinessTypeModel;
import com.masdar.commercialregistration.model.CrCancellationModel;
import com.masdar.commercialregistration.model.CrCompanyModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;
import com.masdar.commercialregistration.model.CrLocationModel;
import com.masdar.commercialregistration.model.CrStatusModel;



/**
 * The Class CompanyRegistrationInfoToModelPopulator.
 */
public class CompanyRegistrationInfoToBeanPopulator implements Populator<CompanyRegistrationInfoModel, CompanyRegistrationInfo>
{
	@Resource(name = "crActivitiesToBeanConverter")
	private Converter<CrActivitiesModel, CrActivities> crActivitiesToBeanConverter;

	@Resource(name = "crFiscalYearToBeanConverter")
	private Converter<CrFiscalYearModel, CrFiscalYear> crFiscalYearToBeanConverter;

	/** The cr company converter. */
	@Resource(name = "crCompanyToBeanConverter")
	private Converter<CrCompanyModel, CrCompany> crCompanyToBeanConverter;

	/** The cr business converter. */
	@Resource(name = "crBusinessTypeToBeanConverter")
	private Converter<CrBusinessTypeModel, CrBusinessType> crBusinessTypeToBeanConverter;

	/** The cr cancellation converter. */
	@Resource(name = "crCancellationToBeanConverter")
	private Converter<CrCancellationModel, CrCancellation> crCancellationToBeanConverter;

	@Resource(name = "crStatusToBeanConverter")
	private Converter<CrStatusModel, CrStatus> crStatusToBeanConverter;

	@Resource(name = "crLocationToBeanConverter")
	private Converter<CrLocationModel, CrLocation> crLocationToBeanConverter;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @return the crActivitiesToModelConverter
	 */
	protected Converter<CrActivitiesModel, CrActivities> getCrActivitiesToBeanConverter()
	{
		return crActivitiesToBeanConverter;
	}


	/**
	 * @return the crFiscalYearToModelConverter
	 */
	protected Converter<CrFiscalYearModel, CrFiscalYear> getCrFiscalYearToBeanConverter()
	{
		return crFiscalYearToBeanConverter;
	}

	/**
	 * @return the crLocationToModelConverter
	 */
	protected Converter<CrLocationModel, CrLocation> getCrLocationToBeanConverter()
	{
		return crLocationToBeanConverter;
	}

	/**
	 * @return the crStatusToModelConverter
	 */
	protected Converter<CrStatusModel, CrStatus> getCrStatusToBeanConverter()
	{
		return crStatusToBeanConverter;
	}

	/**
	 * @return the crCompanyToModelConverter
	 */
	protected Converter<CrCompanyModel, CrCompany> getCrCompanyToBeanConverter()
	{
		return crCompanyToBeanConverter;
	}

	/**
	 * @return the crBusinessTypeToModelConverter
	 */
	protected Converter<CrBusinessTypeModel, CrBusinessType> getCrBusinessTypeToBeanConverter()
	{
		return crBusinessTypeToBeanConverter;
	}

	/**
	 * @return the crCancellationToModelConverter
	 */
	protected Converter<CrCancellationModel, CrCancellation> getCrCancellationToBeanConverter()
	{
		return crCancellationToBeanConverter;
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCrNumber());
		target.setCrName(source.getCrName());
		target.setCrNumber(source.getCrNumber());
		target.setCrEntityNumber(source.getCrEntityNumber());
		target.setIssueDate(source.getIssueDate());
		target.setExpiryDate(source.getExpiryDate());
		target.setCrMainNumber(source.getCrMainNumber());
		populateBusinessType(source, target);
		populateFiscalYear(source, target);
		populateCRStatus(source, target);
		populateCancellation(source, target);
		populateLocation(source, target);
		populateCompany(source, target);
		populateActivities(source, target);

	}


	/**
	 *
	 */
	private void populateLocation(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		if (source.getLocation() == null)
		{
			return;
		}

		target.setLocation(getCrLocationToBeanConverter().convert(source.getLocation()));

	}

	/**
	 *
	 */
	private void populateFiscalYear(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{

		if (source.getFiscalYear() == null)
		{
			return;
		}

		target.setFiscalYear(getCrFiscalYearToBeanConverter().convert(source.getFiscalYear()));
	}

	/**
	 *
	 */
	private void populateActivities(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{

		if (source.getActivities() == null)
		{
			return;
		}

		target.setActivities(getCrActivitiesToBeanConverter().convert(source.getActivities()));

	}

	/**
	 *
	 */
	private void populateCRStatus(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		if (source.getStatus() == null)
		{
			return;
		}

		target.setStatus(getCrStatusToBeanConverter().convert(source.getStatus()));

	}

	/**
	 *
	 */
	private void populateCompany(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		if (source.getCompany() == null)
		{
			return;
		}

		target.setCompany(getCrCompanyToBeanConverter().convert(source.getCompany()));

	}




	/**
	 *
	 */
	private void populateBusinessType(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		if (source.getBusinessType() == null)
		{
			return;
		}

		target.setBusinessType(getCrBusinessTypeToBeanConverter().convert(source.getBusinessType()));

	}




	/**
	 *
	 */
	private void populateCancellation(final CompanyRegistrationInfoModel source, final CompanyRegistrationInfo target)
	{
		if (source.getCancellation() == null)
		{
			return;
		}

		target.setCancellation(getCrCancellationToBeanConverter().convert(source.getCancellation()));
	}


}

