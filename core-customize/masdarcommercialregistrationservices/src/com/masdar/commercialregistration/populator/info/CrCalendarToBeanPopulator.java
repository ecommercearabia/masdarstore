/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator.info;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrCalendarType;
import com.masdar.commercialregistration.model.CrCalendarTypeModel;

/**
 *
 */
public class CrCalendarToBeanPopulator implements Populator<CrCalendarTypeModel, CrCalendarType>
{

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrCalendarTypeModel source, final CrCalendarType target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setId(source.getId());
		target.setName(source.getName());

	}


}
