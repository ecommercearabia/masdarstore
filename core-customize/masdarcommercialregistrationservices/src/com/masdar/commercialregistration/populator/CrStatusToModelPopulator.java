/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrStatus;
import com.masdar.commercialregistration.model.CrStatusModel;


/**
 *
 */
public class CrStatusToModelPopulator implements Populator<CrStatus, CrStatusModel>
{

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrStatus source, final CrStatusModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setId(source.getId());
		target.setName(source.getName());
		target.setNameEn(source.getNameEn());

	}


}
