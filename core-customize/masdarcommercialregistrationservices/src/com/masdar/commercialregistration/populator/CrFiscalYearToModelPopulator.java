/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.commercialregistration.beans.CrCalendarType;
import com.masdar.commercialregistration.beans.CrFiscalYear;
import com.masdar.commercialregistration.model.CrCalendarTypeModel;
import com.masdar.commercialregistration.model.CrFiscalYearModel;


/**
 *
 */
public class CrFiscalYearToModelPopulator implements Populator<CrFiscalYear, CrFiscalYearModel>
{
	@Resource(name = "crCalendarToModelConverter")
	private Converter<CrCalendarType, CrCalendarTypeModel> crCalendarToModelConverter;

	/**
	 * @return the crCalendarToModelConverter
	 */
	protected Converter<CrCalendarType, CrCalendarTypeModel> getCrCalendarToModelConverter()
	{
		return crCalendarToModelConverter;
	}

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrFiscalYear source, final CrFiscalYearModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDay(source.getDay());
		target.setMonth(source.getMonth());

		populateCalendar(source, target);
	}

	/**
	 * Populate calendar.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populateCalendar(final CrFiscalYear source, final CrFiscalYearModel target)
	{

		if (source.getCalendarType() == null)
		{
			return;
		}

		CrCalendarTypeModel calendarType = target.getCalendarType();
		if (calendarType == null)
		{
			calendarType = getModelService().create(CrCalendarTypeModel.class);
		}
		target.setCalendarType(getCrCalendarToModelConverter().convert(source.getCalendarType(), calendarType));
	}


}
