/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.masdar.commercialregistration.beans.CrActivities;
import com.masdar.commercialregistration.beans.CrIsic;
import com.masdar.commercialregistration.model.CrActivitiesModel;
import com.masdar.commercialregistration.model.CrIsicModel;


/**
 * The Class CrActivitiesToModelPopulator.
 */
public class CrActivitiesToModelPopulator implements Populator<CrActivities, CrActivitiesModel>
{
	@Resource(name = "crIsicToModelConverter")
	private Converter<CrIsic, CrIsicModel> crIsicToModelConverter;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @return the crIsicToModelConverter
	 */
	protected Converter<CrIsic, CrIsicModel> getCrIsicToModelConverter()
	{
		return crIsicToModelConverter;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CrActivities source, final CrActivitiesModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDescription(source.getDescription());
		target.setLongDescription(source.getDescription());
		populateIsic(source, target);
	}

	/**
	 *
	 */
	private void populateIsic(final CrActivities source, final CrActivitiesModel target)
	{
		final List<CrIsic> isic = source.getIsic();
		if (CollectionUtils.isEmpty(isic))
		{
			return;
		}

		final List<CrIsicModel> isicModelList = new ArrayList<>();
		for (final CrIsic crIsic : isic)
		{
			final CrIsicModel crIsicModel = getModelService().create(CrIsicModel.class);

			isicModelList.add(getCrIsicToModelConverter().convert(crIsic, crIsicModel));
		}

		target.setIsic(isicModelList);
	}
}
