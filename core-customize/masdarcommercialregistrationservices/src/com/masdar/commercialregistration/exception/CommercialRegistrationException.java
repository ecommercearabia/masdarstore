package com.masdar.commercialregistration.exception;

import com.masdar.commercialregistration.exception.type.CommercialRegistrationExceptionType;


public class CommercialRegistrationException extends Exception
{

	private final CommercialRegistrationExceptionType commercialRegistrationExceptionType;


	public CommercialRegistrationException(final String message, final CommercialRegistrationExceptionType type)
	{
		super(message);
		this.commercialRegistrationExceptionType = type;
	}


	/**
	 * @return the loyaltyPaymentExceptionType
	 */
	public CommercialRegistrationExceptionType getCommercialRegistrationExceptionType()
	{
		return commercialRegistrationExceptionType;
	}

}
