/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.exception.type;

import java.util.HashMap;
import java.util.Map;


/**
 *
 */
public enum CommercialRegistrationExceptionType
{
	INTERNAL_ERROR("000", "Internal Error"),
	UNKNOWN_ERROR_CODE("001", "Internal Error"),
	ENTITY_NUMBER_NOTVALID("400.2.1", "Commercial Registration number or entity Number must be 10 digits"), 
	ENTITY_NUMBER_NOTVALID_FORMAT("400.2.2", "Commercial Registration Entity Number must be in valid format"),
	INPUT_DATA_EMPTY("400.2.3", "Input data is required"),
	UNABLE_TO_PROCESS_REQUEST("400.2.10", "Unable to Process the Request"),
	WATHQ_INTERNAL_ERROR("400.3.1", "Internal Error");

	private final String code;
	private final String msg;

	/**
	 *
	 */
	private CommercialRegistrationExceptionType(final String code, final String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}


	private static Map<String, CommercialRegistrationExceptionType> map = new HashMap<>();

	static
	{
		for (final CommercialRegistrationExceptionType wathqExceptionType : CommercialRegistrationExceptionType.values())
		{
			map.put(wathqExceptionType.code, wathqExceptionType);
		}
	}

	public static CommercialRegistrationExceptionType getExceptionFromCode(final String code)
	{
		final CommercialRegistrationExceptionType exception = map.get(code);

		return exception == null ? UNKNOWN_ERROR_CODE : exception;
	}


}
