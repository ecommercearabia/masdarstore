/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.commercialregistration.constants;

/**
 * Global class for all Masdarcommercialregistrationservices constants. You can add global constants for your extension into this class.
 */
public final class MasdarcommercialregistrationservicesConstants extends GeneratedMasdarcommercialregistrationservicesConstants
{
	public static final String EXTENSIONNAME = "masdarcommercialregistrationservices";

	private MasdarcommercialregistrationservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
