/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomaddon.jalo;

import com.masdar.masdarcustomercouponcustomaddon.constants.MasdarcustomercouponcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarcustomercouponcustomaddonManager extends GeneratedMasdarcustomercouponcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarcustomercouponcustomaddonManager.class.getName() );
	
	public static final MasdarcustomercouponcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarcustomercouponcustomaddonManager) em.getExtension(MasdarcustomercouponcustomaddonConstants.EXTENSIONNAME);
	}
	
}
