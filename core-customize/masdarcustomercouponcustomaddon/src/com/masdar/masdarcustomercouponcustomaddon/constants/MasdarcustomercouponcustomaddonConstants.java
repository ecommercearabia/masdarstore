/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomaddon.constants;

/**
 * Masdarcustomercouponcustomaddon constants
 */
public final class MasdarcustomercouponcustomaddonConstants extends GeneratedMasdarcustomercouponcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarcustomercouponcustomaddon";

	private MasdarcustomercouponcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
