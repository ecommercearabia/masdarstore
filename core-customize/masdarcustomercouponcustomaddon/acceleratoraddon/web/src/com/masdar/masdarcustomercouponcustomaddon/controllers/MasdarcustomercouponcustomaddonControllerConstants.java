/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomaddon.controllers;


/**
 */
public interface MasdarcustomercouponcustomaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/masdarcustomercouponcustomaddon";

	interface Views
	{
		interface Pages
		{
			interface COUPONS // NOSONAR
			{
				String ConponsPage = "/my-account/coupons"; // NOSONAR
			}

		}

		interface Fragments
		{
			interface Coupons
			{
				String CustomerCouponSubPage = ADDON_PREFIX + "/fragments/customer360/customerCouponSubPage"; // NOSONAR
			}
		}

	}

	/**
	 * Class with action name constants
	 */
	interface Actions
	{
		interface Cms // NOSONAR
		{
			String _Prefix = "/view/"; // NOSONAR
			String _Suffix = "Controller"; // NOSONAR
		}
	}

}
