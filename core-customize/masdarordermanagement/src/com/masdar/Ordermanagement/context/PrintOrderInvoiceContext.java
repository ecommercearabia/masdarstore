/**
 *
 */
package com.masdar.Ordermanagement.context;

import de.hybris.platform.acceleratorservices.document.context.AbstractDocumentContext;
import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.velocity.tools.generic.ConversionTool;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.NumberTool;

import com.masdar.masdarfacades.data.SupplierData;


/**
 * @author monzer
 *
 */
public class PrintOrderInvoiceContext extends AbstractDocumentContext<OrderProcessModel>
{
	private DateTool date;
	private NumberTool number;
	private EscapeTool escapeTool;
	private OrderModel order;
	private Date deliveryDate;
	private SupplierData supplierData;
	@Resource(name = "productFacade")
	private ProductFacade productFacade;
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	private String consignmentCodes;
	
	private OrderData orderDataEn;

	private OrderData orderDataAr;

	private CustomerData customerDataEn;

	private CustomerData customerDataAr;
	
	private String warehouseCodes;
	public static final String NUMBER_TOOL = "numberTool";
	
	private String consInvoiceNumbers;

	private Locale saLocale;

	private TimeZone saTimeZone;
	
	
	
	@Override
	public void init(final OrderProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		
		saLocale = Locale.forLanguageTag("ar_SA");
		saTimeZone = TimeZone.getTimeZone("Asia/Riyadh");
		
		this.order = businessProcessModel.getOrder();
		this.date = new DateTool();
		this.number = new NumberTool();
		this.escapeTool = new EscapeTool();
		this.deliveryDate = DateUtils.addDays(order.getDate(), 3);
		fillSupplierData(order);
		Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag("ar"));
		this.orderDataAr = orderFacade.getOrderDetailsForCode(order.getCode());
		this.customerDataAr = orderDataAr.getB2bCustomerData();
		i18nService.setCurrentLocale(Locale.forLanguageTag("en"));
		this.orderDataEn = orderFacade.getOrderDetailsForCode(order.getCode());
		this.customerDataEn = orderDataEn.getB2bCustomerData();
		i18nService.setCurrentLocale(currentLocale);
		this.warehouseCodes = fillAllWarehouseCodes(getOrder());
		put(NUMBER_TOOL, new NumberTool());
		fillConsignmentCodes(getOrder());
		fillConsignmentInvoiceNumbers(order);
	}

	private void fillConsignmentCodes(OrderModel order)
	{
		this.consignmentCodes = order.getConsignments().stream().map(consignment -> {
			String formattedCode = consignment.getCode().replace("cons", "D");
			return formattedCode.replace("_", "");
		}).collect(Collectors.joining(", "));
	}
	
	private void fillConsignmentInvoiceNumbers(OrderModel order)
	{
		this.consInvoiceNumbers = order.getConsignments().stream()
				.filter(consignment -> StringUtils.isNotBlank(consignment.getInvoiceNumber()))
				.map(consignment -> consignment.getInvoiceNumber()).collect(Collectors.joining(", "));
	}

	private void fillSupplierData(OrderModel order)
	{
		supplierData = new SupplierData();
		Locale arabicLocale = Locale.forLanguageTag("ar");
		supplierData.setSupplierNameEn(order.getStore().getSupplierName(Locale.ENGLISH));
		supplierData.setSupplierNameAr(order.getStore().getSupplierName(arabicLocale));
		supplierData.setSupplierAddressEn(order.getStore().getSupplierAddress(Locale.ENGLISH));
		supplierData.setSupplierAddressAr(order.getStore().getSupplierAddress(arabicLocale));
		supplierData.setSupplierEmail(order.getStore().getSupplierEmail());
		supplierData.setSupplierPhoneNumber(order.getStore().getSupplierPhoneNumber());
		supplierData.setSupplierVatId(order.getStore().getSupplierVatId());
	}
	
	private String fillAllWarehouseCodes(OrderModel order) {
		String warehousing = order.getConsignments().stream()
				.filter(consignment -> consignment.getWarehouse() != null
				&& StringUtils.isNotBlank(consignment.getWarehouse().getCode()))
				.map(consignment -> consignment.getWarehouse().getCode()).collect(Collectors.joining(", "));
		return warehousing;
	}

	@Override
	protected BaseSiteModel getSite(final OrderProcessModel businessProcessModel)
	{
		return businessProcessModel.getOrder().getSite();
	}

	@Override
	protected LanguageModel getDocumentLanguage(final OrderProcessModel businessProcessModel)
	{
		return businessProcessModel.getOrder().getSite().getDefaultLanguage();
	}

	public String getProductImageURL(OrderEntryModel orderEntryModel)
	{
		String path = null;
		MediaModel media = orderEntryModel.getProduct().getThumbnail();
		if (media != null)
		{

			path = media.getDownloadURL();
		}
		return path;
	}

	public String getProductDescription(final String productCode, String lang)
	{
		if (productCode == null)
		{
			return null;
		}
		Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(lang));
		
		ProductData product = null;

		try
		{
			product = productFacade.getProductForCodeAndOptions(productCode,
					Arrays.asList(ProductOption.BASIC, ProductOption.DESCRIPTION));

		}
		catch (UnknownIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);

		return product == null ? null : product.getDescription();

	}
	
	public double roundPrice(final BigDecimal price, final int digits)
	{
		final double digitsWeight1 = Math.pow(10, digits + 1);
		final double rounded = Math.round(price.doubleValue() * digitsWeight1) / digitsWeight1;
		final double digitsWeight2 = Math.pow(10, digits);
		return Math.round(rounded * digitsWeight2) / digitsWeight2;
	}

	/**
	 * @return the order
	 */
	public OrderModel getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *                 the order to set
	 */
	public void setOrder(OrderModel order)
	{
		this.order = order;
	}

	public String escapeHtml(String stringToEscape)
	{
		return this.escapeTool.html(stringToEscape);
	}

	public DateTool getDate()
	{
		return this.date;
	}

	public NumberTool getNumber()
	{
		return this.number;
	}

	/**
	 * @return the escapeTool
	 */
	public EscapeTool getEscapeTool()
	{
		return escapeTool;
	}

	/**
	 * @return the customerDataEn
	 */
	public CustomerData getCustomerDataEn()
	{
		return customerDataEn;
	}

	/**
	 * @param customerDataEn
	 *                          the customerDataEn to set
	 */
	public void setCustomerDataEn(CustomerData customerDataEn)
	{
		this.customerDataEn = customerDataEn;
	}

	/**
	 * @return the customerDataAr
	 */
	public CustomerData getCustomerDataAr()
	{
		return customerDataAr;
	}

	/**
	 * @param customerDataAr
	 *                          the customerDataAr to set
	 */
	public void setCustomerDataAr(CustomerData customerDataAr)
	{
		this.customerDataAr = customerDataAr;
	}

	/**
	 * @param escapeTool
	 *                      the escapeTool to set
	 */
	public void setEscapeTool(EscapeTool escapeTool)
	{
		this.escapeTool = escapeTool;
	}

	/**
	 * @return the orderFacade
	 */
	public OrderFacade getOrderFacade()
	{
		return orderFacade;
	}

	/**
	 * @param orderFacade
	 *                       the orderFacade to set
	 */
	public void setOrderFacade(OrderFacade orderFacade)
	{
		this.orderFacade = orderFacade;
	}



	/**
	 * @return the orderDataEn
	 */
	public OrderData getOrderDataEn()
	{
		return orderDataEn;
	}

	/**
	 * @param orderDataEn
	 *                       the orderDataEn to set
	 */
	public void setOrderDataEn(OrderData orderDataEn)
	{
		this.orderDataEn = orderDataEn;
	}

	/**
	 * @param date
	 *                the date to set
	 */
	public void setDate(DateTool date)
	{
		this.date = date;
	}

	/**
	 * @param number
	 *                  the number to set
	 */
	public void setNumber(NumberTool number)
	{
		this.number = number;
	}

	/**
	 * @return the orderDataAr
	 */
	public OrderData getOrderDataAr()
	{
		return orderDataAr;
	}

	/**
	 * @param orderDataAr
	 *                       the orderDataAr to set
	 */
	public void setOrderDataAr(OrderData orderDataAr)
	{
		this.orderDataAr = orderDataAr;
	}

	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}

	/**
	 * @param deliveryDate
	 *                        the deliveryDate to set
	 */
	public void setDeliveryDate(Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the supplierData
	 */
	public SupplierData getSupplierData()
	{
		return supplierData;
	}

	/**
	 * @param supplierData
	 *                        the supplierData to set
	 */
	public void setSupplierData(SupplierData supplierData)
	{
		this.supplierData = supplierData;
	}

	/**
	 * @return the warehouseCodes
	 */
	public String getWarehouseCodes()
	{
		return warehouseCodes;
	}

	/**
	 * @param warehouseCodes the warehouseCodes to set
	 */
	public void setWarehouseCodes(String warehouseCodes)
	{
		this.warehouseCodes = warehouseCodes;
	}

	/**
	 * @return the consignmentCodes
	 */
	public String getConsignmentCodes()
	{
		return consignmentCodes;
	}

	/**
	 * @param consignmentCodes the consignmentCodes to set
	 */
	public void setConsignmentCodes(String consignmentCodes)
	{
		this.consignmentCodes = consignmentCodes;
	}

	/**
	 * @return the consInvoiceNumbers
	 */
	public String getConsInvoiceNumbers()
	{
		return consInvoiceNumbers;
	}

	/**
	 * @param consInvoiceNumbers the consInvoiceNumbers to set
	 */
	public void setConsInvoiceNumbers(String consInvoiceNumbers)
	{
		this.consInvoiceNumbers = consInvoiceNumbers;
	}

	/**
	 * @return the saLocale
	 */
	public Locale getSaLocale()
	{
		return saLocale;
	}

	/**
	 * @param saLocale the saLocale to set
	 */
	public void setSaLocale(Locale saLocale)
	{
		this.saLocale = saLocale;
	}

	/**
	 * @return the saTimeZone
	 */
	public TimeZone getSaTimeZone()
	{
		return saTimeZone;
	}

	/**
	 * @param saTimeZone the saTimeZone to set
	 */
	public void setSaTimeZone(TimeZone saTimeZone)
	{
		this.saTimeZone = saTimeZone;
	}
	
}
