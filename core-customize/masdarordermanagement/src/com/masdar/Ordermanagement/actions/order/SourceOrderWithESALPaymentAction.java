package com.masdar.Ordermanagement.actions.order;

import java.util.Collection;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.warehousing.data.sourcing.SourcingResults;


public class SourceOrderWithESALPaymentAction extends SourceOrderAction
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceOrderWithESALPaymentAction.class);

	@Override
	public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		LOGGER.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final OrderModel order = process.getOrder();

		boolean partialFulfillment = false;
		boolean failedFulfillment = true;

		SourcingResults results = null;
		try
		{
			results = getSourcingService().sourceOrder(order);
		}
		catch (final IllegalArgumentException e) //NOSONAR
		{
			LOGGER.info(e.getMessage());
			LOGGER.info("Could not create SourcingResults. Changing order status to SUSPENDED");
		}

		if (results != null)
		{
			results.getResults().forEach(this::logSourcingInfo);

			final Collection<ConsignmentModel> consignments = getAllocationService().createConsignments(process.getOrder(),
					"cons" + process.getOrder().getCode(), results);
			LOGGER.debug("Number of consignments created during allocation: {}", consignments.size());
			startConsignmentSubProcess(consignments, process);
			if (!PaymentStatus.PAID.equals(order.getPaymentStatus()) && !order.isPaymentB2BInvoiceCreatedSuccessfully())
			{
				order.setStatus(OrderStatus.PENDING);
				order.setPaymentStatus(PaymentStatus.NOTPAID);
			}
			partialFulfillment = order.getEntries().stream()
					.anyMatch(orderEntry -> ((OrderEntryModel) orderEntry).getQuantityUnallocated().longValue() > 0);
			failedFulfillment = order.getEntries().stream()
					.allMatch(orderEntry -> ((OrderEntryModel) orderEntry).getQuantityAllocated().longValue() == 0);
		}

		if (results == null || failedFulfillment)
		{
			LOGGER.info("Order failed to be sourced");
			order.setStatus(OrderStatus.SUSPENDED);
		}
		else if (partialFulfillment)
		{
			LOGGER.info("Order partially sourced");
			order.setStatus(OrderStatus.SUSPENDED);
		}
		else
		{
			LOGGER.info("Order was successfully sourced");
		}
		getModelService().save(order);
	}


}
