/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.masdar.Ordermanagement.actions.order;

import java.util.Date;
import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.Ordermanagement.actions.order.PaymentMethodCheckAction.Transition;
import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.enums.PaymentProviderStatus;
import com.masdar.masdarb2bpayment.service.PaymentB2BWebServiceRecordService;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;


/**
 * This example action checks the order for required data in the business process. Skipping this action may result in
 * failure in one of the subsequent steps of the process. The relation between the order and the business process is
 * defined in basecommerce extension through item OrderProcess. Therefore if your business process has to access the
 * order (a typical case), it is recommended to use the OrderProcess as a parentClass instead of the plain
 * BusinessProcess.
 */
public class CreateESALB2CInvoiceAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateESALB2CInvoiceAction.class);


	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;

	@Resource(name = "paymentB2BWebServiceRecordService")
	private PaymentB2BWebServiceRecordService paymentB2BWebServiceRecordService;

	@Override
	public void executeAction(OrderProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final OrderModel order = process.getOrder();

		if (Objects.isNull(order) || Objects.isNull(order.getPaymentMode())
				|| !"ESAL".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{
			LOG.info("Missing the order or payment method is not supported , exiting the process");
			return;
		}
		if (order.isPaymentB2BInvoiceCreatedSuccessfully())
		{
			LOG.info("invoice already created");
			return;
		}

		try

		{
			paymentB2BContext.createInvoice(order);
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage());
			order.setPaymentProviderStatus(PaymentProviderStatus.FAILED);
		}
		modelService.save(order);
	}

}
