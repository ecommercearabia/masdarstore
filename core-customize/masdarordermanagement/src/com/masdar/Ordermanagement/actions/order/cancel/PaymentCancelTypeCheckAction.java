/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.masdar.Ordermanagement.actions.order.cancel;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;


/**
 * Creates the required events from the inventory when a cancellation is requested and process the cancellation
 */
public class PaymentCancelTypeCheckAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PaymentCancelTypeCheckAction.class);

	protected enum Transition
	{
		NORMAL_FULL, NORMAL_PARTIAL, ESAL;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	@Override
	public String execute(OrderProcessModel process) throws Exception
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		OrderModel order = process.getOrder();
		CheckoutPaymentType paymentType = order.getPaymentType();
		OrderStatus status = order.getStatus();
		if (!CheckoutPaymentType.ESAL.equals(paymentType) && OrderStatus.CANCELLED.equals(status) )
		{
			return Transition.NORMAL_FULL.toString();
		}
		if (!CheckoutPaymentType.ESAL.equals(paymentType) && OrderStatus.READY.equals(status))
		{
			return Transition.NORMAL_PARTIAL.toString();
		}

		if (OrderStatus.CANCELLED.equals(status))
		{
			order.setPaymentProviderTransactionType(PaymentProviderTransactionType.FULL_CANCEL);
		}
		else
		{
			order.setPaymentProviderTransactionType(PaymentProviderTransactionType.PARTIAL_CANCEL);
		}
		getModelService().save(order);
		return Transition.ESAL.toString();

	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
