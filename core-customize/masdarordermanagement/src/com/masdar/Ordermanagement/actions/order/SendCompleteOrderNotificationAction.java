/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.masdar.Ordermanagement.actions.order;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.masdarotp.context.OTPContext;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.OrderCompletedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.task.RetryLaterException;


/**
 * This example action checks the order for required data in the business process. Skipping this action may result in
 * failure in one of the subsequent steps of the process. The relation between the order and the business process is
 * defined in basecommerce extension through item OrderProcess. Therefore if your business process has to access the
 * order (a typical case), it is recommended to use the OrderProcess as a parentClass instead of the plain
 * BusinessProcess.
 */
public class SendCompleteOrderNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendCompleteOrderNotificationAction.class);

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;


	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Override
	public void executeAction(OrderProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		OrderModel order = process.getOrder();
		Preconditions.checkNotNull(order, String.format("Order For [%s]  is null", process.getCode()));
		Preconditions.checkNotNull(order.getStore(), String.format("Store For [%s] Order is null", order.getCode()));
		BaseStoreModel store = order.getStore();
		if (!store.isEnableOrderCompletedMessage())
		{
			return;
		}

		if (store.isEnableOrderCompletedSMSMessage() && !order.isSentOrderCompletedSMSNotification())
		{
			try
			{
				getOtpContext().sendOrderCompletedSMSMessage(order);
				order.setSentOrderCompletedSMSNotification(true);
				getModelService().save(order);
			}
			catch (Exception e)
			{
				LOG.error(e.getMessage());
			}
		}

		if (store.isEnableOrderCompletedEmailMessage() && !order.isSentOrderCompletedEmailNotification())
		{

			final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
					"orderCompletedEmailProcess-" + order.getCode() + "-" + System.currentTimeMillis(), "orderCompletedEmailProcess");
			orderProcessModel.setOrder(order);
			getModelService().save(orderProcessModel);
			getBusinessProcessService().startProcess(orderProcessModel);

			order.setSentOrderCompletedEmailNotification(true);
			getModelService().save(order);
		}

	}

	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @return the eventService
	 */
	protected EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @return the otpContext
	 */
	protected OTPContext getOtpContext()
	{
		return otpContext;
	}

}
