package com.masdar.Ordermanagement.actions.order;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarpayment.enums.PaymentModeType;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;


/**
 * Verifies whether order is cancelled completely or all the consignment processes are complete or not and updates the Order status/delivery status to
 * reflect this.
 */
public class PaymentMethodCheckAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PaymentMethodCheckAction.class);

	@Override
	public String execute(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		LOG.debug("Process: {} is checking for order payment type and {} subprocess results", process.getCode(),
				process.getConsignmentProcesses().size());

		final OrderModel order = process.getOrder();

		 PaymentModeModel paymentMode = order.getPaymentMode();

		if(Objects.isNull(paymentMode) || Objects.isNull(paymentMode.getCode()))
		{
			LOG.info("Process: {} in step {} , paymentType {}", process.getCode(), getClass().getSimpleName(), "null");

			return Transition.NORMAL.toString();
		}
		
		LOG.info("Process: {} in step {} , paymentType {}", process.getCode(), getClass().getSimpleName(), paymentMode.getCode());
		
		return "ESAL".equalsIgnoreCase(paymentMode.getCode())?Transition.ESAL.toString() :Transition.NORMAL.toString();
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	protected enum Transition
	{
		NORMAL,ESAL;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}
}
