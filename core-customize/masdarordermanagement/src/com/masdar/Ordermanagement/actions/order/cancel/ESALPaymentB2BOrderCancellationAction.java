/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.masdar.Ordermanagement.actions.order.cancel;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarb2bpayment.context.PaymentB2BContext;
import com.masdar.masdarb2bpayment.enums.PaymentProviderTransactionType;

import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;


/**
 * Creates the required events from the inventory when a cancellation is requested and process the cancellation
 */
public class ESALPaymentB2BOrderCancellationAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ESALPaymentB2BOrderCancellationAction.class);

	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;

	protected enum Transition
	{
		FULL, PARTIAL, FAILED_FULL, FAILED_PARTIAL,REFUND;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	@Override
	public String execute(OrderProcessModel process) throws Exception
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		OrderModel order = process.getOrder();

		 PaymentStatus paymentStatus = order.getPaymentStatus();
		if(PaymentStatus.PAID.equals(paymentStatus)) {
			return  Transition.REFUND.toString();
		}
		

		boolean cancelOrder = paymentB2BContext.cancelOrder(order);
		String transition;

		if (cancelOrder && PaymentProviderTransactionType.FULL_CANCEL.equals(order.getPaymentProviderTransactionType()))
		{
			transition = Transition.FULL.toString();
		}
		else if (!cancelOrder && PaymentProviderTransactionType.FULL_CANCEL.equals(order.getPaymentProviderTransactionType()))
		{
			transition = Transition.FAILED_FULL.toString();
		}
		else if (cancelOrder && PaymentProviderTransactionType.PARTIAL_CANCEL.equals(order.getPaymentProviderTransactionType()))
		{
			transition = Transition.PARTIAL.toString();
		}
		else
		{
			transition = Transition.FAILED_PARTIAL.toString();
		}

		getModelService().save(order);
		return transition;
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
