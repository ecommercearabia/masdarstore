package com.masdar.Ordermanagement.actions.order;

import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Verifies whether order is cancelled completely or all the consignment processes are complete or not and updates the Order status/delivery status to
 * reflect this.
 */
public class ESALPaymentAction// extends AbstractAction<OrderProcessModel>
{
	/*
	 * private static final Logger LOG = LoggerFactory.getLogger(ESALPaymentAction.class);
	 * 
	 * @Override public String execute(final OrderProcessModel process) { LOG.info("Process: {} in step {}",
	 * process.getCode(), getClass().getSimpleName());
	 * LOG.debug("Process: {} is checking for order payment type and {} subprocess results", process.getCode(),
	 * process.getConsignmentProcesses().size());
	 * 
	 * final OrderModel order = process.getOrder();
	 * if(Strings.isBlank(order.getPaymentB2BInvoiceId())&&!order.isPaymentB2BInvoiceCreatedSuccessfully()) { return
	 * Transition.CREATE.toString(); } return Transition.WAITING.toString(); }
	 * 
	 * @Override public Set<String> getTransitions() { return Transition.getStringValues(); }
	 * 
	 * protected enum Transition { CREATE , WAITING;
	 * 
	 * public static Set<String> getStringValues() { final Set<String> res = new HashSet<>();
	 * 
	 * for (final Transition transition : Transition.values()) { res.add(transition.toString()); } return res; } }
	 */
}
