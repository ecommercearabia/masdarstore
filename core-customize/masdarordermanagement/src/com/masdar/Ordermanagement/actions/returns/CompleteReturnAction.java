/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.Ordermanagement.actions.returns;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.returns.OrderReturnRecordHandler;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;


/**
 * Update the {@link ReturnRequestModel} status to COMPLETED and finalize the corresponding
 * {@link OrderReturnRecordModel} for this {@link ReturnRequestModel}<br/>
 */
public class CompleteReturnAction extends AbstractSimpleDecisionAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CompleteReturnAction.class);
	private OrderReturnRecordHandler orderReturnRecordsHandler;
	@Resource(name = "scpiService")
	private SCPIService scpiService;

	@Override
	public Transition executeAction(final ReturnProcessModel process) throws Exception
	{
		LOG.debug("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final ReturnRequestModel returnRequest = process.getReturnRequest();
		returnRequest.setStatus(ReturnStatus.COMPLETED);
		returnRequest.getReturnEntries().forEach(entry -> {
			entry.setStatus(ReturnStatus.COMPLETED);
			getModelService().save(entry);
		});
		getModelService().save(returnRequest);


		if (!sendSCPIApproveReturn(returnRequest))
		{
			return Transition.NOK;
		}


		getOrderReturnRecordsHandler().finalizeOrderReturnRecordForReturnRequest(returnRequest);
		return Transition.OK;
	}


	private boolean sendSCPIApproveReturn(ReturnRequestModel returnRequest)
	{
		if (!returnRequest.getOrder().getStore().isScpiSendReturnActionEnable())
		{
			return true;
		}

		try
		{
			scpiService.sendSCPIApproveReturn(returnRequest);
		}
		catch (SCPIException e)
		{
			LOG.error(e.getMessage());
			return false;
		}

		return true;
	}


	protected OrderReturnRecordHandler getOrderReturnRecordsHandler()
	{
		return orderReturnRecordsHandler;
	}

	@Required
	public void setOrderReturnRecordsHandler(OrderReturnRecordHandler orderReturnRecordsHandler)
	{
		this.orderReturnRecordsHandler = orderReturnRecordsHandler;
	}
}
