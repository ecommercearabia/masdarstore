package com.masdar.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.task.RetryLaterException;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class SendSCPIConsignmentAction extends AbstractSimpleDecisionAction<ConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendSCPIConsignmentAction.class);

	@Resource(name = "scpiService")
	private SCPIService scpiService;




	public SCPIService getScpiService()
	{
		return scpiService;
	}


	@Override
	public Transition executeAction(ConsignmentProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		ConsignmentModel consignment = process.getConsignment();
		Preconditions.checkNotNull(consignment, String.format("Consigment For [%s] is null", process.getCode()));
		Preconditions.checkNotNull(consignment.getOrder(),
				String.format("Order For [%s] consignment is null", consignment.getCode()));
		Preconditions.checkNotNull(consignment.getOrder().getStore(),
				String.format("Store For [%s] Order is null", consignment.getOrder().getCode()));

		LOG.info("{}: Sending SCPI SalesOrder for {}", this.getClass().getSimpleName(), consignment.getCode());

		Transition result = Transition.OK;

		BaseStoreModel store = consignment.getOrder().getStore();
		if (!store.isScpiSendOrderActionEnable())
		{
			return result;
		}

		try
		{
			if (consignment.getOrder().getStore().isEnabelCheckSCPISendOrder())
			{
				if (!consignment.getOrder().isScpiSentOrder())
				{
					getScpiService().sendSCPIOrder(consignment);
					if (store.isScpiSaveSuccessSendOrder() && consignment.getOrder() instanceof OrderModel)
					{
						final OrderModel orderModel = (OrderModel) consignment.getOrder();
						modelService.refresh(orderModel);
						orderModel.setScpiSentOrder(true);
						modelService.save(orderModel);
					}
				}
			}
			else
			{
				getScpiService().sendSCPIOrder(consignment);
			}
			if (store.isScpiSendConsginmentActionEnable())
			{
				getScpiService().sendSCPIConsignment(consignment);
			}
		}
		catch (final Exception  e)
		{
			e.printStackTrace();
			LOG.error("Error Message: " + e.getMessage());
			result = Transition.NOK;
		}
		

		return result;
	}

}
