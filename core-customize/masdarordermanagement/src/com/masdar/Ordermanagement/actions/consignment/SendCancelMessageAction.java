package com.masdar.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.log4j.Logger;

import com.masdar.Ordermanagement.events.SendOrderCancelEmailEvent;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SendCancelMessageAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendCancelMessageAction.class);

	@Resource(name = "eventService")
	private EventService eventService;
	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.
	 * BusinessProcessModel)
	 */
	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());		
		getEventService().publishEvent(new SendOrderCancelEmailEvent(process));
	}
	

}
