package com.masdar.Ordermanagement.actions.consignment;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.masdar.core.event.ConsignmentShippedEvent;
import com.masdar.masdarotp.context.OTPContext;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class SendConfirmShipConsignmentNotificationAction extends AbstractProceduralAction<ConsignmentProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendConfirmShipConsignmentNotificationAction.class);

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.
	 * BusinessProcessModel)
	 */
	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		ConsignmentModel consignment = process.getConsignment();
		Preconditions.checkNotNull(consignment, String.format("Consigment For [%s] is null", process.getCode()));
		Preconditions.checkNotNull(consignment.getOrder(),
				String.format("Order For [%s] consignment is null", consignment.getCode()));
		Preconditions.checkNotNull(consignment.getOrder().getStore(),
				String.format("Store For [%s] Order is null", consignment.getOrder().getCode()));


		BaseStoreModel store = consignment.getOrder().getStore();

		if (!store.isEnableConsignmentShippedMessage())
		{
			return;
		}

		if (store.isEnableOrderCompletedSMSMessage() && !consignment.isSentConfirmShipSMSNotification())
		{
			try
			{
				getOtpContext().sendShippingConfirmationSMSMessage(consignment);
				consignment.setSentConfirmShipSMSNotification(true);
				getModelService().save(consignment);
			}
			catch (Exception e)
			{
				LOG.error(e.getMessage());
			}
		}

		if (store.isEnableOrderCompletedEmailMessage() && !consignment.isSentConfirmShipEmailNotification())
		{

			final ConsignmentProcessModel consignmentProcessModel = getBusinessProcessService().createProcess(
					"sendConsignmentShippedEmailProcess-" + consignment.getCode() + "-" + System.currentTimeMillis(),
					"sendConsignmentShippedEmailProcess");
			consignmentProcessModel.setConsignment(consignment);
			getModelService().save(consignmentProcessModel);
			getBusinessProcessService().startProcess(consignmentProcessModel);

			consignment.setSentConfirmShipEmailNotification(true);
			getModelService().save(consignment);
		}

	}


	/**
	 * @return the eventService
	 */
	protected EventService getEventService()
	{
		return eventService;
	}


	/**
	 * @return the otpContext
	 */
	protected OTPContext getOtpContext()
	{
		return otpContext;
	}


	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}



}
