# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
INSERT_UPDATE DynamicProcessDefinition;code[unique=true];active;content
;order-process;true;"<process xmlns='http://www.hybris.de/xsd/processdefinition' start='checkOrder'
name='order-process' processClass='de.hybris.platform.orderprocessing.model.OrderProcessModel'>

<!-- Check Order -->
<action id='checkOrder' bean='checkOrderAction'>
    <transition name='OK' to='fraudCheck'/>
    <transition name='NOK' to='error'/>
</action>

<!-- Fraud Check -->
<action id='fraudCheck' bean='fraudCheckOrderInternalAction'>
    <transition name='OK' to='sendOrderPlacedNotification'/>
    <transition name='POTENTIAL' to='manualOrderCheckCSA'/>
    <transition name='FRAUD' to='cancelOrder'/>
</action>

<!-- Fraud Check : OK -->
<action id='sendOrderPlacedNotification' bean='sendOrderPlacedNotificationAction'>
    <transition name='OK' to='geocodeShippingAddress'/>
</action>

<!-- Fraud Check : FRAUD -->
<action id='cancelOrder' bean='cancelOrderAction'>
    <transition name='OK' to='notifyCustomer'/>
</action>

<action id='notifyCustomer' bean='notifyCustomerAboutFraudAction'>
<transition name='OK' to='failed'/>
</action>

<!-- Fraud Check : POTENTIAL -->
<action id='manualOrderCheckCSA' bean='prepareOrderForManualCheckAction'>
    <transition name='OK' to='waitForManualOrderCheckCSA'/>
</action>

<wait id='waitForManualOrderCheckCSA' then='orderManualChecked' prependProcessCode='true'>
    <event>CSAOrderVerified</event>
</wait>

<action id='orderManualChecked' bean='orderManualCheckedAction'>
    <transition name='OK' to='sendOrderPlacedNotification'/>
    <transition name='NOK' to='cancelOrder'/>
    <transition name='CANCELLED' to='success'/>
</action>

<!-- Sourcing and Allocation -->
<action id='geocodeShippingAddress' bean='geocodeShippingAddressAction'>
    <transition name='OK' to='paymentTypeCheck'/>
</action>

<action id='paymentTypeCheck' bean='paymentTypeCheckAction'>
    <transition name='NORMAL' to='sourceOrder'/>
    <transition name='ESAL' to='sourceOrderWithESALPayment'/>
</action>


<!-- ESAL Actions -->

<action id='sourceOrderWithESALPayment' bean='sourceOrderWithESALPaymentAction'>
    <transition name='OK' to='createESALInvoice'/>
</action>

<action id='createESALInvoice' bean='createESALInvoiceAction'>
    <transition name='OK' to='waitForOrderAction'/>
</action>

<action id='sourceOrder' bean='sourceOrderAction'>
    <transition name='OK' to='waitForOrderAction'/>
</action>

<!-- Wait to perform action on Order -->
<wait id='waitForOrderAction' prependProcessCode='true' then='failed'>
    <case event='OrderActionEvent'>
        <choice id='consignmentProcessEnded' then='verifyOrderCompletion'/>
        <choice id='cancelOrder' then='processOrderCancellation'/>
        <choice id='cancelled' then='success'/>
        <choice id='reSource' then='paymentTypeCheck'/>
        <choice id='putOnHold' then='putOrderOnHold'/>
        <choice id='createESALInvoice' then='createESALInvoice'/>
    </case>
</wait>

<!-- Wait for order cancellation to be completed -->
<action id='processOrderCancellation' bean='processOrderCancellationAction'>
    <transition name='OK' to='paymentCancelTypeCheck'/>
    <transition name='WAIT' to='paymentCancelTypeCheck'/>
    <transition name='SOURCING' to='paymentTypeCheck'/>
</action>
<action id='paymentCancelTypeCheck' bean='paymentCancelTypeCheckAction'>
    <transition name='NORMAL_FULL' to='verifyOrderCompletion'/>
    <transition name='NORMAL_PARTIAL' to='waitForOrderAction'/>
    <transition name='ESAL' to='esalPaymentB2BOrderCancellation'/>
</action>
<action id='esalPaymentB2BOrderCancellation' bean='esalPaymentB2BOrderCancellationAction'>
    <transition name='FULL' to='verifyOrderCompletion'/>
    <transition name='PARTIAL' to='waitForOrderAction'/>
    <transition name='FAILED_FULL' to='verifyOrderCompletion'/>
    <transition name='FAILED_PARTIAL' to='waitForOrderAction'/>
    <transition name='REFUND' to='esalRefund'/>
</action>

<action id='esalRefund' bean='esalRefundAction'>
    <transition name='FULL' to='verifyOrderCompletion'/>
    <transition name='PARTIAL' to='waitForOrderAction'/>
</action>
<action id='verifyOrderCompletion' bean='verifyOrderCompletionAction'>
    <transition name='OK' to='postTaxes'/>
    <transition name='WAIT' to='waitForOrderAction'/>
    <transition name='CANCELLED' to='success'/>
</action>

<action id='putOrderOnHold' bean='putOrderOnHoldAction'>
    <transition name='OK' to='waitForOrderAction'/>
</action>

<!-- Tax and Payment -->
<action id='postTaxes' bean='postTaxesAction'>
    <transition name='OK' to='takePayment'/>
</action>

<action id='takePayment' bean='takePaymentAction'>
    <transition name='OK' to='completeOrder'/>
    <transition name='NOK' to='sendPaymentFailedNotification'/>
</action>

<action id='completeOrder' bean='completeOrderAction'>
    <transition name='OK' to='sendCompleteOrderNotification'/>
</action>

<action id='sendCompleteOrderNotification' bean='sendCompleteOrderNotification'>
    <transition name='OK' to='success'/>
</action>

<action id='sendPaymentFailedNotification' bean='sendPaymentFailedNotificationAction'>
    <transition name='OK' to='failed'/>
</action>

<end id='error' state='ERROR'>Order process error.</end>
<end id='failed' state='FAILED'>Order process failed.</end>
<end id='success' state='SUCCEEDED'>Order process completed.</end>

</process>"

