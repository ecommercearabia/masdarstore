/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.masdarb2bordermanagementcustomwebservices.constants;


/**
 * Global class for all Masdarb2bordermanagementcustomwebservices constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bordermanagementcustomwebservicesConstants extends GeneratedMasdarb2bordermanagementcustomwebservicesConstants
{
	public static final String EXTENSIONNAME = "masdarb2bordermanagementcustomwebservices";

	private Masdarb2bordermanagementcustomwebservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
