/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationcustomaddon.constants;

/**
 * Global class for all Masdarpersonalizationcustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class MasdarpersonalizationcustomaddonConstants extends GeneratedMasdarpersonalizationcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarpersonalizationcustomaddon";

	private MasdarpersonalizationcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
