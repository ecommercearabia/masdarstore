//
// Decompiled by Procyon v0.5.36
//

package com.masdar.masdarpersonalizationcustomaddon.setup.impl;

import de.hybris.platform.addonsupport.setup.impl.GenericAddOnSampleDataEventListener;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.util.Config;

public class CxAddOnSampleDataEventListener extends GenericAddOnSampleDataEventListener
{
    @Override
	protected void onEvent(final AbstractEvent event) {
		if (Config.getBoolean("masdarpersonalizationcustomaddon.import.active", false))
		{
            super.onEvent(event);
        }
    }
}
