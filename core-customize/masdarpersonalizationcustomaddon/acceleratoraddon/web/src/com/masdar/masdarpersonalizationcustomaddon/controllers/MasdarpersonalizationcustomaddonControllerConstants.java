/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpersonalizationcustomaddon.controllers;

/**
 */
public interface MasdarpersonalizationcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
