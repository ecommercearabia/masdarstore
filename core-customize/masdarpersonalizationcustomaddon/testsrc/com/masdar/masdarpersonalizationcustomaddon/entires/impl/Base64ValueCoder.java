/*
 * Decompiled with CFR 0.150.
 */
package com.masdar.masdarpersonalizationcustomaddon.entires.impl;

import java.nio.charset.Charset;
import java.util.Base64;

import com.masdar.masdarpersonalizationcustomaddon.entires.CxViewValueCoder;


public class Base64ValueCoder implements CxViewValueCoder
{
	private static final Charset CHARSET = Charset.forName("UTF-8");

	@Override
	public String encode(final String input)
	{
		if (input == null)
		{
			return null;
		}
		final byte[] inputBytes = input.getBytes(CHARSET);
		return Base64.getEncoder().encodeToString(inputBytes);
	}

	@Override
	public String decode(final String input)
	{
		if (input == null)
		{
			return null;
		}
		final byte[] inputBytes = input.getBytes(CHARSET);
		final byte[] outputBytes = Base64.getDecoder().decode(inputBytes);
		return new String(outputBytes, CHARSET);
	}
}

