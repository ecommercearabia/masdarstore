/**
 *
 */
package com.masdar.masdarpersonalizationcustomaddon.entires;

/**
 * @author mnasro
 *
 */
public interface CxViewValueCoder
{
	public String encode(String var1);

	public String decode(String var1);
}
