/**
 *
 */
package com.masdar.storefront.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 */
public class AddressForm
{
	private String addressName;
	private String firstName;
	private String lastName;
	private String addressId;
	private String titleCode;
	private String line1;
	private String line2;
	private String townCity;
	private String regionIso;
	private String postcode;
	private String countryIso;
	private Boolean saveInAddressBook;
	private Boolean defaultAddress;
	private Boolean shippingAddress;
	private Boolean billingAddress;
	private Boolean editAddress;
	private String phone;
	private double latitude;
	private double longitude;
	private String instructions;
	private String mobile;
	private String mobileCountry;
	private String cityCode;
	private String areaCode;
	private String nearestLandmark;
	private String buildingName;
	private String streetName;
	private String apartmentNumber;
	private String floorNumber;
	private String moreInstructions;


	/**
	 * @return the moreInstructions
	 */
	public String getMoreInstructions()
	{
		return moreInstructions;
	}

	/**
	 * @param moreInstructions
	 *           the moreInstructions to set
	 */
	public void setMoreInstructions(final String moreInstructions)
	{
		this.moreInstructions = moreInstructions;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile()
	{
		return mobile;
	}

	/**
	 * @param mobile
	 *           the mobile to set
	 */
	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the cityCode
	 */
	public String getCityCode()
	{
		return cityCode;
	}

	/**
	 * @param cityCode
	 *           the cityCode to set
	 */
	public void setCityCode(final String cityCode)
	{
		this.cityCode = cityCode;
	}

	/**
	 * @return the areaCode
	 */
	public String getAreaCode()
	{
		return areaCode;
	}

	/**
	 * @param areaCode
	 *           the areaCode to set
	 */
	public void setAreaCode(final String areaCode)
	{
		this.areaCode = areaCode;
	}

	/**
	 * @return the nearestLandmark
	 */
	public String getNearestLandmark()
	{
		return nearestLandmark;
	}

	/**
	 * @param nearestLandmark
	 *           the nearestLandmark to set
	 */
	public void setNearestLandmark(final String nearestLandmark)
	{
		this.nearestLandmark = nearestLandmark;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName()
	{
		return buildingName;
	}

	/**
	 * @param buildingName
	 *           the buildingName to set
	 */
	public void setBuildingName(final String buildingName)
	{
		this.buildingName = buildingName;
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName()
	{
		return streetName;
	}

	/**
	 * @param streetName
	 *           the streetName to set
	 */
	public void setStreetName(final String streetName)
	{
		this.streetName = streetName;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber()
	{
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *           the apartmentNumber to set
	 */
	public void setApartmentNumber(final String apartmentNumber)
	{
		this.apartmentNumber = apartmentNumber;
	}

	/**
	 * @return the floorNumber
	 */
	public String getFloorNumber()
	{
		return floorNumber;
	}

	/**
	 * @param floorNumber
	 *           the floorNumber to set
	 */
	public void setFloorNumber(final String floorNumber)
	{
		this.floorNumber = floorNumber;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the instructions
	 */
	public String getInstructions()
	{
		return instructions;
	}

	/**
	 * @param instructions
	 *           the instructions to set
	 */
	public void setInstructions(final String instructions)
	{
		this.instructions = instructions;
	}

	/**
	 * @return the addressName
	 */
	public String getAddressName()
	{
		return addressName;
	}

	/**
	 * @param addressName
	 *           the addressName to set
	 */
	public void setAddressName(final String addressName)
	{
		this.addressName = addressName;
	}

	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	@Size(max = 255, message = "{address.title.invalid}")
	public String getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	@NotNull(message = "{address.line1.invalid}")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public String getTownCity()
	{
		return townCity;
	}

	public void setTownCity(final String townCity)
	{
		this.townCity = townCity;
	}

	public String getRegionIso()
	{
		return regionIso;
	}

	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	@NotNull(message = "{address.postcode.invalid}")
	@Size(min = 1, max = 10, message = "{address.postcode.invalid}")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@NotNull(message = "{address.country.invalid}")
	@Size(min = 1, max = 255, message = "{address.country.invalid}")
	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public Boolean getSaveInAddressBook()
	{
		return saveInAddressBook;
	}

	public void setSaveInAddressBook(final Boolean saveInAddressBook)
	{
		this.saveInAddressBook = saveInAddressBook;
	}

	public Boolean getDefaultAddress()
	{
		return defaultAddress;
	}

	public void setDefaultAddress(final Boolean defaultAddress)
	{
		this.defaultAddress = defaultAddress;
	}

	public Boolean getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(final Boolean shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	public Boolean getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final Boolean billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public Boolean getEditAddress()
	{
		return editAddress;
	}

	public void setEditAddress(final Boolean editAddress)
	{
		this.editAddress = editAddress;
	}

	public String getPhone()
	{
		return phone;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude()
	{
		return latitude;
	}

	/**
	 * @param latitude
	 *           the latitude to set
	 */
	public void setLatitude(final double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude()
	{
		return longitude;
	}

	/**
	 * @param longitude
	 *           the longitude to set
	 */
	public void setLongitude(final double longitude)
	{
		this.longitude = longitude;
	}

	public void setPhone(final String value)
	{
		phone = value;
	}

}
