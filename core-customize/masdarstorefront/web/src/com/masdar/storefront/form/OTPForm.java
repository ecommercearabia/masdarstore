package com.masdar.storefront.form;

/**
 * The Class OTPForm.
 *
 * @author mnasro
 */
public class OTPForm
{

	/** The mobile number. */
	private String mobileNumber;

	/** The mobile country. */
	private String mobileCountry;

	private String otpCode;

	private String token;


	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @param token
	 *           the token to set
	 */
	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getOtpCode()
	{
		return otpCode;
	}

	public void setOtpCode(final String otpCode)
	{
		this.otpCode = otpCode;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber
	 *           the new mobile number
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the mobile country.
	 *
	 * @return the mobile country
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * Sets the mobile country.
	 *
	 * @param mobileCountry
	 *           the new mobile country
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

}
