package com.masdar.storefront.form;

/**
 * Form object for updating profile.
 */
public class UpdateProfileForm
{

	private String titleCode;
	private String firstName;
	private String lastName;
	private String iban;
	private String bankName;
	private String accountNumber;
	private String accountHolderName;
	private String mobileCountry;
	private String mobile;
	private String VatId;




	/**
	 * @return the vAT_Id
	 */
	public String getVatId()
	{
		return this.VatId;
	}

	/**
	 * @param vAT_Id
	 *           the vAT_Id to set
	 */
	public void setVatId(final String vatId)
	{
		this.VatId = vatId;
	}

	public String getMobileCountry()
	{
		return mobileCountry;
	}

	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}


	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the iban
	 */
	public String getIban()
	{
		return iban;
	}

	/**
	 * @param iban
	 *           the iban to set
	 */
	public void setIban(final String iban)
	{
		this.iban = iban;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName()
	{
		return bankName;
	}

	/**
	 * @param bankName
	 *           the bankName to set
	 */
	public void setBankName(final String bankName)
	{
		this.bankName = bankName;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	 * @param accountNumber
	 *           the accountNumber to set
	 */
	public void setAccountNumber(final String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountHolderName
	 */
	public String getAccountHolderName()
	{
		return accountHolderName;
	}

	/**
	 * @param accountHolderName
	 *           the accountHolderName to set
	 */
	public void setAccountHolderName(final String accountHolderName)
	{
		this.accountHolderName = accountHolderName;
	}



}
