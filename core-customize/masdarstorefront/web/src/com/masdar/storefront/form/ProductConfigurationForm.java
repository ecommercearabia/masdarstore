/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.storefront.form;

import de.hybris.platform.catalog.enums.ConfiguratorType;

import java.util.Map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class ProductConfigurationForm
{
	private Map<ConfiguratorType, Map<String, String>> configurationsKeyValueMap;

	@NotNull(message = "{basket.error.quantity.notNull}")
	@Min(value = 0, message = "{basket.error.quantity.invalid}")
	@Digits(fraction = 0, integer = 10, message = "{basket.error.quantity.invalid}")
	private long qty = 1L;

	public void setQty(final long quantity)
	{
		this.qty = quantity;
	}

	public long getQty()
	{
		return qty;
	}

	public Map<ConfiguratorType, Map<String, String>> getConfigurationsKeyValueMap()
	{
		return configurationsKeyValueMap;
	}

	public void setConfigurationsKeyValueMap(final Map<ConfiguratorType, Map<String, String>> configurationsKeyValueMap)
	{
		this.configurationsKeyValueMap = configurationsKeyValueMap;
	}
}
