/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.storefront.form.validation;


import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.masdar.core.service.MobilePhoneService;
import com.masdar.storefront.form.UpdateProfileForm;


/**
 * Validator for profile forms.
 */
@Component("customProfileValidator")
public class ProfileValidator implements Validator
{

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdateProfileForm profileForm = (UpdateProfileForm) object;
		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();

		final String mobileCountry = profileForm.getMobileCountry();
		final String mobileNumber = profileForm.getMobile();

		if (StringUtils.isNotEmpty(title) && StringUtils.length(title) > 255)
		{
			errors.rejectValue("titleCode", "profile.title.invalid");
		}

		if (StringUtils.isBlank(firstName) || StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		//		if (StringUtils.isBlank(lastName) || StringUtils.length(lastName) > 255)
		//		{
		//			errors.rejectValue("lastName", "profile.lastName.invalid");
		//		}


		if (!validateMobileNumber(mobileCountry, mobileNumber, profileForm))
		{

			errors.rejectValue("mobile", "text.secureportal.register.mobile.format.invalid");
			errors.rejectValue("mobileCountry", "text.secureportal.register.mobilecountry.invalid");
		}

	}


	protected boolean validateMobileNumber(final String mobileCountry, final String mobileNumber,
			final UpdateProfileForm updateProfileForm)
	{
		if (StringUtils.isBlank(mobileCountry) || StringUtils.isBlank(mobileNumber))
		{
			return false;
		}
		final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
				mobileNumber);
		if (normalizedPhoneNumber.isEmpty())
		{
			return false;
		}
		updateProfileForm.setMobile(normalizedPhoneNumber.get());
		updateProfileForm.setMobileCountry(mobileCountry);
		return true;
	}

}
