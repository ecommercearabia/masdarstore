package com.masdar.storefront.form.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.masdar.storefront.form.OTPForm;


/**
 * The Class OTPValidator.
 *
 * @author mnasro
 */
@Component("otpRegisterValidator")
public class OTPRegisterValidator extends OTPValidator
{

	@Override
	public boolean supports(final Class<?> form)
	{
		return form.getClass().equals(OTPForm.class);
	}

	@Override
	public void validate(final Object form, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) form;
		validateSend(otpForm, errors);
		validateOTPCode(otpForm, errors);
	}

	@Override
	public void validateSend(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateStandardFields(otpForm, errors);
		//		validatePhoneNumber(otpForm, errors);
	}

	//	public void validatePhoneNumber(final OTPForm form, final Errors errors)
	//	{
	//		if (StringUtils.isNotBlank(form.getMobileNumber()))
	//		{
	//			final List<CustomerModel> customers = getMobilePhoneService().getCustomersByMobileNumber(form.getMobileNumber());
	//			if (!CollectionUtils.isEmpty(customers))
	//			{
	//				errors.rejectValue("mobileNumber", "test.found");
	//			}
	//		}
	//	}

}
