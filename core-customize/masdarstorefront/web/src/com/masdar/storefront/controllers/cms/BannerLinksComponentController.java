/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masdar.masdarcomponents.model.BannerLinksComponentModel;
import com.masdar.storefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */
@Controller("BannerLinksComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.BannerLinksComponent)
public class BannerLinksComponentController extends AbstractAcceleratorCMSComponentController<BannerLinksComponentModel>
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BannerLinksComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			model.addAttribute("media", component.getMedia());
			model.addAttribute("urlLink", component.getUrlLink());
			model.addAttribute("link", component.getLink());
			model.addAttribute("links", component.getCmsLinkComponents());
			model.addAttribute("title", component.getTitle());
		}
	}




}
