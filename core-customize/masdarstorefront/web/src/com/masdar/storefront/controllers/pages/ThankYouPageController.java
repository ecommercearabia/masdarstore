/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Controller for MyCart functionality.
 */
@Controller
@RequestMapping(value = "/thank-you")
public class ThankYouPageController extends AbstractPageController
{
	private static final String REGISTRATION_THANK_YOU_PAGE = "registration-thank-you";


	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String thankYou(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final ContentPageModel registrationPage = getContentPageForLabelOrId(REGISTRATION_THANK_YOU_PAGE);

		storeCmsPageInModel(model, registrationPage);
		setUpMetaDataForContentPage(model, registrationPage);

		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		model.addAttribute("redirectCount", currentSite.getThankYouPageCountDown());
		return getViewForPage(registrationPage);
	}
}
