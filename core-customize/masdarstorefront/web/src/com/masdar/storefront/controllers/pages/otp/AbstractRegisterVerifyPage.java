/**
 *
 */
package com.masdar.storefront.controllers.pages.otp;

import javax.annotation.Resource;

import com.masdar.storefront.form.validation.OTPRegisterValidator;
import com.masdar.storefront.form.validation.OTPValidator;


/**
 * @author mnasro
 *
 */
public abstract class AbstractRegisterVerifyPage extends AbstractOTPVerifyPage
{


	/** The otp validator. */
	@Resource(name = "otpRegisterValidator")
	private OTPRegisterValidator otpRegisterValidator;


	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpRegisterValidator;
	}


	@Override
	protected String getPageLabelOrId()
	{
		return "/verify";
	}
}
