/**
 *
 */
package com.masdar.storefront.controllers.pages.otp;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.util.localization.Localization;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.exception.TokenInvalidatedException;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;
import com.masdar.masdarsecureportalcustomaddon.constants.MasdarsecureportalcustomaddonConstants;
import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.exceptions.CustomerAlreadyExistsException;
import com.masdar.masdarsecureportalcustomaddon.facades.B2BRegistrationFacade;
import com.masdar.storefront.controllers.pages.RegisterPageController;
import com.masdar.storefront.form.OTPForm;


/**
 * The Class OTPRegisterVerifyPage.
 *
 * @author @author mnasro
 */
@Controller
@Scope("tenant")
@RequestMapping("/register/verify")
public class OTPRegisterVerifyPage extends AbstractRegisterVerifyPage
{
	private static final String LOGIN_PAGE = REDIRECT_PREFIX + "/login";
	private static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";
	private static final String ADD_ON_PREFIX = "addon:";
	private static final String VIEW_PAGE_PREFIX = ADD_ON_PREFIX + "/" + MasdarsecureportalcustomaddonConstants.EXTENSIONNAME;
	private static final String REGISTRATION_PAGE = REDIRECT_PREFIX + "/pages/accountRegistration";
	private static final String REGISTRATION_THANK_YOU_PAGE = REDIRECT_PREFIX + "/thank-you/registration";
	private static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
	private static final String REGISTER_ERROR_OCCURRED = "text.secureportal.register.submit.confirmation";



	private static final Logger LOG = Logger.getLogger(OTPRegisterVerifyPage.class);

	@Resource(name = "b2bRegistrationFacade")
	private B2BRegistrationFacade b2bRegistrationFacade;

	/**
	 * Show verify page.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String showVerifyPage(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.showVerifyPage(token, model, request, response);
	}

	/**
	 * Resend OTP code.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/resend")
	public String resendOTPCode(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.resendOTPCode(token, model, request, response, redirectModel);
	}

	/**
	 * Gets the change mobile number view.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the change mobile number view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/change-number")
	public String getChangeMobileNumberView(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.getChangeMobileNumberView(token, model, request, response);
	}


	/**
	 * Send.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/send")
	public String send(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		return super.send(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Verify.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws TokenInvalidatedException
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public String verify(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{
		return super.verify(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Gets the thankyou.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the thankyou
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/thank-you")
	public String getThankyou(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.THANK_YOU_VIEW);
		return super.getThankyou(token, otpForm, model, request, response, redirectModel);
	}

	/**
	 * Gets the OTP view URL.
	 *
	 * @return the OTP view URL
	 */
	@Override
	protected String getOTPViewURL()
	{
		return "/register/verify";
	}


	/**
	 * Gets the OTP verification token type.
	 *
	 * @return the OTP verification token type
	 */
	@Override
	protected OTPVerificationTokenType getOTPVerificationTokenType()
	{
		return OTPVerificationTokenType.REGISTRATION;
	}

	/**
	 * Gets the failed error label or id page.
	 *
	 * @return the failed error label or id page
	 */
	@Override
	protected String getFailedErrorLabelOrIdPage()
	{
		return "register";
	}

	/**
	 * Gets the failed error view.
	 *
	 * @return the failed error view
	 */
	@Override
	protected String getFailedErrorView()
	{
		return "/register";
	}


	/**
	 * Gets the register page controller.
	 *
	 * @param httpRequest
	 *           the http request
	 * @return the register page controller
	 */
	protected static RegisterPageController getRegisterPageController(final HttpServletRequest httpRequest)
	{
		return getSpringBean(httpRequest, "registerPageController", RegisterPageController.class);
	}


	/**
	 * Do action.
	 *
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @param otpVerificationToken
	 *           the otp verification token
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	protected String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationToken, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException

	{

		final B2BRegistrationData data = (B2BRegistrationData) otpVerificationToken.getData();
		// Update registration mobile number and country in case the mobile number has been changed
		data.setMobileCountry(otpVerificationToken.getMobileCountry());
		data.setMobileNumber(otpVerificationToken.getMobileNumber());
		try
		{
			data.setCashCustomer(true);
			b2bRegistrationFacade.register(data);
		}
		catch (final CustomerAlreadyExistsException e) //NOSONAR
		{
			LOG.error("Failed to register account. Account already exists.");
			GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(REGISTER_ACCOUNT_EXISTING));
			return REDIRECT_PREFIX + getFailedErrorView();
		}
		catch (final Exception e)
		{
			GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(REGISTER_ERROR_OCCURRED));
			return REDIRECT_PREFIX + getFailedErrorView();
		}

		GlobalMessages.addInfoMessage(model, Localization.getLocalizedString(REGISTER_SUBMIT_CONFIRMATION));
		//		GlobalMessages.addErrorMessage(model, error.getDefaultMessage());

		return REGISTRATION_THANK_YOU_PAGE;

	}

	protected String getRegistrationView()
	{
		return REGISTRATION_PAGE;
	}

	protected String getThankYouURL()
	{
		return "/register/verify/thank-you";
	}

	/**
	 * Gets the verify action URL.
	 *
	 * @return the verify action URL
	 */
	@Override
	protected String getVerifyActionURL()
	{
		return "/register/verify";
	}

	/**
	 * Gets the change number action URL.
	 *
	 * @return the change number action URL
	 */
	@Override
	protected String getChangeNumberActionURL()
	{
		return "/register/verify/change-number";
	}

	/**
	 * Gets the resend verify action URL.
	 *
	 * @return the resend verify action URL
	 */
	@Override
	protected String getResendVerifyActionURL()
	{
		return "/register/verify/resend";
	}

	/**
	 * Gets the send verify action URL.
	 *
	 * @return the send verify action URL
	 */
	@Override
	protected String getSendVerifyActionURL()
	{
		return "/register/verify/send";
	}

}
