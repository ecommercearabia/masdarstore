/**
 *
 */
package com.masdar.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masdar.masdarcomponents.model.RegistrationB2BLoginComponentModel;
import com.masdar.storefront.controllers.ControllerConstants;


/**
 * @author monzer
 *
 */
@Controller("RegistrationB2BLoginComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.RegistrationB2BLoginComponent)
public class RegistrationB2BLoginComponentController
		extends AbstractAcceleratorCMSComponentController<RegistrationB2BLoginComponentModel>
{

	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final RegistrationB2BLoginComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			model.addAttribute("title", component.getTitle());
			model.addAttribute("content", component.getContent());
			model.addAttribute("link", component.getLink());
		}

	}

}
