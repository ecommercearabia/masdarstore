/**
 *
 */
package com.masdar.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masdar.masdarcomponents.model.LastOrderComponentModel;
import com.masdar.storefront.controllers.ControllerConstants;


/**
 * @author monzer
 *
 */
@Controller("LastOrderComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.LastOrderComponent)
public class LastOrderComponentController extends AbstractAcceleratorCMSComponentController<LastOrderComponentModel>
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;
	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Resource
	private UserService userService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final LastOrderComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			OrderModel order = null;
			final UserModel currentCustomer = getUserService().getCurrentUser();
			if (currentCustomer != null && (currentCustomer instanceof B2BCustomerModel))
			{
				final B2BCustomerModel b2bcurrentCustomer = (B2BCustomerModel) currentCustomer;
				if (b2bcurrentCustomer.getOrders() != null)
				{
					for (final OrderModel a : b2bcurrentCustomer.getOrders())
					{
						if (order == null || a.getCreationtime().after(order.getCreationtime()))
						{
							order = a;
						}
					}
				}
			}
			if (order != null)
			{
				final OrderData orderData = orderConverter.convert(order);
				model.addAttribute("lastOrder", orderData);

			}
		}
	}


	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
