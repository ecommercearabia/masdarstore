/**
 *
 */
package com.masdar.storefront.controllers.pages.otp;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.exception.TokenInvalidatedException;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;
import com.masdar.storefront.controllers.pages.RegisterPageController;
import com.masdar.storefront.form.OTPForm;


/**
 * The Class OTPRegisterVerifyPage.
 *
 * @author @author mnasro
 */
@Controller
@Scope("tenant")
@RequestMapping("/my-account/update-profile/verify")
public class OTPUpdateProfileVerifyPage extends AbstractRegisterVerifyPage
{

	private static final Logger LOG = Logger.getLogger(OTPUpdateProfileVerifyPage.class);

	private static final String UPDATE_PROFILE = "/my-account/update-profile";

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	/**
	 * Show verify page.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String showVerifyPage(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.showVerifyPage(token, model, request, response);
	}

	/**
	 * Resend OTP code.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/resend")
	public String resendOTPCode(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.resendOTPCode(token, model, request, response, redirectModel);
	}

	/**
	 * Gets the change mobile number view.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the change mobile number view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/change-number")
	public String getChangeMobileNumberView(@RequestParam(required = false)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.getChangeMobileNumberView(token, model, request, response);
	}


	/**
	 * Send.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/send")
	public String send(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		return super.send(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Verify.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws TokenInvalidatedException
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public String verify(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{
		return super.verify(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Gets the thankyou.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the thankyou
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/thank-you")
	public String getThankyou(@RequestParam(required = false)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute(OTPConstants.OTP_VIEW, OTPConstants.Views.THANK_YOU_VIEW);
		return super.getThankyou(token, otpForm, model, request, response, redirectModel);
	}

	/**
	 * Gets the OTP view URL.
	 *
	 * @return the OTP view URL
	 */
	@Override
	protected String getOTPViewURL()
	{
		return "/my-account/update-profile/verify";
	}


	/**
	 * Gets the OTP verification token type.
	 *
	 * @return the OTP verification token type
	 */
	@Override
	protected OTPVerificationTokenType getOTPVerificationTokenType()
	{
		return OTPVerificationTokenType.REGISTRATION;
	}

	/**
	 * Gets the failed error label or id page.
	 *
	 * @return the failed error label or id page
	 */
	@Override
	protected String getFailedErrorLabelOrIdPage()
	{
		return "register";
	}

	/**
	 * Gets the failed error view.
	 *
	 * @return the failed error view
	 */
	@Override
	protected String getFailedErrorView()
	{
		return UPDATE_PROFILE;
	}


	/**
	 * Do action.
	 *
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @param otpVerificationToken
	 *           the otp verification token
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	protected String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationToken, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{

		final CustomerData data = (CustomerData) otpVerificationToken.getData();
		try
		{
			customerFacade.updateProfile(data);

			return getRedirectPageWithMsg(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"text.account.profile.confirmationUpdated", UPDATE_PROFILE);

		}
		catch (final DuplicateUidException e)
		{
			bindingResult.rejectValue("email", "registration.error.account.exists.title");

			return getRedirectPageWithMsg(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"registration.error.account.exists.title", UPDATE_PROFILE);

		}

	}

	protected String getRedirectPageWithMsg(final RedirectAttributes redirectAttributes, final String globalMessages,
			final String msgKey, final String redirectURL)
	{

		GlobalMessages.addFlashMessage(redirectAttributes, globalMessages, msgKey, null);

		return REDIRECT_PREFIX + redirectURL;
	}
	protected String getThankYouURL()
	{
		return "/my-account/update-profile/verify/thank-you";
	}

	/**
	 * Gets the verify action URL.
	 *
	 * @return the verify action URL
	 */
	@Override
	protected String getVerifyActionURL()
	{
		return "/my-account/update-profile/verify";
	}

	/**
	 * Gets the change number action URL.
	 *
	 * @return the change number action URL
	 */
	@Override
	protected String getChangeNumberActionURL()
	{
		return "/my-account/update-profile/verify/change-number";
	}

	/**
	 * Gets the resend verify action URL.
	 *
	 * @return the resend verify action URL
	 */
	@Override
	protected String getResendVerifyActionURL()
	{
		return "/my-account/update-profile/verify/resend";
	}

	/**
	 * Gets the send verify action URL.
	 *
	 * @return the send verify action URL
	 */
	@Override
	protected String getSendVerifyActionURL()
	{
		return "/my-account/update-profile/verify/send";
	}

}
