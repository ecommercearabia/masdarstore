/**
 *
 */
package com.masdar.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.order.CommerceQuoteService;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masdar.masdarcomponents.model.LastQuoteComponentModel;
import com.masdar.storefront.controllers.ControllerConstants;


/**
 * @author monzer
 *
 */
@Controller("LastQuoteComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.LastQuoteComponent)
public class LastQuoteComponentController extends AbstractAcceleratorCMSComponentController<LastQuoteComponentModel>
{

	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private CommerceQuoteService commerceQuoteService;
	@Resource
	private UserService userService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final LastQuoteComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			QuoteModel quote = null;
			final UserModel currentCustomer = getUserService().getCurrentUser();
			if (currentCustomer != null && (currentCustomer instanceof B2BCustomerModel))
			{
				final B2BCustomerModel b2bcurrentCustomer = (B2BCustomerModel) currentCustomer;
				if (b2bcurrentCustomer.getQuotes() != null)
				{
					for (final QuoteModel a : b2bcurrentCustomer.getQuotes())
					{
						if (quote == null || a.getCreationtime().after(quote.getCreationtime()))
						{
							quote = a;
						}
					}
				}
			}
			model.addAttribute("lastQuote", quote);
		}
	}

	protected CommerceQuoteService getCommerceQuoteService()
	{
		return commerceQuoteService;
	}

	@Required
	public void setCommerceQuoteService(final CommerceQuoteService commerceQuoteService)
	{
		this.commerceQuoteService = commerceQuoteService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
