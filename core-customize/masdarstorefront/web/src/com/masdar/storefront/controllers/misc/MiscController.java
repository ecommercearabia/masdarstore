/**
 *
 */
package com.masdar.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.facades.CommercialRegistrationFacade;
import com.masdar.facades.area.facade.AreaFacade;
import com.masdar.facades.city.facade.CityFacade;
import com.masdar.facades.facade.PointOfServiceFacade;
import com.masdar.masdarfacades.data.MetaData;
import com.masdar.masdarfacades.data.ResponseData;
import com.masdar.storefront.controllers.ControllerConstants;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Controller
@RequestMapping("/misc")
public class MiscController extends AbstractController
{
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "commercialRegistrationFacade")
	private CommercialRegistrationFacade commercialRegistrationFacade;


	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;


	@Resource(name = "pointOfServiceFacade")
	private PointOfServiceFacade pointOfServiceFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@RequestMapping(value = "/region/{regionCode}/cities")
	@ResponseBody
	public ResponseData getCitiesByRegionId(final HttpServletResponse response, @PathVariable
	final String regionCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(cityFacade.getByRegionIsocode(regionCode).get());
			meta.setMessage("getting cites by isoCode of country");
			meta.setStatusCode(HttpStatus.OK.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get cites by isoCode of country");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	@RequestMapping(value = "/city/{cityCode}/registrationAreas")
	@ResponseBody
	public ResponseData getRegistrationAreasByCityCode(final HttpServletResponse response, @PathVariable
	final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(getAreaFacade().getRegistrationAreasByCityCode(cityCode));
			meta.setMessage("getting areas by city code");
			meta.setStatusCode(HttpStatus.OK.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get areas by city code");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	@RequestMapping(value = "/city/{cityCode}/defaultRegistrationArea")
	@ResponseBody
	public ResponseData getDefaultRegistrationAreasByCityCode(final HttpServletResponse response, @PathVariable
	final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(getAreaFacade().getDefaultRegistrationAreaByCityCode(cityCode));
			meta.setMessage("getting Default Registration Area by city code");
			meta.setStatusCode(HttpStatus.OK.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get areas by city code");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	@RequestMapping(value = "/plp/view/set", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData setPlpView(final HttpServletResponse response, @NotNull
	@RequestParam(required = true, defaultValue = "grid", name = "viewType")
	final String viewType)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		try
		{
			sessionService.setAttribute(ControllerConstants.Actions.SessionPlpView.plpViewKey, viewType);
			data.setData(viewType);
			meta.setMessage("setting plp view  ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final Exception e)
		{
			data.setData(null);
			meta.setMessage("failed to set Plp View");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}
		return data;
	}



	@RequestMapping(value = "/company-registration-info/{cr}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCompanyRegistrationInfoData(final HttpServletResponse response, @PathVariable
	final String cr)
	{
		final ResponseData responseData = new ResponseData();
		final MetaData meta = new MetaData();
		Object data = null;
		String message = null;
		String displayMessage = null;
		int statusCode = HttpStatus.OK.value();
		try
		{
			final Optional<CompanyRegistrationInfoData> companyRegistrationInfoData = commercialRegistrationFacade
					.getCompanyCrInfoByCurrentBaseStore(cr);
			message = "getting company registration info Data by cr number";

			if (companyRegistrationInfoData.isPresent())
			{
				data = companyRegistrationInfoData.get();
				displayMessage = getMessage("text.misc.retrive.commercial.registration.success", new Object[]
				{ cr });
				statusCode = HttpStatus.OK.value();
			}
			else
			{
				displayMessage = getMessage("text.misc.retrive.commercial.registration.fail", new Object[]
				{ cr });
				statusCode = HttpStatus.NOT_FOUND.value();
			}
		}
		catch (final Exception e)
		{
			message = "failed to get company registration info Data by cr number: cr[" + cr + "] error msg[" + e.getMessage() + "]";
			statusCode = HttpStatus.BAD_REQUEST.value();
			displayMessage = getMessage("", null);
		}
		meta.setStatusCode(statusCode);
		meta.setMessage(message);
		meta.setDisplayMessage(displayMessage);
		responseData.setData(data);
		responseData.setMeta(meta);

		return responseData;
	}

	@RequestMapping(value = "/points-of-services/{countyIsoCode}/{regionIsoCode}", method = RequestMethod.GET)
	@ResponseBody
	public List<PointOfServiceData> getPointOfServices(@PathVariable
	final String countyIsoCode, @PathVariable
	final String regionIsoCode)
	{
		return getPointOfServiceFacade().getPointOfServicesForRegion(countyIsoCode, regionIsoCode,
				getBaseStoreService().getCurrentBaseStore());
	}

	@RequestMapping(value = "/inctax", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData setIncTax(final HttpServletResponse response, @RequestParam
	final boolean incTax)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			sessionService.getCurrentSession().setAttribute("incTax", incTax);

			meta.setMessage("set Inc tax");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to set Inc tax");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	/**
	 *
	 */
	private String getMessage(final String msgKey, final Object[] errorParm)
	{
		if (StringUtils.isBlank(msgKey))
		{
			return null;
		}
		return messageSource.getMessage(msgKey, errorParm, i18nService.getCurrentLocale());
	}


	/**
	 * @return the pointOfServiceFacade
	 */
	public PointOfServiceFacade getPointOfServiceFacade()
	{
		return pointOfServiceFacade;
	}


	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	protected CityFacade getCityFacade()
	{
		return cityFacade;
	}

	protected AreaFacade getAreaFacade()
	{
		return areaFacade;
	}


}
