/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.storefront.filters;

import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistory;
import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistoryEntry;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Filter that initializes the session for the masdarstorefront. This is a spring configured filter that is executed by
 * the PlatformFilterChain.
 */
public class CashCustomerStorefrontFilter extends OncePerRequestFilter
{
	private static final Logger LOG = LoggerFactory.getLogger(CashCustomerStorefrontFilter.class);


	public static final String AJAX_REQUEST_HEADER_NAME = "X-Requested-With";
	public static final String ORIGINAL_REFERER = "originalReferer";

	private StoreSessionFacade storeSessionFacade;
	private BrowseHistory browseHistory;
	private Set<String> refererExcludeUrlSet;
	private PathMatcher pathMatcher;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource(name = "b2bCustomerService")
	private B2BCustomerService<B2BCustomerModel, B2BUnitModel> b2bCustomerService;

	private boolean isCurrentCustomerCashCustomer()
	{
		final B2BCustomerModel customer = getB2bCustomerService().getCurrentB2BCustomer();
		if (customer == null)
		{
			return false;
		}

		final Optional<B2BUnitModel> findAny = getB2bUnitService().getAllUnitsOfOrganization(customer).stream()
				.filter(B2BUnitModel::isCashCustomer).findAny();
		return findAny.isPresent();
	}

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		final HttpSession session = request.getSession();
		final String queryString = request.getQueryString();

		if (isSessionNotInitialized(session, queryString))
		{
			initDefaults(request);

			markSessionInitialized(session);
		}

		if (isGetMethod(request) && isCurrentCustomerCashCustomer())
		{
			final Set<String> forbiddenPatterns = getForbiddenURLPatterns();

			for (final String pattern : forbiddenPatterns)
			{
				if (request.getServletPath() != null && request.getServletPath().matches(pattern))
				{
					response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/")); //NOSONAR

					LOG.debug("Cash Customer detected with request to  {}", request.getServletPath());
					getBrowseHistory().addBrowseHistoryEntry(new BrowseHistoryEntry(request.getRequestURI(), null));
				}
			}
		}

		filterChain.doFilter(request, response);
	}

	/**
	 * @return
	 */
	private Set<String> getForbiddenURLPatterns()
	{
		if (getCmsSiteService().getCurrentSite() == null
				|| CollectionUtils.isEmpty(getCmsSiteService().getCurrentSite().getCashCustomerForbiddenURLsPatterns()))
		{
			return Collections.emptySet();
		}

		return new HashSet<>(getCmsSiteService().getCurrentSite().getCashCustomerForbiddenURLsPatterns());

	}

	protected boolean isGetMethod(final HttpServletRequest httpRequest)
	{
		return "GET".equalsIgnoreCase(httpRequest.getMethod());
	}

	protected boolean isRequestSecure(final HttpServletRequest httpRequest)
	{
		return httpRequest.isSecure();
	}

	protected boolean isSessionNotInitialized(final HttpSession session, final String queryString)
	{
		return session.isNew() || StringUtils.contains(queryString, CMSFilter.CLEAR_CMSSITE_PARAM)
				|| !isSessionInitialized(session);
	}

	@Required
	public void setStoreSessionFacade(final StoreSessionFacade storeSessionFacade)
	{
		this.storeSessionFacade = storeSessionFacade;
	}

	@Required
	public void setBrowseHistory(final BrowseHistory browseHistory)
	{
		this.browseHistory = browseHistory;
	}

	protected void initDefaults(final HttpServletRequest request)
	{
		getStoreSessionFacade().initializeSession(Collections.list(request.getLocales()));
	}

	protected StoreSessionFacade getStoreSessionFacade()
	{
		return storeSessionFacade;
	}

	protected BrowseHistory getBrowseHistory()
	{
		return browseHistory;
	}


	protected boolean isSessionInitialized(final HttpSession session)
	{
		return session.getAttribute(this.getClass().getName()) != null;
	}

	protected void markSessionInitialized(final HttpSession session)
	{
		session.setAttribute(this.getClass().getName(), "initialized");
	}

	protected boolean isRequestPathExcluded(final HttpServletRequest request)
	{
		final Set<String> inputSet = getRefererExcludeUrlSet();
		final String servletPath = request.getServletPath();

		for (final String input : inputSet)
		{
			if (getPathMatcher().match(input, servletPath))
			{
				return true;
			}
		}

		return false;
	}

	protected Set<String> getRefererExcludeUrlSet()
	{
		return refererExcludeUrlSet;
	}

	@Required
	public void setRefererExcludeUrlSet(final Set<String> refererExcludeUrlSet)
	{
		this.refererExcludeUrlSet = refererExcludeUrlSet;
	}

	protected PathMatcher getPathMatcher()
	{
		return pathMatcher;
	}

	@Required
	public void setPathMatcher(final PathMatcher pathMatcher)
	{
		this.pathMatcher = pathMatcher;
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	protected B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	protected B2BCustomerService<B2BCustomerModel, B2BUnitModel> getB2bCustomerService()
	{
		return b2bCustomerService;
	}
}
