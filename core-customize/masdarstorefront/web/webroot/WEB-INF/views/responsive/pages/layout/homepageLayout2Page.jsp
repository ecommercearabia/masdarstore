<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page2 pageTitle="${pageTitle}">

    <cms:pageSlot position="Section1" var="feature" element="div"   class="row m-b-10">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12 no-header m-b-20 m-t-20 hover_style"/>
    </cms:pageSlot>
    <div class="row withouttext">
    <div class="col-md-8">
    <div class="row hidden-md hidden-sm hidden-lg m-b-20">
    <cms:pageSlot position="Section5" var="feature" element="div" class="col-xs-12 m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper "/>
    </cms:pageSlot>
    <cms:pageSlot position="Section6" var="feature" element="div" class="col-xs-12">
        <cms:component component="${feature}" element="div" class="yComponentWrapper "/>
    </cms:pageSlot>
    </div>
    <cms:pageSlot position="Section2" var="feature" element="div" class="row m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <cms:pageSlot position="Section3" var="feature" element="div" class="row m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  text-center col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <cms:pageSlot position="Section4" var="feature" element="div" class="row  m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  text-center col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    </div>
    </div>
    </div>
    <div class="col-md-4 hidden-xs">
    <cms:pageSlot position="Section5" var="feature" element="div" class="row m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section6" var="feature" element="div" class="row">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    </div>
    </div>
    <cms:pageSlot position="Section7" var="feature" element="div" class="row m-t-20 m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
<div class="row inverce_margin m-t-20 m-b-20 grayback padd-20-30 thumb_style">
    <cms:pageSlot position="Section8A" var="feature">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-4 col-sm-12 col-xs-12 border-bottom-xs"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section8B" var="feature" >
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-4 col-sm-12 col-xs-12 border-bottom-xs"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section8C" var="feature">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-4 col-sm-12 col-xs-12 border-bottom-xs no-border"/>
    </cms:pageSlot>
    </div>
    <cms:pageSlot position="Section9" var="feature" element="div" class="row m-t-20 m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12 no-header hover_style"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section9A" var="feature" element="div" class="row  blueback inverce_margin">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section10" var="feature" element="div" class="row m-t-20 m-b-20">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section11" var="feature" element="div" class="row inverce_margin">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12 no-margin no-padding"/>
    </cms:pageSlot>

    <c:url var="b2bUrl" value="${siteBaseUrl}"/>
    <c:url var="b2cUrl" value="/login"/>
<div class="popuphome hidden">
    <div class="popuphomebox"> 
      <a href="javascript:;" class="hidepopuphome"><i class="far fa-times"></i></a>
      <p><spring:theme code="homePopupText" /> </p><br/>
      <a href="javascript:;" class="hidepopuphome2 btn btn-primary"><spring:theme code="homePopupb2blink" /></a>&nbsp;&nbsp;&nbsp;<a href="${b2bUrl}" class="popuplink b2blink btn btn-primary"><spring:theme code="homePopupb2clink" /></a></div>
   </div>

</template:page2>
