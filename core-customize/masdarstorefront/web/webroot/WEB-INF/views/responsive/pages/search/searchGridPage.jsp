<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">

	<div class="row ${layout}_view plpage" id="displaymode">
	<c:url value="/misc/plp/view/set?viewType=" var="linkurl"/>
	<input type="hidden" value="${linkurl}" id="acrionurl"/>
		<div class="col-xs-3">
			<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="search-grid-page-left-refinements-slot">
				<cms:component component="${feature}" element="div" class="search-grid-page-left-refinements-component"/>
			</cms:pageSlot>
		</div>
		<div class="col-sm-12 col-md-9">
			<cms:pageSlot position="SearchResultsGridSlot" var="feature" element="div" class="search-grid-page-result-grid-slot">
				<cms:component component="${feature}" element="div" class="search-grid-page-result-grid-component"/>
			</cms:pageSlot>
		</div>
	</div>

	<storepickup:pickupStorePopup />

</template:page>