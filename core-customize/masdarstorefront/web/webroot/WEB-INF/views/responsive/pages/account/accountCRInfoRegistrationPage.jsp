<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement"
           tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="account-section-header">
    <div class="row">
        <div class=" col-xs-12 col-sm-6 headerCR">
            <spring:theme code="text.account.profile.cr.info"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-lg col-md-12">
        <div class="account-section-content">
            <div class="account-section-form">
                <c:choose>
                    <c:when test="${ not empty companyCrInfo }">
                        <div class="box">
							 <span class="titleBox"> <spring:theme
									 code="text.account.profile.cr.info.crNumberForm" />
                            </span>
							<hr class="titleLine" />
                            <div class="row">
                                <div class="col-xs-12 styleforms">
                                    <c:url var="updateCRInfoActionURL" value="/register/cr-info"/>
                                    <form:form method="post" id="crNumberForm"
                                               modelAttribute="crNumberForm" action="${updateCRInfoActionURL}">
                                        <form:input path="token" type="hidden"/>
                                        <formElement:formInputBox idKey="crNumber"
                                                                  labelKey="text.secureportal.register.companyCr"
                                                                  path="crNumber" inputCSS="form-control"/>
                                        <button type="submit" class="btn btn-primary virifyBtn"><spring:theme
                                            code="text.account.profile.cr.info.crchangeForm"/></button>
                                    </form:form>
                                    <c:url var="registerActionURL" value="/register/cr-info/register"/>
                                    <form:form method="post" id="registerConfirm"
                                               modelAttribute="token" action="${registerActionURL}">
                                        <input value="${crNumberForm.token}" name="token" type="hidden"/>
                                        <button class="btn btn-default virifyBtn "><spring:theme
                                            code="text.account.profile.cr.info.cregisterForm"/></button>
                                    </form:form>
                                </div>



                            </div>

                        </div>
                        <div class="box">
							<span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.label"/>
							</span>
                            <hr class="titleLine"/>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.crName"/>
									</span> <span class="valueColCR">${companyCrInfo.crName}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.crNumber"/>
									</span><span class="valueColCR">${companyCrInfo.crNumber} </span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.crEntityNumber"/>
									</span><span class="valueColCR">${companyCrInfo.crEntityNumber}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.issueDate"/>
									</span><span class="valueColCR">${companyCrInfo.issueDate}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.crMainNumber"/>
									</span><span class="valueColCR">${companyCrInfo.crMainNumber}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.businessType"/>
									</span><span class="valueColCR"></span> <br/>
                                </div>
                               <%-- <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.businessType.id"/>
									</span><span class="valueColCR">${companyCrInfo.businessType.id}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
									<span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.businessType.name"/>
									</span><span class="valueColCR">${companyCrInfo.businessType.name}</span>
                                </div>--%>
                            </div>
                        </div>
						<div class="box">
                            <span class="titleBox"> <spring:theme
									code="text.account.profile.cr.info.status" />
                            </span>
							<hr class="titleLine" />
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
											code="text.account.profile.cr.info.status.id" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.id} </span>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
											code="text.account.profile.cr.info.status.name" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.name} </span>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
											code="text.account.profile.cr.info.status.nameEn" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.nameEn}
								</span>
								</div>
							</div>
						</div>
                        <%--<div class="box">
                            <span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.status" />
                            </span>
                            <hr class="titleLine" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.status.id" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.id} </span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.status.name" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.name} </span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.status.nameEn" />
                                    </span> <span class="valueColCR">${companyCrInfo.status.nameEn}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.cancellation" />
                            </span>
                            <hr class="titleLine" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.cancellation.date" />
                                    </span> <span class="valueColCR">${companyCrInfo.cancellation.date}
                                    </span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.cancellation.reason" />
                                    </span> <span class="valueColCR">${companyCrInfo.cancellation.reason}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.location" />
                            </span>
                            <hr class="titleLine" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.id" />
                                    </span> <span class="valueColCR">${companyCrInfo.location.id}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.name" />
                                    </span> <span class="valueColCR">${companyCrInfo.location.name}</span>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.company" />
                            </span>
                            <hr class="titleLine" />
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.company.period" />
                                    </span> <span class="valueColCR">${companyCrInfo.company.period}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.company.startDate" />
                                    </span> <span class="valueColCR">${companyCrInfo.company.startDate}</span>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <span class="titleColCR"> <spring:theme
                                            code="text.account.profile.cr.info.company.endDate" />
                                    </span> <span class="valueColCR">${companyCrInfo.company.endDate}</span>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <span class="titleBox"> <spring:theme
                                    code="text.account.profile.cr.info.activities" />
                            </span>
                            <hr class="titleLine" />
                            <div class="row">
                                <span class="titleColCR desTitle"> <spring:theme
                                        code="text.account.profile.cr.info.activities.description" />
                                </span> <span class="valueColCR desValue">
                                    ${companyCrInfo.activities.description}</span> <br /> <br />
                                <table class=" responsive-table crTable">
                                    <tr class="responsive-table-head hidden-xs">
                                        <th><spring:theme
                                                code="text.account.profile.cr.info.activities.isic.id" /></th>
                                        <th><spring:theme
                                                code="text.account.profile.cr.info.activities.isic.name" />
                                        </th>
                                        <th><spring:theme
                                                code="text.account.profile.cr.info.activities.isic.nameEn" />${isic.nameEn }
                                        </th>
                                    </tr>
                                    <c:forEach items="${companyCrInfo.activities.isic }" var="isic">
                                        <tr class="responsive-table-item">
                                            <td class="hidden-sm hidden-md hidden-lg mobileTitleth">
                                                <spring:theme
                                                    code="text.account.profile.cr.info.activities.isic.id" />
                                            </td>
                                            <td class="responsive-table-cell">${isic.id }</td>
                                            <td class="hidden-sm hidden-md hidden-lg mobileTitleth">
                                                <spring:theme
                                                    code="text.account.profile.cr.info.activities.isic.name" />
                                            </td>
                                            <td class="responsive-table-cell">${isic.name }</td>
                                            <td class="hidden-sm hidden-md hidden-lg mobileTitleth">
                                                <spring:theme
                                                    code="text.account.profile.cr.info.activities.isic.nameEn" />
                                            </td>
                                            <td class="responsive-table-cell">${isic.nameEN }</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>--%>
                    </c:when>
                    <c:otherwise>
                        <div class="content-empty box text-center">
                            <spring:theme code="text.account.profile.cr.info.is.empty"/>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>