<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page2 pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature" element="div"   class="row">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section2" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper witharrow col-md-4 col-sm-4 col-xs-4"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section3" var="feature" element="div" class="row paddfull header_style grayback">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  text-center col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section4" var="feature" element="div" class="row paddfull grayback">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-4 col-sm-6 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section5" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section6" var="feature" element="div" class="row">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section7" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section8" var="feature" element="div" class="row  blueback">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section9" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section10" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
</template:page2>
