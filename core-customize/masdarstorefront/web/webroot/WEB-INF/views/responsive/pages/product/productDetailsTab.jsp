<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<div class="box m-t-20">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="tab-container">
					<product:productDetailsTab product="${product}" />
				</div>
			</div>
		</div>
	</div>


