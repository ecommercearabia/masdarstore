<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true"/>


<spring:url value="/my-account/my-quotes/" var="quoteBaseLink" htmlEscape="false"/>
<c:url value="/" var="homeLink"/>

<div class="checkout-success__body">
    <div class="box quotethankDiv">
        <div class="checkout-success__body__headline">
            <spring:theme code="quote.confirmation.title"/>
        </div>
       <p><spring:theme code="quote.confirmation.content"/><b> ${fn:escapeXml(quoteData.code)}</b></p>
       <p><spring:theme code="checkout.quoteConfirmation.copySentToShort"/><b> ${fn:escapeXml(email)}</b></p>
        <a href="${homeLink}" class="btn btn-default"> <spring:theme
                code="quote.confirmation.start.shopping"/>
        </a>

        <a href="${quoteBaseLink}" class="btn btn-default"> <spring:theme
                code="quote.confirmation.my.quotes"/>
        </a>
    </div>
</div>
	


