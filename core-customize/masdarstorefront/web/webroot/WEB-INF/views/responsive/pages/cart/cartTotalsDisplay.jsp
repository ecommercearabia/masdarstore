<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-4">
       
            <cart:cartVoucher cartData="${cartData}"/>
       
    </div>
    <div class="col-xs-12 col-md-6 col-lg-4 pull-right">
        <div class="cart-totals box">
            <cart:cartTotals cartData="${cartData}" showTax="false"/>
            <cart:ajaxCartTotals/>
        </div>
    </div>
</div>