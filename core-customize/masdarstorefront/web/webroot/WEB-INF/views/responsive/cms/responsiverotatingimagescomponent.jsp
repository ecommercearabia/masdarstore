<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/responsive/component"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
	<c:when test="${theme eq 'THEME1'}">
		<c:if test="${not empty banners}">
			<div class="main_banner">
				<c:if test="${not empty title}"> <div class="headline">${title}<br/><span>${content}</span></div></c:if>
				<div class="owl-carousel carousel js-owl-carousel owl-carousel js-owl-rotating-image owl-theme">
					<c:forEach items="${banners}" var="banner" varStatus="loop">
						<div class="item">
							<component:simpleresponsivebanner medias="${banner.medias}" urlLink="${banner.urlLink}" header="${banner.header}" content="${banner.content}"  linkTarget="${banner.linkTarget}"   youtubeId="${banner.youtubeId}"   />
						</div>
					</c:forEach>
				</div>
			</div>
		</c:if>
	</c:when>
	<c:otherwise>
		<c:if test="${not empty banners}">
			<div class="carousel__component text-center">
				<c:if test="${not empty title}"> <div class="carousel__component--headline">${title}<br/><span>${content}</span></div></c:if>
				<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl--theme1 owl-theme">
					<c:forEach items="${banners}" var="banner" varStatus="loop">
						<div class="carousel__item">
							<component:simpleresponsivebanner medias="${banner.medias}" urlLink="${banner.urlLink}" header="${banner.header}" content="${banner.content}"  linkTarget="${banner.linkTarget}"   youtubeId="${banner.youtubeId}"   />
						</div>
					</c:forEach>
				</div>
			</div>
		</c:if>
	</c:otherwise>
</c:choose>
