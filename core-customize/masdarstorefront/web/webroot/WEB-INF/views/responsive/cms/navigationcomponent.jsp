<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set value="${fn:escapeXml(component.styleClass)}" var="navigationClassHtml" />

<c:if test="${component.visible}">

       <li class="secondary"  aria-haspopup="true">
            <c:if test="${not empty component.navigationNode.title }">
                <a href="javascript:;">
                  <i class="fas fa-user"></i> <c:out value="${component.navigationNode.title}"/>
                </a>
            </c:if>
            <div class="wsshoptabing wtsdepartmentmenu clearfix">
              <div class="wsshopwp clearfix">
            <ul class="wstliststy02 clearfix">
            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="hidden-sm hidden-xs hidden-company">
									<ycommerce:testId code="header_LoggedUser">
										<a><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" /></a>
									</ycommerce:testId>
								</li>
							</sec:authorize>
            <c:forEach items="${component.navigationNode.children}" var="topLevelChild">
                <c:forEach items="${topLevelChild.entries}" var="entry">
                  
                        <cms:component component="${entry.item}" evaluateRestriction="true" element="li"/>
                  
                </c:forEach>
            </c:forEach>
            </ul>
            </div>
            </div>
        </li>
        
        	
   
</c:if>