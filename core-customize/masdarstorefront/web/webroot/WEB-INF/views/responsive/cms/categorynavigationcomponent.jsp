<%@ page trimDirectiveWhitespaces="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>
<spring:theme code="label.incVat" var="incVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.Vat" var="VatLabel"/>
<c:if test="${component.visible}">


    <nav class="wsmenu clearfix">
        <ul class="wsmenu-list hidden-xs hidden-sm">
            <li aria-haspopup="true"><a href="#" class="navtext"><span><i class="far fa-bars"></i> <spring:theme
                    code="text.categorization" text="categorization"/></span></a>
                <div class="wsshoptabing wtsdepartmentmenu clearfix">
                    <div class="wsshopwp clearfix">
                        <ul class="wstabitem clearfix">
                            <c:forEach items="${component.navigationNode.children}" var="childLevel1" varStatus="loop">
                                <li class="${loop.index eq 0 ? 'wsshoplink-active' : ''}">
                                    <c:set var="name" value=""></c:set>
                                    <c:set var="totalSubNavigationColumns" value="${0}"/>
                                    <c:set var="hasSubChild" value="false"/>
                                    <c:forEach items="${childLevel1.children}" var="childLevel2">
                                        <c:if test="${not empty childLevel2.children}">
                                            <c:set var="hasSubChild" value="true"/>
                                            <c:set var="subSectionColumns"
                                                   value="${fn:length(childLevel2.children)/component.wrapAfter}"/>
                                            <c:if test="${subSectionColumns > 1 && fn:length(childLevel2.children)%component.wrapAfter > 0}">
                                                <c:set var="subSectionColumns" value="${subSectionColumns + 1}"/>
                                            </c:if>
                                            <c:choose>
                                                <c:when test="${subSectionColumns > 1}">
                                                    <c:set var="totalSubNavigationColumns"
                                                           value="${totalSubNavigationColumns + subSectionColumns}"/>
                                                </c:when>

                                                <c:when test="${subSectionColumns < 1}">
                                                    <c:set var="totalSubNavigationColumns"
                                                           value="${totalSubNavigationColumns + 1}"/>
                                                </c:when>
                                            </c:choose>
                                        </c:if>
                                    </c:forEach>
                                    <c:forEach items="${childLevel1.entries}" var="childlink1">
                                        <c:if test="${not empty childlink1.item && childlink1.item.visible }">
                                            <c:set var="name" value="${childlink1.item.linkName}"></c:set>
                                            <%-- 								<cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="nav__link js_nav__link" /> --%>
                                            <c:url value="${childlink1.item.url}" var="link"></c:url>
                                            <a href="${link}" class="a_link_hover">
                                                <c:if test="${not empty childlink1.navigationNode.icon}">
                                                    <img class="cat_icon"
                                                         title="${fn:escapeXml(childlink1.navigationNode.icon.altText)}"
                                                         alt="${fn:escapeXml(childlink1.navigationNode.icon.altText)}"
                                                         src="${fn:escapeXml(childlink1.navigationNode.icon.url)}">
                                                </c:if>
                                                <span class="cat_name">${childlink1.item.linkName}</span>


                                            </a>

                                            <c:if test="${not empty childLevel1.children}">
                                                <div class="wstitemright clearfix wstitemrightactive">
                                                    <div class="container-fluid">
                                                        <div class="row">

                                                            <c:choose>
                                                            <c:when test="${hasSubChild}">
                                                            <c:forEach items="${childLevel1.children}"
                                                                       var="childLevel2">
                                                            <ul class="wstliststy02 clearfix">
                                                                <c:forEach items="${childLevel2.children}"
                                                                           var="childLevel3"
                                                                           step="${component.wrapAfter}" varStatus="i">

                                                                <c:forEach items="${childLevel2.children}"
                                                                           var="childLevel3" begin="${i.index}"
                                                                           end="${i.index + component.wrapAfter - 1}">
                                                                <c:forEach items="${childLevel3.entries}"
                                                                           var="childlink3">
                                                                    <cms:component component="${childlink3.item}"
                                                                                   evaluateRestriction="true"
                                                                                   element="li"/>
                                                                </c:forEach>
                                                                </c:forEach>
                                                                </c:forEach>
                                                                <ul>
                                                                    </c:forEach>
                                                                    </c:when>
                                                                    <c:otherwise>

                                                                    <c:forEach items="${childLevel1.children}"
                                                                               var="childLevel2">
                                                                    <ul class="wstliststy02 clearfix">
                                                                        <c:forEach items="${childLevel2.entries}"
                                                                                   var="childlink2">
                                                                            <cms:component
                                                                                    component="${childlink2.item}"
                                                                                    evaluateRestriction="true"
                                                                                    element="li"/>
                                                                        </c:forEach>
                                                                    </ul>
                                                                    </c:forEach>

                                                                    </c:otherwise>
                                                                    </c:choose>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>

                                        </c:if>
                                    </c:forEach>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="wsmenu-list hidden-lg hidden-md">
            <c:url var="b2cLoginUrl" value="${siteBaseUrl}/login"/>
            <c:url var="b2cRegisterUrl" value="${siteBaseUrl}/register"/>
            <c:url var="b2bLoginUrl" value="/login"/>
            <c:url var="b2bRegisterUrl" value="/register"/>

            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">


                <li class="primary" aria-haspopup="false">
                    <ycommerce:testId code="header_Login_link">
                        <c:url value="/login" var="loginUrl"/>
                        <a href="${b2bLoginUrl}">
                            <span class="fas fa-user"></span> <spring:theme code="header.link.login"/>
                        </a>
                        <a href="javaScript:;" class="hidden">
                            <span class="fas fa-user"></span> <spring:theme code="header.link.login"/>
                        </a>
                        <div class="wsshoptabing wtsdepartmentmenu clearfix">
                            <div class="wsshopwp clearfix">
                                <ul class="wstliststy02 clearfix">
                                    <li>
                                        <a href="${b2bLoginUrl}">
                                            <spring:theme code="header.link.login.b2b"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="${b2cLoginUrl}">
                                            <spring:theme code="header.link.login.b2c"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </ycommerce:testId>
                </li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                <li class="primary" aria-haspopup="false">
                    <ycommerce:testId code="header_Login_link">
                        <c:url value="/register" var="registerUrl"/>
                        <a href="${b2bRegisterUrl}">
                            <span class="fas fa-briefcase"></span> <spring:theme
                            code="text.secureportal.link.createAccount"/>
                        </a>
                        <a href="javaScript:;" class="hidden">
                            <span class="fas fa-briefcase"></span> <spring:theme
                                code="text.secureportal.link.createAccount"/>
                        </a>
                        <div class="wsshoptabing wtsdepartmentmenu clearfix">
                            <div class="wsshopwp clearfix">
                                <ul class="wstliststy02 clearfix">

                                    <li>
                                        <a href="${b2bRegisterUrl}">
                                            <spring:theme code="header.link.register.b2b"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="${b2cRegisterUrl}">
                                            <spring:theme code="header.link.register.b2c"/>
                                        </a>
                                    </li>


                                </ul>

                            </div>
                        </div>
                    </ycommerce:testId>
                </li>
            </sec:authorize>


            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                <c:set var="maxNumberChars" value="25"/>
                <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                    <c:set target="${user}" property="firstName"
                           value="${fn:substring(user.firstName, 0, maxNumberChars)}..."/>
                </c:if>

                <li class="primary" aria-haspopup="true">
                    <ycommerce:testId code="header_LoggedUser">
                        <a> <spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}"/></a>
                    </ycommerce:testId>
                </li>
            </sec:authorize>


            <c:if test="${uiExperienceOverride}">
                <li class="primary" aria-haspopup="true">
                    <c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl"/>
                    <a href="${fn:escapeXml(backToMobileStoreUrl)}">
                        <spring:theme code="text.backToMobileStore"/>
                    </a>
                </li>
            </c:if>


            <c:forEach items="${component.navigationNode.children}" var="childLevel1" varStatus="loop">
                <li aria-haspopup="true">

                    <c:set var="name" value=""></c:set>
                    <c:set var="totalSubNavigationColumns" value="${0}"/>
                    <c:set var="hasSubChild" value="false"/>
                    <c:forEach items="${childLevel1.children}" var="childLevel2">
                        <c:if test="${not empty childLevel2.children}">
                            <c:set var="hasSubChild" value="true"/>
                            <c:set var="subSectionColumns"
                                   value="${fn:length(childLevel2.children)/component.wrapAfter}"/>
                            <c:if test="${subSectionColumns > 1 && fn:length(childLevel2.children)%component.wrapAfter > 0}">
                                <c:set var="subSectionColumns" value="${subSectionColumns + 1}"/>
                            </c:if>
                            <c:choose>
                                <c:when test="${subSectionColumns > 1}">
                                    <c:set var="totalSubNavigationColumns"
                                           value="${totalSubNavigationColumns + subSectionColumns}"/>
                                </c:when>

                                <c:when test="${subSectionColumns < 1}">
                                    <c:set var="totalSubNavigationColumns" value="${totalSubNavigationColumns + 1}"/>
                                </c:when>
                            </c:choose>
                        </c:if>
                    </c:forEach>
                    <c:forEach items="${childLevel1.entries}" var="childlink1">
                        <c:if test="${not empty childlink1.item && childlink1.item.visible }">
                            <c:set var="name" value="${childlink1.item.linkName}"></c:set>
                            <%-- 								<cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="nav__link js_nav__link" /> --%>
                            <c:url value="${childlink1.item.url}" var="link"></c:url>
                            <a href="${link}" class="a_link_hover">
                                <c:if test="${not empty childlink1.navigationNode.icon}">
                                    <img class="cat_icon"
                                         title="${fn:escapeXml(childlink1.navigationNode.icon.altText)}"
                                         alt="${fn:escapeXml(childlink1.navigationNode.icon.altText)}"
                                         src="${fn:escapeXml(childlink1.navigationNode.icon.url)}">
                                </c:if>
                                <span class="cat_name">${childlink1.item.linkName}</span>


                            </a>

                            <c:if test="${not empty childLevel1.children}">
                                <div class="wsshoptabing wtsdepartmentmenu clearfix">
                                    <div class="wsshopwp clearfix">


                                        <c:choose>
                                        <c:when test="${hasSubChild}">
                                        <c:forEach items="${childLevel1.children}" var="childLevel2">
                                        <ul class="wstliststy02 clearfix">
                                            <c:forEach items="${childLevel2.children}" var="childLevel3"
                                                       step="${component.wrapAfter}" varStatus="i">

                                            <c:forEach items="${childLevel2.children}" var="childLevel3"
                                                       begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                            <c:forEach items="${childLevel3.entries}" var="childlink3">
                                                <cms:component component="${childlink3.item}" evaluateRestriction="true"
                                                               element="li"/>
                                            </c:forEach>
                                            </c:forEach>
                                            </c:forEach>
                                            <ul>
                                                </c:forEach>
                                                </c:when>
                                                <c:otherwise>

                                                <c:forEach items="${childLevel1.children}" var="childLevel2">
                                                <ul class="wstliststy02 clearfix">
                                                    <c:forEach items="${childLevel2.entries}" var="childlink2">
                                                        <cms:component component="${childlink2.item}"
                                                                       evaluateRestriction="true" element="li"/>
                                                    </c:forEach>
                                                </ul>
                                                </c:forEach>

                                                </c:otherwise>
                                                </c:choose>
                                    </div>
                                </div>

                            </c:if>

                        </c:if>
                    </c:forEach>
                </li>
            </c:forEach>

            <cms:pageSlot position="LastOrderQuote" var="component">
                <cms:component component="${component}"/>
            </cms:pageSlot>

            <li class="swithchTax">
                        <span class="labelVat">
                                ${VatLabel}
                        </span>

                    ${exVatLabel}

                <c:choose>
                    <c:when test="${incTax}">
                        <label class="switch">
                            <input class="Vat_switch" type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>

                    </c:when>
                    <c:otherwise>
                        <label class="switch">
                            <input class="Vat_switch" type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </c:otherwise>
                </c:choose>
                    ${incVatLabel}
            </li>
            <li aria-haspopup="true" class="secondarycolor">
              
                <a href="${siteBaseUrl}"><spring:theme code="header.link.individual.site"/></a>
            </li>

            <c:if test="${not empty cmsSite.contactUsNumber}">
                <li aria-haspopup="true" class="secondarycolor">
                    <c:set var="datain"><spring:theme code="text.contactus"
                                                      arguments="${cmsSite.contactUsNumber}"/></c:set>
                    <c:set var="dateParts" value='${fn:split(datain, "+")}'/>
                    <a href="tel:${dateParts[1]}"><span class="fas fa-phone"></span> <spring:theme code="text.contactus"
                                                                                                   arguments="${cmsSite.contactUsNumber}"/></a>
                </li>
            </c:if>
            <li aria-haspopup="true" class="secondarycolor">
                <c:url value="/store-finder" var="storeFinderUrl"/>
                <a href="${fn:escapeXml(storeFinderUrl)}">
                    <span class="fas fa-map-marker-alt"></span> <spring:theme code="branches.locator"
                                                                              text="branches locator"/>
                </a>
            </li>

            
            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                <li aria-haspopup="true" class="secondarycolor">
                    <ycommerce:testId code="header_signOut">
                        <c:url value="/logout" var="logoutUrl"/>
                        <a href="${fn:escapeXml(logoutUrl)}">
                            <span class="fas fa-sign-out-alt"></span> <spring:theme code="header.link.logout"/>
                        </a>
                    </ycommerce:testId>
                </li>
            </sec:authorize>


            <li>
                <header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>
            </li>
        </ul>
    </nav>

</c:if>





