<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="${component.urlLink}" var="bannerUrl" />
<c:url value="${component.external}" var="external" />


	<c:if test="${ycommerce:validateUrlScheme(component.media.url)}">
		<c:choose>
			<c:when test="${empty bannerUrl || bannerUrl eq '#' || !ycommerce:validateUrlScheme(bannerUrl)}">
				<div class="banner__component simple-banner">
				<img title="${fn:escapeXml(component.media.altText)}" alt="${fn:escapeXml(component.media.altText)}"
					src="${fn:escapeXml(component.media.url)}">
				</div>
				<c:if test="${not empty component.content || not empty component.headline}"><div class="banner__component2"></c:if>
				<c:if test="${not empty component.headline}">
				<h2>${component.headline}</h2>
				</c:if>
				<c:if test="${not empty component.content}">
				<h3>${component.content}</h3>
				</c:if>
				<c:if test="${not empty component.content || not empty component.headline}"></div></c:if>
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(bannerUrl)}">
				<div class="banner__component simple-banner">
				<img title="${fn:escapeXml(component.media.altText)}"
					alt="${fn:escapeXml(component.media.altText)}" src="${fn:escapeXml(component.media.url)}">
				</div>	
				<c:if test="${not empty component.content || not empty component.headline}"><div class="banner__component2"></c:if>
				<c:if test="${not empty component.headline}">
				<h2>${component.headline}</h2>
				</c:if>
				<c:if test="${not empty component.content}">
				<h3>${component.content}</h3>
				</c:if>
				<c:if test="${not empty component.content || not empty component.headline}"></div></c:if>
					</a>

			</c:otherwise>
		</c:choose>
	</c:if>

