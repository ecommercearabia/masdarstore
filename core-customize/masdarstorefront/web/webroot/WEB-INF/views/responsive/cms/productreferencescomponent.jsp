<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<div class="carousel-component box m-t-20">

			<div class="carousel__component--headline">${fn:escapeXml(component.title)}</div>

			<div class="carousel__component--carousel js-owl-carousel  owl-carousel js-owl-theme1">

				
				<c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="productReference">
					<c:url value="${productReference.target.url}/quickView" var="productUrl"/>
					<div class="carousel__item">
						<a href="${fn:escapeXml(productUrl)}" class="js-reference-item" data-quickview-title="<spring:theme code="popup.quick.view.select"/></span>">
                            <div class="carousel__item--thumb relativepos">
                               <c:if test="${not empty productReference.target.productLabel}">
										<span class="productlabel">${fn:escapeXml(productReference.target.productLabel)}</span>
										</c:if>
							    <product:productPrimaryReferenceImage product="${productReference.target}" format="zoom" />
                            </div>
						 	
                            <c:if test="${component.displayProductTitles}">
                                <div class="carousel__item--name">${fn:escapeXml(productReference.target.name)}</div>
                            </c:if>
                            <c:if test="${component.displayProductPrices}">
                                <div class="carousel__item--price">
                                    <ycommerce:testId code="product_productPrice">
										<product:productPricePanel product="${productReference.target}"/>
									</ycommerce:testId>
                                </div>
                            </c:if>
						</a>
		
					</div>
				</c:forEach>
			</div>
		</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>