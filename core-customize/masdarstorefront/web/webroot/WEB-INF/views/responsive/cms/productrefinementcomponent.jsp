<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet box m-t-20">
   <div class="facetheader hidden-md hidden-lg"><spring:theme code="filter" text="filter"/> <a href="javascript:;" class="closedivfacet" ><i class="fas fa-times"></i></a></div>
    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
    <nav:facetNavRefinements pageData="${searchPageData}"/>
</div>