<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="option" required="true" type="com.masdar.facades.data.ProductConfigurationOptionData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<option value="${fn:escapeXml(option.name)}" ${isSelected ? 'selected="selected"' : ''}>
	${fn:escapeXml(option.name)}
</option>