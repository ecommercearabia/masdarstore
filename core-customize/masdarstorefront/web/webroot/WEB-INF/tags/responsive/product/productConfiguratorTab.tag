<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="configurations" required="true" type="java.util.ArrayList" %>
<%@ attribute name="readOnly" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="product__config">
    <c:forEach var="configuration" items="${configurations}">
        <div class="product__config-row">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <c:set var="configurationId" value="configurationsKeyValueMap[${configuration.configuratorType}][${configuration.configurationLabel}]"/>
						<c:choose>
							<c:when test="${not empty configuration.options}">
								<label for="${fn:escapeXml(configurationId)}">
                                ${fn:escapeXml(configuration.configurationLabel)}
                      			  </label>
                      			  
								<select id="${fn:escapeXml(configurationId)}" name="${fn:escapeXml(configurationId)}" class="form-control">
									<c:forEach items="${configuration.options}" var="option">
										<product:configurationOption option="${option}" />
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<label for="${fn:escapeXml(configurationId)}">
                                ${fn:escapeXml(configuration.configurationLabel)}
                      			  </label>
                        		<input <c:if test="${readOnly}"> readonly </c:if>
                               id="${fn:escapeXml(configurationId)}"
                               name="${fn:escapeXml(configurationId)}"
                               class="form-control" type="text" value="${fn:escapeXml(configuration.configurationValue)}">
							</c:otherwise>
						</c:choose>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>