<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
		<div class="box m-t-20 relativepos">
		<c:if test="${not empty product.productLabel}">
		<span class="productlabel">${fn:escapeXml(product.productLabel)}</span>
		</c:if>
		<product:productImagePanel galleryImages="${galleryImages}" />
		</div>
	</div>
	<div class="clearfix hidden-sm hidden-md hidden-lg"></div>
	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
		<div class="product-main-info box m-t-20">

			

		
			
					<div class="product-details">

	<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
		<div class="name">${fn:escapeXml(product.name)}</div>
		<span class="sku">SKU</span><span class="code">${fn:escapeXml(product.code)}</span>
	</ycommerce:testId>
	<product:productReviewSummary product="${product}" showLinks="true"/>

						<product:productPromotionSection product="${product}"/>
						
						
						
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" />
						</ycommerce:testId>
						
					</div>
			

				
					<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
					</cms:pageSlot>
					<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
					</cms:pageSlot>
				
			
		</div>

	</div>
</div>