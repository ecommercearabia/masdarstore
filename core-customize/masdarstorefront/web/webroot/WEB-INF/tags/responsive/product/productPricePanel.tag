<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="isOrderForm" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="product.incVat" var="incVatLabel"/>
<spring:theme code="product.exVat" var="exVatLabel"/>


<c:choose>
    <c:when test="${incTax}">


        <c:choose>

            <c:when test="${not empty product.discount}">
                <p class="price">
                    <c:if test="${not empty product.discountWithTax.discountPrice}">
                        <span class="perprice">%${product.discountWithTax.percentage}</span>
                        <span class="oldprice"><format:fromPrice priceData="${product.priceWithTax}"/></span>

                        <span class="newprice"><format:fromPrice
                                priceData="${product.discountWithTax.discountPrice}"/> <span
                                class="incVat">${incVatLabel}</span></span>

                        <br/>
                        <span class="oldprice"><format:fromPrice priceData="${product.priceWithoutTax}"/></span>

                        <span class="newprice priceDefaultColor"><format:fromPrice
                                priceData="${product.discountWithoutTax.discountPrice}"/> <span
                                class="exVat">${exVatLabel}</span></span>

                        <span class="saveprice"><spring:theme code="price.save" text="You save "/> <format:fromPrice
                                priceData="${product.discountWithTax.saving}"/></span>
                    </c:if>
                    <c:if test="${empty product.discountWithTax.discountPrice}">
                        <format:fromPrice priceData="${product.priceWithTax}"/>
                    </c:if>
                </p>
            </c:when>
            <c:when test="${empty product.volumePrices}">

                <c:choose>
                    <c:when test="${(not empty product.priceRangeWithTax)  and ((empty product.baseProduct) or (not empty isOrderForm and isOrderForm))}">
                       
                        <c:choose>
                        
                        <c:when test="${(product.priceRangeWithTax.minPrice.value ne product.priceRangeWithTax.maxPrice.value) }">
                        <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithTax.minPrice}"/>
                            <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                            <span>
                                &nbsp; - &nbsp;
				</span>
                            <span>
					<format:price priceData="${product.priceRangeWithTax.maxPrice}"/> <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                        </p>


                        <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithoutTax.minPrice}"/>
                            <span
                                    class="exVat">${exVatLabel}</span>



				</span>
                            <span>
					&nbsp; -  &nbsp;
				</span>
                            <span>
					<format:price priceData="${product.priceRangeWithoutTax.maxPrice}"/> <span
                                    class="exVat">${exVatLabel}</span>
				</span>
                        </p>
                        
                        
                        </c:when>
                        
                        
                        
                        <c:otherwise>
                         <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithTax.minPrice}"/>
                            <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                           
                        </p>


                        <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithoutTax.minPrice}"/>
                            <span
                                    class="exVat">${exVatLabel}</span>



				</span>
                            
                        </p>
                        
                        
                        
                        
                        
                        
                        </c:otherwise>
                        
                        
                        </c:choose>
                       

                    </c:when>
                    <c:otherwise>
                        <p class="price">
                                <%-- <format:fromPrice priceData="${product.price}"/> --%>
                            <format:fromPrice priceData="${product.priceWithTax}"/> <span
                                class="incVat">${incVatLabel}</span>
                            <br/>
                            <span class="priceDefaultColor"> <format:fromPrice priceData="${product.priceWithoutTax}"/>
                                <span class="exVat">${exVatLabel}</span>
                                   </span>

                        </p>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty product.volumePricesWithTax}">
                    <ul class="volPrice">
                        <c:forEach var="volPrice" items="${product.volumePricesWithTax}">

                            <li>

                                <c:choose>
                                    <c:when test="${empty volPrice.maxQuantity}">
                                        <i>    ${volPrice.minQuantity}+</i>
                                    </c:when>
                                    <c:otherwise>
                                        <i>${volPrice.minQuantity}-${volPrice.maxQuantity}</i>
                                    </c:otherwise>
                                </c:choose>
                                <span>${fn:escapeXml(volPrice.formattedValue)}  <span
                                        class="incVat">${incVatLabel}</span></span>

                            </li>
                        </c:forEach>
                        <br/>
                        <c:forEach var="volPrice" items="${product.volumePricesWithoutTax}">

                            <li>


                                <span>${fn:escapeXml(volPrice.formattedValue)} <span
                                        class="exVat">${exVatLabel}</span></span>

                            </li>
                        </c:forEach>


                    </ul>

                </c:if>

            </c:otherwise>
        </c:choose>
    </c:when>

    <c:otherwise>
    
        <c:choose>

            <c:when test="${not empty product.discount}">
                <p class="price">
                    <c:if test="${not empty product.discountWithoutTax.discountPrice}">
                        <span class="perprice">%${product.discountWithoutTax.percentage}</span>
                        <span class="oldprice"><format:fromPrice priceData="${product.priceWithoutTax}"/></span>

                        <span class="newprice"><format:fromPrice
                                priceData="${product.discountWithoutTax.discountPrice}"/> <span
                                class="exVat">${exVatLabel}</span></span>

                        <br/>
                        <span class="oldprice"><format:fromPrice priceData="${product.priceWithTax}"/></span>

                        <span class="newprice priceDefaultColor"><format:fromPrice
                                priceData="${product.discountWithTax.discountPrice}"/> <span
                                class="incVat">${incVatLabel}</span></span>


                        <span class="saveprice"><spring:theme code="price.save" text="You save "/> <format:fromPrice
                                priceData="${product.discountWithoutTax.saving}"/></span>
                    </c:if>
                    <c:if test="${empty product.discountWithoutTax.discountPrice}">
                        <format:fromPrice priceData="${product.priceWithoutTax}"/>
                    </c:if>
                </p>
            </c:when>
            <c:when test="${empty product.volumePrices}">

                <c:choose>
                    <c:when test="${(not empty product.priceRangeWithoutTax)  and ((empty product.baseProduct) or (not empty isOrderForm and isOrderForm))}">
                         <c:choose>
                        
                        <c:when test="${(product.priceRangeWithTax.minPrice.value ne product.priceRangeWithTax.maxPrice.value) }">
                     


                        <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithoutTax.minPrice}"/>
                            <span
                                    class="exVat">${exVatLabel}</span>



				</span>
                            <span>
					&nbsp; -  &nbsp;
				</span>
                            <span>
					<format:price priceData="${product.priceRangeWithoutTax.maxPrice}"/> <span
                                    class="exVat">${exVatLabel}</span>
				</span>
                        </p>
                        
                           <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithTax.minPrice}"/>
                            <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                            <span>
                                &nbsp; - &nbsp;
				</span>
                            <span>
					<format:price priceData="${product.priceRangeWithTax.maxPrice}"/> <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                        </p>
                        
                        </c:when>
                        
                        
                        
                        <c:otherwise>
                          <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithoutTax.minPrice}"/>
                            <span
                                    class="exVat">${exVatLabel}</span>



				</span>
                            
                        </p>
                         <p class="price">
                        <span>
					<format:price priceData="${product.priceRangeWithTax.minPrice}"/>
                            <span
                                    class="incVat">${incVatLabel}</span>
				</span>
                           
                        </p>


                      
                        
                        
                        
                        
                        
                        
                        </c:otherwise>
                        
                        
                        </c:choose>
                    </c:when>
                    <c:otherwise>

                        <p class="price">
                                <%-- <format:fromPrice priceData="${product.price}"/> --%>

                            <format:fromPrice priceData="${product.priceWithoutTax}"/>
                            <span class="exVat">${exVatLabel}</span>

                            <br/>
                            <span class="priceDefaultColor">  <format:fromPrice
                                    priceData="${product.priceWithTax}"/> <span
                                    class="incVat">${incVatLabel}</span></span>


                        </p>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty product.volumePricesWithoutTax}">
                    <ul class="volPrice">
                        <c:forEach var="volPrice" items="${product.volumePricesWithoutTax}">

                            <li>

                                <c:choose>
                                    <c:when test="${empty volPrice.maxQuantity}">
                                        <i>    ${volPrice.minQuantity}+</i>
                                    </c:when>
                                    <c:otherwise>
                                        <i>${volPrice.minQuantity}-${volPrice.maxQuantity}</i>
                                    </c:otherwise>
                                </c:choose>
                                <span>${fn:escapeXml(volPrice.formattedValue)} <span
                                        class="exVat">${exVatLabel}</span></span>

                            </li>
                        </c:forEach>
                        <br/>

                        <c:forEach var="volPrice" items="${product.volumePricesWithTax}">

                            <li>


                                    <span>${fn:escapeXml(volPrice.formattedValue)}  <span
                                            class="incVat">${incVatLabel}</span></span>

                            </li>
                        </c:forEach>


                    </ul>

                </c:if>

            </c:otherwise>
        </c:choose>


    </c:otherwise>

</c:choose>


