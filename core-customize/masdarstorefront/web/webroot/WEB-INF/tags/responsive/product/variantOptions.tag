<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="statusCount" required="false" type="java.lang.Integer" %>

<%@ attribute name="variantOptions" required="false" type="java.util.List" %>
<%@ attribute name="selectedVariantOptionValue" required="false" type="de.hybris.platform.commercefacades.product.data.VariantOptionData" %>


            <div class="variant-section">
                <div class="variant-selector">
                    <div class="variant-name pull-left">
                        <label for="Type">
                        
                        <c:choose>
                        	<c:when test="${statusCount==1 }">Unit
                        	<c:set value="variant" var="classVariant"/>
                        	</c:when>
                        	<c:otherwise>Size & Color
                        	                        	<c:set value="variant2" var="classVariant"/>
                        	
                        	
                        	</c:otherwise>
                        
                        </c:choose>
                        
                        
                        
                        <span
                                class="variant-selected typeName"></span></label>
                    </div>
                    <select id="${classVariant}" class="form-control variant-select" disabled="disabled">
                        <option selected="selected" disabled="disabled"><spring:theme
                                code="product.variants.select.variant"/></option>
                        <c:forEach items="${variantOptions}" var="variantOption">
                            <c:set var="optionsStringHtml" value=""/>
                            <c:set var="nameStringHtml" value=""/>
                            <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
                            	<c:if test="${not empty variantOptionQualifier.value  && !(statusCount>1 && variantOptionQualifier.qualifier eq 'unitVariant') }">
                            	
                            	
                                <c:set var="optionsStringHtml">${optionsStringHtml}&nbsp;${fn:escapeXml(variantOptionQualifier.value)} </c:set>
                                <c:set var="nameStringHtml">${fn:escapeXml(variantOptionQualifier.value)}</c:set>
                                </c:if>
                            </c:forEach>

                            <c:if test="${(variantOption.stock.stockLevel gt 0) and (variantSize.stock.stockLevelStatus ne 'outOfStock')}">
                                <c:set var="stockLevelHtml">${fn:escapeXml(variantOption.stock.stockLevel)} <spring:theme code="product.variants.in.stock"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus eq 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.available"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus ne 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.out.of.stock"/></c:set>
                            </c:if>
                            <c:choose>
                                <c:when test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
                                    <c:set var="showAddToCart" value="${true}" scope="session"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="showAddToCart" value="${false}" scope="session"/>
                                </c:otherwise>
                            </c:choose>
                            <c:url value="${variantOption.url}" var="variantOptionUrl"/>
                            <c:if test="${(variantOption.url eq product.url)}">
                                <c:set var="showAddToCart" value="${true}" scope="session"/>
                                <c:set var="currentSizeHtml" value="${nameStringHtml}"/>
                            </c:if>
                           
                            <option value="${fn:escapeXml(variantOptionUrl)}" ${(variantOption.url eq selectedVariantOptionValue.url) ? 'selected="selected"' : ''}>
                                <span class="variant-selected">${optionsStringHtml}</span>
                            </option>
                        </c:forEach>
                    </select>
                    <div id="currentTypeValue" data-type-value="${currentSizeHtml}"></div>
                </div>
            </div>
            










































<%--         <c:if test="${not empty variantOptionValues}"> --%>
<!--             <div class="variant-section"> -->
<!--                 <div class="variant-selector"> -->
<!--                     <div class="variant-name"> -->
<%--                         <label for="Type">${variantOptionValues[0]}<span --%>
<!--                                 class="variant-selected typeName"></span></label> -->
<!--                     </div> -->
<!--                     <select id="" class="variantClass form-control variant-select show-tick "  disabled="disabled"> -->
<%--                         <option selected="selected" disabled="disabled"><spring:theme --%>
<%--                                 code="product.variants.select.variant"/></option> --%>
<%--                         <c:forEach items="${variantOptionValues}" varStatus="status"> --%>
                        
                       
<%--                         	<c:set var="variantOption" 	value="${variantOptionValues[fn:length(variantOptionValues) - status.count]}" /> --%>
			
<%--                             <c:set var="optionsString" value=""/> --%>
<%--                             <c:set var="nameString" value=""/> --%>
                       
<%--                             <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier"> --%>
<%--                                 <c:set var="optionsString">${optionsString}&nbsp;${fn:escapeXml(variantOptionQualifier.name)}&nbsp;${fn:escapeXml(variantOptionQualifier.value)}, </c:set> --%>
<%--                                 <c:set var="nameString">${fn:escapeXml(variantOptionQualifier.value)}</c:set> --%>
<%--                             </c:forEach> --%>

<%--                             <c:if test="${(variantOption.stock.stockLevel gt 0) and (variantSize.stock.stockLevelStatus ne 'outOfStock')}"> --%>
<%--                                 <c:set var="stockLevel">${variantOption.stock.stockLevel} <spring:theme --%>
<%--                                         code="product.variants.in.stock"/></c:set> --%>
<%--                             </c:if> --%>
<%--                             <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus eq 'inStock')}"> --%>
<%--                                 <c:set var="stockLevel"><spring:theme code="product.variants.available"/></c:set> --%>
<%--                             </c:if> --%>
<%--                             <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus ne 'inStock')}"> --%>
<%--                                 <c:set var="stockLevel"><spring:theme code="product.variants.out.of.stock"/></c:set> --%>
<%--                             </c:if> --%>

<%--                             <c:url value="${variantOption.url}" var="variantOptionUrl"/> --%>
                            
<%--                             <option value="${variantOptionUrl}" ${(variantOption.url eq selectedVariantOptionValue.url) ? 'selected="selected"' : ''}> --%>
<%--                                 <span class="variant-selected">${nameString} </span> --%>
<!--                             </option> -->
<%--                         </c:forEach> --%>
<!--                     </select> -->
<%--                     <div id="currentTypeValue" data-type-value="${currentSize}"></div> --%>
<!--                 </div> -->
<!--             </div> -->
<%--         </c:if> --%>