<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row">
<div class="tab-details col-lg-10 col-lg-offset-1 col-md-12">
	<div class="carousel__component--headline"><spring:theme code="overview.plp.title" text="Overview" /></div>
	<ycommerce:testId code="productDetails_content_label">
		<p>
			
			${product.description}
			
		<p>
	</ycommerce:testId>
</div>
</div>