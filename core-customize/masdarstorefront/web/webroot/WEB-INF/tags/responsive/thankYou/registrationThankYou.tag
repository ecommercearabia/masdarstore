<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:url value="/" var="link"/>
<c:set var="redirectCount" value="${cmsSite.thankYouPageCountDown}"/>
<c:set var="contentParagraph" value="${cmsSite.thankYouPageContent}"/>

<div class="row ">
    <div class="col-xs-12 col-md-6 col-md-offset-3  text-center ">
        <div class="thankYouDiv"><i class="fa fa-thumbs-up thankYouDivIcon"></i>
            <h3 class="headThanks"><spring:theme code="thankYou.title"/></h3>
            <p class="paragraphThanks">${contentParagraph }
            </p>
            <div><a href="${link}" class="btn btn-primary registerLinkRedirect"> <spring:theme
                    code="thankYou.start.shopping"/>
            </a></div>
            <div class="redirectDiv">
                <input id="countSecondRedirect" type="hidden" value="${redirectCount}"/>
                <spring:theme code="thankYou.redirect"/> &nbsp; <span
                    class="number-of-second-redirect">${redirectCount}</span> &nbsp;<spring:theme
                    code="thankYou.redirect.second"/>
            </div>
        </div>

    </div>

</div>
