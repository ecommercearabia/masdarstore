<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<footer >
   <div class="row paddfull">
  
    
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
    <cms:pageSlot position="LeftFooter" var="feature" element="div" class=" footerlogo text-center">
        <cms:component component="${feature}"/>
    </cms:pageSlot><br/>
     <cms:pageSlot position="FooterSocialMedia" var="feature" element="div" class=" footersocialmedia text-center" limit="1">
     ${feature.content}
<%--         <cms:component component="${feature}"/> --%>
    </cms:pageSlot>
    </div>
    
    
     <cms:pageSlot position="Footer" var="feature"  element="div" class="col-md-7 col-lg-7 col-sm-9 col-xs-12">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <cms:pageSlot position="RightFooter" var="feature"  element="div" class="col-md-2 col-lg-2 col-sm-3 col-xs-12  footertext">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    <div class="row paddfull lightback text-center">
    <cms:pageSlot position="CopyrightFooter" var="feature"  element="div" class="col-md-12">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
</footer>