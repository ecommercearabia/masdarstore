<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>

<c:url var="b2cLoginUrl" value="${siteBaseUrl}/login"/>
<c:url var="b2cRegisterUrl" value="${siteBaseUrl}/register"/>

<spring:theme code="label.incVat" var="incVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.Vat" var="VatLabel"/>
<spring:htmlEscape defaultHtmlEscape="true"/>
<input type="hidden" id="VAT" value="${incTax}"/>
<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">

    <c:if test="${ not user.isEmailVerified}">
        <div class="orangeback">
            <c:url value="/email/requestverify" var="link"></c:url>

            <b><i class="far fa-exclamation-triangle"></i><span>${cmsSite.emailVerificationMessage}</span><a
                    data-url="${link}" href="javascript:;" class="resend"><spring:theme
                    code="text.re_sendemail"/></a></b>
        </div>
    </c:if>

</sec:authorize>

<cms:pageSlot position="TopHeaderSlot" var="component" element="div">
    <cms:component component="${component}"/>
</cms:pageSlot>
<cms:pageSlot position="LastOrderQuote" var="component" element="ul" class="topsection hidden-sm hidden-xs">
    <cms:component component="${component}"/>
</cms:pageSlot>
<header class="js-mainHeader">

    <nav class="top_nav ">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-12 ">
                <div class="row">
                    <div class="col-sm-2 col-xs-3 hidden-md hidden-lg">
                        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    </div>

                    <div class="col-sm-8 col-xs-6 col-md-12 SiteLogo">
                        <cms:pageSlot position="SiteLogo" var="logo" limit="1">
                            <cms:component component="${logo}" element="div" class="yComponentWrapper"/>
                        </cms:pageSlot>
                    </div>
                    <div class="col-sm-2 col-xs-3 cartmobile hidden-md hidden-lg">
                        <cms:pageSlot position="MiniCart" var="cart" element="div"
                                      class="miniCartSlot componentContainer">
                            <cms:component component="${cart}"/>
                        </cms:pageSlot>
                    </div>
                </div>
            </div>


            <div class="col-lg-10 col-md-9 hidden-sm hidden-xs">
                <ul class="pull-right topnavlinks">

                    <li class="swithchTax">
                        <span class="labelVat">
                            ${VatLabel}
                        </span>

                        ${exVatLabel}

                        <c:choose>
                            <c:when test="${incTax}">
                                <label class="switch">
                                    <input class="Vat_switch" type="checkbox">
                                    <span class="slider round"></span>
                                </label>

                            </c:when>
                            <c:otherwise>
                                <label class="switch">
                                    <input class="Vat_switch" type="checkbox" checked>
                                    <span class="slider round"></span>
                                </label>
                            </c:otherwise>
                        </c:choose>
                        ${incVatLabel}
                    </li>

                    <c:if test="${empty hideHeaderLinks}">


                        <c:if test="${uiExperienceOverride}">
                            <li>
                                <c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl"/>
                                <a href="${fn:escapeXml(backToMobileStoreUrl)}">
                                    <spring:theme code="text.backToMobileStore"/>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${not empty cmsSite.contactUsNumber}">
                            <li>
                                <span class="fas fa-phone"></span> <spring:theme code="text.contactus"
                                                                                 arguments="${cmsSite.contactUsNumber}"/>
                            </li>
                        </c:if>
                        
						<li>
							<a href="${siteBaseUrl}"><spring:theme code="header.link.individual.site"/></a>
                        </li>

                        <header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>

                        <c:if test="${cmsSite.enableRegistration == true && isASMUser == true }">
                            <ycommerce:testId code="header_Login_link">
                                <li class="liOffcanvas">
                                    <c:url value="/credit-customer/register" var="creditCustomerRegistration"/>
                                    <a href="${fn:escapeXml(creditCustomerRegistration)}">
                                        <span class="fas fa-briefcase"></span> <spring:theme
                                            code="text.secureportal.link.createCreditAccount"/>
                                    </a>
                                </li>
                            </ycommerce:testId>
                        </c:if>
						<c:url var="b2cLoginUrl" value="${siteBaseUrl}/login"/>
						<c:url var="b2cRegisterUrl" value="${siteBaseUrl}/register"/>
						<c:url var="b2bLoginUrl" value="/login"/>
						<c:url var="b2bRegisterUrl" value="/register"/>
                        <c:if test="${cmsSite.enableRegistration == true }">
                            <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                <li aria-haspopup="false">
                                    <ycommerce:testId code="header_Login_link">
                                        <c:url value="/register" var="registerUrl"/>
                                        <a href="${b2bRegisterUrl}">
												<span class="fas fa-briefcase"></span> <spring:theme
                                                code="text.secureportal.link.createAccount"/>
											</a>
                                        <a href="javaScript:;" class="loginTapheader hidden">
                                            <span class="fas fa-briefcase"></span> <spring:theme
                                                code="text.secureportal.link.createAccount"/>
                                        </a>
										<ul class="registerTapContent">
										<li>
											<a href="${b2bRegisterUrl}">
												<spring:theme code="header.link.register.b2b"/>
											</a>
										</li>
										<li>
											<a href="${b2cRegisterUrl}">
												<spring:theme code="header.link.register.b2c"/>
											</a>
										</li>
									</ul>
                                    </ycommerce:testId>
                                </li>
                            </sec:authorize>
                        </c:if>


                        <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">

                            <ycommerce:testId code="header_Login_link">
                                <li class="liOffcanvas">
                                    <c:url value="/login" var="loginUrl"/>
                                    <a href="${b2bLoginUrl}">
												 <span class="fas fa-user"></span> <spring:theme code="header.link.login"/>
											</a>
                                    <a href="javaScript:;" class="loginTapheader hidden">
                                        <span class="fas fa-user"></span> <spring:theme code="header.link.login"/>
                                    </a>
<ul class="loginTapContent">
										<li>
											<a href="${b2bLoginUrl}">
												<spring:theme code="header.link.login.b2b"/>
											</a>
										</li>
										<li>
											<a href="${b2cLoginUrl}">
												<spring:theme code="header.link.login.b2c"/>
											</a>
										</li>
										</ul>
									

                                </li>
                            </ycommerce:testId>





                        </sec:authorize>

                        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                            <li class="liOffcanvas">
                                <ycommerce:testId code="header_signOut">
                                    <c:url value="/logout" var="logoutUrl"/>
                                    <a href="${fn:escapeXml(logoutUrl)}">
                                        <span class="fas fa-sign-out-alt"></span> <spring:theme
                                            code="header.link.logout"/>
                                    </a>
                                </ycommerce:testId>
                            </li>
                        </sec:authorize>

                    </c:if>
                    <c:if test="${empty hideHeaderLinks}">
                        <ycommerce:testId code="header_StoreFinder_link">
                            <li>
                                <c:url value="/store-finder" var="storeFinderUrl"/>
                                <a href="${fn:escapeXml(storeFinderUrl)}">
                                    <span class="fas fa-map-marker-alt"></span> <spring:theme code="branches.locator"
                                                                                              text="branches locator"/>
                                </a>
                            </li>
                        </ycommerce:testId>
                    </c:if>

                    <cms:pageSlot position="MiniCart" var="cart" element="li" class="miniCartSlot componentContainer">
                        <cms:component component="${cart}"/>
                    </cms:pageSlot>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="menu_search">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <nav:topNavigation/>
            </div>
            <div class="col-md-9 col-sm-12">
                <cms:pageSlot position="SearchBox" var="component">
                    <cms:component component="${component}" element="div"/>
                </cms:pageSlot>
            </div>
        </div>
    </nav>


    <a id="skiptonavigation"></a>

</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div" class="container-fluid">
    <cms:component component="${component}"/>
</cms:pageSlot>
<cms:pageSlot position="Breadcrumb" var="component" element="div" class="container-fluid">
    <cms:component component="${component}"/>
</cms:pageSlot>

