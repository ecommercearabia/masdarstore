ACC.region = {
    getCitiesByRegionId: function () {
        $("#regionId").change(
            function () {
                var regionId = $(this).val();

                $('#cityId').attr('data-live-search', 'true')

                $.ajax({
                    type: "GET",
                    url: ACC.config.encodedContextPath + "/misc/region/" + regionId + "/cities",
                    dataType: "json",
                    success: function (response) {

                        var emptyListLBL = $("#cityId option:first").html();
                        var options = '';
                        $('#cityId').html('');

                        var emptyOption = '<option value="" disabled="disabled" selected="selected" class="__web-inspector-hide-shortcut__">' + emptyListLBL + '</option>';
                        $('#cityId').append(emptyOption);
                        for (i = 0; i < response.data.length; i++) {
                            var code = response.data[i].code;
                            var name = response.data[i].name;
                            options += '<option value="' + code + '">' + name + '</option>';
                        }
                        $('#cityId').append(options);

                        $('#cityId').attr('data-live-search', 'true')


                        $('#cityId').selectpicker('refresh');

                    }
                });

//
//
//				$.ajax({
//					type: "GET",
//					url: ACC.config.encodedContextPath + "/misc/points-of-services/SA/" + regionId,
//					dataType: "json",
//					success: function(data) {
//						var options = '';
//						$('#posID').html('');
//						for (i = 0; i < data.length; i++) {
//							var code = data[i].name;
//							var name = data[i].displayName;
//							options += '<option value="' + code + '">' + name + '</option>';
//						}
//						$('#posID').append(options);
//						$('#posID').selectpicker('refresh');
//					}
//				});
            });
    },

    getRegistrationAreas: function () {
        $("#registrationCityId").change(
            function () {
                var cityCode = $(this).val();
                $.ajax({
                    type: "GET",
                    url: ACC.config.encodedContextPath + "/misc/city/" + cityCode + "/defaultRegistrationArea",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        var data = response.data;
                        $('#registrationDistrictsId').html('');
                        $('#registrationDistrictsId').val(data.code);
                    }
                });

            }
        );
    }
}

$(document).ready(function () {
    with (ACC.region) {
        getCitiesByRegionId();
        getRegistrationAreas();
    }
});
