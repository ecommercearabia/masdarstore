$('body').on('click', function (e) {

    $('.bootstrap-select.form-control').removeClass('open');

});
$("#change-country option, #mcountry option, #addresscountry option").each(function () {
    $(this).addClass("option-with-flag");
    $(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {

    $("select#regionId").attr('data-live-search', 'true')
    $("#regionId").selectpicker('refresh');

    $("select#registrationCityId").attr('data-live-search', 'true')
    $("#registrationCityId").selectpicker('refresh');

    $("select#cityId").attr('data-live-search', 'true')
    $("#cityId").selectpicker('refresh');
}
$('#change-country').selectpicker('refresh');
$('#mcountry').selectpicker('refresh');
$('#addresscountry').selectpicker('refresh');
$('select').selectpicker();

$("#mobileCountry option, #acountry option").each(function () {
    $(this).addClass("option-with-flag");
    $(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});

$('#mobileCountry').selectpicker('refresh');
$('#acountry').selectpicker('refresh');


$('select').selectpicker();
var ias = $.ias({
    container: ".product__listing.product__grid",
    item: ".product-item",
    delay: 300,
    pagination: ".plpage .pagination-bar.top .pagination",
    next: ".plpage .pagination-bar.top .pagination-next a"
});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
    text: '',
    offset: '100',
    html: '<div class="ias-trigger ias-trigger-next" ><a class="btn btn-primary btn-block">{text}</a></div>'// override text when no pages left
})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
    text: '',
}));

ias.on('rendered', function () {

    ACC.ratingstars.bindRatingStars();
    ACC.product.bindToAddToCartForm();
    ACC.product.bindToAddToCartStorePickUpForm();
    ACC.product.enableStorePickupButton();
    ACC.product.enableAddToCartButton();

});
$('.link_ar').click(function () {
    $('.lang-selector').val('ar');
    $(".lang-form").submit();

})
$('.link_en').click(function () {
    $('.lang-selector').val('en');
    $(".lang-form").submit();


})

$('.js-openTab').click(function () {
    $('.review-list').show();
    $('html, body').animate({
        scrollTop: $(".tabreview").offset().top
    }, 2000);
});
$('.closedivfacet').click(function () {
    $('#product-facet').removeClass('show-facet');

});
$('.closedivproduct').click(function () {
    $('.addproductdiv').hide();

});
$('.listview').click(function () {
    $('#displaymode').removeClass('grid_view').addClass('list_view');
    var acrionurl = $('#acrionurl').val()
    $.ajax({
        method: "POST",
        url: acrionurl + "list"
    });
})
$('.gridview').click(function () {
    $('#displaymode').removeClass('list_view').addClass('grid_view');
    var acrionurl = $('#acrionurl').val()
    $.ajax({
        method: "POST",
        url: acrionurl + "grid"
    });
})
$(".js-mainHeader").sticky({topSpacing: 0});


//qty in cartpage

$('.plus').click(function () {
    var form = $(this).closest('form');
    var productCode = form.find('input[name=quantity]').val();
    var initialCartQuantity = form.find('input[name=initialQuantity]').val();
    var newCartQuantity = parseFloat(initialCartQuantity) + 1;
    form.find('input[name=quantity]').val(newCartQuantity);

    form.submit();
});
$('.minus').click(function () {
    var form = $(this).closest('form');
    var productCode = form.find('input[name=productCode]').val();
    var initialCartQuantity = form.find('input[name=initialQuantity]').val();
    var newCartQuantity = parseFloat(initialCartQuantity) - 1;
    form.find('input[name=quantity]').val(newCartQuantity);

    form.submit();
});
$("footer .title, footer p").click(function () {
    $(this).toggleClass('activemenu');
});


if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    if($('.pageLabel-SecureCustomerPortalRegisterPage').length || $('.page-add-edit-address').length || $('addressForm').length ){}

    else{$('select').selectpicker('mobile');}
}
setTimeout(function () {
    $(".global-alerts .alert").hide('blind', {}, 500)
}, 5000);

$('input[type=radio][name=paymentType]').change(function () {
    if (this.value == 'ESAL') {
        $(".ifesal").addClass("hidden");
    } else {
        $(".ifesal").removeClass("hidden");
    }

});

/*$( ".virifyBtn" ).click(function(e) {
	e.preventDefault();
	var value= $('#companyCr').val()
	var urlget = ACC.config.encodedContextPath+'/misc/company-registration-info/'+value;
	
	$.get(urlget).done(function(obj) {
		var data=obj.data;
		console.log(obj)
		//set Data in template
		//section crInfo
		if(data){
		if(data.crName){$('.crName').html(data.crName)}
		if(data.crNumber){$('.crNumber').html(data.crNumber)}
			if(data.crEntityNumber){$('.crEntityNumber').html(data.crEntityNumber)}
				if(data.issueDate){$('.issueDate').html(data.issueDate)}
					if(data.crMainNumber){$('.crMainNumber').html(data.crMainNumber)}
						if(data.businessType.id){$('.businessTypeid').html(data.businessType.id)}
							if(data.businessType.name){$('.businessTypename').html(data.businessType.name)}
		
		
		//section CR FISCAL
		$('.fiscalYearmonth').html()
		$('.fiscalYearday').html()
		$('.calendarType').html()
		
		
		
		//section CR STATUS
		if(data.status){
			if(data.status.id){$('.statusid').html(data.status.id)}
			if(data.status.name){$('.statusname').html(data.status.name)}
			if(data.status.nameEn){$('.statusnameEn').html(data.status.nameEn)}
			}
		
		
		//section CR CANCELLATION
		if(data.cancellation){
		
			if(data.cancellation.data){$('.cancellationdate').html(data.cancellation.data)}
			if(data.cancellation.reason){$('.cancellationreason').html(data.cancellation.reason)}
			}
		
		
		
		//section CR LOCATION
		if(data.location){
			if(data.location.id){$('.locationid').html(data.location.id)}
			if(data.location.name){$('.locationname').html(data.location.name)}
		}
		
		
		//section CR COMPANY
		$('.companyperiod').html()
		$('.companystartDate').html()
		$('.companyendDate').html()
		
		
		//section CR ACTIVITIES
		if(data.activities){
			if(data.activities.description){$('.activitiesdescription').html(data.activities.description)}
			//table 
		
		}
		
		
		$(".companyCrpopup").fadeOut("medium").fadeOut("medium", function () {
			$(".companyCrpopup").removeClass('hidden');

		})
		}
		else{
			
			$(".companyCrpopupError").fadeOut("medium").fadeOut("medium", function () {
				$(".companyCrpopupError").removeClass('hidden');

			})
		}
	});
	
});*/


$('.companyCrpopup').click(function () {


    $(".companyCrpopup").fadeIn("medium").fadeIn("medium", function () {
        $(".companyCrpopup").addClass('hidden');

    })

})

$(".popupBoxcompanyCr").click(function (e) {
    // Do something
    e.stopPropagation();
});


$('.closepopupcompanyCr').click(function (e) {
    $('.companyCrpopup').click()

})

$('#companyCr').on("input", function () {

    if ($(this).val() == '') {
        $('.virifyBtn').prop("disabled", true);
    } else {

        $('.virifyBtn').prop("disabled", false);
    }
})


$('.companyCrpopupError').click(function () {


    $(".companyCrpopupError").fadeIn("medium").fadeIn("medium", function () {
        $(".companyCrpopupError").addClass('hidden');

    })

})

$(".popupBoxcompanyCrError").click(function (e) {
    // Do something
    e.stopPropagation();
});


$('.closepopupcompanyCrError').click(function (e) {
    $('.companyCrpopupError').click()

})

//var urlget = ACC.config.encodedContextPath+'/misc//points-of-services/SAU/NG';
//	
//	$.get(urlget).done(function(obj) {
//		
//		console.log(obj)
//	})


$('.resend').click(function (e) {

    var url = $(this).data('url');

    $.ajax({
        url: url,
        method: "post",
    }).done(function () {
        //console.log(data)
        $('.orangeback').addClass('hidden');
    });


})


if ($('.page-RegistrationThankYouPage').length) {
    var value = $('#countSecondRedirect').val();


    var timeleft = Number(value);
    var downloadTimer = setInterval(function () {
        if (timeleft === 0) {

            $('.registerLinkRedirect')[0].click()
            clearInterval(downloadTimer);

        }else{
            timeleft -= 1;
            $('.number-of-second-redirect').html(timeleft);
        }


    }, 1000);

}




$('.Vat_switch').change(function () {


    var tax = true
    var value = $('#VAT').val();

    if (value === 'true') {
        tax = false
    }
    console.log(typeof tax)

    var VatUrl = ACC.config.encodedContextPath + '/misc/inctax';


    $.post(VatUrl, {'incTax': tax},
        function (returnedData) {
            //console.log(returnedData);
            location.reload();
        })

})


$(document).ready(function () {


    if ($('.page-homepage2').length && sessionStorage.getItem('popupOnce') !== 'true') {
        $('.popuphome').removeClass('hidden');
        sessionStorage.setItem('popupOnce','true');
          
        }
          else{
            $('.popuphome').addClass('hidden');
         
          } 
         
        $('.hidepopuphome').on('click',function(){
        
        $('.popuphome').addClass('hidden');
        });
        $('.hidepopuphome2').on('click',function(){
        
            $('.popuphome').addClass('hidden');
            });
        $('.popuplink').on('click',function(){
             event.preventDefault();
     
               window.location = $(this).attr('href');
        });
    });