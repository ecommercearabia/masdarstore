/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaradaptivesearchsamplescustomaddon.constants;


public final class MasdaradaptivesearchsamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdaradaptivesearchsamplescustomaddon";

	private MasdaradaptivesearchsamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
