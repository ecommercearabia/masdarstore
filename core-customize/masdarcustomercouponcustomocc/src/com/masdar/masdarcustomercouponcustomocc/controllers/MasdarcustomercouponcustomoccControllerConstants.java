/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomocc.controllers;

/**
 */
public interface MasdarcustomercouponcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
