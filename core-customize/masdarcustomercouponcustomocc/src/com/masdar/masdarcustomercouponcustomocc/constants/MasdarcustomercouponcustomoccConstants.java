/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomocc.constants;

/**
 * Masdarcustomercouponcustomocc constants
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class MasdarcustomercouponcustomoccConstants extends GeneratedMasdarcustomercouponcustomoccConstants
{
	public static final String EXTENSIONNAME = "masdarcustomercouponcustomocc"; //NOSONAR

	private MasdarcustomercouponcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
