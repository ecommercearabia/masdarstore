/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcustomercouponcustomocc.jalo;

import com.masdar.masdarcustomercouponcustomocc.constants.MasdarcustomercouponcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarcustomercouponcustomoccManager extends GeneratedMasdarcustomercouponcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarcustomercouponcustomoccManager.class.getName() );
	
	public static final MasdarcustomercouponcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarcustomercouponcustomoccManager) em.getExtension(MasdarcustomercouponcustomoccConstants.EXTENSIONNAME);
	}
	
}
