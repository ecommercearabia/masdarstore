/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommerceorgsamplescustomaddon.constants;

/**
 * Global class for all Masdarcommerceorgsamplescustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarcommerceorgsamplescustomaddonWebConstants // NOSONAR
{
	private MasdarcommerceorgsamplescustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
