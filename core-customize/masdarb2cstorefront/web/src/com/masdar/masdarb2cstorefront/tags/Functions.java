package com.masdar.masdarb2cstorefront.tags;

import de.hybris.platform.consignmenttrackingservices.daos.ConsignmentDao;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;
import de.hybris.platform.warehousing.process.impl.DefaultConsignmentProcessService;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.masdar.core.service.CustomPrintMediaService;
import com.masdar.core.service.impl.DefaultQuoteProcessService;


/**
 * JSP EL Functions. This file contains static methods that are used by JSP EL.
 */
public class Functions
{
	private static final Logger LOG = Logger.getLogger(Functions.class);
	public static final String DEFAULT_HOMEPAGE_URL = "/";

	private static final String PACK_LABEL_DOCUMENT_TEMPLATE = "PackLabelDocumentTemplate";
	private static final String QUOTE_INVOICE_DOCUMENT_TEMPLATE = "QuoteInvoiceDocumentTemplate";

	/**
	 * JSP EL Function to get a primary Image for a Product in a specific format
	 *
	 * @param product
	 *           the product
	 * @param format
	 *           the desired format
	 * @return the image
	 */
	public static String printInvoice(final String orderCode, final String consignmentCode)
	{
		if (StringUtils.isBlank(orderCode) || StringUtils.isBlank(consignmentCode))
		{
			return null;
		}
		final Optional<ConsignmentModel> consignment = getConsignmentDao().findConsignmentByCode(orderCode, consignmentCode);
		if (consignment.isEmpty() || consignment.get().getEInvoicePDF() == null)
		{
			return null;
		}
		return consignment.get().getEInvoicePDF().getURL();
	}

	public static String printQuoteInvoice(final String quoteCode)
	{
		if (StringUtils.isBlank(quoteCode))
		{
			return null;
		}
		final QuoteModel quoteModel = getQuoteProcessService().getQuoteModelForCode(quoteCode);
		if (quoteModel == null)
		{
			return null;
		}

		return getPrintQuoteInvoice(quoteModel);
	}

	/**
	 * @param quoteModel
	 * @return
	 */
	private static String getPrintQuoteInvoice(final QuoteModel quoteModel)
	{
		try
		{
			final MediaModel pickListMedia = getCustomPrintMediaService().getMediaForTemplate(QUOTE_INVOICE_DOCUMENT_TEMPLATE,
					getQuoteProcessService().getQuoteProcess(quoteModel));
			return getCustomPrintMediaService().generateHtmlMediaTemplate(pickListMedia);
		}
		catch (final Exception e)
		{
			LOG.error("getPrintQuoteInvoice Exception:" + e.getMessage());
			return null;
		}
	}

	private static String getPrintInvoice(final ConsignmentModel consignmentModel)
	{
		try
		{
			final MediaModel pickListMedia = getPrintMediaService().getMediaForTemplate(PACK_LABEL_DOCUMENT_TEMPLATE,
					getConsignmentBusinessProcessService().getConsignmentProcess(consignmentModel));
			return getPrintMediaService().generateHtmlMediaTemplate(pickListMedia);
		}
		catch (final Exception e)
		{
			return null;
		}
	}



	protected static CustomPrintMediaService getCustomPrintMediaService()
	{
		return getSpringBean(getCurrentRequest(), "customPrintMediaService", CustomPrintMediaService.class);
	}

	protected static DefaultConsignmentProcessService getConsignmentBusinessProcessService()
	{
		return getSpringBean(getCurrentRequest(), "consignmentBusinessProcessService", DefaultConsignmentProcessService.class);
	}

	protected static PrintMediaService getPrintMediaService()
	{
		return getSpringBean(getCurrentRequest(), "printMediaService", PrintMediaService.class);
	}

	protected static ConsignmentDao getConsignmentDao()
	{
		return getSpringBean(getCurrentRequest(), "consignmentDao", ConsignmentDao.class);
	}

	protected static QuoteService getQuoteService()
	{
		return getSpringBean(getCurrentRequest(), "quoteService", QuoteService.class);
	}

	protected static DefaultQuoteProcessService getQuoteProcessService()
	{
		return getSpringBean(getCurrentRequest(), "quoteProcessService", DefaultQuoteProcessService.class);
	}


	protected static <T> T getSpringBean(final HttpServletRequest httpRequest, final String beanName, final Class<T> beanClass)
	{
		return RequestContextUtils.findWebApplicationContext(httpRequest, httpRequest.getSession().getServletContext())
				.getBean(beanName, beanClass);
	}

	protected static HttpServletRequest getCurrentRequest()
	{
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}
}
