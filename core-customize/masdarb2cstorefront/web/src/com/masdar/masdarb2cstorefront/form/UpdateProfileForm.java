package com.masdar.masdarb2cstorefront.form;

/**
 * Form object for updating profile.
 */
public class UpdateProfileForm
{

	private String titleCode;
	private String firstName;
	private String lastName;
	private String iban;
	private String bankName;
	private String accountNumber;
	private String accountHolderName;

	/** The mobile number. */
	private String mobileNumber;

	/** The mobile country. */
	private String mobileCountry;
	private String nationality;
	private String nationalityId;
	private String birthDate;
	private boolean hasNationalId;
	private boolean involvedInLoyaltyProgram;
	private String maritalStatusCode;

	/**
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}


	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the iban
	 */
	public String getIban()
	{
		return iban;
	}

	/**
	 * @param iban
	 *           the iban to set
	 */
	public void setIban(final String iban)
	{
		this.iban = iban;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName()
	{
		return bankName;
	}

	/**
	 * @param bankName
	 *           the bankName to set
	 */
	public void setBankName(final String bankName)
	{
		this.bankName = bankName;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	 * @param accountNumber
	 *           the accountNumber to set
	 */
	public void setAccountNumber(final String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountHolderName
	 */
	public String getAccountHolderName()
	{
		return accountHolderName;
	}

	/**
	 * @param accountHolderName
	 *           the accountHolderName to set
	 */
	public void setAccountHolderName(final String accountHolderName)
	{
		this.accountHolderName = accountHolderName;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getMobileCountry()
	{
		return mobileCountry;
	}

	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	public String getNationality()
	{
		return nationality;
	}

	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	public String getNationalityId()
	{
		return nationalityId;
	}

	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	public String getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(final String birthDate)
	{
		this.birthDate = birthDate;
	}

	public boolean isHasNationalId()
	{
		return hasNationalId;
	}

	public void setHasNationalId(final boolean hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}

	public boolean isInvolvedInLoyaltyProgram()
	{
		return involvedInLoyaltyProgram;
	}

	public void setInvolvedInLoyaltyProgram(final boolean involvedInLoyaltyProgram)
	{
		this.involvedInLoyaltyProgram = involvedInLoyaltyProgram;
	}

	public String getMaritalStatusCode()
	{
		return maritalStatusCode;
	}

	public void setMaritalStatusCode(final String maritalStatusCode)
	{
		this.maritalStatusCode = maritalStatusCode;
	}



}
