/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.form.validation;


import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.masdar.core.enums.MaritalStatus;
import com.masdar.core.model.NationalityModel;
import com.masdar.core.nationality.service.NationalityService;
import com.masdar.core.service.MobilePhoneService;
import com.masdar.masdarb2cstorefront.form.UpdateProfileForm;


/**
 * Validator for profile forms.
 */
@Component("customProfileValidator")
public class ProfileValidator implements Validator
{

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdateProfileForm profileForm = (UpdateProfileForm) object;
		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();

		final String nationality = profileForm.getNationality();
		final String nationalityID = profileForm.getNationalityId();
		final String birthDate = profileForm.getBirthDate();

		validateUserData(title, firstName, lastName, errors);
		validateMobileAttributes(profileForm, errors);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateNationality(errors, currentSite, nationality);
		if (profileForm.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityID);
		}
		validateBirthDate(errors, currentSite, birthDate);

		if (currentSite != null && currentSite.isCustomerMaritalStatusEnabled() && currentSite.isCustomerMaritalStatusRequired())
		{
			validateMaritalStatus(profileForm.getMaritalStatusCode(), errors);
		}

	}

	private void validateUserData(final String title, final String firstName, final String lastName, final Errors errors)
	{

		if (StringUtils.isBlank(firstName) || StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}
	}

	private void validateMobileAttributes(final UpdateProfileForm registerForm, final Errors errors)
	{
		if (StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(registerForm.getMobileNumber()) || !validateMobileNumber(registerForm.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerForm.getMobileCountry(), registerForm.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				registerForm.setMobileNumber(normalizedPhoneNumber.get());

			}
			else
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			}
		}
	}

	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "profile.nationality.invalid");
			}
		}
	}

	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue("nationalityID", "profile.nationalityid.invalid");
		}

	}

	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue("birthDate", "profile.birthofdate.invalid");
		}

	}

	private void validateMaritalStatus(final String maritalStatusCode, final Errors errors)
	{
		if (StringUtils.isBlank(maritalStatusCode))
		{
			errors.rejectValue("maritalStatusCode", "profile.maritalStatus.empty");
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatusCode);
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("maritalStatusCode", "profile.maritalStatus.invalid");
		}
	}

	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

}
