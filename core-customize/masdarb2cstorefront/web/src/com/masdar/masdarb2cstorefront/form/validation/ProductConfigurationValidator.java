/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.masdarb2cstorefront.form.validation;

import de.hybris.platform.catalog.enums.ConfiguratorType;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.masdar.masdarb2cstorefront.form.ProductConfigurationForm;


@Component("productConfigurationValidator")
public class ProductConfigurationValidator implements Validator
{

	protected static final int MAXIMUM_LENGTH_THRESHOLD = 255;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ProductConfigurationForm.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		final ProductConfigurationForm form = (ProductConfigurationForm) o;
		if (form.getQty() <= 0)
		{
			errors.rejectValue("quantity", "basket.error.quantity.invalid");
		}
		final Map<ConfiguratorType, Map<String, String>> values = form.getConfigurationsKeyValueMap();
		if (values != null)
		{
			values.entrySet().stream().filter(entry -> entry.getKey() == ConfiguratorType.TEXTFIELD)
					.flatMap(entry -> entry.getValue().entrySet().stream()).forEach(property -> {
						if (property.getValue().length() > MAXIMUM_LENGTH_THRESHOLD)
						{
							errors.rejectValue("configurationsKeyValueMap['" + ConfiguratorType.TEXTFIELD.getCode() + "']['"
									+ property.getKey() + "']", "cart.product.configuration.toolong");
						}
					});
		}
	}
}
