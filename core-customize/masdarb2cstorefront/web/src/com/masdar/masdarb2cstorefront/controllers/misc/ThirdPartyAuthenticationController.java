/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.facades.customer.facade.CustomCustomerFacade;
import com.masdar.masdarb2cstorefront.security.ThirdPartyAutoLoginStrategy;
import com.masdar.masdarthirdpartyauthentication.context.ThirdPartyAuthenticationContext;
import com.masdar.masdarthirdpartyauthentication.context.ThirdPartyAuthenticationProviderContext;
import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.masdar.masdarthirdpartyauthentication.entry.TwitterAuthenticatonData;
import com.masdar.masdarthirdpartyauthentication.entry.TwitterCredinatial;
import com.masdar.masdarthirdpartyauthentication.entry.TwitterFormData;
import com.masdar.masdarthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.masdar.masdarthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.masdar.masdarthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.masdar.masdarthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.masdar.masdarthirdpartyauthentication.model.TwitterAuthenticationProviderModel;
import com.masdar.masdarthirdpartyauthentication.service.ThirdPartyUserService;



/**
 *
 *
 */
@Controller
@RequestMapping("/thirdpartyauthentication")
public class ThirdPartyAuthenticationController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(ThirdPartyAuthenticationController.class);
	private static final String LOGIN_REDIRECT = REDIRECT_PREFIX + "/login";
	private static final String THIRD_PARTY_USER = "ThirdPartyUser";
	private static final String TOKEN = "token";
	private static final String TYPE = "type";
	private static final String EXISTS = "exists";
	@Resource(name = "thirdPartyAuthenticationProviderContext")
	private ThirdPartyAuthenticationProviderContext thirdPartyAuthenticationProviderContext;

	@Resource(name = "thirdPartyAuthenticationContext")
	private ThirdPartyAuthenticationContext thirdPartyAuthenticationContext;


	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "thirdPartyAutoLoginStrategy")
	private ThirdPartyAutoLoginStrategy autoLoginStrategy;

	@Resource(name = "thirdPartyUserService")
	private ThirdPartyUserService thirdPartyUserService;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@RequestMapping(value = "/getuserdata", method = RequestMethod.GET)
	public String getuserdata(final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes, final Model model, @RequestParam(name = "data", required = true)
			final String data, @RequestParam(name = "type", required = true)
			final String type) throws IOException
	{
		ThirdPartyAuthenticationType providerType = null;
		try
		{
			providerType = ThirdPartyAuthenticationType.valueOf(type);
		}
		catch (final IllegalArgumentException ex)
		{
			LOG.error(ex.getMessage());
			return LOGIN_REDIRECT;
		}

		Optional<ThirdPartyAuthenticationUserData> userDataByCurrentStore = Optional.empty();
		try
		{
			userDataByCurrentStore = thirdPartyAuthenticationContext.getThirdPartyUserDataByCurrentStore(data, providerType);
		}
		catch (final ThirdPartyAuthenticationException e)
		{
			LOG.error(e.getMessage());
			return LOGIN_REDIRECT;
		}

		if (userDataByCurrentStore.isEmpty())
		{
			LOG.error("User is not present!");
			return LOGIN_REDIRECT;
		}
		String redirectUrl = REDIRECT_PREFIX + "/third-party/update-profile";

		final ThirdPartyAuthenticationUserData user = userDataByCurrentStore.get();
		final boolean userExisting = thirdPartyUserService.isUserExist(user.getId());

		CustomerData customer = null;
		if (userExisting)
		{
			customer = customerFacade.getCustomerByCustomerId(user.getId());
			if (checkCustomerAttributes(customer))
			{
				redirectUrl = REDIRECT_PREFIX + "/";

				final boolean login = autoLoginStrategy.login(customer.getUid(), data, type, request, response);
				if (login)
				{
					LOG.info(customer.getUid() + " is authenticated successfully!");
					return redirectUrl;
				}
				else
				{
					LOG.info(user.getEmail() + " is not authenticated successfully!");
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"text.account.profile.not.confirmationUpdated", null);
				}
			}
			sessionService.setAttribute(EXISTS, true);
		}
		sessionService.setAttribute(THIRD_PARTY_USER, user);
		sessionService.setAttribute(TOKEN, data);
		sessionService.setAttribute(TYPE, type);
		sessionService.setAttribute(EXISTS, false);
		return redirectUrl;
	}

	@RequestMapping(value = "/twitter")
	public void twitterRedirect(final HttpServletRequest request, final HttpServletResponse response)
			throws ThirdPartyAuthenticationException
	{

		final Optional<ThirdPartyAuthenticationProviderModel> twitterProviderModel = thirdPartyAuthenticationProviderContext
				.getThirdPartyAuthenticationProviderByCurrentSite(TwitterAuthenticationProviderModel.class);

		if (twitterProviderModel.isEmpty())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND,
					ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND.getMsg());
		}

		final String consumerKey = ((TwitterAuthenticationProviderModel) twitterProviderModel.get()).getConsumerKey();
		final String consumerSecret = ((TwitterAuthenticationProviderModel) twitterProviderModel.get()).getConsumerSecret();
		final String callbackUrl = request.getRequestURL().toString();
		final TwitterCredinatial twitterCredinatial = new TwitterCredinatial(consumerKey, consumerSecret);
		final Optional<TwitterFormData> twitterForm = thirdPartyAuthenticationContext
				.getThirdPartyFormDataByCurrentStore(twitterCredinatial, ThirdPartyAuthenticationType.TWITTER, callbackUrl);
		if (!twitterForm.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.CALLBACK_NOT_AUTHPRIZED,
					ThirdPartyAuthenticationExceptionType.CALLBACK_NOT_AUTHPRIZED.getMsg());
		}

		final String formUrl = twitterForm.get().getSrc();

		sessionService.getCurrentSession().setAttribute("twitterOuathToken", twitterForm.get().getOuathToken());
		sessionService.getCurrentSession().setAttribute("twitterOuathTokenSecret", twitterForm.get().getOuathTokenSecret());

		try
		{
			response.sendRedirect(formUrl);
		}
		catch (final IOException e)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.REDIRECT_ERROR,
					ThirdPartyAuthenticationExceptionType.REDIRECT_ERROR.getMsg());

		}
	}

	@RequestMapping(value = "/twitter/getuserdata", method = RequestMethod.GET)
	public String getTwitteruserdata(final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes) throws ThirdPartyAuthenticationException
	{
		final Optional<ThirdPartyAuthenticationProviderModel> twitterProviderModel = thirdPartyAuthenticationProviderContext
				.getThirdPartyAuthenticationProviderByCurrentSite(TwitterAuthenticationProviderModel.class);

		if (twitterProviderModel.isEmpty())
		{

			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND,
					ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND.getMsg());

		}

		final String consumerKey = ((TwitterAuthenticationProviderModel) twitterProviderModel.get()).getConsumerKey();
		final String consumerSecret = ((TwitterAuthenticationProviderModel) twitterProviderModel.get()).getConsumerSecret();
		final String ouathToken = (String) sessionService.getAttribute("twitterOuathToken");
		sessionService.removeAttribute("twitterOuathToken");
		final String ouathTokenSecret = (String) sessionService.getAttribute("twitterOuathTokenSecret");
		sessionService.removeAttribute("twitterOuathTokenSecret");
		final String oauthVerifier = request.getParameter("oauth_verifier");
		final TwitterAuthenticatonData twitterCredinatial = new TwitterAuthenticatonData(consumerKey, consumerSecret, ouathToken,
				ouathTokenSecret, oauthVerifier);


		final Optional<ThirdPartyAuthenticationUserData> thirdPartyUserDataByCurrentStore = thirdPartyAuthenticationContext
				.getThirdPartyUserDataByCurrentStore(twitterCredinatial, ThirdPartyAuthenticationType.TWITTER);

		if (thirdPartyUserDataByCurrentStore.isEmpty())
		{
			LOG.error("User is not present!");
			return LOGIN_REDIRECT;
		}
		String redirectUrl = REDIRECT_PREFIX + "/third-party/update-profile";

		final ThirdPartyAuthenticationUserData user = thirdPartyUserDataByCurrentStore.get();

		final boolean userExisting = thirdPartyUserService.isUserExist(user.getId());

		CustomerData customer = null;
		if (userExisting)
		{
			customer = customerFacade.getCustomerByCustomerId(user.getId());
			if (checkCustomerAttributes(customer))
			{
				redirectUrl = REDIRECT_PREFIX + "/";

				autoLoginStrategy.login(customer.getUid(), ouathToken, "TWITTER", request, response);
				LOG.info(customer.getUid() + " is authenticated successfully!");
				return redirectUrl;
			}
			sessionService.setAttribute(EXISTS, true);
		}
		sessionService.setAttribute(THIRD_PARTY_USER, user);
		sessionService.setAttribute(TOKEN, ouathToken);
		sessionService.setAttribute(TYPE, "TWITTER");
		sessionService.setAttribute(EXISTS, false);
		return REDIRECT_PREFIX + "/third-party/update-profile";
	}

	private boolean checkCustomerAttributes(final CustomerData customer)
	{
		if (StringUtils.isNotBlank(customer.getUid()))
		{
			final String email = customer.getUid().split("\\|")[0];
			return !StringUtils.isBlank(email);
		}
		if (StringUtils.isBlank(customer.getFirstName()))
		{
			return false;
		}
		if (StringUtils.isBlank(customer.getLastName()))
		{
			return false;
		}
		if (customer.getMobileCountry() == null)
		{
			return false;
		}
		if (StringUtils.isBlank(customer.getMobileNumber()))
		{
			return false;
		}

		return true;
	}

	@RequestMapping(value = "/apple")
	public String getUserDataFromApple(final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes)
	{
		LOG.info("apple redirect url controller post method");
		final ThirdPartyAuthenticationType type = ThirdPartyAuthenticationType.APPLE;
		final String authorizationCode = request.getParameter("code");
		Optional<ThirdPartyAuthenticationUserData> userDataByCurrentStore = Optional.empty();

		try
		{
			userDataByCurrentStore = thirdPartyAuthenticationContext.getThirdPartyUserDataByCurrentStore(authorizationCode, type);
		}
		catch (final ThirdPartyAuthenticationException e)
		{
			LOG.error(e.getMessage());
			return LOGIN_REDIRECT;
		}
		if (userDataByCurrentStore.isEmpty())
		{
			LOG.error("User is not present!");
			return LOGIN_REDIRECT;
		}
		LOG.info("user data returned successfully!");
		String redirectUrl = REDIRECT_PREFIX + "/third-party/update-profile";

		final ThirdPartyAuthenticationUserData user = userDataByCurrentStore.get();
		final boolean userExisting = thirdPartyUserService.isUserExist(user.getId());

		CustomerData customer = null;
		if (userExisting)
		{
			LOG.info("user does exist");
			customer = customerFacade.getCustomerByCustomerId(user.getId());
			if (checkCustomerAttributes(customer))
			{
				redirectUrl = REDIRECT_PREFIX + "/";

				final boolean login = autoLoginStrategy.login(customer.getUid(), authorizationCode, type.toString(), request,
						response);
				if (login)
				{
					LOG.info(customer.getUid() + " is authenticated successfully!");
					return redirectUrl;
				}
				else
				{
					LOG.info(user.getEmail() + " is not authenticated successfully!");
					GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"text.account.profile.not.confirmationUpdated", null);
				}
			}
			sessionService.setAttribute(EXISTS, true);
		}
		sessionService.setAttribute(THIRD_PARTY_USER, user);
		sessionService.setAttribute(TOKEN, authorizationCode);
		sessionService.setAttribute(TYPE, type.toString());
		sessionService.setAttribute(EXISTS, false);
		return redirectUrl;
	}

}
