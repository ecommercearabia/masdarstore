/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.facades.facade.CustomCheckoutFlowFacade;
import com.masdar.masdarb2cstorefront.checkout.steps.CheckoutStep;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;
import com.masdar.masdarpayment.entry.PaymentRequestData;
import com.masdar.masdarpayment.entry.PaymentResponseData;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOGGER = Logger.getLogger(SummaryCheckoutStepController.class);

	private static final String SUMMARY = "summary";

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "calculationService")
	private CalculationService calculationService;


	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	//	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, // NOSONAR
			CommerceCartModificationException
	{
		if (validateCart(redirectAttributes))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode, Arrays.asList(
						ProductOption.BASIC, ProductOption.PRICE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.PRICE_RANGE));
				entry.setProduct(product);
			}
		}


		if ("card".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode())
				|| "mada".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode()))
		{
			final Pair<Optional<PaymentRequestData>, Boolean> supportedPaymentData = getCheckoutFacade().getSupportedPaymentData();

			if (Boolean.TRUE.equals(supportedPaymentData.getRight()) && cartService.getSessionCart() != null)
			{
				try
				{
					return completeOrder(cartService.getSessionCart().getHyperpayCheckoutMap());
				}
				catch (final Exception e)
				{
					LOGGER.error("Failed to place Order", e);

				}

			}

			if (!supportedPaymentData.getLeft().isPresent())
			{
				return back(redirectAttributes);
			}
			model.addAttribute("supportedPaymentData", supportedPaymentData.getLeft().get());

		}


		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());

		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption());
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());

		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);

		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}



	@RequestMapping(value = "/placeOrder") //NOSONAR
	@PreValidateQuoteCheckoutStep
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm")
	final PlaceOrderForm placeOrderForm, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel, final BindingResult bindingResult) throws CMSItemNotFoundException, // NOSONAR
			InvalidCartException, CommerceCartModificationException
	{
		if (validateOrderForm(placeOrderForm, model, bindingResult))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}
		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final Exception e)
		{
			LOGGER.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return enterStep(model, redirectModel);
		}

		return redirectToOrderConfirmationPage(orderData);
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @param bindingResult
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model, final BindingResult bindingResult)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}

		if (getCustomCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			bindingResult.rejectValue("termsCheck", "checkout.error.terms.not.accepted");
			model.addAttribute("termsCheckInvalid", true);
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOGGER.error(String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			invalid = true;
		}

		if (!cartData.isCalculated())
		{
			LOGGER.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOGGER.error(String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			invalid = true;
		}

		if (!cartData.isCalculated())
		{
			LOGGER.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}



	@RequestMapping(value = "/callback", method = RequestMethod.POST)
	@RequireHardLogIn
	public String callback(final HttpServletRequest httpServletRequest, @RequestParam(value = "encResp", required = false)
	final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCallback(httpServletRequest);
	}

	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCancelCallback(final Model model, final HttpServletRequest httpServletRequest,
			@RequestParam(value = "encResp", required = false)
			final String encResp) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return resolveCallback(httpServletRequest);
	}

	@RequestMapping(value = "/callback/cancel", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCancelCallback(final Model model, final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/callback/cancel", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelCallback(final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	private String resolveCallback(final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{


		//TODO check User
		final OrderData orderData = null;
		try
		{
			//	final Optional<PaymentProviderModel> supportedPaymentProvider = getCheckoutFacade().getSupportedPaymentProvider();
			final Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();

			String[] encResp = null;

			encResp = parameterMap.get("id");


			final String data = Arrays.stream(encResp).collect(Collectors.joining());

			final Optional<PaymentResponseData> paymentResponseData = getCheckoutFacade().getOrderPaymentResponseDataByData(data);
			setPaymentResponseBody(paymentResponseData.get().getResponseData());

			if (!paymentResponseData.isPresent() || paymentResponseData.get().getResponseData() == null
					|| !(paymentResponseData.get().getResponseData() instanceof Map))
			{
				return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=0000";
			}

			final Map<String, Object> orderInfoMap = paymentResponseData.get().getResponseData();

			if (!getCheckoutFacade().isSuccessfulPaidOrderByOrder())
			{
				LOGGER.warn("NOT_SUCCESSFUL_PAYMENT: " + orderInfoMap);
				if (orderInfoMap.get("result") != null && orderInfoMap.get("result") instanceof Map
						&& ((Map<String, Object>) orderInfoMap.get("result")).get("code") != null)
				{
					return REDIRECT_URL_ERROR + "/?decision=1&reasonCode="
							+ ((Map<String, Object>) orderInfoMap.get("result")).get("code");
				}
				return REDIRECT_URL_ERROR + "/?decision=0&reasonCode=0000";
			}

			return completeOrder(orderInfoMap);

		}
		catch (final Exception e)
		{
			LOGGER.error("Failed to place Order", e);
			//			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return getCheckoutStep().currentStep();
		}



	}

	private void setPaymentResponseBody(final Map<String, Object> orderBody)
	{
		//				final CartModel sessionCart = cartService.getSessionCart();
		//				sessionCart.setResponsePaymentBody(orderBody == null ? "null" : orderBody.toString());
		//				modelService.save(sessionCart);
	}

	protected String completeOrder(final Map<String, Object> orderInfoMap) throws InvalidCartException
	{
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true);


		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.get().getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			LOGGER.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + "/?decision=" + paymentSubscriptionResultData.get().getDecision() + "&reasonCode="
					+ paymentSubscriptionResultData.get().getResultCode();
		}

		final OrderData orderData = getCheckoutFacade().placeOrder();
		return redirectToOrderConfirmationPage(orderData);


	}

	@RequestMapping(value = "/callback/mpgs/cancel", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMpgsCancelCallback(final Model model, final HttpServletRequest httpServletRequest)
			throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}

}
