/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.misc;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.masdar.core.enums.ShipmentType;
import com.masdar.facades.facade.CustomCartFacade;
import com.masdar.facades.shipment.modification.ShipmentTypeModificationData;
import com.masdar.masdarfacades.data.MetaData;
import com.masdar.masdarfacades.data.ResponseData;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping("/misc")
public class PreChangeCartShipmentTypeController
{
	private static final Logger LOG = Logger.getLogger(PreChangeCartShipmentTypeController.class);

	@Resource(name = "cartFacade")
	private CustomCartFacade cartFacade;

	@GetMapping("/cart/shipment-type/prechange")
	@ResponseBody
	public ResponseData preChangeCurrentCartShipmentType(@RequestParam(name = "shipmentTypeCode", required = true)
	final String shipmentTypeCode, @RequestParam(name = "cityCode", required = false)
	final String cityCode, @RequestParam(name = "posCode", required = false)
	final String posCode)
	{
		final ResponseData response = new ResponseData();
		final MetaData meta = new MetaData();
		if (!getCartFacade().hasSessionCart())
		{
			meta.setMessage("No cart was found!");
			meta.setStatusCode(HttpStatus.OK.value());
			response.setMeta(meta);
			response.setData(null);
			return response;
		}
		final ShipmentTypeModificationData preChangeShipmentType = getCartFacade()
				.preChangeShipmentType(getShipmentType(shipmentTypeCode), cityCode, posCode);
		meta.setMessage("Got the entry modifications for shipment type change");
		meta.setStatusCode(HttpStatus.OK.value());
		response.setMeta(meta);
		response.setData(preChangeShipmentType);
		return response;
	}

	private ShipmentType getShipmentType(final String code)
	{
		try
		{
			return ShipmentType.valueOf(code);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

	/**
	 * @return the cartFacade
	 */
	public CustomCartFacade getCartFacade()
	{
		return cartFacade;
	}

}
