/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.cms;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.city.facade.CityFacade;
import com.masdar.facades.shipmenttype.data.ShipmentTypeInfoData;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;
import com.masdar.masdarcomponents.model.CustomerLocationCitiesComponentModel;


/**
 * @author tuqa-pc
 *
 */
@Controller("CustomerLocationCitiesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CustomerLocationCitiesComponent)
public class CustomerLocationCitiesComponentController
		extends AbstractAcceleratorCMSComponentController<CustomerLocationCitiesComponentModel>
{

	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE);

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "customUserService")
	private CustomUserService userService;

	@Resource(name = "shipmentTypeInfoConverter")
	private Converter<ShipmentTypeInfo, ShipmentTypeInfoData> shipmentTypeInfoConverter;


	/**
	 * @return the userService
	 */
	protected CustomUserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the cityFacade
	 */
	public CityFacade getCityFacade()
	{
		return cityFacade;
	}


	/**
	 * @return the shipmentTypeInfoConverter
	 */
	protected Converter<ShipmentTypeInfo, ShipmentTypeInfoData> getShipmentTypeInfoConverter()
	{
		return shipmentTypeInfoConverter;
	}

	/**
	 * Fill model.
	 *
	 * @param request
	 *           the request
	 * @param model
	 *           the model
	 * @param component
	 *           the component
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final CustomerLocationCitiesComponentModel component)
	{

		model.addAttribute("title", component.getTitle());
		model.addAttribute("content", component.getContent());
		model.addAttribute("customerLocationCities", getCityFacade().getWarehousesCitiesByCurrentBaseStore());
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getUserService().getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isPresent() && Objects.nonNull(shipmentTypeInfoForCurrentUser.get().getCity()))
		{
			final ShipmentTypeInfoData convert = getShipmentTypeInfoConverter().convert(shipmentTypeInfoForCurrentUser.get());
			model.addAttribute("selectedCustomerLocationCity", Objects.nonNull(convert) ? convert.getSelectedCity() : null);
		}
	}


}
