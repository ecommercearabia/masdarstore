/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.masdarb2cstorefront.checkout.steps.CheckoutStep;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;
import com.masdar.masdarb2cstorefront.form.TimeSlotForm;
import com.masdar.masdarb2cstorefront.form.validation.TimeSlotValidator;
import com.masdar.masdartimeslotfacades.TimeSlotData;
import com.masdar.masdartimeslotfacades.TimeSlotInfoData;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;
import com.masdar.masdartimeslotfacades.facade.TimeSlotFacade;




@Controller
@RequestMapping(value = "/checkout/multi/time-slot")
public class TimeSlotCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(TimeSlotCheckoutStepController.class);
	private static final String TIME_SLOT = "time-slot";

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotValidator")
	private TimeSlotValidator timeSlotValidator;

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = TIME_SLOT)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, getCheckoutFacade().getCheckoutCart(), new TimeSlotForm());
		Optional<TimeSlotData> timeSlot = null;
		try
		{
			timeSlot = timeSlotFacade.getTimeSlotData(cartData.getDeliveryMode().getCode());
		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
			return next(redirectAttributes);
		}
		if (!timeSlot.isPresent())
		{
			return next(redirectAttributes);
		}

		model.addAttribute("timeSlot", timeSlot.get());

		return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseTimeSlotPage;
	}

	/**
	 * This method gets called when the "Use Selected Time Slot" button is clicked. It sets the selected time slot on the
	 * checkout facade and reloads the page highlighting the selected time slot.
	 *
	 * @return - a URL to the page to load.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectTimeSlot(@Valid
	final TimeSlotForm timeSlotForm, final Model model, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		timeSlotValidator.validate(timeSlotForm, bindingResult);

		populateCommonModelAttributes(model, getCheckoutFacade().getCheckoutCart(), timeSlotForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.timeslot.entry.invalid");
			Optional<TimeSlotData> timeSlot = null;
			try
			{
				timeSlot = timeSlotFacade.getTimeSlotData(getCheckoutFacade().getCheckoutCart().getDeliveryMode().getCode());
			}
			catch (final TimeSlotException e)
			{
				LOG.error(e.getMessage(), e);
				return next(redirectModel);
			}
			if (!timeSlot.isPresent())
			{
				return next(redirectModel);
			}

			model.addAttribute("timeSlot", timeSlot.get());

			return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseTimeSlotPage;
		}
		final TimeSlotInfoData timeSlotInfoData = convertForm(timeSlotForm);

		getCheckoutFacade().setTimeSlot(timeSlotInfoData);

		return getCheckoutStep().nextStep();
	}

	protected void populateCommonModelAttributes(final Model model, final CartData cartData, final TimeSlotForm timeSlotForm)
			throws CMSItemNotFoundException
	{
		model.addAttribute("cartData", cartData);
		model.addAttribute("timeSlotForm", timeSlotForm);
		model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));

		prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.timeSlot.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(TIME_SLOT);
	}

	private TimeSlotInfoData convertForm(final TimeSlotForm timeSlotForm)
	{
		final TimeSlotInfoData data = new TimeSlotInfoData();
		final String deliveryModeCode = getCheckoutFacade().getCheckoutCart().getDeliveryMode().getCode();
		data.setPeriodCode(timeSlotForm.getPeriodCode());
		data.setStart(LocalTime.parse(timeSlotForm.getStart(), DateTimeFormatter.ofPattern("H:mm")));
		data.setEnd(LocalTime.parse(timeSlotForm.getEnd(), DateTimeFormatter.ofPattern("H:mm")));
		data.setDay(timeSlotForm.getDay());
		data.setDate(timeSlotForm.getDate());
		return data;
	}
	/**
	 * @return the timeSlotFacade
	 */
	public TimeSlotFacade getTimeSlotFacade()
	{
		return timeSlotFacade;
	}

	/**
	 * @param timeSlotFacade
	 *           the timeSlotFacade to set
	 */
	public void setTimeSlotFacade(final TimeSlotFacade timeSlotFacade)
	{
		this.timeSlotFacade = timeSlotFacade;
	}

	/**
	 * @return the timeSlotValidator
	 */
	public TimeSlotValidator getTimeSlotValidator()
	{
		return timeSlotValidator;
	}

	/**
	 * @param timeSlotValidator
	 *           the timeSlotValidator to set
	 */
	public void setTimeSlotValidator(final TimeSlotValidator timeSlotValidator)
	{
		this.timeSlotValidator = timeSlotValidator;
	}

}
