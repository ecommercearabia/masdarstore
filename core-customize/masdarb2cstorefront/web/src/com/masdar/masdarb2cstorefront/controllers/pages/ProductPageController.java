/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.futurestock.FutureStockFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.FutureStockForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.FutureStockData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.MetadataDescriptionProductOption;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.city.facade.CityFacade;
import com.masdar.facades.facade.CustomCartFacade;
import com.masdar.facades.shipmenttype.data.ShipmentTypeInfoData;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;


/**
 * Controller for product details page
 */
@Controller
@RequestMapping(value = "/**/p")
public class ProductPageController extends AbstractPageController
{

	public static final int MAX_PAGE_LIMIT = 100; // should be configured

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains one or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";

	private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
	private static final String STOCK_SERVICE_UNAVAILABLE = "basket.page.viewFuture.unavailable";
	private static final String NOT_MULTISKU_ITEM_ERROR = "basket.page.viewFuture.not.multisku";

	@Resource(name = "productDataUrlResolver")
	private UrlResolver<ProductData> productDataUrlResolver;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "futureStockFacade")
	private FutureStockFacade futureStockFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "scpiService")
	private SCPIService scpiService;

	@Resource(name = "shipmentTypeInfoConverter")
	private Converter<ShipmentTypeInfo, ShipmentTypeInfoData> shipmentTypeInfoConverter;

	@Resource(name = "cartFacade")
	private CustomCartFacade cartFacade;

	@Resource(name = "commerceStockService")
	private CommerceStockService commerceStockService;

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode")
	final String encodedProductCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);

		updateInventoryForProduct(productCode);

		final List<ProductOption> extraOptions = new LinkedList<>();
		extraOptions.add(ProductOption.VARIANT_MATRIX_BASE);
		extraOptions.add(ProductOption.VARIANT_MATRIX_URL);
		extraOptions.add(ProductOption.VARIANT_MATRIX_MEDIA);

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite.isDisplayProductMetadataDescription())
		{
			extraOptions.add(ProductOption.DESCRIPTION);
			extraOptions.add(ProductOption.SUMMARY);
			extraOptions.add(ProductOption.KEYWORDS);
		}
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);


		final ProductModel productModel = getProductService().getProductForCode(productCode);

		if (productModel != null && !isActiveOnB2C(productModel))
		{
			throw new UnknownIdentifierException("Product [" + productModel.getCode() + "] is not active on B2C");
		}

		final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		updatePageTitle(productCode, model);


		populateProductDetailForDisplay(productCode, model, request, extraOptions);

		model.addAttribute("isAvailablePOS", isAvailablePOSStock(productModel));

		populateDeliveryShippment(model);

		model.addAttribute(new ReviewForm());
		model.addAttribute("pageType", PageType.PRODUCT.name());
		model.addAttribute("futureStockEnabled", Boolean.valueOf(Config.getBoolean(FUTURE_STOCK_ENABLED, false)));
		final List<MetadataDescriptionProductOption> metadataProductOptions = currentSite.getMetadataDescriptionProductOptions();
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = getMetaDescription(productData, metadataProductOptions, metaKeywords);
		setUpMetaData(model, metaKeywords, metaDescription);
		return getViewForPage(model);
	}


	boolean isAvailablePOSStock(final ProductModel productModel)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isPresent() && shipmentTypeInfoForCurrentUser.get().getPointOfService() != null
				&& StringUtils.isNotBlank(shipmentTypeInfoForCurrentUser.get().getPointOfService().getName()))
		{
			final StockLevelStatus stockLevelStatus = commerceStockService.getStockLevelStatusForProductAndPointOfService(
					productModel, shipmentTypeInfoForCurrentUser.get().getPointOfService());

			if (stockLevelStatus != null
					&& (StockLevelStatus.INSTOCK.equals(stockLevelStatus) || StockLevelStatus.LOWSTOCK.equals(stockLevelStatus)))
			{
				return true;
			}
		}

		return false;
	}


	/**
	 * @param productCode
	 */
	private void updateInventoryForProduct(final String productCode)
	{
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		final ProductModel productModel = getProductService().getProductForCode(productCode);

		if (!baseStore.isScpiEnableUpdateInventoryOnPDP())
		{
			return;
		}

		try
		{
			getScpiService().updateInventoryForProduct(productModel, baseStore);
		}
		catch (final SCPIException e)
		{
			LOG.error(String.format("Could not update inventory for product [%s]: \n%s", productCode, e.getMessage()));
		}
	}

	/**
	 * @param model
	 */
	private void populateDeliveryShippment(final Model model)
	{

		model.addAttribute("customerLocationCities", getCityFacade().getWarehousesCitiesByCurrentBaseStore());
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isPresent() && Objects.nonNull(shipmentTypeInfoForCurrentUser.get().getCity()))
		{
			final ShipmentTypeInfoData convert = getShipmentTypeInfoConverter().convert(shipmentTypeInfoForCurrentUser.get());
			model.addAttribute("selectedCustomerLocationCity", Objects.nonNull(convert) ? convert.getSelectedCity() : null);
		}


		final CartData checkoutCart = getCheckoutFacade().getCheckoutCart();
		if (Objects.isNull(checkoutCart))
		{
			return;
		}

		final DeliveryModeData deliveryMode = checkoutCart.getDeliveryMode();
		model.addAttribute("deliveryMode", deliveryMode);



	}

	/**
	 * @param productData
	 * @param metadataProductOptions
	 * @param metaKeywords
	 * @return
	 */
	private String getMetaDescription(final ProductData productData,
			final List<MetadataDescriptionProductOption> metadataProductOptions, final String metaKeywords)
	{
		final List<String> data = new LinkedList<>();


		if (metadataProductOptions.contains(MetadataDescriptionProductOption.DESCRIPTION)
				&& StringUtils.isNotBlank(productData.getDescription()))
		{
			data.add(MetaSanitizerUtil.sanitizeDescription(productData.getDescription()));
		}
		if (metadataProductOptions.contains(MetadataDescriptionProductOption.KEYWORD) && StringUtils.isNotBlank(metaKeywords))
		{
			data.add(metaKeywords);
		}
		if (metadataProductOptions.contains(MetadataDescriptionProductOption.SUMMARY)
				&& StringUtils.isNotBlank(productData.getSummary()))
		{
			data.add(MetaSanitizerUtil.sanitizeDescription(productData.getSummary()));
		}
		return String.join(" | ", data);

	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/orderForm", method = RequestMethod.GET)
	public String productOrderForm(@PathVariable("productCode")
	final String encodedProductCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE,
				ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.VARIANT_MATRIX_STOCK,
				ProductOption.URL);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);
		updatePageTitle(productCode, model);

		populateProductDetailForDisplay(productCode, model, request, extraOptions);

		if (!model.containsAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT))
		{
			return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
		}

		return ControllerConstants.Views.Pages.Product.OrderForm;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages", method = RequestMethod.GET)
	public String showZoomImages(@PathVariable("productCode")
	final String encodedProductCode, @RequestParam(value = "galleryPosition", required = false)
	final String galleryPosition, final Model model)
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Collections.singleton(ProductOption.GALLERY));
		final List<Map<String, ImageData>> images = getGalleryImages(productData);
		populateProductData(productData, model);
		if (galleryPosition != null)
		{
			try
			{
				model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
			}
			catch (final IndexOutOfBoundsException | NumberFormatException ex)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(ex);
				}
				model.addAttribute("zoomImageUrl", "");
			}
		}
		return ControllerConstants.Views.Fragments.Product.ZoomImagesPopup;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
	public String showQuickView(@PathVariable("productCode")
	final String encodedProductCode, final Model model, final HttpServletRequest request)
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.STOCK, ProductOption.REVIEW,
						ProductOption.VARIANT_FULL, ProductOption.DELIVERY_MODE_AVAILABILITY));

		sortVariantOptionData(productData);
		populateProductData(productData, model);
		getRequestContextData(request).setProduct(productModel);

		return ControllerConstants.Views.Fragments.Product.QuickViewPopup;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/review", method =
	{ RequestMethod.GET, RequestMethod.POST }) //NOSONAR
	public String postReview(@PathVariable("productCode")
	final String encodedProductCode, final ReviewForm form, final BindingResult result, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectAttrs) throws CMSItemNotFoundException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		getReviewValidator().validate(form, result);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);
		if (result.hasErrors())
		{
			updatePageTitle(productCode, model);
			GlobalMessages.addErrorMessage(model, "review.general.error");
			model.addAttribute("showReviewForm", Boolean.TRUE);
			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			storeCmsPageInModel(model, getPageForProduct(productCode));
			return getViewForPage(model);
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/reviewhtml/"
			+ REVIEWS_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewHtml(@PathVariable("productCode")
	final String encodedProductCode, @PathVariable("numberOfReviews")
	final String numberOfReviews, final Model model, final HttpServletRequest request)
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final List<ReviewData> reviews;
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.REVIEW));

		if ("all".equals(numberOfReviews))
		{
			reviews = productFacade.getReviews(productCode);
		}
		else
		{
			final int reviewCount = Math.min(Integer.parseInt(numberOfReviews),
					productData.getNumberOfReviews() == null ? 0 : productData.getNumberOfReviews().intValue());
			reviews = productFacade.getReviews(productCode, Integer.valueOf(reviewCount));
		}

		getRequestContextData(request).setProduct(productModel);
		model.addAttribute("reviews", reviews);
		model.addAttribute("reviewsTotal", productData.getNumberOfReviews());
		model.addAttribute(new ReviewForm());

		return ControllerConstants.Views.Fragments.Product.ReviewsTab;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.GET)
	public String writeReview(@PathVariable("productCode")
	final String encodedProductCode, final Model model) throws CMSItemNotFoundException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		model.addAttribute(new ReviewForm());
		setUpReviewPage(model, productCode);
		return ControllerConstants.Views.Pages.Product.WriteReview;
	}

	protected void setUpReviewPage(final Model model, final String productCode) throws CMSItemNotFoundException
	{
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productCode));
		model.addAttribute("product", productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC)));
		updatePageTitle(productCode, model);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.POST)
	public String writeReview(@PathVariable("productCode")
	final String encodedProductCode, final ReviewForm form, final BindingResult result, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectAttrs) throws CMSItemNotFoundException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		getReviewValidator().validate(form, result);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);

		if (result.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "review.general.error");
			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			setUpReviewPage(model, productCode);
			return ControllerConstants.Views.Pages.Product.WriteReview;
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/futureStock", method = RequestMethod.GET)
	public String productFutureStock(@PathVariable("productCode")
	final String encodedProductCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
		final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);
		if (futureStockEnabled)
		{
			final List<FutureStockData> futureStockList = futureStockFacade.getFutureAvailability(productCode);
			if (futureStockList == null)
			{
				GlobalMessages.addErrorMessage(model, STOCK_SERVICE_UNAVAILABLE);
			}
			else if (futureStockList.isEmpty())
			{
				GlobalMessages.addInfoMessage(model, "product.product.details.future.nostock");
			}

			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			model.addAttribute("futureStocks", futureStockList);

			return ControllerConstants.Views.Fragments.Product.FutureStockPopup;
		}
		else
		{
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}
	}

	@ResponseBody
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/grid/skusFutureStock", method =
	{ RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public final Map<String, Object> productSkusFutureStock(final FutureStockForm form, final Model model)
	{
		final String productCode = form.getProductCode();
		final List<String> skus = form.getSkus();
		final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);

		Map<String, Object> result = new HashMap<>();
		if (futureStockEnabled && CollectionUtils.isNotEmpty(skus) && StringUtils.isNotBlank(productCode))
		{
			final Map<String, List<FutureStockData>> futureStockData = futureStockFacade
					.getFutureAvailabilityForSelectedVariants(productCode, skus);

			if (futureStockData == null)
			{
				// future availability service is down, we show this to the user
				result = Maps.newHashMap();
				final String errorMessage = getMessageSource().getMessage(NOT_MULTISKU_ITEM_ERROR, null,
						getI18nService().getCurrentLocale());
				result.put(NOT_MULTISKU_ITEM_ERROR, errorMessage);
			}
			else
			{
				for (final Map.Entry<String, List<FutureStockData>> entry : futureStockData.entrySet())
				{
					result.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return result;
	}

	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	protected void updatePageTitle(final String productCode, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productCode));
	}

	protected void populateProductDetailForDisplay(final String productCode, final Model model, final HttpServletRequest request,
			final List<ProductOption> extraOptions) throws CMSItemNotFoundException
	{
		ProductModel productModel = getProductService().getProductForCode(productCode);

		getRequestContextData(request).setProduct(productModel);

		final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.VARIANT_FIRST_VARIANT, ProductOption.BASIC,
				ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
				ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
				ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES, ProductOption.PRICE_RANGE,
				ProductOption.DELIVERY_MODE_AVAILABILITY));

		options.addAll(extraOptions);

		productModel = getProductDisplay(productModel);
		model.addAttribute("configurations", productFacade.getConfiguratorSettingsForCode(productModel.getCode()));

		final ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(), options);

		sortVariantOptionData(productData);
		storeCmsPageInModel(model, getPageForProduct(productModel.getCode()));
		populateProductData(productData, model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productModel.getCode()));

		if (CollectionUtils.isNotEmpty(productData.getVariantMatrix()))
		{
			model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT,
					Boolean.valueOf(CollectionUtils.isNotEmpty(productData.getVariantMatrix())));
		}
	}

	protected ProductModel getProductDisplay(final ProductModel productModel)
	{
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			return productModel;
		}
		final List<ProductModel> variantsProducts = getVariantsProducts(productModel);
		for (final ProductModel product : variantsProducts)
		{
			if (product.isActiveOnB2C())
			{
				return product;
			}
		}
		return productModel;
	}

	protected ProductModel getProductVariant(final ProductModel productModel, final List<ProductOption> options)
	{
		if (CollectionUtils.isNotEmpty(options) && options.contains(ProductOption.VARIANT_FIRST_VARIANT)
				&& CollectionUtils.isNotEmpty(productModel.getVariants()))
		{
			return getProductVariant(Iterables.get(productModel.getVariants(), 0), options);
		}
		else
		{
			return productModel;
		}


	}

	protected void populateProductData(final ProductData productData, final Model model)
	{
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute("product", productData);
		if (productData.getConfigurable())
		{
			final List<ConfigurationInfoData> configurations = productFacade.getConfiguratorSettingsForCode(productData.getCode());
			if (CollectionUtils.isNotEmpty(configurations))
			{
				model.addAttribute("configuratorType", configurations.get(0).getConfiguratorType());
			}
		}
	}

	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			Collections.sort(productData.getVariantOptions(), variantSortStrategy);
		}
	}

	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images, new Comparator<ImageData>()
			{
				@Override
				public int compare(final ImageData image1, final ImageData image2)
				{
					return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
				}
			});

			if (CollectionUtils.isNotEmpty(images))
			{
				addFormatsToGalleryImages(galleryImages, images);
			}
		}
		return galleryImages;
	}

	protected void addFormatsToGalleryImages(final List<Map<String, ImageData>> galleryImages, final List<ImageData> images)
	{
		int currentIndex = images.get(0).getGalleryIndex().intValue();
		Map<String, ImageData> formats = new HashMap<String, ImageData>();
		for (final ImageData image : images)
		{
			if (currentIndex != image.getGalleryIndex().intValue())
			{
				galleryImages.add(formats);
				formats = new HashMap<>();
				currentIndex = image.getGalleryIndex().intValue();
			}
			formats.put(image.getFormat(), image);
		}
		if (!formats.isEmpty())
		{
			galleryImages.add(formats);
		}
	}

	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	protected AbstractPageModel getPageForProduct(final String productCode) throws CMSItemNotFoundException
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		return cmsPageService.getPageForProduct(productModel, getCmsPreviewService().getPagePreviewCriteria());
	}

	@ModelAttribute("shipmentTypeInfo")
	private ShipmentTypeInfoData populateShipmentTypeInfo(final Model model)
	{
		// [Monzer]: getting the info from cart, then the customer, then the current site
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentCart = getCartFacade().getShipmentTypeInfoForCurrentCart();
		ShipmentTypeInfo info = shipmentTypeInfoForCurrentCart.isPresent() ? shipmentTypeInfoForCurrentCart.get() : null;
		if (info != null && info.getShipmentType() != null)
		{
			return shipmentTypeInfoConverter.convert(shipmentTypeInfoForCurrentCart.get());
		}

		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();
		info = shipmentTypeInfoForCurrentUser.isPresent() ? shipmentTypeInfoForCurrentUser.get() : null;
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite == null && info == null)
		{
			info = new ShipmentTypeInfo(ShipmentType.DELIVERY, null, null);
		}
		else if (currentSite != null && info == null)
		{
			info = new ShipmentTypeInfo(currentSite.getDefaultShipmentType(), currentSite.getDefaultDeliveryLocationCity(),
					currentSite.getDefaultDeliveryPointOfService());
		}

		return shipmentTypeInfoConverter.convert(info);
	}

	/**
	 * @return the customUserService
	 */
	protected CustomUserService getCustomUserService()
	{
		return customUserService;
	}

	/**
	 * @return the cityFacade
	 */
	protected CityFacade getCityFacade()
	{
		return cityFacade;
	}

	/**
	 * @return the shipmentTypeInfoConverter
	 */
	protected Converter<ShipmentTypeInfo, ShipmentTypeInfoData> getShipmentTypeInfoConverter()
	{
		return shipmentTypeInfoConverter;
	}

	/**
	 * @return the checkoutFacade
	 */
	protected CheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	protected SCPIService getScpiService()
	{
		return scpiService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @return the cartFacade
	 */
	public CustomCartFacade getCartFacade()
	{
		return cartFacade;
	}


	private void getVariantsProducts(final ProductModel product, final List<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);
		}
	}

	private List<ProductModel> getVariantsProducts(final ProductModel product)
	{
		if (product == null)
		{
			return Collections.emptyList();
		}
		final List<ProductModel> products = new ArrayList<>();

		getVariantsProducts(product, products);
		return products;
	}

	private boolean isActiveOnB2C(final ProductModel productModel)
	{
		if (productModel == null)
		{
			return false;
		}

		final List<ProductModel> variantsProducts = getVariantsProducts(productModel);
		if (CollectionUtils.isEmpty(variantsProducts))
		{
			return productModel.isActiveOnB2C();
		}

		for (final ProductModel product : variantsProducts)
		{
			if (product.isActiveOnB2C())
			{
				return true;
			}
		}


		return false;

	}
}
