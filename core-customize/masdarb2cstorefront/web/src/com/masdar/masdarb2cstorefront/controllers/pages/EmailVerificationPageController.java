/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.masdar.facades.customer.facade.CustomCustomerFacade;


/**
 * Controller for the forgotten password pages. Supports requesting a password reset email as well as changing the
 * password once you have got the token that was sent via email.
 */
@Controller
@RequestMapping(value = "/email")
public class EmailVerificationPageController extends AbstractPageController
{

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(EmailVerificationPageController.class);

	private static final String REDIRECT_LOGIN = "redirect:/login";
	private static final String REDIRECT_HOME = "redirect:/";

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "secureTokenService")
	private SecureTokenService secureTokenService;

	@RequestMapping(value = "/verify", method = RequestMethod.GET)
	public String verifyCustomer(@RequestParam(required = false)
	final String token, final Model model)
	{
		if (Strings.isBlank(token))
		{
			return REDIRECT_LOGIN;
		}
		final SecureToken decryptData = getSecureTokenService().decryptData(token);
		getCustomerFacade().verifyCustomerById(decryptData.getData());
		return REDIRECT_HOME;
	}


	@RequireHardLogIn
	@RequestMapping(value = "/requestverify", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void requestVerificationEmail(final Model model)
	{
		final CustomerData currentCustomer = getCustomerFacade().getCurrentCustomer();
		getCustomerFacade().sendEmailVerificationEmail(currentCustomer.getCustomerId());
	}

	/**
	 * @return the customerFacade
	 */
	@Override
	public CustomCustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	/**
	 * @return the secureTokenService
	 */
	public SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}
}
