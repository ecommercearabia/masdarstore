/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.pages.otp;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.masdarb2cstorefront.form.OTPForm;
import com.masdar.masdarb2cstorefront.form.validation.OTPUpdateProfileValidator;
import com.masdar.masdarb2cstorefront.form.validation.OTPValidator;
import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.exception.TokenInvalidatedException;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;



/**
 * The Class OTPRegisterVerifyPage.
 *
 * @author @author mnasro
 */
@Controller
@Scope("tenant")
@RequestMapping("/my-account/update-profile/verify")
public class OTPUpdateProfileVerifyPage extends AbstractOTPVerifyPage
{

	/** The otp validator. */
	@Resource(name = "otpUpdateProfileValidator")
	private OTPUpdateProfileValidator otpUpdateProfileValidator;

	@Resource(name = "customCustomerFacade")
	private CustomerFacade customerFacade;

	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpUpdateProfileValidator;
	}

	@Override
	protected String getVerifyActionURL()
	{
		return "/my-account/update-profile/verify";

	}

	@Override
	protected String getChangeNumberActionURL()
	{
		return "/my-account/update-profile/verify/change-number";
	}

	@Override
	protected String getResendVerifyActionURL()
	{
		return "/my-account/update-profile/verify/resend";

	}

	@Override
	protected String getSendVerifyActionURL()
	{
		return "/my-account/update-profile/verify/send";
	}

	@Override
	protected String getFailedErrorLabelOrIdPage()
	{
		return "update-profile";
	}

	@Override
	protected String getOTPViewURL()
	{
		return "/my-account/update-profile/verify";
	}

	@Override
	protected String getFailedErrorView()
	{
		return "/my-account/update-profile";
	}

	@Override
	protected OTPVerificationTokenType getOTPVerificationTokenType()
	{
		return OTPVerificationTokenType.UPDATE_PROFILE;
	}


	/**
	 * Show verify page.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String showVerifyPage(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.showVerifyPage(token, model, request, response);
	}

	/**
	 * Resend OTP code.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/resend")
	public String resendOTPCode(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return super.resendOTPCode(token, model, request, response, redirectModel);
	}

	/**
	 * Gets the change mobile number view.
	 *
	 * @param token
	 *           the token
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the change mobile number view
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/change-number")
	public String getChangeMobileNumberView(@RequestParam(required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.getChangeMobileNumberView(token, model, request, response);
	}


	/**
	 * Send.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/send")
	public String send(@RequestParam(required = true)
	final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		return super.send(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	/**
	 * Verify.
	 *
	 * @param token
	 *           the token
	 * @param otpForm
	 *           the otp form
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @param bindingResult
	 *           the binding result
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws TokenInvalidatedException
	 */
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public String verify(final String token, final OTPForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException, TokenInvalidatedException
	{
		return super.verify(token, otpForm, model, request, response, redirectModel, bindingResult);
	}

	@Override
	protected String doAction(final Model model, final RedirectAttributes redirectModel,
			final OTPVerificationTokenModel otpVerificationTokenModel, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		final CustomerData customerData = (CustomerData) otpVerificationTokenModel.getData();
		customerData.setMobileNumber(otpVerificationTokenModel.getMobileNumber());
		final CountryData mobileCountry = new CountryData();
		mobileCountry.setIsocode(otpVerificationTokenModel.getMobileCountry());
		customerData.setMobileCountry(mobileCountry);
		try
		{
			customerFacade.updateProfile(customerData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.profile.confirmationUpdated", null);
		}
		catch (final DuplicateUidException e)
		{
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			return handleError(model);
		}

		return REDIRECT_PREFIX + "/my-account/update-profile";
	}

}
