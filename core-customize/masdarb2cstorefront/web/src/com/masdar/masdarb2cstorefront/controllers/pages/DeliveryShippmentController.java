/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.enums.ShipmentType;
import com.masdar.facades.facade.CustomCartFacade;


@Controller
@RequestMapping("/store-delivery")
public class DeliveryShippmentController extends AbstractSearchPageController
{
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";

	private static final Logger LOG = Logger.getLogger(DeliveryShippmentController.class);
	private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";



	@Resource(name = "cartFacade")
	private CustomCartFacade cartFacade;

	/**
	 * @return the cartFacade
	 */
	protected CustomCartFacade getCartFacade()
	{
		return cartFacade;
	}

	@RequestMapping(value = "/select/city", method = RequestMethod.POST)
	public String selectDelivery(@RequestParam("cityId")
	final String cityId, final Model model, final RedirectAttributes redirectModel, final HttpServletRequest request)
	{


		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(REDIRECT_PREFIX).append(request.getHeader("Referer"));
		boolean isAddFlashMessages = false;
		try
		{
			final List<CartModificationData> cartModifications = getCartFacade()
					.updateShipmentTypeForAllCartEntry(ShipmentType.DELIVERY, null, cityId);

			isAddFlashMessages = addFlashMessages(request, redirectModel, cartModifications);
		}
		catch (final Exception e)
		{
			LOG.error("Error In Delivery: " + e.getMessage());
			return stringBuilder.toString();
		}

		return isAddFlashMessages ? REDIRECT_CART_URL : stringBuilder.toString();
	}

	protected boolean addFlashMessages(final HttpServletRequest request, final RedirectAttributes redirectModel,
			final List<CartModificationData> cartModifications)
	{
		if (CollectionUtils.isEmpty(cartModifications))
		{
			return false;
		}

		boolean foundError = false;
		for (final CartModificationData cartModification : cartModifications)
		{
			if ("lowStock".equals(cartModification.getStatusCode()))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"basket.page.message.update.reducedNumberOfItemsAdded.lowStock", new Object[]
						{ XSSFilterUtil.filter(cartModification.getEntry().getProduct().getName()),
								Long.valueOf(cartModification.getQuantity()), 10,
								request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				foundError = true;
			}
			else if ("noStock".equals(cartModification.getStatusCode()))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"basket.page.message.update.reducedNumberOfItemsAdded.noStock", new Object[]
						{ XSSFilterUtil.filter(cartModification.getEntry().getProduct().getName()),
								request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				foundError = true;
			}

		}
		return foundError;
	}


	protected boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}


}
