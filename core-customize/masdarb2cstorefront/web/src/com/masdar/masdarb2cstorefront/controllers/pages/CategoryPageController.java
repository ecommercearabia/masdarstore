/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.masdar.core.resolver.ShippementTypeURLResolver;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;


/**
 * Controller for a category page
 */
@Controller
@RequestMapping(value = "/**/c")
public class CategoryPageController extends AbstractCategoryPageController
{


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "shippementTypeURLResolver")
	private ShippementTypeURLResolver shippementTypeURLResolver;

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String category(@PathVariable("categoryCode")
	final String categoryCode, // NOSONAR
			@RequestParam(value = "q", required = false)
			final String searchQuery, @RequestParam(value = "page", defaultValue = "0")
			final int page, @RequestParam(value = "show", defaultValue = "Page")
			final ShowMode showMode, @RequestParam(value = "sort", required = false)
			final String sortCode, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws UnsupportedEncodingException
	{
		final Object attribute = sessionService.getAttribute(ControllerConstants.Actions.SessionPlpView.plpViewKey);
		model.addAttribute("layout", "list");
		if (attribute != null)
		{
			model.addAttribute("layout", sessionService.getAttribute("plpViewData"));
		}
		return performSearchAndGetResultsPage(categoryCode, getQuery(searchQuery, sortCode), page, showMode, sortCode, model,
				request, response);
	}

	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode")
	final String categoryCode, @RequestParam(value = "q", required = false)
	final String searchQuery, @RequestParam(value = "page", defaultValue = "0")
	final int page, @RequestParam(value = "show", defaultValue = "Page")
	final ShowMode showMode, @RequestParam(value = "sort", required = false)
	final String sortCode) throws UnsupportedEncodingException
	{
		return performSearchAndGetFacets(categoryCode, getQuery(searchQuery, sortCode), page, showMode, sortCode);
	}

	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode")
	final String categoryCode, @RequestParam(value = "q", required = false)
	final String searchQuery, @RequestParam(value = "page", defaultValue = "0")
	final int page, @RequestParam(value = "show", defaultValue = "Page")
	final ShowMode showMode, @RequestParam(value = "sort", required = false)
	final String sortCode) throws UnsupportedEncodingException
	{
		return performSearchAndGetResultsData(categoryCode, getQuery(searchQuery, sortCode), page, showMode, sortCode);
	}


	protected String getQuery(String searchQuery, final String sortCode)
	{

		if (!cmsSiteService.getCurrentSite().isEnableWarehouseStockFilterOnPLP())
		{
			return searchQuery;
		}
		if (Strings.isBlank(searchQuery))
		{
			searchQuery = ":relevance";
		}
		final String query = getShippementTypeURLResolver().getSolarSearchQueryByCurrentCustomer(sortCode);
		if (!Strings.isBlank(query))
		{
			searchQuery = resetWarehouseStock(searchQuery, sortCode);
		}

		return Objects.nonNull(searchQuery) ? searchQuery.concat(query) : query;
	}


	/**
	 * @param searchQuery
	 * @return
	 */
	private String resetWarehouseStock(final String searchQuery, final String sortCode)
	{
		if (Strings.isBlank(searchQuery))
		{
			return Strings.EMPTY;
		}
		final String toRemove = "(:warehouseStock:\\w+)";
		final String firstQueryParam = "(^:\\w+)";
		String result = searchQuery.replaceAll(toRemove, "");

		if (Strings.isNotBlank(sortCode))
		{
			result = result.replaceAll(firstQueryParam, "");
		}
		return result;
	}

	/**
	 * @return the shippementTypeURLResolver
	 */
	protected ShippementTypeURLResolver getShippementTypeURLResolver()
	{
		return shippementTypeURLResolver;
	}

}
