/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.area.facade.AreaFacade;
import com.masdar.facades.city.facade.CityFacade;
import com.masdar.facades.delivery.DeliveryLocationService;
import com.masdar.masdarfacades.customer.TimeDeliveryLocationData;
import com.masdar.masdarfacades.data.MetaData;
import com.masdar.masdarfacades.data.ResponseData;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;
import com.masdar.masdartimeslotfacades.exception.type.TimeSlotExceptionType;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Controller
@RequestMapping("/misc")
public class MiscController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(MiscController.class);

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;
	@Resource(name = "customerService")
	private CustomerService customerService;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "deliveryLocationService")
	private DeliveryLocationService deliveryLocationService;
	@Resource(name = "cityConverter")
	private Converter<CityData, CityModel> cityConverter;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	private List<CityData> filterBySelectedCity(final String regionCode)
	{
		if (StringUtils.isBlank(regionCode))
		{
			return Collections.<CityData> emptyList();
		}

		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || Objects.isNull(shipmentTypeInfoForCurrentUser.get().getCity()))
		{
			return Collections.<CityData> emptyList();
		}

		final CityModel selectedCityModel = shipmentTypeInfoForCurrentUser.get().getCity();

		final RegionModel selectedRegionModel = selectedCityModel.getRegion();

		if (Objects.isNull(selectedRegionModel) || StringUtils.isBlank(selectedRegionModel.getIsocode())
				|| !selectedRegionModel.isActiveOnB2C() || !selectedRegionModel.getIsocode().equals(regionCode))
		{
			return Collections.<CityData> emptyList();
		}

		final Optional<CityData> city = cityFacade.get(selectedCityModel.getCode());

		return city.isPresent() ? Arrays.asList(city.get()) : Collections.<CityData> emptyList();

	}

	@RequestMapping(value = "/region/{regionCode}/cities")
	@ResponseBody
	public ResponseData getCitiesByRegionId(final HttpServletResponse response, @RequestParam("isCheckout")
	final boolean isCheckout, @PathVariable
	final String regionCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			final List<CityData> cityList = isCheckout ? filterBySelectedCity(regionCode)
					: cityFacade.getByRegionIsocode(regionCode).get();

			data.setData(cityList);
			meta.setMessage("getting cites by isoCode of country");
			meta.setStatusCode(HttpStatus.OK.value());
			data.setMeta(meta);
		}
		catch (final Exception e)
		{
			data.setData(null);
			meta.setMessage("failed to get cites by isoCode of country");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	/**
	 * Gets the cites.
	 *
	 * @param response
	 *           the response
	 * @param code
	 *           the code
	 * @return the cites
	 */
	@RequestMapping(value = "/country/{code}/cities", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCities(final HttpServletResponse response, @PathVariable
	final String code)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		//	customerFacade.getCurrentCustomer()

		try
		{
			final Optional<List<CityData>> countryIsocode = cityFacade.getByCountryIsocode(code);
			if (countryIsocode.isPresent())
			{
				data.setData(countryIsocode.get());
			}
			meta.setMessage("getting cities by isoCode of country");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get cites by isoCode of country");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);

		}

		return data;
	}

	/**
	 * Gets the areas.
	 *
	 * @param response
	 *           the response
	 * @param cityCode
	 *           the city code
	 * @return the areas
	 */
	@RequestMapping(value = "/city/{cityCode}/areas", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getAreas(final HttpServletResponse response, @PathVariable
	final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();


		try
		{
			data.setData(areaFacade.getByCityCode(cityCode).orElse(null));
			meta.setMessage("getting areas by city code ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get areas by city code");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	@RequestMapping(value = "/delivery-location/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData saveCustomerDeliveryLocation(final HttpServletResponse response,
			@RequestParam(value = "areaCode", required = true)
			final String areaCode, @RequestParam(value = "cityCode", required = true)
			final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		Optional<TimeDeliveryLocationData> saveCustomerDeliveryLocation = Optional.empty();
		try
		{
			saveCustomerDeliveryLocation = deliveryLocationService.saveCustomerDeliveryLocation(areaCode, cityCode);
			if (saveCustomerDeliveryLocation.isPresent())
			{
				data.setData(saveCustomerDeliveryLocation.get());
				meta.setMessage("Saving CustomerDelivery Location areas by city code ");
				meta.setStatusCode(HttpStatus.ACCEPTED.value());
				data.setMeta(meta);
			}
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get city or areas by there codes");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			meta.setDisplayMessage("Failed to get city or area by code");
			data.setMeta(meta);
		}
		catch (final TimeSlotException ex)
		{
			data.setData(null);
			meta.setMessage(ex.getMessage());
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			meta.setDisplayMessage(getDisplayErrorMessage(ex));
			data.setMeta(meta);
		}
		return data;
	}

	/**
	 * @param ex
	 * @return
	 */
	private String getDisplayErrorMessage(final TimeSlotException ex)
	{
		return ex.getMessage();
	}

	@RequestMapping(value = "/delivery-location/{cityCode}/{areaCode}/location", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCustomerDeliveryLocationData(final HttpServletResponse response, @PathVariable(required = true)
	final String areaCode, @PathVariable(required = true)
	final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		try
		{
			final Optional<TimeDeliveryLocationData> customerDeliveryLocationData = deliveryLocationService
					.getTimeDeliveryLocationData(areaCode, cityCode);
			if (!customerDeliveryLocationData.isPresent())
			{
				throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOTS_FOUND, "no timeslots found");
			}
			data.setData(customerDeliveryLocationData.get());
			meta.setMessage("Saving CustomerDelivery Location areas by city code ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException | TimeSlotException e)
		{
			data.setData(null);
			meta.setMessage("failed to get city or areas by there codes || getCustomerDeliveryLocationData");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}
		return data;



	}



	/**
	 * Gets the country.
	 *
	 * @param response
	 *           the response
	 * @param code
	 *           the code
	 * @return the country
	 */
	@RequestMapping(value = "/country/{code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCountry(final HttpServletResponse response, @PathVariable
	final String code)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(countryConverter.convert(commonI18NService.getCountry(code)));
			meta.setMessage("getting country by isoCode ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get country by isoCode");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}


	@RequestMapping(value = "/inctax", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData setIncTax(final HttpServletResponse response, @RequestParam
	final boolean incTax)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		try
		{
			LOG.info("MiscController->setIncTax: incTax[" + incTax + "] ...");

			sessionService.getCurrentSession().setAttribute("incTax", incTax);
			data.setData(sessionService.getCurrentSession().getAttribute("incTax"));
			meta.setMessage("set Inc tax");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);

			LOG.info("MiscController->getIncTax: incTax[" + sessionService.getCurrentSession().getAttribute("incTax") + "]");

		}
		catch (final IllegalArgumentException e)
		{
			LOG.info("MiscController->setIncTax: Exception[" + e.getMessage() + "]");

			data.setData(null);
			meta.setMessage("failed to set Inc tax");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	/**
	 * @return the cityConverter
	 */
	public Converter<CityData, CityModel> getCityConverter()
	{
		return cityConverter;
	}
}
