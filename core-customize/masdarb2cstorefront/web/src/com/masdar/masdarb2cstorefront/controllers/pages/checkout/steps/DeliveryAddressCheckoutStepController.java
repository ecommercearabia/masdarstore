/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.constants.GeneratedMasdarCoreConstants.Enumerations.ShipmentType;
import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.area.facade.AreaFacade;
import com.masdar.facades.city.facade.CityFacade;
import com.masdar.masdarb2cstorefront.checkout.steps.CheckoutStep;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.ValidationResults;
import com.masdar.masdarb2cstorefront.controllers.ControllerConstants;
import com.masdar.masdarb2cstorefront.form.AddressForm;
import com.masdar.masdarb2cstorefront.util.AddressDataUtil;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{
	private static final String DELIVERY_ADDRESS = "delivery-address";
	private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";
	private static final String AREAS = "areas";
	private static final String CITIES = "cities";
	@Resource(name = "customAddressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;


	@Resource(name = "regionConverter")
	private Converter<RegionModel, RegionData> regionConverter;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;


	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getCheckoutFacade().setDeliveryAddressIfAvailable();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final AddressForm addressForm = new AddressForm();
		populateCommonModelAttributes(model, cartData, addressForm);
		setDefaultDeliveryModeIfAvailable();
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	/**
	 *
	 */
	private void setDefaultDeliveryModeIfAvailable()
	{
		final List<? extends DeliveryModeData> supportedDeliveryModes = getCheckoutFacade().getSupportedDeliveryModes();

		if (supportedDeliveryModes != null && !supportedDeliveryModes.isEmpty())
		{
			//Set first delivery method
			getCheckoutFacade().setDeliveryMode(supportedDeliveryModes.get(0).getCode());
		}

	}

	/**
	 * @param addressForm
	 */
	private void fetchDefaultMobileCountryIntoAddressFormByCurrentSite(final AddressForm addressForm)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite == null || !currentSite.isUseDefaultMobileCountryOnAddress())
		{
			return;
		}
		addressForm.setMobileCountry(getDefaultCountryCodeByForSite(getCmsSiteService().getCurrentSite()));
	}

	protected String getDefaultCountryCodeByForSite(final CMSSiteModel cmsSite)
	{
		return cmsSite == null || cmsSite.getDefaultMobileCountry() == null ? "" : cmsSite.getDefaultMobileCountry().getIsocode();
	}

	@RequestMapping(value = "/pickup-in-store", method = RequestMethod.POST)
	@RequireHardLogIn
	public String pickupInStore(final Model model) throws CMSItemNotFoundException
	{

		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (cartData != null && cartData.getShipmentType() != null && cartData.getShipmentType().getCode() != null
				&& cartData.getShipmentType().getCode().equalsIgnoreCase(ShipmentType.PICKUP_IN_STORE))
		{
			return getCheckoutStep().nextStep();
		}
		return getCheckoutStep().currentStep();
	}



	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		fetchDefaultMobileCountryIntoAddressFormByCurrentSite(addressForm);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		getAddressValidator().validate(addressForm, bindingResult);
		populateCommonModelAttributes(model, cartData, addressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

		processAddressVisibilityAndDefault(addressForm, newAddress);

		// Verify the address data.
		//		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
		//				.verifyAddressData(newAddress);
		//		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
		//				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
		//				"checkout.multi.address.updated");
		//
		//		if (addressRequiresReview)
		//		{
		//			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		//		}

		getUserFacade().addAddress(newAddress);

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibilityAndDefault(final AddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && CollectionUtils.isEmpty(getUserFacade().getAddressBook()))
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode")
	final String editAddressCode, final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}

		AddressData addressData = null;
		if (StringUtils.isNotEmpty(editAddressCode))
		{
			addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
		}

		final AddressForm addressForm = new AddressForm();
		final boolean hasAddressData = addressData != null;
		if (hasAddressData)
		{
			addressDataUtil.convert(addressData, addressForm);
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, cartData, addressForm);

		if (addressData != null)
		{
			model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
		}

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, cartData, addressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = addressDataUtil.convertToAddressData(addressForm);

		processAddressVisibility(addressForm, newAddress);

		newAddress.setDefaultAddress(CollectionUtils.isEmpty(getUserFacade().getAddressBook())
				|| getUserFacade().getAddressBook().size() == 1 || Boolean.TRUE.equals(addressForm.getDefaultAddress()));

		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			if (StringUtils.isNotEmpty(addressForm.getAddressId()))
			{
				final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(addressForm.getAddressId());
				if (addressData != null)
				{
					model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
					model.addAttribute("edit", Boolean.TRUE);
				}
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().editAddress(newAddress);
		getCheckoutFacade().setDeliveryModeIfAvailable();
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibility(final AddressForm addressForm, final AddressData newAddress)
	{

		if (addressForm.getSaveInAddressBook() == null)
		{
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(addressForm.getSaveInAddressBook()));
		}
	}

	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST }) //NOSONAR
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode")
	final String addressCode, final RedirectAttributes redirectModel, final Model model) throws CMSItemNotFoundException
	{
		if (getCheckoutFacade().isRemoveAddressEnabledForCart())
		{
			final AddressData addressData = new AddressData();
			addressData.setId(addressCode);
			getUserFacade().removeAddress(addressData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.confirmation.address.removed");
		}
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute("addressForm", new AddressForm());

		return getCheckoutStep().currentStep();
	}

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = addressDataUtil.convertToAddressData(addressForm);
		final CountryData countryData = selectedAddress.getCountry();

		if (!resolveCountryRegions.contains(countryData.getIsocode()))
		{
			selectedAddress.setRegion(null);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryAddress(@RequestParam("selectedAddressCode")
	final String selectedAddressCode, final RedirectAttributes redirectAttributes)
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				setDeliveryAddress(selectedAddressData);
			}
		}
		return getCheckoutStep().nextStep();
	}

	protected void setDeliveryAddress(final AddressData selectedAddressData)
	{
		final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
		{
			getCheckoutFacade().setDeliveryAddress(selectedAddressData);
			if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
			{ // temporary address should be removed
				getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
			}
		}
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected String getBreadcrumbKey()
	{
		return "checkout.multi." + getCheckoutStep().getProgressBarId() + ".breadcrumb";
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}

	private List<RegionData> getRegions(final String countryIsoCode)
	{

		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || Objects.isNull(shipmentTypeInfoForCurrentUser.get().getCity()))
		{
			return Collections.emptyList();
		}

		final CityModel selectedCityModel = shipmentTypeInfoForCurrentUser.get().getCity();

		final RegionModel selectedRegionModel = selectedCityModel.getRegion();

		if (Objects.isNull(selectedRegionModel) || StringUtils.isBlank(selectedRegionModel.getIsocode())
				|| !selectedRegionModel.isActiveOnB2C() || selectedRegionModel.getCountry() == null
				|| !countryIsoCode.equals(selectedRegionModel.getCountry().getIsocode()))
		{
			return Collections.emptyList();
		}
		final RegionData regionData = regionConverter.convert(selectedRegionModel);


		return regionData == null ? Collections.emptyList() : Arrays.asList(regionData);
	}

	protected void populateCommonModelAttributes(final Model model, final CartData cartData, final AddressForm addressForm)
			throws CMSItemNotFoundException
	{
		model.addAttribute("cartData", cartData);
		model.addAttribute("addressForm", addressForm);
		model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("addressFormEnabled", Boolean.valueOf(getCheckoutFacade().isNewAddressEnabledForCart()));
		model.addAttribute("removeAddressEnabled", Boolean.valueOf(getCheckoutFacade().isRemoveAddressEnabledForCart()));
		model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(getBreadcrumbKey()));
		model.addAttribute("metaRobots", "noindex,nofollow");
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute("regions", getRegions(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}

		if (StringUtils.isNotBlank(addressForm.getRegionIso()))
		{
			final List<CityData> cities = filterBySelectedCity(addressForm.getRegionIso());

			model.addAttribute(CITIES, cities);
		}

		if (StringUtils.isNotBlank(addressForm.getCityCode()))
		{
			final Optional<List<AreaData>> areas = areaFacade.getByCityCode(addressForm.getCityCode());
			model.addAttribute(AREAS, areas.isPresent() ? areas.get() : null);
		}


		prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		String code = null;
		if (getCmsSiteService().getCurrentSite().getAddressBookDefaultCity() != null)
		{
			code = getCmsSiteService().getCurrentSite().getAddressBookDefaultCity().getCode();
		}

		final Optional<List<AreaData>> areasByCityCode = areaFacade.getByCityCode(code);
		if (StringUtils.isNotBlank(code) && areasByCityCode.isPresent())
		{
			addressForm.setCityCode(code);
			model.addAttribute("areas", areasByCityCode.get());
		}
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		final boolean anonymousUser = getUserFacade().isAnonymousUser();
		addressForm.setMobileCountry(preselectMobileCountry(currentSite, anonymousUser));

	}

	private String preselectMobileCountry(final CMSSiteModel currentSite, final boolean anonymousUser)
	{
		String mobileCountry = "";
		//&& currentSite.isMobileCountryPreSelecetd()
		if (currentSite != null)
		{
			mobileCountry = currentSite.getDefaultMobileCountry() == null ? "" : currentSite.getDefaultMobileCountry().getIsocode();
		}
		else if (!anonymousUser)
		{
			final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();
			mobileCountry = currentCustomerData.getMobileCountry() == null ? ""
					: currentCustomerData.getMobileCountry().getIsocode();
		}
		else
		{
			mobileCountry = currentSite == null || currentSite.getDefaultMobileCountry() == null ? ""
					: currentSite.getDefaultMobileCountry().getIsocode();
		}
		return mobileCountry;
	}

	private List<CityData> filterBySelectedCity(final String regionCode)
	{
		if (StringUtils.isBlank(regionCode))
		{
			return Collections.<CityData> emptyList();
		}

		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || Objects.isNull(shipmentTypeInfoForCurrentUser.get().getCity()))
		{
			return Collections.<CityData> emptyList();
		}

		final CityModel selectedCityModel = shipmentTypeInfoForCurrentUser.get().getCity();

		final RegionModel selectedRegionModel = selectedCityModel.getRegion();

		if (Objects.isNull(selectedRegionModel) || StringUtils.isBlank(selectedRegionModel.getIsocode())
				|| !selectedRegionModel.isActiveOnB2C() || !selectedRegionModel.getIsocode().equals(regionCode))
		{
			return Collections.<CityData> emptyList();
		}

		final Optional<CityData> city = cityFacade.get(selectedCityModel.getCode());

		return city.isPresent() ? Arrays.asList(city.get()) : Collections.<CityData> emptyList();

	}
}
