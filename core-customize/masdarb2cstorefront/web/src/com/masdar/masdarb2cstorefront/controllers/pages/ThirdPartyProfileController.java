/**
 *
 */
package com.masdar.masdarb2cstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.enums.MaritalStatus;
import com.masdar.facades.country.facade.CountryFacade;
import com.masdar.facades.customer.facade.CustomCustomerFacade;
import com.masdar.facades.nationality.facade.NationalityFacade;
import com.masdar.masdarb2c.form.data.ThirdPartyProfileFormData;
import com.masdar.masdarb2cstorefront.form.validation.ThirdPartyProfileValidator;
import com.masdar.masdarb2cstorefront.security.ThirdPartyAutoLoginStrategy;
import com.masdar.masdarfacades.customer.data.MaritalStatusData;
import com.masdar.masdarotp.context.OTPContext;
import com.masdar.masdarotp.enums.OTPVerificationTokenType;
import com.masdar.masdarotp.exception.OTPException;
import com.masdar.masdarotp.model.OTPVerificationTokenModel;
import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.masdar.masdarthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.masdar.masdarthirdpartyauthentication.service.ThirdPartyUserService;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/third-party")
public class ThirdPartyProfileController extends AbstractSearchPageController
{

	private static final String THIRD_PARTY_USER = "ThirdPartyUser";
	private static final String TOKEN = "token";
	private static final String TYPE = "type";
	private static final String EXISTS = "exists";
	private static final Logger LOG = Logger.getLogger(ThirdPartyProfileController.class);
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
	private static final String UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE = "third-party-profile";
	private static final String TITLE_DATA_ATTR = "titleData";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	private static final String THIRD_PARTY_OTP_PAGE = "/third-party/verify";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	@Resource(name = "thirdPartyProfileValidator")
	private ThirdPartyProfileValidator profileValidator;

	@Resource(name = "thirdPartyUserService")
	private ThirdPartyUserService thirdPartyUserService;

	@Resource(name = "thirdPartyAutoLoginStrategy")
	private ThirdPartyAutoLoginStrategy autoLoginStrategy;

	@Resource(name = "nationalityFacade")
	private NationalityFacade nationalityFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "maritalStatusDataConverter")
	private Converter<MaritalStatus, MaritalStatusData> maritalStatusDataConverter;

	@Resource(name = "otpContext")
	protected OTPContext otpContext;

	@ModelAttribute("maritalStatuses")
	public Collection<MaritalStatusData> getMaritalStatuses()
	{
		return customerFacade.getAllMaritailStatuses();
	}


	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	public String getUpdateProfileView(final Model model, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final Map<String, Object> sessionAttributes = extractSessionAttributes();
		if (sessionAttributes.isEmpty())
		{
			return REDIRECT_PREFIX + "/login";
		}
		final ThirdPartyAuthenticationUserData profile = (ThirdPartyAuthenticationUserData) sessionAttributes.get(THIRD_PARTY_USER);
		final String token = String.valueOf(sessionAttributes.get(TOKEN));
		final String type = String.valueOf(sessionAttributes.get(TYPE));
		final Boolean exists = (Boolean) sessionAttributes.get(EXISTS);

		if (Boolean.TRUE.equals(exists))
		{
			final String username = profile.getEmail() + "|" + profile.getId();

			autoLoginStrategy.login(username, token, type, request, response);

			LOG.info(profile.getEmail() + " is authenticated successfully!");
		}

		model.addAttribute(TITLE_DATA_ATTR, userFacade.getTitles());

		final ThirdPartyProfileFormData profileForm = new ThirdPartyProfileFormData();
		profileForm.setFirstName(profile.getFirstName());
		profileForm.setLastName(profile.getLastName());
		profileForm.setEmail(profile.getEmail());
		profileForm.setMobileNumber(profile.getMobileNumber());
		profileForm.setBirthDate(profile.getDateOfBirth());
		profileForm.setNationality(profile.getNationality());
		profileForm.setNationalityId(profile.getNationalityId());
		profileForm.setMaritalStatusCode(profile.getMaritalStatus() == null ? null : profile.getMaritalStatus().getCode());
		profileForm.setInvolvedInLoyaltyProgram(profile.isInvolvedInLoyaltyProgram());
		model.addAttribute("thirdPartyProfileForm", profileForm);
		model.addAttribute("nationalities", nationalityFacade.getByCurrentSite());
		model.addAttribute("cmsSite", cmsSiteService.getCurrentSite());
		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		if (profileForm.getEmail() == null || profileForm.getMobileCountry() == null || profileForm.getMobileNumber() == null
				|| profileForm.getFirstName() == null || profileForm.getLastName() == null)
		{
			return getViewForPage(model);
		}
		else
		{
			return REDIRECT_PREFIX + "/";
		}
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	public String updateProfileView(final ThirdPartyProfileFormData profile, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		profile.setTitleCode("mr");
		final Map<String, Object> sessionAttributes = extractSessionAttributes();
		if (Collections.emptyMap().equals(sessionAttributes))
		{
			return "";
		}

		final ThirdPartyAuthenticationUserData sessionedProfile = (ThirdPartyAuthenticationUserData) sessionAttributes
				.get(THIRD_PARTY_USER);

		if (sessionedProfile != null)
		{
			if (StringUtils.isBlank(profile.getFirstName()))
			{
				profile.setFirstName(sessionedProfile.getFirstName());
			}
			if (StringUtils.isBlank(profile.getLastName()))
			{
				profile.setLastName(sessionedProfile.getLastName());
			}
			if (StringUtils.isBlank(profile.getEmail()))
			{
				profile.setEmail(sessionedProfile.getEmail());
			}
		}
		profileValidator.validate(profile, bindingResult);
		String returnedAction = "";
		if (bindingResult.hasErrors())
		{
			returnedAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
			return returnedAction;
		}

		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite != null && currentSite.isActiveOTPForThirdPartyProfile() && !profile.isVerified())
		{
			return generatOTP(profile, model);
		}

		returnedAction = REDIRECT_PREFIX + "/";

		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(profile.getTitleCode());
		customerData.setFirstName(profile.getFirstName());
		customerData.setLastName(profile.getLastName());
		final CountryData mobileCountry = new CountryData();
		mobileCountry.setIsocode(profile.getMobileCountry());
		customerData.setMobileCountry(mobileCountry);
		customerData.setMobileNumber(profile.getMobileNumber());
		customerData.setCustomerId(sessionedProfile.getId());
		customerData.setUid(sessionedProfile.getId() + "|" + sessionedProfile.getEmail());
		customerData.setName(StringUtils.isBlank(sessionedProfile.getName()) ? profile.getFirstName() + " " + profile.getLastName()
				: sessionedProfile.getName());
		customerData.setNationalityID(profile.getNationalityId());
		customerData.setBirthDate(profile.getBirthDate());
		final ThirdPartyAuthenticationType thirdPartyType = ThirdPartyAuthenticationType
				.valueOf(String.valueOf(sessionAttributes.get(TYPE)));
		customerData.setThirdPartyType(thirdPartyType);
		if (StringUtils.isNotBlank(profile.getNationality()))
		{
			final Optional<NationalityData> updateProfileNationality = nationalityFacade.get(profile.getNationality());
			if (updateProfileNationality.isPresent())
			{
				customerData.setNationality(updateProfileNationality.get());
			}

		}
		if (StringUtils.isNotBlank(profile.getMaritalStatusCode()))
		{
			MaritalStatus status = null;
			try
			{
				status = MaritalStatus.valueOf(profile.getMaritalStatusCode());
			}
			catch (final IllegalArgumentException e)
			{
				LOG.info("Third-Party customer " + status + " marital status is invalid");
			}
			if (status != null)
			{
				customerData.setMaritalStatus(maritalStatusDataConverter.convert(status));
			}
		}
		customerData.setInvolvedInLoyaltyProgram(profile.isInvolvedInLoyaltyProgram());
		final Boolean exists = (Boolean) sessionAttributes.get(EXISTS);
		try
		{
			if (Boolean.TRUE.equals(exists))
			{
				customerFacade.updateProfile(customerData);
			}
			else
			{
				thirdPartyUserService.saveUser(customerData);
			}
			final String token = (String) sessionAttributes.get(TOKEN);
			final String type = (String) sessionAttributes.get(TYPE);

			final boolean login = autoLoginStrategy.login(customerData.getUid(), token, type, request, response);
			if (login)
			{
				LOG.info(profile.getEmail() + " is authenticated successfully!");
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);
			}
			else
			{
				LOG.info(profile.getEmail() + " is not authenticated successfully!");
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"text.account.profile.not.confirmationUpdated", null);
			}
		}
		catch (final DuplicateUidException e)
		{
			LOG.error(e.getMessage());
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			returnedAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
			return returnedAction;
		}

		getSessionService().removeAttribute(EXISTS);
		getSessionService().removeAttribute(TOKEN);
		getSessionService().removeAttribute(TYPE);
		getSessionService().removeAttribute(THIRD_PARTY_USER);

		final ContentPageModel updateProfilePage = getContentPageForLabelOrId(UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
		storeCmsPageInModel(model, updateProfilePage);
		setUpMetaDataForContentPage(model, updateProfilePage);
		model.addAttribute("thirdPartyProfileForm", profile);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return returnedAction;
	}

	@Override
	@ModelAttribute("mobileCountries")
	public Collection<CountryData> getMobileCountries()
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		return mobileCountries.isPresent() ? mobileCountries.get() : null;
	}

	protected String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
		final ContentPageModel cmsPage = getContentPageForLabelOrId(cmsPageLabelOrId);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, cmsPage);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute("nationalities", nationalityFacade.getByCurrentSite());
		return getViewForPage(model);
	}

	private String generatOTP(final ThirdPartyProfileFormData profile, final Model model) throws CMSItemNotFoundException
	{
		try
		{
			final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = otpContext.sendOTPCodeByCurrentSiteAndCustomer(
					OTPVerificationTokenType.UPDATE_PROFILE, profile.getMobileCountry(), profile.getMobileNumber(), profile);
			if (otpVerificationTokenModel.isEmpty())
			{
				return setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
			}
			String encodeToken = otpVerificationTokenModel.get().getToken();
			try
			{
				encodeToken = URLEncoder.encode(otpVerificationTokenModel.get().getToken(), StandardCharsets.UTF_8.toString());
			}
			catch (final UnsupportedEncodingException e)
			{
				LOG.error(String.format("token from otpVerificationTokenModel could not be decoded : %s",
						otpVerificationTokenModel.get().getToken()), e);
			}

			return REDIRECT_PREFIX + "/third-party/verify?token=" + encodeToken;
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
				case VERIFICATION_ERROR:
				case MESSAGE_CAN_NOT_BE_SENT:
					GlobalMessages.addErrorMessage(model, ex.getMessage());
			}

			return setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_THIRD_PARTY_CMS_PAGE);
		}
	}

	private boolean checkCustomerAttributes(final CustomerData customer)
	{
		if (StringUtils.isNotBlank(customer.getUid()))
		{
			final String email = customer.getUid().split("\\|")[1];
			if (StringUtils.isBlank(email))
			{
				return false;
			}
		}
		if (StringUtils.isBlank(customer.getFirstName()))
		{
			return false;
		}
		if (StringUtils.isBlank(customer.getLastName()))
		{
			return false;
		}
		if (customer.getMobileCountry() == null)
		{
			return false;
		}
		return !StringUtils.isBlank(customer.getMobileNumber());
	}

	private Map<String, Object> extractSessionAttributes()
	{
		final Map<String, Object> sessionAttributes = getSessionService().getAllAttributes();
		if (!sessionAttributes.containsKey(THIRD_PARTY_USER))
		{
			return Collections.emptyMap();
		}
		if (!sessionAttributes.containsKey(TOKEN))
		{
			return Collections.emptyMap();
		}
		if (!sessionAttributes.containsKey(TYPE))
		{
			return Collections.emptyMap();
		}
		if (!sessionAttributes.containsKey(EXISTS))
		{
			return Collections.emptyMap();
		}
		return sessionAttributes;
	}

}
