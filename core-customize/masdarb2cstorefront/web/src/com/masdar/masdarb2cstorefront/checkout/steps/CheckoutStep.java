/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.checkout.steps;



import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.facades.facade.CustomCheckoutFlowFacade;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.CheckoutStepValidator;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.ValidationResults;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;


public class CheckoutStep implements StepTransition
{
	public static final String PREVIOUS = "previous";
	public static final String CURRENT = "current";
	public static final String NEXT = "next";
	private Map<String, String> transitions;
	private CheckoutStepValidator checkoutStepValidator;
	private CheckoutGroup checkoutGroup;
	private String progressBarId;

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Override
	public String go(final String transition)
	{
		if (getTransitions().containsKey(transition))
		{
			return getTransitions().get(transition);
		}
		return null;
	}

	@Override
	public String onValidation(final ValidationResults validationResult)
	{
		if (getCheckoutGroup().getValidationResultsMap().containsKey(validationResult.name()))
		{
			return getCheckoutGroup().getValidationResultsMap().get(validationResult.name());
		}
		return null;
	}

	public ValidationResults validate(final RedirectAttributes redirectAttributes)
	{
		return (getCheckoutStepValidator() != null) ? getCheckoutStepValidator().validateOnEnter(redirectAttributes)
				: ValidationResults.SUCCESS;
	}

	public boolean checkIfValidationErrors(final ValidationResults validationResult)
	{
		return !validationResult.equals(ValidationResults.SUCCESS);
	}

	public boolean isEnabled()
	{
		try
		{
			return !"timeSlot".equals(this.progressBarId) || getCustomCheckoutFlowFacade().isTimeSlotEnabledByCurrentSite();
		}
		catch (final TimeSlotException e)
		{
			// [Monzer] If any exception occurred on timeslot, timeslot is not enabled
			return false;
		}
	}

	public String previousStep()
	{
		return go(PREVIOUS);
	}

	public String currentStep()
	{
		return go(CURRENT);
	}

	public String nextStep()
	{
		if (getCheckoutStepValidator() != null)
		{
			final ValidationResults validationResult = getCheckoutStepValidator().validateOnExit();
			if (validationResult != null && !validationResult.equals(ValidationResults.SUCCESS))
			{
				return onValidation(validationResult);
			}
		}
		return go(NEXT);
	}

	public Map<String, String> getTransitions()
	{
		return transitions;
	}

	@Required
	public void setTransitions(final Map<String, String> transitions)
	{
		this.transitions = transitions;
	}

	public CheckoutStepValidator getCheckoutStepValidator()
	{
		return checkoutStepValidator;
	}

	public void setCheckoutStepValidator(final CheckoutStepValidator checkoutStepValidator)
	{
		this.checkoutStepValidator = checkoutStepValidator;
	}

	public CheckoutGroup getCheckoutGroup()
	{
		return checkoutGroup;
	}

	@Required
	public void setCheckoutGroup(final CheckoutGroup checkoutGroup)
	{
		this.checkoutGroup = checkoutGroup;
	}

	public String getProgressBarId()
	{
		return progressBarId;
	}

	@Required
	public void setProgressBarId(final String progressBarId)
	{
		this.progressBarId = progressBarId;
	}

	/**
	 * @return the customCheckoutFlowFacade
	 */
	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}
}
