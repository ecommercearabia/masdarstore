/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.facade.CustomCheckoutFlowFacade;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.ValidationResults;


public class ResponsiveDeliveryMethodCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveDeliveryMethodCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.REDIRECT_TO_CART;
		}


		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || shipmentTypeInfoForCurrentUser.get() == null
				|| !ShipmentType.PICKUP_IN_STORE.equals(shipmentTypeInfoForCurrentUser.get().getShipmentType())
						&& shipmentTypeInfoForCurrentUser.get().getCity() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.shipmenttype.city.selected");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			if (getCheckoutFlowFacade().hasNoDeliveryAddress())
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.multi.deliveryAddress.notprovided");
				return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}
			if (!getCustomCheckoutFlowFacade().hasCity())
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.multi.deliveryAddress.city.notprovided");
				return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}

		}

		if (!getCheckoutFacade().hasShippingItems())
		{
			if (getCheckoutFlowFacade().hasPickUpItems())
			{
				return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;

			}
			if (!getCustomCheckoutFlowFacade().hasCityForDeliveryPointOfService())
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.multi.deliveryAddress.pos.city.notprovided");
				return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}
		}

		return ValidationResults.SUCCESS;
	}

}
