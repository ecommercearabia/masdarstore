/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.resolver.CustomUrlResolver;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.facade.CustomAcceleratorCheckoutFacade;
import com.masdar.facades.facade.CustomCheckoutFlowFacade;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.ValidationResults;


public class ResponsiveSummaryCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveSummaryCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "updateProfileUrlResolver")
	private CustomUrlResolver updateProfileUrlResolver;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade customAcceleratorCheckoutFacade;



	public CustomAcceleratorCheckoutFacade getCustomAcceleratorCheckoutFacade()
	{
		return customAcceleratorCheckoutFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final ValidationResults cartResult = checkCartAndDelivery(redirectAttributes);
		if (cartResult != null)
		{
			return cartResult;
		}

		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || shipmentTypeInfoForCurrentUser.get() == null
				|| !ShipmentType.PICKUP_IN_STORE.equals(shipmentTypeInfoForCurrentUser.get().getShipmentType())
						&& shipmentTypeInfoForCurrentUser.get().getCity() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.shipmenttype.city.selected");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFacade().hasShippingItems() && getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}

		if (getCheckoutFacade().hasShippingItems() && !getCustomCheckoutFlowFacade().hasCity())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.city.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;

		}

		if (!getCheckoutFacade().hasShippingItems() && !getCustomCheckoutFlowFacade().hasCityForDeliveryPointOfService())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.pos.city.notprovided");

			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		if (getCustomCheckoutFlowFacade().hasNoPaymentMode())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if ("card".equalsIgnoreCase(getCustomAcceleratorCheckoutFacade().getCheckoutCart().getPaymentMode().getCode())
				&& !getCustomCheckoutFlowFacade().hasPaymentProvider())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOGGER.error(String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					getCheckoutFacade().getCheckoutCart().getCode()));
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().getCheckoutCart().isCalculated())
		{
			LOGGER.error(String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue",
					getCheckoutFacade().getCheckoutCart().getCode()));
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}
		if (!getCustomAcceleratorCheckoutFacade().isAddressValid())
		{
			LOGGER.error(String.format("Cart %s has invalid address", getCheckoutFacade().getCheckoutCart().getCode()));
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		if (!getCustomAcceleratorCheckoutFacade().isCustomerValidForEsal())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.customerMobile.notvalid", new Object[]
					{ getUpdateProfileUrlResolver().getUrl() });
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}
		return ValidationResults.SUCCESS;
	}

	protected ValidationResults checkPaymentMethodAndPickup(final RedirectAttributes redirectAttributes)
	{
		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		final CartData cartData = getCustomAcceleratorCheckoutFacade().getCheckoutCart();

		if (!getCustomAcceleratorCheckoutFacade().hasShippingItems())
		{
			cartData.setDeliveryAddress(null);
		}

		if (!getCustomAcceleratorCheckoutFacade().hasPickUpItems() && "pickup".equals(cartData.getDeliveryMode().getCode()))
		{
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		return null;
	}

	protected ValidationResults checkCartAndDelivery(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		return null;
	}

	/**
	 * @return the updateProfileUrlResolver
	 */
	protected CustomUrlResolver getUpdateProfileUrlResolver()
	{
		return updateProfileUrlResolver;
	}


}
