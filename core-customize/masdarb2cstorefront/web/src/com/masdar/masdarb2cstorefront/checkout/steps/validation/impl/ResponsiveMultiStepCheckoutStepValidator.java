/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.checkout.steps.validation.impl;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.service.CustomUserService;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.masdar.masdarb2cstorefront.checkout.steps.validation.ValidationResults;


public class ResponsiveMultiStepCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsiveMultiStepCheckoutStepValidator.class);

	@Resource(name = "customUserService")
	private CustomUserService customUserService;


	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			return ValidationResults.SUCCESS;
		}
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = customUserService.getShipmentTypeInfoForCurrentUser();

		if (shipmentTypeInfoForCurrentUser.isEmpty() || shipmentTypeInfoForCurrentUser.get() == null
				|| !ShipmentType.PICKUP_IN_STORE.equals(shipmentTypeInfoForCurrentUser.get().getShipmentType())
						&& shipmentTypeInfoForCurrentUser.get().getCity() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.shipmenttype.city.selected");
			return ValidationResults.REDIRECT_TO_CART;
		}
		LOGGER.info("Missing, empty or unsupported cart");
		return ValidationResults.FAILED;
	}
}
