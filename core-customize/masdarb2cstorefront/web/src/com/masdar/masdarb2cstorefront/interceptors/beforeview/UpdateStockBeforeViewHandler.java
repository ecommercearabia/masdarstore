/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cstorefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.masdar.facades.facade.CustomCartFacade;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;


/**
 * Filter to load Google Maps API Keys into the model.
 *
 */
public class UpdateStockBeforeViewHandler implements BeforeViewHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateStockBeforeViewHandler.class);

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "cartFacade")
	private CustomCartFacade cartFacade;

	@Resource(name = "scpiService")
	private SCPIService scpiService;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

		if (currentBaseStore == null || !currentBaseStore.isEnableUpdateStockOnCartPage() || !isGetMethod(request)
				|| !isCartPage(request.getServletPath()))
		{
			return;
		}

		final CartModel sessionCart = getCartService().getSessionCart();
		if (sessionCart == null)
		{
			return;
		}

		try
		{
			LOG.info("Sending Cart[{}] info to UpdateSCPIInventory", sessionCart.getCode());
			getScpiService().updateSCPIInventory(sessionCart);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.info("Cart[{}] has incorrect data, cannot send data to UpdateSCPIInventory", sessionCart.getCode());
			LOG.warn(e.getMessage());
		}
		catch (final SCPIException e)
		{
			LOG.info("Fail in sending Cart[{}] info to UpdateSCPIInventory", sessionCart.getCode());
			LOG.error(e.getMessage());
		}

	}



	/**
	 * @param servletPath
	 * @return
	 */
	private boolean isCartPage(final String servletPath)
	{
		return "/cart".equalsIgnoreCase(servletPath);
	}



	protected boolean isGetMethod(final HttpServletRequest httpRequest)
	{
		return "GET".equalsIgnoreCase(httpRequest.getMethod());
	}



	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}



	public CartService getCartService()
	{
		return cartService;
	}



	public SCPIService getScpiService()
	{
		return scpiService;
	}
}
