
ACC.bankTransaction = {
	edit : function() {
		$("#editBankTransactionId").click(
				function() {
					$("#editBankTransactionIdForm").show();
				})
	}
}

$(document).ready(function() {
	$("#editBankTransactionIdForm").hide();
	with (ACC.bankTransaction) {
		edit();
	}
});
				