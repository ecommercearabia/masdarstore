ACC.shipmenttype = {

    _autoload: [
        "bindShipmentType"
    ],
    bindShipmentType: async function (SHIPMENT_TYPE, cityCode = '', POS = '') {

        if (SHIPMENT_TYPE) {
            var argCity = '&cityCode'
            if (cityCode) {
                argCity = '&cityCode=' + cityCode;

            }
            var argPos = '&posCode'
            if (POS) {

                argPos = '&posCode=' + POS;

            }
            var SHIPMENTUrl = ACC.config.encodedContextPath + '/misc/cart/shipment-type/prechange?shipmentTypeCode=' + SHIPMENT_TYPE + argCity + argPos;

            var result = ''
            await $.get(SHIPMENTUrl,
                function (returnedData) {


                    if (returnedData.data) {


                        if (returnedData.data.validChange) {

                            result = true;


                        } else {


                            var template = returnedData.data.modifications;

                            var vaildItem = '';
                            if (SHIPMENT_TYPE == 'DELIVERY') {
                                vaildItem = "<div class='subtitle'>" + ACC.shipmentPopupMsgDelivery + "</div>"
                            } else {
                                vaildItem = "<div class='subtitle'>" + ACC.shipmentPopupMsgPickup + "</div>"
                            }
                            var cartRemoved = false;
                            var cartModified = false;
                            for (var i = 0; i < template.length; i++) {
                                if (!template[i].validChange && template[i].stockLevel == 0) {
                                    cartRemoved = true
                                }

                                if (!template[i].validChange && template[i].stockLevel != 0) {
                                    cartModified = true
                                }
                            }
                            vaildItem += "<div class='cartItemDiv'>"
                            if (cartModified) {
                                vaildItem += "<div class='modification-div'>"
                                vaildItem += "<div class='subtitleitems'>" + ACC.shipmentPopupMsgmodify + "</div>"
                                vaildItem += "<ol class='mini-cart-list'>";
                                for (var i = 0; i < template.length; i++) {
                                    if (!template[i].validChange && template[i].stockLevel != 0) {
                                        var item = template[i].product;
                                        vaildItem += "<li class='mini-cart-item'>";
                                        vaildItem += "<div class='thumb'>"
                                        vaildItem += "<a href=" + item.url + ">";
                                        if (item.images) {
                                            vaildItem += "<img src=" + item.images[0].url + " />";
                                        } else {
                                            var defaultImage = ACC.config.themeResourcePath + "/images/missing_product_EN_300x300.jpg "
                                            vaildItem += "<img src=" + defaultImage + " />";
                                        }
                                        vaildItem += "</a>"
                                        vaildItem += "</div>"
                                        vaildItem += "<div class='details'>"
                                        vaildItem += "<a class='name' href=" + item.url + " >" + item.name + "</a>";
                                        vaildItem += "<div class='qty'>" + ACC.shipmentPopupqty + ":<span class='redColor'>" + template[i].stockLevel + "</span></div>";
                                        vaildItem += "</div>"
                                        vaildItem += "</li>"


                                    }

                                }
                                vaildItem += '</ol>';
                                vaildItem += "</div>"
                            }


                            if (cartRemoved) {
                                vaildItem += "<div class='removed-div'>"
                                vaildItem += "<div class='subtitleitems'>" + ACC.shipmentPopupMsgremoved + "</div>"
                                vaildItem += "<ol class='mini-cart-list'>";
                                for (var i = 0; i < template.length; i++) {
                                    if (!template[i].validChange && template[i].stockLevel == 0) {
                                        var item = template[i].product;
                                        vaildItem += "<li class='mini-cart-item'>";
                                        vaildItem += "<div class='thumb'>"
                                        vaildItem += "<a href=" + item.url + ">";
                                        if (item.images) {
                                            vaildItem += "<img src=" + item.images[0].url + " />";
                                        } else {
                                            var defaultImage = ACC.config.themeResourcePath + "/images/missing_product_EN_300x300.jpg "
                                            vaildItem += "<img src=" + defaultImage + " />";
                                        }
                                        vaildItem += "</a>"
                                        vaildItem += "</div>"
                                        vaildItem += "<div class='details'>"
                                        vaildItem += "<a class='name' href=" + item.url + " >" + item.name + "</a>";
                                        vaildItem += "<div class='qty'>" + ACC.shipmentPopupqty + ":" + template[i].entryStockLevel + "</div>";
                                        vaildItem += "</div>"
                                        vaildItem += "</li>"

                                    }

                                }
                                vaildItem += '</ol>';
                                vaildItem += "</div>"
                            }
                            vaildItem += "</div>"
                            result = vaildItem;

                        }

                    } else {

                        result = true
                    }
                })

            return result
        }
    }
}
