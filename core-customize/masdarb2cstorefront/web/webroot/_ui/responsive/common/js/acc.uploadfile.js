ACC.uploadfile = {

    _autoload: [
        "onFileChosen",
            "bindCustomProductActions"
    ],



    onFileChosen: function () {
        $(document).on('click', '.customProduct-js', function () {
            $(".customProduct-from-js").toggleClass("hidden");
            //$(this).toggleClass("hidden");
        });
        $(document).on('click', '.remove_item', function () {
            $(this).parents('form').submit();
        });
        
    },


    isSelectedFilesValid: function (selectedFiles) {
        if (window.File && window.Blob) {
            var fileMaxSize = $('.js-file-upload__input').data('max-upload-size');
            var totalSize = 0;

            for (var i = 0; i < selectedFiles.files.length; ++i){
                totalSize += selectedFiles.files[i].size;
                
            }

            if ($.isNumeric(fileMaxSize) && totalSize > parseFloat(fileMaxSize)) {
						$("#filesError").show();
	         return false;
            }
else{$("#filesError").hide();}
        }

        return true;
    },

    displayCustomerTicketingAlert: function (options) {
        var alertTemplateSelector;

        switch (options.type) {
            case 'error':
                alertTemplateSelector = '#global-alert-danger-template';
                break;
            case 'warning':
                alertTemplateSelector = '#global-alert-warning-template';
                break;
            default:
                alertTemplateSelector = '#global-alert-info-template';
        }
        
        
        if (typeof options.message !== 'undefined') {
            $('#upload-alerts').append($(alertTemplateSelector).tmpl({message: options.message}));
        }

        if (typeof options.messageId !== 'undefined') {
            $('#upload-alerts').append($(alertTemplateSelector).tmpl({message: $('#' + options.messageId).text()}));
        }
    },

    displayGlobalAlert: function (options) {
        var alertTemplateSelector;

        switch (options.type) {
            case 'error':
                alertTemplateSelector = '#global-alert-danger-template';
                break;
            case 'warning':
                alertTemplateSelector = '#global-alert-warning-template';
                break;
            default:
                alertTemplateSelector = '#global-alert-info-template';
        }

        if (typeof options.message !== 'undefined' && options.message !=null) {
        
            $('#upload-alerts').append($(alertTemplateSelector).tmpl({message: options.message}));
        }

        if (typeof options.messageId !== 'undefined') {
            $('#upload-alerts').append($(alertTemplateSelector).tmpl({message: $('#' + options.messageId).text()}));
        }
    },

bindCustomProductActions: function () {
        
        $(document).on('click', '#choosePaymentType_continue_button',
            function (event) {
//           $('.help-block').html('');
//           $("#registerUser .row").removeClass('has-error');
//           $("#registerUser .col-md-4").removeClass('has-error');
             
            
                event.preventDefault();

                ACC.uploadfile.formPostCustomProductAction();
            });
    },
    


    formPostCustomProductAction: function () {
//        ACC.uploadfile.clearAlerts();
        var customProductForm = document.getElementById("selectPaymentTypeForm");
        var formData = new window.FormData(customProductForm);

        var selectedFile = document.getElementById('attachmentFiles');
        
        if (!ACC.uploadfile.isSelectedFilesValid(selectedFile)) {
            ACC.uploadfile.displayCustomerTicketingAlert({
                type: 'error',
                messageId: 'attachment-file-max-size-exceeded-error-message'
            });
            return;
        }
//        customProductForm.submit();
        
        $.ajax({
            url: customProductForm.action,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data, textStatus, jqXHR) {
			var linkurl = data.replace('redirect:','');
            window.location.replace(ACC.config.encodedContextPath+linkurl);
                
               
            },
            error: function (jqXHR) {
			console.log(jqXHR)
            ACC.uploadfile.processErrorResponse(jqXHR);
                

            }
        });
    },

    formPostAction: function (successRedirectUrl) {

        ACC.uploadfile.clearAlerts();
        var form = document.getElementById("selectPaymentTypeForm");
        var formData = new window.FormData(form);

        var selectedFile = document.getElementById('attachmentFiles');
        if (!ACC.uploadfile.isSelectedFilesValid(selectedFile)) {
            ACC.uploadfile.displayCustomerTicketingAlert({
                type: 'error',
                messageId: 'attachment-file-max-size-exceeded-error-message'
            });
            return;
        }
       
        $.ajax({
            url: form.action,
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (url) {
                
                if(url == '/')
                    {
                     window.location.replace(ACC.config.encodedContextPath);
                    }else
                        {
                         window.location.replace(url);
                        }
                
               
            },
            error: function (jqXHR) {
                ACC.uploadfile.processErrorResponse(jqXHR);

            }
        });
    },

    processErrorResponse: function (jqXHR) {
        ACC.uploadfile.clearAlerts();
        if (jqXHR.status === 400 && jqXHR.responseJSON) {

            $.each(jqXHR.responseJSON, function() {
                $.each(this, function(k, v) {
                    var target = '#' + k;
                    if (k === 'form-global-error' ) {
                        ACC.uploadfile.displayGlobalAlert({type: 'error', message: v});
                    }
                    else {
                            
                        ACC.uploadfile.addHasErrorClass(target,v);
                        
                    }
                   
                   
                });
            });

            return;
        }

        ACC.uploadfile.displayCustomerTicketingAlert({type: 'error', messageId: 'registerform-tryLater'});
    },

    addHasErrorClass: function (target,v) {
         var id = target;
         $(id).show();
        
        $(id).parents(".form-group").addClass('has-error');
		$(id).parents(".form-group").find(".help-block").remove();
 		$(id).parents(".form-group").append('<div class="help-block"><span>'+v+'</span></div>');

 $('#upload-alerts').removeClass('hidden');

    },

    clearAlerts: function () {
        $('#global-alerts').empty();
		$('#upload-alerts').empty();
        $(".form-group").removeClass('has-error');
		$('.has-error .help-block').remove();
        $("#nameError").empty().hide();
        $("#descriptionError").empty().hide();
        
        
    }
};
