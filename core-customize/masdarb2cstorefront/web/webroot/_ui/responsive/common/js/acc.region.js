ACC.region = {
	getCitiesByRegionId : function() {
		$("#regionId").change(
				function() {
					var regionId = $(this).val();
  			        var FormPage;
      if($('.page-multiStepCheckoutSummaryPage').length){
         FormPage= true;
      }else{
         FormPage=false;
         
         }

					
					$.ajax({
						type : "GET",
						url : ACC.config.encodedContextPath + "/misc/region/" + regionId + "/cities",
						data: { 'isCheckout':FormPage },
						dataType : "json",
						success : function(response) {
							//console.log(response)
		                    var emptyListLBL = $("#cityId option:first").html();
		                    var options = '';
		                    $('select#cityId').html('');
	                        var emptyOption = '<option value="" disabled="disabled" selected="selected" class="__web-inspector-hide-shortcut__">' + emptyListLBL + '</option>';
	                        $('select#cityId').append(emptyOption);
	                        for (i = 0; i < response.data.length; i++) {
	                            var code = response.data[i].code;
	                            var name = response.data[i].name;
								var lng = response.data[i].longitude;
								var lat = response.data[i].latitude;
	                            options += '<option value="' + code + '" data-lat="' + lat + '" data-lng="' + lng + '">' + name + '</option>';
	                        }
							console.log( $('select#cityId').append(options))

	                        $('select#cityId').selectpicker('refresh');
							$('select').selectpicker('refresh');
						}
				});
					
					
					
// 					$.ajax({
// 						type : "GET",
// 						url : ACC.config.encodedContextPath + "/misc/points-of-services/SA/" + regionId,
// 						dataType : "json",
// 						success : function(data) {
//
//
//
// 		                    var emptyListLBL = $("#posID option:first").html();
// 		                    var options = '';
// 		                    $('#posID').html('');
// //	                        var emptyOption = '<option value="" disabled="disabled" selected="selected" class="__web-inspector-hide-shortcut__">' + emptyListLBL + '</option>';
// //	                        $('#posID').append(emptyOption);
// 	                        for (i = 0; i < data.length; i++) {
// 	                            var code = data[i].name;
// 	                            var name = data[i].displayName;
// 	                           // console.log('name -->',name,'     ' , 'code -->',code)
// 	                            options += '<option value="' + code + '">' + name + '</option>';
// 	                        }
// 	                        $('#posID').append(options);
// 	                        $('#posID').selectpicker('refresh');
// 						}
// 				});
					
					
					
					
					
					
					
					
	});
		
		
}}

$(document).ready(function() {
	with (ACC.region) {
		getCitiesByRegionId();

	}
});
