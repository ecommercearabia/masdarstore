<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="cartPage" required="true" type="java.lang.Boolean" %>
<%@ attribute name="entryNumber" required="false" type="java.lang.Long" %>
<%@ attribute name="deliveryPointOfService" required="false" type="java.lang.String" %>
<%@ attribute name="quantity" required="false" type="java.lang.Integer" %>
<%@ attribute name="searchResultsPage" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="defaultUrl" value="/store-pickup/${ycommerce:encodeUrl(product.code)}/pointOfServices"/>
<c:url var="pickUpInStoreFormAction" value="${empty actionUrl ? defaultUrl : actionUrl}"/>
<c:set var="productPrimaryImageHtml">
    <product:productPrimaryImage product="${product}" format="thumbnail"/>
</c:set>
<c:set var="variantsJSON">
    {
    <c:if test="${not empty product.baseOptions[0].selected.variantOptionQualifiers}">
        <c:forEach var="variant" items="${product.baseOptions[0].selected.variantOptionQualifiers}"
                   varStatus="productcartVariantsNumber">
            <c:if test="${not empty variant.value}">
                <c:set var="variantValue"><spring:theme code="basket.pickup.product.variant"
                                                        arguments="${variant.name},${variant.value}"
                                                        htmlEscape="false"/></c:set>
                "${ycommerce:encodeJSON(variant.name)}":"${ycommerce:encodeJSON(variantValue)}"<c:if
                    test="${!productcartVariantsNumber.last}">,</c:if>
            </c:if>
        </c:forEach>
    </c:if>
    }
</c:set>

<div class="pos-relative">
    <div class="header_section">
        <div class="header-div-pickup"><span class="icon-header-pickup"><i
                class="far fa-store-alt"></i></span><spring:theme code="checkout.multi.pickupInStore"/></div>
        <div class="description-div-pickup">

            <b>
                <c:if test="${not empty shipmentTypeInfo}">
                    <c:if test="${not empty shipmentTypeInfo.selectedCity }">
                        ${shipmentTypeInfo.selectedCity.name }
                    </c:if>
                    -
                    <c:choose>
	                    <c:when test="${not empty shipmentTypeInfo.selectedPointOfService }">
	                    	${shipmentTypeInfo.selectedPointOfService.displayName }
	                    </c:when>
	                    <c:otherwise>
							<spring:theme code="empty.selected.pickup"/>
	                    </c:otherwise>
                    </c:choose>
                </c:if>
            </b>
            
            
           <span class='resultStock' style='margin-left: 5px'>
           <c:choose>
	            <c:when test="${isAvailablePOS }">
	            	<i class="far fa-check-circle"></i> <spring:theme code='product.variants.in.stock'/>
	            </c:when>
	            <c:otherwise>
					<i class="far fa-times-circle"></i> <spring:theme code='product.variants.out.of.stock'/>
	            </c:otherwise>
       	   </c:choose>

        </div>
        <c:if test="${isAvailablePOS }"><div class="massage-div-pickup"><spring:theme code="pickup.collect.time.message"/></div></c:if>
    </div>
    <ycommerce:testId code="pickupInStoreButton">
        <c:choose>
            <c:when test="${cartPage}">
                <a href="javascript:void(0)"
                   class="js-pickup-in-store-button"
                   id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}" disabled="disabled"
                   data-productcart='${fn:escapeXml(product.price.formattedValue)}'
                   data-productcart-variants='${fn:escapeXml(variantsJSON)}'
                   data-img-html="${fn:escapeXml(productPrimaryImageHtml) }"
                   data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"
                   data-cartpage="${fn:escapeXml(cartPage)}"
                   data-entryNumber="${fn:escapeXml(entryNumber)}"
                   data-actionurl="${fn:escapeXml(pickUpInStoreFormAction)}"
                   data-value="${fn:escapeXml(quantity)}">
                    <c:choose>
                        <c:when test="${not empty deliveryPointOfService}">
                            <spring:theme code="basket.page.shipping.change.store"/>
                        </c:when>
                        <c:otherwise>
                            <spring:theme code="basket.page.shipping.find.store"/>
                        </c:otherwise>
                    </c:choose>
                </a>
            </c:when>
            <c:when test="${searchResultsPage}">
                <button class="js-pickup-in-store-button "
                        disabled="disabled" id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}"
                        type="button submit"
                        data-productcart='${fn:escapeXml(product.price.formattedValue)}'
                        data-productcart-variants='${fn:escapeXml(variantsJSON)}'
                        data-img-html="${fn:escapeXml(productPrimaryImageHtml)}"
                        data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"
                        data-cartpage="false" data-entryNumber="0"
                        data-actionurl="${fn:escapeXml(pickUpInStoreFormAction)}" data-value="1">
                </button>
            </c:when>
            <c:otherwise>
                <button class=" js-pickup-in-store-button "
                        disabled="disabled" id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}"
                        type="submit" data-productavailable="${fn:escapeXml(product.availableForPickup)}"
                        data-productcart='${fn:escapeXml(product.price.formattedValue)}'
                        data-productcart-variants='${fn:escapeXml(variantsJSON)}'
                        data-img-html="${fn:escapeXml(productPrimaryImageHtml)}"
                        data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"
                        data-cartpage="false" data-entryNumber="0"
                        data-actionurl="${fn:escapeXml(pickUpInStoreFormAction)}" data-value="1">
                    <c:choose>
                        <c:when test="${not empty deliveryPointOfService}">
                            <spring:theme code="basket.page.shipping.change.store"/>
                        </c:when>
                        <c:otherwise>
                            <spring:theme code="basket.page.shipping.find.store"/>
                        </c:otherwise>
                    </c:choose>
                </button>
            </c:otherwise>
        </c:choose>
    </ycommerce:testId>

</div>
