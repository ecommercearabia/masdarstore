<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:forEach items="${cartData.pickupOrderGroups}" var="groupData" varStatus="status">

    <div class="checkout-shipping-items row">
        <div class="col-xs-12  checkout-pickup-items">
            <div class="checkout-shipping-items-header">
                <spring:theme code="checkout.multi.pickup.items"
                              arguments="${status.index + 1},${fn:length(groupData.entries)}"
                              text="Pick Up # ${status.index + 1} - ${fn:length(groupData.entries)} Item(s)">
                </spring:theme>
            </div>

            <ul>
                <c:forEach items="${groupData.entries}" var="entry">
                    <li class="row">
                        <span class="name col-xs-8">${fn:escapeXml(entry.product.name)}</span>
                        <span class="qty col-xs-4"><spring:theme
                                code="basket.page.qty"/>&nbsp;${fn:escapeXml(entry.quantity)}</span>
                    </li>
                </c:forEach>
            </ul>
        </div>

        <div class="col-xs-12">
            <div class="checkout-shipping-items-header">
                <spring:theme code="checkout.multi.inStore"/>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="flex-map-location"><span class="mapSvg"></span>
                        <strong>${fn:escapeXml(groupData.deliveryPointOfService.name)}</strong>
                    </div>
                    <br>
                    <c:if test="${ not empty groupData.deliveryPointOfService.address.line1 }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.line1)},&nbsp;
                    </c:if>
                    <c:if test="${ not empty groupData.deliveryPointOfService.address.line2 }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.line2)},&nbsp;
                    </c:if>
                    <c:if test="${not empty groupData.deliveryPointOfService.address.town }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.town)},&nbsp;
                    </c:if>
                    <c:if test="${ not empty groupData.deliveryPointOfService.address.region.name }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.region.name)},&nbsp;
                    </c:if>
                    <c:if test="${ not empty groupData.deliveryPointOfService.address.postalCode }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.postalCode)},&nbsp;
                    </c:if>
                    <c:if test="${ not empty groupData.deliveryPointOfService.address.country.name }">
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.country.name)}

                    </c:if>
                </div>
                <div class="col-xs-4">

                    <storepickup:clickPickupInStore product="${cartData.entries[0].product}"
                                                    deliveryPointOfService="${cartData.deliveryPointOfService}"
                                                    cartPage="false"/>
                    <storepickup:pickupStorePopup/>
                </div>
            </div>
            <br/>
            <c:if test="${ not empty groupData.deliveryPointOfService.address.phone }">
                <div class="phone_num"><i
                        class="fas fa-phone-alt"></i>
                        ${fn:escapeXml(groupData.deliveryPointOfService.address.phone)}
                </div>
            </c:if>
            <br/>

            <c:if test="${not empty groupData.deliveryPointOfService.openingHours.weekDayOpeningList}">
                <div class="openingHours">
                    <i class="fal fa-clock"></i> <spring:theme code="opening.hours.weekday" text="working hours"/>

                    <c:forEach items="${groupData.deliveryPointOfService.openingHours.weekDayOpeningList}"
                               var="weekDayOpening">

                        <dl class="dl-horizontal js-store-openings">
                            <dt>${weekDayOpening.weekDay} </dt>
                            <dd>
                                <c:choose>
                                    <c:when test="${weekDayOpening.closed}"><spring:theme code="opening.hours.closed"
                                                                                          text="closed"/></c:when>
                                    <c:otherwise>
                                        ${weekDayOpening.openingTime.formattedHour} - ${weekDayOpening.closingTime.formattedHour}
                                    </c:otherwise>

                                </c:choose>
                            </dd>

                        </dl>

                    </c:forEach>
                </div>
            </c:if>
            <c:if test="${not empty groupData.deliveryPointOfService.openingHours.specialDayOpeningList}">
                <spring:theme code="opening.hours.specialday" text="Special Day"/>
                <c:forEach items="${groupData.deliveryPointOfService.openingHours.specialDayOpeningList}"
                           var="specialDayOpening">
                    <dl class="dl-horizontal js-store-openings">
                        <dt>${specialDayOpening.weekDay} </dt>
                        <dd>
                            <c:choose>
                                <c:when test="${specialDayOpening.closed}"><spring:theme code="opening.hours.closed"
                                                                                         text="closed"/></c:when>
                                <c:otherwise>
                                    ${specialDayOpening.openingTime.formattedHour} - ${specialDayOpening.closingTime.formattedHour}
                                </c:otherwise>

                            </c:choose>
                        </dd>
                    </dl>
                </c:forEach>
            </c:if>
        </div>
    </div>
</c:forEach>
