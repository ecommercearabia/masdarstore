<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="timeSlot" required="true" type="com.masdar.masdartimeslotfacades.TimeSlotData" %>
<%@ attribute name="selectedSlot" required="false" type="com.masdar.masdartimeslotfacades.TimeSlotInfoData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
 	<div class="carousel__component"> 
 	<div class="carousel__component--carousel owl-carousel js-owl-carousel js-owl-slot-time owl-theme">
<c:forEach items="${timeSlot.timeSlotDays}" var="day">


		<multi-checkout:timeSlotDetails day="${day}" selectedSlot="${selectedSlot}"/>
	
	</c:forEach>
	
	
	</div></div>
<!-- 	<div class="carousel__component"> -->
<!-- 	<div class="carousel__component--carousel owl-carousel js-owl-carousel js-owl-slot-time"> -->
<%-- 	<c:forEach items="${timeSlot.timeSlotDays}" var="day"> --%>


<%-- 		<multi-checkout:timeSlotDetails day="${day}" selectedSlot="${selectedSlot}"/> --%>
<%-- 		<span class="day">${day.day}</span> --%>
<%-- 		<span class="date">${day.date}</span> --%>
	
<%-- 	</c:forEach>	 --%>

