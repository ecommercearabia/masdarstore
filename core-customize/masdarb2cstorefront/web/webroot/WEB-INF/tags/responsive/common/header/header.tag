<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="header"
           tagdir="/WEB-INF/tags/responsive/common/header" %>

<spring:theme code="label.incVat" var="incVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.Vat" var="VatLabel"/>
<spring:htmlEscape defaultHtmlEscape="true"/>

<input type="hidden" id="VAT" value="${incTax}"/>
<cms:pageSlot position="TopHeaderSlot" var="component" element="div">
    <cms:component component="${component}"/>
</cms:pageSlot>
<cms:pageSlot position="LastOrderQuote" var="component" element="ul"
              class="topsection hidden-sm hidden-xs">
    <cms:component component="${component}"/>
</cms:pageSlot>

<header class="js-mainHeader">

    <div class="darkback hidden"></div>
    <nav class="top_nav ">
        <div class="row align-row">
            <div class="col-lg-2 col-md-2 col-xs-12 ">
                <div class="row">
                    <div class="col-sm-2 col-xs-3 hidden-md hidden-lg">
                        <button
                                class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation"
                                type="button">
                            <i class="far fa-bars"></i>
                        </button>
                    </div>

                    <div class="col-sm-8 col-xs-6 col-md-12 SiteLogo">
                        <cms:pageSlot position="SiteLogo" var="logo" limit="1">
                            <cms:component component="${logo}" element="div"
                                           class="yComponentWrapper"/>
                        </cms:pageSlot>
                    </div>
                    <div class="col-sm-2 col-xs-3 cartmobile hidden-md hidden-lg">
                        <cms:pageSlot position="MiniCart" var="cart" element="div"
                                      class="miniCartSlot componentContainer">
                            <cms:component component="${cart}"/>
                        </cms:pageSlot>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-7 hidden-xs hidden-sm med-section">
                <div class="top-section">
                    <ul class="topnavlinks">
                        <c:if test="${empty hideHeaderLinks}">
                            <c:if test="${uiExperienceOverride}">
                                <li><c:url value="/_s/ui-experience?level="
                                           var="backToMobileStoreUrl"/> <a
                                        href="${fn:escapeXml(backToMobileStoreUrl)}"> <spring:theme
                                        code="text.backToMobileStore"/>
                                </a></li>
                            </c:if>
                            <header:languageSelector languages="${languages}"
                                                     currentLanguage="${currentLanguage}"/>
                            <c:if test="${empty hideHeaderLinks}">
                                <ycommerce:testId code="header_StoreFinder_link">
                                    <li><c:url value="/store-finder" var="storeFinderUrl"/> <a
                                            href="${fn:escapeXml(storeFinderUrl)}"> <spring:theme
                                            code="branches.locator" text="branches locator"/>
                                    </a></li>
                                </ycommerce:testId>
                            </c:if>

                            <c:if test="${not empty cmsSite.contactUsNumber}">
                                <li><spring:theme
                                        code="text.contactus" arguments="${cmsSite.contactUsNumber}"/>
                                </li>
                            </c:if>
							<li>
								<a href="${siteBaseUrl}"><spring:theme code="header.link.business.site"/></a>
                            </li>

                        </c:if>


                    </ul>
                    <div class="swithchTax">
                        <span class="labelVat"> ${VatLabel} </span> ${exVatLabel}

                        <c:choose>
                            <c:when test="${incTax}">
                                <label class="switch"> <input class="Vat_switch"
                                                              type="checkbox" checked> <span
                                        class="slider round"></span>
                                </label>

                            </c:when>
                            <c:otherwise>
                                <label class="switch"> <input class="Vat_switch"
                                                              type="checkbox"> <span class="slider round"></span>
                                </label>
                            </c:otherwise>
                        </c:choose>
                        ${incVatLabel}
                    </div>

                </div>
                <div class="search-section">
                    <cms:pageSlot position="SearchBox" var="component">
                        <cms:component component="${component}" element="div"/>
                    </cms:pageSlot>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 hidden-sm hidden-xs space_header">
                <ul class="topnavlinks">
                    <c:if test="${empty hideHeaderLinks}">

                        <c:url var="b2bLoginUrl" value="${siteBaseUrl}/login"/>
                        <c:url var="b2bRegisterUrl" value="${siteBaseUrl}/register"/>
                        <c:url var="b2cLoginUrl" value="/login"/>
                        <c:url var="b2cRegisterUrl" value="/register"/>
                        <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">

                            <ycommerce:testId code="header_Login_link">
                                <li class="liOffcanvas registration"><c:url value="/login"
                                                                            var="loginUrl"/> <a
                                        class="login_action_tap"> <span
                                        class="fas fa-user"></span> <spring:theme
                                        code="header.link.login"/>
                                </a>

                                    <a class="login_action_mobile hidden"
                                       href="javaScript:;"> <span class="fas fa-user"></span>
                                        <spring:theme code="header.link.login"/>
                                    </a>

                                    <ul class="taplogInreqister">
                                        <li>
                                            <a href="${b2bLoginUrl}">
                                                <spring:theme code="header.link.login.b2b"/>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="${b2cLoginUrl}">
                                                <spring:theme code="header.link.login.b2c"/>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="${b2bRegisterUrl}">
                                                <spring:theme code="header.link.register.b2b"/>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="${b2cRegisterUrl}">
                                                <spring:theme code="header.link.register.b2c"/>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                            </ycommerce:testId>
                            <div class="login_box hidden">
                                <cms:pageSlot position="LoginPopup" var="component" limit="1">
                                    <cms:component component="${component}"/>
                                </cms:pageSlot>
                            </div>
                        </sec:authorize>

                        <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                            <cms:pageSlot position="HeaderLinks" var="link" element="li"
                                          class="secondary">
                                <cms:component component="${link}"/>
                            </cms:pageSlot>
                        </sec:authorize>

                    </c:if>


                    <cms:pageSlot position="MiniCart" var="cart" element="li"
                                  class="miniCartSlot componentContainer">
                        <cms:component component="${cart}"/>
                    </cms:pageSlot>
                </ul>

                <cms:pageSlot position="CustomerCity" var="feature" element="div"
                              class="selectCityDiv">
                    <cms:component component="${feature}" element="div"
                                   class="boxSelectCity"/>
                </cms:pageSlot>

            </div>
        </div>
    </nav>
    <div class="row hidden-lg hidden-md padd_space">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2   ">
            <cms:pageSlot position="CustomerCity" var="feature" element="div"
                          class="selectCityDiv">
                <cms:component component="${feature}" element="div"
                               class="boxSelectCity"/>
            </cms:pageSlot>

        </div>
    </div>
    <nav class="menu_search">


        <div class="search-section hidden-md hidden-lg">
            <div class="loginmobileDiv hidden">
                <ul class="taplogInreqister">
                    <li>
                        <a href="${b2bLoginUrl}">
                            <spring:theme code="header.link.login.b2b"/>
                        </a>
                    </li>

                    <li>
                        <a href="${b2cLoginUrl}">
                            <spring:theme code="header.link.login.b2c"/>
                        </a>
                    </li>

                    <li>
                        <a href="${b2bRegisterUrl}">
                            <spring:theme code="header.link.register.b2b"/>
                        </a>
                    </li>

                    <li>
                        <a href="${b2cRegisterUrl}">
                            <spring:theme code="header.link.register.b2c"/>
                        </a>
                    </li>
                </ul>
            </div>
            <cms:pageSlot position="SearchBox" var="component">
                <cms:component component="${component}" element="div"/>
            </cms:pageSlot>
        </div>


        <nav:topNavigation/>

        </div>
    </nav>
    <a id="skiptonavigation"></a>


</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"
              class="container-fluid">
    <cms:component component="${component}"/>
</cms:pageSlot>
<cms:pageSlot position="Breadcrumb" var="component" element="div"
              class="container-fluid">
    <cms:component component="${component}"/>
</cms:pageSlot>

