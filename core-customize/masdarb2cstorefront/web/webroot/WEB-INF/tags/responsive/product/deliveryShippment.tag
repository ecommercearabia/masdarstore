<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="pos-relative">

    <div class="header-div-pickup">

        <span class="icon-header-pickup"><i class="far fa-truck"></i></span>
        <spring:theme code="home.de" />

    </div>
    <div class="description-div-pickup">
        <c:choose >
        	<c:when test="${ not empty selectedCustomerLocationCity.name}">
	            <b>${selectedCustomerLocationCity.name}</b>
		        <span class="resultStock">
		        <c:choose>
		            <c:when test="${product.selectedLocationStock.stockLevelStatus.code eq 'inStock' }">
		                <i class="far fa-check-circle"></i> <spring:theme code='product.variants.in.stock'/>
		            </c:when>
		            <c:otherwise>
		                <i class="far fa-times-circle"></i> <spring:theme code='product.variants.out.of.stock'/>
		            </c:otherwise>
		        </c:choose>
		        </span>
        	</c:when>
        	<c:otherwise>
        		<span class="resultStock">
	        		<c:choose>
			            <c:when test="${product.stock.stockLevelStatus.code eq 'inStock' }">
			                <i class="far fa-check-circle"></i> <spring:theme code='product.variants.in.stock'/>
			            </c:when>
			            <c:otherwise>
			                <i class="far fa-times-circle"></i> <spring:theme code='product.variants.out.of.stock'/>
			            </c:otherwise>
			        </c:choose>
		        </span>
        	</c:otherwise>
        </c:choose>
    </div>

    <%--    <c:url var="addToCartToPickupInStoreUrl" value="/store-delivery/select/city"/>--%>
    <%--    <spring:theme code="product.delivery.to"/> ${selectedCustomerLocationCity.name}--%>
    <%--<form:form action="${addToCartToPickupInStoreUrl}" id="cityForm" method="post">--%>
    <%--<input value="AL_AL_JILDAH" type="text" id="cityId" name="cityId" />--%>
    <%--  <button type="submit">ssss</button>--%>
    <%--</form:form>--%>
    <%--<select>--%>
    <%--	<c:forEach items="${customerLocationCities}" var="city">--%>
    <%--		<option value="${fn:escapeXml(city.code)}" label="${fn:escapeXml(city.name)}">${fn:escapeXml(city.name)}</option>--%>
    <%--	</c:forEach>--%>
    <%--</select>--%>


    <c:if test="${not empty cmsSite.deliveryTagMsg }">
        ${cmsSite.deliveryTagMsg}

    </c:if>
    <br/>



    <c:if test="${not empty deliveryMode}">
        <spring:theme code="product.delivery.shipping"/>    ${deliveryMode.description}

    </c:if>

<button class="CityClickBtn hidden-xs hidden-sm">
    <spring:theme code="change.city" />

</button>


    <button class="CityClickBtn hidden-md hidden-lg">
        <spring:theme code="change.city" />

    </button>
</div>
