<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
<div class="product-classifications col-lg-10 col-lg-offset-1 col-md-12">
	<c:if test="${not empty product.classifications}">
		<c:forEach items="${product.classifications}" var="classification">
			<c:if test="${not classification.hiddenPDP}">
						<div class="row">
						<div class="carousel__component--headline col-md-12 col-sm-12 col-xs-12">${fn:escapeXml(classification.name)}</div>
						
						<c:forEach items="${classification.features}" var="feature">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="row">
								<div class="attrib col-xs-4 col-md-3"><b>${fn:escapeXml(feature.name)}</b></div>
								<div class="value col-xs-8 col-md-9">
									<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
										<b>${fn:escapeXml(value.value)}
										<c:choose>
											<c:when test="${feature.range}">
												${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
											</c:when>
											<c:otherwise>
												${fn:escapeXml(feature.featureUnit.symbol)}
												${not status.last ? '<br/>' : ''}
											</c:otherwise>
										</c:choose>
										</b>
									</c:forEach>
								</div>
							</div>
							</div>
						</c:forEach>
						</div>
						</c:if>
		</c:forEach>
	</c:if>
</div>
</div>