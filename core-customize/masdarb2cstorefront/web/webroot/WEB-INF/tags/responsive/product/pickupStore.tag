<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>

<c:if test="${empty showAddToCart ? ycommerce:checkIfPickupEnabledForStore() : showAddToCart and ycommerce:checkIfPickupEnabledForStore() and product.enabledForPickup}">
	<c:set var="actionUrl" value="${fn:replace(url,'{productCode}', ycommerce:encodeUrl(product.code))}" scope="request"/>
	<div>
	<storepickup:clickPickupInStore product="${product}" cartPage="false"/>
	<storepickup:pickupStorePopup/>
	<c:remove var="actionUrl"/>
	</div>
</c:if>


