<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${product.url}/reviewhtml/3" var="getPageOfReviewsUrl"/>
<c:url value="${product.url}/reviewhtml/all" var="getAllReviewsUrl"/>
<c:url value="${product.url}/review" var="productReviewActionUrl"/>
<div class="carousel__component--headline"><spring:theme code="review.reviews" /></div>
<div class="tab-review">
	<div class="row">
	<div class=" col-lg-10 col-lg-offset-1 col-md-12">

	<div class="write-review js-review-write">
		<form:form method="post" action="${productReviewActionUrl}" modelAttribute="reviewForm">
			<div class="form-group text-center">
			
				


				<div class="rating rating-set js-ratingCalcSet">
					<div class="rating-stars js-writeReviewStars">
                        <c:forEach  begin="1" end="10" varStatus="loop">
                            <span class="js-ratingIcon glyphicon glyphicon-star ${loop.index % 2 == 0 ? 'lh' : 'fh'}"></span>
                        </c:forEach>
					</div>
				</div>
				<label><spring:theme code="review.rating"/></label>

				<formElement:formInputBox idKey="reviewrating" labelKey="review.rating" path="rating" inputCSS="sr-only js-ratingSetInput" labelCSS="sr-only" mandatory="true"/>
	
				
			</div>
			<div class="form-group">
			<formElement:formInputBox idKey="alias" labelKey="review.alias" path="alias" inputCSS="form-control" mandatory="false"/>
			</div>
			<div class="form-group">
				<formElement:formInputBox idKey="reviewheadline" labelKey="review.headline" path="headline" inputCSS="form-control" mandatory="true"/>
			</div>
			<div class="form-group">
				<formElement:formTextArea idKey="reviewcomment" labelKey="review.comment" path="comment" areaCSS="form-control" mandatory="true"/>
			</div>
			
			

			<button type="submit" class="btn btn-primary" value="<spring:theme code="review.submit"/>"><spring:theme code="review.submit"/></button>
		</form:form>

	</div>

	<ul id="reviews" class="review-list" data-reviews="${fn:escapeXml(getPageOfReviewsUrl)}"  data-allreviews="${fn:escapeXml(getAllReviewsUrl)}"></ul>

	<div class="review-pagination-bar text-center">
		<button class="btn btn-default js-writeReviewTab" data-head="<spring:theme code='review.write.title'/>"><spring:theme code="review.write.title"/></button>

		<div class="right hidden">
			<button class="btn btn-default all-reviews-btn"><spring:theme code="review.show.all" /></button>
			<button class="btn btn-default less-reviews-btn"><spring:theme code="review.show.less" /></button>
		</div>
	</div>

	
	<div class="review-pagination-bar">

		<div class="right hidden">
			<button class="btn btn-default all-reviews-btn"><spring:theme code="review.show.all" /></button>
			<button class="btn btn-default less-reviews-btn"><spring:theme code="review.show.less" /></button>
		</div>
	</div>
</div>
</div>
</div>
