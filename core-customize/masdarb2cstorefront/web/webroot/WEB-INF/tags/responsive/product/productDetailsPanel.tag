<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row align-row-pdp">
    <div class="col-xs-12 col-sm-6 col-md-6 algin-pdp">
        <div class="box m-t-20 relativepos">
            <c:if test="${not empty product.productLabels}">

                <div class="productlabel">
                    <c:forEach items="${product.productLabels}" var="label">
                        <c:choose>
                            <c:when test="${fn:escapeXml(currentLanguage.isocode) eq 'ar'}">


                                <c:if test="${fn:length(label) eq 4}"><span class="new">${label}</span></c:if>
                                <c:if test="${fn:length(label) eq 5}"><span class="sale">${label}</span></c:if>
                                <c:if test="${fn:length(label) eq 7}"><span class="recommended">${label}</span></c:if>
                                <c:if test="${(fn:length(label) ne 4) && (fn:length(label) ne 5) && (fn:length(label) ne 7)  }"><span
                                >${label}</span></c:if>

                            </c:when>
                            <c:otherwise>

                                <c:set var="ClassName" value="${fn:replace(fn:toLowerCase(fn:trim(label)),' ', '')}"/>

                                <span class="${ClassName}">${label}</span>

                            </c:otherwise>
                        </c:choose>


                    </c:forEach>
                </div>

            </c:if>
            <product:productImagePanel galleryImages="${galleryImages}"/>
        </div>
    </div>
    <div class="clearfix hidden-sm hidden-md hidden-lg"></div>
    <div class="col-xs-12 col-sm-6 col-md-6 algin-pdp">
        <div class="product-main-info box m-t-20">


            <div class="product-details">


                <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                    <div class="name">${fn:escapeXml(product.name)}</div>
                    <span class="sku">SKU</span><span class="code">${fn:escapeXml(product.code)}</span>
                </ycommerce:testId>
                <product:productReviewSummary product="${product}" showLinks="true"/>

                <product:productPromotionSection product="${product}"/>


                <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                    <product:productPricePanel product="${product}"/>
                </ycommerce:testId>

            </div>


            <cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
                <cms:component component="${component}" element="div"
                               class="yComponentWrapper page-details-variants-select-component"/>
            </cms:pageSlot>
            <cms:pageSlot position="AddToCart" var="component" element="div"
                          class="page-details-variants-select">
                <cms:component component="${component}" element="div"
                               class="yComponentWrapper page-details-add-to-cart-component"/>
            </cms:pageSlot>


        </div>


    </div>
</div>
<div class="row StoreSection">

    <div class="col-md-6 col-xs-12 colStore">
        <div class="box">
            <product:deliveryShippment/>

        </div>
    </div>
    <c:if test="${empty showAddToCart ? ycommerce:checkIfPickupEnabledForStore() : showAddToCart and ycommerce:checkIfPickupEnabledForStore() and product.enabledForPickup}">
        <div class="col-md-6 col-xs-12 colStore">
            <div class="box">
                <product:pickupStore/>
            </div>
        </div>
    </c:if>

</div>
