<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<div hidden="hidden">
    <c:forEach items="${product.productLabels}" var="label">
        ${label}
    </c:forEach>

</div>

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<c:set value="product-item" var="productTagClasses"/>
<c:forEach var="tag" items="${product.tags}">
    <c:set value="${productTagClasses} tag-${tag}" var="productTagClasses"/>
</c:forEach>

<div class="${fn:escapeXml(productTagClasses)}">
    <div class="boxin">
        <%-- 	${product.productLabel} --%>

        <ycommerce:testId code="product_wholeProduct">
        <a class="thumb relativepos" href="${fn:escapeXml(productUrl)}" title="${fn:escapeXml(product.name)}">
            <c:if test="${not empty product.productLabels}">

                <div class="productlabel">
                    <c:forEach items="${product.productLabels}" var="label">
                        <c:choose>
                            <c:when test="${fn:escapeXml(currentLanguage.isocode) eq 'ar'}">


                                <c:if test="${fn:length(label) eq 4}"><span class="new">${label}</span></c:if>
                                <c:if test="${fn:length(label) eq 5}"><span class="sale">${label}</span></c:if>
                                <c:if test="${fn:length(label) eq 7}"><span class="recommended">${label}</span></c:if>
                                <c:if test="${(fn:length(label) ne 4) && (fn:length(label) ne 5) && (fn:length(label) ne 7)  }"><span
                                >${label}</span></c:if>

                            </c:when>
                            <c:otherwise>

                                <c:set var="ClassName" value="${fn:replace(fn:toLowerCase(fn:trim(label)),' ', '')}"/>

                                <span class="${ClassName}">${label}</span>

                            </c:otherwise>
                        </c:choose>


                    </c:forEach>
                </div>

            </c:if>
            <product:productPrimaryImage product="${product}" format="product"/>
        </a>
        <div class="details">
            <div class="pull-left-details">

                <ycommerce:testId code="product_productName">
                    <a class="name" href="${fn:escapeXml(productUrl)}">
                        <c:out escapeXml="false" value="${ycommerce:sanitizeHTML(product.name)}"/>
                    </a>
                </ycommerce:testId>

                <c:if test="${not empty product.potentialPromotions}">
                    <div class="promo">
                        <c:forEach items="${product.potentialPromotions}" var="promotion">
                            ${ycommerce:sanitizeHTML(promotion.description)}
                        </c:forEach>
                    </div>
                </c:if>

                <ycommerce:testId code="product_productPrice">
                    <product:productPricePanel product="${product}"/>
                </ycommerce:testId>
                <c:forEach var="variantOption" items="${product.variantOptions}">
                    <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
                        <c:if test="${variantOptionQualifier.qualifier eq 'rollupProperty'}">
                            <c:set var="rollupProperty" value="${variantOptionQualifier.value}"/>
                        </c:if>
                        <c:if test="${variantOptionQualifier.qualifier eq 'thumbnail'}">
                            <c:set var="imageUrlHtml" value="${fn:escapeXml(variantOptionQualifier.value)}"/>
                        </c:if>
                        <c:if test="${variantOptionQualifier.qualifier eq rollupProperty}">
                            <c:set var="variantNameHtml" value="${fn:escapeXml(variantOptionQualifier.value)}"/>
                        </c:if>
                    </c:forEach>
                    <img style="width: 32px; height: 32px;" src="${imageUrlHtml}" title="${variantNameHtml}"
                         alt="${variantNameHtml}"/>
                </c:forEach>

                <div class="listcontent">

                    <product:productVariantSelector product="${product}"/>${product.variantOptions}
                    <c:set var="product" value="${product}" scope="request"/>
                    <c:set var="addToCartText" value="${addToCartText}" scope="request"/>
                    <c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>
                    <c:set var="isGrid" value="true" scope="request"/>

                    </ycommerce:testId>
                </div>
            </div>
            <div class="addtocart addtocartbox">
                <%-- 			<product:addtocartitem showQuantityBox="true" product="${product}" /> --%>
                <%-- 			<div class="actions-container-for-${fn:escapeXml(component.uid)} <c:if test="${ycommerce:checkIfPickupEnabledForStore() and product.availableForPickup}"> pickup-in-store-available</c:if>"> --%>
                <%-- 				<action:actions element="div" parentComponent="${component}"/> --%>
                <!-- 			</div> -->
                <a href="${fn:escapeXml(productUrl)}">
                    <button type="button" class="btn btn-primary js-enable-btn"
                            title="<spring:theme code='basket.add.to.basket'/>"
                            disabled="disabled">
                        <span class="Add_cart_icon"><spring:theme code='basket.add.to.basket'/></span>
                    </button>
                </a>

                <div class="plpIcon-list">
                    <c:if test="${product.stockAvailableForDelivery }">
                        <div class="homeDe">

                            <span class="icon-header-pickup"><i class="far fa-truck"></i></span>
                            <spring:theme code="home.de"/>
                        </div>
                    </c:if>
                    <c:if test="${product.stockAvailableForPickup }">
                        <div class="pickupStore">
				<span class="icon-header-pickup"><i
                        class="far fa-store-alt"></i></span><spring:theme code="checkout.multi.pickupInStore"/>
                        </div>
                    </c:if>
                </div>

            </div>


        </div>

    </div>
</div>
