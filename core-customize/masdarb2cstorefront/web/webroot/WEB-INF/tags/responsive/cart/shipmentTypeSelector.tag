<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:url var="changeStoreUrlAction" value="/cart/point-of-services"/>

<c:if test="${fn:length(supportedShipmentTypes) > 1}">
    <spring:url value="/cart/shipment-type" var="setShipmentTypeActionUrl"/>
    <spring:url value="/cart/shipment-type" var="setShipmentTypeActionUrl"/>
    <form:form action="${setShipmentTypeActionUrl}" method="post" id="shipmenttype-form">
        <div class="form-group">
            <spring:theme code="text.shipmenttype" var="shipmentTypeText"/>
            <label class="control-label sr-only" for="shipmenttype-selector">${shipmentTypeText}</label>

            <select name="code" id="shipmenttype-selector" class="form-control hidden">
                <c:forEach items="${supportedShipmentTypes}" var="shipmentType">
                    <c:choose>
                        <c:when test="${shipmentType.code == currentShipmentType.code}">
                            <option value="${fn:escapeXml(shipmentType.code)}" selected="selected"
                                    lang="${fn:escapeXml(shipmentType.code)}">${fn:escapeXml(shipmentType.name)}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${fn:escapeXml(shipmentType.code)}"
                                    lang="${fn:escapeXml(shipmentType.code)}">
                                    ${fn:escapeXml(shipmentType.name)}
                            </option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>


            <div class="pickUpInCart">

                <c:forEach items="${supportedShipmentTypes}" var="shipmentType">

                    <c:if test="${shipmentType.code == 'PICKUP_IN_STORE'}">
                        <c:set value="" var="activeRbtn"/>
                        <c:if test="${currentShipmentType.code  == 'PICKUP_IN_STORE'}">
                            <c:set value="activeR" var="activeRbtn"/>
                        </c:if>
                        <label class="container ${activeRbtn}">
                            <div class="left-content-checkmark">
                                <div class="content-check">
                                <span class="icon-header-pickup"><i
                                        class="far fa-store-alt"></i></span>
                                    <div class="content-div-text">
                                        <div class="header-div-pickup">
                                                ${fn:escapeXml(shipmentType.name)}


                                        </div>

                                        <div class="massage-div-pickup"><spring:theme
                                                code="pickup.collect.time.message"/></div>

                                    </div>

                                </div>

                                <c:choose>
                                    <c:when test="${currentShipmentType.code =='PICKUP_IN_STORE'}">
                                        <input type="radio" checked="checked" value="${fn:escapeXml(shipmentType.code)}"
                                               lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                        <span class="checkmark"></i></span>

                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" value="${fn:escapeXml(shipmentType.code)}"
                                               lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                        <span class="checkmark"></span>
                                    </c:otherwise>

                                </c:choose>
                            </div>
                            <div class="right-content-checkmark">
                               ${pickupEntries } :<spring:theme
                                       code="product.item.msg"/>
                            </div>
                        </label>

                    </c:if>
                    <c:if test="${shipmentType.code != 'PICKUP_IN_STORE'}">
                        <c:set value="" var="activeRbtn"/>
                        <c:if test="${currentShipmentType.code  != 'PICKUP_IN_STORE'}">
                            <c:set value="activeR" var="activeRbtn"/>
                        </c:if>
                        <label class="container ${activeRbtn}">
                            <div class="left-content-checkmark">
                                <div class="content-check">
													<span class="icon-header-pickup">

                                                        <i class="far fa-truck"></i>
                                                    </span>
                                    <div class="content-div-text">
                                        <div class="header-div-pickup">${fn:escapeXml(shipmentType.name)}</div>

                                        <div class="massage-div-pickup">

                                            <c:if test="${not empty cmsSite.deliveryTagMsg }">
                                                ${cmsSite.deliveryTagMsg}

                                            </c:if>

                                        </div>

                                    </div>
                                </div>


                                <c:choose>
                                    <c:when test="${currentShipmentType.code !='PICKUP_IN_STORE'}">
                                        <input type="radio" checked="checked" value="${fn:escapeXml(shipmentType.code)}"
                                               lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                        <span class="checkmark"></span>

                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" value="${fn:escapeXml(shipmentType.code)}"
                                               lang="${fn:escapeXml(shipmentType.code)}" name="radioCartPickup">
                                        <span class="checkmark"></span>
                                    </c:otherwise>

                                </c:choose>
                            </div>
                            <div class="right-content-checkmark">
                               ${deliveryEntries }:<spring:theme
                                    code="product.item.msg"/>
                            </div>

                        </label>
                    </c:if>

                </c:forEach>
            </div>

        </div>
    </form:form>
    <%--    <c:if test="${not empty cartData.shipmentType && cartData.shipmentType.code eq 'PICKUP_IN_STORE'}">--%>
    <%--        <ycommerce:testId code="pickupInStoreButton">--%>
    <%--            <c:choose>--%>
    <%--                <c:when test="${cartPage}">--%>
    <%--                    <a href="javascript:void(0)"--%>
    <%--                       class="btn btn-default btn-block js-pickup-in-store-button"--%>
    <%--                       id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}" disabled="disabled"--%>
    <%--                       data-productcart='${fn:escapeXml(product.price.formattedValue)}'--%>
    <%--                       data-productcart-variants='${fn:escapeXml(variantsJSON)}'--%>
    <%--                       data-img-html="${fn:escapeXml(productPrimaryImageHtml) }"--%>
    <%--                       data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"--%>
    <%--                       data-cartpage="${fn:escapeXml(cartPage)}"--%>
    <%--                       data-entryNumber="${fn:escapeXml(entryNumber)}"--%>
    <%--                       data-actionurl="${fn:escapeXml(changeStoreUrlAction)}"--%>
    <%--                       data-value="${fn:escapeXml(quantity)}">--%>
    <%--                        <spring:theme code="basket.page.shipping.change.store"/>--%>
    <%--                    </a>--%>
    <%--                </c:when>--%>
    <%--                <c:when test="${searchResultsPage}">--%>
    <%--                    <button class="btn btn-default btn-block js-pickup-in-store-button glyphicon glyphicon-map-marker"--%>
    <%--                            disabled="disabled" id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}"--%>
    <%--                            type="button submit"--%>
    <%--                            data-productcart='${fn:escapeXml(product.price.formattedValue)}'--%>
    <%--                            data-productcart-variants='${fn:escapeXml(variantsJSON)}'--%>
    <%--                            data-img-html="${fn:escapeXml(productPrimaryImageHtml)}"--%>
    <%--                            data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"--%>
    <%--                            data-cartpage="false" data-entryNumber="0"--%>
    <%--                            data-actionurl="${fn:escapeXml(changeStoreUrlAction)}" data-value="1">--%>
    <%--                    </button>--%>
    <%--                </c:when>--%>
    <%--                <c:otherwise>--%>
    <%--                    <button class="btn btn-default btn-block js-pickup-in-store-button glyphicon-map-marker btn-icon"--%>
    <%--                            disabled="disabled" id="product_${fn:escapeXml(product.code)}${fn:escapeXml(entryNumber)}"--%>
    <%--                            type="submit" data-productavailable="${fn:escapeXml(product.availableForPickup)}"--%>
    <%--                            data-productcart='${fn:escapeXml(product.price.formattedValue)}'--%>
    <%--                            data-productcart-variants='${fn:escapeXml(variantsJSON)}'--%>
    <%--                            data-img-html="${fn:escapeXml(productPrimaryImageHtml)}"--%>
    <%--                            data-productname-html="${fn:escapeXml(ycommerce:sanitizeHTML(product.name))}"--%>
    <%--                            data-cartpage="false" data-entryNumber="0"--%>
    <%--                            data-actionurl="${fn:escapeXml(changeStoreUrlAction)}" data-value="1">--%>
    <%--                        <spring:theme code="basket.page.shipping.change.store"/>--%>
    <%--                    </button>--%>
    <%--                </c:otherwise>--%>
    <%--            </c:choose>--%>
    <%--        </ycommerce:testId>--%>
    <%--    </c:if>--%>


</c:if>
