<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>"/>

<c:forEach items="${supportedShipmentTypes}" var="shipmentType">

    <c:if test="${shipmentType.code == 'PICKUP_IN_STORE'}">
        <c:if test="${currentShipmentType.code  == 'PICKUP_IN_STORE'}">
            <c:if test="${not empty cartData.entries}">
                <div class="cartPickup">
                    <div class="description-div-pickup">
                        <b>
                            <c:if test="${not empty cartData.deliveryPointOfService}">
                                <c:if test="${not empty cartData.selectedCity }">
                                    ${cartData.selectedCity.name }

                                -
                                </c:if>
                                <c:if test="${not empty cartData.deliveryPointOfService }">
                                    ${cartData.deliveryPointOfService.displayName }
                                </c:if>
                            </c:if>
                        </b>
                    </div>
                    <storepickup:clickPickupInStore product="${cartData.entries[0].product}" deliveryPointOfService="${cartData.deliveryPointOfService}" cartPage="false"/>
                    <storepickup:pickupStorePopup/>
                    <br/>
                </div>
            </c:if>
        </c:if>
    </c:if>
</c:forEach>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="boxback">
            <div class="pull-right m-b-10">

                <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                    <c:if test="${not empty savedCartCount and savedCartCount ne 0}">
                        <spring:url value="/my-account/saved-carts" var="listSavedCartUrl" htmlEscape="false"/>
                        <a href="${fn:escapeXml(listSavedCartUrl)}" class="save__cart--link cart__head--link">
                            <spring:theme code="saved.cart.total.number" arguments="${savedCartCount}"/>
                        </a>
                        <c:if test="${not empty quoteCount and quoteCount ne 0}">
                            <spring:url value="/my-account/my-quotes" var="listQuotesUrl" htmlEscape="false"/>
                            <a href="${fn:escapeXml(listQuotesUrl)}" class="cart__quotes--link cart__head--link">
                                <spring:theme code="saved.quote.total.number" arguments="${quoteCount}"/>
                            </a>
                        </c:if>

                    </c:if>
                </sec:authorize>
                <cart:saveCart/>
            </div>
            <ul class="item__list item__list__cart">
                <li class="hidden-xs hidden-sm">
                    <ul class="item__list--header">
                        <li class="item__toggle"></li>
                        <li class="item__image"></li>
                        <li class="item__info"><spring:theme code="basket.page.item"/></li>


                        <c:choose>
                            <c:when test="${cmsSite.showPricesWithTax || (ycommerce:isQuoteUserSalesRep() && !disableUpdate && allowedActions['DISCOUNT']) }">
                                <li class="item__netprice"><spring:theme code="basket.page.price"/></li>
                                <c:if test="${(ycommerce:isQuoteUserSalesRep() && !disableUpdate && allowedActions['DISCOUNT']) }">
                                    <li class="item__sellingprice"><spring:theme code="sellingprice"/></li>
                                    <li class="item__lowestprice"><spring:theme code="lowestprice"/></li>
                                </c:if>
                                <li class="item__price"><spring:theme code="basket.page.price.withtax"/></li>
                            </c:when>
                            <c:otherwise>
                                <li class="item__price"><spring:theme code="basket.page.price"/></li>
                                <c:if test="${ (ycommerce:isQuoteUserSalesRep() && !disableUpdate && allowedActions['DISCOUNT']) }">
                                    <li class="item__sellingprice"><spring:theme code="sellingprice"/></li>
                                    <li class="item__lowestprice"><spring:theme code="lowestprice"/></li>
                                </c:if>
                            </c:otherwise>
                        </c:choose>


                        <li class="item__quantity"><spring:theme code="basket.page.qty"/></li>
                        <%--             <li class="item__delivery"><spring:theme code="basket.page.delivery"/></li> --%>
                        <c:choose>
                            <c:when test="${cmsSite.showPricesWithTax || (ycommerce:isQuoteUserSalesRep() && !disableUpdate && allowedActions['DISCOUNT']) }">
                                <li class="item__total--column"><spring:theme code="basket.page.nettotal.custom"/></li>

                                <li class="item__total--column"><spring:theme code="basket.page.total.custom"/></li>
                            </c:when>
                            <c:otherwise>
                                <li class="item__total--column"><spring:theme code="basket.page.total"/></li>
                            </c:otherwise>
                        </c:choose>

                        <li class="item__remove"></li>
                    </ul>
                </li>

                <c:forEach items="${cartData.rootGroups}" var="group" varStatus="loop">
                    <cart:rootEntryGroup cartData="${cartData}" entryGroup="${group}"/>
                </c:forEach>
            </ul>

            <product:productOrderFormJQueryTemplates/>

            <storepickup:pickupStorePopup/>
        </div>
    </div>
</div>
