<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="consignment" required="true" type="de.hybris.platform.commercefacades.order.data.ConsignmentData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:if test="${not empty consignment.trackingID }">
	<div>
	    <div class="label-order">
	        <spring:theme code="text.tracking"/>
	    </div>
	    ${fn:escapeXml(consignment.trackingID)}
	
	</div>
</c:if>