<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="thirdpartyAuthenticationProvider" tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider"%>
<%@ attribute name="providers" required="false" type="java.util.List"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty providers }" >
	<p class="login-social-text"><spring:theme code="thirdparty.authentication.provider.description" /></p>

<ul class="medialogin">
<c:forEach items="${providers}" var="provider">
	<c:choose>
		<c:when test="${provider.enabledForStoreFront && provider.type eq 'FACEBOOK' }">
			<li class="fb"><i class=" fab fa-facebook-f"></i><thirdpartyAuthenticationProvider:facebook provider="${provider}"/>
			</li>
		</c:when>
		<c:when test="${provider.enabledForStoreFront && provider.type eq 'GOOGLE' }">
			<li  class="google"> <a href="javascript:;" id="google_action"><i class=" fab fa-google"></i></a>
			<thirdpartyAuthenticationProvider:google provider="${provider}"/>
			</li>
		</c:when>
		<c:when test="${provider.enabledForStoreFront && provider.type eq 'TWITTER' }">
			<li class="twitter"><a href="javascript:;" id="twitter_action"><i class=" fab fa-twitter"></i></a>
			<thirdpartyAuthenticationProvider:twitter provider="${provider}"/></li>
		</c:when>
		<c:when test="${provider.enabledForStoreFront && provider.type eq 'APPLE' }">
			<li ><thirdpartyAuthenticationProvider:apple provider="${provider}"/>
			</li>
		</c:when>
	</c:choose>
</c:forEach>
</ul>
</c:if>
