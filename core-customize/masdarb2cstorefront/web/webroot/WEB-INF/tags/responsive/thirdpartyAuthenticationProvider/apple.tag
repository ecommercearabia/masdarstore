<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ attribute name="provider" required="true"
	type="com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="${appleBaseRedirectUrl}/thirdpartyauthentication/apple"
	var="getUserDataURL" />

<script type="text/javascript" src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>

<script type="text/javascript">
            AppleID.auth.init({
                clientId : '${provider.id}',
                scope : 'email',
                redirectURI: '${getUserDataURL}'
            });
</script>

<div id="appleid-signin" data-color="black" data-border="true" data-type="sign in"></div>
