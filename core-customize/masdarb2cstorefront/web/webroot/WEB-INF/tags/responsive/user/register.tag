<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="thirdpartyAuthenticationProvider"
           tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<spring:url value="/login/register/termsandconditions" var="getTermsAndConditionsUrl"/>

<c:url var="b2bRegisterUrl" value="${siteBaseUrl}/register" />

<div class="user-register__headline">
    <spring:theme code="register.new.customer"/>
</div>
<p class="p-description">
    <spring:theme code="register.description"/>
</p>

<a href="${b2bRegisterUrl}" class="linkRegPage">
	<spring:theme code="header.link.login.company" text="To login as a Company, click here" />
</a>

<form:form method="post" modelAttribute="registerForm" action="${action}">
    <!-- Title is not required
    <formElement:formSelectBoxDefaultEnabled idKey="register.title" labelKey="register.title"
                                             selectCSSClass="form-control" path="titleCode" mandatory="true"
                                             skipBlank="false" skipBlankMessageKey="form.select.none"
                                             items="${titles}"/>
    -->
    <div class="row">
    <div class="col-xs-12">
        <formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" path="firstName"
                                  inputCSS="form-control" mandatory="true" placeholder="register.firstName"/>
    </div>
    <div class="col-md-6 col-xs-12">
        <formElement:formPasswordBox idKey="password" labelKey="register.pwd" path="pwd"
                                     inputCSS="form-control password-strength" mandatory="true"/>
    </div>
    <div class="col-md-6 col-xs-12 checkPwd">
        <formElement:formPasswordBox idKey="register.checkPwd" labelKey="register.checkPwd" path="checkPwd"
                                     inputCSS="form-control" mandatory="true"/>
    </div>
    </div>
    <div class="row">

        <div class="col-md-5 col-xs-12">
            <formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry" labelKey="register.mobileCountry"
                                                            selectCSSClass="form-control f16 " path="mobileCountry"
                                                            mandatory="true" skipBlank="false"
                                                            skipBlankMessageKey="form.select.empty"
                                                            items="${mobileCountries}"/>
        </div>
		<div class="col-md-7 col-xs-12">
			<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobileNumber" path="mobileNumber"
									  inputCSS="form-control" mandatory="true" placeholder="register.mobileNumber"/>
		</div>


        <div class="col-xs-12">
            <formElement:formInputBox idKey="register.email" labelKey="register.email" path="email"
                                      inputCSS="form-control" mandatory="true" placeholder="register.email"/>
        </div>

        <!-- ADDITIONAL REGISTRATION ATTRIBUTES -->

        <div class="col-md-6  col-xs-12 dateInput">
            <c:if test="${cmsSite.birthOfDateCustomerEnabled}">
                <div class="<c:if test="${cmsSite.birthOfDateCustomerHidden}">hidden</c:if>">
                    <formElement:formInputBox idKey="register.dob" labelKey="register.dob" inputCSS="form-control"
                                              mandatory="${cmsSite.birthOfDateCustomerRequired}" path="birthDate"
                                              placeholder="register.dob"/>
                    <img class="IconDate" src="${fn:escapeXml(themeResourcePath)}/images/Birthday.svg">
                </div>
            </c:if>
        </div>

        <div class="col-md-6 col-xs-12">
            <c:if test="${cmsSite.nationalityCustomerEnabled}">
                <div
                        class="<c:if test="${cmsSite.nationalityCustomerHidden}">hidden</c:if>">
                    <formElement:formSelectBoxDefaultEnabled idKey="nationality" labelKey="register.nationality"
                                                             selectCSSClass="form-control f16" path="nationality"
                                                             mandatory="${cmsSite.nationalityCustomerRequired}"
                                                             skipBlank="false" skipBlankMessageKey="form.select.none"
                                                             items="${nationalities}"/>
                </div>
            </c:if>
        </div>

        <div class="col-xs-12 checkboxMartal">
            <c:if test="${cmsSite.customerMaritalStatusEnabled}">
                <div
                        class="<c:if test="${cmsSite.customerMaritalStatusHidden}">hidden</c:if>">
                    <div class="hidden">
                        <formElement:formSelectBoxDefaultEnabled idKey="register.maritalStatus"
                                                                 labelKey="register.maritalStatus"
                                                                 selectCSSClass="form-control" path="maritalStatusCode"
                                                                 mandatory="${cmsSite.customerMaritalStatusRequired}"
                                                                 skipBlank="false"
                                                                 skipBlankMessageKey="form.select.none"
                                                                 items="${maritalStatuses }"/>
                    </div>
                    <span class="titleStatus"><spring:theme code="Marital.Status"/></span>

                    <label class="container firstCheckout"><spring:theme code="Single"/>
                        <input type="radio" name="radioMarital" value="SINGLE">
                        <span class="checkmark"></span>
                    </label>
                    <label class="container">
                        <spring:theme code="Married"/>
                        <input type="radio" name="radioMarital" value="MARRIED">
                        <span class="checkmark"></span>
                    </label>
                    <div class="help-block custom hidden"></div>
                </div>
            </c:if>
        </div>

        <!-- - - - - - - - - - - - - - - - - - --->
<div class="col-xs-12">
        <c:if test="${ not empty consentTemplateData }">
            <form:hidden path="consentForm.consentTemplateId" value="${consentTemplateData.id}"/>
            <form:hidden path="consentForm.consentTemplateVersion" value="${consentTemplateData.version}"/>
            <div class="checkbox">
                <label class="control-label uncased">
                    <form:checkbox path="consentForm.consentGiven" disabled="true"/>
                    <c:out value="${consentTemplateData.description}"/>
                </label>
            </div>
            <div class="help-block">
                <spring:theme code="registration.consent.link"/>
            </div>
        </c:if>

        <spring:theme code="register.termsConditions" arguments="${getTermsAndConditionsUrl}" var="termsConditionsHtml"
                      htmlEscape="false"/>
        <template:errorSpanField path="termsCheck">
            <div class="checkbox">
                <label class="control-label uncased">
                    <form:checkbox id="registerChkTermsConditions" path="termsCheck" disabled="true"/>
                        ${ycommerce:sanitizeHTML(termsConditionsHtml)}
                </label>
            </div>
        </template:errorSpanField>
</div>
        <input type="hidden" id="recaptchaChallangeAnswered"
               value="${fn:escapeXml(requestScope.recaptchaChallangeAnswered)}"/>
        <div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
        <div class="form-actions clearfix">
            <ycommerce:testId code="register_Register_button">
                <button type="submit" class="btn btn-primary pull-right w-200" disabled="disabled">
                    <spring:theme code='${actionNameKey}'/>
                </button>
            </ycommerce:testId>
        </div>

    </div>
</form:form>

<thirdpartyAuthenticationProvider:providers providers="${supportedThirdPartyAuthenticationProvider}"/>
