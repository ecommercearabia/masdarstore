<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true"
              type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:forEach items="${pageData.facets}" var="facet" varStatus="count">
    <c:choose>
        <c:when test="${facet.code eq 'availableInStores'}">
            <nav:facetNavRefinementStoresFacet facetData="${facet}" userLocation="${userLocation}"/>
        </c:when>
        <c:otherwise>
            <c:set var="autoOpen" value=""/>
            <c:if test="${not empty facet.values}">
				<c:if test="${count.index eq 0 }">
					<c:set var="autoOpen" value="active"/></c:if>
                <ycommerce:testId code="facetNav_title_${facet.name}">
                    <div class="facet js-facet box ${autoOpen}">
                        <div class="facet__name js-facet-name">


                            <c:if test="${count.index eq 0 }">
                                <c:set var="autoOpen" value="activeUL"/></c:if>

                            <spring:theme code="search.nav.facetTitle" arguments="${facet.name}"/>
                            <span class="clickarrowFilter">
										<c:choose>
                                            <c:when test="${count.index eq 0 }">
                                                <i class="far fa-minus "></i>
                                            </c:when>
                                            <c:otherwise>
                                                <i class="far fa-plus "></i>
                                            </c:otherwise>
                                        </c:choose>

							</span>
                        </div>


                        <div class="facet__values js-facet-values js-facet-form ">

                            <ul class="facet__list js-facet-list ${autoOpen}">

                                <c:forEach items="${facet.values}" var="facetValue">
                                    <li>
                                        <c:if test="${facet.multiSelect}">
                                            <ycommerce:testId code="facetNav_selectForm">
                                                <form action="#" method="get">
                                                    <!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
                                                    <input type="hidden" name="q"
                                                           value="${facetValue.query.query.value}"/>
                                                    <input type="hidden" name="text"
                                                           value="${searchPageData.freeTextSearch}"/>
                                                    <label>
                                                        <input type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''}
                                                               class="facet__list__checkbox js-facet-checkbox sr-only"/>
                                                        <span class="facet__list__label">
										<span class="facet__list__mark"></span>
										<span class="facet__list__text">
											<span class="title-text-faces">${fn:escapeXml(facetValue.name)}&nbsp;</span>
											<ycommerce:testId code="facetNav_count">
                                                <span class="facet__value__count"><spring:theme
                                                        code="search.nav.facetValueCount"
                                                        arguments="${facetValue.count}"/></span>
                                            </ycommerce:testId>
										</span>
									</span>
                                                    </label>
                                                </form>
                                            </ycommerce:testId>
                                        </c:if>
                                        <c:if test="${not facet.multiSelect}">
                                            <c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
                                            <span class="facet__text">
								<a href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)}</a>
								<ycommerce:testId code="facetNav_count">
                                    <span class="facet__value__count"><spring:theme code="search.nav.facetValueCount"
                                                                                    arguments="${facetValue.count}"/></span>
                                </ycommerce:testId>
							</span>
                                        </c:if>
                                    </li>
                                </c:forEach>
                            </ul>


                        </div>
                    </div>
                </ycommerce:testId>
            </c:if>
        </c:otherwise>
    </c:choose>
</c:forEach>


