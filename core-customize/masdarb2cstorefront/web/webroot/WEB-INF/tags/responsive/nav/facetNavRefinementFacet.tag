<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="autoOpen" value=""/>
<c:if test="${not empty facetData.values}">
	<ycommerce:testId code="facetNav_title_${facetData.name}">
		<div class="facet js-facet box">
			<div class="facet__name js-facet-name">


				<c:if test="${facetData.code eq 'expressDelivery' }">
					<c:set var="autoOpen" value="activeUL"/></c:if>

				<spring:theme code="search.nav.facetTitle" arguments="${facetData.name}"/>
				<span class="clickarrowFilter">
			<c:choose>
				<c:when test="${facetData.code eq 'expressDelivery' }">
					<i class="far fa-minus "></i>
				</c:when>
				<c:otherwise>
					<i class="far fa-plus "></i>
				</c:otherwise>
			</c:choose>

			</span>
			</div>


			<div class="facet__values js-facet-values js-facet-form ">

				<ul class="facet__list js-facet-list ${autoOpen}">

					<c:forEach items="${facetData.values}" var="facetValue">
						<li>
							<c:if test="${facetData.multiSelect}">
								<ycommerce:testId code="facetNav_selectForm">
									<form action="#" method="get">
										<!-- facetValue.query.query.value and searchPageData.freeTextSearch are html output encoded in the backend -->
										<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
										<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
										<label>
											<input type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''}
												   class="facet__list__checkbox js-facet-checkbox sr-only"/>
											<span class="facet__list__label">
										<span class="facet__list__mark"></span>
										<span class="facet__list__text">
											${fn:escapeXml(facetValue.name)}&nbsp;
											<ycommerce:testId code="facetNav_count">
                                                <span class="facet__value__count"><spring:theme
														code="search.nav.facetValueCount"
														arguments="${facetValue.count}"/></span>
											</ycommerce:testId>
										</span>
									</span>
										</label>
									</form>
								</ycommerce:testId>
							</c:if>
							<c:if test="${not facetData.multiSelect}">
								<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
								<span class="facet__text">
								<a href="${fn:escapeXml(facetValueQueryUrl)}">${fn:escapeXml(facetValue.name)}</a>
								<ycommerce:testId code="facetNav_count">
                                    <span class="facet__value__count"><spring:theme code="search.nav.facetValueCount"
																					arguments="${facetValue.count}"/></span>
								</ycommerce:testId>
							</span>
							</c:if>
						</li>
					</c:forEach>
				</ul>


			</div>
		</div>
	</ycommerce:testId>
</c:if>
