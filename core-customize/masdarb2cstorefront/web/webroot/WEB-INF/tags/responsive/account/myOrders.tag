<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="numberOfOrders" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<spring:url var="orderHistoryURL" value="{contextPath}/my-account/orders" htmlEscape="false">
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <spring:param name="contextPath" value="${request.contextPath}"/>
</spring:url>

<div class="cont_box order_myAccount">

    <div class="headline">
        <div class="d-flex-center">
            <div class="left-side">
                <div class="fal fa-clock"></div>
            </div>
            <div class="head_myaccount">
                <spring:theme code="my.orders"/></div>
        </div>
        <a href="${orderHistoryURL}" class="view_All_order">View All Orders</a>
    </div>


    <div class="right-side">
        <div class="body">

            <div class="row">
                    <order:accountOrderHistoryDash searchPageData="${orderSearchPageData}" />

            </div>
        </div>
    </div>
<%--     <a href="${orderHistoryURL}" class="link_box_cont"></a> --%>
</div>
