<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="quoteData" required="true" type="de.hybris.platform.commercefacades.quote.data.QuoteData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${cmsSite.enableQuoteEntryDiscount}">
	<c:set var="entryNumberHtml" value="${fn:escapeXml(entry.entryNumber)}"/>
	<spring:url value="/quote/{/quoteCode}/discount/apply/{/entryNumber}" var="quoteEntryDiscountApplyAction" htmlEscape="false">
	    <spring:param name="quoteCode"  value="${fn:escapeXml(quoteData.code)}"/>
	    <spring:param name="entryNumber"  value="${fn:escapeXml(entry.entryNumber)}"/>
	</spring:url>
	<c:if test="${ycommerce:isQuoteUserSalesRep() && !disableUpdate && allowedActions['DISCOUNT']}">
		<li>
		 	<a href="#" class="js-quote-entry-discount-link <c:if test="${cartData.quoteDiscounts.value > 0}">quote-entry-discount-link</c:if>" 
		 		data-entry-number="${entry.entryNumber}"> <spring:theme code="text.cart.entry.quote.button.label" text="Discount"/></a>
			<div style="display: none">
				<spring:theme code="text.quote.discount.modal.title" arguments="${entryNumberHtml}" var="discountModalTitleHtml" /> 
				<div id="js-quote-entry-discount-modal${entryNumberHtml}"
					data-quote-entry-modal-title="Entry ${entry.entryNumber}"
					data-quote-entry-modal-total="${fn:escapeXml(entry.netBasePrice.value *entry.quantity)}"
					data-quote-entry-modal-quote-entry-discount="${fn:escapeXml(entry.quoteDiscounts.value)}"
					data-quote-entry-modal-currency="${fn:escapeXml(currentCurrency.symbol)}">
					<div class="quote-entry-discount__modal">
						<form:form id="quoteEntryDiscountForm${entryNumberHtml}"
							action="${quoteEntryDiscountApplyAction}" method="post"
							modelAttribute="quoteEntryDiscountForm">
							<div class="row">
								<div class="col-xs-6 col-sm-7">
									<label class="quote-discount__modal--label text-left"> <spring:theme code="text.quote.discount.by.percentage" /></label>
								</div>
								<div class="col-xs-6 col-sm-5">
									<div class="input-group quote-entry-discount__modal--input">
										<span class="quote-discount__modal--input__label">%</span> 
										<input type="text" min="0" max="100" step="any" class="form-control input-sm pull-right text-right" name="quote-entry-discount-by-percentage" 
										data-entry-number="${entryNumberHtml}" id="js-quote-entry-discount-by-percentage${entryNumberHtml}" maxlength="10" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-7">
									<label class="quote-discount__modal--label text-left"> <spring:theme code="text.quote.discount.by.amount" /></label>
								</div>
								<div class="col-xs-6 col-sm-5">
									<div class="input-group quote-entry-discount__modal--input">
										<span class="quote-discount__modal--input__label">${fn:escapeXml(currentCurrency.symbol)}</span> 
										<input type="text" step="any" class="form-control input-sm pull-right text-right" name="quote-entry-discount-by-amount" 
										data-entry-number="${entryNumberHtml}" id="js-quote-entry-discount-by-amount${entryNumberHtml}" maxlength="10" />
									</div>
								</div>
							</div>
								
							<div class="row">
								<div class="col-xs-6 col-sm-7">
									<label class="quote-discount__modal--label text-left"> <spring:theme code="text.quote.discount.adjust.total" /></label>
								</div>
								<div class="col-xs-6 col-sm-5">
									<div class="input-group quote-entry-discount__modal--input">
										<span class="quote-discount__modal--input__label">${fn:escapeXml(currentCurrency.symbol)}</span> 
										<input type="text" step="any" class="form-control input-sm pull-right text-right" name="quote-entry-discount-adjust-total" 
											data-entry-number="${entryNumberHtml}" id="js-quote-entry-discount-adjust-total${entryNumberHtml}" maxlength="10" />
									</div>
								</div>
							</div>
							<div class="quote-discount__modal--original__total">
								<div class="row">
									<div class="col-xs-6 text-left">
										<spring:theme code="basket.page.totals.quote.total.original" />
									</div>
								
									<div class="col-xs-6 text-right">
										<format:price priceData="${entry.netTotalPrice}" />
									</div>
								</div>
							</div>
							<div class="quote-discount__modal--new__total">
								<div class="row">
									<div class="col-xs-6 text-left">
										<spring:theme code="basket.page.totals.quote.total.after.discount" />
									</div>
									<div class="col-xs-6 text-right" id="js-quote-entry-discount-new-total${entryNumberHtml}">
										<format:price priceData="${entry.netTotalPrice}" />
									</div>
								</div>
							</div>
								
							<form:input type="hidden" name="quote-entry-discount-rate" id="js-quote-entry-discount-rate${entryNumberHtml}" value="${entry.quoteDiscountsRate}" maxlength="10" path="discountRate" />
							<form:input type="hidden" name="quote-entry-discount-type" id="js-quote-entry-discount-type${entryNumberHtml}" value="${entry.quoteDiscountsType}" maxlength="10" path="discountType" />
								
							<button type="submit" class="btn btn-primary btn-block" id="submitEntryButton${entryNumberHtml}">
								<spring:theme code="text.quote.done.button.label" />
							</button>
							<button type="button" class="btn btn-default btn-block cancelEntryButton" id="cancelEntryButton${entryNumberHtml}" >
								<spring:theme code="text.quote.cancel.button.label" />
							</button>
						</form:form>
					</div>
				</div>
			</div>
		</li>
	</c:if>
</c:if>                      
	                        
	                        
	                        
	                        
	                        
	                        