<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="thirdpartyAuthenticationProvider" tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider"%>

<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	<div class="login-page__headline">
		<spring:theme code="login.popup.title" />
	</div>
	<p>
		<spring:theme code="login.popup.description" />
	</p>
	<c:url value="/j_spring_security_check" var="loginActionUrl" />
	<form:form action="${loginActionUrl}" method="post" modelAttribute="loginForm">
	<c:if test="${not empty message}">
		<span class="has-error"> <spring:theme code="${message}" />
		</span>
	</c:if>	
	
		<formElement:formInputBox idKey="j_username" labelKey="login.email"
			path="j_username" mandatory="true" placeholder="login.email" />
			
		<div class="password-holder">
		<formElement:formPasswordBox idKey="j_password"
			labelKey="login.password" path="j_password" inputCSS="form-control" 
			mandatory="true"  />
	</div>
				
	
			<div class="forgotten-password">
				<ycommerce:testId code="login_forgotPassword_link">
					<a href="#" data-link="<c:url value='/login/pw/request'/>" class="js-password-forgotten" data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
						<spring:theme code="login.link.forgottenPwd" />
					</a>
				</ycommerce:testId>
			</div>
			
		<ycommerce:testId code="loginAndCheckoutButton">
			<button type="submit" class="btn btn-primary btn-block">
				<spring:theme code="login.popup.label" />
			</button>
		</ycommerce:testId>
		
			
</form:form>

<thirdpartyAuthenticationProvider:providers providers="${supportedThirdPartyAuthenticationProvider}"/>

<c:url value="/register" var="registerPageUrl" />
			<div class="sgin_up">
			<span class="or-style"><span><spring:theme code="login.singup.or" /></span></span>
			<a href="${registerPageUrl}"> <spring:theme
					code="login.singup.popup" text="Sign Up" />
			</a>
			</div>
</sec:authorize>
