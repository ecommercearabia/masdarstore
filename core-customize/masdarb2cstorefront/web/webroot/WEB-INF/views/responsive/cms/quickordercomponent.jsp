<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="row" style="background:url(${fn:escapeXml(backgroundImage.url)}) no-repeat center top">
	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
		<div class="bluebox">
			<h1>${title}</h1>
			<h6>${content}</h6>
			
				<c:url value="/cart/add" var="addToCartUrl" />
				<div class="AddToCart-AddToCartAction">
				<form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">
					<div class="form-group">
				<label class="control-label " for="itemcode"> <spring:theme code="quick.order.component.add.to.basket.code" />
			</label>
			<input id="itemcode"  name="productCodePost" class=" form-control" type="text" value=""/>
		
		</div>
				<div class="form-group">
				<label class="control-label " for="itemcode"> <spring:theme code="quick.order.component.add.to.basket.qty" />
			</label>
			<input id="qty" name="qty" class="qty form-control" value="" type="number" min="1" >
		
		</div>			

				
					<p id="errorcode"><i class="far fa-exclamation-circle"></i> <spring:theme code="quick.order.component.add.to.basket.error"/></p>

				<button id="addToCartButton" type="submit" class="btn btn-primary btn-block js-add-to-cart js-enable-btn" disabled="disabled">
									<spring:theme code="quick.order.component.add.to.basket" text="Add to Cart"/>
								</button>
				</form:form>
				</div>
		</div>
	</div>
</div>



<!-- <img title="${fn:escapeXml(backgroundImage.altText)}" alt="${fn:escapeXml(backgroundImage.altText)}"
					src="${fn:escapeXml(backgroundImage.url)}"> -->