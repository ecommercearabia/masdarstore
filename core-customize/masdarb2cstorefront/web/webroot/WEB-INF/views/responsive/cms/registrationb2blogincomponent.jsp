<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:if test="${cmsSite.enableRegistration == true}">
	<div class="textsection2">
		<div class="row">
			<div class="col-md-12">
				<div class="user-register__headline">Register your account
					online${component.title}</div>
				<ul>
					<li>View your balance and easily pay your invoices</li>
					<li>View details of previous orders</li>
					<li>Your trade pricing terms agreed with your branch</li>
					<c:if test="${not empty component.content }">
						<li>${component.content}</li>
					</c:if>
				</ul>
			</div>
			<div class="col-md-12 text-center">
				<br />
				<div class="btn btn-default pull-right">
					<cms:component component="${component.link}" />
				</div>
			</div>
		</div>
	</div>
</c:if>
