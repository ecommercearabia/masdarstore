<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


	<c:if test="${not empty lastOrder}">
	<c:url var="link" value="/my-account/order/${lastOrder.code}"/> 
	<li class="secondarycolor"><a href="${link}"><i class="fas fa-clipboard-list"></i> <spring:theme code="text.lastorder"/> :<spring:theme code="text.account.order.status.display.${lastOrder.statusDisplay}" text=" "/></a></li>
	</c:if> 

	
	 