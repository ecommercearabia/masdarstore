<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement"
           tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="thirdpartyAuthenticationProvider"
           tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<%--<spring:theme code="customer.cities.popup.title"/>--%>

<%--<spring:theme code="customer.cities.popup.description"/>--%>

<%-- <c:choose> --%>
<%-- 	<c:when test="${not empty selectedCustomerLocationCity}">  --%>
<%-- 	${fn:escapeXml(selectedCustomerLocationCity)}<br /> --%>
<%-- 	</c:when> --%>
<%-- 	<c:otherwise> --%>
<%-- 		<spring:theme code="customer.cities.popup.not.selected" /> --%>
<!-- <br /> -->
<%-- 	</c:otherwise> --%>
<%-- </c:choose> --%>

<c:url var="addToCartToPickupInStoreUrl" value="/store-delivery/select/city"/>

<div class="selectBox">
    <div class="CityNameSelect">
        <c:choose>
            <c:when test="${not empty selectedCustomerLocationCity.name}">

                <span class="titleCity"><spring:theme code="City.name"/>:</span><span class="selectCity">${selectedCustomerLocationCity.name}</span>
            </c:when>
            <c:otherwise>

                <span class="notSelectedCity"><spring:theme code="City.Not.Selected"/></span>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="CityClickBtn">

        <c:choose>
            <c:when test="${not empty selectedCustomerLocationCity.name}">

                <i class="fas fa-map-marker-alt"></i>
                <span class="titleboxCitybtn">


                    <spring:theme code="Change.City"/>
	</span>
            </c:when>
            <c:otherwise>
                <i class="fas fa-map-marker-alt"></i>
                <span class="titleboxCitybtn">

                    <spring:theme code="please.select.City"/>
	</span>
            </c:otherwise>
        </c:choose>


    </div>
</div>


<div class="deliverypopup hidden">
    <div class="popupBoxdelivery">
        <div class="deliveryContent">
            <div class="headerdeliveryBox">
                <span class="titleheaderdelivery"><spring:theme code="select.a.city"/> </span>
                <span class="closepopupdelivery"><i class="fal fa-times"></i></span>
            </div>
            <div class="pagesdeliveryBox">
                <div class="default-view">
                    <div class="select-default">
                        <div class="title-select">
                            <spring:theme code="select.a.city.msg"/>

                        </div>

                    </div>
                    <div class="row">
                        <div class="inputSerach col-md-4 col-xs-12">

                            <input id="searchInputText" class="form-control "
                                   placeholder='<spring:theme code="search.by.City.name"/>'>

                        </div>
                    </div>
                    <div class="divChangeCity">
                        <c:choose>
                            <c:when test="${not empty selectedCustomerLocationCity.name}">


                                <span class="titleboxCitybtn">
		                         <spring:theme code="Change.City"/>
	                            </span>
                            </c:when>
                            <c:otherwise>

                                <span class="titleboxCitybtn">
		                           <spring:theme code="please.select.City"/>
	                            </span>
                            </c:otherwise>
                        </c:choose>

                    </div>
                    <div id="itemListdata" class="itemListPlaces row">
                        <c:forEach items="${customerLocationCities}" var="city">





                            <c:choose>
                                <c:when test="${selectedCustomerLocationCity.name eq city.name}">
                                    <div class="col-md-3 col-sm-6 col-xs-12 ">
                                    <div class="itemCity selectedCity" value="${fn:escapeXml(city.code)}"
                                    >${fn:escapeXml(city.name)}</div>
                                    </div>
                                </c:when>
                                <c:otherwise>
                            <div class="col-md-3 col-sm-6 col-xs-12 ">
                                    <div class="itemCity " value="${fn:escapeXml(city.code)}"
                                    >${fn:escapeXml(city.name)}</div>
                            </div>
                                </c:otherwise>
                            </c:choose>



                        </c:forEach>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>


<form:form action="${addToCartToPickupInStoreUrl}" id="cityForm" method="post" class="hidden">
    <input value="${selectedCustomerLocationCity.code}" type="text" id="cityId" name="cityId"/>
    <button type="submit">City name</button>

</form:form>


