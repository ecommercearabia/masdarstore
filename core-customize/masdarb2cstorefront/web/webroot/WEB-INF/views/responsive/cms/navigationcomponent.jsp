<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set value="${fn:escapeXml(component.styleClass)}" var="navigationClassHtml" />

<c:if test="${component.visible}">
<c:url value="/my-account" var="account"></c:url>
<%--<div class="nav_account">--%>
<%--<a href="${account}" class="personal_info"><span class="boder_personal_info"><i class="user"></i><c:out value="${component.navigationNode.title}"/></span></a>--%>


<%--</div>--%>
<div class="hidden-sm hidden-xs desktop-menu">
            <c:if test="${not empty component.navigationNode.title }">
                <a href="${account}">
                  <i class="fas fa-user"></i> <c:out value="${component.navigationNode.title}"/>
                </a>
            </c:if>
            <div class="wsshoptabing wtsdepartmentmenu clearfix hidden-sm hidden-xs">
              <div class="wsshopwp clearfix">
            <ul class="wstliststy02 clearfix">
            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="hidden-sm hidden-xs hidden-company">
									<ycommerce:testId code="header_LoggedUser">
										<a><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" /></a>
									</ycommerce:testId>
								</li>
							</sec:authorize>
            <c:forEach items="${component.navigationNode.children}" var="topLevelChild">
                <c:forEach items="${topLevelChild.entries}" var="entry">

                        <cms:component component="${entry.item}" evaluateRestriction="true" element="li"/>

                </c:forEach>
            </c:forEach>
            </ul>
            </div>
            </div>

</div>

    <div class="hidden-md hidden-lg">
        <c:if test="${not empty component.navigationNode.title }">
            <span class="firstLevelItem">
            <a href="javascript:;">
                 <c:out value="${component.navigationNode.title}"/>
            </a>
                <span class="wsarrow"></span>
						<i class="far fa-chevron-down hidden-md hidden-lg"></i>
            </span>
        </c:if>

                <ul class="sub-menu levelTwoContainer">
                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <c:set var="maxNumberChars" value="25" />
                        <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                            <c:set target="${user}" property="firstName"
                                   value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
                        </c:if>

                        <li class="hidden-sm hidden-xs hidden-company">
                            <ycommerce:testId code="header_LoggedUser">
                                <a><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" /></a>
                            </ycommerce:testId>
                        </li>
                    </sec:authorize>
                    <c:forEach items="${component.navigationNode.children}" var="topLevelChild">
                        <c:forEach items="${topLevelChild.entries}" var="entry">

                            <cms:component component="${entry.item}" evaluateRestriction="true" element="li"/>

                        </c:forEach>
                    </c:forEach>
                </ul>


    </div>


</c:if>