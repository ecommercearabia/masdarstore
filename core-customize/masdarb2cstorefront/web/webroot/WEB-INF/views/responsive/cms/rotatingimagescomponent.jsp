<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="carousel__component text-center">
			
		
			<div class="carousel__component--headline">${fn:escapeXml(title)}</div>
			
<div class="carousel__component--carousel js-owl-carousel  owl-carousel js-owl--${not empty component.theme ? fn:toLowerCase(component.theme) : 'theme2'}">
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<div class="carousel__item">
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
					<c:if test="${ycommerce:validateUrlScheme(banner.media.url)}">
					
							<c:choose>
								<c:when test="${!ycommerce:validateUrlScheme(encodedUrl)}">
									<img src="${fn:escapeXml(banner.media.url)}"  class="logosize"
										alt="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}" 
										title="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}"/>
								</c:when>
								
								<c:otherwise>
									<a tabindex="-1" href="${fn:escapeXml(encodedUrl)}"<c:if test="${banner.external}"> target="_blank"</c:if>>
										<img src="${fn:escapeXml(banner.media.url)}" class="logosize"
											alt="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}" 
											title="${not empty banner.headline ? fn:escapeXml(banner.headline) : fn:escapeXml(banner.media.altText)}"/>
									</a>
								</c:otherwise>
							</c:choose>
					
					</c:if>
				</c:if>
				</div>
			</c:forEach>
</div>

	<c:if test="${not empty component.link}">
<div class="btn btn-primary">
<cms:component component="${component.link}" />
</div>
	</c:if>
</div>

