<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:choose>
    <c:when test="${not empty productData}">
        <div class="carousel__component">

            <div class="carousel__component-title">
                <div class="carousel__component--headline">
                        ${fn:escapeXml(title)}</div>
                <span class="text-right"><cms:component component="${component.link}"/></span>
            </div>
            <c:choose>
                <c:when test="${component.popup}">
                    <div class="carousel__component--carousel js-owl-carousel  owl-carousel js-owl-theme1">
                        <div id="quickViewTitle" class="quickView-header display-none">
                            <div class="headline">
                                <span class="headline-text"><spring:theme code="popup.quick.view.select"/></span>
                            </div>
                        </div>
                        <c:forEach items="${productData}" var="product">

                            <c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
                            <div class="carousel__item">
                                <a href="${productQuickViewUrl}" class="js-reference-item">
                                    <div class="carousel__item--thumb">
                                        <product:productPrimaryReferenceImage product="${product}" format="product"/>
                                    </div>
                                    <div class="carousel__item--name">${fn:escapeXml(product.name)}</div>
                                    <div class="carousel__item--price"><format:fromPrice
                                            priceData="${product.price}"/></div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="carousel__component--carousel js-owl-carousel  owl-carousel js-owl-theme1">
                        <c:forEach items="${productData}" var="product">

                           

                            <div class="carousel__item">
                             <c:url value="${product.url}" var="productUrl"/>
                                <a href="${productUrl}">
                                    <div class="carousel__item--thumb relativepos">
                                        <c:if test="${not empty product.productLabels}">

                                            <div class="productlabel">
                                                <c:forEach items="${product.productLabels}" var="label">
                                                    <c:choose>
                                                        <c:when test="${fn:escapeXml(currentLanguage.isocode) eq 'ar'}">


                                                            <c:if test="${fn:length(label) eq 4}"><span
                                                                    class="new">${label}</span></c:if>
                                                            <c:if test="${fn:length(label) eq 5}"><span
                                                                    class="sale">${label}</span></c:if>
                                                            <c:if test="${fn:length(label) eq 7}"><span
                                                                    class="recommended">${label}</span></c:if>
                                                            <c:if test="${(fn:length(label) ne 4) && (fn:length(label) ne 5) && (fn:length(label) ne 7)  }"><span
                                                            >${label}</span></c:if>

                                                        </c:when>
                                                        <c:otherwise>

                                                            <c:set var="ClassName"
                                                                   value="${fn:replace(fn:toLowerCase(fn:trim(label)),' ', '')}"/>

                                                            <span class="${ClassName}">${label}</span>

                                                        </c:otherwise>
                                                    </c:choose>


                                                </c:forEach>
                                            </div>

                                        </c:if>
                                        <product:productPrimaryImage product="${product}" format="zoom"/>
                                    </div>
                                    <div class="carousel__item--name">${fn:escapeXml(product.name)}</div>

                                    <div class="carousel__item--price">

                                        <ycommerce:testId code="product_productPrice">
                                            <product:productPricePanel product="${product}"/>
                                        </ycommerce:testId>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </c:when>

    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>

