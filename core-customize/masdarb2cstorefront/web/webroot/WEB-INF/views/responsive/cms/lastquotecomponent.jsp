<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty lastQuote}">
	<c:url var="link" value="/my-account/my-quotes/${lastQuote.code}"/> 
	<li class="secondarycolor"><a href="${link}"><i class="fas fa-sack-dollar"></i> <spring:theme code="text.lastquote"/> : <spring:theme code="last.quote.state.${lastQuote.state}" /></a></li>
</c:if> 

	