<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:theme code="label.incVat" var="incVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.exVat" var="exVatLabel"/>
<spring:theme code="label.Vat" var="VatLabel"/>
<c:if test="${component.visible}">
    <nav class="navigation navigation--bottom js_navigation--bottom js-enquire-offcanvas-navigation" role="navigation">

        <div class="navigation__overflow w1300">

            <ul class="sticky-nav-top hidden-lg hidden-md js-sticky-user-group hidden-md hidden-lg">
                    <%--Dynamically generated by 'acc.navigation.js"--%>
            </ul>


            <nav class="wsmenu clearfix ">
                <ul class="wsmenu-list">
                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <cms:pageSlot position="HeaderLinks" var="link" element="li"
                                      class="firstLevelMenu hidden-md hidden-lg">
                            <cms:component component="${link}"/>
                        </cms:pageSlot>
                    </sec:authorize>
                    <c:forEach items="${component.navigationNode.children}" var="childLevel1" varStatus="loop">

                        <li aria-haspopup="true" class="firstLevelMenu">
                            <c:set var="name" value=""></c:set>
                            <c:set var="totalSubNavigationColumns" value="${0}"/>
                            <c:set var="hasSubChild" value="false"/>
                            <c:forEach items="${childLevel1.children}" var="childLevel2">
                                <c:if test="${not empty childLevel2.children}">
                                    <c:set var="hasSubChild" value="true"/>
                                    <c:set var="subSectionColumns"
                                           value="${fn:length(childLevel2.children)/component.wrapAfter}"/>
                                    <c:if test="${subSectionColumns > 1 && fn:length(childLevel2.children)%component.wrapAfter > 0}">
                                        <c:set var="subSectionColumns" value="${subSectionColumns + 1}"/>
                                    </c:if>
                                    <c:choose>
                                        <c:when test="${subSectionColumns > 1}">
                                            <c:set var="totalSubNavigationColumns"
                                                   value="${totalSubNavigationColumns + subSectionColumns}"/>
                                        </c:when>

                                        <c:when test="${subSectionColumns < 1}">
                                            <c:set var="totalSubNavigationColumns"
                                                   value="${totalSubNavigationColumns + 1}"/>
                                        </c:when>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                            <c:forEach items="${childLevel1.entries}" var="childlink1">
                                <c:if test="${not empty childlink1.item && childlink1.item.visible }">
                                    <c:set var="name" value="${childlink1.item.linkName}"></c:set>
                                    <%-- 								<cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="nav__link js_nav__link" /> --%>
                                    <c:url value="${childlink1.item.url}" var="link"></c:url>
                                    <span class="firstLevelItem">
									<a href="${link}" class="a_link_hover">

										<span class="cat_name">${childlink1.item.linkName}</span>


									</a>
								<span class="wsarrow"></span>
						<i class="far fa-chevron-down hidden-md hidden-lg"></i>

							</span>

                                    <c:if test="${not empty childLevel1.children}">


                                        <c:choose>
                                            <c:when test="${hasSubChild}">
                                                <c:forEach items="${childLevel1.children}" var="childLevel2">
                                                    <ul class="sub-menu levelTwoContainer">

                                                        <c:forEach items="${childLevel2.children}" var="childLevel3"
                                                                   step="${component.wrapAfter}" varStatus="i">

                                                            <c:forEach items="${childLevel2.children}" var="childLevel3"
                                                                       begin="${i.index}"
                                                                       end="${i.index + component.wrapAfter - 1}"
                                                                       varStatus="var">
                                                                <c:forEach items="${childLevel3.entries}"
                                                                           var="childlink3">
                                                                    <li aria-haspopup="true"
                                                                        class="ChildLevelTwo ${var.index}">
															<span class="ChildLevelTwoItem">
															<cms:component component="${childlink3.item}"
                                                                           evaluateRestriction="true" element="a"/>
															</span>
                                                                    </li>
                                                                </c:forEach>
                                                            </c:forEach>
                                                        </c:forEach>
                                                        <span class="triangle hidden-xs hidden-sm"></span>
                                                    </ul>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>

                                                <c:forEach items="${childLevel1.children}" var="childLevel2">
                                                    <ul class="sub-menu levelTwoContainer">
                                                        <c:forEach items="${childLevel2.entries}" var="childlink2">
                                                            <li aria-haspopup="true" class="ChildLevelTwo">
															<span class="ChildLevelTwoItem">
																	<cms:component component="${childlink2.item}"
                                                                                   evaluateRestriction="true"
                                                                                   element="a"/>

															</span>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:forEach>

                                            </c:otherwise>
                                        </c:choose>

                                    </c:if>

                                </c:if>
                            </c:forEach>
                        </li>
                    </c:forEach>
                 
                        <li class="firstLevelMenu hidden-md hidden-lg" aria-haspopup="true">
      
                                <a href="${siteBaseUrl}" class="store-finder-mobile" ><spring:theme code="header.link.business.site"/></a>

                        </li>
                   
                    <c:if test="${empty hideHeaderLinks}">
                        <li class="firstLevelMenu hidden-md hidden-lg" aria-haspopup="true">
                            <ycommerce:testId code="header_StoreFinder_link">

                                <c:url value="/store-finder" var="storeFinderUrl"/>
                                <a href="${fn:escapeXml(storeFinderUrl)}" class="store-finder-mobile">
                                    <span class="fas fa-map-marker-alt"></span> <spring:theme
                                        code="branches.locator"
                                        text="branches locator"/>
                                </a>

                            </ycommerce:testId>
                        </li>
                    </c:if>


                    <li class="firstLevelMenu hidden-md hidden-lg" aria-haspopup="true">
                        <div class="swithchTax">
                        <span class="labelVat">
                                ${VatLabel}
                        </span>

                                ${exVatLabel}

                            <c:choose>
                                <c:when test="${incTax}">
                                    <label class="switch">
                                        <input class="Vat_switch" type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>

                                </c:when>
                                <c:otherwise>
                                    <label class="switch">
                                        <input class="Vat_switch" type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </c:otherwise>
                            </c:choose>
                                ${incVatLabel}
                        </div>
                    </li>

                    <c:if test="${not empty cmsSite.contactUsNumber}">
                        <li class="firstLevelMenu hidden-md hidden-lg" aria-haspopup="true">

                            <div class="phoneNumber-mobile">
                                <span class="fas fa-phone"></span> <spring:theme code="text.contactus"
                                                                                 arguments="${cmsSite.contactUsNumber}"/>

                            </div>
                        </li>
                    </c:if>
					<li class="LangMobileMenu hidden-md hidden-lg">
                <span class="titleLangMobile "><spring:theme code="lang.text"/></span>

                <label class="container firstCheckout"><spring:theme code="english.lang"/>
                    <input type="radio" name="radioLang" value="EN">
                    <span class="checkmark"></span>
                </label>
                <label class="container"><spring:theme code="arabic.lang"/>
                    <input type="radio" name="radioLang" value="AR">
                    <span class="checkmark"></span>
                </label>
            </li>

                </ul>
            </nav>
            

        </div>
    </nav>
</c:if>
