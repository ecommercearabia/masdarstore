<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:if test="${cmsSite.enableRegistration == true}">
	<h1>${title}</h1>
	<div class="box">
		${content}
		<div class="row ">
			<div class="col-md-12 text-center">
				<div class="btn btn-primary">
					<cms:component component="${link}" />
				</div>
			</div>
		</div>
	</div>
</c:if>