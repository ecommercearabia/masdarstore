<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${ not empty pageTitle}">
	<c:set var="titlePage" value="${fn:split(pageTitle, '|')}" />
</c:if>
<div class="hidden">
	<div>
		<span> ${titlePage[0] }</span> <span></span>
	</div>
	<div class="ProductNumberFound"></div>
</div>

<div id="product-facet"
	class="hidden-sm hidden-xs product__facet js-product-facet  m-t-20">

	<div class="facetheader hidden-md hidden-lg">
		<spring:theme code="filter" text="filter" />
		<a href="javascript:;" class="closedivfacet"><i
			class="fas fa-times"></i></a>
	</div>
	<nav:facetNavAppliedFilters pageData="${searchPageData}" />
	<nav:facetNavRefinements pageData="${searchPageData}" />
</div>
