<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="tabhead hidden">
	<a href="">${fn:escapeXml(component.title)}</a><span class="glyphicon"></span>
</div>
<div class="tabbody hidden">${ycommerce:sanitizeHTML(component.content)}</div>
