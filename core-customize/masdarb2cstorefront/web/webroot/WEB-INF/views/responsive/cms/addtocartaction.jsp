<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:set var="selectedStock"
       value="${not empty shipmentTypeInfo.selectedShipmentType and shipmentTypeInfo.selectedShipmentType.code eq 'DELIVERY' ? product.stock : product.selectedPOSStock}"/>

<c:url value="${url}" var="addToCartUrl"/>
<c:url value="/cart/add/productConfig" var="addToCartUrlWithConfig"/>
<c:choose>
    <c:when test="${product.configurable && not empty configurations}">
        <form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrlWithConfig}">
            <c:if test="${product.purchasable}">
                <input type="hidden" maxlength="5" size="1" id="qty" name="qty" class="qty js-qty-selector-input"
                       value="1">
            </c:if>
            <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>
            <product:productConfiguratorTab configurations="${configurations}"/>
            <c:if test="${empty showAddToCart ? true : showAddToCart}">
                <c:set var="buttonType">button</c:set>
                <c:if test="${product.purchasable and selectedStock.stockLevelStatus.code ne 'outOfStock' }">
                    <c:set var="buttonType">submit</c:set>
                </c:if>
                <c:choose>
                    <c:when test="${fn:contains(buttonType, 'button')}">
                        <button type="${buttonType}" class="btn btn-primary btn-150-block js-add-to-cart outOfStock"
                                disabled="disabled">
                            <spring:theme code="product.variants.out.of.stock"/>
                        </button>
                    </c:when>
                    <c:otherwise>
                        <ycommerce:testId code="addToCartButton">
                            <button id="addToCartButton" type="${buttonType}"
                                    class="btn btn-primary btn-150-block js-add-to-cart js-enable-btn"
                                    disabled="disabled">
                                <spring:theme code="basket.add.to.basket"/>
                            </button>
                        </ycommerce:testId>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </form:form>
    </c:when>
    <c:otherwise>
        <form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">
            <c:if test="${product.purchasable}">
                <input type="hidden" maxlength="5" size="1" id="qty" name="qty" class="qty js-qty-selector-input"
                       value="1">
            </c:if>
            <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>

            <c:if test="${empty showAddToCart ? true : showAddToCart}">
                <c:set var="buttonType">button</c:set>
                <c:if test="${product.purchasable and selectedStock.stockLevelStatus.code ne 'outOfStock' }">
                    <c:set var="buttonType">submit</c:set>
                </c:if>
                <c:choose>
                    <c:when test="${fn:contains(buttonType, 'button')}">
                        <button type="${buttonType}" class="btn btn-primary btn-150-block js-add-to-cart outOfStock"
                                disabled="disabled">
                            <spring:theme code="product.variants.out.of.stock"/>
                        </button>
                    </c:when>
                    <c:otherwise>
                        <ycommerce:testId code="addToCartButton">
                            <button id="addToCartButton" type="${buttonType}"
                                    class="btn btn-primary btn-150-block js-add-to-cart js-enable-btn"
                                    disabled="disabled">
                                <spring:theme code="basket.add.to.basket"/>
                            </button>
                        </ycommerce:testId>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </form:form>
    </c:otherwise>
</c:choose>

