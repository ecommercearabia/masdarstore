<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-header">
	<div class="row">
		<div class="container-lg col-md-6">
			<spring:theme code="thirdParty.profile.complete" />
		</div>
	</div>
</div>

<div class="row">
	<div class="container-lg col-md-6">
		<div class="account-section-content">
			<div class="account-section-form">

				<form:form action="update-profile" method="post"
					modelAttribute="thirdPartyProfileForm">

					<c:if test="${empty thirdPartyProfileForm.firstName}">
						<formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" disabled="false" placeholder="profile.firstName"/>
					</c:if>
					<c:if test="${not empty thirdPartyProfileForm.firstName  }">
						<formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" disabled="true" placeholder="profile.firstName"/>
					</c:if>

					<c:if test="${empty thirdPartyProfileForm.lastName}">
						<formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" disabled="false" placeholder="profile.lastName"/>
					</c:if>
					<c:if test="${not empty thirdPartyProfileForm.lastName}">
						<formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" disabled="true" placeholder="profile.lastName"/>
					</c:if>

					<c:if test="${empty thirdPartyProfileForm.email}">
						<formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email" inputCSS="text" mandatory="true" disabled="false"  placeholder="profile.email"/>
					</c:if>
					
					<c:if test="${!empty thirdPartyProfileForm.email}">
						<formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email" inputCSS="text" mandatory="true" disabled="true"  placeholder="profile.email"/>
					</c:if>

					<c:if test="${cmsSite.birthOfDateCustomerEnabled}">
						<div class="<c:if test="${cmsSite.birthOfDateCustomerHidden}">hidden</c:if>">
							<formElement:formInputBox idKey="register.dob" labelKey="register.dob" inputCSS="form-control" mandatory="${cmsSite.birthOfDateCustomerRequired}" path="birthDate" placeholder="register.dob" />
						</div>
					</c:if>

					<c:if test="${cmsSite.customerMaritalStatusEnabled}">
						<div class="<c:if test="${cmsSite.customerMaritalStatusHidden}">hidden</c:if>">
						<formElement:formSelectBoxDefaultEnabled idKey="profile.maritalStatus" labelKey="profile.maritalStatus" selectCSSClass="form-control" path="maritalStatusCode" mandatory="${cmsSite.customerMaritalStatusRequired}" skipBlank="false" skipBlankMessageKey="form.select.none" items="${maritalStatuses}" />
						</div>
					</c:if>
					
					<formElement:formCountrySelectBoxDefaultEnabled idKey="profile.mobileCountry" labelKey="profile.mobileCountry" selectCSSClass="form-control" path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />

					<c:if test="${empty profile.mobileNumber}">
						<formElement:formInputBox idKey="profile.mobileNumber" labelKey="profile.mobileNumber" path="mobileNumber" inputCSS="form-control" mandatory="true" disabled="false" placeholder="profile.mobileNumber"/>
					</c:if>
					<c:if test="${not empty profile.mobileNumber}">
						<formElement:formInputBox idKey="profile.mobileNumber" labelKey="profile.mobileNumber" path="mobileNumber" inputCSS="form-control" mandatory="true" disabled="true" placeholder="profile.mobileNumber"/>
					</c:if>

					<c:if test="${cmsSite.nationalityCustomerEnabled}">
						<div class="<c:if test="${cmsSite.nationalityCustomerHidden}">hidden</c:if>">
							<formElement:formSelectBoxDefaultEnabled idKey="profile.nationality" labelKey="profile.nationality" selectCSSClass="form-control" path="nationality" mandatory="${cmsSite.nationalityCustomerRequired}" skipBlank="false" skipBlankMessageKey="form.select.none" items="${nationalities}" />
						</div>
					</c:if>

					<%-- <spring:theme code="profile.involvedInLoyaltyProgram" var="involvedInLoyaltyProgram" htmlEscape="false"/>
					<template:errorSpanField path="involvedInLoyaltyProgram">
						<div class="checkbox">
							<label class="control-label uncased">
								<span class="hidden">
									<form:checkbox id="involvedInLoyaltyProgram" path="involvedInLoyaltyProgram" disabled="false"/>
								</span>
								<label class="el-switch">
									<input  type="checkbox" name="involvedInLoyaltyProgram" id="involvedInLoyaltyProgram">
									<span class="el-switch-style"></span>
									<span class="margin-r">${ycommerce:sanitizeHTML(involvedInLoyaltyProgram)}</span>
								</label>	
							<div>
							</div>
							</label>
						</div>
					</template:errorSpanField> --%>	

					<c:if test="${cmsSite.nationalityIdCustomerEnabled}">

						<div class="<c:if test="${cmsSite.nationalityIdCustomerHidden}">hidden</c:if>">
							<div class="checkbox">
								<label class="control-label uncased"> 
									<span class="hidden"> 
										<form:checkbox id="nationalIdBox" path="hasNationalId" disabled="false" />
									</span> 
									<label class="el-switch"> 
										<input type="checkbox" name="switchNationalform" id="switchNationalform"> 
										<span class="el-switch-style"></span> 
										<span class="margin-r"><spring:theme code="profile.emirates.id.label" /></span>
									</label>
									<div class="FormNational">
										<formElement:formInputBox idKey="register.national" labelKey="register.national" placeholder="register.national" path="nationalityId" inputCSS="form-control" mandatory="${cmsSite.nationalityIdCustomerRequired}" />
										<button class="btn">
											<spring:theme code="Verify" />
										</button>
									</div>
								</label>
							</div>
						</div>
					</c:if>

					<div class="row">
						<div class="col-sm-6 col-sm-push-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_savePersonalDetails_button">
									<button type="submit" class="btn btn-primary btn-block">
										<spring:theme code="text.account.profile.saveUpdates" text="Save Updates" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
						<div class="col-sm-6 col-sm-pull-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_cancelPersonalDetails_button">
									<button type="button"
										class="btn btn-default btn-block backToHome">
										<spring:theme code="text.account.profile.cancel" text="Cancel" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>