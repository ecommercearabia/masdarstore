<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<template:page pageTitle="${pageTitle}">
         <cart:cartValidation/>
        <cms:pageSlot position="TopContent" var="feature" >
            <cms:component component="${feature}" />
        </cms:pageSlot>
       
        

        <cms:pageSlot position="BodyContent" var="feature" element="div" class="account-section-content  box m-t-20">
            <cms:component component="${feature}" />
        </cms:pageSlot>

        <cms:pageSlot position="BottomContent" var="feature" element="div" class="accountPageBottomContent  box m-t-20">
            <cms:component component="${feature}" />
        </cms:pageSlot>
</template:page>