<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<template:page pageTitle="${pageTitle}">


    <cms:pageSlot position="Section1" var="feature" element="div" class="product-grid-section1-slot">
        <cms:component component="${feature}" element="div"
                       class="yComponentWrapper map product-grid-section1-component"/>
    </cms:pageSlot>

    <div class="row list_view plpage" id="displaymode">
        <c:url value="/misc/plp/view/set?viewType=" var="linkurl"/>
        <input type="hidden" value="${linkurl}" id="acrionurl"/>

        <div class="col-xs-12 col-header">
            <c:if test="${ not empty pageTitle}">
                <c:set var="titlePage" value="${fn:split(pageTitle, '|')}"/>
            </c:if>

            <div class="productNumPlp">
                <div class="productTitlePlp">
                        ${titlePage[0] }
                </div>
                <div class="ProductNumberFound"></div>
            </div>
        </div>
        <div class="col-xs-3 col-md-4 col-lg-3">
            <cms:pageSlot position="ProductLeftRefinements" var="feature" element="div"
                          class="product-grid-left-refinements-slot">
                <cms:component component="${feature}" element="div"
                               class="yComponentWrapper product-grid-left-refinements-component"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-8 col-lg-9">
            <cms:pageSlot position="ProductGridSlot" var="feature" element="div" class="product-grid-right-result-slot">
                <cms:component component="${feature}" element="div"
                               class="product__list--wrapper yComponentWrapper product-grid-right-result-component"/>
            </cms:pageSlot>
        </div>
    </div>
    <storepickup:pickupStorePopup/>
</template:page>
