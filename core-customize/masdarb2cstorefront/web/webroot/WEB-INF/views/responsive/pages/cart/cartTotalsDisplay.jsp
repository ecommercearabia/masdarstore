<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-8 col-sm-offset-2 col-md-offset-0 vouher-code">
        <div >
        	<cart:cartPromotions cartData="${cartData}"/>
<cart:ajaxCartPromotions/>
            <cart:cartVoucher cartData="${cartData}"/>
        </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-12 col-sm-offset-2 col-md-offset-0 order-total">
        <div class="cart-totals box">
            <cart:cartTotals cartData="${cartData}" showTax="false"/>
            <cart:ajaxCartTotals/>
        </div>
    </div>
</div>