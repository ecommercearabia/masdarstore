<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


  <div class="col-md-12 account-section"> <div class="account-section-header">${categoryName}</div></div>
    
    <div class="col-md-12">
<div class="row categoryGroup">

<c:forEach items="${searchPageData.subCategories}" var="categoryData">
    <c:url value="${categoryData.url}" var="categoryURL"/>
    <div class="col-xs-12 col-sm-6 col-md-3 categoryItem">
       


            <c:url value="${categoryData.url}" var="categoryUrl"/>
            <c:if test="${ycommerce:validateUrlScheme(categoryData.image.url)}">
                <c:set var="imageurl" value="${fn:escapeXml(themeResourcePath)}/images/empty_cat.jpg"/>
            <c:if test="${not empty categoryData.image.url}">
                <c:set var="imageurl" value="${fn:escapeXml(categoryData.image.url)}"/>
            </c:if>
            <c:choose>
                    <c:when test="${empty categoryUrl || categoryUrl eq '#' || !ycommerce:validateUrlScheme(categoryUrl)}">
                        <a href="${categoryURL}">    
                    <img title="${fn:escapeXml(media.altText)}"
                             alt="${fn:escapeXml(categoryData.image.altText)}"
                             src="${imageurl}"/>
                             <div class="categoyNaPatternConstraintme">${categoryData.name}</div> 
                            </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${fn:escapeXml(categoryUrl)}" class="img-cont categoryImage"><img
                                title="${fn:escapeXml(categoryData.image.altText)}"
                                alt="${fn:escapeXml(categoryData.image.altText)}"
                                src="${imageurl}"/> 
                             <div class="categoyNaPatternConstraintme">${categoryData.name}</div> 
                            </a>
                    </c:otherwise>
                </c:choose>
            </c:if>

          
        

    </div>

</c:forEach>
</div>
</div>
