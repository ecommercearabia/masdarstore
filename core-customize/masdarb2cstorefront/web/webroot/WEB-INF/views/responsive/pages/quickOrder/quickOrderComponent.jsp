<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

					<spring:theme code="quick.order.component.title"/>

					<spring:theme code="quick.order.component.des"/>

<c:url value="/cart/add" var="addToCartUrl" />
<div class="AddToCart-AddToCartAction">
<form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">

<label class="control-label ">
			<spring:theme code="quick.order.component.add.to.basket" />
		</label>
                                  

<input  name="productCodePost" value=""/>
	<input   size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">

<button id="addToCartButton" type="${buttonType}" class="btn btn-primary btn-block js-add-to-cart js-enable-btn btn-icon glyphicon-shopping-cart" disabled="disabled">
					<spring:theme code="quick.order.component.add.to.basket"/>
				</button>
</form:form>
</div>
