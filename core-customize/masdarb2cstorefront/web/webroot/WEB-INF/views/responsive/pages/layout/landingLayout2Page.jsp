<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page2 pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature" element="div"   class="row">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section2" var="feature" element="div" class="row paddfull carousel-section">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  col-xs-12"/>
    </cms:pageSlot>

    <div class="section-gallary">
    <cms:pageSlot position="Section3" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  text-center col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <div class="row paddfull">
    <cms:pageSlot position="Section4A" var="feature" element="div" class="  col-sm-6 col-xs-12">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  "/>
    </cms:pageSlot>
    <cms:pageSlot position="Section4B" var="feature" element="div" class="  col-sm-6 col-xs-12">
        <cms:component component="${feature}" element="div" class="yComponentWrapper "/>
    </cms:pageSlot>
    </div>

    <cms:pageSlot position="Section5" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>

    <div class="row paddfull">
    <cms:pageSlot position="Section6A" var="feature" element="div" class="col-md-6 col-sm-6 col-xs-12">
        <cms:component component="${feature}" element="div" class="yComponentWrapper "/>
    </cms:pageSlot>
        <cms:pageSlot position="Section6B" var="feature" element="div" class="col-md-6 col-sm-6 col-xs-12">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>

    </div>
    <div class="row paddfull mobilesize">
    <cms:pageSlot position="Section7A" var="feature" element="div" class="col-sm-3 col-xs-6">
        <cms:component component="${feature}" element="div" class="yComponentWrapper  "/>
    </cms:pageSlot>
        <cms:pageSlot position="Section7B" var="feature" element="div" class="col-sm-3 col-xs-6">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>
        <cms:pageSlot position="Section7C" var="feature" element="div" class="col-sm-3 col-xs-6">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>
        <cms:pageSlot position="Section7D" var="feature" element="div" class="col-sm-3 col-xs-6">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>
    </div>
</div>
    <cms:pageSlot position="Section8" var="feature" element="div" class="row carousel-section  paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
    <cms:pageSlot position="Section9" var="feature" element="div" class="row paddfull">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
    </cms:pageSlot>
</template:page2>
