<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-sm-10 col-sm-offset-1 col-xs-12 ">
			<div class="box m-t-20">
				<div class="row">
					<div class="col-sm-12 col-xs-12">
			<cms:pageSlot position="LeftContentSlot" var="feature" element="div" class="login-left-content-slot">
				<cms:component component="${feature}"  element="div" class="login-left-content-component"/>
			</cms:pageSlot>
			<cms:pageSlot position="RightContentSlot" var="feature" element="div" class="login-right-content-slot">
				<cms:component component="${feature}"  element="div" class="login-right-content-component"/>
			</cms:pageSlot>
		</div>
				</div>
			</div>
		</div>
	</div>
</template:page>