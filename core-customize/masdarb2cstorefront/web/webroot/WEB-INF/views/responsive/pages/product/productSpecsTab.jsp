<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty product.classifications}">
	<div class="box m-t-20">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="tab-container">
					<product:productDetailsClassifications product="${product}" />
				</div>
			</div>
		</div>
	</div>
</c:if>

