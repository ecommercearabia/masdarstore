<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="TemplateReferral" value=""/>
<c:if test="${baseStore.referralCodeEnable }">
    <c:set var="TemplateReferral" value="referralCode"/>
</c:if>

<div class="row d-flex-account">

    <div class="col-md-3 col-sm-12">
        <div class="row">
            <%-- <div class=" col-xs-12">	<account:myOrders numberOfOrders="${numberOfOrders}"/></div> --%>
            <div class=" col-xs-12"><account:personalDetails customer="${customerData}"/></div>
            <div class=" col-xs-12"><account:emailAddress email="${customerData.email}" customerType="${customerData.customerType }"/></div>

        </div>
    </div>
    <div class="col-md-3 col-sm-12">
        <div class="row">
            <div class=" col-xs-12"><account:deliveryAddress
                    defaultAddress="${customerData.defaultShippingAddress}"/></div>
            <div class=" col-xs-12"><account:changePassword/></div>

        </div>
    </div>


    <div class="col-xs-12 col-md-6 order-section"><account:myOrders numberOfOrders="${numberOfOrders}"/></div>
</div>