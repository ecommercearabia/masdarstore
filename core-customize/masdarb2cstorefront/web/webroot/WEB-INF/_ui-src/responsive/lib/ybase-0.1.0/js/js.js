$('#change-country').selectpicker('refresh');
$('#mcountry').selectpicker('refresh');
$('#addresscountry').selectpicker('refresh');
$('select').selectpicker();
$("#mobileCountry option").each(function () {
    $(this).addClass("option-with-flag");
    $(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});
$(".js-mainHeader").sticky({topSpacing: 0});
$('.closedivproduct').click(function () {
    $('.addproductdiv').hide();

});
$('.closedivfacet').click(function () {
    $('#product-facet').removeClass('show-facet');

});

$("select[name='SelectproductQtyItemS']").each(function () {

    var val = $(this).data('value')
    $(this).val(val)
})
//$("select[name='SelectproductQtyItemS']").val($(this).data('value'))
$("select[name='SelectproductQtyItemS']").selectpicker('refresh');
$(".js-offcanvas-links").sticky({topSpacing: 0});


if ($('.smartEditComponent').length) {

    $('.sticky-wrapper').addClass('disable_sticky')
}


$("#nationality option").each(function () {
    console.log($(this).val().toLowerCase())
    $(this).addClass("option-with-flag");
    $(this).attr("data-content", "<span class='inline-flag flag " + $(this).val().toLowerCase() + "'></span><span class='text'>" + $(this).html() + "</span>");
});


$('#mobileCountry').selectpicker('refresh');
$('#nationality').selectpicker('refresh');
//add checked in radio in slot time
$("input[name='selector_selector']").click(function () {
    var data_code = $("input[name='selector_selector']:checked").data('code');
    var data_start = $("input[name='selector_selector']:checked").data('start');
    var data_end = $("input[name='selector_selector']:checked").data('end');
    var data_day = $("input[name='selector_selector']:checked").data('day');
    var data_date = $("input[name='selector_selector']:checked").data('date');
    $("#periodCode").attr("value", data_code)
    $("#start").attr("value", data_start)
    $("#end").attr("value", data_end);
    $("#day").attr("value", data_day);
    $("#date").attr("value", data_date);

});

//payment method on load
var payment_ul = $('#selectPaymentMethodForm ul li');
for (let li of payment_ul) {
    var nameClass = $(li).hasClass("selected")
    if (nameClass) {
        var input_pay = $("input[name='paymentmothed']").parent('label');
        for (let label_pay of input_pay) {
            if ($(label_pay).text().trim() == $(li).find('.text').text().trim()) {
                $(label_pay).find('input').attr('checked', true)


            }

        }
    }

}
//payment method in change value
$("input[name='paymentmothed']").click(function () {


    $("#selectPaymentMethodForm select.form-control").val($(this).val());


});


$('body').on('click', function (e) {
    //if()
    $('.bootstrap-select').removeClass('open');
    // $('.').addClass()
    var x = $(e.target).attr("class")
    if (x) {


        if ($(e.target).attr("class").includes('SelectitemUnit') || $(e.target).attr("class").includes('arrowUnitVal') || $(e.target).attr("class").includes('valueUnit')) {

            $(this).closest('.actions').find('.SelectproductQtyItem').removeClass('hidden')

        } else {
            if (!($(e.target).attr("class").includes('closeBox')))
                $('.SelectproductQtyItem').addClass('hidden')
        }
    } else {
        $('.SelectproductQtyItem').addClass('hidden')
    }


});


if ($('.pageLabel-orders').length) {
    var headerOrder = $('.account-section-header').text();
    var numproduct = $('.pagination-bar-results').html()
    $('.ProductNumberFound').html(numproduct)
    $('.titleotherPages').text(headerOrder)

}


let label_pass = $('.topnavlinks .password-holder .control-label').text().trim();
if ($('.confirmPass').length) {
    let labelCheck = $('.confirmPass label').text().trim()
    $('.confirmPass').attr("placeholder", labelCheck);
}
$('[id*="password"]').attr("placeholder", label_pass);

let label_passcheck = $('.checkPwd label').text().trim();
$('[id*="register\\.checkPwd"]').attr("placeholder", label_passcheck);


if ($('.pwdprofile').length) {
    let label_passcheckprofile = $('.pwdprofile label').text().trim();
    $('[id*="profile\\.pwd"]').attr("placeholder", label_passcheckprofile);
}

//new password page


if ($('.currentPassword').length) {
    let label_passcheckcurrent = $('.currentPassword label').text().trim();
    $('[id*="currentPassword"]').attr("placeholder", label_passcheckcurrent);
}
if ($('.newPassword').length) {
    let label_passcheckNew = $('.newPassword label').text().trim();
    $('[id*="newPassword"]').attr("placeholder", label_passcheckNew);
}
if ($('.checkNewPassword').length) {
    let label_passcheckcheck = $('.checkNewPassword label').text().trim();
    $('[id*="checkNewPassword"]').attr("placeholder", label_passcheckcheck);
}


//popup form_login in home_page
$(".registration .login_action").click(function () {
    $(".darkback").fadeIn("300", function () {
        $('.darkback').removeClass("hidden");
    });
    $(".login_box").fadeIn("300", function () {
        $('.login_box').removeClass("hidden");

    });

    $('.registration').addClass("click_border");

});

$('.darkback').click(function () {
    $(".login_box").fadeOut("medium").fadeOut("medium", function () {
        $('.login_box').addClass("hidden");
        $('.darkback').addClass("hidden");

    });
    $('.registration').removeClass("click_border");
});

if ($('.page-productGrid').length) {

    var numproduct = $('.pagination-bar-results').html()
    $('.ProductNumberFound').html(numproduct)
}
$('#register\\.dob').datepicker({
    language: 'en',
    dateFormat: 'dd/mm/yyyy'
});
if ($('#profile\\.dob').length) {

    var dateSelect = $('#profile\\.dob').val()
    $('#profile\\.dob').datepicker({
        language: 'en',
        dateFormat: 'dd/mm/yyyy',
        startDate: new Date($('#profile\\.dob').val())
    });


    $(".IconDate").click(function () {

        $('#register\\.dob').datepicker("show");
    });
}

/*var ias = $.ias({
    container: ".product__listing.product__grid",
    item: ".product-item",
    delay: 500,
    pagination: ".pagination-bar.top .pagination",
    next: ".pagination-bar.top .pagination-next a"
});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
    text: 'Load More',
    offset: '10',
    html: '<div class="ias-trigger ias-trigger-next" ><a class="btn btn-primary btn-block">{text}</a></div>'// override text when no pages left
})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
    text: '',
}));
$('.js-update-entry-quantity-input').attr('autocomplete', 'off');
ias.on('rendered', function () {


    ACC.product.bindToAddToCartForm();
    ACC.product.bindToAddToCartStorePickUpForm();
    ACC.product.enableStorePickupButton();
    ACC.product.enableAddToCartButton();
    lazyloadingAddtocart = false;
    $('.addproductitem').on('submit', function (e) {

        e.preventDefault();
        var formelement = $(this)
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'POST',


            data: formelement.serialize(),
            async: false,
            success: function (data) {

                ACC.product.displayAddToCartPopup(data, formelement)
            }
        });

    });
    $(".js-enable-btn").click(function () {


        var select_this_cart = $(this).parents(".actions")

        setTimeout(function () {
            var html_content_add_to_car_box = $("#addedtocartno").find('.cart_popup_error_msg').text();
            //console.log(html_content_add_to_car_box.length)
            if (html_content_add_to_car_box.length === 0 || html_content_add_to_car_box.length == 7) {

                var add_to_cart = select_this_cart.find('.addedtocart')

                $(add_to_cart).fadeIn("300", function () {
                    $(add_to_cart).css("display", "block");
                });
                setTimeout(() => $(add_to_cart).fadeOut("medium"), 2000);
            } else if (html_content_add_to_car_box.length === 49 || html_content_add_to_car_box.length == 34) {
                var add_to_cart = select_this_cart.find(".addedtocarterror");
                $(add_to_cart).fadeIn("300", function () {
                    $(add_to_cart).css("display", "block");
                });
                setTimeout(() => $(add_to_cart).fadeOut("medium"), 3000);

            } else if (html_content_add_to_car_box.length === 86 || html_content_add_to_car_box.length == 70) {
                var add_to_cart = select_this_cart.find(".addedtocartinfo");
                $(add_to_cart).fadeIn("300", function () {
                    $(add_to_cart).css("display", "block");
                });
                setTimeout(() => $(add_to_cart).fadeOut("medium"), 5000);

            }

        }, 250);


    });
//	if($('.stock-0.selected').length){
//		$('.stock-0.selected').each(function() {
//				var list = $(this).parents('.list');
//			$(list).find('.size_change').each(function(index) {
//
//				if($(this).data('stock') > 0){
//					$(this).trigger('click');
//					return false;
//
//
//				}
//
//			});
//
//
//		});
//		}

});
ias.on('load', function (event) {
    TweenMax.staggerTo('.slice', 1, {
        opacity: 1,
        repeat: 1000
    }, 0.1);
});*/


$('.link_ar').click(function () {
    $('.lang-selector').val('ar');
    $(".lang-form").submit();

})
$('.link_en').click(function () {
    $('.lang-selector').val('en');
    $(".lang-form").submit();


})


if ($('.page-update-profile').length) {

    var valueStatus = $('#profile\\.maritalStatus').val();

    if (valueStatus == 'MARRIED') {
        $('input[type=radio][name=radioMaritalprofile][value="MARRIED"]').prop('checked', true);
    } else if (valueStatus == 'SINGLE') {
        $('input[type=radio][name=radioMaritalprofile][value="SINGLE"]').prop('checked', true);

    }
    if ($('#involvedInLoyaltyProgram').is(":checked")) {
        $('#involvedInLoyalty').prop('checked', true);
    } else {
        $('#involvedInLoyalty').prop('checked', false);


    }


}


if ($('.register__section').length) {

    var textError = $('.checkboxMartal .has-error').find('.help-block').html();
    var valueStatus = $('#register\\.maritalStatus').val();

    if (valueStatus == 'MARRIED') {
        $('input[type=radio][name=radioMarital][value="MARRIED"]').prop('checked', true);
    } else if (valueStatus == 'SINGLE') {
        $('input[type=radio][name=radioMarital][value="SINGLE"]').prop('checked', true);

    }
    $('.checkboxMartal .help-block.custom').html(textError)
    $('.checkboxMartal .help-block.custom').removeClass('hidden')


}


$(".registration .login_action").click(function () {
    $(".darkback").fadeIn("300", function () {
        $('.darkback').removeClass("hidden");
    });
    $(".login_box,.tab_login_header").fadeIn("300", function () {
        $('.login_box').removeClass("hidden");
        $('.tab_login_header').removeClass("hidden");

    });

    $('.registration').addClass("click_border");

});

$('.darkback').click(function () {

    $(".login_box,.tab_login_header").fadeOut("medium").fadeOut("medium", function () {
        $('.tab_login_header').addClass("hidden");
        $('.login_box').addClass("hidden");


        $('.darkback').addClass("hidden");

    });
    $('.registration').removeClass("click_border");
});


$('body').on('click', '.google_action', function () {

    $(".abcRioButtonContentWrapper").click()
});


$('body').on('click', '.myAccountList', function () {

    $(".darkback").fadeIn("300", function () {
        $('.darkback').removeClass("hidden");
    });
    $(".NAVcompONENT,.accNavComponent__child-wrap,.tab_profile_header").fadeIn("300", function () {
        $('.NAVcompONENT').removeClass("display-none");
        $('.accNavComponent__child-wrap').removeClass("display-none");
        $('.tab_profile_header').removeClass("display-none");

    });


});

$('.darkback').click(function () {

    $(".login_box,.tab_login_header,.NAVcompONENT,.tab_profile_header").fadeOut("medium").fadeOut("medium", function () {
        $('.tab_login_header').addClass("hidden");
        $('.NAVcompONENT').addClass("display-none");
        $('.tab_profile_header').addClass("display-none");
        $('.accNavComponent__child-wrap').addClass("display-none");
        $('.login_box').addClass("hidden");
        $('.darkback').addClass("hidden");

    });
    $('.registration').removeClass("click_border");
});


$('input[type=radio][name=radioMarital]').change(function () {
    $('#register\\.maritalStatus').val(this.value)


    $('select').selectpicker();
});

$('input[type=radio][name=radioMaritalprofile]').change(function () {
    $('#profile\\.maritalStatus').val(this.value)


    $('select').selectpicker();
});


if ($('.language-en').length) {
    $('input[type=radio][name=radioLang][value="EN"]').prop('checked', true);
} else {
    $('input[type=radio][name=radioLang][value="AR"]').prop('checked', true);
}


$('input[type=radio][name=radioLang]').change(function () {
    if (this.value == 'EN') {

        $('.lang-selector').val('en');
        $(".lang-form").submit();
    } else if (this.value == 'AR') {
        $('.lang-selector').val('ar');
        $(".lang-form").submit();

    }
});


$('#switchNationalform').change(function () {


    if ($(this).is(":checked")) {
        $('.FormNational').css('display', 'inline-block')
    } else {

        $('.FormNational').css('display', 'none')
    }

});


$('#involvedInLoyalty').change(function () {


    if ($(this).is(":checked")) {
        $('#involvedInLoyaltyProgram').prop('checked', true);
    } else {
        $('#involvedInLoyaltyProgram').prop('checked', false);


    }

});


$('#registerChkTermsConditionGuest').change(function () {


    if ($(this).is(":checked")) {
        $('.registerBtn').prop("disabled", false);
        $('#registerChkTermsConditions').attr('checked', true);
    } else {

        $('.registerBtn').prop("disabled", 'disabled');
        $('#registerChkTermsConditions').attr('checked', false);

    }

});


$('#registerChkTermsConditionGuest').change(function () {
    $('#registerChkTermsConditionGuest').click();


    if ($(this).is(":checked")) {
        $('.accountActions-bottom .btnLogin').prop("disabled", false);
    } else {

        $('.accountActions-bottom .btnLogin').prop("disabled", 'disabled');

    }

});


$('#registerChkTermsCondition').change(function () {


    if ($(this).is(":checked")) {
        $('.registerBtn').prop("disabled", false);
        $('#registerChkTermsConditions').attr('checked', true);
    } else {

        $('.registerBtn').prop("disabled", 'disabled');
        $('#registerChkTermsConditions').attr('checked', false);

    }

});


$(".mobile-action .title").click(function () {
    $(".mobile-action .footer__nav--links").removeClass('activefooter');
    $('.contactDiv ul').removeClass('activefooter');
    $('.contactDiv').removeClass('activeClassIcon');
    $('.footer__nav--container').removeClass('activeClassIcon');
    $(this).parent('.footer__nav--container').find('.footer__nav--links').toggleClass('activefooter');
    $(this).parent('.footer__nav--container').addClass('activeClassIcon')
});


$('.contactDiv p').click(function () {

    $('.footer__nav--container').removeClass('activeClassIcon');
    $(".mobile-action .footer__nav--links").removeClass('activefooter');
    $('.contactDiv ul').addClass('activefooter');
    $('.contactDiv').addClass('activeClassIcon');
});


$('.hidden-md.CityClickBtn ').click(function () {


    $(".hidden-md.hidden-lg .deliverypopup").fadeOut("medium").fadeOut("medium", function () {
        $(".hidden-md.hidden-lg .deliverypopup").removeClass('hidden');

    })
});


$('.hidden-xs.CityClickBtn ').click(function () {


    $(".hidden-xs.hidden-sm .deliverypopup").fadeOut("medium").fadeOut("medium", function () {
        $(".hidden-xs.hidden-sm .deliverypopup").removeClass('hidden');

    })
});


$('.hidden-md.hidden-lg .CityClickBtn ').click(function () {


    $(".hidden-md.hidden-lg .deliverypopup").fadeOut("medium").fadeOut("medium", function () {
        $(".hidden-md.hidden-lg .deliverypopup").removeClass('hidden');

    })
});


$('.hidden-md.hidden-lg .deliverypopup').click(function () {


    $(".hidden-md.hidden-lg .deliverypopup").fadeIn("medium").fadeIn("medium", function () {
        $(".hidden-md.hidden-lg .deliverypopup").addClass('hidden');

    })

})

$(".hidden-md.hidden-lg .popupBoxdelivery").click(function (e) {
    // Do something
    e.stopPropagation();
});


$('.hidden-md.hidden-lg #searchInputText').keyup(function () {
    var input, filter, ul, li, a, i, txtValue;
    input = $(this).val();

    filter = input.toUpperCase();
    div = $(".hidden-md.hidden-lg #itemListdata");


    divItem = $(".hidden-md.hidden-lg .itemCity");


    for (i = 0; i < divItem.length; i++) {


        txtValue = divItem[i].textContent || divItem[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            divItem[i].parentNode.style.display = "";


        } else {
            divItem[i].parentNode.style.display = "none";
        }
    }
});


$('.hidden-md.hidden-lg .closepopupdelivery').click(function (e) {
    $('.hidden-md.hidden-lg .deliverypopup').click()

})


$('.hidden-sm.hidden-xs .CityClickBtn ').click(function () {


    $(".hidden-sm.hidden-xs .deliverypopup").fadeOut("medium").fadeOut("medium", function () {
        $(".hidden-sm.hidden-xs .deliverypopup").removeClass('hidden');

    })
});


$('.hidden-sm.hidden-xs .deliverypopup').click(function () {


    $(".hidden-sm.hidden-xs .deliverypopup").fadeIn("medium").fadeIn("medium", function () {
        $(".hidden-sm.hidden-xs .deliverypopup").addClass('hidden');

    })

})

$(".hidden-sm.hidden-xs .popupBoxdelivery").click(function (e) {
    // Do something
    e.stopPropagation();
});


$('.hidden-sm.hidden-xs #searchInputText').keyup(function () {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("searchInputText");
    filter = input.value.toUpperCase();
    div = document.getElementById("itemListdata");
    divItem = div.getElementsByClassName("itemCity");
    for (i = 0; i < divItem.length; i++) {


        txtValue = divItem[i].textContent || divItem[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            divItem[i].parentNode.style.display = "";


        } else {
            divItem[i].parentNode.style.display = "none";
        }
    }
});


$(".itemCity").click(async function (e) {
    var CId = $(this).attr('value');
    var result = await ACC.shipmenttype.bindShipmentType('DELIVERY', CId, '')
    if (result === true) {
        $('#cityId').val(CId)
        $('#cityForm').submit();
    } else {

        $().timedDialog({
            type: 'confirmation',
            title: 'Confirm action',
            body: result,
            width: 500,
            height: 500,
            btnConfirm: {
                text: ACC.shipmentPopupBtnConfirm,
                action: () => {
                    $('#cityId').val(CId)
                    $('#cityForm').submit();

                }
            }
        });
    }


})


$('.hidden-sm.hidden-xs .closepopupdelivery').click(function (e) {
    $('.hidden-sm.hidden-xs .deliverypopup').click()

})


$('body').on('click', '.AccountMobile_action', function () {

    $(".darkback").fadeIn("300", function () {
        $('.darkbackMobile').removeClass("hidden");
    });
    $(".mobileAccountBox ,.AccountNavmobile .tab_profile_header").fadeIn("300", function () {
        $('.mobileAccountBox').removeClass("hidden");

        $('.AccountNavmobile .tab_profile_header').removeClass("hidden");

    });


});

$('.darkbackMobile').click(function () {

    $(".mobileAccountBox ,.AccountNavmobile .tab_profile_header").fadeOut("medium").fadeOut("medium", function () {
        $('.mobileAccountBox').addClass("hidden");
        $('.AccountNavmobile .tab_profile_header').addClass("hidden");


    });

});


//filter plp

$('.js-facet-name').click(function () {

    if ($(this).find('.clickarrowFilter .fa-plus').length) {
        $(this).find('.clickarrowFilter .fa-plus').removeClass('fa-plus').addClass('fa-minus');

        $(this).parent().find('.facet__list').slideToggle("medium");

    } else {


        $(this).find('.clickarrowFilter .fa-minus').removeClass('fa-minus').addClass('fa-plus');
        $(this).parent().find('.facet__list').slideToggle("medium");

    }


})


$('.plus').click(function () {
    var form = $(this).closest('form');
    var productCode = form.find('input[name=quantity]').val();
    var initialCartQuantity = form.find('input[name=initialQuantity]').val();
    var newCartQuantity = parseFloat(initialCartQuantity) + 1;
    form.find('input[name=quantity]').val(newCartQuantity);

    form.submit();
});
$('.minus').click(function () {
    var form = $(this).closest('form');
    var productCode = form.find('input[name=productCode]').val();
    var initialCartQuantity = form.find('input[name=initialQuantity]').val();
    var newCartQuantity = parseFloat(initialCartQuantity) - 1;
    form.find('input[name=quantity]').val(newCartQuantity);

    form.submit();
});


if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    $('select').selectpicker('mobile');
}


//map in locationpicker

var lat = 23.58238473915374;
var long = 54.065219534058286;

if ($('#latitude').val()) {

    lat = $('#latitude').val()
}
if ($('#longitude').val()) {

    long = $('#longitude').val()
}
if (!($('.page-storefinderPage').length || $('.pageType-ProductPage').length)) {
    $('#us2').locationpicker({
        location: {
            latitude: lat,
            longitude: long
        },
        radius: 0,
        zoom: 8,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            locationNameInput: $('#us2-address')
        },
        enableAutocomplete: true,


    });
}


var checkarea;

$('.reLoadOrder').click(function () {
    location.reload();
});


//main menu Direction Desktop
$(document).ready(MenuDirection);

$(window).resize(MenuDirection);
jQuery(window).on('resize', function () {
    MenuDirection()
})

function MenuDirection() {
    if (jQuery(window).width() > 1023) {

        $('.firstLevelMenu').each(function () {
            var item = $(this)
            var of = item.offset(), // this will return the left and top
                left = of.left, // this will return left
                right = $(window).width() - left - item.width();

            if ($('.language-en').length) {


                // you can get right by calculate
                if (left > 284) {
                    item.find('.levelTwoContainer').removeClass('leftSideMenu').addClass('rightSideMenu')
                } else {
                    item.find('.levelTwoContainer').removeClass('rightSideMenu').addClass('leftSideMenu')
                }
                if (right < 568) {

                    item.find('.levelThreeContainer').addClass('leftSideMenu3')

                } else {
                    $(this).find('.levelThreeContainer').removeClass('leftSideMenu3')

                }
            } else {
                if (right > 284) {
                    item.find('.levelTwoContainer').removeClass('leftSideMenu').addClass('rightSideMenu')
                } else {
                    item.find('.levelTwoContainer').removeClass('rightSideMenu').addClass('leftSideMenu')
                }
                if (left < 568) {

                    item.find('.levelThreeContainer').addClass('leftSideMenu3')

                } else {
                    $(this).find('.levelThreeContainer').removeClass('leftSideMenu3')

                }

            }


        })
    }
}

$(document).on(' click', '.firstLevelItem', function () {
    if ($(this).parent().find('.levelTwoContainer').length) {
        if ($(this).find('.fa-chevron-down').length) {
            $(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        } else {
            $(this).find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        }
        $(this).parent().find('.levelTwoContainer').slideToggle('slow');
    }
});


$(document).on(' click', '.ChildLevelTwoItem', function () {
    if ($(this).parent().find('.levelThreeContainer').length) {
        if ($(this).find('.fa-chevron-down').length) {
            $(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        } else {
            $(this).find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        }
        $(this).parent().find('.levelThreeContainer').slideToggle('slow');
    }
});


$('.js-popup-wishlist').click(function () {


    $(".wishlistLogin").fadeOut("medium").fadeOut("medium", function () {
        $(".wishlistLogin").removeClass('hidden');

    })

})


$('body').on('click', '.wishlistLogin', function () {

    $('.wishlistLogin').fadeIn("medium").fadeIn("medium", function () {
        $('.wishlistLogin').addClass('hidden');

    })
});


$('body').on('click', '.wishlist_popup', function (e) {
    // Do something

    e.stopPropagation();
});
$('body').on('click', '.Close_wishlist', function () {
    $('.wishlistLogin').click()

})


$(document).ready(function () {
    var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    var $form = $('#mc-embedded-subscribe-form')
    if ($form.length > 0) {
        $('#mc-embedded-subscribe-form button[type="submit"]').bind('click', function (event) {
            if (event) event.preventDefault()
            if ($('#mce-EMAIL').val()) {
                console.log(ACC.errorsubscribe)
                if (email_regex.test($('#mce-EMAIL').val())) {

                    $('#mc-embedded-subscribe').addClass('Btnloading');
                    register($form)
                } else {
                    console.log('Please enter a valid email')
                    $('.msgFormSub').removeClass('hidden');
                    $('.msgFormSub').text(ACC.EnterEmailerrorsubscribe)
                    $('.msgFormSub').css('color', '#F44336');
                    $('#mce-EMAIL').css('border', '1px solid #F44336')
                }


            } else {
                console.log('Please enter a email')

                $('.msgFormSub').removeClass('hidden');
                $('.msgFormSub').html(ACC.EnterEmailerrorsubscribe)
                $('.msgFormSub').css('color', '#F44336');
                $('#mce-EMAIL').css('border', '1px solid #F44336')
            }
        })
    }
})

function register($form) {

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        error: function (err) {

            $('.msgFormSub').removeClass('hidden');
            $('.msgFormSub').html(ACC.EnterEmailerrorsubscribe)
            $('.msgFormSub').css('color', '#F44336');
            $('#mce-EMAIL').css('border', '1px solid #F44336');
            $('#mc-embedded-subscribe').removeClass('Btnloading');
        },


        success: function (data) {

            if (data.result === 'success') {
                // Yeahhhh Success
                $('.msgFormSub').removeClass('hidden');
                $('.msgFormSub').html(ACC.thanksubscribe)
                $('.msgFormSub').css('color', '#009C48')
                $('#mce-EMAIL').css('border', '1px solid #009C48')
                $('#mc-embedded-subscribe').removeClass('Btnloading');

                $('#mce-EMAIL').val('')
            } else {
                // Something went wrong, do something to notify the user.
                console.log(data.msg)
                $('#mce-EMAIL').css('border', '1px solid #F44336')


                $('.msgFormSub').removeClass('hidden');
                $('.msgFormSub').html(ACC.msgsubscribe)
                $('.msgFormSub').css('color', ' #d89514')
                $('#mce-EMAIL').css('border', '1px solid #d89514')
                $('#mc-embedded-subscribe').removeClass('Btnloading');


            }
        }
    })
};
$("input[name='storeCredit']").click(function () {
    var checked_item = $("input[name='storeCredit']:checked").attr('id');

    $("#sctCode").val(checked_item)
    if (checked_item == "REDEEM_SPECIFIC_AMOUNT") {
        $(".storeCreditAmount").removeClass("hidden");

    } else {

        $(".storeCreditAmount").addClass("hidden");

    }

});

if ($("input[name='storeCredit']").length) {

    var checked_item = $("input[name='storeCredit']:checked").attr('id');
    if (checked_item == "REDEEM_SPECIFIC_AMOUNT") {
        $(".storeCreditAmount").removeClass("hidden");

    } else {

        $(".storeCreditAmount").addClass("hidden");

    }

}


$("input[name='loyaltyPayment']").click(function () {
    var checked_item = $("input[name='loyaltyPayment']:checked").attr('id');
    $("#loyaltyPaymentCode").val(checked_item.substring(1))
    if (checked_item == "lREDEEM_SPECIFIC_AMOUNT") {
        $(".loyaltyPaymentAmount").removeClass("hidden");

    } else {

        $(".loyaltyPaymentAmount").addClass("hidden");

    }

});

if ($("input[name='loyaltyPayment']").length) {

    var checked_item = $("input[name='loyaltyPayment']:checked").attr('id');
    if (checked_item == "lREDEEM_SPECIFIC_AMOUNT") {
        $(".loyaltyPaymentAmount").removeClass("hidden");

    } else {

        $(".loyaltyPaymentAmount").addClass("hidden");

    }

}


$('#SelectproductQtyItem').change(function () {

    var itemSelect = $('#SelectproductQtyItem :selected')
    itemSelect = $(itemSelect);
    var value = itemSelect.data("value");
    var qty = itemSelect.data("qty");
    var stock = itemSelect.data("stock");
    var price = itemSelect.data("price");
    //console.log(value,qty,stock,price)
    var discountPrice = itemSelect.data("discountedprice");
    //$('.price').html(price);


    if (discountPrice) {
        $('.product-details .price').html(discountPrice);
        $('.product-details .scratched').removeClass('hidden')
        $('.product-details .line_dis').removeClass('hidden')
        $('.product-details .Pricescratched').html(price);

    } else {
        $('.product-details .price').html(price);
        $(' .product-details .scratched').addClass('hidden')


    }


    $('.js-qty-selector-input').val(qty);
    if (stock) {
        $('.InStock').removeClass('hidden');
        $('.OutOfStock').addClass('hidden');
        $(this).closest('.addtocart-component ').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")

    } else {

        $('.OutOfStock').removeClass('hidden');
        $('.InStock').addClass('hidden');
        $(this).closest('.addtocart-component ').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")
    }


});


$('.SelectitemUnit').click(function () {
    // console.log($(this).closest('.actions').find('.btnpdpAddtocart').attr('disabled'))
    $('.SelectproductQtyItem').addClass('hidden')
    $(this).closest('.actions').find('.SelectproductQtyItem').removeClass('hidden')

})

$('.closeBox i').click(function () {
    $(this).closest('.SelectproductQtyItem').addClass('hidden')

})

$('.itemSelectQty').click(function () {
    $(this).closest('.SelectproductQtyItem').addClass('hidden');
    var itemSelect = $(this)
    $('.itemSelectQty').removeClass('active')
    var value = itemSelect.data("value");
    var qty = itemSelect.data("qty");
    var stock = itemSelect.data("stock");
    var price = itemSelect.data("price");
    //console.log(value,qty,stock,price);

    var discountPrice = itemSelect.data("discountedprice");


    //console.log(value,qty,stock,price,discountPrice)

    //product-details


    if ($(this).closest('.product-item-content').length) {
        //1 plp

        $(this).addClass('active')
        $(this).closest('.actions').find('.valueUnit').html(value);
        $(this).closest('.actions').find('.js-qty-selector-input').val(qty);
        $(this).closest('.actions').find('input[name="productPostPrice"]').val(price);
        //$(this).closest('.product-item-content').find('.price').html(price);
        if (discountPrice) {
            $(this).closest('.product-item-content').find('.price-cont .price').html(discountPrice);
            $(this).closest('.product-item-content').find('.scratched').removeClass('hidden')
            $(this).closest('.product-item-content').find('.line_dis').removeClass('hidden')
            $(this).closest('.product-item-content').find('.Pricescratched').html(price);

        } else {
            $(this).closest('.product-item-content').find('.price-cont .price').html(price);
            $(this).closest('.product-item-content').find('.scratched').addClass('hidden')


        }


        if (stock) {

            $(this).closest('.product-item-content').find('.stockLevel').addClass('hidden');
            $(this).closest('.actions').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")
        } else {
            $(this).closest('.product-item-content').find('.stockLevel').removeClass('hidden');
            $(this).closest('.actions').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")


        }


    } else {
        if ($('.page-my-wishlist').length) {
            //wishlist

            $(this).addClass('active')
            $(this).closest('.actions').find('.valueUnit').html(value);

            $(this).closest('.item__list--item').find('.priceVAlueWish').html(price);

            $(this).closest('.actions').find('.js-qty-selector-input').val(qty);


            /*if(discountPrice){
                $(this).closest('.product-item-content').find('.price').html(discountPrice);
                $(this).closest('.product-item-content').find('.scratched').removeClass('hidden')
                $(this).closest('.product-item-content').find('.line_dis').removeClass('hidden')
                $(this).closest('.product-item-content').find('.Pricescratched').html(price);

            }else{
                $(this).closest('.product-item-content').find('.price').html(price);
                $(this).closest('.product-item-content').find('.scratched').addClass('hidden')


            }*/


            if (stock) {
                $(this).closest('.item__list--item').find('.InStock').removeClass('hidden');
                $(this).closest('.item__list--item').find('.OutOfStock').addClass('hidden');
                $(this).closest('.add_to_cart_form ').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")

            } else {

                $(this).closest('.item__list--item').find('.OutOfStock').removeClass('hidden');
                $(this).closest('.item__list--item').find('.InStock').addClass('hidden');
                $(this).closest('.add_to_cart_form').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")
            }

        } else {
            //carousel

            $(this).addClass('active')
            $(this).closest('.actions').find('.valueUnit').html(value);
            $(this).closest('.cont_detail_carousel').find('.carousel__item--price .price').html(price);
            $(this).closest('.actions').find('.js-qty-selector-input').val(qty);
            $(this).closest('.actions').find('input[name="productPostPrice"]').val(price);

            if (discountPrice) {
                $(this).closest('.cont_detail_carousel').find('.carousel__item--price .price').html(discountPrice);
                $(this).closest('.cont_detail_carousel').find('.scratched').removeClass('hidden')
                $(this).closest('.cont_detail_carousel').find('.line_dis').removeClass('hidden')
                $(this).closest('.cont_detail_carousel').find('.Pricescratched').html(price);

            } else {
                $(this).closest('.cont_detail_carousel').find('.carousel__item--price .price').html(price);
                $(this).closest('.cont_detail_carousel').find('.scratched').addClass('hidden')


            }


            if (stock) {

                $(this).closest('.carousel__item').find('.stockLevel').addClass('hidden');
                $(this).closest('.actions').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")
            } else {
                $(this).closest('.carousel__item').find('.stockLevel').removeClass('hidden');
                $(this).closest('.actions').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")


            }

        }
    }


})

$('select[name="SelectproductQtyItemS"]').change(function () {

    var itemSelect = $(this).find(' :selected');

    var value = itemSelect.data("value");
    var qty = itemSelect.data("qty");
    var stock = itemSelect.data("stock");
    var price = itemSelect.data("price");
    //console.log(value,qty,stock,price);

    $(this).closest('form').find('.js-update-entry-quantity-input').val(qty)
    $(this).closest('form').find('input[name="initialQuantity"]').val(qty)
    $(this).closest('.item__list--item').find('.PriceValue').html(price)
    $(this).closest('.item__list--item').find('.js-item-total ').html(price);

    var form = $(this).closest('form');
    var url = form.attr('action');

    //console.log(form.serialize())
    $.ajax({
        url: url,
        type: 'POST',
        data: form.serialize(),
        async: true,
        success: function (data) {
            // console.log('Done')

            var total_data = $(data).find('.js-cart-totals').html();
            $('.js-cart-totals').html(total_data);
            ACC.minicart.updateMiniCartDisplay();
        }


    });


//	 var value= itemSelect.data("value");
//		var qty= itemSelect.data("qty");
//		var stock= itemSelect.data("stock");
//		var price= itemSelect.data("price");
//		console.log(value,qty,stock,price)
//		$('.price').html(price);
//		$('.js-qty-selector-input').val(qty);
//


    //	if(stock){
//			$('.InStock').removeClass('hidden');
//			$('.OutOfStock').addClass('hidden');
//			$(this).closest('.addtocart-component ').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")
//
//		}else{
//
//			$('.OutOfStock').removeClass('hidden');
//			$('.InStock').addClass('hidden');
//			$(this).closest('.addtocart-component ').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")
//		}
//
//


})

//if($('.page-details-add-to-cart-component').length){
//	if($('#SelectproductQtyItem').length){
//		var val=$('#SelectproductQtyItem').val()
//		$('.js-qty-selector-input').val(val)
//
//
//	}
//
//
//}


//if($('.add_to_cart_form').length){
//
//
//	$('.valueUnit').each(function(){
//
//		var qty = $(this).closest('.pdpAddtocart').find('.SelectproductQtyItem .itemSelectQty.active').data('qty')
//
//		$(this).closest('.pdpAddtocart').find('.js-qty-selector-input').val(qty)
//	})
//
//	//$(this).closest('.pdpAddtocart').find('js-qty-selector-input').val()
//}


$(".FAQ-action").click(function () {
    $(".FAQ__nav--container .FAQ__nav--links").removeClass('activeFaq');
    $('.FAQ-action').find(".title span").removeClass('fa-minus').addClass('fa-plus');
    $(this).parent('.FAQ__nav--container').find('.FAQ__nav--links').toggleClass('activeFaq');
    $(this).parent('.FAQ__nav--container').find('.title span').removeClass('fa-plus').addClass('fa-minus')
});


/*
$('.SelectproductQtyItemWish').change(function() {
	//$('#SelectproductQtyItemWish').selectpicker();
	var itemSelect = $('.SelectproductQtyItemWish :selected');


	itemSelect =$(itemSelect);
	var value= itemSelect.data("value");
	var qty= itemSelect.data("qty");
	var stock= itemSelect.data("stock");
	var price= itemSelect.data("price");
	console.log(value,qty,stock,price)
	$(this).closest('.item__list--item').find('.priceVAlueWish').html(price);

	$(this).closest('.add_to_cart_form').find('.js-qty-selector-input').val(qty);
	if(stock){
		$(this).closest('.item__list--item').find('.instock').removeClass('hidden');
		$(this).closest('.item__list--item').find('.OutOfStock').addClass('hidden');
		$(this).closest('.add_to_cart_form ').find('.js-enable-btn.btnpdpAddtocart').removeAttr("disabled")

	}else{

		$(this).closest('.item__list--item').find('.OutOfStock').removeClass('hidden');
		$(this).closest('.item__list--item').find('.instock').addClass('hidden');
		$(this).closest('.add_to_cart_form').find('.js-enable-btn.btnpdpAddtocart').attr("disabled", "disabled")
	}


});
*/


$('#storelocator-query').keypress(function (e) {
    var code = e.keyCode || e.which;

    if (code === 13) {
        e.preventDefault();
        if (!$(this).val()) {
            return;
        } else {
            $('#storeFinderForm').submit();

        }

    }
    ;
});


$('#Terms1').on('click', function () {
    var checked = $('#Terms1').is(':checked');
    $("#placeOrder").prop("disabled", !checked);
});

//popup form_login in home_page
$(" .login_action").click(function () {
    $(".darkback").fadeIn("300", function () {
        $('.darkback').removeClass("hidden");
    });
    $(".login_box").fadeIn("300", function () {
        $('.login_box').removeClass("hidden");

    });

    //$('.registration').addClass("click_border");

});

$('.darkback').click(function () {
    $(".login_box").fadeOut("medium").fadeOut("medium", function () {
        $('.login_box').addClass("hidden");
        $('.darkback').addClass("hidden");

    });
    //$('.registration').removeClass("click_border");
});


//main menu Direction Desktop
$(document).ready(MenuDirection);

$(window).resize(MenuDirection);
jQuery(window).on('resize', function () {
    MenuDirection()
})

function MenuDirection() {
    if (jQuery(window).width() > 1023) {

        $('.firstLevelMenu').each(function (i) {

            var item = $(this)
            var of = item.offset(), // this will return the left and top
                left = of.left, // this will return left
                right = $(window).width() - left - item.width();

            if ($('.language-en').length) {


                // you can get right by calculate
                if (right < 244) {

                    item.find('.levelTwoContainer').removeClass('leftSideMenu').addClass('rightSideMenu')
                } else {

                    item.find('.levelTwoContainer').removeClass('rightSideMenu').addClass('leftSideMenu')
                }

            } else {
                if (right > 244) {
                    item.find('.levelTwoContainer').removeClass('leftSideMenu').addClass('rightSideMenu')
                } else {
                    item.find('.levelTwoContainer').removeClass('rightSideMenu').addClass('leftSideMenu')
                }


            }


        })
    }
}

$(document).on(' click', '.firstLevelItem', function () {
    if ($(this).parent().find('.levelTwoContainer').length) {
        console.log($(this).html())
        if (!$(this).find('.fa-chevron-down').length) {
            $(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        } else {
            $(this).find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        }
        //$(this).parent().find('.levelTwoContainer').slideToggle('slow');
    }
});

$('.js-facet-name').click(function () {

    if (!$(this).find('.clickarrowFilter .fa-plus').length) {
        $(this).find('.clickarrowFilter .fa-plus').removeClass('fa-plus').addClass('fa-minus');

        // $(this).parent().find('.facet__list').slideToggle("medium");

    } else {


        $(this).find('.clickarrowFilter .fa-minus').removeClass('fa-minus').addClass('fa-plus');
        //$(this).parent().find('.facet__list').slideToggle("medium");

    }


})


$('.Vat_switch').change(function () {


    var tax = true
    var value = $('#VAT').val();

    if (value === 'true') {
        tax = false
    }


    var VatUrl = ACC.config.encodedContextPath + '/misc/inctax';


    $.post(VatUrl, {'incTax': tax},
        function (returnedData) {
            //console.log(returnedData);
            location.reload();
        })

})


if ($('.page-RegistrationThankYouPage').length) {
    var value = $('#countSecondRedirect').val();


    var timeleft = Number(value);
    var downloadTimer = setInterval(function () {
        if (timeleft === 0) {

            $('.registerLinkRedirect')[0].click()
            clearInterval(downloadTimer);

        } else {
            timeleft -= 1;
            $('.number-of-second-redirect').html(timeleft);
        }


    }, 1000);

}


$("input[name='selector_selector']").click(function () {
    var data_code = $("input[name='selector_selector']:checked").data('code');
    var data_start = $("input[name='selector_selector']:checked").data('start');
    var data_end = $("input[name='selector_selector']:checked").data('end');
    var data_day = $("input[name='selector_selector']:checked").data('day');
    var data_date = $("input[name='selector_selector']:checked").data('date');
    $("#periodCode").attr("value", data_code)
    $("#start").attr("value", data_start)
    $("#end").attr("value", data_end);
    $("#day").attr("value", data_day);
    $("#date").attr("value", data_date);

});

//payment method on load
var payment_ul = $('#selectPaymentMethodForm ul li');
for (let li of payment_ul) {
    var nameClass = $(li).hasClass("selected")
    if (nameClass) {
        var input_pay = $("input[name='paymentmothed']").parent('label');
        for (let label_pay of input_pay) {
            if ($(label_pay).text().trim() == $(li).find('.text').text().trim()) {
                $(label_pay).find('input').attr('checked', true)


            }

        }
    }
}
//payment method in change value
$("input[name='paymentmothed']").click(function () {


    $("#selectPaymentMethodForm select.form-control").val($(this).val());


});
$("input[name='radioCartPickup']" ).bind( "click", async function() {
    var val = $(this).val();
    var result = await ACC.shipmenttype.bindShipmentType($(this).val(), '', '')
    if (result === true) {

        $('#shipmenttype-selector').val(val)
        $('#shipmenttype-form').submit();

    } else {

        $().timedDialog({
            type: 'confirmation',
            title: 'Confirm action',
            body: result,
            width: 500,
            height: 500,
            btnConfirm: {
                text: ACC.shipmentPopupBtnConfirm,
                action: () => {

                    $('#shipmenttype-selector').val(val)
                    $('#shipmenttype-form').submit();

                }
            }
        });
    }
});



$('body').on('click', '#add_to_cart_storepickup_form', async function (e) {
    e.preventDefault();
    var form = $(this)

    var storeId = $(this).find('.js-store-id').val()

    var result = await ACC.shipmenttype.bindShipmentType('PICKUP_IN_STORE', '', storeId)
    if (result === true) {

        form.submit();

    } else {

        $().timedDialog({
            type: 'confirmation',
            title: 'Confirm action',
            body: result,
            width: 500,
            height: 500,
            btnConfirm: {
                text: ACC.shipmentPopupBtnConfirm,
                action: () => {

                    form.submit();

                }
            }
        });
    }


})

$('body').on('click', '.userSign', function () {

    $('.loginmobileDiv').toggleClass('hidden')
})


$('.loginmobileDiv').click(function () {
    $('.loginmobileDiv').addClass('hidden')
})


$(document).ready(function () {

    if ($('.page-homepageb2c2').length &&  sessionStorage.getItem('popupOnce') !== 'true') {
        $('.popuphome').removeClass('hidden');
        sessionStorage.setItem('popupOnce','true');
          
        }
          else{
            $('.popuphome').addClass('hidden');
         
          } 
         
        $('.hidepopuphome').on('click',function(){
        
        $('.popuphome').addClass('hidden');
        });
        $('.hidepopuphome2').on('click',function(){
        
            $('.popuphome').addClass('hidden');
            });
        $('.popuplink').on('click',function(){
             event.preventDefault();
     
               window.location = $(this).attr('href');
        });
    });