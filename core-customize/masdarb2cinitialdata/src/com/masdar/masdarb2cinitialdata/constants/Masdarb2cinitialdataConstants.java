/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cinitialdata.constants;

/**
 * Global class for all Masdarb2cinitialdata constants.
 */
public final class Masdarb2cinitialdataConstants extends GeneratedMasdarb2cinitialdataConstants
{
	public static final String EXTENSIONNAME = "masdarb2cinitialdata";

	private Masdarb2cinitialdataConstants()
	{
		//empty
	}
}
