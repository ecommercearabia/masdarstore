/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2cinitialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.masdar.masdarb2cinitialdata.constants.Masdarb2cinitialdataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = Masdarb2cinitialdataConstants.EXTENSIONNAME)
public class MasdarB2CDataSystemSetup extends AbstractSystemSetup
{
	public static final String MASDARB2C = "masdarb2c";
	public static final String MASDAR = "masdar";


	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;

	@SystemSetupParameterMethod
	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

		return params;
	}

	//	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	//	public void createEssentialData(final SystemSetupContext context)
	//	{
	//		final List<ImportData> importData = new ArrayList<ImportData>();
	//
	//		final ImportData masdarb2cImportData = new ImportData();
	//		masdarb2cImportData.setProductCatalogName(MASDAR);
	//		masdarb2cImportData.setContentCatalogNames(Arrays.asList(MASDARB2C));
	//		masdarb2cImportData.setStoreNames(Arrays.asList(MASDARB2C));
	//		importData.add(masdarb2cImportData);
	//
	//		getCoreDataImportService().execute(this, context, importData);
	//		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	//
	//		getSampleDataImportService().execute(this, context, importData);
	//		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	//	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData masdarb2cImportData = new ImportData();
		masdarb2cImportData.setProductCatalogName(MASDAR);
		masdarb2cImportData.setContentCatalogNames(Arrays.asList(MASDARB2C));
		masdarb2cImportData.setStoreNames(Arrays.asList(MASDARB2C));
		importData.add(masdarb2cImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	}

	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}
}

