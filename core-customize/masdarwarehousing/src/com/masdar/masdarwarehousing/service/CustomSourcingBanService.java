/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.service;

import de.hybris.platform.warehousing.sourcing.ban.service.SourcingBanService;

import com.masdar.masdarwarehousing.model.SourcingBanConfigModel;



/**
 * @author monzer
 */
public interface CustomSourcingBanService extends SourcingBanService
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
