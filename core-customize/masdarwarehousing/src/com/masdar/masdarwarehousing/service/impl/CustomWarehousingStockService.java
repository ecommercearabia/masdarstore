/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.service.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.warehousing.stock.services.impl.DefaultWarehouseStockService;


/**
 *
 */
public class CustomWarehousingStockService extends DefaultWarehouseStockService
{

	@Override
	public Long getStockLevelForProductCodeAndWarehouse(final String productCode, final WarehouseModel warehouse)
	{
		final Long quantity = super.getStockLevelForProductCodeAndWarehouse(productCode, warehouse);

		return quantity == null ? 0L : quantity;
	}

}
