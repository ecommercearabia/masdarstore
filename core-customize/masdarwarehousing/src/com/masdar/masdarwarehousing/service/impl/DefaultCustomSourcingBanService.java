/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.service.impl;

import de.hybris.platform.warehousing.sourcing.ban.service.impl.DefaultSourcingBanService;

import javax.annotation.Resource;

import com.masdar.masdarwarehousing.dao.SourcingBanConfigurationDao;
import com.masdar.masdarwarehousing.model.SourcingBanConfigModel;
import com.masdar.masdarwarehousing.service.CustomSourcingBanService;

/**
 * @author monzer
 */
public class DefaultCustomSourcingBanService extends DefaultSourcingBanService implements CustomSourcingBanService
{

	@Resource(name = "sourcingBanConfigurationDao")
	private SourcingBanConfigurationDao sourcingBanConfigDao;

	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		return sourcingBanConfigDao.getSourcingBanConfiguration();
	}

}
