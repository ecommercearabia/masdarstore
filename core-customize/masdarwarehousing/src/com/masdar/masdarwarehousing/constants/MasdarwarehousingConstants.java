/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.constants;

/**
 * Global class for all Masdarwarehousing constants. You can add global constants for your extension into this class.
 */
public final class MasdarwarehousingConstants extends GeneratedMasdarwarehousingConstants
{
	public static final String EXTENSIONNAME = "masdarwarehousing";

	private MasdarwarehousingConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarwarehousingPlatformLogo";
}
