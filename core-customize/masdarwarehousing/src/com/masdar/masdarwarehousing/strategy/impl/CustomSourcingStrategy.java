/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.warehousing.data.sourcing.SourcingContext;
import de.hybris.platform.warehousing.data.sourcing.SourcingLocation;
import de.hybris.platform.warehousing.sourcing.strategy.AbstractSourcingStrategy;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.model.CityModel;
import com.masdar.masdarwarehousing.enums.CustomSourcingStrategyType;


/**
 *
 */
public class CustomSourcingStrategy extends AbstractSourcingStrategy
{

	private final Logger LOG = LoggerFactory.getLogger(CustomSourcingStrategy.class);

	@Override
	public void source(final SourcingContext sourcingContext)
	{
		final AbstractOrderModel order = getOrder(sourcingContext);
		final BaseStoreModel baseStore = getStore(order);

		if (order == null || baseStore == null)
		{
			LOG.warn("Ignoring Custom Sourcing Strategy due to Order[{}] or BaseStore[{}] are null", order, baseStore);
			return;
		}

		final CustomSourcingStrategyType sourcingStrategyType = baseStore.getSourcingStrategyType();


		if (sourcingStrategyType == null || CustomSourcingStrategyType.DEFAULT_SOURCING.equals(sourcingStrategyType))
		{
			return;
		}


		if (CustomSourcingStrategyType.BY_DEFAULT_WAREHOUSE.equals(sourcingStrategyType))
		{
			sourceUsingDefaultWarehouse(sourcingContext, baseStore);
			return;
		}


		if (CustomSourcingStrategyType.BY_SELECTED_CITY.equals(sourcingStrategyType))
		{
			sourceUsingSelectedCity(sourcingContext, order, baseStore);
			return;
		}


		LOG.warn("BaseStore Is using unimplemented CustomSourcingStrategyType[{}]", sourcingStrategyType);

	}

	/**
	 * @param baseStore2
	 *
	 */
	private void sourceUsingSelectedCity(final SourcingContext sourcingContext, final AbstractOrderModel order,
			final BaseStoreModel baseStore)
	{

		final CityModel selectedCity = getSelectedCity(order);
		// If the city is not selected continue with usual sourcing
		if (selectedCity == null)
		{
			return;
		}

		// Remove all sourcingLocations that is not in the selected City and continue with the sourcing
		final List<SourcingLocation> filteredWarehouses = sourcingContext
				.getSourcingLocations().stream().filter(e -> baseStore.getWarehouses().contains(e.getWarehouse())
						&& !Collections.isEmpty(e.getWarehouse().getCity()) && e.getWarehouse().getCity().contains(selectedCity))
				.collect(Collectors.toList());

		if (Collections.isEmpty(filteredWarehouses))
		{
			LOG.warn("sourceUsingSelectedCity: could not find any warehouse using City[{}] & Store [{}], sourcing will fail",
					selectedCity.getCode(), baseStore.getUid());
		}

		sourcingContext.setSourcingLocations(filteredWarehouses);

	}

	/**
	 *
	 */
	private void sourceUsingDefaultWarehouse(final SourcingContext sourcingContext, final BaseStoreModel baseStore)
	{
		final List<SourcingLocation> collect = sourcingContext.getSourcingLocations().stream()
				.filter(e -> e.getWarehouse().getDefault()).collect(Collectors.toList());

		if (Collections.isEmpty(collect))
		{
			LOG.warn("Could not find the default warehouse");
			return;
		}


		if (baseStore.isFilterDefaultWarehouseOnCustomSourcingStrategy())
		{
			sourcingContext.setSourcingLocations(collect);
			return;
		}


		if (collect.size() > 1)
		{
			LOG.info("Found Multiple Default warehouses [{}], choosing first one", collect);

		}
		final SourcingLocation sourcingLocation = collect.iterator().next();


		sourcingContext.getResult().getResults()
				.add(this.getSourcingResultFactory().create(sourcingContext.getOrderEntries(), sourcingLocation));
		if (!this.checkSourceCompleted(sourcingContext))
		{
			LOG.error("Sourcing is not complete");
			throw new IllegalArgumentException("Sourcing is not complete");
		}

		sourcingContext.getResult().setComplete(true);
	}

	/**
	 *
	 */
	private BaseStoreModel getStore(final AbstractOrderModel order)
	{
		if (order == null)
		{
			LOG.warn("Cannot get baseStore since Order Is null");
			return null;
		}

		if (order.getStore() == null)
		{
			LOG.warn("Cannot get baseStore since the Order[{}] doesn't contain a Store", order);
			return null;

		}

		return order.getStore();
	}

	/**
	 *
	 */
	private AbstractOrderModel getOrder(final SourcingContext sourcingContext)
	{
		if (Collections.isEmpty(sourcingContext.getOrderEntries()))
		{
			LOG.warn("Cannot Do soucring due to sourcingContext doesn't contain any orderEntry");
			return null;
		}


		final Optional<AbstractOrderEntryModel> abstractOrderEntryModel = sourcingContext.getOrderEntries().stream()
				.filter(e -> e.getOrder() != null).findAny();

		if (abstractOrderEntryModel.isEmpty())
		{
			LOG.warn("Cannot retrive OrderModel due to all orderEntries not containing an order");
			return null;
		}
		return abstractOrderEntryModel.get().getOrder();

	}


	private CityModel getSelectedCity(final AbstractOrderModel order)
	{
		return order.getSelectedCity();
	}

}
