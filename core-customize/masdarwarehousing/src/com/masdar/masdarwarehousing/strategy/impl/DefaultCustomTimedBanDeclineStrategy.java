/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.strategy.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.warehousing.allocation.decline.action.DeclineActionStrategy;
import de.hybris.platform.warehousing.data.allocation.DeclineEntry;
import de.hybris.platform.warehousing.enums.DeclineReason;

import java.util.Collection;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarwarehousing.model.SourcingBanConfigModel;
import com.masdar.masdarwarehousing.service.CustomSourcingBanService;


/**
 * The Class DefaultCustomTimedBanDeclineStrategy.
 *
 * @author monzer
 */
public class DefaultCustomTimedBanDeclineStrategy implements DeclineActionStrategy
{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCustomTimedBanDeclineStrategy.class);

	/** The sourcing ban service. */
	@Resource(name = "sourcingBanService")
	private CustomSourcingBanService sourcingBanService;

	/**
	 * Execute.
	 *
	 * @param declineEntry
	 *           the decline entry
	 */
	public void execute(final DeclineEntry declineEntry)
	{
		ServicesUtil.validateParameterNotNull(declineEntry, "Decline Entry cannot be null");
		LOGGER.debug("Default Decline Action Ban Strategy is being invoked, a Sourcing Ban will be created for warehouse: "
				+ declineEntry.getConsignmentEntry().getConsignment().getWarehouse());
		final SourcingBanConfigModel sourcingBanConfiguration = getSourcingBanService().getSourcingBanConfiguration();
		if (isAllowed(declineEntry, sourcingBanConfiguration))
		{
			this.getSourcingBanService().createSourcingBan(declineEntry.getConsignmentEntry().getConsignment().getWarehouse());
		}
	}

	/**
	 * Whether to ban source or not based on SourcingBanConfigModel.
	 *
	 * @param declineEntry
	 *           the decline entry
	 * @param sourcingBanConfiguration
	 *           the sourcing ban configuration
	 * @return true, if is allowed
	 */
	private boolean isAllowed(final DeclineEntry declineEntry, final SourcingBanConfigModel sourcingBanConfiguration)
	{
		if (sourcingBanConfiguration == null || !sourcingBanConfiguration.isSourcingBanAllowed())
		{
			return false;
		}

		if (DeclineReason.TOOBUSY.equals(declineEntry.getReason()))
		{
			return sourcingBanConfiguration.isSourcingBanDeclineReasonTooBusyAllowed();
		}
		else if (DeclineReason.STORECLOSED.equals(declineEntry.getReason()))
		{
			return sourcingBanConfiguration.isSourcingBanDeclineReasonStoreCloseAllowed();
		}
		else if (DeclineReason.OTHER.equals(declineEntry.getReason()))
		{
			return sourcingBanConfiguration.isSourcingBanDeclineReasonOtherAllowed();
		}
		else
		{
			return false;
		}
	}

	/**
	 * Execute.
	 *
	 * @param declineEntries
	 *           the decline entries
	 */
	public void execute(final Collection<DeclineEntry> declineEntries)
	{
		ServicesUtil.validateIfAnyResult(declineEntries, "Nothing to decline");
		final DeclineEntry declineEntry = declineEntries.iterator().next();
		this.execute(declineEntry);
	}

	/**
	 * Gets the sourcing ban service.
	 *
	 * @return the sourcing ban service
	 */
	protected CustomSourcingBanService getSourcingBanService()
	{
		return this.sourcingBanService;
	}

}
