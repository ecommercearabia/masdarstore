/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwarehousing.dao;

import com.masdar.masdarwarehousing.model.SourcingBanConfigModel;

/**
 * @author monzer
 */
public interface SourcingBanConfigurationDao
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
