/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarstorecredit.constants;

/**
 * Global class for all Masdarstorecredit constants. You can add global constants for your extension into this class.
 */
public final class MasdarstorecreditConstants extends GeneratedMasdarstorecreditConstants
{
	public static final String EXTENSIONNAME = "masdarstorecredit";

	private MasdarstorecreditConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
