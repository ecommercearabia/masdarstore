/**
 *
 */
package com.masdar.masdarstorecredit.dao;


import com.masdar.masdarstorecredit.enums.StoreCreditModeType;
import com.masdar.masdarstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeDao
{
	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);
}
