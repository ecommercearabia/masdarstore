/**
 *
 */
package com.masdar.masdarstorecredit.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.masdar.masdarstorecredit.enums.StoreCreditModeType;
import com.masdar.masdarstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeService
{
	public StoreCreditModeModel getStoreCreditMode(String storeCreditModeTypeCode);

	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);

	public List<StoreCreditModeModel> getSupportedStoreCreditModes(BaseStoreModel baseStoreModel);

	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore();
}
