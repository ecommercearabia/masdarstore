/**
 *
 */
package com.masdar.masdarstorecredit.exception;

import com.masdar.masdarstorecredit.exception.enums.StoreCreditExceptionType;


/**
 * @author yhammad
 *
 */
public class StoreCreditException extends RuntimeException
{

	StoreCreditExceptionType storeCreditExceptionType;


	public StoreCreditException(final String message, final StoreCreditExceptionType type)
	{
		super(message);
		this.storeCreditExceptionType = type;
	}

	/**
	 * @return the type
	 */
	public StoreCreditExceptionType getStoreCreditExceptionType()
	{
		return storeCreditExceptionType;
	}
}
