/**
 *
 */
package com.masdar.masdarstorecredit.exception.enums;

/**
 * @author yhammad
 *
 */
public enum StoreCreditExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
