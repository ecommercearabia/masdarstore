package com.masdar.masdarstorecredit.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.PriceValue;


public interface FindStoreCreditAmaountStrategy
{

	/**
	 *
	 */
	PriceValue getStoreCreditAmaount(AbstractOrderModel order, double finalTotalAmount);

}
