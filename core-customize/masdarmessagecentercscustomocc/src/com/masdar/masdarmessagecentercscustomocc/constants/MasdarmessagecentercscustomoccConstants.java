/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarmessagecentercscustomocc.constants;

/**
 * Global class for all masdarmessagecentercscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class MasdarmessagecentercscustomoccConstants extends GeneratedMasdarmessagecentercscustomoccConstants
{
	public static final String EXTENSIONNAME = "masdarmessagecentercscustomocc";

	private MasdarmessagecentercscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
