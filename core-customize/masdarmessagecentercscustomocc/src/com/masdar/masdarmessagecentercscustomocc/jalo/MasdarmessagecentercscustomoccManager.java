/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarmessagecentercscustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.masdarmessagecentercscustomocc.constants.MasdarmessagecentercscustomoccConstants;
import org.apache.log4j.Logger;

public class MasdarmessagecentercscustomoccManager extends GeneratedMasdarmessagecentercscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarmessagecentercscustomoccManager.class.getName() );
	
	public static final MasdarmessagecentercscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarmessagecentercscustomoccManager) em.getExtension(MasdarmessagecentercscustomoccConstants.EXTENSIONNAME);
	}
	
}
