/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaresalclientwebservice.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.masdar.masdaresalclientwebservice.constants.MasdaresalclientwebserviceConstants;

public class MasdaresalclientwebserviceManager extends GeneratedMasdaresalclientwebserviceManager
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( MasdaresalclientwebserviceManager.class.getName() );
	
	public static final MasdaresalclientwebserviceManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdaresalclientwebserviceManager) em.getExtension(MasdaresalclientwebserviceConstants.EXTENSIONNAME);
	}
	
}
