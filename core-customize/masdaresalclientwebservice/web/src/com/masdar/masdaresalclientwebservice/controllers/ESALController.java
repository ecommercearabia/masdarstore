/**
 *
 */
package com.masdar.masdaresalclientwebservice.controllers;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.masdar.masdarb2bpayment.esal.beans.webhook.paymentnotification.PaymentNotificationRequest;
import com.masdar.masdarb2bpayment.esal.beans.webhook.sadad.SadadBillIdRequest;
import com.masdar.masdarb2bpayment.esal.services.EsalClientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author mbaker
 *
 */
@Controller
@RequestMapping(value = "/")
@Api(tags = "ESAL")
public class ESALController
{

	private static final Logger LOG = LoggerFactory.getLogger(ESALController.class);

	@Resource(name = "esalClientService")
	private EsalClientService esalClientService;


	@RequestMapping(value = "/invoice/upload/notification", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(value = "SadadBill Id", notes = "SadadBill Id request", produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> sadadBillId(@ApiParam(value = "SadadBill Id ", required = true)
	@RequestBody
	final SadadBillIdRequest sadadBillIdRequest)
	{
		esalClientService.saveSadadBillId(sadadBillIdRequest);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(value = "Payment Notification", notes = "Payment Notification", produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> paymentNotification(@ApiParam(value = "Payment Notification request", required = true)
	@RequestBody
	final PaymentNotificationRequest paymentNotificationRequest)
	{
		waitABitForESAL();
		esalClientService.updatePayment(paymentNotificationRequest);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 *
	 */
	private void waitABitForESAL()
	{
		try
		{
			LOG.info("Waiting 5 seconds for esal before updateing the status to make sure esal created the invoice");
			Thread.sleep(5000);
		}
		catch (final InterruptedException e)
		{
			LOG.warn("Interrupted!", e);
			// Restore interrupted state...
			Thread.currentThread().interrupt();
		}

	}

	@RequestMapping(value = "/settlement", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(value = "Settlement Report", notes = "Settlement Report", produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> settlementReport(@ApiParam(value = "Settlement Report request", required = true)
	@RequestBody
	final Map<String, Object> settlementReportRequest)
	{
		System.out.println(settlementReportRequest);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
