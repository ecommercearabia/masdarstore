/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bcustomoccaddon.constants;

/**
 * Global class for all Masdarb2bcustomoccaddon constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bcustomoccaddonConstants extends GeneratedMasdarb2bcustomoccaddonConstants
{
	public static final String EXTENSIONNAME = "masdarb2bcustomoccaddon";
	public static final String OCC_REWRITE_OVERLAPPING_BASE_SITE_USER_PATH = "#{ ${occ.rewrite.overlapping.paths.enabled:false} ? '/{baseSiteId}/orgUsers/{userId}' : '/{baseSiteId}/users/{userId}'}";
	public static final String OCC_REWRITE_OVERLAPPING_PRODUCTS_PATH = "#{ ${occ.rewrite.overlapping.paths.enabled:false} ? '/{baseSiteId}/orgProducts' : '/{baseSiteId}/products'}";

	private Masdarb2bcustomoccaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
