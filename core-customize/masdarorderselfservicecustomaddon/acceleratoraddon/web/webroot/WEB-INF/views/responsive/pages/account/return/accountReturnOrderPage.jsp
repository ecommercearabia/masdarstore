<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="return" tagdir="/WEB-INF/tags/addons/masdarorderselfservicecustomaddon/responsive/return" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<div class="box">
<ycommerce:testId code="returnOrder_section">
    <return:accountReturnOrder order="${orderData}"/>
</ycommerce:testId>
</div>

