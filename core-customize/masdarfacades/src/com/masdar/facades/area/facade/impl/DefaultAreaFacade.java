/**
 *
 */
package com.masdar.facades.area.facade.impl;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.masdar.core.area.service.AreaService;
import com.masdar.core.city.service.CityService;
import com.masdar.core.model.AreaModel;
import com.masdar.core.model.CityModel;
import com.masdar.facades.area.facade.AreaFacade;



/**
 * The Class DefaultAreaFacade.
 *
 * @author mnasro
 */
public class DefaultAreaFacade implements AreaFacade
{

	/** The area service. */
	@Resource(name = "areaService")
	private AreaService areaService;

	/** The area service. */
	@Resource(name = "cityService")
	private CityService cityService;

	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;



	/**
	 * Gets the by city code.
	 *
	 * @param cityCode
	 *           the city code
	 * @return the by city code
	 */
	@Override
	public Optional<List<AreaData>> getByCityCode(final String cityCode)
	{
		if (Strings.isBlank(cityCode))
		{
			return Optional.empty();
		}
		final Optional<List<AreaModel>> areas = getAreaService().getByCityCode(cityCode);
		if (areas.isPresent())
		{
			final List<AreaData> areaList = getAreaConverter().convertAll(areas.get());

			Collections.sort(areaList, new Comparator<>()
			{
				public int compare(final AreaData a1, final AreaData a2)
				{
					return a1.getName().compareTo(a2.getName());
				}
			});


			return Optional.ofNullable(areaList);
		}
		return Optional.empty();
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<AreaData>> getAll()
	{
		final Optional<List<AreaModel>> areas = getAreaService().getAll();
		if (areas.isPresent())
		{
			return Optional.ofNullable(getAreaConverter().convertAll(areas.get()));
		}
		return Optional.empty();
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaData> get(final String code)
	{

		final Optional<AreaModel> area = getAreaService().get(code);
		if (area.isPresent())
		{
			return Optional.of(getAreaConverter().convert(area.get()));
		}
		return Optional.empty();
	}

	protected AreaService getAreaService()
	{
		return areaService;
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}

	@Override
	public List<AreaData> getRegistrationAreasByCityCode(final String cityCode)
	{
		final Optional<List<AreaModel>> areas = getAreaService().getByCityCode(cityCode);
		if (areas.isPresent())
		{
			return getAreaConverter()
					.convertAll(areas.get().stream().filter(e -> e.getPointOfService() != null).collect(Collectors.toList()));
		}
		return Collections.emptyList();
	}

	@Override
	public AreaData getDefaultRegistrationAreaByCityCode(final String cityCode)
	{
		final Optional<CityModel> optional = getCityService().get(cityCode);
		if (optional.isEmpty() || optional.get().getDefaultArea() == null)
		{
			return null;
		}

		return getAreaConverter().convert(optional.get().getDefaultArea());
	}

	public CityService getCityService()
	{
		return cityService;
	}
}
