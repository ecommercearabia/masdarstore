/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.country.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;


/**
 * @author ashati
 */
public class CountryPopulator implements Populator<CountryModel, CountryData>
{

	@Resource(name = "regionConverter")
	private Converter<RegionModel, RegionData> regionConverter;

	@Override
	public void populate(final CountryModel source, final CountryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setLatitude(source.getLatitude() == null ? "0.0" : source.getLatitude().toString());
		target.setLongitude(source.getLongitude() == null ? "0.0" : source.getLongitude().toString());

		target.setIsdcode(source.getIsdcode());
		target.setRegions(regionConverter.convertAll(source.getRegions()));
	}

}
