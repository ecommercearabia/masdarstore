/**
 *
 */
package com.masdar.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.warehousing.labels.context.PackContext;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.util.CollectionUtils;

import com.masdar.masdarfacades.data.SupplierData;


/**
 * @author Husam
 * @author monzer
 */
public class CustomPackContext extends PackContext
{
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "warehousingConsignmentFacade")
	private WarehousingConsignmentFacade warehousingConsignmentFacade;

	private static final Logger LOG = Logger.getLogger(CustomPackContext.class);

	public static final String NUMBER_TOOL = "numberTool";
	private static final String DELIVERY_COST = "deliveryCost";
	private static final String DELIVERY_COST_TAX = "deliveryCostTax";

	private Double deliveryCostTax;


	private String formatedDate;

	private OrderData orderDataEn;

	private OrderData orderDataAr;

	private CustomerData customerDataEn;

	private CustomerData customerDataAr;

	private Date deliveryDate;

	private ConsignmentData consignmentDataEn;
	private ConsignmentData consignmentDataAr;

	private SupplierData supplierData;

	private Locale saLocale;

	private TimeZone saTimeZone;


	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);

		saLocale = Locale.forLanguageTag("ar_SA");
		saTimeZone = TimeZone.getTimeZone("Asia/Riyadh");

		this.deliveryDate = DateUtils.addDays(getOrder().getDate(), 3);
		this.formatedDate = new DateTool().format(getConsignment().getInvoiceDate());
		fillOrderDataEnAndAr();
		fillCustomerDataEnAndAr();
		fillconsignmentDataEnAndAr();
		fillSupplierData(getOrder());

		put(NUMBER_TOOL, new NumberTool());

		put(DELIVERY_COST, getDeliveryCost());
		put(DELIVERY_COST_TAX, getDeliveryCostTax());

	}

	/**
	 *
	 */
	private void fillconsignmentDataEnAndAr()
	{
		final Locale currentLocale = i18nService.getCurrentLocale();
		String code = getConsignment().getCode();
		i18nService.setCurrentLocale(Locale.forLanguageTag("ar"));
		this.consignmentDataAr = warehousingConsignmentFacade.getConsignmentForCode(code);
		i18nService.setCurrentLocale(Locale.forLanguageTag("en"));
		this.consignmentDataEn = warehousingConsignmentFacade.getConsignmentForCode(code);
		code = code.replace("cons", "D");
		code = code.replace("_", "");
		this.consignmentDataAr.setCode(code);
		this.consignmentDataEn.setCode(code);

		i18nService.setCurrentLocale(currentLocale);

	}

	/**
	 *
	 */
	private void fillCustomerDataEnAndAr()
	{
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag("ar"));
		this.customerDataAr = orderDataAr.getB2bCustomerData();
		i18nService.setCurrentLocale(Locale.forLanguageTag("en"));
		this.customerDataEn = orderDataEn.getB2bCustomerData();
		i18nService.setCurrentLocale(currentLocale);
	}

	/**
	 *
	 */
	private void fillOrderDataEnAndAr()
	{
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag("ar"));
		this.orderDataAr = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		i18nService.setCurrentLocale(Locale.forLanguageTag("en"));
		this.orderDataEn = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		i18nService.setCurrentLocale(currentLocale);
	}

	private void fillSupplierData(final OrderModel order)
	{
		supplierData = new SupplierData();
		final Locale arabicLocale = Locale.forLanguageTag("ar");
		supplierData.setSupplierNameEn(order.getStore().getSupplierName(Locale.ENGLISH));
		supplierData.setSupplierNameAr(order.getStore().getSupplierName(arabicLocale));
		supplierData.setSupplierAddressEn(order.getStore().getSupplierAddress(Locale.ENGLISH));
		supplierData.setSupplierAddressAr(order.getStore().getSupplierAddress(arabicLocale));
		supplierData.setSupplierEmail(order.getStore().getSupplierEmail());
		supplierData.setSupplierPhoneNumber(order.getStore().getSupplierPhoneNumber());
		supplierData.setSupplierVatId(order.getStore().getSupplierVatId());
	}

	@Override
	public String getProductImageURL(final ConsignmentEntryModel consignmentEntryModel)
	{
		String path = super.getProductImageURL(consignmentEntryModel);
		if (path == null)
		{
			final ProductData product = productFacade
					.getProductForCodeAndOptions(consignmentEntryModel.getOrderEntry().getProduct().getCode(), null);

			if (product.getImages() != null && !product.getImages().isEmpty())
			{
				final Optional<ImageData> findFirst = product.getImages().stream()
						.filter(p -> p.getFormat().equals("thumbnail") && p.getImageType().equals(ImageDataType.PRIMARY)).findFirst();

				if (findFirst.isPresent())
				{
					path = findFirst.get().getUrl();
				}
			}
		}
		return path;
	}

	public String getProductDescription(final String productCode, final String lang)
	{
		if (productCode == null)
		{
			return null;
		}
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(lang));

		ProductData product = null;

		try
		{
			product = productFacade.getProductForCodeAndOptions(productCode,
					Arrays.asList(ProductOption.BASIC, ProductOption.DESCRIPTION));

		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);

		return product == null ? null : product.getDescription();

	}


	public String getProductName(final String productCode, final String lang)
	{
		if (productCode == null)
		{
			return null;
		}
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(lang));

		ProductData product = null;

		try
		{
			product = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC));

		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);

		return product == null ? null : product.getName();

	}


	public double roundPrice(final BigDecimal price, final int digits)
	{
		final double digitsWeight1 = Math.pow(10, digits + 1d);
		final double rounded = Math.round(price.doubleValue() * digitsWeight1) / digitsWeight1;
		final double digitsWeight2 = Math.pow(10, digits);
		return Math.round(rounded * digitsWeight2) / digitsWeight2;
	}

	/**
	 * @return the orderDataEn
	 */
	public OrderData getOrderDataEn()
	{
		return orderDataEn;
	}

	/**
	 * @param orderDataEn
	 *           the orderDataEn to set
	 */
	public void setOrderDataEn(final OrderData orderDataEn)
	{
		this.orderDataEn = orderDataEn;
	}

	/**
	 * @return the orderDataAr
	 */
	public OrderData getOrderDataAr()
	{
		return orderDataAr;
	}

	/**
	 * @param orderDataAr
	 *           the orderDataAr to set
	 */
	public void setOrderDataAr(final OrderData orderDataAr)
	{
		this.orderDataAr = orderDataAr;
	}

	/**
	 * @return the customerDataEn
	 */
	public CustomerData getCustomerDataEn()
	{
		return customerDataEn;
	}

	/**
	 * @param customerDataEn
	 *           the customerDataEn to set
	 */
	public void setCustomerDataEn(final CustomerData customerDataEn)
	{
		this.customerDataEn = customerDataEn;
	}

	/**
	 * @return the customerDataAr
	 */
	public CustomerData getCustomerDataAr()
	{
		return customerDataAr;
	}

	/**
	 * @param customerDataAr
	 *           the customerDataAr to set
	 */
	public void setCustomerDataAr(final CustomerData customerDataAr)
	{
		this.customerDataAr = customerDataAr;
	}

	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}

	/**
	 * @param deliveryDate
	 *           the deliveryDate to set
	 */
	public void setDeliveryDate(final Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the supplierData
	 */
	public SupplierData getSupplierData()
	{
		return supplierData;
	}

	/**
	 * @param supplierData
	 *           the supplierData to set
	 */
	public void setSupplierData(final SupplierData supplierData)
	{
		this.supplierData = supplierData;
	}

	/**
	 * @return the consignmentDataEn
	 */
	public ConsignmentData getConsignmentDataEn()
	{
		return consignmentDataEn;
	}

	/**
	 * @param consignmentDataEn
	 *           the consignmentDataEn to set
	 */
	public void setConsignmentDataEn(final ConsignmentData consignmentDataEn)
	{
		this.consignmentDataEn = consignmentDataEn;
	}

	/**
	 * @return the consignmentDataAr
	 */
	public ConsignmentData getConsignmentDataAr()
	{
		return consignmentDataAr;
	}

	/**
	 * @param consignmentDataAr
	 *           the consignmentDataAr to set
	 */
	public void setConsignmentDataAr(final ConsignmentData consignmentDataAr)
	{
		this.consignmentDataAr = consignmentDataAr;
	}

	/**
	 * @return the formatedDate
	 */
	public String getFormatedDate()
	{
		return formatedDate;
	}

	/**
	 * @param formatedDate
	 *           the formatedDate to set
	 */
	public void setFormatedDate(final String formatedDate)
	{
		this.formatedDate = formatedDate;
	}

	/**
	 * @return the saLocale
	 */
	public Locale getSaLocale()
	{
		return saLocale;
	}

	/**
	 * @param saLocale
	 *           the saLocale to set
	 */
	public void setSaLocale(final Locale saLocale)
	{
		this.saLocale = saLocale;
	}

	/**
	 * @return the saTimeZone
	 */
	public TimeZone getSaTimeZone()
	{
		return saTimeZone;
	}

	/**
	 * @param saTimeZone
	 *           the saTimeZone to set
	 */
	public void setSaTimeZone(final TimeZone saTimeZone)
	{
		this.saTimeZone = saTimeZone;
	}

	public double getDeliveryCost()
	{
		final ConsignmentModel consignment = getConsignment();
		final OrderModel order = getOrder();

		if (order.getDeliveryCostAppliedOnConsignment() != null
				&& consignment.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			return order.getDeliveryCost() == null ? 0.0d : order.getDeliveryCost().doubleValue();
		}

		return 0.0d;
	}

	public double getDeliveryCostTax()
	{
		final OrderModel order = getOrder();
		final double deliveryCost = getDeliveryCost();

		final double taxVal = order == null || CollectionUtils.isEmpty(order.getTotalTaxValues()) ? 0.0d
				: order.getTotalTaxValues().iterator().next().getValue();

		return deliveryCost * taxVal / 100;
	}

}
