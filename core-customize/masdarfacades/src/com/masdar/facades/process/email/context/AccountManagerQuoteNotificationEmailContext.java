/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.QuoteProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.masdar.core.service.CustomUserService;


/**
 * Velocity context for a quote notification email.
 */
public class AccountManagerQuoteNotificationEmailContext extends AbstractEmailContext<QuoteProcessModel>
{
	private QuoteService quoteService;

	private Converter<QuoteModel, QuoteData> quoteConverter;

	private QuoteData quoteData;

	public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+\\b";

	@Resource(name = "customUserService")
	private transient CustomUserService customUserService;

	private CustomerData customerData;

	protected boolean validateEmailAddress(final String email)
	{
		if (StringUtils.isBlank(email))
		{
			return false;
		}

		final Matcher matcher = Pattern.compile(EMAIL_REGEX).matcher(email);
		return matcher.matches();
	}

	@Override
	public void init(final QuoteProcessModel quoteProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(quoteProcessModel, emailPageModel);
		final QuoteModel quote = getQuote(quoteProcessModel);
		final B2BCustomerModel customer = ((B2BCustomerModel) quote.getUser());
		;
		final B2BUnitModel unit = customUserService.getRootB2BUnitHaveAccountManagerOrAccountManagerGroups(customer,
				customer.getAllGroups());
		final EmployeeModel accountManager = unit != null ? unit.getAccountManager() : null;

		put(TITLE, "");
		put(DISPLAY_NAME, "");
		put(EMAIL, "");

		if (accountManager != null)
		{
			final String email = accountManager.getAccountManagerEmail();
			if (validateEmailAddress(email))
			{
				put(TITLE, (accountManager.getName() != null ? accountManager.getName() : ""));
				put(DISPLAY_NAME, (accountManager.getDisplayName() != null ? accountManager.getDisplayName() : ""));
				put(EMAIL, email);
			}
		}

		quoteData = getQuoteConverter().convert(quote);
		customerData = quoteData.getB2bCustomerData();
	}

	public QuoteData getQuote()
	{
		return quoteData;
	}

	@Override
	protected BaseSiteModel getSite(final QuoteProcessModel quoteProcessModel)
	{
		return getQuote(quoteProcessModel).getSite();
	}

	@Override
	protected CustomerModel getCustomer(final QuoteProcessModel quoteProcessModel)
	{
		return (CustomerModel) getQuote(quoteProcessModel).getUser();
	}

	@Override
	protected LanguageModel getEmailLanguage(final QuoteProcessModel quoteProcessModel)
	{
		return getCustomer(quoteProcessModel).getSessionLanguage();
	}

	protected QuoteModel getQuote(final QuoteProcessModel quoteProcessModel)
	{
		return Optional.of(quoteProcessModel).map(QuoteProcessModel::getQuoteCode).map(getQuoteService()::getCurrentQuoteForCode)
				.orElseThrow();
	}

	@Required
	public void setQuoteService(final QuoteService quoteService)
	{
		this.quoteService = quoteService;
	}

	protected QuoteService getQuoteService()
	{
		return quoteService;
	}

	@Required
	public void setQuoteConverter(final Converter<QuoteModel, QuoteData> quoteConverter)
	{
		this.quoteConverter = quoteConverter;
	}

	protected Converter<QuoteModel, QuoteData> getQuoteConverter()
	{
		return quoteConverter;
	}

	/**
	 * @return the customerData
	 */
	public CustomerData getCustomerData()
	{
		return customerData;
	}


}
