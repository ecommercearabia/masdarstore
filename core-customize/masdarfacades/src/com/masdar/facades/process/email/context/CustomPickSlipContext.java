/**
 *
 */
package com.masdar.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.warehousing.labels.context.PickSlipContext;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.NumberTool;


/**
 * @author Husam
 *
 */
public class CustomPickSlipContext extends PickSlipContext
{
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;
	@Resource(name = "productFacade")
	private ProductFacade productFacade;
	@Resource(name = "i18nService")
	private I18NService i18nService;
	private OrderData orderDataEn;

	private OrderData orderDataAr;

	private CustomerData customerDataEn;

	private CustomerData customerDataAr;

	private Date deliveryDate;

	@Resource(name = "warehousingConsignmentFacade")
	private WarehousingConsignmentFacade warehousingConsignmentFacade;

	private ConsignmentData consignmentData;

	private Long entriesQuantity;

	private Locale saLocale;

	private TimeZone saTimeZone;

	private static final Logger LOG = Logger.getLogger(CustomPickSlipContext.class);
	public static final String NUMBER_TOOL = "numberTool";
	private static final String DELIVERY_COST = "deliveryCost";

	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);

		this.deliveryDate = DateUtils.addDays(getOrder().getDate(), 3);
		saLocale = Locale.forLanguageTag("ar_SA");
		saTimeZone = TimeZone.getTimeZone("Asia/Riyadh");

		String code = getConsignment().getCode();
		this.consignmentData = warehousingConsignmentFacade.getConsignmentForCode(getConsignment().getCode());
		code = code.replace("cons", "D");
		code = code.replace("_", "");
		consignmentData.setCode(code);

		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag("ar"));
		this.orderDataAr = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		this.customerDataAr = orderDataAr.getB2bCustomerData();
		i18nService.setCurrentLocale(Locale.forLanguageTag("en"));
		this.orderDataEn = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		this.customerDataEn = orderDataEn.getB2bCustomerData();
		i18nService.setCurrentLocale(currentLocale);
		this.entriesQuantity = consignmentData.getEntries().stream().mapToLong(entry -> entry.getQuantity()).sum();
		put(NUMBER_TOOL, new NumberTool());
		put(DELIVERY_COST, getDeliveryCost());
		//CustomerRegistration/cr

	}

	@Override
	public String getProductImageURL(final ConsignmentEntryModel consignmentEntryModel)
	{
		String path = super.getProductImageURL(consignmentEntryModel);
		if (path == null)
		{
			final ProductData product = productFacade
					.getProductForCodeAndOptions(consignmentEntryModel.getOrderEntry().getProduct().getCode(), null);

			if (product.getImages() != null && !product.getImages().isEmpty())
			{
				final Optional<ImageData> findFirst = product.getImages().stream()
						.filter(p -> p.getFormat().equals("thumbnail") && p.getImageType().equals(ImageDataType.PRIMARY)).findFirst();

				if (findFirst.isPresent())
				{
					path = findFirst.get().getUrl();
				}
			}
		}
		return path;
	}

	public String getProductDescription(final String productCode, final String lang)
	{
		if (productCode == null)
		{
			return null;
		}
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(lang));

		ProductData product = null;

		try
		{
			product = productFacade.getProductForCodeAndOptions(productCode,
					Arrays.asList(ProductOption.BASIC, ProductOption.DESCRIPTION));

		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);

		return product == null ? null : product.getDescription();

	}

	/**
	 * @return the orderDataEn
	 */
	public OrderData getOrderDataEn()
	{
		return orderDataEn;
	}

	/**
	 * @param orderDataEn
	 *           the orderDataEn to set
	 */
	public void setOrderDataEn(final OrderData orderDataEn)
	{
		this.orderDataEn = orderDataEn;
	}

	/**
	 * @return the orderDataAr
	 */
	public OrderData getOrderDataAr()
	{
		return orderDataAr;
	}

	/**
	 * @param orderDataAr
	 *           the orderDataAr to set
	 */
	public void setOrderDataAr(final OrderData orderDataAr)
	{
		this.orderDataAr = orderDataAr;
	}

	/**
	 * @return the customerDataEn
	 */
	public CustomerData getCustomerDataEn()
	{
		return customerDataEn;
	}

	/**
	 * @param customerDataEn
	 *           the customerDataEn to set
	 */
	public void setCustomerDataEn(final CustomerData customerDataEn)
	{
		this.customerDataEn = customerDataEn;
	}

	/**
	 * @return the customerDataAr
	 */
	public CustomerData getCustomerDataAr()
	{
		return customerDataAr;
	}

	/**
	 * @param customerDataAr
	 *           the customerDataAr to set
	 */
	public void setCustomerDataAr(final CustomerData customerDataAr)
	{
		this.customerDataAr = customerDataAr;
	}

	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}

	/**
	 * @param deliveryDate
	 *           the deliveryDate to set
	 */
	public void setDeliveryDate(final Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the consignmentData
	 */
	public ConsignmentData getConsignmentData()
	{
		return consignmentData;
	}

	/**
	 * @param consignmentData
	 *           the consignmentData to set
	 */
	public void setConsignmentData(final ConsignmentData consignmentData)
	{
		this.consignmentData = consignmentData;
	}

	/**
	 * @return the entriesQuantity
	 */
	public Long getEntriesQuantity()
	{
		return entriesQuantity;
	}

	/**
	 * @param entriesQuantity
	 *           the entriesQuantity to set
	 */
	public void setEntriesQuantity(final Long entriesQuantity)
	{
		this.entriesQuantity = entriesQuantity;
	}

	/**
	 * @return the saLocale
	 */
	public Locale getSaLocale()
	{
		return saLocale;
	}

	/**
	 * @param saLocale
	 *           the saLocale to set
	 */
	public void setSaLocale(final Locale saLocale)
	{
		this.saLocale = saLocale;
	}

	/**
	 * @return the saTimeZone
	 */
	public TimeZone getSaTimeZone()
	{
		return saTimeZone;
	}

	public String getProductName(final String productCode, final String lang)
	{
		if (productCode == null)
		{
			return null;
		}
		final Locale currentLocale = i18nService.getCurrentLocale();
		i18nService.setCurrentLocale(Locale.forLanguageTag(lang));

		ProductData product = null;

		try
		{
			product = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC));

		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
		i18nService.setCurrentLocale(currentLocale);

		return product == null ? null : product.getName();

	}

	/**
	 * @param saTimeZone
	 *           the saTimeZone to set
	 */
	public void setSaTimeZone(final TimeZone saTimeZone)
	{
		this.saTimeZone = saTimeZone;
	}

	public double getDeliveryCost()
	{
		final ConsignmentModel consignment = getConsignment();
		final OrderModel order = getOrder();

		if (order.getDeliveryCostAppliedOnConsignment() != null
				&& consignment.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			return order.getDeliveryCost() == null ? 0.0d : order.getDeliveryCost().doubleValue();
		}

		return 0.0d;
	}

}
