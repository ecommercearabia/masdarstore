/**
 *
 */
package com.masdar.facades.delivery;

import java.util.Optional;

import com.masdar.masdarfacades.customer.DeliveryCityAreaData;
import com.masdar.masdarfacades.customer.TimeDeliveryLocationData;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;


/**
 * @author mohammedBaker
 *
 */
public interface DeliveryLocationService
{
	public Optional<TimeDeliveryLocationData> saveCustomerDeliveryLocation(final String AreaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationData(final String areaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationDataForCurrentCustomer() throws TimeSlotException;

	Optional<DeliveryCityAreaData> getSelectedDeliveryCityAndArea();
}
