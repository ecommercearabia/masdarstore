/**
 *
 */
package com.masdar.facades.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author monzer
 *
 */
public class PriceCalculationUtil
{

	private PriceCalculationUtil()
	{
	}

	public static double calculateTax(final double priceAfterDiscount, final double[] percantages)
	{
		if (percantages == null)
		{
			return 0;
		}
		double totalTax = 0;
		for (final double percentage : percantages)
		{
			totalTax += (priceAfterDiscount * (percentage / 100));
		}
		return totalTax;
	}

	public static double calculateDiscountPerEntry(final double totalDiscount, final Long baseQuantity,
			final Long invoiceQuantity)
	{
		return baseQuantity == 0 ? 0 : (totalDiscount / baseQuantity) * invoiceQuantity;
	}

	public static double getTotalBasePrice(final long quantity, final double basePrice)
	{
		return quantity * basePrice;
	}

	public static double getRoundedUp(final int digitScale, final double value)
	{
		return BigDecimal.valueOf(value).setScale(digitScale, RoundingMode.HALF_UP).doubleValue();
	}

}
