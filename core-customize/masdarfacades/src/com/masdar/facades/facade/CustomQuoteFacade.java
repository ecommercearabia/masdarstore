/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.commercefacades.order.QuoteFacade;

/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomQuoteFacade extends QuoteFacade
{
	/**
	 * Applies the discount to the session cart given the discount and it's type.
	 *
	 * @param discountRate
	 *           the discount rate to be applied
	 * @param discountTypeCode
	 *           the code of the discount type to be applied
	 * @param entryNumber
	 *           the entry number to have discount applied to
	 */
	void applyQuoteEntryDiscount(Double discountRate, String discountTypeCode, long entryNumber);

}
