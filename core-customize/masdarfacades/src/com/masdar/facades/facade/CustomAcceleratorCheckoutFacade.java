/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.facade;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.masdarpayment.entry.PaymentRequestData;
import com.masdar.masdarpayment.entry.PaymentResponseData;
import com.masdar.masdarpayment.exception.PaymentException;
import com.masdar.masdarpayment.model.PaymentProviderModel;
import com.masdar.masdarstorecreditfacades.data.StoreCreditModeData;
import com.masdar.masdartimeslotfacades.TimeSlotData;
import com.masdar.masdartimeslotfacades.TimeSlotInfoData;


/**
 * The Interface CustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 */
public interface CustomAcceleratorCheckoutFacade extends AcceleratorCheckoutFacade
{
	public void resetPaymentInfo();

	public OrderData placeOrder(SalesApplication salesApplication) throws InvalidCartException;

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	public Optional<List<PaymentModeData>> getSupportedPaymentModes();

	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	public void setPaymentMode(final String paymentMode);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Pair<Optional<PaymentRequestData>, Boolean> getSupportedPaymentData();

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	public Optional<PaymentResponseData> getPaymentResponseData(Object data);

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	public Optional<PaymentResponseData> getOrderPaymentResponseData(Object data);

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	public Optional<PaymentProviderModel> getSupportedPaymentProvider();

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(Object data);

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the no card payment info data
	 */
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData);

	/**
	 * Set Payment Details on the cart.
	 *
	 * @param paymentInfoId
	 *           the ID of the payment info to set as the default payment
	 * @return true if operation succeeded
	 */
	boolean setGeneralPaymentDetails(String paymentInfoId);


	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	void saveBillingAddress(AddressData addressData);


	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount);



	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 * @throws PaymentException
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(Object data) throws PaymentException;

	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes();

	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected);

	public void redeemStoreCreditAmount(final AbstractOrderModel orderModel);

	public boolean isStoreCreditModeSupported(final String storeCreditTypeCode);


	public Optional<PriceData> getStoreCreditAmountFullRedeem();

	public Optional<PriceData> getAvailableBalanceStoreCreditAmount();

	public Optional<TimeSlotData> getSupportedTimeSlot();

	void setTimeSlot(TimeSlotInfoData timeSlotInfoData);

	void createCartFromOrder(final String orderCode);


	PaymentSubscriptionResultData completePaymentTransactionForStorefront(String resultIndicator, String sessionVersion)
			throws PaymentException, InvalidCartException;

	PaymentSubscriptionResultData completePaymentTransactionForOCC(Object data) throws PaymentException, InvalidCartException;

	public Optional<PaymentModeData> getDefaultPaymentModesForStore(BaseStoreModel baseStore);


	/**
	 * @return
	 */
	public boolean isSuccessfulPaidOrderByOrder();

	public boolean isAddressValid();

	/**

	 *

	 */

	public Optional<AddressData> getCartAddress();


	public List<ShipmentTypeData> getSupportedShipmentTypes();


	public ShipmentTypeData getCurrentShipmentType();

	boolean isCustomerValidForEsal();

	public boolean isAddressValidForEsal();

	public Optional<PaymentResponseData> getOrderPaymentResponseDataByData(Object data);

}
