/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.commercefacades.order.CheckoutFacade;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomB2CCheckoutFacade extends CheckoutFacade
{
	void createCartFromOrder(final String orderCode);

}
