/**
 *
 */
package com.masdar.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.order.impl.DefaultQuoteFacade;
import de.hybris.platform.commerceservices.enums.DiscountType;

import com.masdar.core.service.CustomCommerceQuoteService;
import com.masdar.facades.facade.CustomQuoteFacade;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomQuoteFacade extends DefaultQuoteFacade implements CustomQuoteFacade
{


	private CustomCommerceQuoteService customCommerceQuoteService;

	@Override
	public void applyQuoteEntryDiscount(final Double discountRate, final String discountTypeCode, final long entryNumber)
	{
		validateParameterNotNull(discountRate, "DiscountRate cannot be null");
		validateParameterNotNull(discountTypeCode, "DiscountTypeCode cannot be null");

		getCustomCommerceQuoteService().applyQuoteEntryDiscount(getCartService().getSessionCart(),
				getQuoteUserIdentificationStrategy().getCurrentQuoteUser(), discountRate, DiscountType.valueOf(discountTypeCode),
				entryNumber);

	}


	/**
	 * @return the customCommerceQuoteService
	 */
	public CustomCommerceQuoteService getCustomCommerceQuoteService()
	{
		return customCommerceQuoteService;
	}


	/**
	 * @param customCommerceQuoteService
	 *           the customCommerceQuoteService to set
	 */
	public void setCustomCommerceQuoteService(final CustomCommerceQuoteService customCommerceQuoteService)
	{
		this.customCommerceQuoteService = customCommerceQuoteService;
	}



}
