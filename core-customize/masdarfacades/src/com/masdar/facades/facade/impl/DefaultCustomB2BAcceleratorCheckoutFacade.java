package com.masdar.facades.facade.impl;

import static de.hybris.platform.util.localization.Localization.getLocalizedString;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BPermissionResultModel;
import de.hybris.platform.b2b.services.B2BCartService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BAcceleratorCheckoutFacade;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.context.SerialNumberConfigurationContext;
import com.masdar.core.enums.SerialNumberSource;


/**
 * B2B specific implementation of the {@link AcceleratorCheckoutFacade} interface extending
 * {@link DefaultB2BCheckoutFacade}. Delegates {@link AcceleratorCheckoutFacade} methods to the
 * defaultAcceleratorCheckoutFacade bean.
 */
public class DefaultCustomB2BAcceleratorCheckoutFacade extends DefaultB2BAcceleratorCheckoutFacade
		implements AcceleratorCheckoutFacade
{

	private static final String CART_CHECKOUT_COSTCENTER_INVALID = "cart.costcenter.invalid";
	private static final String CART_CHECKOUT_NOT_CALCULATED = "cart.not.calculated";
	private static final String CART_CHECKOUT_DELIVERYADDRESS_INVALID = "cart.deliveryAddress.invalid";
	private static final String CART_CHECKOUT_DELIVERYMODE_INVALID = "cart.deliveryMode.invalid";
	private static final String CART_CHECKOUT_PAYMENTINFO_EMPTY = "cart.paymentInfo.empty";
	private static final String CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED = "cart.quote.requirements.not.satisfied";

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;


	@Resource(name = "b2bUnitFacade")
	protected B2BUnitFacade b2bUnitFacade;

	@Override
	public boolean isNewAddressEnabledForCart()
	{
		if (isCashCustomer())
		{
			return true;
		}
		return !isAccountPayment();
	}

	public boolean isCashCustomer()
	{
		final B2BUnitData parentUnit = b2bUnitFacade.getParentUnit();

		return parentUnit != null && parentUnit.isCashCustomer();
	}

	@Override
	public boolean isRemoveAddressEnabledForCart()
	{
		if (isCashCustomer())
		{
			return true;
		}
		return !isAccountPayment();
	}

	@Override
	protected boolean isAccountPayment()
	{
		return getCheckoutCart().getPaymentType() != null
				&& (CheckoutPaymentType.ACCOUNT.getCode().equals(getCheckoutCart().getPaymentType().getCode())
						|| CheckoutPaymentType.BANK.getCode().equals(getCheckoutCart().getPaymentType().getCode())
						|| CheckoutPaymentType.ESAL.getCode().equals(getCheckoutCart().getPaymentType().getCode()));
	}

	@Override
	protected boolean isValidCheckoutCart(final PlaceOrderData placeOrderData)
	{
		final CartData cartData = getCheckoutCart();
		final boolean valid = true;

		if (!cartData.isCalculated())
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NOT_CALCULATED));
		}

		if (cartData.getDeliveryAddress() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYADDRESS_INVALID));
		}

		if (cartData.getDeliveryMode() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYMODE_INVALID));
		}

		final boolean accountPaymentType = CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode())
				|| CheckoutPaymentType.BANK.getCode().equals(cartData.getPaymentType().getCode())
				|| CheckoutPaymentType.ESAL.getCode().equals(cartData.getPaymentType().getCode());
		if (!accountPaymentType && cartData.getPaymentInfo() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTINFO_EMPTY));
		}

		if (Boolean.TRUE.equals(placeOrderData.getNegotiateQuote()) && !cartData.getQuoteAllowed())
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED));
		}

		return valid;
	}

	@Override
	public void createCartFromOrder(final String orderCode)
	{
		final OrderModel order = getB2BOrderService().getOrderForCode(orderCode);
		if (order == null || !order.getUser().equals(getUserService().getCurrentUser()))
		{
			throw new IllegalArgumentException("Order doesn't exist nor belong to this user.");
		}
		AddressModel originalDeliveryAddress = order.getDeliveryAddress();
		if (originalDeliveryAddress != null)
		{
			originalDeliveryAddress = originalDeliveryAddress.getOriginal();
		}

		AddressModel originalPaymentAddress = order.getPaymentAddress();
		if (originalPaymentAddress != null)
		{
			originalPaymentAddress = originalPaymentAddress.getOriginal();
		}

		final PaymentInfoModel paymentInfoModel = getPaymentInfoModelForClonedCart(order);

		// detach the order and null the attribute that is not available on the cart to avoid cloning errors.
		getModelService().detach(order);
		order.setSchedulingCronJob(null);
		order.setOriginalVersion(null);
		order.setStatus(OrderStatus.CREATED);
		order.setPaymentAddress(null);
		order.setDeliveryAddress(null);
		order.setHistoryEntries(null);
		order.setPaymentInfo(null);
		order.setB2bcomments(Collections.<B2BCommentModel> emptyList());
		order.setWorkflow(null);
		order.setPermissionResults(Collections.<B2BPermissionResultModel> emptyList());
		order.setExhaustedApprovers(Collections.<B2BCustomerModel> emptySet());
		order.setDeliveryCostAppliedOnQR(false);
		order.setDeliveryCostAppliedOnConsignment(null);
		// reset quote related fields
		resetQuoteRelatedFields(order);

		order.setScpiActionHistory(null);
		order.setScpiSentOrder(false);

		order.setPaymentTypeAttachment(null);

		// create cart from the order object.
		final CartModel cart = this.<B2BCartService> getCartService().createCartFromAbstractOrder(order);
		if (cart != null)
		{
			cart.setDeliveryAddress(originalDeliveryAddress);
			cart.setPaymentAddress(originalPaymentAddress);
			cart.setPaymentInfo(paymentInfoModel);
			getModelService().save(cart);
			getCartService().removeSessionCart();
			getCommerceCartService().calculateCart(cart);
			getModelService().refresh(cart);
			getCartService().setSessionCart(cart);
		}
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel order)
	{
		super.afterPlaceOrder(cartModel, order);
		if (cartModel != null && order != null)
		{
			final Optional<String> orderNumber = generateOrderNumber(order);
			if (orderNumber.isPresent())
			{
				order.setOrderNumber(orderNumber.get());
				order.setInvoiceDate(new Date());
				getModelService().save(order);
				getModelService().refresh(order);
			}
		}

		// Retrieve a session cart.
		getCartService().getSessionCart();
	}

	protected Optional<String> generateOrderNumber(final AbstractOrderModel abstractOrder)
	{
		final Optional<String> generatedValue = getSerialNumberConfigurationContext()
				.generateSerialNumberForBaseStore(abstractOrder.getStore(), SerialNumberSource.ABSTRACT_ORDER);
		return generatedValue;
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	public SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}
}
