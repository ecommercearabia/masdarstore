/**
 *
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.masdar.core.model.BankTransferModel;
import com.masdar.core.service.BankTransferService;
import com.masdar.facades.facade.BankTransferFacade;
import com.masdar.masdarfacades.data.BankTransferData;


/**
 * @author monzer
 *
 */
public class DefaultBankTransferFacade implements BankTransferFacade
{
	@Resource(name = "bankTransferService")
	private BankTransferService bankTransferService;

	@Resource(name = "bankTransferConverter")
	private Converter<BankTransferModel, BankTransferData> bankTransferConverter;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	@Override
	public Optional<BankTransferData> getByCode(final String code)
	{
		final Optional<BankTransferModel> bankTransfer = getBankTransferService().getByCode(code);
		if (bankTransfer.isPresent())
		{
			return Optional.ofNullable(getBankTransferConverter().convert(bankTransfer.get()));
		}else
		{
			return Optional.empty();
		}

	}

	@Override
	public List<BankTransferData> getAll()
	{
		final Optional<List<BankTransferModel>> all = getBankTransferService().getAll();

		if (all.isPresent() && !CollectionUtils.isEmpty(all.get()))
		{
			return Converters.convertAll(all.get(), getBankTransferConverter());
		}
		else
		{
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public List<BankTransferData> getSupportedBankTransfers(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel is null");

		return Converters.convertAll(baseStoreModel.getSupportedBankTransfers(), bankTransferConverter);

	}

	/**
	 * @return the bankTransferService
	 */
	protected BankTransferService getBankTransferService()
	{
		return bankTransferService;
	}

	/**
	 * @return the bankTransferConverter
	 */
	protected Converter<BankTransferModel, BankTransferData> getBankTransferConverter()
	{
		return bankTransferConverter;
	}


	@Override
	public boolean isSupportedBankTransfer(final BaseStoreModel baseStoreModel, final String code)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel is null");

		Preconditions.checkArgument(StringUtils.isNotBlank(code), "Code is blank");

		final List<BankTransferData> supportedBankTransfers = getSupportedBankTransfers(baseStoreModel);
		if (supportedBankTransfers == null || supportedBankTransfers.isEmpty())
		{
			return false;
		}
		for (final BankTransferData bank : supportedBankTransfers)
		{
			if (code.equals(bank.getCode()))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isSupportedBankTransferForCurrentStore(final String code)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return currentBaseStore != null && isSupportedBankTransfer(currentBaseStore, code);
	}
}