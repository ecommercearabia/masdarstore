/**
 *
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.service.CustomPointOfServiceService;
import com.masdar.facades.facade.PointOfServiceFacade;


/**
 * @author origin
 *
 */
public class DefaultPointOfServiceFacade implements PointOfServiceFacade
{

	@Resource(name = "pointOfServiceConverter")
	private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;


	@Resource(name = "pointOfServiceService")
	private CustomPointOfServiceService pointOfServiceService;

	@Override
	public List<PointOfServiceData> getPointOfServicesForRegion(final String countryIsoCode, final String regionIsoCode,
			final BaseStoreModel baseStore)
	{
		final List<PointOfServiceModel> pointOfServicesForRegion = this.getPointOfServiceService()
				.getPointOfServicesForRegion(countryIsoCode, regionIsoCode, baseStore);
		final List<PointOfServiceData> pointOfServices = this.getPointOfServiceConverter().convertAll(pointOfServicesForRegion);

		return pointOfServices;
	}

	/**
	 * @return the pointOfServiceConverter
	 */
	public Converter<PointOfServiceModel, PointOfServiceData> getPointOfServiceConverter()
	{
		return pointOfServiceConverter;
	}

	/**
	 * @return the pointOfServiceService
	 */
	public CustomPointOfServiceService getPointOfServiceService()
	{
		return pointOfServiceService;
	}

	@Override
	public Optional<PointOfServiceData> getPointOfServicesForName(final String name)
	{

		return Optional.ofNullable(
				this.getPointOfServiceConverter().convert(this.getPointOfServiceService().getPointOfServiceForName(name)));
	}



}
