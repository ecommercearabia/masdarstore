/**
 *
 */
package com.masdar.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.city.service.CityService;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomCommerceStockService;
import com.masdar.core.service.CustomUserService;
import com.masdar.facades.facade.CustomCartFacade;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.facades.shipment.modification.ShipmentTypeModificationData;
import com.masdar.facades.shipment.modification.ShipmentTypeModificationEntryData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomCartFacadeImpl extends DefaultCartFacade implements CustomCartFacade
{


	@Resource(name = "cityService")
	private CityService cityService;

	@Resource(name = "shipmentTypeConverter")
	private Converter<ShipmentType, ShipmentTypeData> shipmentTypeConverter;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	@Override
	public CartModificationData updateCartEntry(final OrderEntryData orderEntry) throws CommerceCartModificationException
	{
		validateParameterNotNullStandardMessage("orderEntry", orderEntry);
		validateParameterNotNullStandardMessage("entryNumber", orderEntry.getEntryNumber());

		CartModificationData cartModificationData = null;

		if (orderEntry.getEntryNumber().intValue() == -1)
		{
			final Integer entryNumberForMultiD = getOrderEntryNumberForMultiD(orderEntry);
			if (entryNumberForMultiD == null)
			{
				cartModificationData = addToCart(orderEntry.getProduct().getCode(), orderEntry.getQuantity().longValue());
			}
			else
			{
				orderEntry.setEntryNumber(entryNumberForMultiD);

				// if deleting all variants at once (i.e. deleting parent)
				boolean isDeleteParent = false;
				if (orderEntry.getQuantity().intValue() == 0)
				{
					final List<ProductOption> extraOptions = Arrays.asList(ProductOption.BASIC);
					final ProductData productData = getProductFacade().getProductForCodeAndOptions(orderEntry.getProduct().getCode(),
							extraOptions);
					isDeleteParent = productData.getBaseProduct() == null;
				}

				if (isDeleteParent)
				{
					cartModificationData = deleteGroupedOrderEntriesMultiD(orderEntry);
				}
				else
				{
					cartModificationData = updateCartEntry(orderEntry.getEntryNumber().longValue(),
							orderEntry.getQuantity().longValue());
				}
			}
		}
		else
		{
			cartModificationData = updateCartEntry(orderEntry.getEntryNumber().longValue(), orderEntry.getQuantity().longValue());
		}

		if (CollectionUtils.isEmpty(orderEntry.getEntries()))
		{
			mergeOrderEntryWithModelConfiguration(orderEntry);
			cartModificationData = configureCartEntry(orderEntry, cartModificationData);
		}

		return cartModificationData;
	}

	@Override
	protected CartModificationData configureCartEntry(final OrderEntryData orderEntry,
			final CartModificationData cartModificationData) throws CommerceCartModificationException
	{
		if (CollectionUtils.isEmpty(orderEntry.getConfigurationInfos()))
		{
			return cartModificationData;
		}

		final List<ProductConfigurationItem> productConfigurationItemList = orderEntry.getConfigurationInfos().stream()
				.map(this::configurationInfoToProductConfiguration).collect(Collectors.toList());

		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(orderEntry, productConfigurationItemList);

		getCommerceCartService().configureCartEntry(commerceCartParameter);

		// The order entry has changed, so it should be reloaded.
		final CartModel cart = getCartService().getSessionCart();
		final AbstractOrderEntryModel entry = getCartService().getEntryForNumber(cart, orderEntry.getEntryNumber());
		getOrderEntryConverter().convert(entry, orderEntry);

		if (cartModificationData != null)
		{
			cartModificationData.setEntry(orderEntry);
		}

		return cartModificationData;
	}

	@Override
	protected ProductConfigurationItem configurationInfoToProductConfiguration(final ConfigurationInfoData configurationInfoData)
	{
		final ProductConfigurationItem productConfigurationItem = new ProductConfigurationItem();
		productConfigurationItem.setKey(configurationInfoData.getConfigurationLabel());
		productConfigurationItem.setValue(configurationInfoData.getConfigurationValue());
		productConfigurationItem.setStatus(configurationInfoData.getStatus());
		productConfigurationItem.setConfiguratorType(configurationInfoData.getConfiguratorType());
		productConfigurationItem.setOptions(configurationInfoData.getOptions());

		return productConfigurationItem;
	}

	@Override
	public CartModificationData addToCart(final String code, final long quantity, final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(code);
		params.setQuantity(quantity);
		params.setShipmentType(shipmentType == null ? null : shipmentType.getCode());

		return this.addToCart(params);

	}

	@Override
	public CartModificationData updateShipmentTypeForCartEntry(final long entryNumber, final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		final AddToCartParams dto = new AddToCartParams();
		dto.setShipmentType(shipmentType != null ? shipmentType.getCode() : null);
		dto.setStoreId(getStoreId(shipmentType));
		final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(dto);
		parameter.setEnableHooks(true);
		parameter.setEntryNumber(entryNumber);
		final CommerceCartModification commerceCartModification;
		if (parameter.getPointOfService() == null)
		{
			commerceCartModification = getCommerceCartService().updateToShippingModeForCartEntry(parameter);
		}
		else
		{
			commerceCartModification = getCommerceCartService().updatePointOfServiceForCartEntry(parameter);
		}
		return getCartModificationConverter().convert(commerceCartModification);
	}

	protected List<CartModificationData> updateShipmentTypeForAllCartEntry(final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		final CartData sessionCart = getSessionCart();

		if (sessionCart == null || CollectionUtils.isEmpty(sessionCart.getEntries()))
		{
			return Collections.emptyList();
		}

		final List<CartModificationData> cartModificationDataList = new ArrayList<>();

		for (final OrderEntryData orderEntryData : sessionCart.getEntries())
		{
			cartModificationDataList.add(updateShipmentTypeForCartEntry(orderEntryData.getEntryNumber(), shipmentType));
		}

		return cartModificationDataList;
	}

	/**
	 * @param shipmentType
	 * @return
	 */
	private String getStoreId(final ShipmentType shipmentType)
	{
		if (ShipmentType.PICKUP_IN_STORE.equals(getShipmentType(shipmentType)) && hasSessionCart())
		{
			final PointOfServiceModel selectedDeliveryPointOfService = getCartService().getSessionCart()
					.getSelectedDeliveryPointOfService();

			return selectedDeliveryPointOfService == null ? getDefaultDeliveryPointOfServiceId()
					: selectedDeliveryPointOfService.getName();

		}
		else
		{
			return null;
		}

	}

	/**
	 * @return
	 */
	private String getDefaultDeliveryPointOfServiceId()
	{
		return getCartService().getSessionCart().getSite() != null
				&& getCartService().getSessionCart().getSite().getDefaultDeliveryPointOfService() != null
						? getCartService().getSessionCart().getSite().getDefaultDeliveryPointOfService().getName()
						: null;
	}

	/**
	 * @param shipmentType
	 */
	private ShipmentType getShipmentType(final ShipmentType shipmentType)
	{
		if (shipmentType != null)
		{
			return shipmentType;
		}

		if (hasSessionCart())
		{
			if (getCartService().getSessionCart().getShipmentType() != null)
			{
				return getCartService().getSessionCart().getShipmentType();
			}
			else if (getCartService().getSessionCart().getSite() != null
					&& getCartService().getSessionCart().getSite().getDefaultShipmentType() != null)
			{
				return getCartService().getSessionCart().getSite().getDefaultShipmentType();
			}
		}
		return ShipmentType.DELIVERY;
	}

	@Override
	public List<ShipmentTypeData> getSupportedShipmentTypes()
	{
		return getShipmentTypeConverter().convertAll(Arrays.asList(ShipmentType.values()));
	}

	@Override
	public ShipmentTypeData getCurrentShipmentType()
	{
		final ShipmentType currentShipmentType = getCurrentShipmentTypeModle();

		return getShipmentTypeConverter().convert(currentShipmentType);
	}

	private ShipmentType getCurrentShipmentTypeModle()
	{
		if (hasSessionCart() && getCartService().getSessionCart().getShipmentType() != null)
		{
			return getCartService().getSessionCart().getShipmentType();
		}
		else if (hasSessionCart() && getCartService().getSessionCart().getSite() != null
				&& getCartService().getSessionCart().getSite().getDefaultShipmentType() != null)
		{
			return getCartService().getSessionCart().getSite().getDefaultShipmentType();
		}
		return ShipmentType.DELIVERY;
	}

	@Override
	public List<CartModificationData> updateShipmentTypeForAllCartEntryWithData(final ShipmentType shipmentType)
			throws CommerceCartModificationException
	{
		if (shipmentType == null)
		{
			throw new IllegalArgumentException("Shipment type cannot be empty");
		}
		final CartModel cart = getSessionCartModel();
		if (cart == null || CollectionUtils.isEmpty(cart.getEntries()))
		{
			return Collections.emptyList();
		}
		String cityId = null;
		String storeId = null;
		if (ShipmentType.DELIVERY.equals(shipmentType))
		{
			cityId = getCityId(cart);
		}
		else
		{
			storeId = getStoreId(cart);
		}

		return updateShipmentTypeForAllCartEntry(shipmentType, storeId, cityId);
	}

	private String getCityId(final CartModel cart)
	{
		if (cart.getSelectedCity() != null)
		{
			return cart.getSelectedCity().getCode();
		}
		else if (cart.getUser() != null && cart.getUser().getSelectedCity() != null)
		{
			return cart.getUser().getSelectedCity().getCode();
		}
		return null;
	}

	private String getStoreId(final CartModel cart)
	{
		if (cart.getSelectedDeliveryPointOfService() != null)
		{
			return cart.getSelectedDeliveryPointOfService().getName();
		}
		else if (cart.getUser() != null && cart.getUser().getSelectedDeliveryPointOfService() != null)
		{
			return cart.getUser().getSelectedDeliveryPointOfService().getName();
		}
		else if (cart.getSite() != null && cart.getSite().getDefaultDeliveryPointOfService() != null)
		{
			return cart.getSite().getDefaultDeliveryPointOfService().getName();
		}
		throw new IllegalArgumentException("No point of service was selected, check the site pickup configs at least!");
	}

	@Override
	public List<CartModificationData> updateShipmentTypeForAllCartEntry(final ShipmentType shipmentType, final String storeId,
			final String cityId) throws CommerceCartModificationException
	{
		if (shipmentType == null)
		{
			throw new IllegalArgumentException("Shipment type cannot be empty");
		}
		final boolean isPickUp = ShipmentType.PICKUP_IN_STORE.equals(shipmentType);
		if (isPickUp && StringUtils.isEmpty(storeId))
		{
			throw new CommerceCartModificationException("Store ID cannot be empty for pickup in store action");
		}

		PointOfServiceModel selectedDeliveryPointOfService = null;
		CityModel selectedCity = null;
		if (isPickUp)
		{
			selectedDeliveryPointOfService = getPointOfServiceService().getPointOfServiceForName(storeId);
			if (selectedDeliveryPointOfService == null)
			{
				throw new CommerceCartModificationException("No store has been found for id: " + storeId);
			}
		}
		else
		{
			if (!StringUtils.isBlank(cityId))
			{
				final Optional<CityModel> city = getCityService().get(cityId);
				selectedCity = city.isPresent() ? city.get() : null;
			}
		}
		final CartModel sessionCartModel = getSessionCartModel();
		if (sessionCartModel != null)
		{
			updateShipmentTypeInfoForCart(sessionCartModel,
					new ShipmentTypeInfo(shipmentType, selectedCity, selectedDeliveryPointOfService));
		}
		else
		{
			getCustomUserService().updateShipmentTypeInfoForCurrentUser(
					new ShipmentTypeInfo(shipmentType, selectedCity, selectedDeliveryPointOfService));
			return Collections.emptyList();
		}

		return updateShipmentTypeForAllCartEntry(shipmentType);
	}

	/**
	 * @param sessionCartModel
	 */
	private void updateShipmentTypeInfoForCart(final CartModel sessionCartModel, final ShipmentTypeInfo shipmentType)
	{
		getCustomUserService().updateShipmentTypeInfo(sessionCartModel.getUser(), shipmentType);

		getModelService().refresh(sessionCartModel);
		sessionCartModel.setSelectedShipmentType(shipmentType.getShipmentType());
		if (shipmentType.getPointOfService() != null)
		{
			sessionCartModel.setSelectedDeliveryPointOfService(shipmentType.getPointOfService());
		}
		if (shipmentType.getCity() != null)
		{
			sessionCartModel.setSelectedCity(shipmentType.getCity());
		}
		getModelService().save(sessionCartModel);
	}

	private CartModel getSessionCartModel()
	{
		return hasSessionCart() ? getCartService().getSessionCart() : null;
	}

	/**
	 * @return the cityService
	 */
	protected CityService getCityService()
	{
		return cityService;
	}

	/**
	 * @return the customUserService
	 */
	public CustomUserService getCustomUserService()
	{
		return customUserService;
	}

	/**
	 * @return the shipmentTypeConverter
	 */
	protected Converter<ShipmentType, ShipmentTypeData> getShipmentTypeConverter()
	{
		return shipmentTypeConverter;
	}

	@Override
	public ShipmentTypeModificationData preChangeShipmentType(final ShipmentType shipmentType, final String cityCode,
			final String posCode)
	{
		final ShipmentTypeModificationData modifications = new ShipmentTypeModificationData();
		final CartModel cart = getSessionCartModel();
		if (cart == null || shipmentType == null)
		{
			modifications.setValidChange(false);
			return modifications;
		}
		final List<ShipmentTypeModificationEntryData> entryModifications = new ArrayList<>();

		cart.getEntries().stream().forEach(e -> {
			final Long stock = getProductStockForShipmentType(shipmentType, e, cityCode, posCode);
			final ShipmentTypeModificationEntryData entry = new ShipmentTypeModificationEntryData();
			entry.setEntryStockLevel(e.getQuantity());
			entry.setStockLevel(stock);
			entry.setProduct(getProductConverter().convert(e.getProduct()));
			entry.setValidChange(e.getQuantity() <= stock);
			entryModifications.add(entry);
		});
		modifications.setCurrentShipmentType(getShipmentTypeConverter().convert(cart.getShipmentType()));
		modifications.setModifications(entryModifications);
		modifications.setInvalidEntries(entryModifications.stream().filter(em -> !em.isValidChange()).collect(Collectors.toList()));
		modifications.setValidChange(entryModifications.stream().allMatch(ShipmentTypeModificationEntryData::isValidChange));
		final Long qty = (long) (cart.getEntries().size() - modifications.getInvalidEntries().size());
		modifications.setInvalidEntryCount(qty + "/" + cart.getEntries().size());

		return modifications;
	}

	private Long getProductStockForShipmentType(final ShipmentType shipmentType, final AbstractOrderEntryModel e,
			final String cityCode, final String posCode)
	{
		final boolean isPickup = ShipmentType.PICKUP_IN_STORE.equals(shipmentType);

		if (isPickup)
		{
			return StringUtils.isNotBlank(posCode) && getPointOfServiceService().getPointOfServiceForName(posCode) != null
					? getCustomCommerceStockService().getStockLevelForProductAndPointOfService(e.getProduct(),
							getPointOfServiceService().getPointOfServiceForName(posCode))
					: getCustomCommerceStockService().getStockLevelForProductAndPOSByCurrentCustomer(e.getProduct());
		}
		else
		{
			return StringUtils.isNotBlank(cityCode) && getCityService().get(cityCode).isPresent()
					? getCustomCommerceStockService().getStockLevelForProductAndCity(e.getProduct(),
							getCityService().get(cityCode).get())
					: getCustomCommerceStockService().getStockLevelForProductAndCityByCurrentCustomer(e.getProduct());
		}

	}

	/**
	 * @return the customCommerceStockService
	 */
	public CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	/**
	 * @return the productConverter
	 */
	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public Optional<ShipmentTypeInfo> getShipmentTypeInfoForCurrentCart()
	{
		final CartModel cartModel = getSessionCartModel();
		if (cartModel == null)
		{
			return Optional.empty();
		}
		return getShipmentTypeInfo(cartModel);
	}

	@Override
	public Optional<ShipmentTypeInfo> getShipmentTypeInfo(final CartModel cart)
	{
		if (cart == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(new ShipmentTypeInfo(cart.getSelectedShipmentType(), cart.getSelectedCity(),
				cart.getSelectedDeliveryPointOfService()));
	}

}
