/**
 *
 */
package com.masdar.facades.facade.impl;

import static de.hybris.platform.util.localization.Localization.getLocalizedString;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BPermissionResultModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCartService;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.comments.model.CommentAttachmentModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.multipart.MultipartFile;

import com.masdar.core.model.BankTransferModel;
import com.masdar.core.service.BankTransferService;
import com.masdar.core.service.CustomCommerceQuoteService;
import com.masdar.core.service.CustomerAttachmentsService;
import com.masdar.core.service.MobilePhoneService;
import com.masdar.facades.facade.BankTransferFacade;
import com.masdar.facades.facade.CustomCheckoutFacade;
import com.masdar.masdarfacades.data.BankTransferData;


/**
 * @author mnasro
 * @author monzer
 *
 */
public class DefaultCustomB2BCheckoutFacade extends DefaultB2BCheckoutFacade implements CustomCheckoutFacade
{
	private static final String CART_CHECKOUT_PAYMENTTYPE_INVALID = "cart.paymenttype.invalid";

	private static final String CART_CHECKOUT_COSTCENTER_PAYMENTTYPE_INVALID = "cart.costcenter.paymenttypeInvalid";

	private static final String CART_CHECKOUT_BANK_TRANSFER_PAYMENTTYPE_INVALID = "cart.bankTransfer.paymenttypeInvalid";

	private static final String CART_CHECKOUT_COSTCENTER_INVALID = "cart.costcenter.invalid";
	private static final String CART_CHECKOUT_NOT_CALCULATED = "cart.not.calculated";
	private static final String CART_CHECKOUT_DELIVERYADDRESS_INVALID = "cart.deliveryAddress.invalid";
	private static final String CART_CHECKOUT_DELIVERYMODE_INVALID = "cart.deliveryMode.invalid";
	private static final String CART_CHECKOUT_CART_NOT_FOUND = "cart.notfound";

	private static final String CART_CHECKOUT_ATTACHMENT_MODEL_NOT_CREATED = "cart.attachment.model.not.created";

	private static final String CART_CHECKOUT_PAYMENTINFO_EMPTY = "cart.paymentInfo.empty";
	private static final String CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED = "cart.quote.requirements.not.satisfied";


	private static final Logger LOG = Logger.getLogger(DefaultCustomB2BCheckoutFacade.class);

	@Resource(name = "bankTransferFacade")
	private BankTransferFacade bankTransferFacade;

	@Resource(name = "bankTransferService")
	private BankTransferService bankTransferService;
	@Resource(name = "customerAttachmentsService")
	private CustomerAttachmentsService customerAttachmentsService;

	@Resource(name = "defaultCustomCommerceQuoteService")
	private CustomCommerceQuoteService defaultCustomCommerceQuoteService;

	@Resource(name = "b2bCustomerService")
	private B2BCustomerService<B2BCustomerModel, B2BUnitModel> b2bCustomerService;

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Override
	public List<B2BPaymentTypeData> getSupportedPaymentTypes()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

		if (currentBaseStore == null || currentBaseStore.getSupportedPaymentTypes() == null)
		{
			return null;
		}

		if (getCart().getUser() != null)
		{

			return getSupportedPaymentTypesForCustomer(currentBaseStore, (B2BCustomerModel) getCart().getUser());
		}

		return null;
	}

	/**
	 * @param currentBaseStore
	 * @param user
	 */
	private List<B2BPaymentTypeData> getSupportedPaymentTypesForCustomer(final BaseStoreModel currentBaseStore,
			final B2BCustomerModel user)
	{
		final List<B2BPaymentTypeData> paymentTypes = Converters.convertAll(currentBaseStore.getSupportedPaymentTypes(),
				getB2bPaymentTypeDataConverter());

		final Optional<B2BUnitModel> findAny = getB2bUnitService().getAllUnitsOfOrganization(user).stream()
				.filter(e -> e.isCashCustomer()).findAny();
		if (findAny.isEmpty())
		{
			return paymentTypes;
		}

		return paymentTypes.stream().filter(e -> "ESAL".equals(e.getCode())).collect(Collectors.toList());
	}

	public List<BankTransferData> getSupportedBankTransfers()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

		if (currentBaseStore == null || currentBaseStore.getSupportedBankTransfers() == null)
		{
			return null;
		}
		return getBankTransferFacade().getSupportedBankTransfers(currentBaseStore);
	}

	@Override
	public CartData updateCheckoutCart(final CartData cartData)
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			return null;
		}
		// set payment type
		if (cartData.getPaymentType() != null)
		{
			final String newPaymentTypeCode = cartData.getPaymentType().getCode();

			// clear delivery address, delivery mode and payment details when changing payment type
			if (cartModel.getPaymentType() == null || !newPaymentTypeCode.equalsIgnoreCase(cartModel.getPaymentType().getCode()))
			{
				cartModel.setDeliveryAddress(null);
				cartModel.setDeliveryMode(null);
				cartModel.setPaymentInfo(null);
				cartModel.setSelectedBankTransfer(null);
			}

			setPaymentTypeForCart(newPaymentTypeCode, cartModel);
			// cost center need to be be cleared if using card
			if (CheckoutPaymentType.CARD.getCode().equals(newPaymentTypeCode))
			{
				setCostCenterForCart(null, cartModel);
			}
			if (CheckoutPaymentType.BANK.getCode().equals(newPaymentTypeCode))
			{
				setSelectedBankTransferForCart(null, cartModel);
			}
			if (CheckoutPaymentType.ESAL.getCode().equals(newPaymentTypeCode))
			{
				resetEsalForCart(cartModel);
			}
			if (CheckoutPaymentType.ACCOUNT.getCode().equals(newPaymentTypeCode))
			{
				setCostCenterForCart(null, cartModel);
			}
		}

		// set cost center
		if (cartData.getCostCenter() != null)
		{
			setCostCenterForCart(cartData.getCostCenter().getCode(), cartModel);
		}
		// set cost center
		if (cartData.getSelectedBankTransfer() != null)
		{
			setSelectedBankTransferForCart(cartData.getSelectedBankTransfer().getCode(), cartModel);
		}
		// set purchase order number
		if (cartData.getPurchaseOrderNumber() != null)
		{
			cartModel.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
		}

		// set delivery address
		if (cartData.getDeliveryAddress() != null)
		{
			setDeliveryAddress(cartData.getDeliveryAddress());
		}

		// set quote request description
		if (cartData.getB2BComment() != null)
		{
			final B2BCommentModel b2bComment = getModelService().create(B2BCommentModel.class);
			b2bComment.setComment(cartData.getB2BComment().getComment());
			getB2bCommentService().addComment(cartModel, b2bComment);
		}

		getModelService().save(cartModel);
		return getCheckoutCart();

	}

	/**
	 * @param code
	 * @param cartModel
	 */
	private void setSelectedBankTransferForCart(final String code, final CartModel cartModel)
	{
		final B2BPaymentTypeData paymentType = getCheckoutCart().getPaymentType();
		if (CheckoutPaymentType.BANK.getCode().equals(paymentType.getCode()) && StringUtils.isNotBlank(code))
		{
			final Optional<BankTransferModel> byCode = getBankTransferService().getByCode(code);
			if (!byCode.isPresent())
			{
				throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_BANK_TRANSFER_PAYMENTTYPE_INVALID));
			}
			cartModel.setSelectedBankTransfer(byCode.get());
		}
		else
		{
			cartModel.setSelectedBankTransfer(null);
		}
		getModelService().save(cartModel);
		getModelService().refresh(cartModel);
		// if set Selected Bank Transfer, delivery address and mode need to be cleared
		removeDeliveryAddress();
		removeDeliveryMode();
	}

	/**
	 *
	 * @param cartModel
	 */
	private void resetEsalForCart(final CartModel cartModel)
	{
		final B2BPaymentTypeData paymentType = getCheckoutCart().getPaymentType();
		if (CheckoutPaymentType.ESAL.getCode().equals(paymentType.getCode()))
		{
			cartModel.setSelectedBankTransfer(null);
		}

		getModelService().save(cartModel);
		getModelService().refresh(cartModel);
		// if set Selected Bank Transfer, delivery address and mode need to be cleared
		removeDeliveryAddress();
		removeDeliveryMode();
	}

	@Override
	protected void setPaymentTypeForCart(final String paymentType, final CartModel cartModel)
	{
		final List<CheckoutPaymentType> checkoutPaymentTypes = getEnumerationService()
				.getEnumerationValues(CheckoutPaymentType._TYPECODE);
		if (!checkoutPaymentTypes.contains(CheckoutPaymentType.valueOf(paymentType)))
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTTYPE_INVALID));
		}

		cartModel.setPaymentType(CheckoutPaymentType.valueOf(paymentType));
		if (CheckoutPaymentType.ACCOUNT.getCode().equals(paymentType) || CheckoutPaymentType.BANK.getCode().equals(paymentType)
				|| CheckoutPaymentType.ESAL.getCode().equals(paymentType))
		{
			cartModel.setPaymentInfo(getCommerceCartService().createInvoicePaymentInfo(cartModel));
		}

		getModelService().save(cartModel);
		getModelService().refresh(cartModel);

	}

	/**
	 * @return the bankTransferFacade
	 */
	protected BankTransferFacade getBankTransferFacade()
	{
		return bankTransferFacade;
	}

	/**
	 * @return the bankTransferService
	 */
	protected BankTransferService getBankTransferService()
	{
		return bankTransferService;
	}

	@Override
	public void setPaymentTypeForCurrentCart(final String paymentType)
	{
		setPaymentTypeForCart(paymentType, getCart());

	}

	public void resetPaymentTypeForCurrentCart()
	{
		final CartModel cart = getCart();
		cart.setPaymentType(null);
		cart.setSelectedBankTransfer(null);
		getModelService().save(cart);
		getModelService().refresh(cart);
		removeDeliveryAddress();
		removeDeliveryMode();
	}

	@Override
	protected void setCostCenterForCart(final String costCenterCode, final CartModel cartModel)
	{
		final B2BPaymentTypeData paymentType = getCheckoutCart().getPaymentType();
		if (paymentType.getCode().equals("CARD") || costCenterCode == null)
		{
			if (costCenterCode != null)
			{
				throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_COSTCENTER_PAYMENTTYPE_INVALID));
			}

			for (final AbstractOrderEntryModel abstractOrderEntry : cartModel.getEntries())
			{
				abstractOrderEntry.setCostCenter(null);
				getModelService().save(abstractOrderEntry);
			}
			return;
		}

		B2BCostCenterModel costCenterModel = null;
		if (costCenterCode != null)
		{
			costCenterModel = getB2bCostCenterService().getCostCenterForCode(costCenterCode);
		}

		if (costCenterModel == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_COSTCENTER_INVALID, new Object[]
			{ costCenterCode }));
		}

		for (final AbstractOrderEntryModel abstractOrderEntry : cartModel.getEntries())
		{
			if (!Objects.equals(abstractOrderEntry.getCostCenter(), costCenterModel))
			{
				abstractOrderEntry.setCostCenter(costCenterModel);
				getModelService().save(abstractOrderEntry);
			}
		}

		// if set cost center, delivery address and mode need to be cleared
		removeDeliveryAddress();
		removeDeliveryMode();
	}

	@Override
	protected boolean isValidCheckoutCart(final PlaceOrderData placeOrderData)
	{
		final CartData cartData = getCheckoutCart();
		final boolean valid = true;

		if (!cartData.isCalculated())
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NOT_CALCULATED));
		}

		if (cartData.getDeliveryAddress() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYADDRESS_INVALID));
		}

		if (cartData.getDeliveryMode() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYMODE_INVALID));
		}

		final boolean accountPaymentType = CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode())
				|| CheckoutPaymentType.BANK.getCode().equals(cartData.getPaymentType().getCode());
		if (!accountPaymentType && cartData.getPaymentInfo() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTINFO_EMPTY));
		}

		if (Boolean.TRUE.equals(placeOrderData.getNegotiateQuote()) && !cartData.getQuoteAllowed())
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_QUOTE_REQUIREMENTS_NOT_SATISFIED));
		}

		return valid;
	}

	@Override
	public void setPaymentTypeAttachmentForCurrentCart(final List<MultipartFile> attachmentsFile)
	{
		final CartModel abstractOrderModel = getCart();
		if (abstractOrderModel == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_CART_NOT_FOUND));
		}
		final UserModel customer = abstractOrderModel.getUser();
		if (attachmentsFile != null && !attachmentsFile.isEmpty() && customer != null)
		{
			final ArrayList<CommentAttachmentModel> attachments = new ArrayList<CommentAttachmentModel>();
			for (final MultipartFile file : attachmentsFile)
			{
				if (file.getOriginalFilename() != null && !file.getOriginalFilename().isEmpty())
				{
					try
					{
						final CommentAttachmentModel attachmentModel2 = (CommentAttachmentModel) getModelService()
								.create(CommentAttachmentModel.class);
						attachmentModel2.setItem(customerAttachmentsService.createAttachment(file.getOriginalFilename(),
								file.getContentType(), file.getBytes(), customer,
								abstractOrderModel.getStore().getAllowedUploadedFormatsForPaymentType()));
						attachments.add(attachmentModel2);

					}
					catch (final IOException e)
					{
						throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_ATTACHMENT_MODEL_NOT_CREATED));
					}
				}
			}

			attachments.forEach(attachmentModel -> {
				attachmentModel.setOwner(customer);
			});
			getModelService().saveAll(attachments);
			if (attachments != null && !attachments.isEmpty())
			{
				abstractOrderModel.setPaymentTypeAttachment(attachments.get(0));
				getModelService().saveAll(abstractOrderModel);
			}
		}
	}

	@Override
	public String getAllowedUploadedFormatsForPaymentTypeForCurrentCart()
	{
		final CartModel abstractOrderModel = getCart();
		if (abstractOrderModel == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_CART_NOT_FOUND));
		}

		return abstractOrderModel.getStore() != null
				&& StringUtils.isNotBlank(abstractOrderModel.getStore().getAllowedUploadedFormatsForPaymentType())
						? abstractOrderModel.getStore().getAllowedUploadedFormatsForPaymentType()
						: customerAttachmentsService.getAllowedUploadedFormats();
	}

	@Override
	protected void resetQuoteRelatedFields(final OrderModel order)
	{
		super.resetQuoteRelatedFields(order);
		getDefaultCustomCommerceQuoteService().removeQuoteEntriesDiscount(order);
	}

	/**
	 * @return the defaultCustomCommerceQuoteService
	 */
	public CustomCommerceQuoteService getDefaultCustomCommerceQuoteService()
	{
		return defaultCustomCommerceQuoteService;
	}

	@Override
	public void createCartFromOrder(final String orderCode)
	{
		final OrderModel order = getB2BOrderService().getOrderForCode(orderCode);
		if (order == null || !order.getUser().equals(getUserService().getCurrentUser()))
		{
			throw new IllegalArgumentException("Order doesn't exist nor belong to this user.");
		}
		AddressModel originalDeliveryAddress = order.getDeliveryAddress();
		if (originalDeliveryAddress != null)
		{
			originalDeliveryAddress = originalDeliveryAddress.getOriginal();
		}

		AddressModel originalPaymentAddress = order.getPaymentAddress();
		if (originalPaymentAddress != null)
		{
			originalPaymentAddress = originalPaymentAddress.getOriginal();
		}

		final PaymentInfoModel paymentInfoModel = getPaymentInfoModelForClonedCart(order);

		// detach the order and null the attribute that is not available on the cart to avoid cloning errors.
		getModelService().detach(order);
		order.setSchedulingCronJob(null);
		order.setOriginalVersion(null);
		order.setPaymentAddress(null);
		order.setDeliveryAddress(null);
		order.setHistoryEntries(null);
		order.setPaymentInfo(null);
		order.setB2bcomments(Collections.<B2BCommentModel> emptyList());
		order.setWorkflow(null);
		order.setPermissionResults(Collections.<B2BPermissionResultModel> emptyList());
		order.setExhaustedApprovers(Collections.<B2BCustomerModel> emptySet());
		order.setDeliveryCostAppliedOnQR(false);
		order.setDeliveryCostAppliedOnConsignment(null);
		// reset quote related fields
		resetQuoteRelatedFields(order);

		// Handle Cancelled Order
		if (order.getStatus().equals(OrderStatus.CANCELLED))
		{
			order.getEntries().forEach(e -> e.setQuantity(((OrderEntryModel) e).getQuantityCancelled()));
		}
		order.setStatus(OrderStatus.CREATED);
		order.setScpiActionHistory(null);
		order.setScpiSentOrder(false);

		resetEsalFields(order);

		// create cart from the order object.
		final CartModel cart = this.<B2BCartService> getCartService().createCartFromAbstractOrder(order);
		if (cart != null)
		{
			cart.setDeliveryAddress(originalDeliveryAddress);
			cart.setPaymentAddress(originalPaymentAddress);
			cart.setPaymentInfo(paymentInfoModel);
			getModelService().save(cart);
			getCartService().removeSessionCart();
			getCommerceCartService().calculateCart(cart);
			getModelService().refresh(cart);
			getCartService().setSessionCart(cart);
		}
	}

	/**
	 * @param order
	 */
	private void resetEsalFields(final OrderModel order)
	{
		order.setPaymentB2BInvoiceCreatedSuccessfully(false);
		order.setPaymentB2BInvoiceId(null);
		order.setPaymentB2BPaidAmount(0);
		order.setSadadBillId(null);
		order.setSadadBillIdEmailSent(false);
		order.setPaymentUpdateEmailSent(false);
		order.setPaymentProviderStatus(null);
		order.setEsalRefundStatus(null);
		order.setEsalCreditNoteRecords(null);
		order.setWebServiceRecords(null);
		order.setInvoiceCreationDate(null);

	}

	@Override
	public boolean isAddressValidForEsal()
	{
		final CartModel cart = getCart();
		if (Objects.isNull(cart))
		{
			return false;
		}
		if (Objects.nonNull(cart.getPaymentType()) && !CheckoutPaymentType.ESAL.equals(cart.getPaymentType()))
		{
			return true;
		}

		return Objects.nonNull(cart.getDeliveryAddress()) && Strings.isNotBlank(cart.getDeliveryAddress().getLine1());
	}

	@Override
	public boolean isCustomerValidForEsal()
	{
		final CartModel cart = getCart();
		if (Objects.isNull(cart))
		{
			return false;
		}
		if (Objects.nonNull(cart.getPaymentType()) && !CheckoutPaymentType.ESAL.equals(cart.getPaymentType()))
		{
			return true;
		}

		if (Objects.isNull(cart.getUser()) && !(cart.getUser() instanceof CustomerModel))
		{
			return false;
		}

		final CustomerModel customer = (CustomerModel) cart.getUser();
		final CountryModel mobileCountry = customer.getMobileCountry();
		if (Objects.isNull(mobileCountry) || !"SA".equals(mobileCountry.getIsocode()))
		{
			return false;
		}
		final String mobileNumber = customer.getMobileNumber();
		final Optional<String> validNumber = getMobilePhoneService()
				.validateAndNormalizePhoneNumberByIsoCode(mobileCountry.getIsocode(), mobileNumber);

		return validNumber.isPresent();
	}

	/**
	 * @return the mobilePhoneService
	 */
	protected MobilePhoneService getMobilePhoneService()
	{
		return mobilePhoneService;
	}

	@Override
	public boolean isPaymentTypeAttachmentValidForCurrentCart(final List<MultipartFile> attachmentsFile)
	{
		final CartModel abstractOrderModel = getCart();
		if (abstractOrderModel == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_CART_NOT_FOUND));
		}
		final UserModel customer = abstractOrderModel.getUser();
		if (attachmentsFile != null && !attachmentsFile.isEmpty() && customer != null)
		{
			return false;
		}
		boolean flag = false;
		for (final MultipartFile file : attachmentsFile)
		{
			if (file == null || Strings.isBlank(file.getOriginalFilename()))
			{
				continue;
			}
			if (!customerAttachmentsService.isFileExtensionValid(file.getOriginalFilename(),
					abstractOrderModel.getStore().getAllowedUploadedFormatsForPaymentType()))
			{
				return false;
			}
			flag = true;
		}

		return flag;
	}



	@Override
	public boolean hasValidCr()
	{
		final CartModel cart = getCart();
		final B2BCustomerModel currentB2BCustomer = getB2bCustomerService().getCurrentB2BCustomer();
		final B2BUnitModel b2bunit = getB2bUnitService().getParent(currentB2BCustomer);

		return cart != null && b2bunit != null && Strings.isNotBlank(b2bunit.getId()) && b2bunit.getId().trim().length() == 10
				&& StringUtils.isNumeric(b2bunit.getId());
	}

	@Override
	public boolean hasValidVAT()
	{
		final CartModel cart = getCart();
		return cart != null && cart.getUnit() != null && Strings.isNotBlank(cart.getUnit().getVatID())
				&& cart.getUnit().getVatID().trim().length() == 15;
	}

	@Override
	public boolean hasCity()
	{
		final CartModel cart = getCart();

		return cart != null && cart.getDeliveryAddress() != null && cart.getDeliveryAddress().getCity() != null;
	}

	/**
	 * @return the b2bCustomerService
	 */
	public B2BCustomerService<B2BCustomerModel, B2BUnitModel> getB2bCustomerService()
	{
		return b2bCustomerService;
	}

	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
	{
		return b2bUnitService;
	}


}
