package com.masdar.facades.facade.impl;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.order.cart.exception.CartValidationException;
import com.masdar.core.order.cart.service.CartValidationService;
import com.masdar.facades.facade.CustomAcceleratorCheckoutFacade;
import com.masdar.facades.facade.CustomCheckoutFlowFacade;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;
import com.masdar.masdartimeslotfacades.facade.TimeSlotFacade;
import com.masdar.masdartimeslotfacades.validation.TimeSlotValidationService;




/**
 * @author mnasro
 *
 *         The Class DefaultCustomCheckoutFlowFacade.
 */
public class DefaultCustomCheckoutFlowFacade implements CustomCheckoutFlowFacade
{

	/** The checkout flow facade. */
	@Resource(name = "checkoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotValidationService")
	private TimeSlotValidationService timeSlotValidationService;

	@Override
	public boolean hasValidCart()
	{
		try
		{
			return checkoutFlowFacade.hasValidCart() && cartService.hasSessionCart()
					&& cartValidationService.validateCartMaxAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartMinAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartDeliveryType(cartService.getSessionCart());
		}
		catch (final CartValidationException e)
		{
			return false;
		}
	}

	/**
	 * Gets the checkout flow facade.
	 *
	 * @return the checkoutFlowFacade
	 */
	public de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade getCheckoutFlowFacade()
	{
		return checkoutFlowFacade;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	@Override
	public boolean hasNoPaymentMode()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || cartData.getPaymentMode() == null;
	}

	@Override
	public boolean hasPaymentProvider()
	{
		if (!"card".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode()))
		{
			return false;
		}

		return getCheckoutFacade().getSupportedPaymentProvider().isPresent();
	}

	/**
	 * @return the checkoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Override
	public boolean hasNoTimeSlot() throws TimeSlotException
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite()
				&& !timeSlotValidationService.validate(cartData.getTimeSlotInfoData());
	}

	@Override
	public boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException
	{
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite();
	}

	@Override
	public boolean hasCity()
	{
		if (!cartService.hasSessionCart())
		{
			return false;
		}
		final CartModel cart = cartService.getSessionCart();

		return cart != null && cart.getDeliveryAddress() != null && cart.getDeliveryAddress().getCity() != null;
	}

	@Override
	public boolean hasCityForDeliveryPointOfService()
	{
		if (!cartService.hasSessionCart())
		{
			return false;
		}
		final CartModel cart = cartService.getSessionCart();
		final AddressModel firstDeliveryPointOfService = getFirstDeliveryPointOfService(cart);
		return cart != null && firstDeliveryPointOfService != null && firstDeliveryPointOfService.getCity() != null;
	}

	/**
	 * @param cart
	 * @return
	 */
	private AddressModel getFirstDeliveryPointOfService(final CartModel cart)
	{
		if( cart == null || CollectionUtils.isEmpty(cart.getEntries()) )
		{
			return null;
		}

		for (final AbstractOrderEntryModel order : cart.getEntries())
		{
			if (order.getDeliveryPointOfService() != null && order.getDeliveryPointOfService().getAddress() != null)
			{
				return order.getDeliveryPointOfService().getAddress();
			}
		}
		return null;
	}


}
