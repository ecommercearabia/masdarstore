/**
 * 
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordermanagementfacades.returns.impl.DefaultOmsReturnFacade;

import java.math.BigDecimal;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomOmsReturnFacade extends DefaultOmsReturnFacade
{

	@Override
	protected BigDecimal calculateRefundEntryAmount(final AbstractOrderEntryModel orderEntryModel, final Long expectedQuantity,
			final boolean completeReturn)
	{
		final BigDecimal entryAmount;
		if (completeReturn)
		{
			entryAmount = BigDecimal.valueOf(orderEntryModel.getTotalPrice());
		}
		else
		{
			entryAmount = BigDecimal.valueOf(orderEntryModel.getBasePrice() * expectedQuantity);
		}
		return entryAmount;
	}
}
