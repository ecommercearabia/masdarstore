/**
 *
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.b2bcommercefacades.company.impl.DefaultB2BUnitFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.masdar.core.service.CustomB2BUnitService;
import com.masdar.facades.facade.CustomB2BUnitFacade;


/**
 * @author tuqa-pc
 *
 */
public class DefaultCustomB2BUnitFacade extends DefaultB2BUnitFacade implements CustomB2BUnitFacade
{
	@Resource(name = "defaultCustomB2BUnitService")
	private CustomB2BUnitService<B2BUnitModel, B2BCustomerModel> customB2BUnitService;
	@Resource(name = "b2bUnitConverter")
	private Converter<B2BUnitData, B2BUnitModel> b2BUnitConverter;


	/**
	 * @return the customB2BUnitService
	 */
	public CustomB2BUnitService getCustomB2BUnitService()
	{
		return customB2BUnitService;
	}

	@Override
	public void updateB2BUnitVatId(final B2BUnitData b2bUnitData, final String vatId)
	{

		final B2BUnitModel unitForUid = (B2BUnitModel) getCustomB2BUnitService().getUnitForUid(b2bUnitData.getUid());

		getCustomB2BUnitService().updateB2BUnitVatId(unitForUid, vatId);
	}


}
