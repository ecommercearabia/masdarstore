/**
 *
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.i18n.impl.DefaultI18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.model.CityModel;
import com.masdar.facades.facade.CustomI18NFacade;


/**
 * @author mnasro
 *
 */
public class CustomI18NFacadeImpl extends DefaultI18NFacade implements CustomI18NFacade
{
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public List<RegionData> getRegionsForCountryIso(final String countryIso)
	{
		final CountryModel countryModel = getCommonI18NService().getCountry(countryIso);

		if (CollectionUtils.isEmpty(countryModel.getRegions()))
		{
			return Collections.emptyList();
		}

		final SiteChannel siteChannel = cmsSiteService.getCurrentSite() == null
				|| cmsSiteService.getCurrentSite().getChannel() == null ? null : cmsSiteService.getCurrentSite().getChannel();

		final List<RegionModel> regions = countryModel.getRegions().stream().filter(region -> checkRegions(region, siteChannel))
				.collect(Collectors.toList());
		Collections.sort(regions, RegionNameComparator.INSTANCE);
		return Converters.convertAll(regions, getRegionConverter());
	}

	public boolean checkRegions(final RegionModel region, final SiteChannel siteChannel)
	{
		if (siteChannel == null)
		{
			return true;
		}
		if (region == null)
		{
			return false;
		}
		if (SiteChannel.B2C.equals(siteChannel))
		{
			return region.isActiveOnB2C();
		}
		else
		{
			return region.isActiveOnB2B();
		}

	}

	@Override
	public List<RegionData> getRegionsForCountryIsoWithActiveCities(final String countryIso)
	{
		final CountryModel countryModel = getCommonI18NService().getCountry(countryIso);

		if (CollectionUtils.isEmpty(countryModel.getRegions()))
		{
			return Collections.emptyList();
		}

		final SiteChannel siteChannel = cmsSiteService.getCurrentSite() == null
				|| cmsSiteService.getCurrentSite().getChannel() == null ? null : cmsSiteService.getCurrentSite().getChannel();

		List<RegionModel> regions = countryModel.getRegions().stream().filter(region -> checkRegions(region, siteChannel))
				.collect(Collectors.toList());

		regions = regions.stream().filter(e -> e.getCities().stream().anyMatch(c -> checkCities(c, siteChannel)))
				.collect(Collectors.toList());

		Collections.sort(regions, RegionNameComparator.INSTANCE);
		return Converters.convertAll(regions, getRegionConverter());
	}


	public boolean checkCities(final CityModel city, final SiteChannel siteChannel)
	{
		if (siteChannel == null)
		{
			return true;
		}
		if (city == null)
		{
			return false;
		}
		if (SiteChannel.B2C.equals(siteChannel))
		{
			return city.isActiveOnB2C();
		}
		else
		{
			return city.isActiveOnB2B();
		}

	}

}
