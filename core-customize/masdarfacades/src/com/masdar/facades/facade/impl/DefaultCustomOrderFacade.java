/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.facades.facade.CustomOrderFacade;



/**
 * @author monzer
 *
 */
public class DefaultCustomOrderFacade implements CustomOrderFacade
{
	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Override
	public int getNumberOfOrders(final OrderStatus... statuses)
	{
		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		final List<OrderModel> orderList = customerAccountService.getOrderList(currentCustomer, currentBaseStore, statuses);
		return CollectionUtils.isEmpty(orderList) ? 0 : orderList.size();
	}

	@Override
	public List<OrderData> getOrdersForCurrentUser(final OrderStatus... statuses)
	{
		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		final List<OrderModel> orderList = customerAccountService.getOrderList(currentCustomer, currentBaseStore, statuses);
		return orderConverter.convertAll(orderList);
	}

}
