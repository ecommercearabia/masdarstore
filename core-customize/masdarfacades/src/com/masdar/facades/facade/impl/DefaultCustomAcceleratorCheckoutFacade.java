package com.masdar.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.dao.CreditCardPaymentSubscriptionDao;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.strategies.CreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import java.lang.Character.UnicodeBlock;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.masdar.core.context.SerialNumberConfigurationContext;
import com.masdar.core.enums.SerialNumberSource;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.order.service.CustomOrderService;
import com.masdar.core.service.CustomCustomerAccountService;
import com.masdar.core.service.MobilePhoneService;
import com.masdar.facades.facade.CustomAcceleratorCheckoutFacade;
import com.masdar.facades.facade.CustomCartFacade;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.masdarpayment.context.PaymentContext;
import com.masdar.masdarpayment.context.PaymentProviderContext;
import com.masdar.masdarpayment.entry.PaymentRequestData;
import com.masdar.masdarpayment.entry.PaymentResponseData;
import com.masdar.masdarpayment.enums.PaymentModeType;
import com.masdar.masdarpayment.exception.PaymentException;
import com.masdar.masdarpayment.hyperpay.exception.ExceptionType;
import com.masdar.masdarpayment.hyperpay.exception.HyperpayPaymentException;
import com.masdar.masdarpayment.model.PaymentProviderModel;
import com.masdar.masdarstorecredit.service.StoreCreditModeService;
import com.masdar.masdarstorecredit.service.StoreCreditService;
import com.masdar.masdarstorecreditfacades.data.StoreCreditModeData;
import com.masdar.masdarstorecreditfacades.facade.StoreCreditFacade;
import com.masdar.masdarstorecreditfacades.facade.StoreCreditModeFacade;
import com.masdar.masdartimeslot.model.TimeSlotInfoModel;
import com.masdar.masdartimeslot.service.TimeSlotService;
import com.masdar.masdartimeslotfacades.TimeSlotData;
import com.masdar.masdartimeslotfacades.TimeSlotInfoData;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;
import com.masdar.masdartimeslotfacades.facade.TimeSlotFacade;




/**
 *
 * /** The Class DefaultCustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 *
 */
public class DefaultCustomAcceleratorCheckoutFacade extends DefaultAcceleratorCheckoutFacade
		implements CustomAcceleratorCheckoutFacade
{

	/**
	 *
	 */
	private static final String CONTINUE = "CONTINUE";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomAcceleratorCheckoutFacade.class);

	/** The Constant SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";

	/** The Constant SIGNATURE_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";

	/** The Constant PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";

	/** The Constant ORDER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";

	/** The Constant CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";

	/** The Constant AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG. */
	private static final String AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG = "AuthReplyData cannot be null";

	/** The Constant DECISION_CANNOT_BE_NULL_MSG. */
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";

	/** The Constant CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG. */
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";

	public static final String REDIRECT_PREFIX = "redirect:";

	protected static final String REDIRECT_URL_ERROR = REDIRECT_PREFIX + "/checkout/multi/hop/error";

	protected static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	/** The customer account service. */
	@Resource(name = "customerAccountService")
	private CustomCustomerAccountService customerAccountService;

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The payment context. */
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private PaymentTransactionStrategy paymentTransactionStrategy;

	/** The credit card payment info create strategy. */
	@Resource(name = "creditCardPaymentInfoCreateStrategy")
	private CreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy;

	/** The credit card payment subscription dao. */
	@Resource(name = "creditCardPaymentSubscriptionDao")
	private CreditCardPaymentSubscriptionDao creditCardPaymentSubscriptionDao;

	/** The payment subscription result data converter. */
	@Resource(name = "paymentSubscriptionResultDataConverter")
	private Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> paymentSubscriptionResultDataConverter;


	/** The payment mode service. */
	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;

	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Resource(name = "storeCreditModeFacade")
	private StoreCreditModeFacade storeCreditModeFacade;

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotInfoReverseConverter")
	private Converter<TimeSlotInfoData, TimeSlotInfoModel> timeSlotInfoReverseConverter;

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@Resource(name = "cartService")
	private CartService customCartService;

	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	/**
	 * Gets the payment mode service.
	 *
	 * @return the payment mode service
	 */
	protected PaymentModeService getPaymentModeService()
	{
		return paymentModeService;
	}


	/**
	 * @return the mobilePhoneService
	 */
	protected MobilePhoneService getMobilePhoneService()
	{
		return mobilePhoneService;
	}


	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModes()
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			return Optional.empty();
		}
		if (cartModel.getDeliveryMode() == null)
		{
			LOG.error("No DeliveryMode select For current cart code : " + cartModel.getCode());
			return Optional.empty();
		}

		if (CollectionUtils.isEmpty(cartModel.getDeliveryMode().getSupportedPaymentModes()))
		{
			LOG.error("No Payment Modes Defined For delivery Mode Code : " + cartModel.getDeliveryMode().getCode());
			return Optional.empty();
		}
		if (cartModel.getTotalPrice() == 0)
		{
			final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
					.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && CONTINUE.equalsIgnoreCase(p.getCode()))
					.collect(Collectors.toList());

			return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
		}
		final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
				.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && !CONTINUE.equalsIgnoreCase(p.getCode()))
				.collect(Collectors.toList());

		return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
	}


	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the optional
	 */
	@Override
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddressData", billingAddressData);
		if (checkIfCurrentUserIsTheCartUser())
		{
			final BillingInfo billingInfo = new BillingInfo();
			billingInfo.setCity(billingAddressData.getTown());
			billingInfo.setCountry(billingAddressData.getCountry() == null ? null : billingAddressData.getCountry().getIsocode());
			billingInfo.setRegion(billingAddressData.getRegion() == null ? null : billingAddressData.getRegion().getIsocode());
			billingInfo.setFirstName(billingAddressData.getFirstName());
			billingInfo.setLastName(billingAddressData.getLastName());
			billingInfo.setEmail(billingAddressData.getEmail());
			billingInfo.setPhoneNumber(billingAddressData.getPhone());
			billingInfo.setPostalCode(billingAddressData.getPostalCode());
			billingInfo.setStreet1(billingAddressData.getLine1());
			billingInfo.setStreet2(billingAddressData.getLine2());

			final Optional<NoCardPaymentInfoModel> noCardPaymentInfo = getCustomerAccountService().createPaymentSubscription(
					getCurrentUserForCheckout(), billingInfo, billingAddressData.getTitleCode(), getPaymentProvider(),
					paymentInfoData.isSaved(),
					paymentInfoData.getNoCardTypeData() == null ? null : paymentInfoData.getNoCardTypeData().getCode());

			return noCardPaymentInfo.isPresent()
					? Optional.ofNullable(getNoCardPaymentInfoConverter().convert(noCardPaymentInfo.get()))
					: Optional.empty();
		}
		return Optional.empty();
	}

	/**
	 * Sets the general payment details.
	 *
	 * @param paymentInfoId
	 *           the payment info id
	 * @return true, if successful
	 */
	@Override
	public boolean setGeneralPaymentDetails(final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser() && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = getCurrentUserForCheckout();
			final Optional<PaymentInfoModel> paymentInfo = getCustomerAccountService().getPaymentInfoForCode(currentUserForCheckout,
					paymentInfoId);

			final CartModel cartModel = getCart();
			if (paymentInfo.isPresent())
			{
				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setPaymentInfo(paymentInfo.get());
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			LOG.warn(String.format(
					"Did not find paymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo Will not get set.",
					currentUserForCheckout, cartModel, paymentInfoId));
		}
		return false;
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}



	/**
	 * Gets the customer account service.
	 *
	 * @return the customer account service
	 */
	@Override
	protected CustomCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}


	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	@Override
	public void setPaymentMode(final String paymentMode)
	{
		if (getCart() != null)
		{
			validateParameterNotNullStandardMessage("paymentMode", paymentMode);
			final PaymentModeModel paymentModeModel = getPaymentModeService().getPaymentModeForCode(paymentMode);
			getCart().setPaymentMode(paymentModeModel);
			final String mode = paymentModeModel.getCode();
			if ("CARD".equalsIgnoreCase(mode) || CONTINUE.equalsIgnoreCase(mode))
			{
				getCart().setPaymentStatus(PaymentStatus.PAID);
			}
			else if ("esal".equalsIgnoreCase(mode))
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
				getCart().setPaymentType(CheckoutPaymentType.ESAL_B2C);
			}
			else if ("pis".equals(mode))
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
				getCart().setPaymentType(CheckoutPaymentType.PAY_IN_STORE);
			}
			else if ("cod".equals(mode))
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
				getCart().setPaymentType(CheckoutPaymentType.COD);
			}
			else if ("ccod".equals(mode))
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
				getCart().setPaymentType(CheckoutPaymentType.CCOD);
			}
			else
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
			}
			getCart().setCalculated(Boolean.FALSE);

			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}
	}

	/**
	 * Gets the supported payment data.
	 *
	 * @return the supported payment data
	 */
	@Override
	public Pair<Optional<PaymentRequestData>, Boolean> getSupportedPaymentData()
	{

		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return Pair.of(Optional.empty(), false);
		}
		Optional<PaymentRequestData> paymentData = Optional.empty();
		try
		{
			paymentData = getPaymentContext().getPaymentData(cart);
		}
		catch (final PaymentException ex)
		{
			if (ex instanceof HyperpayPaymentException
					&& ExceptionType.ORDER_ALREADY_PAID.equals(((HyperpayPaymentException) ex).getType()))
			{
				return Pair.of(paymentData, true);
			}
		}

		return Pair.of(paymentData, false);
	}

	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	@Override
	public void saveBillingAddress(final AddressData addressData)
	{
		final CartModel sessionCart = getCart();

		if (sessionCart == null)
		{
			return;
		}
		final AddressModel addressModel = getCartAddressModel(sessionCart);
		if (addressData.getId() != null && addressModel != null && addressData.getId().equals(addressModel.getPk().toString()))
		{
			addressModel.setBillingAddress(true);
			sessionCart.setPaymentAddress(addressModel);
			getModelService().save(addressModel);
		}
		else
		{
			final AddressModel convertReverse = addressReverseConverter.convert(addressData);
			convertReverse.setOwner(sessionCart.getUser());
			sessionCart.setPaymentAddress(convertReverse);
			getModelService().save(convertReverse);
			getModelService().save(sessionCart);
		}

	}

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	@Override
	public Optional<PaymentProviderModel> getSupportedPaymentProvider()
	{
		return getPaymentProviderContext().getProvider(getCart());
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return false;
		}
		return getPaymentContext().isSuccessfulPaidOrder(cart, data);
	}

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentResponseData(final Object data)
	{
		//		try
		//		{
		return getPaymentContext().getResponseData(getCart(), data);
		//		}
		//		catch (final PaymentException e)
		//		{
		//			return Optional.empty();
		//		}
	}

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	@Override
	public Optional<PaymentResponseData> getOrderPaymentResponseData(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return Optional.empty();
		}
		return getPaymentContext().getResponseData(cart, data);
	}

	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	@Override
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount)
	{

		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return Optional.empty();
		}
		final Optional<CreateSubscriptionResult> response = getPaymentContext().interpretResponse(orderInfoMap, cart);
		ServicesUtil.validateParameterNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(response.get().getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		if (response.isPresent())
		{
			paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()));
			paymentSubscriptionResult.setDecision(String.valueOf(response.get().getDecision()));
			paymentSubscriptionResult.setResultCode(String.valueOf(response.get().getReasonCode()));
		}


		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()))
		{
			final CustomerModel customerModel = (CustomerModel) cart.getUser();
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(customerModel, response.get().getRequestId(), response.get().getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.get().getCustomerInfoData(), response.get().getSubscriptionInfoData(),
					response.get().getPaymentInfoData(), saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null)
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				savePaymentTransactionEntry.setPaymentTransaction(paymentTransaction);
				addPaymentTransactionOnCart(cart, paymentTransaction);
				getModelService().save(cart);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.get().getDecision(), response.get().getReasonCode());
			LOG.error(logData);
		}


		if (paymentSubscriptionResult != null)
		{
			return Optional.ofNullable(getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResult));
		}
		else
		{
			return Optional.empty();
		}

	}

	/**
	 * @param cart
	 * @param paymentTransaction
	 */
	private void addPaymentTransactionOnCart(final CartModel cart, final PaymentTransactionModel paymentTransaction)
	{
		final List<PaymentTransactionModel> paymentTransactions = cart.getPaymentTransactions();
		final List<PaymentTransactionModel> paymentTransactionsList = new LinkedList(paymentTransactions);
		paymentTransactionsList.add(paymentTransaction);
		cart.setPaymentTransactions(paymentTransactionsList);

	}

	/**
	 * Gets the payment subscription result data converter.
	 *
	 * @return the payment subscription result data converter
	 */
	protected Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> getPaymentSubscriptionResultDataConverter()
	{
		return paymentSubscriptionResultDataConverter;
	}

	/**
	 * Gets the credit card payment subscription dao.
	 *
	 * @return the credit card payment subscription dao
	 */
	protected CreditCardPaymentSubscriptionDao getCreditCardPaymentSubscriptionDao()
	{
		return creditCardPaymentSubscriptionDao;
	}

	/**
	 * Gets the credit card payment info create strategy.
	 *
	 * @return the credit card payment info create strategy
	 */
	protected CreditCardPaymentInfoCreateStrategy getCreditCardPaymentInfoCreateStrategy()
	{
		return creditCardPaymentInfoCreateStrategy;
	}

	/**
	 * Gets the payment transaction strategy.
	 *
	 * @return the payment transaction strategy
	 */
	protected PaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	/**
	 * Gets the payment provider context.
	 *
	 * @return the payment provider context
	 */
	protected PaymentProviderContext getPaymentProviderContext()
	{
		return paymentProviderContext;
	}

	/**
	 * Gets the payment context.
	 *
	 * @return the payment context
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final Object data) throws PaymentException
	{
		return getPaymentContext().getPaymentOrderStatusResponseDataByCurrentStore(data, getCart());
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		redeemStoreCreditAmount(orderModel);
		super.afterPlaceOrder(cartModel, orderModel);
		if (cartModel != null && orderModel != null)
		{
			final Optional<String> orderNumber = generateOrderNumber(orderModel);
			if (orderNumber.isPresent())
			{
				orderModel.setOrderNumber(orderNumber.get());
				getModelService().save(orderModel);
				getModelService().refresh(orderModel);
			}
		}
	}

	@Override
	public OrderData placeOrder(final SalesApplication salesApplication) throws InvalidCartException
	{
		final CartModel cartModel = getCart();
		if (cartModel != null
				&& (cartModel.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout()))
		{
			beforePlaceOrder(cartModel);
			final OrderModel orderModel = placeOrder(cartModel);
			afterPlaceOrder(cartModel, orderModel);
			if (orderModel != null)
			{
				orderModel.setSalesApplication(salesApplication);
				getModelService().save(orderModel);
				getModelService().refresh(orderModel);
				return getOrderConverter().convert(orderModel);
			}
		}
		return null;
	}

	@Override
	public void resetPaymentInfo()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			cartModel.setPaymentInfo(null);
			getModelService().save(cartModel);
			getModelService().refresh(cartModel);
		}
	}

	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes()
	{
		return storeCreditModeFacade.getSupportedStoreCreditModesCurrentBaseStore();
	}

	@Override
	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected)
	{
		if (getCart() != null)
		{
			getCart().setStoreCreditMode(storeCreditModeService.getStoreCreditMode(StoreCreditTypeCode));
			getCart().setStoreCreditAmountSelected(storeCreditAmaountSelected);
			getCart().setCalculated(Boolean.FALSE);
			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.facades.facade.CustomAcceleratorCheckoutFacade#redeemStoreCreditAmount(de.hybris.platform.core.model.
	 * order.AbstractOrderModel)
	 */
	@Override
	public void redeemStoreCreditAmount(final AbstractOrderModel orderModel)
	{
		if (orderModel != null && orderModel.getStoreCreditAmount() != null && orderModel.getStoreCreditAmount().doubleValue() > 0)
		{

			storeCreditService.redeemStoreCreditAmount(orderModel);
		}
	}


	@Override
	public boolean isStoreCreditModeSupported(final String StoreCreditTypeCode)
	{
		return storeCreditModeFacade.isStoreCreditModeSupportedByCurrentBaseStore(StoreCreditTypeCode);
	}

	@Override
	public Optional<PriceData> getAvailableBalanceStoreCreditAmount()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
	}


	@Override
	public Optional<PriceData> getStoreCreditAmountFullRedeem()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountFullRedeem(getCart());
	}

	@Override
	public Optional<TimeSlotData> getSupportedTimeSlot()
	{
		final CartModel cartModel = getCart();
		Optional<TimeSlotData> timeSlot = Optional.empty();
		final AddressModel cartAddressModel = getCartAddressModel(cartModel);
		if (cartAddressModel == null)
		{
			LOG.warn("No address was found neither on cart or on point of service");
			return Optional.empty();
		}
		try
		{
			switch (cmsSiteService.getCurrentSite().getTimeSlotConfigType())
			{
				case BY_AREA:
					if (cartAddressModel.getArea() == null || StringUtils.isBlank(cartAddressModel.getArea().getCode()))
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotDataByArea(cartAddressModel.getArea().getCode());

					break;
				case BY_DELIVERYMODE:
					if (cartModel.getDeliveryMode() == null || cartModel.getDeliveryMode().getCode() == null)
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;

				default:
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;
			}
		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
			return Optional.empty();
		}
		return timeSlot;
	}

	@Override
	public void setTimeSlot(final TimeSlotInfoData timeSlotInfoData)
	{
		validateParameterNotNullStandardMessage("timeSlotInfoData", timeSlotInfoData);
		final TimeSlotInfoModel infoModel = timeSlotInfoReverseConverter.convert(timeSlotInfoData);
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			timeSlotService.saveTimeSlotInfo(infoModel, sessionCart, timeSlotInfoData.getDate(), timeSlotInfoData.getStart());
		}
	}

	@Override
	public void createCartFromOrder(final String orderCode)
	{

		//		final OrderModel order = customOrderService.getOrderByCodeAndOrderStatus(orderCode);
		//		if (order == null || !order.getUser().equals(getUserService().getCurrentUser()))
		//		{
		//			throw new IllegalArgumentException("Order doesn't exist nor belong to this user.");
		//		}
		//		AddressModel originalDeliveryAddress = order.getDeliveryAddress();
		//		if (originalDeliveryAddress != null)
		//		{
		//			originalDeliveryAddress = originalDeliveryAddress.getOriginal();
		//		}
		//
		//		AddressModel originalPaymentAddress = order.getPaymentAddress();
		//		if (originalPaymentAddress != null)
		//		{
		//			originalPaymentAddress = originalPaymentAddress.getOriginal();
		//		}
		//
		//		final PaymentInfoModel paymentInfoModel = order.getPaymentInfo();
		//
		//		// detach the order and null the attribute that is not available on the cart to avoid cloning errors.
		//		getModelService().detach(order);
		//
		//		order.setOriginalVersion(null);
		//		order.setStatus(OrderStatus.CREATED);
		//		order.setPaymentAddress(null);
		//		order.setDeliveryAddress(null);
		//		order.setHistoryEntries(null);
		//		order.setPaymentInfo(null);
		//		order.setOrderCode(null);
		//		order.setPaymentReferenceId(null);
		//		order.setPaymentTransactions(null);
		//		order.setRequestPaymentBody(null);
		//		order.setResponsePaymentBody(null);
		//
		//		order.setStoreCreditMode(null);
		//		order.setStoreCreditAmount(null);
		//		order.setStoreCreditAmountSelected(null);
		//
		//		// create cart from the order object.
		//		final CartModel cart = customCartService.createCartFromAbstractOrder(order);
		//		if (cart != null)
		//		{
		//			cart.setDeliveryAddress(originalDeliveryAddress);
		//			cart.setPaymentAddress(originalPaymentAddress);
		//			cart.setPaymentInfo(paymentInfoModel);
		//			getModelService().save(cart);
		//			getCartService().removeSessionCart();
		//			commerceCartService.calculateCart(cart);
		//			getModelService().refresh(cart);
		//			getCartService().setSessionCart(cart);
		//		}
	}


	public PaymentSubscriptionResultData completePaymentTransactionForStorefront(final String resultIndicator,
			final String sessionVersion) throws PaymentException, InvalidCartException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = this.getPaymentOrderStatusResponseData(null);

		final Map<String, Object> orderInfoMap = paymentOrderStatusResponseData.isEmpty() ? MapUtils.EMPTY_MAP
				: paymentOrderStatusResponseData.get().getResponseData();
		if (!this.isSuccessfulPaidOrder(null))
		{
			LOG.warn("NOT_SUCCESSFUL_PAYMENT STOREFRONT : " + orderInfoMap);
			final Map<String, Object> response = ((Map<String, Object>) orderInfoMap.get("response"));
			return buildErrorPaymentSubscription(response);
		}
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = this
				.completePaymentCreateSubscription(orderInfoMap, true);
		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final String orderCode = setCardPaymentDetails(paymentSubscriptionResultData.get());
			final OrderData orderData = this.placeOrder();
		}
		else
		{
			LOG.error("Failed to create subscription. Please check the log files for more information");
		}
		return paymentSubscriptionResultData.orElse(null);
	}

	@Override
	public PaymentSubscriptionResultData completePaymentTransactionForOCC(final Object data)
			throws PaymentException, InvalidCartException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = this.getPaymentOrderStatusResponseData(data);
		final Map<String, Object> responseMap = paymentOrderStatusResponseData.isEmpty() ? MapUtils.EMPTY_MAP
				: paymentOrderStatusResponseData.get().getResponseData();
		if (!this.isSuccessfulPaidOrder(null))
		{
			LOG.warn("NOT_SUCCESSFUL_PAYMENT OCC : " + responseMap);
			final Map<String, Object> response = ((Map<String, Object>) responseMap.get("response"));
			if (response != null && response.get("acquirerCode") != null)
			{
				throw new PaymentException("Not successfull payment due to " + (String) response.get("acquirerCode"), null);
			}
			else
			{
				throw new PaymentException("Not successfull payment", null);
			}
		}
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = this
				.completePaymentCreateSubscription(responseMap, true);
		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final String orderCode = setCardPaymentDetails(paymentSubscriptionResultData.get());
		}
		else
		{
			LOG.error("Failed to create subscription. Please check the log files for more information");
		}
		return paymentSubscriptionResultData.orElse(null);
	}

	private String setCardPaymentDetails(final PaymentSubscriptionResultData paymentSubscriptionResultData)
			throws InvalidCartException
	{
		final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

		if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
		{
			getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
		}
		this.setPaymentDetails(newPaymentSubscription.getId());

		return (getCheckoutCustomerStrategy().isAnonymousCheckout() ? getCart().getGuid() : getCart().getCode());
	}

	private PaymentSubscriptionResultData buildErrorPaymentSubscription(final Map<String, Object> response)
	{
		final PaymentSubscriptionResultData result = new PaymentSubscriptionResultData();
		result.setSuccess(false);
		if (response != null && response.get("acquirerCode") != null)
		{
			result.setResultCode((String) response.get("acquirerCode"));
			result.setDecision("1");
		}
		else
		{
			result.setResultCode("0000");
			result.setDecision("0");
		}
		return result;
	}

	/**
	 * @return the userFacade
	 */
	public UserFacade getUserFacade()
	{
		return userFacade;
	}

	/**
	 * @param userFacade
	 *           the userFacade to set
	 */
	public void setUserFacade(final UserFacade userFacade)
	{
		this.userFacade = userFacade;
	}

	protected Optional<String> generateOrderNumber(final AbstractOrderModel abstractOrder)
	{
		final Optional<String> generatedValue = getSerialNumberConfigurationContext()
				.generateSerialNumberForBaseStore(abstractOrder.getStore(), SerialNumberSource.ABSTRACT_ORDER);
		return generatedValue;
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	public SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}

	@Override
	public Optional<PaymentModeData> getDefaultPaymentModesForStore(final BaseStoreModel baseStore)
	{
		if (baseStore == null || baseStore.getDefaultPaymentMode() == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(paymentModeConverter.convert(baseStore.getDefaultPaymentMode()));
	}

	@Override
	public boolean isSuccessfulPaidOrderByOrder()
	{
		if (getCart() == null)
		{
			return false;
		}

		return paymentContext.isSuccessfulPaidOrderByOrderTransactions(getCart());
	}

	@Override
	public boolean isAddressValid()
	{
		final CartModel cart = getCart();

		final AddressModel deliveryAddress = getCartAddressModel(cart);
		if (Objects.isNull(deliveryAddress))
		{
			return false;
		}

		final PaymentModeModel paymentMode = cart.getPaymentMode();

		if (Objects.isNull(paymentMode))
		{
			return false;
		}

		if (!PaymentModeType.CARD.equals(paymentMode.getPaymentModeType()))
		{
			return true;
		}
		return true;

	}

	/**
	 * @param text
	 * @return
	 */
	private boolean containsArabicCharacter(final String text)
	{
		if (Strings.isEmpty(text))
		{
			return false;
		}

		for (final char character : text.toCharArray())
		{
			final UnicodeBlock unicode = Character.UnicodeBlock.of(character);

			if (unicode == Character.UnicodeBlock.ARABIC)
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public List<ShipmentTypeData> getSupportedShipmentTypes()
	{
		return getCustomCartFacade().getSupportedShipmentTypes();
	}

	@Override
	public ShipmentTypeData getCurrentShipmentType()
	{
		return getCustomCartFacade().getCurrentShipmentType();
	}

	/**
	 *
	 * @return the customCartFacade
	 *
	 */

	public CustomCartFacade getCustomCartFacade()

	{
		return (CustomCartFacade) getCartFacade();
	}

	@Override
	public Optional<AddressData> getCartAddress()
	{
		final CartModel cart = getCart();
		if (cart == null || cart.getShipmentType() == null)
		{
			return Optional.empty();
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			final AddressData addressForPickupInStore = getAddressForPickupInStore(cart);

			return addressForPickupInStore == null ? Optional.ofNullable(getDefaultDeliveryPointOfServiceAddress(cart))
					: Optional.ofNullable(addressForPickupInStore);

		}
		else
		{
			return Optional.ofNullable(getDeliveryAddress());
		}

	}

	/**
	 * @param cart
	 */
	private AddressData getAddressForPickupInStore(final CartModel cart)
	{

		return cart == null || cart.getDeliveryPointOfService() == null || cart.getDeliveryPointOfService().getAddress() == null
				? null
				: getAddressConverter().convert(cart.getDeliveryPointOfService().getAddress());

	}

	private AddressModel getAddressModelForPickupInStore(final CartModel cart)
	{
		return cart == null ? null : cart.getDeliveryAddress();
	}


	private AddressModel getCartAddressModel(final CartModel cart)
	{
		if (cart == null || cart.getShipmentType() == null)
		{
			return null;
		}
		if (ShipmentType.PICKUP_IN_STORE.equals(cart.getShipmentType()))
		{

			final AddressModel addressForPickupInStore = getAddressModelForPickupInStore(cart);

			return addressForPickupInStore == null ? getDefaultDeliveryPointOfServiceAddressModel(cart) : addressForPickupInStore;

		}
		else
		{
			return getDeliveryAddressModel(cart);
		}

	}

	private AddressModel getDeliveryAddressModel(final CartModel cart)
	{
		if (cart == null)
		{
			return null;
		}
		final AddressModel deliveryAddress = cart.getDeliveryAddress();
		if (deliveryAddress == null)
		{
			return null;
		}
		// Ensure that the delivery address is in the set of supported addresses
		return getDeliveryAddressModelForCode(deliveryAddress.getPk().toString());

	}

	private AddressModel getDefaultDeliveryPointOfServiceAddressModel(final CartModel cart)
	{
		return cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();

	}

	private AddressData getDefaultDeliveryPointOfServiceAddress(final CartModel cart)
	{
		final AddressModel addressModel = cart.getSite().getDefaultDeliveryPointOfService() == null
				|| cart.getSite().getDefaultDeliveryPointOfService().getAddress() == null ? null
						: cart.getSite().getDefaultDeliveryPointOfService().getAddress();
		return addressModel == null ? null : getAddressConverter().convert(addressModel);

	}

	@Override
	public boolean isCustomerValidForEsal()
	{
		final CartModel cart = getCart();
		if (Objects.isNull(cart))
		{
			return false;
		}
		if (Objects.nonNull(cart.getPaymentMode()) && !"ESAL".equalsIgnoreCase(cart.getPaymentMode().getCode()))
		{
			return true;
		}

		if (Objects.isNull(cart.getUser()) && !(cart.getUser() instanceof CustomerModel))
		{
			return false;
		}

		final CustomerModel customer = (CustomerModel) cart.getUser();
		final CountryModel mobileCountry = customer.getMobileCountry();
		if (Objects.isNull(mobileCountry) || !"SA".equals(mobileCountry.getIsocode()))
		{
			return false;
		}
		final String mobileNumber = customer.getMobileNumber();
		final Optional<String> validNumber = getMobilePhoneService()
				.validateAndNormalizePhoneNumberByIsoCode(mobileCountry.getIsocode(), mobileNumber);

		return validNumber.isPresent();

	}

	@Override
	public boolean isAddressValidForEsal()
	{
		final CartModel cart = getCart();
		if (Objects.isNull(cart))
		{
			return false;
		}
		if (Objects.nonNull(cart.getPaymentMode()) && !"ESAL".equalsIgnoreCase(cart.getPaymentMode().getCode()))
		{
			return true;
		}

		return Objects.nonNull(cart.getDeliveryAddress()) && Strings.isNotBlank(cart.getDeliveryAddress().getLine1());
	}


	@Override
	public Optional<PaymentResponseData> getOrderPaymentResponseDataByData(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return Optional.empty();
		}
		return getPaymentContext().getResponseDataByData(cart, data);
	}



}
