/**
 *
 */
package com.masdar.facades.facade.impl;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BPermissionResultModel;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Collections;

import javax.annotation.Resource;

import com.masdar.core.service.CustomB2CCartService;
import com.masdar.core.service.CustomB2COrderService;
import com.masdar.facades.facade.CustomB2CCheckoutFacade;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomCheckoutFacade extends DefaultCheckoutFacade implements CustomB2CCheckoutFacade
{

	@Resource(name = "customB2COrderService")
	private CustomB2COrderService customB2COrderService;

	@Resource(name = "customB2CCartService")
	private CustomB2CCartService customB2CCartService;


	@Override
	public void createCartFromOrder(final String orderCode)
	{
		final OrderModel order = getCustomB2COrderService().getOrderForCode(orderCode);
		if (order == null || !order.getUser().equals(getUserService().getCurrentUser()))
		{
			throw new IllegalArgumentException("Order doesn't exist nor belong to this user.");
		}
		AddressModel originalDeliveryAddress = order.getDeliveryAddress();
		if (originalDeliveryAddress != null)
		{
			originalDeliveryAddress = originalDeliveryAddress.getOriginal();
		}

		AddressModel originalPaymentAddress = order.getPaymentAddress();
		if (originalPaymentAddress != null)
		{
			originalPaymentAddress = originalPaymentAddress.getOriginal();
		}

		final PaymentInfoModel paymentInfoModel = getPaymentInfoModelForClonedCart(order);

		// detach the order and null the attribute that is not available on the cart to avoid cloning errors.
		getModelService().detach(order);
		order.setSchedulingCronJob(null);
		order.setOriginalVersion(null);
		order.setPaymentAddress(null);
		order.setDeliveryAddress(null);
		order.setHistoryEntries(null);
		order.setPaymentInfo(null);
		order.setB2bcomments(Collections.<B2BCommentModel> emptyList());
		order.setWorkflow(null);
		order.setPermissionResults(Collections.<B2BPermissionResultModel> emptyList());
		order.setExhaustedApprovers(Collections.<B2BCustomerModel> emptySet());
		order.setDeliveryCostAppliedOnQR(false);
		order.setDeliveryCostAppliedOnConsignment(null);
		// reset quote related fields
		//		resetQuoteRelatedFields(order);

		// Handle Cancelled Order
		if (order.getStatus().equals(OrderStatus.CANCELLED))
		{
			order.getEntries().forEach(e -> e.setQuantity(((OrderEntryModel) e).getQuantityCancelled()));
		}
		order.setStatus(OrderStatus.CREATED);
		order.setScpiActionHistory(null);
		order.setScpiSentOrder(false);

		order.setPaymentReferenceId(null);
		order.setRequestPaymentBody(null);
		order.setResponsePaymentBody(null);
		order.setPaymentTransactions(Collections.emptyList());
		order.setHyperpayCheckoutIds(Collections.emptyList());
		order.setHyperpayPayments(Collections.emptyList());
		order.setHyperpayCheckoutMap(Collections.emptyMap());
		order.setMerchantTransactions(Collections.emptyList());
		order.setOrderCode(null);

		resetEsalFields(order);
		// create cart from the order object.

		final CartModel cart = getCustomB2CCartService().createCartFromAbstractOrder(order);
		if (cart != null)
		{
			cart.setDeliveryAddress(originalDeliveryAddress);
			cart.setPaymentAddress(originalPaymentAddress);
			cart.setPaymentInfo(paymentInfoModel);
			getModelService().save(cart);
			getCartService().removeSessionCart();
			getCommerceCheckoutService().calculateCart(cart);
			getModelService().refresh(cart);
			getCartService().setSessionCart(cart);
		}
	}


	/**
	 * @param order
	 */
	private void resetEsalFields(final OrderModel order)
	{
		order.setPaymentB2BInvoiceCreatedSuccessfully(false);
		order.setPaymentB2BInvoiceId(null);
		order.setPaymentB2BPaidAmount(0);
		order.setSadadBillId(null);
		order.setSadadBillIdEmailSent(false);
		order.setPaymentUpdateEmailSent(false);
		order.setPaymentProviderStatus(null);
		order.setEsalRefundStatus(null);
		order.setEsalCreditNoteRecords(null);
		order.setWebServiceRecords(null);
		order.setInvoiceCreationDate(null);

	}

	protected PaymentInfoModel getPaymentInfoModelForClonedCart(final OrderModel order)
	{
		final PaymentInfoModel paymentInfoModel = order.getPaymentInfo();
		return (paymentInfoModel instanceof InvoicePaymentInfoModel) ? (PaymentInfoModel) paymentInfoModel.getOriginal() : null;
	}

	protected CustomB2COrderService getCustomB2COrderService()
	{
		return customB2COrderService;
	}


	public CustomB2CCartService getCustomB2CCartService()
	{
		return customB2CCartService;
	}




}
