/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.masdar.masdarfacades.data.BankTransferData;


/**
 * @author monzer
 *
 */
public interface BankTransferFacade
{
	public Optional<BankTransferData> getByCode(String code);

	public List<BankTransferData> getAll();

	public List<BankTransferData> getSupportedBankTransfers(BaseStoreModel baseStoreModel);

	public boolean isSupportedBankTransfer(BaseStoreModel baseStoreModel, String code);

	public boolean isSupportedBankTransferForCurrentStore(String code);
}
