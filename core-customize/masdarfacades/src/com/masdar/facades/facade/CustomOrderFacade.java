/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.facade;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.enums.OrderStatus;

import java.util.List;


/**
 * @author monzer
 *
 */
public interface CustomOrderFacade
{
	public int getNumberOfOrders(final OrderStatus... statuses);

	public List<OrderData> getOrdersForCurrentUser(final OrderStatus... statuses);
}
