/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;


/**
 * @author tuqa
 *
 */
public interface CustomB2BUnitFacade extends B2BUnitFacade
{
	public void updateB2BUnitVatId(B2BUnitData b2bUnitData, String vatId);
}
