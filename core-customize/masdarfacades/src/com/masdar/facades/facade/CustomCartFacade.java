/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;
import java.util.Optional;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.facades.shipment.modification.ShipmentTypeModificationData;


/**
 * @author mnasro
 *
 */
public interface CustomCartFacade extends CartFacade
{
	public CartModificationData addToCart(final String code, final long quantity, final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public CartModificationData updateShipmentTypeForCartEntry(final long entryNumber, final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public List<CartModificationData> updateShipmentTypeForAllCartEntryWithData(final ShipmentType shipmentType)
			throws CommerceCartModificationException;

	public List<CartModificationData> updateShipmentTypeForAllCartEntry(final ShipmentType shipmentType, String storeId,
			String cityId) throws CommerceCartModificationException;

	public List<ShipmentTypeData> getSupportedShipmentTypes();

	public ShipmentTypeData getCurrentShipmentType();

	ShipmentTypeModificationData preChangeShipmentType(final ShipmentType shipmentType, String cityCode, String posCode);

	public Optional<ShipmentTypeInfo> getShipmentTypeInfoForCurrentCart();

	public Optional<ShipmentTypeInfo> getShipmentTypeInfo(CartModel cart);

}
