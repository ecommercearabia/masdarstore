/**
 *
 */
package com.masdar.facades.facade;



import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;


/**
 * @author origin
 *
 */
public interface PointOfServiceFacade
{

	List<PointOfServiceData> getPointOfServicesForRegion(final String countryIsoCode, final String regionIsoCode,
			final BaseStoreModel baseStore);

	Optional<PointOfServiceData> getPointOfServicesForName(final String name);
}
