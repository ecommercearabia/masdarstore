/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.b2bacceleratorfacades.api.cart.CheckoutFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.masdar.masdarfacades.data.BankTransferData;


/**
 * @author monzer
 *
 */
public interface CustomCheckoutFacade extends CheckoutFacade
{
	/**
	 * Gets the list of possible Supported PaymentTypes for user selection in checkout summary
	 *
	 * @return B2BPaymentTypeEnum
	 */
	List<B2BPaymentTypeData> getSupportedPaymentTypes();

	List<BankTransferData> getSupportedBankTransfers();

	void setPaymentTypeForCurrentCart(String paymentType);

	void resetPaymentTypeForCurrentCart();

	void setPaymentTypeAttachmentForCurrentCart(final List<MultipartFile> attachmentsFile);

	boolean isPaymentTypeAttachmentValidForCurrentCart(final List<MultipartFile> attachmentsFile);

	String getAllowedUploadedFormatsForPaymentTypeForCurrentCart();

	boolean isAddressValidForEsal();

	public boolean isCustomerValidForEsal();

	boolean hasValidCr();

	boolean hasValidVAT();

	boolean hasCity();

}
