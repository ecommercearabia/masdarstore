/**
 *
 */
package com.masdar.facades.facade;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.List;


/**
 * @author core
 *
 */
public interface CustomI18NFacade extends I18NFacade
{
	public List<RegionData> getRegionsForCountryIsoWithActiveCities(final String countryIso);
}
