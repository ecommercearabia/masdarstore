/**
 *
 */
package com.masdar.facades.city.facade;

import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CityFacade.
 *
 * @author mnasro
 */
public interface CityFacade
{

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	public Optional<List<CityData>> getByRegionIsocode(final String isoCode);


	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	public Optional<List<CityData>> getAll();


	/**
	 * Gets the city
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	public Optional<CityData> get(final String code);

	public Optional<List<CityData>> getByCountryIsocode(final String isoCode);

	/**
	 * Gets the cities which have areas which have POS by regions
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	public List<CityData> getRegistrationCitiesByRegionsIsoCode(final List<String> regionsIsoCode);

	/**
	 * Gets the cities which have areas which have POS by country
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	public List<CityData> getRegistrationCitesByCountry(final CountryModel country);


	public List<CityData> getWarehouseCities(BaseStoreModel baseStoreModel);


	public List<CityData> getWarehousesCitiesByCurrentBaseStore();


}
