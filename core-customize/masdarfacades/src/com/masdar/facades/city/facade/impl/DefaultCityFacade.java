/**
 *
 */
package com.masdar.facades.city.facade.impl;

import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.core.model.c2l.C2LItemModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.masdar.core.city.service.CityService;
import com.masdar.core.model.CityModel;
import com.masdar.facades.city.facade.CityFacade;


/**
 * The Class DefaultCityFacade.
 *
 * @author @mnasro
 */
public class DefaultCityFacade implements CityFacade
{
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;



	/** The city service. */
	@Resource(name = "cityService")
	private CityService cityService;

	/** The city converter. */
	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;


	/** The city service. */
	@Resource(name = "userService")
	private UserService userService;

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityData>> getByRegionIsocode(final String isoCode)
	{
		ServicesUtil.validateParameterNotNull(isoCode, "isoCode must not be null");

		final Optional<List<CityModel>> cityList = cityService.getByRegionIsocode(isoCode);
		if (cityList.isPresent())
		{
			final List<CityData> cityListData = cityConverter.convertAll(cityList.get()).stream()
					.sorted((x, y) -> x.getName().compareTo(y.getName())).collect(Collectors.toList());
			return Optional.ofNullable(cityListData);
		}
		return Optional.empty();
	}


	/**
	 * @return the cityConverter
	 */
	protected Converter<CityModel, CityData> getCityConverter()
	{
		return cityConverter;
	}


	/**
	 * Gets the all CityData
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<CityData>> getAll()
	{
		final Optional<List<CityModel>> cityList = cityService.getAll();

		return cityList.isPresent() ? Optional.ofNullable(cityConverter.convertAll(cityList.get())) : Optional.empty();
	}


	/**
	 * Gets the CityData
	 *
	 * @param code
	 *           the code
	 * @return the city data
	 */
	@Override
	public Optional<CityData> get(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");

		final Optional<CityModel> city = cityService.get(code);

		return city.isPresent() ? Optional.ofNullable(cityConverter.convert(city.get())) : Optional.empty();
	}

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityData>> getByCountryIsocode(final String isoCode)
	{
		ServicesUtil.validateParameterNotNull(isoCode, "isoCode must not be null");

		final Optional<List<CityModel>> cityList = cityService.getByCountryIsocode(isoCode);
		if (cityList.isPresent())
		{
			final List<CityData> cityListData = cityConverter.convertAll(cityList.get()).stream()
					.sorted((x, y) -> StringUtils.isBlank(x.getName()) || StringUtils.isBlank(y.getName()) ? 0
							: x.getName().compareTo(y.getName()))
					.collect(Collectors.toList());
			return Optional.ofNullable(cityListData);
		}
		return Optional.empty();
	}

	@Override
	public List<CityData> getRegistrationCitiesByRegionsIsoCode(final List<String> regionsIsoCode)
	{
		if (regionsIsoCode == null || regionsIsoCode.isEmpty())
		{
			return Collections.emptyList();
		}

		final List<CityModel> cities = new ArrayList<>();
		for (final String isoCode : regionsIsoCode)
		{
			final Optional<List<CityModel>> optCityList = cityService.getByRegionIsocode(isoCode);
			if (optCityList.isEmpty() || optCityList.get().isEmpty())
			{
				continue;
			}


			final List<CityModel> list = optCityList.get();
			final List<CityModel> collect = list.stream()
					.filter(e -> e.getDefaultArea() != null && e.getDefaultArea().getPointOfService() != null)
					.collect(Collectors.toList());
			cities.addAll(collect);
		}

		return cityConverter.convertAll(cities);
	}


	@Override
	public List<CityData> getRegistrationCitesByCountry(final CountryModel country)
	{
		final Collection<RegionModel> regions = country.getRegistrationRegions();
		if (regions == null || regions.isEmpty())
		{
			return Collections.emptyList();
		}

		final List<String> isoCodes = regions.stream().map(C2LItemModel::getIsocode).collect(Collectors.toList());
		return getRegistrationCitiesByRegionsIsoCode(isoCodes);
	}


	/**
	 * @return the cityService
	 */
	protected CityService getCityService()
	{
		return cityService;
	}


	@Override
	public List<CityData> getWarehouseCities(final BaseStoreModel baseStoreModel)
	{

		final Collection<CityModel> cities = getCityService().getWarehouseCities(baseStoreModel);
		if (cities.isEmpty())
		{
			return Collections.emptyList();
		}
		return cityConverter.convertAll(cities).stream().sorted((x,
				y) -> StringUtils.isBlank(x.getName()) || StringUtils.isBlank(y.getName()) ? 0 : x.getName().compareTo(y.getName()))
				.collect(Collectors.toList());
	}


	@Override
	public List<CityData> getWarehousesCitiesByCurrentBaseStore()
	{
		final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		return getWarehouseCities(baseStoreModel);
	}



	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	protected UserService getUserService()
	{
		return userService;
	}
}
