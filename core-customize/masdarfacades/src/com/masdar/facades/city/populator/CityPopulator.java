package com.masdar.facades.city.populator;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.model.AreaModel;
import com.masdar.core.model.CityModel;


/**
 * The Class CityPopulator.
 *
 * @author mnasro
 */
public class CityPopulator implements Populator<CityModel, CityData>
{

	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;


	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CityModel source, final CityData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setLatitude(source.getLatitude() == null ? "0.0" : source.getLatitude().toString());
		target.setLongitude(source.getLongitude() == null ? "0.0" : source.getLongitude().toString());

		if (!CollectionUtils.isEmpty(source.getAreas()))
		{
			target.setAreas(getAreaConverter().convertAll(source.getAreas()));
		}
	}

	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}
}
