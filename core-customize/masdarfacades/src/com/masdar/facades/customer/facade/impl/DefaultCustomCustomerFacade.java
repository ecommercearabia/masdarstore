/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.customer.facade.impl;

import static de.hybris.platform.core.model.security.PrincipalModel.UID;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.UpdatedProfileEvent;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.masdar.core.enums.MaritalStatus;
import com.masdar.core.event.CustomerVerificationEvent;
import com.masdar.core.model.NationalityModel;
import com.masdar.core.nationality.service.NationalityService;
import com.masdar.core.service.MobilePhoneService;
import com.masdar.facades.customer.facade.CustomCustomerFacade;
import com.masdar.masdarfacades.customer.data.MaritalStatusData;


/**
 * @author mnasro
 *
 *         Default implementation for the {@link CustomerFacade}.
 */
public class DefaultCustomCustomerFacade extends DefaultCustomerFacade implements CustomCustomerFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerFacade.class);

	/** The customer converter. */
	@Resource(name = "customerConverter")
	private Converter<UserModel, CustomerData> customerConverter;

	/** The customer reverse converter. */
	@Resource(name = "customerReverseConverter")
	private Converter<CustomerData, CustomerModel> customerReverseConverter;

	@Resource(name = "customerService")
	private CustomerService customerService;


	@Resource(name = "modelService")
	private ModelService modelService;


	@Resource(name = "b2bProcessCodeGenerator")
	private KeyGenerator b2bProcessCodeGenerator;


	@Resource(name = "secureTokenService")
	private SecureTokenService secureTokenService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	@Resource(name = "maritalStatusDataConverter")
	private Converter<MaritalStatus, MaritalStatusData> maritalStatusDataConverter;

	/**
	 * Sets the common properties for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	@Override
	protected void setCommonPropertiesForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		customerModel.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		setTitleForRegister(registerData, customerModel);
		setUidForRegister(registerData, customerModel);
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setUid(customerModel.getUid());

		setMobileNumberForRegister(registerData, customerModel);
		customerModel.setMobileNumber(registerData.getMobileNumber());
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setUid(customerModel.getUid());
		customerModel.setNationalityID(registerData.getNationalityId());
		customerModel.setBirthOfDate(registerData.getBirthDate());
		customerModel.setCreatedFrom(registerData.getSalesApp());
		if (StringUtils.isNotBlank(registerData.getNationality()))
		{
			final Optional<NationalityModel> nationalityModel = nationalityService.get(registerData.getNationality());
			if (nationalityModel.isPresent())
			{
				customerModel.setNationality(nationalityModel.get());

			}
		}
		if (StringUtils.isNotBlank(registerData.getMaritalStatusCode()))
		{
			customerModel.setMaritalStatus(MaritalStatus.valueOf(registerData.getMaritalStatusCode()));
		}
	}

	protected void setMobileNumberForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		if (StringUtils.isNotBlank(registerData.getMobileCountry()) && StringUtils.isNotBlank(registerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerData.getMobileCountry(), registerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(registerData.getMobileNumber());
			}

			customerModel.setMobileCountry(getCommonI18NService().getCountry(registerData.getMobileCountry()));
		}
	}

	/**
	 * Update profile.
	 *
	 * @param customerData
	 *           the customer data
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());
		customerReverseConverter.convert(customerData, customer);
		getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());
	}

	@Override
	public CustomerData getCustomerByCustomerId(final String id)
	{
		if (id != null)
		{
			final CustomerModel customerModel = customerService.getCustomerByCustomerId(id);
			if (customerModel != null)
			{
				return customerConverter.convert(customerModel);
			}
		}
		return null;
	}

	@Override
	public boolean isCustomerExists(final String customerId)
	{
		if (customerId != null)
		{
			return getCustomerByCustomerId(customerId) != null;
		}
		return false;
	}

	@Override
	public void updateFullProfile(final CustomerData customerData) throws DuplicateUidException
	{
		validateDataBeforeUpdate(customerData);

		final CustomerModel customer = getCurrentSessionCustomer();
		customerReverseConverter.convert(customerData, customer);
		if (customer.getDefaultPaymentAddress() != null)
		{
			getModelService().save(customer.getDefaultPaymentAddress());
		}

		if (customer.getDefaultShipmentAddress() != null)
		{
			getModelService().save(customer.getDefaultShipmentAddress());
		}

		getModelService().save(customer);
		getEventService().publishEvent(initializeCommerceEvent(new UpdatedProfileEvent(), customer));
	}

	/**
	 * Gets the current customer.
	 *
	 * @return the current customer
	 */
	@Override
	public CustomerData getCurrentCustomer()
	{
		return customerConverter.convert(getCurrentUser());
	}

	public void verifyCustomerById(final String customerId)
	{
		Preconditions.checkArgument(Strings.isNotBlank(customerId), "Customer Id is blank");
		final CustomerModel customer = getCustomerService().getCustomerByCustomerId(customerId);
		customer.setIsEmailVerified(true);
		modelService.save(customer);
		modelService.refresh(customer);
	}

	/**
	 * @return the customerService
	 */
	public CustomerService getCustomerService()
	{
		return customerService;
	}

	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event, final CustomerModel customerModel)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}

	@Override
	public void sendEmailVerificationEmail(final String customerId)
	{
		Preconditions.checkArgument(Strings.isNotBlank(customerId), "Customer Id is blank");
		final CustomerModel customerModel = customerService.getCustomerByCustomerId(customerId);
		Preconditions.checkArgument(Strings.isNotBlank(customerId), "Customer Id is blank");
		final SecureToken token = new SecureToken(customerId, System.currentTimeMillis() + 31536000000L);
		final String encryptedToken = getSecureTokenService().encryptData(token);
		customerModel.setEmailVerificationToken(encryptedToken);
		getModelService().save(customerModel);
		final CustomerVerificationEvent customerVerificationEvent = (CustomerVerificationEvent) initializeEvent(
				new CustomerVerificationEvent(encryptedToken), customerModel);
		final CurrencyModel currency = customerModel.getSessionCurrency() == null ? getCommonI18NService().getCurrentCurrency()
				: customerModel.getSessionCurrency();
		customerVerificationEvent.setCurrency(currency);
		getEventService().publishEvent(customerVerificationEvent);
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		//		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		setCommonPropertiesForRegister(registerData, newCustomer);
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	@Override
	public void forgottenPassword(final String id)
	{
		Assert.hasText(id, "The field [id] cannot be empty");
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(id);
		if (customer == null)
		{
			throw new UnknownIdentifierException("Email: " + id + " does not exist in the database.");
		}
		final Map<String, Object> processMap = Map.of(UID, id.toLowerCase(Locale.ENGLISH));
		final ForgottenPasswordProcessModel forgottenPasswordProcessModel = getBusinessProcessService()
				.createProcess("forgottenPassword-" + id + "-" + System.currentTimeMillis(), "forgottenPasswordProcess", processMap);
		forgottenPasswordProcessModel.setSite(getBaseSiteService().getCurrentBaseSite());
		forgottenPasswordProcessModel.setLanguage(
				customer.getSessionLanguage() == null ? getCommonI18NService().getCurrentLanguage() : customer.getSessionLanguage());
		getBusinessProcessService().startProcess(forgottenPasswordProcessModel);
	}

	/**
	 * @return the b2bProcessCodeGenerator
	 */
	public KeyGenerator getB2bProcessCodeGenerator()
	{
		return b2bProcessCodeGenerator;
	}

	/**
	 * @return the secureTokenService
	 */
	public SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}

	public List<MaritalStatusData> getAllMaritailStatuses()
	{

		return maritalStatusDataConverter.convertAll(Lists.newArrayList(MaritalStatus.values()));
	}

}
