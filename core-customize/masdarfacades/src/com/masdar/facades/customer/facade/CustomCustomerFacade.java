/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.customer.facade;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.List;

import com.masdar.masdarfacades.customer.data.MaritalStatusData;


/**
 *
 */
public interface CustomCustomerFacade extends CustomerFacade
{
	void verifyCustomerById(final String customerId);

	void sendEmailVerificationEmail(final String customerId);

	CustomerData getCustomerByCustomerId(String id);

	boolean isCustomerExists(String customerId);

	public List<MaritalStatusData> getAllMaritailStatuses();
}
