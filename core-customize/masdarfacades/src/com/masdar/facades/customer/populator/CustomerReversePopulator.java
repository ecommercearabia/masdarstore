package com.masdar.facades.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.masdar.core.enums.MaritalStatus;
import com.masdar.core.model.NationalityModel;
import com.masdar.core.nationality.service.NationalityService;
import com.masdar.core.service.MobilePhoneService;


/**
 * The Class CustomerReversePopulator.
 *
 * @author mnasro
 */
public class CustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CustomerData source, final CustomerModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		populateMobileNumber(source, target);
		populateNationality(source, target);
		target.setNationalityID(source.getNationalityID());
		target.setBirthOfDate(source.getBirthDate());
		if (source.getMaritalStatus() != null && StringUtils.isNotBlank(source.getMaritalStatus().getCode()))
		{
			target.setMaritalStatus(MaritalStatus.valueOf(source.getMaritalStatus().getCode()));
		}
		target.setCreatedFrom(source.getSalesApp());
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateNationality(final CustomerData customerData, final CustomerModel customerModel)
	{
		if (customerData.getNationality() != null && StringUtils.isNotBlank(customerData.getNationality().getCode()))
		{
			final Optional<NationalityModel> nationality = nationalityService.get(customerData.getNationality().getCode());
			if (nationality.isPresent())
			{
				customerModel.setNationality(nationality.get());
			}
		}

	}

	protected void populateMobileNumber(final CustomerData customerData, final CustomerModel customerModel)
	{
		customerModel.setMobileNumber(customerModel.getMobileNumber());

		if (customerData.getMobileCountry() != null && StringUtils.isNotBlank(customerData.getMobileCountry().getIsocode())
				&& StringUtils.isNotBlank(customerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(
					customerData.getMobileCountry().getIsocode(), customerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(customerData.getMobileNumber());
			}

		}
		if (customerData.getMobileCountry() != null && !StringUtils.isEmpty(customerData.getMobileCountry().getIsocode()))
		{
			customerModel.setMobileCountry(commonI18NService.getCountry(customerData.getMobileCountry().getIsocode()));
		}
	}
}
