/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.address.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.core.model.AreaModel;
import com.masdar.core.model.CityModel;


/**
 * @author mnasro
 *
 *         Converter implementation for {@link de.hybris.platform.core.model.user.AddressModel} as source and
 *         {@link de.hybris.platform.commercefacades.user.data.AddressData} as target type.
 */
public class AddressPopulator implements Populator<AddressModel, AddressData>
{
	/** The area converter. */
	@Resource(name = "areaConverter")
	private Converter<AreaModel, AreaData> areaConverter;

	/** The city converter. */
	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCity(source.getCity() == null ? null : getCityConverter().convert(source.getCity()));
		target.setArea(source.getArea() == null ? null : getAreaConverter().convert(source.getArea()));
		target.setTown(target.getTown() == null ? null : target.getTown());
		target.setPostalCode(target.getPostalCode());
		target.setLatitude(source.getLatitude());
		target.setLongitude(source.getLongitude());
		target.setAddressName(source.getAddressName());
		target.setTitleCode(source.getTitle() == null ? null : source.getTitle().getCode());
		target.setTitle(source.getTitle() == null ? null : source.getTitle().getName());
		target.setFirstName(source.getFirstname());
		target.setLastName(source.getLastname());
		target.setInstructions(source.getInstructions());
		target.setNearestLandmark(source.getNearestLandmark());
		target.setBuildingName(source.getBuildingName());
		target.setStreetName(source.getCustomStreetName());
		target.setApartmentNumber(source.getApartmentNumber());
		target.setFloorNumber(source.getFloorNumber());
		target.setDeliveryNotes(source.getDeliveryNotes());
		target.setMobileCountry(
				source.getMobileCountry() == null ? null : getCountryConverter().convert(source.getMobileCountry()));
		target.setMobileNumber(source.getMobile());
	}

	/**
	 * @return the areaConverter
	 */
	protected Converter<AreaModel, AreaData> getAreaConverter()
	{
		return areaConverter;
	}



	/**
	 * Gets the city converter.
	 *
	 * @return the city converter
	 */
	protected Converter<CityModel, CityData> getCityConverter()
	{
		return cityConverter;
	}

	/**
	 * Gets the country converter.
	 *
	 * @return the country converter
	 */
	protected Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}

}
