/**
 *
 */
package com.masdar.facades.address.facades;

import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.user.data.AddressData;


/**
 * @author monzer
 *
 */
public interface CustomB2CAddressVerificationFacade extends AddressVerificationFacade
{

	AddressVerificationResult verifyB2CAddressData(AddressData addressData);

}
