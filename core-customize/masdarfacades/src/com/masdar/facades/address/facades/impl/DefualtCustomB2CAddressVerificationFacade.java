/**
 *
 */
package com.masdar.facades.address.facades.impl;

import de.hybris.platform.commercefacades.address.data.AddressVerificationErrorField;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.address.impl.DefaultAddressVerificationFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.address.AddressErrorCode;
import de.hybris.platform.commerceservices.address.AddressFieldType;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.address.data.AddressFieldErrorData;
import de.hybris.platform.commerceservices.address.data.AddressVerificationResultData;
import de.hybris.platform.commerceservices.address.util.AddressVerificationResultUtils;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.masdar.core.address.service.CustomAddressVerificationService;
import com.masdar.facades.address.facades.CustomB2CAddressVerificationFacade;


/**
 * @author monzer
 *
 */
public class DefualtCustomB2CAddressVerificationFacade extends DefaultAddressVerificationFacade
		implements CustomB2CAddressVerificationFacade
{

	private CustomAddressVerificationService<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> customB2CAddressVerificationService;

	@Override
	public AddressVerificationResult verifyB2CAddressData(final AddressData addressData)
	{
		final AddressModel addressModel = getModelService().create(AddressModel.class);
		getAddressReverseConverter().convert(addressData, addressModel);

		// Pass the addressModel to the verification service and process the resulting address errors and suggestions
		final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> result = customB2CAddressVerificationService
				.verifyB2CAddress(addressModel);

		final List<AddressData> suggestedAddresses = new ArrayList<AddressData>();
		if (AddressVerificationResultUtils.hasSuggestedAddresses(result))
		{
			for (final AddressModel suggestedAddressModel : result.getSuggestedAddresses())
			{
				suggestedAddresses.add(getAddressConverter().convert(suggestedAddressModel));
			}
		}

		final Map<String, AddressVerificationErrorField> verificationResultErrorMap = new HashMap<String, AddressVerificationErrorField>();
		if (AddressVerificationResultUtils.requiresErrorHandling(result))
		{
			verificationResultErrorMap.putAll(generateVerificationResultErrorMap(result));
		}

		final AddressVerificationResult<AddressVerificationDecision> avsResult = new AddressVerificationResult<AddressVerificationDecision>();
		avsResult.setDecision(result.getDecision());
		avsResult.setSuggestedAddresses(suggestedAddresses);
		avsResult.setErrors(verificationResultErrorMap);

		return avsResult;
	}

	public CustomAddressVerificationService getCustomB2CAddressVerificationService()
	{
		return customB2CAddressVerificationService;
	}

	public void setCustomB2CAddressVerificationService(final CustomAddressVerificationService customB2CAddressVerificationService)
	{
		this.customB2CAddressVerificationService = customB2CAddressVerificationService;
	}

}
