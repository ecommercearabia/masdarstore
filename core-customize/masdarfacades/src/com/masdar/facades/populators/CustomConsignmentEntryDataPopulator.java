/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.facades.utils.PriceCalculationUtil;


/**
 * @author monzer
 *
 */
public class CustomConsignmentEntryDataPopulator implements Populator<ConsignmentEntryModel, ConsignmentEntryData>
{

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target) throws ConversionException
	{
		if (source != null && source.getOrderEntry() != null && target != null)
		{
			final AbstractOrderEntryModel orderEntry = source.getOrderEntry();
			double taxValue = 0.0;
			double taxAppliedValue = 0.0;
			if (!CollectionUtils.isEmpty(orderEntry.getTaxValues()))
			{
				for (final TaxValue value : orderEntry.getTaxValues())
				{
					taxValue += value.getValue();
					taxAppliedValue += value.getAppliedValue();
				}
			}

			double netDiscount = 0.0;
			if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
			{
				for (final DiscountValue value : orderEntry.getDiscountValues())
				{
					netDiscount += value.getAppliedValue();
				}
			}
			if (orderEntry.getQuantity() != null && orderEntry.getQuantity() > 0)
			{

				netDiscount = netDiscount / orderEntry.getQuantity();
				final PriceData netDiscountRounded = createPrice(orderEntry, netDiscount);
				final double netDiscountRoundedDouble = netDiscountRounded.getValue().setScale(2, RoundingMode.HALF_UP).doubleValue();
				final double netDiscountVal = netDiscountRoundedDouble * source.getQuantity();
				final double netTotalPriceVal = (orderEntry.getBasePrice() - netDiscountRoundedDouble) * source.getQuantity();
				final double totalTaxPriceVal = (taxAppliedValue / orderEntry.getQuantity()) * source.getQuantity();
				final double totalPriceVal = netTotalPriceVal + totalTaxPriceVal;

				final double netBasePriceWithTaxVal = orderEntry.getBasePrice() + (taxAppliedValue / orderEntry.getQuantity());

				final PriceData netBasePrice = createPrice(orderEntry, orderEntry.getBasePrice());
				final PriceData netBaseDiscount = createPrice(orderEntry, netDiscount);
				final PriceData netBasePriceWithTax = createPrice(orderEntry, netBasePriceWithTaxVal);
				final PriceData netTotalPrice = createPrice(orderEntry, netTotalPriceVal);
				final PriceData totalTaxPrice = createPrice(orderEntry, totalTaxPriceVal);
				final PriceData totalPrice = createPrice(orderEntry, totalPriceVal);
				final PriceData netDiscountPrice = createPrice(orderEntry, netDiscountVal);

				target.setNetBasePrice(netBasePrice);
				target.setNetBaseDiscount(netBaseDiscount);
				target.setNetTotalPrice(netTotalPrice);
				target.setTaxValue(taxValue);
				target.setTotalTaxPrice(totalTaxPrice);
				target.setTotalPrice(totalPrice);
				target.setNetDiscount(netDiscountPrice);
				target.setNetBasePriceWithTax(netBasePriceWithTax);



				populateValues(source, target);
			}

		}
	}

	/**
	 * @param target
	 * @param source
	 *
	 */
	private void populateValues(final ConsignmentEntryModel source, final ConsignmentEntryData target)
	{
		if (source == null || source.getOrderEntry() == null || target == null)
		{
			return;
		}

		final AbstractOrderEntryModel orderEntry = source.getOrderEntry();

		final Long invoiceQuantity = source.getQuantity() == null ? 0 : source.getQuantity();
		final Long baseQuantity = orderEntry.getQuantity() == null ? 0 : orderEntry.getQuantity();

		final double basePrice = orderEntry.getBasePrice() == null ? 0 : orderEntry.getBasePrice();
		final double totalBasePrice = PriceCalculationUtil.getTotalBasePrice(invoiceQuantity.longValue(), basePrice);
		final double totalDiscount = getTotalDiscountForConsimentEntry(orderEntry, invoiceQuantity);
		final double discount = PriceCalculationUtil.calculateDiscountPerEntry(totalDiscount, baseQuantity, invoiceQuantity);
		final double priceAfterDiscount = totalBasePrice - totalDiscount;
		final double[] percantage = getTaxPercentages(source);
		final double totalTax = PriceCalculationUtil.calculateTax(priceAfterDiscount, percantage);
		final double priceWithTax = totalTax + priceAfterDiscount;

		target.setNetBasePriceValue(PriceCalculationUtil.getRoundedUp(2, basePrice));
		target.setNetDiscountValue(PriceCalculationUtil.getRoundedUp(2, totalDiscount));
		target.setNetTotalPriceValue(PriceCalculationUtil.getRoundedUp(2, priceAfterDiscount));
		target.setTotalTaxPriceValue(PriceCalculationUtil.getRoundedUp(2, totalTax));
		target.setTotalPriceValue(PriceCalculationUtil.getRoundedUp(2, priceWithTax));

		double priceWithoutTaxAfterDiscount = target.getNetBasePriceValue();
		if (invoiceQuantity > 0)
		{
			priceWithoutTaxAfterDiscount = target.getNetBasePriceValue()
					- (target.getNetDiscountValue() / invoiceQuantity.doubleValue());
		}

		target.setPriceWithoutTaxAfterDiscountValue(priceWithoutTaxAfterDiscount);

		final PriceData priceWithoutTaxAfterDiscountData = createPrice(orderEntry, priceWithoutTaxAfterDiscount);
		target.setPriceWithoutTaxAfterDiscount(priceWithoutTaxAfterDiscountData);



	}

	/**
	 * @param source
	 * @return
	 */
	private double[] getTaxPercentages(final ConsignmentEntryModel source)
	{
		double[] taxes = null;
		final double taxValue = 0.0;
		final double taxAppliedValue = 0.0;
		final AbstractOrderEntryModel orderEntry = source.getOrderEntry();
		if (!CollectionUtils.isEmpty(orderEntry.getTaxValues()))
		{
			taxes = new double[orderEntry.getTaxValues().size()];

			int i = 0;
			for (final TaxValue tax : orderEntry.getTaxValues())
			{
				taxes[i] = tax.getValue();
				++i;
			}

		}
		return taxes;
	}

	/**
	 * @param orderEntry
	 * @param invoiceQuantity
	 * @return
	 */
	private double getTotalDiscountForConsimentEntry(final AbstractOrderEntryModel orderEntry, final long invoiceQuantity)
	{
		final long orderEntryQuantity = orderEntry.getQuantity() == null ? 0 : orderEntry.getQuantity();

		double netDiscount = 0.0;
		if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
		{
			for (final DiscountValue value : orderEntry.getDiscountValues())
			{
				netDiscount += value.getAppliedValue();
			}
		}
		return orderEntryQuantity == 0 ? 0 : (netDiscount / orderEntryQuantity) * invoiceQuantity;
	}

	protected PriceData createPrice(final AbstractOrderEntryModel orderEntry, final Double val)
	{
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
				orderEntry.getOrder().getCurrency());
	}

	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

}
