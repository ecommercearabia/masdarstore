/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.masdar.core.enums.MaritalStatus;
import com.masdar.facades.product.data.GenderData;
import com.masdar.masdarfacades.customer.data.MaritalStatusData;



/**
 * Populates {@link GenderData} with name and code.
 */
public class MaritalStatusDataPopulator implements Populator<MaritalStatus, MaritalStatusData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	@Override
	public void populate(final MaritalStatus source, final MaritalStatusData target)
	{
		target.setCode(source.getCode());
		target.setName(source.name());
	}
}
