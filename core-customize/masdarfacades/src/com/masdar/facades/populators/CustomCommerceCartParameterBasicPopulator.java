/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.converters.populator.CommerceCartParameterBasicPopulator;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.service.CustomUserService;


/**
 * @author monzer
 *
 */
public class CustomCommerceCartParameterBasicPopulator extends CommerceCartParameterBasicPopulator
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomCommerceCartParameterBasicPopulator.class);

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void populate(final AddToCartParams source, final CommerceCartParameter target) throws ConversionException
	{

		super.populate(source, target);
		final CartModel cart = target.getCart();

		if (cart.getSite() == null || !cart.getSite().isCustomerLocationEnabled())
		{
			return;
		}
		final PointOfServiceModel selectedDeliveryPointOfService = cart.getSelectedDeliveryPointOfService();
		if (selectedDeliveryPointOfService == null)
		{
			cart.setSelectedDeliveryPointOfService(cart.getUser().getSelectedDeliveryPointOfService());
			modelService.save(cart);
			modelService.refresh(cart);
			target.setCart(cart);
		}
		populateShipmentType(source, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateShipmentType(final AddToCartParams source, final CommerceCartParameter target)
	{
		if (source == null || target == null)
		{
			return;
		}

		if (target.getCart() == null || target.getCart().getSite() == null || !target.getCart().getSite().isPickUpEnabled()
				|| ShipmentType.DELIVERY.equals(getShipmentType(source.getShipmentType())))
		{
			target.setShipmentType(ShipmentType.DELIVERY);
			target.setPointOfService(null);
			return;
		}

		final ShipmentTypeInfo cartShipmentTypeInfo = getShipmentTypeInfoForCart(target.getCart());
		final ShipmentTypeInfo userShipmentTypeInfo = getShipmentTypeInfoForUser(target.getCart().getUser());
		final ShipmentTypeInfo siteShipmentTypeInfo = getShipmentTypeInfoForSite(target.getCart().getSite());

		final ShipmentTypeInfo finalInfo = getSelectedShipmentType(cartShipmentTypeInfo, userShipmentTypeInfo,
				siteShipmentTypeInfo);

		target.setShipmentType(finalInfo.getShipmentType());
		target.setPointOfService(finalInfo.getPointOfService());
	}

	/**
	 * @param site
	 * @return
	 */
	private ShipmentTypeInfo getShipmentTypeInfoForSite(final BaseSiteModel site)
	{
		return new ShipmentTypeInfo(site.getDefaultShipmentType(), null, site.getDefaultDeliveryPointOfService());
	}

	/**
	 * @param user
	 * @return
	 */
	private ShipmentTypeInfo getShipmentTypeInfoForUser(final UserModel user)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfo = customUserService.getShipmentTypeInfo(user);
		return shipmentTypeInfo.isEmpty() ? new ShipmentTypeInfo(null, null, null) : shipmentTypeInfo.get();
	}

	/**
	 * @param cart
	 * @return
	 */
	private ShipmentTypeInfo getShipmentTypeInfoForCart(final CartModel cart)
	{
		return new ShipmentTypeInfo(cart.getSelectedShipmentType(), null, cart.getSelectedDeliveryPointOfService());
	}

	private PointOfServiceModel getDeliveryPointOfServiceFromFirstCartEntry(final CartModel cart)
	{
		return CollectionUtils.isEmpty(cart.getEntries()) ? null : cart.getEntries().get(0).getDeliveryPointOfService();
	}

	/**
	 * @param cart
	 * @param shipmentTypeInfo
	 */
	private ShipmentTypeInfo getSelectedShipmentType(final ShipmentTypeInfo cartShipmentTypeInfo,
			final ShipmentTypeInfo userShipmentTypeInfo, final ShipmentTypeInfo siteShipmentTypeInfo)
	{

		if (cartShipmentTypeInfo.getShipmentType() != null && checkPickupValidate(cartShipmentTypeInfo))
		{
			LOG.info("Cart shipment type info selected");
			return ShipmentType.PICKUP_IN_STORE.equals(cartShipmentTypeInfo.getShipmentType()) ? cartShipmentTypeInfo
					: new ShipmentTypeInfo(cartShipmentTypeInfo.getShipmentType(), null, null);
		}
		else if (userShipmentTypeInfo.getShipmentType() != null && checkPickupValidate(userShipmentTypeInfo))
		{
			LOG.info("User shipment type info selected");
			return ShipmentType.PICKUP_IN_STORE.equals(userShipmentTypeInfo.getShipmentType()) ? userShipmentTypeInfo
					: new ShipmentTypeInfo(userShipmentTypeInfo.getShipmentType(), null, null);
		}
		else if (siteShipmentTypeInfo.getShipmentType() != null && checkPickupValidate(siteShipmentTypeInfo))
		{
			LOG.info("Site shipment type info selected");
			return ShipmentType.PICKUP_IN_STORE.equals(siteShipmentTypeInfo.getShipmentType()) ? siteShipmentTypeInfo
					: new ShipmentTypeInfo(userShipmentTypeInfo.getShipmentType(), null, null);
		}
		else
		{
			LOG.info("Default shipment type info selected");
			return new ShipmentTypeInfo(ShipmentType.DELIVERY, null, null);
		}

	}

	/**
	 * @param cartShipmentTypeInfo
	 * @return
	 */
	private boolean checkPickupValidate(final ShipmentTypeInfo shipmentTypeInfo)
	{
		if (!ShipmentType.PICKUP_IN_STORE.equals(shipmentTypeInfo.getShipmentType()))
		{
			return true;
		}

		return shipmentTypeInfo.getPointOfService() != null;
	}

	/**
	 * @param shipmentType
	 * @return
	 */
	private ShipmentType getShipmentType(final String shipmentType)
	{
		try
		{
			return ShipmentType.valueOf(shipmentType);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

}
