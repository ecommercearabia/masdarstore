package com.masdar.facades.populators;

import de.hybris.platform.comments.model.CommentAttachmentModel;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;

import com.google.common.math.DoubleMath;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.model.BankTransferModel;
import com.masdar.core.model.CityModel;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.masdarfacades.data.AttachmentData;
import com.masdar.masdarfacades.data.BankTransferData;


/**
 *
 * The Class PaymentAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class CustomAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	private static final double EPSILON = 0.01d;

	@Resource(name = "bankTransferConverter")
	private Converter<BankTransferModel, BankTransferData> bankTransferConverter;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;

	@Resource(name = "promotionResultConverter")
	private Converter<PromotionResultModel, PromotionResultData> promotionResultConverter;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "pointOfServiceConverter")
	private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

	@Resource(name = "shipmentTypeConverter")
	private Converter<ShipmentType, ShipmentTypeData> shipmentTypeConverter;

	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			populateSelectedBankTransfer(source, target);
			populatePaymentTypeAttachment(source, target);

			addPromotions(source, target);
			target.setSadadBillId(source.getSadadBillId());
			target.setPaymentB2BPaidAmount(source.getPaymentB2BPaidAmount());
			target.setInvoiceCreationDate(source.getInvoiceCreationDate());
			target.setInvoiceDueDate(getDueDate(source));
			target.setPaymentB2BInvoiceId(source.getPaymentB2BInvoiceId());
			target.setScpiSentOrder(source.isScpiSentOrder());
			target.setPaymentProviderStatus(
					source.getPaymentProviderStatus() == null ? "" : source.getPaymentProviderStatus().getCode());
			target.setPaymentProviderTransactionType(
					source.getPaymentProviderTransactionType() == null ? "" : source.getPaymentProviderTransactionType().getCode());
			target.setPaymentB2BInvoiceCreatedSuccessfully(source.isPaymentB2BInvoiceCreatedSuccessfully());
			target.setSadadBillIdEmailSent(source.isSadadBillIdEmailSent());
			target.setPaymentUpdateEmailSent(source.isPaymentUpdateEmailSent());
			target.setEsalRefundStatus(source.getEsalRefundStatus() == null ? "" : source.getEsalRefundStatus().getCode());
			target.setEsalRefundValue(String.valueOf(source.getEsalRefundValue()));
			fillConsignmentCodes(source, target);
			fillConsignmentInvoiceNumbers(source, target);
			fillAllWarehouseCodes(source, target);
			target.setSiteChannel(source.getSite().getChannel());
			populateDeliveryPointOfService(source, target);
			populateSelectedCity(source, target);
			if (source.getShipmentType() != null)
			{
				target.setShipmentType(getShipmentTypeConverter().convert(source.getShipmentType()));
			}

			if (!CollectionUtils.isEmpty(source.getAppliedCouponCodes()))
			{
				target.setAppliedCouponCodes(new ArrayList(source.getAppliedCouponCodes()));

			}
		}

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateSelectedCity(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source.getSelectedCity() == null)
		{
			return;
		}
		target.setSelectedCity(getCityConverter().convert(source.getSelectedCity()));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populatePaymentTypeAttachment(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source.getPaymentTypeAttachment() != null)
		{
			final CommentAttachmentModel paymentTypeAttachment = source.getPaymentTypeAttachment();

			final MediaModel attachment = (MediaModel) paymentTypeAttachment.getItem();
			final AttachmentData attachmentData = new AttachmentData();
			attachmentData.setFilename(attachment.getRealFileName());
			attachmentData.setURL(attachment.getURL());
			target.setPaymentTypeAttachment(attachmentData);
			target.setScpiSentOrder(source.isScpiSentOrder());
			//			target.setPaymentProviderStatus(
			//					source.getPaymentProviderStatus() == null ? "" : source.getPaymentProviderStatus().getCode());
			//			target.setPaymentProviderTransactionType(
			//					source.getPaymentProviderTransactionType() == null ? "" : source.getPaymentProviderTransactionType().getCode());
			//			target.setPaymentB2BInvoiceCreatedSuccessfully(source.isPaymentB2BInvoiceCreatedSuccessfully());
			//			target.setSadadBillIdEmailSent(source.isSadadBillIdEmailSent());
			//			target.setPaymentUpdateEmailSent(source.isPaymentUpdateEmailSent());
			//			target.setEsalRefundStatus(source.getEsalRefundStatus() == null ? "" : source.getEsalRefundStatus().getCode());
			//			target.setEsalRefundValue(String.valueOf(source.getEsalRefundValue()));
			fillConsignmentCodes(source, target);
			fillConsignmentInvoiceNumbers(source, target);
			fillAllWarehouseCodes(source, target);
		}

	}

	protected void fillAllWarehouseCodes(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final String warehousing = CollectionUtils.isEmpty(source.getConsignments()) ? ""
				: source.getConsignments().stream()
						.filter(consignment -> consignment.getWarehouse() != null
								&& StringUtils.isNotBlank(consignment.getWarehouse().getCode()))
						.map(consignment -> consignment.getWarehouse().getCode()).collect(Collectors.joining(", "));
		target.setStoreIds(warehousing);
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void fillConsignmentInvoiceNumbers(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final String consInvoiceNumbers = source.getConsignments().stream()
				.filter(consignment -> StringUtils.isNotBlank(consignment.getInvoiceNumber()))
				.map(consignment -> consignment.getInvoiceNumber()).collect(Collectors.joining(", "));
		target.setConsInvoiceNumbers(consInvoiceNumbers);
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void fillConsignmentCodes(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final String consignmentCodes = source.getConsignments().stream().map(consignment -> {
			final String formattedCode = consignment.getCode().replace("cons", "D");
			return formattedCode.replace("_", "");
		}).collect(Collectors.joining(", "));
		target.setConsignmentCodes(consignmentCodes);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateSelectedBankTransfer(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source.getSelectedBankTransfer() != null)
		{
			target.setSelectedBankTransfer(bankTransferConverter.convert(source.getSelectedBankTransfer()));
		}

	}

	/*
	 * Adds applied and potential promotions.
	 */
	protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		addPromotions(source, getPromotionsService().getPromotionResults(source), prototype);
	}

	protected void addPromotions(final AbstractOrderModel source, final PromotionOrderResults promoOrderResults,
			final AbstractOrderData prototype)
	{
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);
		prototype.setQuoteDiscounts(createPrice(source, Double.valueOf(quoteDiscountsAmount)));

		final Pair<DiscountType, Double> quoteDiscountsTypeAndRate = getQuoteDiscountsTypeAndRate(source);
		prototype.setQuoteDiscountsType(quoteDiscountsTypeAndRate.getKey().getCode());
		prototype.setQuoteDiscountsRate(quoteDiscountsTypeAndRate.getValue());

		if (promoOrderResults != null)
		{
			final double productsDiscountsAmount = getProductsDiscountsAmount(source);
			final double orderDiscountsAmount = getOrderDiscountsAmount(source);

			prototype.setProductDiscounts(createPrice(source, Double.valueOf(productsDiscountsAmount)));
			prototype.setOrderDiscounts(createPrice(source, Double.valueOf(orderDiscountsAmount)));
			prototype.setTotalDiscounts(createPrice(source, Double.valueOf(productsDiscountsAmount + orderDiscountsAmount)));
			prototype.setTotalDiscountsWithQuoteDiscounts(
					createPrice(source, Double.valueOf(productsDiscountsAmount + orderDiscountsAmount + quoteDiscountsAmount)));
			prototype.setAppliedOrderPromotions(getPromotions(promoOrderResults.getAppliedOrderPromotions()));
			prototype.setAppliedProductPromotions(getPromotions(promoOrderResults.getAppliedProductPromotions()));
		}
	}


	protected double getQuoteDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue discount : discountValues)
					{
						final double value = discount.getAppliedValue();
						if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
								&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
						{
							discounts += value;
						}
					}
				}
			}
		}
		return discounts;
	}

	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						final double value = dValue.getAppliedValue();
						if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
								&& !CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(dValue.getCode()))
						{
							discounts += value;
						}
					}
				}
			}
		}
		return discounts;
	}

	protected PriceData createPrice(final AbstractOrderModel source, final Double val)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}

		final CurrencyModel currency = source.getCurrency();
		if (currency == null)
		{
			throw new IllegalArgumentException("source order currency must not be null");
		}

		// Get double value, handle null as zero
		final double priceValue = val != null ? val.doubleValue() : 0d;

		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);
	}

	/*
	 * Extracts (and converts to POJOs) promotions from given results.
	 */
	protected List<PromotionResultData> getPromotions(final List<PromotionResult> promotionsResults)
	{
		final ArrayList<PromotionResultModel> promotionResultModels = getModelService().getAll(promotionsResults,
				new ArrayList<PromotionResultModel>());
		return getPromotionResultConverter().convertAll(promotionResultModels);
	}

	protected Pair<DiscountType, Double> getQuoteDiscountsTypeAndRate(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		DiscountType discountType = DiscountType.PERCENT;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					// for now there is only one quote discount entry
					discounts = discount.getValue();
					if (discount.isAsTargetPrice())
					{
						discountType = DiscountType.TARGET;
					}
					else if (discount.isAbsolute())
					{
						discountType = DiscountType.ABSOLUTE;
					}
					break;
				}
			}
		}
		return Pair.of(discountType, Double.valueOf(discounts));
	}

	protected double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& !CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	protected Date getDueDate(final AbstractOrderModel order)
	{
		final Date creationtime = order.getInvoiceCreationDate();
		if (Objects.isNull(creationtime))
		{
			return null;
		}
		final BaseStoreModel store = order.getStore();
		final int esalInvoiceDueDate = store.getEsalInvoiceDueDate();
		final Calendar c = Calendar.getInstance();
		c.setTime(creationtime);
		c.add(Calendar.DATE, esalInvoiceDueDate);
		return c.getTime();
	}

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @return the promotionsResultConverter
	 */
	public Converter<PromotionResultModel, PromotionResultData> getPromotionResultConverter()
	{
		return promotionResultConverter;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateDeliveryPointOfService(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final PointOfServiceModel deliveryPointOfService = source.getSelectedDeliveryPointOfService();//getFirstDeliveryPointOfService(source.getEntries());

		if (deliveryPointOfService == null)
		{
			target.setPointOfServiceSelected(false);
			return;
		}

		target.setDeliveryPointOfService(getPointOfServiceConverter().convert(deliveryPointOfService));
		target.setPointOfServiceSelected(true);
	}

	/**
	 * @param entries
	 * @return
	 */
	private PointOfServiceModel getFirstDeliveryPointOfService(final List<AbstractOrderEntryModel> entries)
	{
		for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
		{
			if (abstractOrderEntryModel.getDeliveryPointOfService() != null)
			{
				return abstractOrderEntryModel.getDeliveryPointOfService();
			}
		}
		return null;
	}

	/**
	 * @return the pointOfServiceConverter
	 */
	protected Converter<PointOfServiceModel, PointOfServiceData> getPointOfServiceConverter()
	{
		return pointOfServiceConverter;
	}

	/**
	 * @return the shipmentTypeConverter
	 */
	public Converter<ShipmentType, ShipmentTypeData> getShipmentTypeConverter()
	{
		return shipmentTypeConverter;
	}

	/**
	 * @return the cityConverter
	 */
	public Converter<CityModel, CityData> getCityConverter()
	{
		return cityConverter;
	}
}
