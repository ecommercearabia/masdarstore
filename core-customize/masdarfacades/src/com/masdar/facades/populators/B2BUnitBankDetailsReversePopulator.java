/**
 *
 */
package com.masdar.facades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author mbaker
 *
 */
public class B2BUnitBankDetailsReversePopulator implements Populator<B2BUnitData, B2BUnitModel>
{

	@Override
	public void populate(final B2BUnitData source, final B2BUnitModel target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		target.setAccountHolderName(source.getAccountHolderName());
		target.setAccountNumber(source.getAccountNumber());
		target.setIban(source.getIban());
		target.setBankName(source.getBankName());

	}

}
