/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.converters.Populator;

import com.masdar.core.model.ProductConfigurationOptionModel;
import com.masdar.facades.data.ProductConfigurationOptionData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductConfigurationOptionPopulator
		implements Populator<ProductConfigurationOptionModel, ProductConfigurationOptionData>
{

	@Override
	public void populate(final ProductConfigurationOptionModel source, final ProductConfigurationOptionData target)
	{
		if (source != null)
		{
			target.setCode(source.getCode());
			target.setName(source.getName());
		}
	}

}
