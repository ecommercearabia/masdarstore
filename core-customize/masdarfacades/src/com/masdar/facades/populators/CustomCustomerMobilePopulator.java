/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCustomerMobilePopulator implements Populator<CustomerModel, CustomerData>
{

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	@Override
	public void populate(final CustomerModel source, final CustomerData target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "Source is null");
		Preconditions.checkNotNull(target, "target is null");
		if (source.getMobileCountry() != null)
		{
			target.setMobileCountry(getCountryConverter().convert(source.getMobileCountry()));
			target.setMobileNumber(source.getMobileNumber());
		}
	}

	protected Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}



}
