package com.masdar.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;


/**
 * Converter implementation for {@link de.hybris.platform.catalog.model.classification.ClassificationClassModel} as
 * source and {@link de.hybris.platform.commercefacades.product.data.ClassificationData} as target type.
 */
public class CustomClassificationPopulator implements Populator<ClassificationClassModel, ClassificationData>
{

	@Override
	public void populate(final ClassificationClassModel source, final ClassificationData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setHiddenPDP(source.isHiddenPDP());
	}
}
