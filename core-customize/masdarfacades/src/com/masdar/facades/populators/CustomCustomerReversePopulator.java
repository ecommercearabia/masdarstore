/**
 *
 */
package com.masdar.facades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.masdar.core.service.MobilePhoneService;


/**
 * @author mbaker
 *
 */
public class CustomCustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Override
	public void populate(final CustomerData source, final CustomerModel target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");

		populateMobileNumber(source, target);

	}

	protected void populateMobileNumber(final CustomerData customerData, final CustomerModel customerModel)
	{
		customerModel.setMobileNumber(customerModel.getMobileNumber());

		if (customerData.getMobileCountry() != null && StringUtils.isNotBlank(customerData.getMobileCountry().getIsocode())
				&& StringUtils.isNotBlank(customerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(
					customerData.getMobileCountry().getIsocode(), customerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(customerData.getMobileNumber());
			}

		}
		if (customerData.getMobileCountry() != null && !StringUtils.isEmpty(customerData.getMobileCountry().getIsocode()))
		{
			customerModel.setMobileCountry(commonI18NService.getCountry(customerData.getMobileCountry().getIsocode()));
		}
	}
}
