/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.masdar.core.enums.ShipmentType;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.facades.product.data.GenderData;


/**
 * Populates {@link GenderData} with name and code.
 */
public class ShipmentTypeDataPopulator implements Populator<ShipmentType, ShipmentTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Override
	public void populate(final ShipmentType source, final ShipmentTypeData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		target.setCode(source.getCode());
		target.setName(getTypeService().getEnumerationValue(source).getName());
	}
}
