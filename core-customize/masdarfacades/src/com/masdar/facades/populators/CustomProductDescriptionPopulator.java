/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductDescriptionPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * Populate the product data with the product's description
 */
public class CustomProductDescriptionPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductDescriptionPopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final String description = safeToString(getProductAttribute(productModel, ProductModel.DESCRIPTION));
		if (description != null && !description.isEmpty())
		{
			productData.setDescription(description);
			return;
		}

		if (productModel instanceof VariantProductModel)
		{
			final ProductModel VarProduct = ((VariantProductModel) productModel).getBaseProduct();
			if (VarProduct.getDescription() != null && !VarProduct.getDescription().isEmpty())
			{
				productData.setDescription(VarProduct.getDescription());
			}
			else
			{
				populate((SOURCE) VarProduct, productData);
			}

		}
	}
}
