/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import com.google.common.math.DoubleMath;


/**
 *
 * The Class PaymentAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class PriceAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	private static final double EPSILON = 0.01d;

	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populateSubTotalBeforeSaving(source, target);
			populateNetAmount(source, target);
		}
	}

	private void populateNetAmount(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(source);
		final double orderDiscountsAmount = getOrderDiscountsAmount(source);
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);
		final double totalDiscountsAmount = productsDiscountsAmount + orderDiscountsAmount;
		final double totalDiscountsWithQuoteDiscountsAmount = totalDiscountsAmount + quoteDiscountsAmount;

		final List<AbstractOrderEntryModel> entries = source.getEntries();

		if (source.getEntries() != null && !source.getEntries().isEmpty())
		{
			double totalBasePrice = 0.0;
			for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
			{
				totalBasePrice += (abstractOrderEntryModel.getQuantity() == null ? 0
						: abstractOrderEntryModel.getBasePrice() * abstractOrderEntryModel.getQuantity());
			}
			target.setTotalPriceWithoutTaxAndDiscountsWithQuoteDiscounts(
					createPrice(source, source.getTotalPrice() + totalDiscountsWithQuoteDiscountsAmount));
			target.setTotalPriceWithoutTaxAndDiscounts(createPrice(source, source.getTotalPrice() + totalDiscountsAmount));
			target.setNetAmount(createPrice(source, totalBasePrice));
			target.setTotalDiscountsWithQuoteDiscounts(createPrice(source, totalDiscountsWithQuoteDiscountsAmount));
			target.setCustomTotalDiscounts(createPrice(source, productsDiscountsAmount));

		}

	}


	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	protected double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& !CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	protected double getQuoteDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	/**
	 *
	 */
	protected void populateSubTotalBeforeSaving(final AbstractOrderModel source, final AbstractOrderData target)
	{
		double subTotalBeforeSaving = 0;
		final List<AbstractOrderEntryModel> entryModels = source.getEntries();

		for (final AbstractOrderEntryModel orderEntry : entryModels)
		{
			final Double basePrice = orderEntry.getBasePrice();
			final Long quantity = orderEntry.getQuantity() == null ? 0L : orderEntry.getQuantity();
			if (basePrice != null && quantity != null)
			{
				final double beforeSavingTotlePrice = basePrice.doubleValue() * quantity.doubleValue();
				subTotalBeforeSaving += beforeSavingTotlePrice;
			}
		}

		final PriceData subTotalBeforeSavingPrice = getPriceDataFactory().create(PriceDataType.BUY,
				BigDecimal.valueOf(subTotalBeforeSaving), source.getCurrency());
		target.setSubTotalBeforeSavingPrice(subTotalBeforeSavingPrice);
	}

	protected PriceData createPrice(final AbstractOrderModel abstractOrderModel, final Double val)
	{
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val == null ? 0 : val.doubleValue()),
				abstractOrderModel.getCurrency());
	}

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

}
