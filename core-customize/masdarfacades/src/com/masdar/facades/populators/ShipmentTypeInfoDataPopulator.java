/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Objects;

import javax.annotation.Resource;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.model.CityModel;
import com.masdar.facades.order.data.ShipmentTypeData;
import com.masdar.facades.shipmenttype.data.ShipmentTypeInfoData;



/**
 * @author monzer
 *
 */
public class ShipmentTypeInfoDataPopulator implements Populator<ShipmentTypeInfo, ShipmentTypeInfoData>
{

	@Resource(name = "shipmentTypeConverter")
	private Converter<ShipmentType, ShipmentTypeData> shipmentTypeConverter;

	@Resource(name = "pointOfServiceConverter")
	private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

	@Resource(name = "cityConverter")
	private Converter<CityModel, CityData> cityConverter;

	@Override
	public void populate(final ShipmentTypeInfo source, final ShipmentTypeInfoData target) throws ConversionException
	{
		if (source == null || target == null)
		{
			return;
		}

		if (Objects.nonNull(source.getShipmentType()))
		{
			target.setSelectedShipmentType(getShipmentTypeConverter().convert(source.getShipmentType()));
		}
		if (Objects.nonNull(source.getPointOfService()))
		{
			target.setSelectedPointOfService(getPointOfServiceConverter().convert(source.getPointOfService()));
		}

		if (Objects.nonNull(source.getCity()))
		{
			target.setSelectedCity(getCityConverter().convert(source.getCity()));
		}
	}

	/**
	 * @return the shipmentTypeConverter
	 */
	public Converter<ShipmentType, ShipmentTypeData> getShipmentTypeConverter()
	{
		return shipmentTypeConverter;
	}

	/**
	 * @return the pointOfServiceConverter
	 */
	public Converter<PointOfServiceModel, PointOfServiceData> getPointOfServiceConverter()
	{
		return pointOfServiceConverter;
	}

	/**
	 * @return the cityConverter
	 */
	public Converter<CityModel, CityData> getCityConverter()
	{
		return cityConverter;
	}

}
