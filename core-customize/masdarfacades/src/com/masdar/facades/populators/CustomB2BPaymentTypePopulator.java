/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.populators;

import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.masdar.facades.facade.CustomCheckoutFacade;


/**
 * Populates {@link CheckoutPaymentType} to {@link B2BPaymentTypeData}.
 */
public class CustomB2BPaymentTypePopulator implements Populator<CheckoutPaymentType, B2BPaymentTypeData>
{
	private TypeService typeService;

	@Resource(name = "customB2BCheckoutFacade")
	private CustomCheckoutFacade b2bCheckoutFacade;

	@Override
	public void populate(final CheckoutPaymentType source, final B2BPaymentTypeData target)
	{
		//		CheckoutPaymentType checkoutPaymentTypeValue = source;
		if (source != null)
		{
			target.setCode(source.getCode());
			target.setDisplayName(getTypeService().getEnumerationValue(source).getName());
			//			final List<B2BPaymentTypeData> paymentTypes = b2bCheckoutFacade.getSupportedPaymentTypes();
			//			checkoutPaymentTypeValue = CheckoutPaymentType.valueOf(paymentTypes.get(0).getCode());
		}
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

}
