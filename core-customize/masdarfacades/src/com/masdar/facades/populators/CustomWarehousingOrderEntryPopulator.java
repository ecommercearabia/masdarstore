package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.warehousingfacades.order.converters.populator.WarehousingOrderEntryPopulator;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;


/**
 * Warehousing populator for converting orderEntry's dynamic attributes added by oms
 */

public class CustomWarehousingOrderEntryPopulator extends WarehousingOrderEntryPopulator
{
	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}

	@Override
	protected void addTotals(final AbstractOrderEntryModel source, final OrderEntryData entry)
	{

		if (source.getBasePrice() != null)
		{
			try
			{
				entry.setBasePrice(createPrice(source, getBasePrice(source)));
			}
			catch (final Exception e)
			{
				entry.setBasePrice(createPrice(source, source.getBasePrice()));
			}
		}
		if (source.getTotalPrice() != null)
		{
			entry.setTotalPrice(createPrice(source, source.getTotalPrice()));
		}
	}

	/**
	 * @param source
	 * @return
	 */
	protected Double getBasePrice(final AbstractOrderEntryModel source)
	{
		if (source != null && source.getProduct() != null)
		{
			final PriceDataType priceType;
			final PriceInformation info;
			if (CollectionUtils.isEmpty(source.getProduct().getVariants()))
			{
				priceType = PriceDataType.BUY;
				info = getCustomCommercePriceService().getWebPriceForProductWithTax(source.getProduct(), true);
			}
			else
			{
				priceType = PriceDataType.FROM;
				info = getCustomCommercePriceService().getFromPriceForProduct(source.getProduct());
			}

			if (info != null && info.getPriceValue() != null)
			{
				return info.getPriceValue().getValue();
			}
		}
		return source.getBasePrice();
	}
}
