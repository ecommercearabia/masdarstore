package com.masdar.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BCustomerPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;


/**
 *
 * The Class CustomB2BCustomerPopulator.
 *
 * @author monzer
 */
public class CustomB2BCustomerPopulator extends B2BCustomerPopulator
{

	/** The principal converter. */
	private Converter<PrincipalModel, PrincipalData> principalConverter;

	/** The address converter. */
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "commercialRegistrationConverter")
	private Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfoData> commercialRegistrationConverter;

	@Override
	public void populate(final CustomerModel source, final CustomerData target) throws ConversionException
	{
		super.populate(source, target);

		if (source instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customer = (B2BCustomerModel) source;
			target.setMobileNumber(customer.getMobileNumber());
		}
	}

	/**
	 * Populate unit.
	 *
	 * @param customer
	 *           the customer
	 * @param target
	 *           the target
	 */
	@Override
	protected void populateUnit(final B2BCustomerModel customer, final CustomerData target)
	{
		super.populateUnit(customer, target);
		final B2BUnitModel parent = getB2bUnitService().getParent(customer);


		if (parent != null && target.getUnit() != null)
		{
			target.getUnit().setVatId(parent.getVatID());
			target.getUnit().setCommercialRegistrationNumber(parent.getId());
			if (parent.getAccountManager() != null)
			{
				target.getUnit().setParentUnitManager(getPrincipalConverter().convert(parent.getAccountManager()));
			}
			if (parent.getShippingAddress() != null)
			{
				target.getUnit().setShippingAddress(getAddressConverter().convert(parent.getShippingAddress()));
			}

			target.getUnit()
					.setCompanyRegistrationInfoData(getCommercialRegistrationConverter().convert(parent.getCompanyRegistrationInfo()));
		}
	}

	/**
	 * Gets the principal converter.
	 *
	 * @return the principalConverter
	 */
	public Converter<PrincipalModel, PrincipalData> getPrincipalConverter()
	{
		return principalConverter;
	}

	/**
	 * Sets the principal converter.
	 *
	 * @param principalConverter
	 *           the principalConverter to set
	 */
	@Required
	public void setPrincipalConverter(final Converter<PrincipalModel, PrincipalData> principalConverter)
	{
		this.principalConverter = principalConverter;
	}

	/**
	 * Gets the address converter.
	 *
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * Sets the address converter.
	 *
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	/**
	 * @return the commercialRegistrationConverter
	 */
	public Converter<CompanyRegistrationInfoModel, CompanyRegistrationInfoData> getCommercialRegistrationConverter()
	{
		return commercialRegistrationConverter;
	}




}
