/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.fest.util.Collections;
import org.springframework.util.CollectionUtils;

import com.masdar.masdarfacades.data.AttachmentData;


/**
 * @author monzer
 *
 */
public class CustomConsignmentDataPopulator implements Populator<ConsignmentModel, ConsignmentData>
{

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;
	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final ConsignmentModel source, final ConsignmentData target) throws ConversionException
	{
		if (source == null || CollectionUtils.isEmpty(source.getConsignmentEntries()) || source.getOrder() == null
				|| target == null)
		{
			return;
		}

		target.setInvoiceNumber(source.getInvoiceNumber());
		target.setInvoiceDate(source.getInvoiceDate());
		target.setOrderNumber(source.getOrder().getOrderNumber());

		final Set<ConsignmentEntryModel> entries = source.getConsignmentEntries();
		double netTotalPrices = 0.0d;
		double netTotalPricesValue = 0.0d;
		double totalDiscountsVal = 0.0d;
		double totalDiscountsValue = 0.0d;
		double totalTaxPriceVal = 0.0d;
		double totalTaxPriceValue = 0.0d;
		double totalPriceWithoutTaxAndDiscountsVal = 0.0d;
		double totalPriceWithoutTaxAndDiscountsValue = 0.0d;
		double totalPriceWithTaxVal = 0.0d;
		double totalPriceWithTaxValue = 0.0d;

		if (!CollectionUtils.isEmpty(entries))
		{
			final List<ConsignmentEntryData> entriesData = consignmentEntryConverter.convertAll(entries);

			for (final ConsignmentEntryData consignmentEntryData : entriesData)
			{
				netTotalPrices += consignmentEntryData.getNetTotalPrice() == null ? 0
						: consignmentEntryData.getNetTotalPrice().getValue().setScale(2, RoundingMode.HALF_UP).doubleValue();
				netTotalPricesValue += consignmentEntryData.getNetTotalPriceValue();

				totalDiscountsVal += (consignmentEntryData.getNetBaseDiscount() == null || consignmentEntryData.getQuantity() == null
						? 0
						: consignmentEntryData.getNetBaseDiscount().getValue().setScale(2, RoundingMode.HALF_UP).doubleValue()
								* consignmentEntryData.getQuantity());
				totalDiscountsValue += consignmentEntryData.getNetDiscountValue();

				//				totalTaxPriceVal += consignmentEntryData.getTotalTaxPrice() == null ? 0
				//						: consignmentEntryData.getTotalTaxPrice().getValue().setScale(2, RoundingMode.HALF_UP).doubleValue();
				//				totalTaxPriceValue += consignmentEntryData.getTotalTaxPriceValue();
			}

			totalPriceWithoutTaxAndDiscountsVal = netTotalPrices + totalDiscountsVal;
			totalPriceWithoutTaxAndDiscountsValue = netTotalPricesValue + totalDiscountsValue;
		}

		double orderDiscountsPercentage = 0.0d;
		final AbstractOrderModel order = source.getOrder();
		if (!Collections.isEmpty(order.getGlobalDiscountValues()))
		{
			orderDiscountsPercentage = getOrderDiscountsPercentage(order, source);
			final double consignmentDiscountsFromOrderVal = netTotalPrices * orderDiscountsPercentage / 100;
			totalDiscountsVal += consignmentDiscountsFromOrderVal;

			final double consignmentDiscountsFromOrderValue = netTotalPricesValue * orderDiscountsPercentage / 100;
			totalDiscountsValue += consignmentDiscountsFromOrderValue;
		}


		final double totalPriceWithDiscountVal = totalPriceWithoutTaxAndDiscountsVal - totalDiscountsVal;
		totalTaxPriceVal = totalPriceWithDiscountVal * 15 / 100;
		totalPriceWithTaxVal = totalPriceWithDiscountVal + totalTaxPriceVal;


		final double totalPriceWithDiscountValue = totalPriceWithoutTaxAndDiscountsValue - totalDiscountsValue;
		totalTaxPriceValue = totalPriceWithDiscountValue * 15 / 100;
		totalPriceWithTaxValue = totalPriceWithDiscountValue + totalTaxPriceValue;


		final PriceData totalPriceWithoutTaxAndDiscounts = createPrice(source, totalPriceWithoutTaxAndDiscountsVal);
		final PriceData totalTaxPrice = createPrice(source, totalTaxPriceVal);
		final PriceData totalDiscounts = createPrice(source, totalDiscountsVal);



		final PriceData totalPriceWithTax = createPrice(source, totalPriceWithTaxVal);
		target.setTotalPriceWithoutTaxAndDiscounts(totalPriceWithoutTaxAndDiscounts);
		target.setTotalTaxPrice(totalTaxPrice);
		target.setTotalPriceWithTax(totalPriceWithTax);
		target.setTotalDiscounts(totalDiscounts);
		target.setTotalPriceWithoutTaxAndDiscountsValue(totalPriceWithoutTaxAndDiscountsValue);
		target.setTotalTaxPriceValue(totalTaxPriceValue);
		target.setTotalPriceWithTaxValue(totalPriceWithTaxValue);
		target.setTotalDiscountsValue(totalDiscountsValue);
		populateDeliveryNote(source, target);
		populateDeliveryCost(source, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateDeliveryCost(final ConsignmentModel source, final ConsignmentData target)
	{
		final AbstractOrderModel order = source.getOrder();
		if (source.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			target.setDeliveryCost(order.getDeliveryCost().doubleValue());
		}

	}

	/**
	 * @param order
	 * @param source
	 * @return
	 */
	private double getOrderDiscountsPercentage(final AbstractOrderModel order, final ConsignmentModel source)
	{
		final double totalOrderDiscount = order.getGlobalDiscountValues().stream().map(e -> e.getAppliedValue())
				.reduce(0.0d, Double::sum).doubleValue();
		final double totalPrice = order.getSubtotal();
		return (totalOrderDiscount / totalPrice) * 100;
	}

	private void populateDeliveryNote(final ConsignmentModel source, final ConsignmentData target)
	{
		if (source.getDeliveryNoteAttachment() != null)
		{
			final MediaModel deliveryNoteAttachment = source.getDeliveryNoteAttachment();

			final AttachmentData attachmentData = new AttachmentData();
			attachmentData.setFilename(deliveryNoteAttachment.getRealFileName());
			attachmentData.setURL(deliveryNoteAttachment.getURL());
			target.setDeliveryNoteAttachment(attachmentData);
		}
	}

	protected PriceData createPrice(final ConsignmentModel consignmentModel, final Double val)
	{
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
				consignmentModel.getOrder().getCurrency());
	}

	protected double formatPrice(final double value, final CurrencyModel currency)
	{
		final int digits = currency.getDigits().intValue();
		return commonI18NService.roundCurrency(value, digits);
	}


	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}


	public double getDeliveryCost(final ConsignmentModel consignment)
	{
		final OrderModel order = getOrder(consignment);
		if (order.getDeliveryCostAppliedOnConsignment() != null
				&& consignment.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			return order.getDeliveryCost() == null ? 0.0d : order.getDeliveryCost().doubleValue();
		}

		return 0.0d;
	}

	public double getDeliveryCostTax(final ConsignmentModel consignment)
	{
		final OrderModel order = getOrder(consignment);
		final double deliveryCost = getDeliveryCost(consignment);


		final double taxVal = order == null || CollectionUtils.isEmpty(order.getTotalTaxValues()) ? 0.0d
				: order.getTotalTaxValues().iterator().next().getValue();

		return deliveryCost * taxVal / 100;
	}

	public OrderModel getOrder(final ConsignmentModel consignment)
	{
		return (OrderModel) consignment.getOrder();

	}
}
