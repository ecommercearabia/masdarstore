/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;


/**
 * @author Husam
 *
 */
public class BankTransactionOrderPopulator implements Populator<OrderModel, OrderData>
{
	private static final Logger LOG = Logger.getLogger(BankTransactionOrderPopulator.class);

	@Override
	public void populate(final OrderModel orderModel, final OrderData orderData) throws ConversionException
	{
		if (orderModel != null && orderData != null)
		{
			orderData.setBankTransactionId(orderModel.getBankTransactionId());
			orderData.setInvoiceNumber(orderModel.getInvoiceNumber());
			orderData.setOrderNumber(orderModel.getOrderNumber());
			orderData.setInvoiceDate(orderModel.getInvoiceDate());
		}

	}

}
