package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


public class CustomQuotePopulator implements Populator<QuoteModel, QuoteData>
{
	private static final Logger LOG = Logger.getLogger(CustomQuotePopulator.class);

	@Resource(name = "b2BCustomerConverter")
	private Converter<UserModel, CustomerData> b2bCustomerConverter;


	/**
	 * @return the b2bCustomerConverter
	 */
	protected Converter<UserModel, CustomerData> getB2bCustomerConverter()
	{
		return b2bCustomerConverter;
	}


	@Override
	public void populate(final QuoteModel source, final QuoteData target)
	{
		if (source == null || target == null)
		{
			return;
		}

		populateB2BCustomer(source, target);

	}


	/**
	 * @param source
	 * @param target
	 */
	protected void populateB2BCustomer(final QuoteModel source, final QuoteData target)
	{
		if (source.getUser() == null)
		{
			return;
		}

		target.setB2bCustomerData(getB2bCustomerConverter().convert(source.getUser()));
	}

}
