/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;

import com.google.common.math.DoubleMath;


/**
 * @author monzer
 *
 */
public class PriceOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;
	private static final double EPSILON = 0.01d;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) throws ConversionException
	{
		if (source == null || target == null)
		{
			return;
		}

		double taxValue = 0.0;
		double taxAppliedValue = 0.0;
		if (!CollectionUtils.isEmpty(source.getTaxValues()))
		{
			for (final TaxValue value : source.getTaxValues())
			{
				taxValue += value.getValue();
				taxAppliedValue += value.getAppliedValue();
			}
		}
		target.setTaxValue(taxValue);
		target.setTotalTaxPrice(createPrice(source, taxAppliedValue));

		double netDiscount = 0.0;
		if (!CollectionUtils.isEmpty(source.getDiscountValues()))
		{
			for (final DiscountValue value : source.getDiscountValues())
			{
				netDiscount += value.getAppliedValue();
			}
		}
		target.setNetBasePrice(createPrice(source, source.getBasePrice()));
		if (source.getQuantity() != null && source.getQuantity() > 0)
		{
			target.setCustomNetBaseDiscount(createPrice(source, netDiscount));
			target.setNetBaseDiscount(createPrice(source, netDiscount / source.getQuantity()));

			final double priceWithoutTaxAfterDiscount = target.getNetBasePrice().getValue().doubleValue()
					- (target.getCustomNetBaseDiscount().getValue().doubleValue() / source.getQuantity().doubleValue());

			target.setPriceWithoutTaxAfterDiscount(createPrice(source, priceWithoutTaxAfterDiscount));
		}

		target.setNetTotalPrice(createPrice(source, source.getTotalPrice()));
		target.setTotalPrice(createPrice(source, (source.getTotalPrice() == null ? 0 : source.getTotalPrice()) + taxAppliedValue));

		final Pair<DiscountType, Double> quoteDiscountsTypeAndRate = getQuoteDiscountsTypeAndRate(source);
		target.setQuoteDiscountsType(quoteDiscountsTypeAndRate.getKey().getCode());
		target.setQuoteDiscountsRate(quoteDiscountsTypeAndRate.getValue());
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);
		target.setQuoteDiscounts(createPrice(source, quoteDiscountsAmount));
		//			target.setNetTotalPriceWithoutQuoteDiscount(createPrice(source, quoteDiscountsAmount));

	}

	protected double getQuoteDiscountsAmount(final AbstractOrderEntryModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					discounts += value;
				}
			}
		}
		return discounts;
	}

	protected PriceData createPrice(final AbstractOrderEntryModel orderEntry, final Double val)
	{
		if (orderEntry == null || orderEntry.getOrder() == null)
		{
			return null;
		}

		final CurrencyModel curr = orderEntry.getOrder() == null ? getCommonI18NService().getBaseCurrency()
				: orderEntry.getOrder().getCurrency();
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val == null ? 0 : val.doubleValue()), curr);
	}


	protected Pair<DiscountType, Double> getQuoteDiscountsTypeAndRate(final AbstractOrderEntryModel source)
	{
		double discounts = 0.0d;
		DiscountType discountType = DiscountType.PERCENT;
		final List<DiscountValue> discountList = source.getDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
						&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
				{
					// for now there is only one quote discount entry
					discounts = discount.getValue();
					if (discount.isAsTargetPrice())
					{
						discountType = DiscountType.TARGET;
					}
					else if (discount.isAbsolute())
					{
						discountType = DiscountType.ABSOLUTE;
					}
					break;
				}
			}
		}
		return Pair.of(discountType, Double.valueOf(discounts));
	}

	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}
}
