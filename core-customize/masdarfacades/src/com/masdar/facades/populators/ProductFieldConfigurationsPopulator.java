/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.facades.populators;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguredProductInfoModel;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.model.ProductConfigurationOptionModel;
import com.masdar.facades.data.ProductConfigurationOptionData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductFieldConfigurationsPopulator<T extends AbstractOrderEntryProductInfoModel>
		implements Populator<T, List<ConfigurationInfoData>>
{
	@Resource(name = "productConfigurationOptionConverter")
	private Converter<ProductConfigurationOptionModel, ProductConfigurationOptionData> productConfigurationOptionConverter;

	@Override
	public void populate(final T source, final List<ConfigurationInfoData> target)
	{
		if (source.getConfiguratorType() == ConfiguratorType.TEXTFIELD)
		{
			if (!(source instanceof TextFieldConfiguredProductInfoModel))
			{
				throw new ConversionException(
						"Instance with type " + source.getConfiguratorType() + " is of class " + source.getClass().getName()
								+ " which is not convertible to " + TextFieldConfiguredProductInfoModel.class.getName());
			}
			final ConfigurationInfoData item = new ConfigurationInfoData();
			final TextFieldConfiguredProductInfoModel model = (TextFieldConfiguredProductInfoModel) source;
			item.setConfiguratorType(model.getConfiguratorType());
			item.setConfigurationLabel(model.getConfigurationLabel());
			item.setConfigurationValue(model.getConfigurationValue());
			item.setStatus(model.getProductInfoStatus());
			if (!CollectionUtils.isEmpty(model.getOptions()))
			{
				item.setOptions(productConfigurationOptionConverter.convertAll(model.getOptions()));
			}
			target.add(item);
		}
	}
}
