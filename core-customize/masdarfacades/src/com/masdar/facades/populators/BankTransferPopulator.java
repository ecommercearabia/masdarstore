/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.masdar.core.model.BankTransferModel;
import com.masdar.masdarfacades.data.BankTransferData;

/**
 * @author monzer
 *
 */
public class BankTransferPopulator implements Populator<BankTransferModel, BankTransferData>
{

	@Override
	public void populate(final BankTransferModel source, final BankTransferData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			target.setName(source.getName());
			target.setCode(source.getCode());
		}
	}

}
