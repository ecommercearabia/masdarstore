/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.converters.Populator;

import com.masdar.core.model.ProductConfigurationOptionModel;
import com.masdar.facades.data.ProductConfigurationOptionData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductConfigurationOptionReversePopulator
		implements Populator<ProductConfigurationOptionData, ProductConfigurationOptionModel>
{

	@Override
	public void populate(final ProductConfigurationOptionData source, final ProductConfigurationOptionModel target)
	{
		if (source != null)
		{
			target.setCode(source.getCode());
			target.setName(source.getName());
		}
	}

}
