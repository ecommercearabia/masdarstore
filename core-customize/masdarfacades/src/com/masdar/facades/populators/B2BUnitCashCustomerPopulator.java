/**
 *
 */
package com.masdar.facades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author Husam Dababneh
 *
 */
public class B2BUnitCashCustomerPopulator implements Populator<B2BUnitModel, B2BUnitData>
{

	@Override
	public void populate(final B2BUnitModel source, final B2BUnitData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter source cannot be null.");
		validateParameterNotNull(target, "Parameter target cannot be null.");
		target.setCashCustomer(source.isCashCustomer());
	}

}
