/**
 *
 */
package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;


/**
 * @author monzer
 *
 */
public class CustomPointOfServicePopulator implements Populator<PointOfServiceModel, PointOfServiceData>
{

	@Override
	public void populate(final PointOfServiceModel source, final PointOfServiceData target) throws ConversionException
	{
		if (source != null && target != null)
		{
			target.setDisplayNameLocalized(source.getDisplayNameLocalized());
			target.setActivity(source.getActivity());
			target.setLocation(source.getLocation());
		}
	}

}
