package com.masdar.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;


/**
 * Warehousing populator for converting orderEntry's dynamic attributes added by oms
 */

public class CustomOrderEntryPopulator extends OrderEntryPopulator
{
	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	/**
	 * @return the customCommercePriceService
	 */
	protected CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}

	@Override
	protected void addTotals(final AbstractOrderEntryModel source, final OrderEntryData entry)
	{

		if (source.getBasePrice() != null)
		{
			entry.setBasePrice(createPrice(source, getBasePrice(source)));
		}

		if (source.getTotalPrice() != null)
		{
			entry.setNetTotalPrice(createPrice(source, source.getTotalPrice()));
			entry.setTotalPrice(createPrice(source, source.getTotalPrice()));
		}


		if (source instanceof OrderEntryModel)
		{
			final OrderEntryModel orderEntry = (OrderEntryModel) source;
			entry.setQuantityCancelled(orderEntry.getQuantityCancelled());
		}


	}

	/**
	 * @param source
	 * @return
	 */
	protected Double getBasePrice(final AbstractOrderEntryModel source)
	{
		if (source != null && source.getProduct() != null)
		{
			final PriceInformation info;
			if (CollectionUtils.isEmpty(source.getProduct().getVariants()))
			{
				info = getCustomCommercePriceService().getWebPriceForProductWithTax(source.getProduct(), true);
			}
			else
			{
				info = getCustomCommercePriceService().getFromPriceForProduct(source.getProduct());
			}

			if (info != null && info.getPriceValue() != null)
			{
				return info.getPriceValue().getValue();
			}
		}
		return 0d;
	}
}
