package com.masdar.facades.populators;

import de.hybris.platform.b2bacceleratorfacades.order.populators.B2BCartPopulator;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;


/**
 * Populates {@link CartData} with {@link CartModel}.
 */
public class CustomB2BCartPopulator<T extends CartData> extends B2BCartPopulator<T>
{

	@Override
	public void populate(final CartModel source, final T target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		addDeliveryAddress(source, target);
		addDeliveryMethod(source, target);

		for (final AbstractOrderEntryModel entry : source.getEntries())
		{
			if (entry.getCostCenter() != null)
			{
				target.setCostCenter(getB2BCostCenterConverter().convert(entry.getCostCenter()));
				break;
			}
		}

		target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
		final CheckoutPaymentType paymentType = (source.getPaymentType() != null ? source.getPaymentType()
				: null);

		target.setPaymentType(getB2bPaymentTypeConverter().convert(paymentType));

		if (!CheckoutPaymentType.ACCOUNT.equals(paymentType) && !CheckoutPaymentType.BANK.equals(paymentType)
				&& !CheckoutPaymentType.ESAL.equals(paymentType))
		{
			addPaymentInformation(source, target);
		}

		if (CollectionUtils.isNotEmpty(source.getB2bcomments()))
		{
			target.setB2BComment(getB2BCommentConverter().convert(source.getB2bcomments().iterator().next()));
		}
		target.setB2bCustomerData(getB2bCustomerConverter().convert(source.getUser()));
		target.setQuoteAllowed(Boolean.valueOf(getB2bOrderService().isQuoteAllowed(source)));
	}


}
