/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.facades.constants;

/**
 * Global class for all MasdarFacades constants.
 */
public class MasdarFacadesConstants extends GeneratedMasdarFacadesConstants
{
	public static final String EXTENSIONNAME = "masdarfacades";

	private MasdarFacadesConstants()
	{
		//empty
	}
}
