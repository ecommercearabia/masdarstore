/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdartimeslotfacades.validation;

import com.masdar.masdartimeslotfacades.TimeSlotInfoData;
import com.masdar.masdartimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 */
public interface TimeSlotValidationService
{
	public boolean validate(final TimeSlotInfoData timeSlotInfo) throws TimeSlotException;
}
