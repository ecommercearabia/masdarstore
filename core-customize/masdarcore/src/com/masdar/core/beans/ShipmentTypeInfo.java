/**
 *
 */
package com.masdar.core.beans;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import com.masdar.core.enums.ShipmentType;
import com.masdar.core.model.CityModel;


/**
 * @author monzer
 *
 */
public class ShipmentTypeInfo
{

	private final ShipmentType shipmentType;
	private final CityModel city;
	private final PointOfServiceModel pointOfService;

	/**
	 * @param user
	 * @param city
	 * @param pointOfService
	 */
	public ShipmentTypeInfo(final ShipmentType shipmentType, final CityModel city, final PointOfServiceModel pointOfService)
	{
		super();
		this.shipmentType = shipmentType;
		this.city = city;
		this.pointOfService = pointOfService;
	}

	public ShipmentType getShipmentType()
	{
		return shipmentType;
	}

	public CityModel getCity()
	{
		return city;
	}

	public PointOfServiceModel getPointOfService()
	{
		return pointOfService;
	}

}
