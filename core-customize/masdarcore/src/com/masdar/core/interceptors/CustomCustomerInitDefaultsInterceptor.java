/**
 *
 */
package com.masdar.core.interceptors;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam Dababneh
 *
 */
public class CustomCustomerInitDefaultsInterceptor implements InitDefaultsInterceptor<CustomerModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomCustomerInitDefaultsInterceptor.class);

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	private static final String B2C_CUSTOMER_GROUP = "b2ccustomergroup";
	private static final String B2C_CUSTOMER_DEFFULT_REGISTRATION_GROUPS_PROP = "b2ccustomer.registration.default.groups";
	private static final String B2B_CUSTOMER_DEFFULT_REGISTRATION_GROUPS_PROP = "b2bcustomer.registration.default.groups";

	@Override
	public void onInitDefaults(final CustomerModel model, final InterceptorContext paramInterceptorContext)
			throws InterceptorException
	{
		String groupsProperties = B2C_CUSTOMER_DEFFULT_REGISTRATION_GROUPS_PROP;
		if (model instanceof B2BCustomerModel)
		{
			groupsProperties = B2B_CUSTOMER_DEFFULT_REGISTRATION_GROUPS_PROP;
		}

		final HashSet<PrincipalGroupModel> defaultB2CCustomerGroup = getDefaultB2CCustomerGroup(groupsProperties);

		if (defaultB2CCustomerGroup == null)
		{
			return;
		}

		final HashSet<PrincipalGroupModel> allGroups = new HashSet<>(model.getAllGroups());
		allGroups.addAll(defaultB2CCustomerGroup);
		model.setGroups(allGroups);
	}

	/**
	 * @param groupsProperties
	 * @return
	 */
	private HashSet<PrincipalGroupModel> getDefaultB2CCustomerGroup(final String groupsProperties)
	{
		final List<String> groupsIds = Arrays
				.asList(getConfigurationService().getConfiguration().getString(groupsProperties).split(","));

		if (Collections.isEmpty(groupsIds))
		{
			return null;
		}


		final HashSet<PrincipalGroupModel> allGroups = new HashSet<>(groupsIds.size());

		for (final String groupId : groupsIds)
		{
			final PrincipalGroupModel userGroup = getUserGroup(groupId);
			if (userGroup != null)
			{
				allGroups.add(userGroup);
				continue;
			}
			LOG.warn("UserGroup with UID[{}] does not exist, please create it", B2C_CUSTOMER_GROUP);
		}

		return Collections.isEmpty(groupsIds) ? null : allGroups;
	}

	/**
	 * @param groupId
	 * @return
	 */
	private PrincipalGroupModel getUserGroup(final String groupId)
	{
		try
		{
			final UserGroupModel userGroupForUID = getUserService().getUserGroupForUID(B2C_CUSTOMER_GROUP);
			return userGroupForUID;
		}
		catch (final UnknownIdentifierException e)
		{
			return null;
		}
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}



}
