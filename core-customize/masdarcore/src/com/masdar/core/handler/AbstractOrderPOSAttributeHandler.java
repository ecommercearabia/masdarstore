/**
 *
 */
package com.masdar.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.springframework.util.CollectionUtils;


/**
 * @author monzer
 *
 */
public class AbstractOrderPOSAttributeHandler implements DynamicAttributeHandler<PointOfServiceModel, AbstractOrderModel>
{

	@Override
	public PointOfServiceModel get(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null || CollectionUtils.isEmpty(abstractOrderModel.getEntries()))
		{
			return null;
		}

		return abstractOrderModel.getEntries().get(0).getDeliveryPointOfService();
	}

	@Override
	public void set(final AbstractOrderModel abstractOrderModel, final PointOfServiceModel shipmentType)
	{
		throw new UnsupportedOperationException();
	}

}
