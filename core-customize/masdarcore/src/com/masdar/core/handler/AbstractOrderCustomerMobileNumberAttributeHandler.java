package com.masdar.core.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


/**
 * @author monzer
 *
 */
public class AbstractOrderCustomerMobileNumberAttributeHandler implements DynamicAttributeHandler<String, AbstractOrderModel>
{

	@Override
	public String get(final AbstractOrderModel abstractOrderModel)
	{
		if (abstractOrderModel == null || abstractOrderModel.getUser() == null
				|| !(abstractOrderModel.getUser() instanceof CustomerModel))
		{
			return null;
		}

		final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();

		return customerModel.getMobileNumber();
	}

	@Override
	public void set(final AbstractOrderModel abstractOrderModel, final String shipmentType)
	{
		throw new UnsupportedOperationException();
	}

}
