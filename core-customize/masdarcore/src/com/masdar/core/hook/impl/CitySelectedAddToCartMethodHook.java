package com.masdar.core.hook.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.service.CustomUserService;


public class CitySelectedAddToCartMethodHook implements CommerceAddToCartMethodHook
{
	private static final Logger LOG = LoggerFactory.getLogger(CitySelectedAddToCartMethodHook.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	@Override
	public void beforeAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameters", parameters);

		final CartModel cartModel = parameters.getCart();

		if (cartModel == null || cartModel.getUser() == null || cartModel.getSite() == null
				|| !cartModel.getSite().isCustomerLocationEnabled())
		{
			return;
		}
		getModelService().refresh(cartModel);

		final ShipmentTypeInfo shipmentTypeInfoForUser = getShipmentTypeInfoForUser(cartModel.getUser());

		cartModel.setSelectedCity(shipmentTypeInfoForUser.getCity());

		getModelService().save(cartModel);
		getModelService().refresh(cartModel);
	}

	@Override
	public void afterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result)
			throws CommerceCartModificationException
	{
		if (result.getQuantityAdded() > 0)
		{

		}
	}

	private ShipmentTypeInfo getShipmentTypeInfoForUser(final UserModel user)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfo = getCustomUserService().getShipmentTypeInfo(user);
		return shipmentTypeInfo.isEmpty() ? new ShipmentTypeInfo(null, null, null) : shipmentTypeInfo.get();
	}


	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the customUserService
	 */
	protected CustomUserService getCustomUserService()
	{
		return customUserService;
	}


}
