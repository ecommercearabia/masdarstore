/**
 *
 */
package com.masdar.core.hook.order;

import de.hybris.platform.b2b.order.hooks.B2BApprovalBusinessProcessCreationPlaceOrderMethodHook;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * @author monzer
 *
 */
public class CustomB2BApprovalBusinessProcessCreationPlaceOrderMethodHook
		extends B2BApprovalBusinessProcessCreationPlaceOrderMethodHook
{

	@Override
	protected boolean isB2BContext(final AbstractOrderModel order)
	{
		if (order != null && order.getSite() != null)
		{
			return SiteChannel.B2B.equals(order.getSite().getChannel());
		}
		else
		{
			return false;
		}
	}

}
