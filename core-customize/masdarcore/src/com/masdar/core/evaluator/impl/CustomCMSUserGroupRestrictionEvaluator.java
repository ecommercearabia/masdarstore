/**
 *
 */
package com.masdar.core.evaluator.impl;

import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.impl.CMSUserGroupRestrictionEvaluator;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


public class CustomCMSUserGroupRestrictionEvaluator extends CMSUserGroupRestrictionEvaluator
{
	private static final Logger LOG = Logger.getLogger(CustomCMSUserGroupRestrictionEvaluator.class);
	private UserService userService;

	@Override
	public boolean evaluate(final CMSUserGroupRestrictionModel restrictionModel, final RestrictionData restrictionData)
	{

		return super.evaluate(restrictionModel, restrictionData) ? getEvaluateNotIncludeSubgroups(restrictionModel, restrictionData)
				: false;
	}

	/**
	 * @param restrictionModel
	 * @param restrictionData
	 * @return
	 */
	private boolean getEvaluateNotIncludeSubgroups(final CMSUserGroupRestrictionModel restrictionModel,
			final RestrictionData restrictionData)
	{
		final Collection<UserGroupModel> groups = restrictionModel.getUnappliedUserGroups();
		final UserModel currentUserModel = this.getUserService().getCurrentUser();
		final Set<PrincipalGroupModel> userGroups = new HashSet<>(currentUserModel.getGroups());
		if (restrictionModel.isIncludeSubgroups())
		{
			userGroups.addAll(this.getSubgroups(userGroups));
		}

		final Collection<UserGroupModel> unappliedUserGroups = restrictionModel.getUnappliedUserGroups();
		final List<String> restrGroupNames = new ArrayList<>();
		for (final UserGroupModel group : groups)
		{
			restrGroupNames.add(group.getUid());
		}
		final List<String> currentGroupNames = new ArrayList<>();
		for (final PrincipalGroupModel principalGroupModel : userGroups)
		{
			currentGroupNames.add(principalGroupModel.getUid());
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Current UserGroups: " + StringUtils.join(currentGroupNames, "; "));
			LOG.debug("Restricted UserGroups: " + StringUtils.join(restrGroupNames, "; "));
		}

		for (final String string : currentGroupNames)
		{
			if (restrGroupNames.contains(string))
			{
				return false;
			}
		}
		return true;
	}

	@Override
	protected List<PrincipalGroupModel> getSubgroups(final Collection<PrincipalGroupModel> groups)
	{
		final ArrayList<PrincipalGroupModel> ret = new ArrayList<PrincipalGroupModel>(groups);
		for (final PrincipalGroupModel principalGroup : groups)
		{
			ret.addAll(this.getSubgroups(principalGroup.getGroups()));
		}
		return ret;
	}

	@Override
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Override
	protected UserService getUserService()
	{
		return this.userService;
	}


}
