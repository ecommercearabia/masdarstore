/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.event;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;


/**
 * @author mohammed.baker@erabia.com
 *
 */
public class ConsignmentShippedEvent extends ConsignmentProcessingEvent
{
	private static final long serialVersionUID = -4143608934213920L;

	public ConsignmentShippedEvent(final ConsignmentProcessModel process)
	{
		super(process);
	}
}
