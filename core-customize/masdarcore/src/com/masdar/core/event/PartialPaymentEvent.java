/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class PartialPaymentEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = -4143608934534130L;

	public PartialPaymentEvent(final OrderProcessModel process)
	{
		super(process);
	}
}
