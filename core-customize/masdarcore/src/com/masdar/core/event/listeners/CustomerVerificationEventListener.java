/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.event.listeners;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.CustomerVerificationEmailProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;

import com.masdar.core.event.CustomerVerificationEvent;


/**
 * Listener for "forgotten password" functionality event.
 */
public class CustomerVerificationEventListener extends AbstractAcceleratorSiteEventListener<CustomerVerificationEvent>
{

	private ModelService modelService;
	private BusinessProcessService businessProcessService;


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final CustomerVerificationEvent event)
	{
		final CustomerVerificationEmailProcessModel customerVerification = (CustomerVerificationEmailProcessModel) getBusinessProcessService()
				.createProcess("customerVerification-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"customerVerificationEmailProcess");
		customerVerification.setSite(event.getSite());
		customerVerification.setCustomer(event.getCustomer());
		customerVerification.setToken(event.getToken());
		customerVerification.setLanguage(event.getLanguage());
		customerVerification.setCurrency(event.getCurrency());
		customerVerification.setStore(event.getBaseStore());
		getModelService().save(customerVerification);
		getBusinessProcessService().startProcess(customerVerification);
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final CustomerVerificationEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}
}
