/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.nationality.dao;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.NationalityModel;


/**
 * @author mnasro
 *
 *         The Interface NationalityDao.
 */
public interface NationalityDao
{
	/**
	 * Find.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<NationalityModel> find(String code);

	/**
	 * Find.
	 *
	 * @return the list
	 */
	public List<NationalityModel> findAll();

}
