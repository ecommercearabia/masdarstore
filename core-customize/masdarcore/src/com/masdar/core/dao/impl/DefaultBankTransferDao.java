/**
 *
 */
package com.masdar.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.CollectionUtils;

import com.masdar.core.dao.BankTransferDao;
import com.masdar.core.model.BankTransferModel;

/**
 * @author monzer
 *
 */
public class DefaultBankTransferDao extends DefaultGenericDao<BankTransferModel> implements BankTransferDao
{

	/**
	 * @param typecode
	 */
	public DefaultBankTransferDao()
	{
		super(BankTransferModel._TYPECODE);
	}

	@Override
	public Optional<BankTransferModel> getByCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "code must not be null");
		final Map<String, Object> params = new HashMap<>();
		params.put(BankTransferModel.CODE, code);
		final List<BankTransferModel> find = find(params);
		return find.stream().findFirst();
	}

	@Override
	public Optional<List<BankTransferModel>> findAll()
	{
		final List<BankTransferModel> find = find();
		return CollectionUtils.isEmpty(find) ? Optional.empty() : Optional.ofNullable(find);
	}

}
