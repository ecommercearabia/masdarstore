package com.masdar.core.dao.impl;

import de.hybris.platform.processengine.impl.DefaultBusinessProcessServiceDao;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;


public class CustomBusinessProcessServiceDao extends DefaultBusinessProcessServiceDao
{
	private FlexibleSearchService flexibleSearchService;

	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	public List<BusinessProcessModel> getProcessByStartWithCode(@Nonnull
	final String processName)
	{
		Objects.requireNonNull(processName, "processName is required");

		final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {pk} FROM {BusinessProcess} where {code} LIKE '"
				+ processName + "%' ORDER BY {" + BusinessProcessModel.MODIFIEDTIME + "} DESC");

		final SearchResult<BusinessProcessModel> result = this.flexibleSearchService.search(query);
		final List<BusinessProcessModel> result2 = result.getResult();
		return result.getResult();
	}
}
