/**
 *
 */
package com.masdar.core.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.impl.DefaultOrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.masdar.core.dao.CustomB2COrderDao;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomB2COrderDao extends DefaultOrderDao implements CustomB2COrderDao
{

	@Override
	public OrderModel findOrderByCode(final String code)
	{
		final Map<String, Object> attr = new HashMap<>(1);
		attr.put(OrderModel.CODE, code);
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT {o:pk} from { ").append(OrderModel._TYPECODE).append(" as o} WHERE {o:code} = ?code ")
				.append("AND {o:" + OrderModel.VERSIONID + "} IS NULL");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(sql.toString());
		query.getQueryParameters().putAll(attr);
		final SearchResult<OrderModel> result = this.getFlexibleSearchService().search(query);
		final List<OrderModel> orders = result.getResult();
		return orders.isEmpty() ? null : (OrderModel) orders.get(0);

	}

}
