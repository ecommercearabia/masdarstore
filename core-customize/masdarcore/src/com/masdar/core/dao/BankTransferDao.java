/**
 *
 */
package com.masdar.core.dao;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.BankTransferModel;


/**
 * @author monzer
 *
 */
public interface BankTransferDao
{
	public Optional<BankTransferModel> getByCode(String code);

	public Optional<List<BankTransferModel>> findAll();

}
