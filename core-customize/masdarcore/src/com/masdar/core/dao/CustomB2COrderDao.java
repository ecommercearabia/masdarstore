/**
 *
 */
package com.masdar.core.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.OrderDao;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomB2COrderDao extends OrderDao
{
	public OrderModel findOrderByCode(String code);
}
