/**
 *
 */
package com.masdar.core.order.dao.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.core.order.dao.CustomOrderDAO;


/**
 * @author mbaker
 *
 */
public class DefaultCustomOrderDAO implements CustomOrderDAO
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The base select query. */
	private static final String BASE_SELECT_QUERY = "SELECT {p:" + OrderModel.PK + "} " + "FROM {" + OrderModel._TYPECODE
			+ " AS p} ";

	/** The code key. */
	private static final String ORDER_STATUS_KEY = "status";

	private static final String ORDER_CODE_KEY = "code";

	@Override
	public List<AbstractOrderModel> findByStatus(final OrderStatus status)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.STATUS + "}=?" + ORDER_STATUS_KEY + " AND {p:"
				+ OrderModel.ORIGINALVERSION + "} IS NULL ";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(ORDER_STATUS_KEY, status);
		return flexibleSearchService.<AbstractOrderModel> search(query).getResult();

	}



	@Override
	public Optional<AbstractOrderModel> findByCodeAndStatus(final String code, final OrderStatus status)
	{
		final String queryString = BASE_SELECT_QUERY + "WHERE " + "{p:" + OrderModel.STATUS + "}=?" + ORDER_STATUS_KEY + " AND {p:"
				+ OrderModel.CODE + "}=?" + ORDER_CODE_KEY;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(ORDER_STATUS_KEY, status);
		query.addQueryParameter(ORDER_CODE_KEY, code);

		final List<AbstractOrderModel> result = flexibleSearchService.<AbstractOrderModel> search(query).getResult();
		return CollectionUtils.isEmpty(result) ? Optional.empty() : Optional.ofNullable(result.get(0));
	}


}
