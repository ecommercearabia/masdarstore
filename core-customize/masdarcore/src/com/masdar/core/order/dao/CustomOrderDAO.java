/**
 *
 */
package com.masdar.core.order.dao;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Optional;


/**
 * @author mbaker
 *
 */
public interface CustomOrderDAO
{
	public List<AbstractOrderModel> findByStatus(OrderStatus status);

	public Optional<AbstractOrderModel> findByCodeAndStatus(String code, OrderStatus status);

}
