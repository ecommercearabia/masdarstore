/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.masdar.core.order.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commerceservices.order.ProductConfigurationHandler;
import de.hybris.platform.commerceservices.service.data.ProductConfigurationItem;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguratorSettingModel;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguredProductInfoModel;
import de.hybris.platform.textfieldconfiguratortemplateservices.order.hook.TextFieldConfigurationHandler;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.masdar.core.model.ProductConfigurationOptionModel;
import com.masdar.core.model.SelectFieldConfiguratorSettingModel;
import com.masdar.facades.data.ProductConfigurationOptionData;


public class CustomTextFieldConfigurationHandler extends TextFieldConfigurationHandler implements ProductConfigurationHandler
{
	@Resource(name = "productConfigurationOptionReverseConverter")
	private Converter<ProductConfigurationOptionData, ProductConfigurationOptionModel> productConfigurationOptionReverseConverter;

	@Override
	public List<AbstractOrderEntryProductInfoModel> createProductInfo(final AbstractConfiguratorSettingModel productSettings)
	{
		if (productSettings instanceof SelectFieldConfiguratorSettingModel)
		{
			final SelectFieldConfiguratorSettingModel selectSetting = (SelectFieldConfiguratorSettingModel) productSettings;

			final TextFieldConfiguredProductInfoModel result = new TextFieldConfiguredProductInfoModel();
			result.setConfiguratorType(ConfiguratorType.TEXTFIELD);
			result.setConfigurationLabel(selectSetting.getTextFieldLabel());
			result.setConfigurationValue(selectSetting.getTextFieldDefaultValue());
			result.setOptions(selectSetting.getProductConfigurationOptions());
			validate(result);
			return Collections.singletonList(result);
		}
		else if (productSettings instanceof TextFieldConfiguratorSettingModel)
		{
			final TextFieldConfiguratorSettingModel textSetting = (TextFieldConfiguratorSettingModel) productSettings;

			final TextFieldConfiguredProductInfoModel result = new TextFieldConfiguredProductInfoModel();
			result.setConfiguratorType(ConfiguratorType.TEXTFIELD);
			result.setConfigurationLabel(textSetting.getTextFieldLabel());
			result.setConfigurationValue(textSetting.getTextFieldDefaultValue());
			validate(result);
			return Collections.singletonList(result);
		}
		else
		{
			throw new IllegalArgumentException(
					"Argument must be a type of TextFieldConfiguratorSettingsModel or SelectFieldConfiguratorSettingModel");
		}
	}

	@Override
	public List<AbstractOrderEntryProductInfoModel> convert(final Collection<ProductConfigurationItem> items,
			final AbstractOrderEntryModel entry)
	{
		validateParameterNotNullStandardMessage("items", items);
		return items.stream().peek(item -> validateParameterNotNull(item, "Items of the input collection must not be null"))
				.map(item -> {
					final TextFieldConfiguredProductInfoModel result = new TextFieldConfiguredProductInfoModel();
					result.setConfigurationLabel(item.getKey());
					if (item.getValue() != null)
					{
						result.setConfigurationValue(item.getValue().toString());
					}
					result.setConfiguratorType(ConfiguratorType.TEXTFIELD);
					if (!CollectionUtils.isEmpty(item.getOptions()))
					{
						result.setOptions(productConfigurationOptionReverseConverter.convertAll(item.getOptions()));
					}

					return result;
				}).peek(this::validate).collect(Collectors.toList());
	}
}
