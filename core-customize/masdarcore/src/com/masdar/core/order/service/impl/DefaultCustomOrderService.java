/**
 *
 */
package com.masdar.core.order.service.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.google.common.base.Preconditions;
import com.masdar.core.order.dao.CustomOrderDAO;
import com.masdar.core.order.service.CustomOrderService;


/**
 * @author mbaker
 *
 */
public class DefaultCustomOrderService implements CustomOrderService
{
	@Resource(name = "customOrderDAO")
	private CustomOrderDAO customOrderDAO;

	private static final String ORDER_STATUS_NULL_MSG = "status is null";

	@Override
	public List<AbstractOrderModel> getOrderByStatus(final OrderStatus status)
	{
		Preconditions.checkArgument(!Objects.isNull(status), ORDER_STATUS_NULL_MSG);
		return customOrderDAO.findByStatus(status);
	}

	@Override
	public Optional<AbstractOrderModel> getOrderByCodeAndOrderStatus(final String orderCode, final OrderStatus status)
	{
		Preconditions.checkArgument(!Objects.isNull(status), ORDER_STATUS_NULL_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(orderCode), "orderCode is null or empty");
		return customOrderDAO.findByCodeAndStatus(orderCode, status);
	}


}
