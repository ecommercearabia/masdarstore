/**
 *
 */
package com.masdar.core.order.service;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Optional;


/**
 * @author mbaker
 *
 */
public interface CustomOrderService
{
	public List<AbstractOrderModel> getOrderByStatus(final OrderStatus status);

	public Optional<AbstractOrderModel> getOrderByCodeAndOrderStatus(final String orderCode, final OrderStatus status);

}
