package com.masdar.core.order.cart.exception;

import com.masdar.core.order.cart.exception.enums.CartExceptionType;


/**
 * The Class CartValidationException.
 */
public class CartValidationException extends Exception
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The cart exception type. */
	private final CartExceptionType cartExceptionType;

	/** The error parm. */
	private final String[] errorParm;

	/**
	 * Instantiates a new cart validation exception.
	 *
	 * @param message
	 *           the message
	 * @param cartExceptionType
	 *           the cart exception type
	 * @param errorParm
	 *           the error parm
	 */
	public CartValidationException(final String message, final CartExceptionType cartExceptionType, final String[] errorParm)
	{
		super(message);
		this.cartExceptionType = cartExceptionType;
		this.errorParm = errorParm;
	}

	/**
	 * Instantiates a new cart validation exception.
	 *
	 * @param cartExceptionType
	 *           the cart exception type
	 * @param errorParm
	 *           the error parm
	 */
	public CartValidationException(final CartExceptionType cartExceptionType, final String[] errorParm)
	{
		super();
		this.cartExceptionType = cartExceptionType;
		this.errorParm = errorParm;
	}

	/**
	 * Gets the error parm.
	 *
	 * @return the errorParm
	 */
	public String[] getErrorParm()
	{
		return errorParm;
	}


	/**
	 * Gets the cart exception type.
	 *
	 * @return the cartExceptionType
	 */
	public CartExceptionType getCartExceptionType()
	{
		return cartExceptionType;
	}

}
