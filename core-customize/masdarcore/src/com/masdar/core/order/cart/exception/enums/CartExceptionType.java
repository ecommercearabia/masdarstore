package com.masdar.core.order.cart.exception.enums;

/**
 *
 */
public enum CartExceptionType
{
	MAX_QTY, MIN_QTY, MIN_AMOUNT, MAX_AMOUNT, MAX_WEIGHT, MIN_WEIGHT, STORE, SHIPPINGMETHOD;
}
