package com.masdar.core.order.cart.service;

import de.hybris.platform.core.model.order.CartModel;

import java.util.Optional;

import com.masdar.core.order.cart.exception.CartValidationException;


/**
 * The Interface CartValidationService.
 *
 * @author mnasro
 */
public interface CartValidationService
{

	/**
	 * Validate cart min amount.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	public boolean validateCartMinAmount(CartModel cartModel) throws CartValidationException;


	/**
	 * Validate cart min amount by current cart.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	public boolean validateCartMinAmountByCurrentCart() throws CartValidationException;

	/**
	 * Validate cart entries delivery/pickup.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            if the cart has delivery and pickup entries
	 */
	public boolean validateCartDeliveryType(CartModel cartModel) throws CartValidationException;

	/**
	 * Validate cart entries delivery/pickup.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            if the cart has delivery and pickup entries
	 */
	public boolean validateCartDeliveryTypeByCurrentCart() throws CartValidationException;

	/**
	 * Validate cart max amount.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	public boolean validateCartMaxAmount(CartModel cartModel) throws CartValidationException;

	/**
	 * Validate cart max amount by current cart.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	public boolean validateCartMaxAmountByCurrentCart() throws CartValidationException;

	public Optional<String> getCartMaxAmountLabel(CartModel cartModel);

	public Optional<String> getCartMinAmountLabel(CartModel cartModel);

	public Optional<String> getCartMaxAmountLabelByCurrentCart();

	public Optional<String> getCartMinAmountLabelByCurrentCart();

}
