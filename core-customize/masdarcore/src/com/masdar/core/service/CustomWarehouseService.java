/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;

import com.masdar.core.model.CityModel;


/**
 * @author core
 *
 */
public interface CustomWarehouseService
{
	Set<WarehouseModel> getActiveWarehousesForCityAndStore(final CityModel city, final BaseStoreModel store);
}
