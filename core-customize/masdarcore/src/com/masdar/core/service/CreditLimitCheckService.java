/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CreditLimitCheckService
{

	boolean isCreditAmountEnough(final AbstractOrderModel cart, final B2BCustomerModel customer,
			final B2BCostCenterModel costCenter);

}
