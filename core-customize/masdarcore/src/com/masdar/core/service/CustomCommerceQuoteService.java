/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.order.CommerceQuoteService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomCommerceQuoteService extends CommerceQuoteService
{
	void applyQuoteEntryDiscount(AbstractOrderModel abstractOrderModel, UserModel userModel, Double discountRate,
			DiscountType discountType, long entryNumber);

	void removeQuoteEntriesDiscount(AbstractOrderModel abstractOrderModel);
}
