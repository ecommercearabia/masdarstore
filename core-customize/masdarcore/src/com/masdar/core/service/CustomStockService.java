package com.masdar.core.service;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;


/**
 * @author monzer
 *
 */
public interface CustomStockService extends StockService
{

	void reserve(StockLevelModel stockLevel, int reserveAmount) throws InsufficientStockLevelException;

	void release(StockLevelModel stockLevel, int releaseAmount);

}
