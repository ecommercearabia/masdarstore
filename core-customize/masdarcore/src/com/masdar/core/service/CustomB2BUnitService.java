package com.masdar.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.core.model.user.UserModel;


/**
 * @author tuqa
 */
public interface CustomB2BUnitService<T extends CompanyModel, U extends UserModel> extends B2BUnitService<T, U>
{
	public void updateB2BUnitVatId(B2BUnitModel b2bUnitmodel, String vatId);
}
