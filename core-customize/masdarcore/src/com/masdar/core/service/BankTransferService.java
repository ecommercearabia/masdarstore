/**
 *
 */
package com.masdar.core.service;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.BankTransferModel;

/**
 * @author monzer
 *
 */
public interface BankTransferService
{
	public Optional<BankTransferModel> getByCode(String code);

	public Optional<List<BankTransferModel>> getAll();


}
