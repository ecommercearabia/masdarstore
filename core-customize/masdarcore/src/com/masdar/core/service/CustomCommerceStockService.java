/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;

import com.masdar.core.model.CityModel;


/**
 * @author mbaker
 *
 */
public interface CustomCommerceStockService extends CommerceStockService
{

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	Long getStockLevelForProductAndCity(ProductModel product, CityModel city);

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	StockLevelStatus getStockLevelStatusForProductAndCity(ProductModel product, CityModel city);

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	Long getStockLevelForProductAndCityByCurrentCustomer(ProductModel product);

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	StockLevelStatus getStockLevelStatusForProductAndCityByCurrentCustomer(ProductModel product);

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	Long getStockLevelForProductAndPOSByCurrentCustomer(ProductModel product);

	/**
	 * @param product
	 * @param city
	 * @return
	 */
	StockLevelStatus getStockLevelStatusForProductAndPOSByCurrentCustomer(ProductModel product);

}
