/**
 *
 */
package com.masdar.core.service;

import java.util.List;

import com.masdar.core.model.ProductColorModel;
import com.masdar.core.model.ProductSizeModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomProductConfigurationsService
{
	List<ProductColorModel> getProductColors();

	List<ProductSizeModel> getProductSizes();
}
