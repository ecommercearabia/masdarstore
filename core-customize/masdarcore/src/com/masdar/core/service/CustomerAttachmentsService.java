package com.masdar.core.service;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserModel;


/**
 * @author mnasro - mohammed.nasro@erabia.com
 *
 */
public interface CustomerAttachmentsService
{

	public MediaModel createAttachment(String name, String contentType, byte[] data, UserModel customer,
			String allowedUploadedFormats);

	public String getAllowedUploadedFormats();

	public boolean isFileExtensionValid(final String name, final String allowedUploadedFormats);
}
