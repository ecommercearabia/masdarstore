/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.commerceservices.order.OrderQuoteDiscountValuesAccessor;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;

import java.util.List;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomOrderQuoteDiscountValuesAccessor extends OrderQuoteDiscountValuesAccessor
{
	/**
	 * Returns a list entry specific discounts for the given entry.
	 *
	 * @param entry
	 *           the entry to return discount values for.
	 * @return a list of entry specific discount values
	 * @throws IllegalArgumentException
	 *            in the given order is null
	 */
	List<DiscountValue> getQuoteEntryDiscountValues(AbstractOrderEntryModel entry);

	void setQuoteDiscountValues(AbstractOrderEntryModel entry, List<DiscountValue> discountValues);

}
