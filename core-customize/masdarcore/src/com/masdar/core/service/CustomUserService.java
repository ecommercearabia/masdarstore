package com.masdar.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Optional;
import java.util.Set;

import com.masdar.core.beans.ShipmentTypeInfo;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomUserService
{
	public B2BUnitModel getRootB2BUnitHaveAccountManagerOrAccountManagerGroups(final PrincipalModel principal,
			final Set<PrincipalGroupModel> groups);

	public Optional<ShipmentTypeInfo> getShipmentTypeInfoForCurrentUser();

	public Optional<ShipmentTypeInfo> getShipmentTypeInfo(UserModel user);

	public void updateShipmentTypeInfoForCurrentUser(final ShipmentTypeInfo info);

	public void updateShipmentTypeInfo(final UserModel user, final ShipmentTypeInfo info);

}
