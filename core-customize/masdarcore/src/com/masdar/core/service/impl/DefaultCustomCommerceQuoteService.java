/**
 *
 */
package com.masdar.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceQuoteService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.google.common.math.DoubleMath;
import com.masdar.core.service.CustomCommerceQuoteService;
import com.masdar.core.service.CustomOrderQuoteDiscountValuesAccessor;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomCommerceQuoteService extends DefaultCommerceQuoteService implements CustomCommerceQuoteService
{
	private static final Logger LOG = Logger.getLogger(DefaultCommerceQuoteService.class);
	private static final double EPSILON = 0.0001d;

	@Resource(name = "orderQuoteDiscountValuesAccessor")
	private CustomOrderQuoteDiscountValuesAccessor customOrderQuoteDiscountValuesAccessor;


	/**
	 * @author husam.dababneh@erabia.com
	 *
	 */
	@Override
	public void applyQuoteEntryDiscount(final AbstractOrderModel abstractOrderModel, final UserModel userModel,
			final Double discountRate, final DiscountType discountType, final long entryNumber)
	{

		validateParameterNotNullStandardMessage("abstractOrderModel", abstractOrderModel);
		validateParameterNotNullStandardMessage("user", userModel);
		validateParameterNotNullStandardMessage("discountRate", discountRate);
		validateParameterNotNullStandardMessage("discountType", discountType);

		QuoteModel quoteModel = null;
		if (abstractOrderModel instanceof CartModel)
		{
			validateQuoteCart((CartModel) abstractOrderModel);
			quoteModel = ((CartModel) abstractOrderModel).getQuoteReference();
		}
		else if (abstractOrderModel instanceof QuoteModel)
		{
			quoteModel = (QuoteModel) abstractOrderModel;
		}
		else
		{
			throw new IllegalArgumentException(
					"The abstract order model is neither a quote model nor a cart model created from quote model.");
		}




		getQuoteActionValidationStrategy().validate(QuoteAction.DISCOUNT, quoteModel, userModel);

		final AbstractOrderEntryModel entry = getEntryByEntryNumber(abstractOrderModel, entryNumber); //
		if (entry == null)
		{
			throw new IllegalArgumentException("Cannot get the entry fron the quote.");
		}


		validateDiscountRateOnEntry(discountRate, discountType, entry);

		//		if (DiscountType.ENTRY.equals(discountType))
		//		{
		//			discountRate = entry.getBasePrice() - discountRate;
//		}

		boolean isCalculationRequired = CollectionUtils
				.isNotEmpty(getCustomOrderQuoteDiscountValuesAccessor().getQuoteDiscountValues(abstractOrderModel))
				|| CollectionUtils.isNotEmpty(getCustomOrderQuoteDiscountValuesAccessor().getQuoteEntryDiscountValues(entry));

		final List<DiscountValue> discountList = removeExistingQuoteEntryDiscount(entry);

		try
		{
			if (discountRate.doubleValue() > EPSILON)
			{
				isCalculationRequired = true;
				final DiscountValue discountValue = createDiscountValue(discountRate, discountType,
						abstractOrderModel.getCurrency().getIsocode(), entry.getQuantity().doubleValue())
								.orElseThrow(() -> new IllegalArgumentException("Discount type cannot be created or supported"));

				discountList.add(discountValue);
				entry.setDiscountValues(discountList);
				getCustomOrderQuoteDiscountValuesAccessor().setQuoteDiscountValues(entry, Collections.singletonList(discountValue)); // keep track of the quote discount
			}

			// calculate if existing ones have been removed and/or new ones have been added
			if (isCalculationRequired)
			{
				getCalculationService().calculateTotals(entry, true);
				getModelService().save(entry);
				getModelService().refresh(entry);
				getModelService().refresh(abstractOrderModel);
				getCalculationService().calculateTotals(abstractOrderModel, true);
				getModelService().save(abstractOrderModel);
			}
		}
		catch (final CalculationException e)
		{
			LOG.error("Failed to calculate cart [" + abstractOrderModel.getCode() + "]", e);
			throw new SystemException("Could not calculate cart [" + abstractOrderModel.getCode() + "] due to : " + e.getMessage(),
					e);
		}
	}


	/**
	 * @author husam.dababneh@erabia.com
	 * @param abstractOrderModel
	 * @param entryNumber
	 * @return AbstractOrderEntryModel that have the specific entryNumber
	 */
	private AbstractOrderEntryModel getEntryByEntryNumber(final AbstractOrderModel abstractOrderModel, final long entryNumber)
	{
		for (final AbstractOrderEntryModel aoe : abstractOrderModel.getEntries())
		{
			if (aoe.getEntryNumber() != null && aoe.getEntryNumber().longValue() == entryNumber)
			{
				return aoe;
			}
		}
		return null;
	}

	@Override
	public void removeQuoteEntriesDiscount(final AbstractOrderModel abstractOrderModel)
	{
		validateParameterNotNullStandardMessage("abstractOrderModel", abstractOrderModel);

		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			removeExistingQuoteEntryDiscount(entry);
		}

	}

	/**
	 * @author husam.dababneh@erabia.com
	 * @param abstractOrderModel
	 * @return removes any quote discount and return a list of all discounts (but quote discounts) applied on a specific
	 *         entry
	 */
	private List<DiscountValue> removeExistingQuoteEntryDiscount(final AbstractOrderEntryModel entry)
	{
		final List<DiscountValue> discountList = new ArrayList<>();
		for (final var a : entry.getDiscountValues())
		{
			if (!CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(a.getCode()))
			{
				discountList.add(a);
			}
		}
		entry.setDiscountValues(discountList);
		getCustomOrderQuoteDiscountValuesAccessor().setQuoteDiscountValues(entry, Collections.emptyList());

		return discountList;
	}



	protected void validateDiscountRateOnEntry(final Double discountRate, final DiscountType discountType,
			final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		if (DiscountType.ENTRY.equals(discountType))
		{

		}
		final double rate = discountRate.doubleValue();

		final double lowestPrice = abstractOrderEntryModel.getProduct() == null ? 0d
				: abstractOrderEntryModel.getProduct().getLowestPrice();

		if (DoubleMath.fuzzyCompare(rate, 0, EPSILON) < 0)
		{
			throw new IllegalArgumentException("The discount rate is less then 0!");
		}
		if (DiscountType.PERCENT.equals(discountType) && (DoubleMath.fuzzyCompare(rate, 100, EPSILON) > 0))
		{
			throw new IllegalArgumentException("Discount type is percent, but the discount rate is greater than 100!");
		}
		if (DiscountType.ABSOLUTE.equals(discountType)
				&& DoubleMath.fuzzyCompare(rate, abstractOrderEntryModel.getTotalPrice().doubleValue(), EPSILON) > 0)
		{
			throw new IllegalArgumentException(
					String.format("Discount type is absolute, but the discont rate is greater than cart total [%s]!",
							abstractOrderEntryModel.getTotalPrice()));
		}
		if (DiscountType.ENTRY.equals(discountType) && DoubleMath.fuzzyCompare(rate, lowestPrice, EPSILON) <= 0)
		{
			throw new IllegalArgumentException(
					String.format(
							"Discount type is per product base price, but the discont rate is lower than than the minimum price [%s]!",
							lowestPrice));
		}
	}

	protected Optional<DiscountValue> createDiscountValue(final Double discountRate, final DiscountType discountType,
			final String currencyIsoCode, final double entryQty)
	{
		DiscountValue discountValue = null;

		if (DiscountType.PERCENT.equals(discountType))
		{
			discountValue = DiscountValue.createRelative(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate);
		}
		else if (DiscountType.ABSOLUTE.equals(discountType))
		{
			discountValue = DiscountValue.createAbsolute(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate / entryQty,
					currencyIsoCode);
		}
		else if (DiscountType.TARGET.equals(discountType))
		{
			discountValue = DiscountValue.createTargetPrice(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate / entryQty,
					currencyIsoCode);
		}
		else if (DiscountType.ENTRY.equals(discountType))
		{
			discountValue = DiscountValue.createTargetPrice(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate,
					currencyIsoCode);
		}

		return (discountType == null || discountValue == null) ? Optional.empty() : Optional.of(discountValue);
	}

	/**
	 * @return the customOrderQuoteDiscountValuesAccessor
	 */
	public CustomOrderQuoteDiscountValuesAccessor getCustomOrderQuoteDiscountValuesAccessor()
	{
		return customOrderQuoteDiscountValuesAccessor;
	}


	/**
	 * @param customOrderQuoteDiscountValuesAccessor
	 *           the customOrderQuoteDiscountValuesAccessor to set
	 */
	public void setCustomOrderQuoteDiscountValuesAccessor(
			final CustomOrderQuoteDiscountValuesAccessor customOrderQuoteDiscountValuesAccessor)
	{
		this.customOrderQuoteDiscountValuesAccessor = customOrderQuoteDiscountValuesAccessor;
	}






}
