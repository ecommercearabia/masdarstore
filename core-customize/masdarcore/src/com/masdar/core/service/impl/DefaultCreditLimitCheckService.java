/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.b2b.dao.B2BOrderDao;
import de.hybris.platform.b2b.enums.B2BRateType;
import de.hybris.platform.b2b.enums.MerchantCheckStatus;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCreditCheckResultModel;
import de.hybris.platform.b2b.model.B2BCreditLimitModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCostCenterService;
import de.hybris.platform.b2b.services.B2BCurrencyConversionService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2b.util.B2BDateUtils;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.StandardDateRange;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;

import com.masdar.core.model.CreditExceededRecordModel;
import com.masdar.core.service.CreditLimitCheckService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCreditLimitCheckService implements CreditLimitCheckService
{
	private static final Logger LOG = Logger.getLogger(DefaultCreditLimitCheckService.class);

	private final MathContext MONEY_HALF_UP = new MathContext(16, RoundingMode.HALF_UP);
	private final BigDecimal ZERO = (new BigDecimal("0", MONEY_HALF_UP)).setScale(2);

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;
	@Resource(name = "b2bCurrencyConversionService")
	private B2BCurrencyConversionService b2bCurrencyConversionService;
	@Resource(name = "b2bOrderDao")
	private B2BOrderDao b2bOrderDao;
	@Resource(name = "b2bCostCenterService")
	private B2BCostCenterService b2bCostCenterService;
	@Resource(name = "b2bDateUtils")
	private B2BDateUtils b2bDateUtils;

	@Resource(name = "modelService")
	private ModelService modelService;


	public boolean isCreditAmountEnough(final AbstractOrderModel cart, final B2BCustomerModel customer,
			final B2BCostCenterModel costCenter)
	{
		if (cart == null || customer == null || costCenter == null || costCenter.getUnit() == null
				|| costCenter.getUnit().getCreditLimit() == null)
		{
			return false;
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Evaluating B2BCreditLimitResultModel for employee: " + customer.getUid());
		}
		final B2BUnitModel b2bUnit = costCenter.getUnit();

		final B2BCreditLimitModel creditLimit = b2bUnit.getCreditLimit();

		final B2BCreditCheckResultModel creditLimitResult = this.getModelService().create(B2BCreditCheckResultModel.class);
		creditLimitResult.setStatus(MerchantCheckStatus.OPEN);

		final List<AbstractOrderEntryModel> entries = cart.getEntries();


		final StandardDateRange creditLimitDateRange = this.getDateRangeForCreditLimit(creditLimit);
		BigDecimal totalCost = BigDecimal.valueOf(this.getTotalCost(costCenter, creditLimitDateRange).doubleValue());

		for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
		{
			totalCost = totalCost.add(getOrderEntryTotal(abstractOrderEntryModel));
		}

		final BigDecimal convertedCreditAmount = BigDecimal.valueOf(getB2bCurrencyConversionService()
				.convertAmount(Double.valueOf(creditLimit.getAmount().doubleValue()), creditLimit.getCurrency(), cart.getCurrency())
				.doubleValue());


		creditLimitResult.setStatus(
				convertedCreditAmount.compareTo(totalCost) > 0 ? MerchantCheckStatus.APPROVED : MerchantCheckStatus.REJECTED);
		creditLimitResult.setCreditLimit(creditLimit.getAmount());
		creditLimitResult.setAmountUtilised(creditLimit.getAmount().subtract(totalCost));
		creditLimitResult.setCurrency(creditLimit.getCurrency());

		if (LOG.isDebugEnabled())
		{
			LOG.debug(ReflectionToStringBuilder.toString(creditLimitResult, ToStringStyle.DEFAULT_STYLE));
		}

		if (!MerchantCheckStatus.APPROVED.equals(creditLimitResult.getStatus()))
		{
			final CreditExceededRecordModel failRecord = getModelService().create(CreditExceededRecordModel.class);
			failRecord.setCustomer(customer);
			failRecord.setDateTime(LocalDateTime.now());
			getModelService().save(failRecord);
			getModelService().refresh(failRecord);
			return false;
		}

		return true;
	}

	protected B2BCreditLimitModel getCreditLimitForCostCenter(final B2BCostCenterModel costCenter, final CurrencyModel currency)
	{
		final B2BUnitModel unit = getB2bUnitService().getUnitWithCreditLimit(costCenter.getUnit());
		return unit.getCreditLimit();
	}

	protected List<B2BCostCenterModel> getCostCentersForUnitWithCreditLimit(final B2BUnitModel unit, final CurrencyModel currency)
	{
		return getB2bCostCenterService().getCostCentersForUnitBranch(unit, currency);
	}


	protected BigDecimal getTotalOfEntriesWithCostCenter(final B2BCostCenterModel costCenter,
			final List<AbstractOrderEntryModel> entries)
	{
		BigDecimal total = BigDecimal.ZERO;
		for (final AbstractOrderEntryModel abstractOrderEntryModel : entries)
		{
			if (abstractOrderEntryModel.getCostCenter().equals(costCenter))
			{
				total = total.add(getOrderEntryTotal(abstractOrderEntryModel));
			}
		}
		return total;
	}


	protected BigDecimal getOrderEntryTotal(final AbstractOrderEntryModel entry)
	{
		return this.toMoney(entry.getTotalPrice().doubleValue()
				+ (entry.getOrder().getNet().booleanValue() ? getTotalTax(entry).doubleValue() : 0));
	}

	protected BigDecimal getTotalTax(final AbstractOrderEntryModel orderEntry)
	{
		BigDecimal totalTax = this.ZERO;
		for (final TaxValue taxValue : orderEntry.getTaxValues())
		{
			totalTax = totalTax.add(BigDecimal.valueOf(taxValue.getAppliedValue()), this.MONEY_HALF_UP);
		}
		return totalTax;
	}


	protected Double getTotalCost(final B2BCostCenterModel costCenter, final StandardDateRange standardDateRange)
	{
		double totalCost = 0;
		totalCost = totalCost + this.getB2bCostCenterService()
				.getTotalCost(costCenter, standardDateRange.getStart(), standardDateRange.getEnd()).doubleValue();
		return Double.valueOf(totalCost);
	}




	public Set<B2BCreditLimitModel> getTypesToEvaluate(final B2BCustomerModel customer, final AbstractOrderModel order)
	{

		final B2BUnitModel unitWithCreditLimit = getB2bUnitService().getUnitWithCreditLimit(order.getUnit());
		return Collections.singleton(unitWithCreditLimit.getCreditLimit());
	}

	/**
	 * To determine if an alert is to be sent to the B2BCustomer based on exceeding a limit and alertSentDate and update
	 * alertSentDate
	 *
	 * @param orderTotals
	 *           the total amount of all orders
	 * @param creditLimit
	 *           the limit type (currency or percentage) which also provides the limit amount
	 * @return true if the credit limit has been exceeded
	 */
	protected boolean shouldCreditLimitTriggerAlert(final BigDecimal orderTotals, final B2BCreditLimitModel creditLimit,
			final StandardDateRange creditLimitDateRange)
	{
		boolean sendAlert = false;
		final boolean alertSent = alertSent(creditLimit, creditLimitDateRange);

		if (!alertSent)
		{
			if (B2BRateType.CURRENCY.equals(creditLimit.getAlertRateType()))
			{
				if (creditLimit.getAlertThreshold().compareTo(orderTotals) <= 0)
				{
					sendAlert = true;
				}
			}
			else if (B2BRateType.PERCENTAGE.equals(creditLimit.getAlertRateType()))
			{
				final BigDecimal thresholdValue = creditLimit.getAmount().multiply(creditLimit.getAlertThreshold())
						.divide(BigDecimal.valueOf(100), MathContext.DECIMAL128);

				if (thresholdValue.compareTo(orderTotals) <= 0)
				{
					sendAlert = true;
				}
			}

			if (sendAlert)
			{
				creditLimit.setAlertSentDate(new Date());
			}
			else
			{
				creditLimit.setAlertSentDate(null);
				LOG.info(String.format(
						"CreditLimit alert not triggered! Alert Type: %s Alert threshold is: %s, order total is: %s, credit limit amount %s",
						creditLimit.getAlertRateType(), creditLimit.getAlertThreshold(), orderTotals, creditLimit.getAmount()));
			}

			getModelService().save(creditLimit);

		}
		return sendAlert;
	}

	/**
	 * To determine if credit limit alert is already sent for current date range period
	 *
	 * @param creditLimit
	 *           Current credit limit to work on
	 * @param creditLimitDateRange
	 *           Date range for current credit limit
	 * @return true if the credit limit alert check should be avoided
	 */
	protected boolean alertSent(final B2BCreditLimitModel creditLimit, final StandardDateRange creditLimitDateRange)
	{
		boolean alertSent = false;

		if (creditLimit.getAlertSentDate() != null && creditLimitDateRange.getStart().before(creditLimit.getAlertSentDate()))
		{
			alertSent = true;
		}
		return alertSent;
	}

	protected StandardDateRange getDateRangeForCreditLimit(final B2BCreditLimitModel creditLimit)
	{

		if (creditLimit.getDateRange() != null)
		{
			return getB2bDateUtils().createDateRange(creditLimit.getDateRange());
		}
		else
		{
			return creditLimit.getDatePeriod();
		}
	}


	protected BigDecimal toMoney(final Double orderTotal)
	{
		return BigDecimal.valueOf(orderTotal.doubleValue()).round(new MathContext(16, RoundingMode.HALF_UP)).setScale(2,
				RoundingMode.HALF_UP);
	}


	/**
	 * @return the mONEY_HALF_UP
	 */
	public MathContext getMONEY_HALF_UP()
	{
		return MONEY_HALF_UP;
	}

	/**
	 * @return the zERO
	 */
	public BigDecimal getZERO()
	{
		return ZERO;
	}

	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * @return the b2bCurrencyConversionService
	 */
	public B2BCurrencyConversionService getB2bCurrencyConversionService()
	{
		return b2bCurrencyConversionService;
	}

	/**
	 * @return the b2bOrderDao
	 */
	public B2BOrderDao getB2bOrderDao()
	{
		return b2bOrderDao;
	}

	/**
	 * @return the b2bCostCenterService
	 */
	public B2BCostCenterService getB2bCostCenterService()
	{
		return b2bCostCenterService;
	}

	/**
	 * @return the b2bDateUtils
	 */
	public B2BDateUtils getB2bDateUtils()
	{
		return b2bDateUtils;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}



}
