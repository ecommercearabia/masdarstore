/**
 *
 */
package com.masdar.core.service.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.dao.BankTransferDao;
import com.masdar.core.model.BankTransferModel;
import com.masdar.core.service.BankTransferService;

/**
 * @author monzer
 *
 */
public class DefaultBankTransferService implements BankTransferService
{
	@Resource(name = "bankTransferDao")
	private BankTransferDao bankTransferDao;

	/**
	 * @return the bankTransferDao
	 */
	protected BankTransferDao getBankTransferDao()
	{
		return bankTransferDao;
	}
	@Override
	public Optional<BankTransferModel> getByCode(final String code)
	{
		return getBankTransferDao().getByCode(code);
	}

	@Override
	public Optional<List<BankTransferModel>> getAll()
	{
		return bankTransferDao.findAll();
	}



}
