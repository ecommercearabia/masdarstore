package com.masdar.core.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.enums.ShipmentType;
import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomUserService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomUserService implements CustomUserService
{

	private static final String SELECTED_SHIPMENT_TYPE = "selectedShipmentType";
	private static final String SELECTED_DELIVERY_POINT_OF_SERVICE = "selectedDeliveryPointOfService";
	private static final String SELECTED_SHIPMENT_CITY = "selectedShipmentCity";

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public B2BUnitModel getRootB2BUnitHaveAccountManagerOrAccountManagerGroups(final PrincipalModel principal,
			final Set<PrincipalGroupModel> groups)
	{
		B2BUnitModel unit = null;

		if (principal instanceof B2BUnitModel)
		{
			final B2BUnitModel unitF = (B2BUnitModel) principal;
			if (unitF.getAccountManager() != null
					|| (unitF.getAccountManagerGroups() != null && !unitF.getAccountManagerGroups().isEmpty()))
			{
				return unitF;
			}
		}

		if (groups != null && !groups.isEmpty())
		{
			for (final PrincipalGroupModel group : groups)
			{
				if (group instanceof B2BUnitModel)
				{
					unit = (B2BUnitModel) group;
					if (unit.getAccountManager() != null
							|| (unit.getAccountManagerGroups() != null && !unit.getAccountManagerGroups().isEmpty()))
					{
						return unit;
					}
					else
					{
						return getRootB2BUnitHaveAccountManagerOrAccountManagerGroups(unit, unit.getGroups());
					}
				}
			}
		}

		return unit;
	}

	@Override
	public Optional<ShipmentTypeInfo> getShipmentTypeInfoForCurrentUser()
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		return getShipmentTypeInfo(currentUser);
	}

	public Optional<ShipmentTypeInfo> getShipmentTypeInfo(final UserModel user)
	{
		if (user == null)
		{
			return Optional.empty();
		}
		CityModel city = null;
		PointOfServiceModel pointOfService = null;
		ShipmentType shipmentType = null;
		CartModel cart = null;
		if (getCartService().hasSessionCart())
		{
			cart = cartService.getSessionCart();
		}
		if (getUserService().isAnonymousUser(user))
		{
			city = cart == null || cart.getSelectedCity() == null
					? getSessionService().<CityModel> getAttribute(SELECTED_SHIPMENT_CITY)
					: cart.getSelectedCity();
			pointOfService = cart == null || cart.getSelectedDeliveryPointOfService() == null
					? getSessionService().<PointOfServiceModel> getAttribute(SELECTED_DELIVERY_POINT_OF_SERVICE)
					: cart.getSelectedDeliveryPointOfService();
			shipmentType = cart == null || cart.getShipmentType() == null
					? getSessionService().<ShipmentType> getAttribute(SELECTED_SHIPMENT_TYPE)
					: cart.getShipmentType();
		}
		else
		{
			city = user.getSelectedCity();
			pointOfService = user.getSelectedDeliveryPointOfService();
			shipmentType = user.getSelectedShipmentType();
		}
		// fetch the shipment type info from site
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		ShipmentTypeInfo info = null;
		if (city == null && pointOfService == null && shipmentType == null && currentSite != null)
		{
			info = fetchShipmentInfoFromSite(currentSite);
		}
		else
		{
			info = new ShipmentTypeInfo(shipmentType, city, pointOfService);
		}
		return Optional.ofNullable(info);
	}

	protected ShipmentTypeInfo fetchShipmentInfoFromSite(final CMSSiteModel currentSite)
	{
		return new ShipmentTypeInfo(
				currentSite.getDefaultShipmentType() == null ? ShipmentType.DELIVERY : currentSite.getDefaultShipmentType(),
				currentSite.getDefaultDeliveryLocationCity(), currentSite.getDefaultDeliveryPointOfService());
	}

	@Override
	public void updateShipmentTypeInfoForCurrentUser(final ShipmentTypeInfo info)
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		updateShipmentTypeInfo(currentUser, info);
	}

	@Override
	public void updateShipmentTypeInfo(final UserModel user, final ShipmentTypeInfo info)
	{
		if (user == null)
		{
			return;
		}
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (getUserService().isAnonymousUser(user))
		{
			if (info.getCity() != null)
			{
				getSessionService().setAttribute(SELECTED_SHIPMENT_CITY, info.getCity());
			}
			else if (currentSite != null)
			{
				getSessionService().setAttribute(SELECTED_SHIPMENT_CITY, currentSite.getDefaultDeliveryLocationCity());
			}

			if (info.getPointOfService() != null)
			{
				getSessionService().setAttribute(SELECTED_DELIVERY_POINT_OF_SERVICE, info.getPointOfService());
			}
			else if (currentSite != null)
			{
				getSessionService().setAttribute(SELECTED_DELIVERY_POINT_OF_SERVICE, currentSite.getDefaultDeliveryPointOfService());
			}

			if (info.getShipmentType() != null)
			{
				getSessionService().setAttribute(SELECTED_SHIPMENT_TYPE, info.getShipmentType());
			}
		}
		else
		{
			if (info.getCity() != null)
			{
				user.setSelectedCity(info.getCity());
			}
			if (info.getPointOfService() != null)
			{
				user.setSelectedDeliveryPointOfService(info.getPointOfService());
			}

			user.setSelectedShipmentType(info.getShipmentType());
			getModelService().save(user);
		}
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}
}
