/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.commerceservices.order.CommerceQuoteService;
import de.hybris.platform.commerceservices.order.strategies.QuoteUserIdentificationStrategy;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.warehousing.process.AbstractWarehousingBusinessProcessService;
import de.hybris.platform.warehousing.process.BusinessProcessException;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.dao.impl.CustomBusinessProcessServiceDao;


/**
 * @author mnasro
 *
 */
public class DefaultQuoteProcessService extends AbstractWarehousingBusinessProcessService<QuoteModel>
{
	private static Logger LOGGER = LoggerFactory.getLogger(DefaultQuoteProcessService.class);

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "commerceQuoteService")
	private CommerceQuoteService commerceQuoteService;

	@Resource(name = "quoteUserIdentificationStrategy")
	private QuoteUserIdentificationStrategy quoteUserIdentificationStrategy;

	@Resource(name = "customBusinessProcessServiceDao")
	private CustomBusinessProcessServiceDao customBusinessProcessServiceDao;

	/**
	 * @return the customBusinessProcessServiceDao
	 */
	public CustomBusinessProcessServiceDao getCustomBusinessProcessServiceDao()
	{
		return customBusinessProcessServiceDao;
	}

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the quoteUserIdentificationStrategy
	 */
	protected QuoteUserIdentificationStrategy getQuoteUserIdentificationStrategy()
	{
		return quoteUserIdentificationStrategy;
	}

	/**
	 * @return the commerceQuoteService
	 */
	protected CommerceQuoteService getCommerceQuoteService()
	{
		return commerceQuoteService;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Override
	public String getProcessCode(final QuoteModel quoteModel)
	{
		final BusinessProcessModel quoteProcess = getQuoteProcess(quoteModel);
		return quoteProcess.getCode();
	}


	public BusinessProcessModel getQuoteProcess(final QuoteModel quoteModel)
	{
		String expectedCode = "quoteBuyerProcess-" + String.valueOf(quoteModel.getCode());

		List<BusinessProcessModel> processByStartWithCode = getCustomBusinessProcessServiceDao()
				.getProcessByStartWithCode(expectedCode);

		if (CollectionUtils.isEmpty(processByStartWithCode))
		{
			expectedCode = "quoteSalesRepProcess-" + String.valueOf(quoteModel.getCode());
			processByStartWithCode = getCustomBusinessProcessServiceDao().getProcessByStartWithCode(expectedCode);
		}

		if (CollectionUtils.isEmpty(processByStartWithCode))
		{

			LOGGER.error("Unable to process event for quote [" + quoteModel.getCode() + "].[" + expectedCode
					+ "] No processes associated to the quote.");
			throw new BusinessProcessException(
					"Unable to process event for quote [" + quoteModel.getCode() + "]. No processes associated to the quote.");
		}
		//		return processByStartWithCode.stream().filter(process -> process.getCode().startsWith(expectedCode)).findFirst()
		//				.orElseThrow(
		//						() -> new BusinessProcessException("No business process found for quote [" + quoteModel.getCode() + "]."));
		return processByStartWithCode.get(0);
	}

	public BusinessProcessModel getQuoteSalesRepProcess(final QuoteModel quoteModel)
	{
		final String expectedCode = "quoteSalesRepProcess-" + String.valueOf(quoteModel.getCode());


		final List<BusinessProcessModel> processByStartWithCode = getCustomBusinessProcessServiceDao()
				.getProcessByStartWithCode(expectedCode);

		if (CollectionUtils.isEmpty(processByStartWithCode))
		{
			LOGGER.error("Unable to process event for quote [" + quoteModel.getCode() + "].[" + expectedCode
					+ "] No processes associated to the quote.");
			throw new BusinessProcessException(
					"Unable to process event for quote [" + quoteModel.getCode() + "]. No processes associated to the quote.");
		}
		//		return processByStartWithCode.stream().filter(process -> process.getCode().startsWith(expectedCode)).findFirst()
		//				.orElseThrow(
		//						() -> new BusinessProcessException("No business process found for quote [" + quoteModel.getCode() + "]."));
		return processByStartWithCode.get(0);
	}

	public QuoteModel getQuoteModelForCode(final String quoteCode)
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		return getCommerceQuoteService().getQuoteByCodeAndCustomerAndStore(currentUser,
				getQuoteUserIdentificationStrategy().getCurrentQuoteUser(), currentBaseStore, quoteCode);
	}
}
