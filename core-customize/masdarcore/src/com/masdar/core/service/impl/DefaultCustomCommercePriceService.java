/**
 *
 */
package com.masdar.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.masdar.core.service.CustomCommercePriceService;
import com.masdar.core.service.CustomPriceService;


/**
 * @author monzer
 *
 */
public class DefaultCustomCommercePriceService extends DefaultCommercePriceService implements CustomCommercePriceService
{

	@Resource(name = "customPriceService")
	private CustomPriceService customPriceService;

	@Override
	public PriceInformation getWebPriceForProduct(final ProductModel product)
	{
		validateParameterNotNull(product, "Product model cannot be null");
		final List<PriceInformation> prices = getCustomPriceService().getPriceInformationsForProductWithTax(product);
		if (CollectionUtils.isNotEmpty(prices))
		{
			PriceInformation minPriceForLowestQuantity = null;
			for (final PriceInformation price : prices)
			{
				if (minPriceForLowestQuantity == null || (((Long) minPriceForLowestQuantity.getQualifierValue("minqtd"))
						.longValue() > ((Long) price.getQualifierValue("minqtd")).longValue()))
				{
					minPriceForLowestQuantity = price;
				}
			}
			return minPriceForLowestQuantity;
		}
		return null;
	}

	@Override
	public PriceInformation getWebPriceForProductWithTax(final ProductModel productModel)
	{
		return this.getWebPriceForProduct(productModel);
	}

	@Override
	public PriceInformation getWebPriceForProductWithTax(final ProductModel productModel, final boolean withTax)
	{
		if (withTax)
		{
			return this.getWebPriceForProductWithTax(productModel);
		}
		else
		{
			return super.getWebPriceForProduct(productModel);
		}
	}

	//	/**
	//	 * @return the priceService
	//	 */
	//	@Override
	//	public CustomPriceService getPriceService()
	//	{
	//		return priceService;
	//	}

	public CustomPriceService getCustomPriceService()
	{
		return customPriceService;
	}
	//	/**
	//	 * @param priceService
	//	 *           the priceService to set
	//	 */
	//	public void setPriceService(final CustomPriceService priceService)
	//	{
	//		this.priceService = priceService;
	//	}


}
