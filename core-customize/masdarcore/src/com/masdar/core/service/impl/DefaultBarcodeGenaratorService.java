/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.masdar.core.qrbarcode.QRBarcodeEncoder;
import com.masdar.core.qrbarcode.tag.InvoiceDate;
import com.masdar.core.qrbarcode.tag.InvoiceTaxAmount;
import com.masdar.core.qrbarcode.tag.InvoiceTotalAmount;
import com.masdar.core.qrbarcode.tag.Seller;
import com.masdar.core.qrbarcode.tag.TaxNumber;
import com.masdar.core.service.BarcodeGenaratorService;
import com.onbarcode.barcode.Code128;



@Service
public class DefaultBarcodeGenaratorService implements BarcodeGenaratorService
{
	private static final String IMAGE_X_PNG = "image/x-png";

	private static final Logger LOG = Logger.getLogger(DefaultBarcodeGenaratorService.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "consignmentConverter")
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

	@Override
	public ByteArrayOutputStream genarateCode128Barcode(final String value) throws IOException
	{
		if (value == null || value.isEmpty())
		{
			throw new IllegalArgumentException("The value can not be null");
		}

		final Code128 barcode = new Code128();

		// Set barcode data text to encode
		barcode.setData(value);
		barcode.setLeftMargin(7f);
		barcode.setRightMargin(7f);
		barcode.setBottomMargin(7f);
		barcode.setTopMargin(7);
		barcode.setBarcodeWidth(100f);
		barcode.setBarcodeHeight(60f);
		barcode.setAutoResize(true);

		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		try
		{
			barcode.drawBarcode(out);
		}
		catch (final Exception e)
		{
			throw new IOException(e.getMessage());
		}

		return out;
	}

	@Override
	public MediaModel generateBarcodeAsMedia(final ConsignmentModel consignment) throws IOException
	{
		if (consignment == null)
		{
			throw new IllegalArgumentException("The consignment can not be null");
		}
		ByteArrayOutputStream byteArray = null;
		try
		{
			byteArray = genarateCode128Barcode(consignment.getTrackingID());
		}
		catch (final IllegalArgumentException e)
		{
			byteArray = genarateCode128Barcode(" ");
		}

		final MediaModel media = getModelService().create(MediaModel.class);
		final String mediaName = consignment.getTrackingID() + "-" + LocalDateTime.now().toString();
		media.setCode(mediaName);
		media.setMime(IMAGE_X_PNG);
		media.setRealFileName(mediaName);
		final AbstractOrderModel order = consignment.getOrder();
		getModelService().refresh(order);

		try
		{
			media.setCatalogVersion(catalogVersionService.getCatalogVersion("Default", "Online"));
			getModelService().save(media);
			getModelService().refresh(media);
			getModelService().refresh(consignment);
			//consignment.setBarcode(media);
			getModelService().save(consignment);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();

		try (InputStream dataStream = new ByteArrayInputStream(byteArray.toByteArray());)
		{
			mediaService.setStreamForMedia(media, dataStream, mediaName, IMAGE_X_PNG, mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate barcode from byteArray", e);
		}

		return media;
	}


	/**
	 * Gets the {@link MediaFolderModel} to save the generated Media
	 *
	 * @return the {@link MediaFolderModel}
	 */
	protected MediaFolderModel getDocumentMediaFolder()
	{
		return mediaService.getFolder("documents");
	}

	@Override
	public ByteArrayOutputStream generateQRCode(final String value) throws IOException
	{
		if (value == null || value.isEmpty())
		{
			throw new IllegalArgumentException("The value can not be null");
		}

		final MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
		final Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
		hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		BitMatrix bitMatrix = null;
		try
		{
			bitMatrix = multiFormatWriter.encode(value, BarcodeFormat.QR_CODE, 500, 500, hintMap);
		}
		catch (final WriterException e)
		{
			e.printStackTrace();
		}


		final BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", byteArrayOutputStream);
		return byteArrayOutputStream;
	}

	@Override
	public MediaModel generateQRCodeAsMedia(final ConsignmentModel consignmentModel) throws IOException
	{
		Preconditions.checkArgument(consignmentModel != null, "The consignment can not be null");

		Preconditions.checkArgument(consignmentModel.getOrder() != null, "The order can not be null");

		final OrderModel orderModel = (OrderModel) consignmentModel.getOrder();
		Preconditions.checkArgument(consignmentModel.getInvoiceDate() != null,
				"The consignment invoice date is not generated yet!");

		final BaseStoreModel store = consignmentModel.getOrder().getStore();
		Preconditions.checkArgument(store != null, "The Order store can not be null");
		final ConsignmentData consignment = consignmentConverter.convert(consignmentModel);
		final boolean isNull = consignment == null;
		final String sellerName = orderModel.getStore().getUid();
		final String timestamp = isNull ? "" : getFormattedDate(consignment.getInvoiceDate());
		final String vatNumber = store.getSupplierVatId() == null ? "" : store.getSupplierVatId();
		final String totalAmount = getTotalAmount(consignment, orderModel);
		final String totalTax = isNull ? "" : String.valueOf(consignment.getTotalTaxPriceValue());

		final String data = QRBarcodeEncoder.encode(new Seller(sellerName), new TaxNumber(vatNumber), new InvoiceDate(timestamp),
				new InvoiceTotalAmount(totalAmount), new InvoiceTaxAmount(totalTax));

		ByteArrayOutputStream byteArray = null;
		try
		{
			byteArray = this.generateQRCode(data);
		}
		catch (final IllegalArgumentException e)
		{
			byteArray = this.generateQRCode(" ");
		}

		final MediaModel media = getModelService().create(MediaModel.class);
		final String mediaName = "InvoiceQrCode-cons-" + consignmentModel.getInvoiceNumber() + "-" + LocalDateTime.now().toString();
		media.setCode(mediaName);
		media.setMime(IMAGE_X_PNG);
		media.setRealFileName(mediaName);

		try
		{
			media.setCatalogVersion(catalogVersionService.getCatalogVersion("Default", "Online"));
			getModelService().save(media);
			getModelService().refresh(media);
			getModelService().refresh(consignmentModel);
			consignmentModel.setInvoiceQRCode(media);
			getModelService().save(consignmentModel);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();
		try (InputStream dataStream = new ByteArrayInputStream(byteArray.toByteArray());)
		{
			mediaService.setStreamForMedia(media, dataStream, mediaName, IMAGE_X_PNG, mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate barcode from byteArray", e);
		}

		return media;
	}

	/**
	 * @param consignment
	 * @return
	 */
	private String getTotalAmount(final ConsignmentData consignment, final AbstractOrderModel abstractOrderModel)
	{
		if (consignment == null)
		{
			return "";
		}
		double totalPriceWithTaxValue = consignment.getTotalPriceWithTaxValue();

		if (abstractOrderModel != null && abstractOrderModel.getDeliveryCost() != null
				&& !abstractOrderModel.isDeliveryCostAppliedOnQR())
		{
			totalPriceWithTaxValue += abstractOrderModel.getDeliveryCost().doubleValue();
			abstractOrderModel.setDeliveryCostAppliedOnQR(true);
			abstractOrderModel.setDeliveryCostAppliedOnConsignment(consignment.getCode());
			getModelService().save(abstractOrderModel);
			getModelService().refresh(abstractOrderModel);
		}

		return String.valueOf(totalPriceWithTaxValue);
	}

	private String getFormattedDate(final Date date)
	{
		return date == null ? ""
				: date.toInstant().atZone(ZoneId.of("Asia/Riyadh")).toLocalDateTime()
						.format(DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy HH:mm:ss"));
	}

	private ModelService getModelService()
	{
		return modelService;
	}

}
