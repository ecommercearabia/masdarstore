/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.commerceservices.model.user.StoreEmployeeGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.impl.DefaultPointOfServiceService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.masdar.core.service.CustomPointOfServiceService;


/**
 * @author Husam Dababneh (husam.dababneh@erabia.com)
 *
 */
public class DefaultCustomPointOfServiceService extends DefaultPointOfServiceService implements CustomPointOfServiceService
{

	@Override
	public List<PointOfServiceModel> getPointOfServicesForRegion(final String countryIsoCode, final String regionIsoCode,
			final BaseStoreModel baseStore)
	{
		Preconditions.checkArgument(!StringUtils.isBlank(countryIsoCode), "countryIsoCode is empty");
		Preconditions.checkArgument(!StringUtils.isBlank(regionIsoCode), "regionIsoCode is empty");
		Preconditions.checkArgument(baseStore != null, baseStore);

		return this.getPointOfServiceDao().getPosForRegion(countryIsoCode, regionIsoCode, baseStore);
	}

	@Override
	public Set<EmployeeModel> getEmployeesForPointOfService(final PointOfServiceModel model)
	{
		Preconditions.checkNotNull(model, "PointOfServiceModel is null");

		final Set<StoreEmployeeGroupModel> storeEmployeeGroups = model.getStoreEmployeeGroups();
		if (storeEmployeeGroups == null || storeEmployeeGroups.size() == 0)
		{
			return null;
		}

		final Set<EmployeeModel> employees = new HashSet<EmployeeModel>();

		for (final StoreEmployeeGroupModel group : storeEmployeeGroups)
		{
			if (group.getMembers() == null || group.getMembers().size() == 0)
			{
				continue;
			}

			for (final PrincipalModel member : group.getMembers())
			{
				if (member instanceof EmployeeModel)
				{
					employees.add((EmployeeModel) member);
				}
			}
		}

		return employees;
	}


}
