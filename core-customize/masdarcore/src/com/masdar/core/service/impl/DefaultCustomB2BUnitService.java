/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BUnitService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.masdar.core.service.CustomB2BUnitService;


/**
 * @author tuqa-pc
 *
 */
public class DefaultCustomB2BUnitService extends DefaultB2BUnitService
		implements CustomB2BUnitService<B2BUnitModel, B2BCustomerModel>
{
	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @return the modelService
	 */
	@Override
	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public void updateB2BUnitVatId(final B2BUnitModel b2bUnitmodel, final String vatId)
	{

		if (StringUtils.isNotBlank(vatId))
		{
			b2bUnitmodel.setVatID(vatId);
		}
		else
		{
			b2bUnitmodel.setVatID(null);
		}
		getModelService().save(b2bUnitmodel);

	}
}
