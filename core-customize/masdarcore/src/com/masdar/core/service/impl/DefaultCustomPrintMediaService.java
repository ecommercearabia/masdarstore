/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.model.process.QuoteProcessModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.warehousing.labels.service.impl.DefaultPrintMediaService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.service.CustomPrintMediaService;


/**
 * @author mnasro
 *
 */
public class DefaultCustomPrintMediaService extends DefaultPrintMediaService implements CustomPrintMediaService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomPrintMediaService.class);

	@Resource(name = "quoteService")
	private QuoteService quoteService;


	/**
	 * @return the quoteService
	 */
	protected QuoteService getQuoteService()
	{
		return quoteService;
	}

	@Override
	public MediaModel getMediaForTemplate(final String frontendTemplateName, final BusinessProcessModel businessProcessModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("frontendTemplateName", frontendTemplateName);
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcessModel", businessProcessModel);
		LOG.info("Generating media for template: [{}] and item associated with business process: [{}]", frontendTemplateName,
				businessProcessModel.getClass().getSimpleName());
		final AbstractOrderModel order = this.getAbstractOrder(businessProcessModel);
		final ImpersonationContext context = new ImpersonationContext();
		context.setOrder(order);
		context.setSite(order.getSite());
		context.setUser(order.getUser());
		context.setCatalogVersions(Collections.emptyList());
		return this.getImpersonationService().executeInContext(context,
				() -> this.getDocumentGenerationService().generate(frontendTemplateName, businessProcessModel));
	}

	protected AbstractOrderModel getAbstractOrder(final BusinessProcessModel businessProcessModel)
	{
		BusinessProcessParameterModel param;
		List consignmentList;
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcessModel", businessProcessModel);
		if (businessProcessModel instanceof OrderProcessModel)
		{
			return ((OrderProcessModel) businessProcessModel).getOrder();
		}
		if (businessProcessModel instanceof ConsignmentProcessModel)
		{
			return ((ConsignmentProcessModel) businessProcessModel).getConsignment().getOrder();
		}
		if (businessProcessModel instanceof ReturnProcessModel)
		{
			return ((ReturnProcessModel) businessProcessModel).getReturnRequest().getOrder();
		}
		if (businessProcessModel instanceof QuoteProcessModel)
		{
			return getQuote(((QuoteProcessModel) businessProcessModel));
		}

		if (businessProcessModel.getContextParameters().iterator().hasNext() && CollectionUtils.isNotEmpty(
				consignmentList = (List) (param = businessProcessModel.getContextParameters().iterator().next()).getValue()))
		{
			return ((ConsignmentModel) consignmentList.iterator().next()).getOrder();
		}
		LOG.info("Unsupported BusinessProcess type [{}] for item [{}]", businessProcessModel.getClass().getSimpleName(),
				businessProcessModel);
		return null;
	}

	protected QuoteModel getQuote(final QuoteProcessModel quoteProcessModel)
	{
		return Optional.of(quoteProcessModel).map(QuoteProcessModel::getQuoteCode).map(getQuoteService()::getCurrentQuoteForCode)
				.orElseThrow();
	}
}
