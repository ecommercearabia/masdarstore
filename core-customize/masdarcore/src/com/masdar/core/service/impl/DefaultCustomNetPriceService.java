/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.commerceservices.price.impl.NetPriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.List;

import com.masdar.core.service.CustomPriceService;


/**
 * @author monzer
 *
 */
public class DefaultCustomNetPriceService extends NetPriceService implements CustomPriceService
{

	private TimeService timeService;

	@Override
	public List<PriceInformation> getPriceInformationsForProductWithTax(final ProductModel model)
	{
		final boolean net = false;
		return getPriceInfo(model, net);
	}



	@Override
	public List<PriceInformation> getPriceInformationsForProduct(final ProductModel model)
	{
		final boolean net = getNetGrossStrategy().isNet();
		return getPriceInfo(model, net);
	}



	private List<PriceInformation> getPriceInfo(final ProductModel model, final boolean net)
	{
		final Product product = (Product) getModelService().getSource(model);
		try
		{
			return product.getPriceInformations(getTimeService().getCurrentTime(), net);
		}
		catch (final JaloPriceFactoryException e)
		{
			throw new SystemException(e.getMessage(), e);
		}
	}



	/**
	 * @return the timeService
	 */
	public TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * @param timeService
	 *           the timeService to set
	 */
	@Override
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

}
