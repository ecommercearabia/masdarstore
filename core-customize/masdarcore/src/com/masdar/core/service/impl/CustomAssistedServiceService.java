/**
 *
 */
package com.masdar.core.service.impl;

import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_NAME_ASC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_NAME_DESC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_UID_ASC;
import static de.hybris.platform.assistedserviceservices.constants.AssistedserviceservicesConstants.SORT_BY_UID_DESC;

import de.hybris.platform.assistedserviceservices.impl.DefaultAssistedServiceService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserConstants;
import de.hybris.platform.site.BaseSiteService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author mnasro
 *
 */
public class CustomAssistedServiceService extends DefaultAssistedServiceService
{
	private static final String CURRENTDATE = "currentDate";
	private static final String USERNAME = "username";
	private static final String LOGINDISABLED_PARAMETER = "loginDisabled";
	public static final String SORT_BY_MobileNumber_ASC = "byMobileNumberAsc";
	public static final String SORT_BY_MobileNumber_DESC = "byMobileNumberDesc";

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;


	@Override
	public SearchPageData<CustomerModel> getCustomers(final String searchCriteria, final PageableData pageableData)
	{
		final StringBuilder builder = getCustomerSearchQuery(searchCriteria);

		final Map<String, Object> params = new HashMap<>();
		params.put(CURRENTDATE, getTimeService().getCurrentTime());
		params.put(LOGINDISABLED_PARAMETER, Boolean.FALSE);

		if (StringUtils.isNotBlank(searchCriteria))
		{
			params.put(USERNAME, searchCriteria.toLowerCase());
		}

		final List<SortQueryData> sortQueries = Arrays.asList(
				createSortQueryData(SORT_BY_UID_ASC, builder.toString() + " ORDER BY {p." + CustomerModel.UID + "} ASC"),
				createSortQueryData(SORT_BY_UID_DESC, builder.toString() + " ORDER BY {p." + CustomerModel.UID + "} DESC"),
				createSortQueryData(SORT_BY_NAME_ASC, builder.toString() + " ORDER BY {p." + CustomerModel.NAME + "} ASC"),
				createSortQueryData(SORT_BY_NAME_DESC, builder.toString() + " ORDER BY {p." + CustomerModel.NAME + "} DESC"),
				createSortQueryData(SORT_BY_MobileNumber_ASC,
						builder.toString() + " ORDER BY {p." + CustomerModel.MOBILENUMBER + "} ASC"),
				createSortQueryData(SORT_BY_MobileNumber_DESC,
						builder.toString() + " ORDER BY {p." + CustomerModel.MOBILENUMBER + "} DESC"));

		return getPagedFlexibleSearchService().search(sortQueries, SORT_BY_UID_ASC, params, pageableData);
	}

	@Override
	protected StringBuilder getCustomerSearchQuery(final String searchCriteria)
	{
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		final SiteChannel siteChannel = getSiteChannel();

		if (SiteChannel.B2B.equals(siteChannel))
		{
			return getB2BCustomerSearchQuery(searchCriteria);
		}

		return getB2CCustomerSearchQuery(searchCriteria);
	}


	/**
	 * @return
	 */
	private SiteChannel getSiteChannel()
	{
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		return currentBaseSite == null || currentBaseSite.getChannel() == null
				|| SiteChannel.B2C.equals(currentBaseSite.getChannel()) ? SiteChannel.B2C : SiteChannel.B2B;
	}

	private StringBuilder getB2CCustomerSearchQuery(final String searchCriteria)
	{
		final StringBuilder builder = new StringBuilder();

		builder.append("SELECT ");
		builder.append("{p:" + CustomerModel.PK + "} ");
		builder.append("FROM {" + CustomerModel._TYPECODE + " AS p} ");
		builder.append("WHERE NOT {" + CustomerModel.UID + "}='" + UserConstants.ANONYMOUS_CUSTOMER_UID + "' ");
		builder.append("AND {p:" + CustomerModel.LOGINDISABLED + "} = ?" + LOGINDISABLED_PARAMETER + " ");
		builder.append("AND ({p:" + CustomerModel.DEACTIVATIONDATE + "} IS NULL OR {p:" + CustomerModel.DEACTIVATIONDATE + "} > ?"
				+ CURRENTDATE + ") ");

		if (!StringUtils.isBlank(searchCriteria))
		{
			builder.append("AND (LOWER({p:" + CustomerModel.UID + "}) LIKE CONCAT(?username, '%') ");
			builder.append("OR LOWER({p:name}) LIKE CONCAT('%', CONCAT(?username, '%')) ");
			builder.append("OR LOWER({p:mobileNumber}) LIKE CONCAT('%', CONCAT(?username, '%'))) ");

		}
		return builder;
	}

	private StringBuilder getB2BCustomerSearchQuery(final String searchCriteria)
	{
		final StringBuilder builder = new StringBuilder();

		builder.append("SELECT ");
		builder.append("{p:" + CustomerModel.PK + "} ");
		builder.append("FROM {");
		builder.append(B2BUnitModel._PRINCIPALGROUPRELATION + " AS rel ");
		builder.append("join " + CustomerModel._TYPECODE + " AS p on {rel.source} = {p." + CustomerModel.PK + "}");
		builder.append("join " + B2BUnitModel._TYPECODE + " AS unit on {rel.target} = {unit." + B2BUnitModel.PK + "}");
		builder.append("}");


		builder.append("WHERE NOT {p:" + CustomerModel.UID + "}='" + UserConstants.ANONYMOUS_CUSTOMER_UID + "' ");
		builder.append("AND {p:" + CustomerModel.LOGINDISABLED + "} = ?" + LOGINDISABLED_PARAMETER + " ");
		builder.append("AND ({p:" + CustomerModel.DEACTIVATIONDATE + "} IS NULL OR {p:" + CustomerModel.DEACTIVATIONDATE + "} > ?"
				+ CURRENTDATE + ") ");

		if (!StringUtils.isBlank(searchCriteria))
		{
			builder.append("AND (");
			builder.append("   LOWER({p:" + CustomerModel.UID + "}) LIKE CONCAT(?username, '%') ");
			builder.append("OR LOWER({p:name})                      LIKE CONCAT('%', CONCAT(?username, '%')) ");
			builder.append("OR LOWER({p:mobileNumber})              LIKE CONCAT('%', CONCAT(?username, '%')) ");
			builder.append("OR LOWER({unit:uid})                    LIKE CONCAT('%', CONCAT(?username, '%')) ");
			builder.append("OR LOWER({unit:id})                     LIKE CONCAT('%', CONCAT(?username, '%')) ");
			builder.append(")");
		}
		return builder;
	}


	/**
	 * @return the baseSiteService
	 */
	@Override
	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}
}
