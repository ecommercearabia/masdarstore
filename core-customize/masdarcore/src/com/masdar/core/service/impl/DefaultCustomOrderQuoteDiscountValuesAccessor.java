/**
 *
 */
package com.masdar.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.impl.DefaultOrderQuoteDiscountValuesAccessor;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.masdar.core.service.CustomOrderQuoteDiscountValuesAccessor;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomOrderQuoteDiscountValuesAccessor extends DefaultOrderQuoteDiscountValuesAccessor
		implements CustomOrderQuoteDiscountValuesAccessor
{

	@Override
	public List<DiscountValue> getQuoteEntryDiscountValues(final AbstractOrderEntryModel entry)
	{
		validateParameterNotNullStandardMessage("entry", entry);

		final String discountValuesString = entry.getDiscountValuesInternal();
		final Collection<DiscountValue> discountValues = DiscountValue.parseDiscountValueCollection(discountValuesString);
		if (discountValues == null)
		{
			return Collections.EMPTY_LIST;
		}
		return discountValues instanceof List ? (List) discountValues : new LinkedList(discountValues);
	}

	@Override
	public void setQuoteDiscountValues(final AbstractOrderEntryModel entry, final List<DiscountValue> discountValues)
	{
		validateParameterNotNullStandardMessage("entry", entry);
		validateParameterNotNullStandardMessage("discountValues", discountValues);
		final String discountValuesString = DiscountValue.toString(discountValues);
		entry.setQuoteDiscountValuesInternal(discountValuesString);
		entry.setCalculated(Boolean.FALSE);
	}

}
