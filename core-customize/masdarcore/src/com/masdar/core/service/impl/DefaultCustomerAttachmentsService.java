package com.masdar.core.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.media.MediaPermissionService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.ticket.service.UnsupportedAttachmentException;

import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.masdar.core.service.CustomerAttachmentsService;


public class DefaultCustomerAttachmentsService implements CustomerAttachmentsService
{
	private MediaService mediaService;
	private MediaPermissionService mediaPermissionService;
	private ModelService modelService;
	private CatalogVersionService catalogVersionService;
	private String catalogVersionName;
	private String catalogId;
	private String folderName;
	private String commonCsAgentUserGroup;
	private UserService userService;
	private String allowedUploadedFormats;

	@Override
	public MediaModel createAttachment(final String name, final String contentType, final byte[] data, final UserModel customer,
			final String allowedUploadedFormats)
	{
		this.checkFileExtension(name, allowedUploadedFormats);
		final MediaModel mediaModel = new MediaModel();

		mediaModel.setCode(UUID.randomUUID().toString());
		mediaModel.setCatalogVersion(
				this.getCatalogVersionService().getCatalogVersion(this.getCatalogId(), this.getCatalogVersionName()));
		mediaModel.setMime(contentType);
		mediaModel.setRealFileName(name);
		mediaModel.setFolder(this.getMediaService().getFolder(this.getFolderName()));
		this.getModelService().save(mediaModel);
		this.getMediaService().setDataForMedia(mediaModel, data);
		this.getMediaPermissionService().grantReadPermission(mediaModel,
				this.getUserService().getUserGroupForUID(this.getCommonCsAgentUserGroup()));
		this.getMediaPermissionService().grantReadPermission(mediaModel, customer);
		return mediaModel;
	}

	protected void checkFileExtension(final String name, final String allowedUploadedFormats)
	{
		final String formats = StringUtils.isNotBlank(allowedUploadedFormats)
				? allowedUploadedFormats.replaceAll("\\s", "").toLowerCase()
				: this.getAllowedUploadedFormats().replaceAll("\\s", "").toLowerCase();
		if (!isFileExtensionValid(name, allowedUploadedFormats))
		{
			throw new UnsupportedAttachmentException(
					String.format("File %s has unsupported extension. Only [%s] allowed.", name, formats));
		}
	}

	public boolean isFileExtensionValid(final String name, final String allowedUploadedFormats)
	{
		final String formats = StringUtils.isNotBlank(allowedUploadedFormats)
				? allowedUploadedFormats.replaceAll("\\s", "").toLowerCase()
				: this.getAllowedUploadedFormats().replaceAll("\\s", "").toLowerCase();
		return FilenameUtils.isExtension(name.toLowerCase(), formats.toLowerCase().split(","));
	}

	protected MediaService getMediaService()
	{
		return this.mediaService;
	}

	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	protected MediaPermissionService getMediaPermissionService()
	{
		return this.mediaPermissionService;
	}

	@Required
	public void setMediaPermissionService(final MediaPermissionService mediaPermissionService)
	{
		this.mediaPermissionService = mediaPermissionService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return this.catalogVersionService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected String getCatalogId()
	{
		return this.catalogId;
	}

	@Required
	public void setCatalogId(final String catalogId)
	{
		this.catalogId = catalogId;
	}

	protected String getCatalogVersionName()
	{
		return this.catalogVersionName;
	}

	@Required
	public void setCatalogVersionName(final String catalogVersionName)
	{
		this.catalogVersionName = catalogVersionName;
	}

	protected String getCommonCsAgentUserGroup()
	{
		return this.commonCsAgentUserGroup;
	}

	@Required
	public void setCommonCsAgentUserGroup(final String commonCsAgentUserGroup)
	{
		this.commonCsAgentUserGroup = commonCsAgentUserGroup;
	}

	protected String getFolderName()
	{
		return this.folderName;
	}

	@Required
	public void setFolderName(final String folderName)
	{
		this.folderName = folderName;
	}

	protected UserService getUserService()
	{
		return this.userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public String getAllowedUploadedFormats()
	{
		return this.allowedUploadedFormats;
	}

	@Required
	public void setAllowedUploadedFormats(final String allowedUploadedFormats)
	{
		this.allowedUploadedFormats = allowedUploadedFormats;
	}
}
