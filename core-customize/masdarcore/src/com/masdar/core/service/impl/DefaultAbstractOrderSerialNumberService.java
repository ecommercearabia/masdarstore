/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.core.model.SerialNumberConfigModel;
import com.masdar.core.service.AbstractSerialNumberConfigurationService;


/**
 * Custom Invoice Number Generator based on @SerialNumberConfigModel.
 *
 * @author monzer
 */
public class DefaultAbstractOrderSerialNumberService extends AbstractSerialNumberConfigurationService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAbstractOrderSerialNumberService.class);

	/**
	 * Generate invoice number for base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the optional
	 */
	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		if (baseStore == null)
		{
			LOG.error("DefaultAbstractOrderSerialNumberService: Null Base Store");
			return Optional.empty();
		}
		final SerialNumberConfigModel invoiceSerialNumberModel = baseStore.getAbstractOrderSerialNumberConfig();
		if (invoiceSerialNumberModel == null)
		{
			LOG.error("DefaultAbstractOrderSerialNumberService: Null Order Invoice Serial Number Configuration Model");
			return Optional.empty();
		}
		return getNextSerialNumber(invoiceSerialNumberModel);
	}

}
