/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.masdar.core.service.CustomB2CCartService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomB2CCartService extends DefaultCartService implements CustomB2CCartService
{

	@Resource(name = "typeService")
	private transient TypeService typeService;

	@Resource(name = "orderCodeGenerator")
	protected transient KeyGenerator keyGenerator;

	@Override
	public CartModel createCartFromAbstractOrder(final AbstractOrderModel order)
	{

		return super.clone(getTypeService().getComposedTypeForClass(CartModel.class),
				getTypeService().getComposedTypeForClass(CartEntryModel.class), order, this.getKeyGenerator().generate().toString());
	}


	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	@Required
	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}
}
