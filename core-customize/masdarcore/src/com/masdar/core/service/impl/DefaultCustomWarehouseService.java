/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;
import java.util.stream.Collectors;

import org.fest.util.Collections;

import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomWarehouseService;


/**
 * @author core
 *
 */
public class DefaultCustomWarehouseService implements CustomWarehouseService
{

	@Override
	public Set<WarehouseModel> getActiveWarehousesForCityAndStore(final CityModel city, final BaseStoreModel store)
	{
		if (city == null || store == null)
		{
			return null;
		}
		return city.getWarehouses().stream()
				.filter(e -> !Collections.isEmpty(e.getBaseStores()) && e.getBaseStores().contains(store))
				.collect(Collectors.toSet());
	}

}
