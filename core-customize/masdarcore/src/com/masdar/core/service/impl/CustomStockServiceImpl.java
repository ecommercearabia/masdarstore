package com.masdar.core.service.impl;

import de.hybris.platform.basecommerce.enums.StockLevelUpdateType;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.impl.DefaultStockService;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;
import de.hybris.platform.util.Utilities;

import java.util.Date;

import com.masdar.core.service.CustomStockService;


/**
 * @author monzer
 *
 */
public class CustomStockServiceImpl extends DefaultStockService implements CustomStockService
{

	@Override
	public void reserve(final StockLevelModel stockLevel, final int reserveAmount) throws InsufficientStockLevelException
	{
		if (reserveAmount <= 0)
		{
			throw new IllegalArgumentException("amount must be greater than zero.");
		}
		if (stockLevel == null)
		{
			throw new IllegalArgumentException("Cannot reserve on empty stock level");
		}

		final Integer reserved = this.getStockLevelDao().reserve(stockLevel, reserveAmount);
		if (reserved == null)
		{
			throw new InsufficientStockLevelException("insufficient available amount for stock level [" + stockLevel.getPk() + "]");
		}
		this.clearCacheForItem(stockLevel);
		this.createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RESERVE, reserved, null);
	}

	private void clearCacheForItem(final StockLevelModel stockLevel)
	{
		Utilities.invalidateCache(stockLevel.getPk());
		this.getModelService().refresh(stockLevel);
	}

	private StockLevelHistoryEntryModel createStockLevelHistoryEntry(final StockLevelModel stockLevel,
			final StockLevelUpdateType updateType, final int reserved, final String comment)
	{
		if (stockLevel.getMaxStockLevelHistoryCount() != 0)
		{
			final StockLevelHistoryEntryModel historyEntry = this.getModelService().create(StockLevelHistoryEntryModel.class);
			historyEntry.setStockLevel(stockLevel);
			historyEntry.setActual(stockLevel.getAvailable());
			historyEntry.setReserved(reserved);
			historyEntry.setUpdateType(updateType);
			if (comment != null)
			{
				historyEntry.setComment(comment);
			}
			historyEntry.setUpdateDate(new Date());
			this.getModelService().save(historyEntry);
			return historyEntry;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void release(final StockLevelModel stockLevel, final int releaseAmount)
	{
		if (releaseAmount <= 0)
		{
			throw new IllegalArgumentException("amount must be greater than zero.");
		}
		if (stockLevel == null)
		{
			throw new IllegalArgumentException("Cannot release on empty stock level");
		}

		final Integer released = this.getStockLevelDao().release(stockLevel, releaseAmount);
		if (released == null)
		{
			throw new SystemException("insufficient available amount for stock level [" + stockLevel.getPk() + "]");
		}
		this.clearCacheForItem(stockLevel);
		this.createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RELEASE, released, null);
	}

}
