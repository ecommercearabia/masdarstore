/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.impl.DefaultCommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.base.Preconditions;
import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.model.CityModel;
import com.masdar.core.service.CustomCommerceStockService;
import com.masdar.core.service.CustomUserService;
import com.masdar.core.service.CustomWarehouseService;


/**
 * @author mbaker
 *
 */
public class DefaultCustomCommerceStockService extends DefaultCommerceStockService implements CustomCommerceStockService
{

	private static final String CITY_IS_NULL = "city is null";
	private static final String PRODUCT_IS_NULL = "product is null";

	private static final String FORCE_IN_STOCK_QUANTITY = "commerce.cart.forceinstock.max.quantity";

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customUserService")
	private CustomUserService customUserService;


	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "customWarehouseService")
	private CustomWarehouseService customWarehouseService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	@Override
	public Long getStockLevelForProductAndCity(final ProductModel product, final CityModel city)
	{
		Preconditions.checkArgument(Objects.nonNull(product), PRODUCT_IS_NULL);
		Preconditions.checkArgument(Objects.nonNull(product), CITY_IS_NULL);

		final Set<WarehouseModel> warehouses = getActiveWarehouseForCity(city);

		if (CollectionUtils.isEmpty(warehouses) || product.isDeliveryFlag())
		{
			return 0l;
		}

		final Long availability = getCommerceStockLevelCalculationStrategy()
				.calculateAvailability(getStockService().getStockLevels(product, warehouses));
		return availability == null ? getForceInStockMaxQuantity() : availability;
	}

	private Set<WarehouseModel> getActiveWarehouseForCity(final CityModel city)
	{
		return getCustomWarehouseService().getActiveWarehousesForCityAndStore(city, getBaseStoreService().getCurrentBaseStore());
	}


	@Override
	public StockLevelStatus getStockLevelStatusForProductAndCity(final ProductModel product, final CityModel city)
	{
		Preconditions.checkArgument(Objects.nonNull(product), PRODUCT_IS_NULL);
		Preconditions.checkArgument(Objects.nonNull(product), CITY_IS_NULL);

		final Set<WarehouseModel> warehouses = getActiveWarehouseForCity(city);

		if (CollectionUtils.isEmpty(warehouses) || product.isDeliveryFlag())
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
		return getStockService().getProductStatus(product, warehouses);
	}

	@Override
	public Long getStockLevelForProductAndCityByCurrentCustomer(final ProductModel product)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isEmpty())
		{
			return 0l;
		}
		final ShipmentTypeInfo info = shipmentTypeInfoForCurrentUser.get();
		final UserModel currentUser = getUserService().getCurrentUser();
		if (Objects.isNull(currentUser) || Objects.isNull(info.getCity()) || product.isDeliveryFlag())
		{
			return 0l;
		}
		return getStockLevelForProductAndCity(product, info.getCity());
	}



	@Override
	public StockLevelStatus getStockLevelStatusForProductAndCityByCurrentCustomer(final ProductModel product)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isEmpty())
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
		final ShipmentTypeInfo info = shipmentTypeInfoForCurrentUser.get();
		final UserModel currentUser = getUserService().getCurrentUser();
		if (Objects.isNull(currentUser) || Objects.isNull(info.getCity()) || product.isDeliveryFlag())
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
		return getStockLevelStatusForProductAndCity(product, info.getCity());
	}

	@Override
	public Long getStockLevelForProductAndPOSByCurrentCustomer(final ProductModel product)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isEmpty())
		{
			return 0l;
		}
		final ShipmentTypeInfo info = shipmentTypeInfoForCurrentUser.get();
		if (Objects.isNull(info) || Objects.isNull(info.getPointOfService()) || !product.isPickupInStoreFlag())
		{
			return 0l;
		}
		final Long availability = getStockLevelForProductAndPointOfService(product, info.getPointOfService());
		return availability == null ? getForceInStockMaxQuantity() : availability;
	}

	@Override
	public StockLevelStatus getStockLevelStatusForProductAndPOSByCurrentCustomer(final ProductModel product)
	{
		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isEmpty())
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
		final ShipmentTypeInfo info = shipmentTypeInfoForCurrentUser.get();
		if (Objects.isNull(info) || Objects.isNull(info.getPointOfService()) || !product.isPickupInStoreFlag())
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
		return getStockLevelStatusForProductAndPointOfService(product, info.getPointOfService());
	}

	protected Long getForceInStockMaxQuantity()
	{
		return getConfigurationService().getConfiguration().getLong(FORCE_IN_STOCK_QUANTITY);
	}

	/**
	 * @return the customUserService
	 */
	protected CustomUserService getCustomUserService()
	{
		return customUserService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @return the customWareohuseService
	 */
	public CustomWarehouseService getCustomWarehouseService()
	{
		return customWarehouseService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


}
