/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.b2b.enums.B2BPeriodRange;
import de.hybris.platform.b2b.model.B2BBudgetExceededPermissionModel;
import de.hybris.platform.b2b.model.B2BBudgetModel;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCreditLimitModel;
import de.hybris.platform.b2b.model.B2BOrderThresholdPermissionModel;
import de.hybris.platform.b2b.model.B2BOrderThresholdTimespanPermissionModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.StandardDateRange;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.exception.type.CommercialRegistrationExceptionType;
import com.masdar.commercialregistration.service.CommercialRegistrationService;
import com.masdar.core.service.CommercialRegistrationB2BUnitService;
import com.masdar.core.service.CustomPointOfServiceService;
import com.masdar.masdarsecureportalcustomaddon.model.B2BRegistrationModel;


/**
 * @author origin
 *
 */
public class DefaultCommercialRegistrationB2BUnitService implements CommercialRegistrationB2BUnitService
{
	/**
	 *
	 */
	private static final Random RANDOM = new Random();


	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(DefaultCommercialRegistrationB2BUnitService.class);


	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "pointOfServiceService")
	private CustomPointOfServiceService customPointOfServiceService;

	@Resource(name = "commercialRegistrationService")
	private CommercialRegistrationService commercialRegistrationService;

	@Override
	public boolean validateCr(final String companyCr, final BaseStoreModel baseStore)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(companyCr), "CRNumber Must Not be Blank");
		Preconditions.checkNotNull(baseStore, "CR Number Must Not be Blank");

		final String COSTCENTER_CODE_POSTFIX = baseStore.getCostCenterCodePostfix() == null ? ""
				: baseStore.getCostCenterCodePostfix();
		final String CREDITLIMMIT_CODE_POSTFIX = baseStore.getCreditLimitCodePostfix() == null ? ""
				: baseStore.getCreditLimitCodePostfix();
		final String BUDGET_CODE_POSTFIX = baseStore.getBudgetCodePostfix() == null ? "" : baseStore.getBudgetCodePostfix();
		final String ORDER_THRESHOLD_TIMESPAN_PERMISSION_CODE_POSTFIX = baseStore
				.getOrderThresholdTimespanPermissionCodePostfix() == null ? ""
						: baseStore.getOrderThresholdTimespanPermissionCodePostfix();
		final String ORDER_THRESHOLD_PERMISSION_CODE_POSTFIX = baseStore.getOrderThresholdPermissionCodePostfix() == null ? ""
				: baseStore.getOrderThresholdPermissionCodePostfix();

		Optional<CompanyRegistrationInfo> optCompanyCrInfo = Optional.empty();
		try
		{
			optCompanyCrInfo = commercialRegistrationService.getCompanyCrInfo(companyCr, baseStore);
		}
		catch (final CommercialRegistrationException e)
		{
			LOG.error(e.getMessage());
			return false;
		}


		final CompanyRegistrationInfo companyCrInfo = optCompanyCrInfo.get();

		return isNotExist(B2BCostCenterModel._TYPECODE, companyCrInfo, B2BCostCenterModel.CODE, COSTCENTER_CODE_POSTFIX)
				&& isNotExist(B2BBudgetModel._TYPECODE, companyCrInfo, B2BBudgetModel.CODE, BUDGET_CODE_POSTFIX)
				&& isNotExist(B2BCreditLimitModel._TYPECODE, companyCrInfo, B2BCreditLimitModel.CODE, CREDITLIMMIT_CODE_POSTFIX)
				&& isNotExist(B2BOrderThresholdPermissionModel._TYPECODE, companyCrInfo, B2BOrderThresholdPermissionModel.CODE,
						ORDER_THRESHOLD_PERMISSION_CODE_POSTFIX)
				&& isNotExist(B2BOrderThresholdTimespanPermissionModel._TYPECODE, companyCrInfo,
						B2BOrderThresholdTimespanPermissionModel.CODE, ORDER_THRESHOLD_TIMESPAN_PERMISSION_CODE_POSTFIX)
				&& isNotExist(B2BBudgetExceededPermissionModel._TYPECODE, companyCrInfo, B2BBudgetExceededPermissionModel.CODE,
						BUDGET_CODE_POSTFIX)
				&& isNotExist(B2BUnitModel._TYPECODE, companyCrInfo, B2BUnitModel.UID, "");
	}


	/**
	 * @param companyCrInfo
	 * @param companyCr
	 * @param baseStore
	 */
	private boolean isNotExist(final String typeCode, final CompanyRegistrationInfo companyCrInfo, final String field,
			final String code)
	{
		return isCountZero(getCountQuery(typeCode, field, companyCrInfo.getCrEntityNumber() + code))
				&& isCountZero(getCountQuery(typeCode, field, companyCrInfo.getCrNumber() + code));
		//				&& isCountZero(getCountQuery(typeCode, field, companyCrInfo.getCrMainNumber() + code))
	}


	private boolean isCountZero(final String queryString)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		final SearchResult<Object> search = getFlexibleSearchService().search(query);
		return search.getCount() == 0;
	}

	@Override
	public B2BUnitModel createB2bUnitFromCrForCurrentStore(final B2BRegistrationModel registration)
			throws CommercialRegistrationException
	{
		Preconditions.checkNotNull(registration, "B2BRegistrationModel Should not be empty or null");
		Preconditions.checkArgument(StringUtils.isNotBlank(registration.getCompanyCr()), "Company Cr Should not be empty or null");
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		Preconditions.checkNotNull(currentBaseStore, "Current BaseStore Should not be empty or null");


		return createB2bUnitFromCr(registration, currentBaseStore);
	}

	@Override
	public B2BUnitModel createB2bUnitFromCr(final B2BRegistrationModel registration, final BaseStoreModel baseStore)
			throws CommercialRegistrationException
	{

		final B2BUnitModel b2bUnit = createB2bUnit(registration);

		final B2BCostCenterModel costCenter = createCostCenter(registration, baseStore, b2bUnit);
		createBudget(registration, baseStore, costCenter, b2bUnit);
		createCreditLimit(registration, baseStore, b2bUnit);
		createThresholdPermission(registration, baseStore, b2bUnit);
		createThresholdTimeSpanPermission(registration, baseStore, b2bUnit);
		createBudgetExceededPermission(registration, baseStore, b2bUnit);

		getModelService().save(b2bUnit);
		registration.setDefaultB2BUnit(b2bUnit);
		getModelService().save(registration);
		return b2bUnit;
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @return
	 * @throws CommercialRegistrationException
	 */
	private B2BUnitModel createB2bUnit(final B2BRegistrationModel registration) throws CommercialRegistrationException
	{
		// Create B2BUnit with
		//  - uid 		= companyCr
		//  - desc 		= companyname
		//  - name  		= companyname
		//  - locname	= companyname
		//  - VatId   	= companyVATNo
		//  - b2bUnitId	= ??
		final UserModel user = sessionService.getAttribute(SessionContext.USER);
		sessionService.setAttribute(SessionContext.USER, getUserService().getUserForUID(Constants.USER.ADMIN_EMPLOYEE));

		final Optional<CompanyRegistrationInfo> optCompanyCrInfo = commercialRegistrationService
				.getCompanyCrInfoByCurrentStore(registration.getCompanyCr());

		if (optCompanyCrInfo.isEmpty())
		{
			throw new CommercialRegistrationException(
					String.format("Cannot Find Cr Info with [%s] Cr Number", registration.getCompanyCr()),
					CommercialRegistrationExceptionType.INTERNAL_ERROR);
		}

		final CompanyRegistrationInfo companyCrInfo = optCompanyCrInfo.get();

		final B2BUnitModel b2bUnit = getModelService().create(B2BUnitModel.class);
		b2bUnit.setUid(registration.getCompanyCr());
		b2bUnit.setName(companyCrInfo.getCrName());
		b2bUnit.setDescription(companyCrInfo.getCrName());
		b2bUnit.setLocName(companyCrInfo.getCrName());
		b2bUnit.setVatID(registration.getCompanyVATNumber());
		b2bUnit.setId(registration.getCompanyCr());
		b2bUnit.setCashCustomer(registration.isCashCustomer());

		// get account manager from PointOfService



		final EmployeeModel accountManager = getRandomAccountManagerFromPos(registration.getPointOfService());
		b2bUnit.setAccountManager(accountManager);
		getModelService().save(b2bUnit);

		sessionService.setAttribute(SessionContext.USER, user);
		return b2bUnit;
	}

	/**
	 * @param pointOfService
	 * @return
	 */
	private EmployeeModel getRandomAccountManagerFromPos(final PointOfServiceModel pointOfService)
	{
		Preconditions.checkNotNull(pointOfService, "PointOfService Must not be null");
		final Set<EmployeeModel> employeesForPointOfService = getCustomPointOfServiceService()
				.getEmployeesForPointOfService(pointOfService);

		if (employeesForPointOfService == null || employeesForPointOfService.isEmpty())
		{
			return null;
		}

		final int randomNum = RANDOM.nextInt(employeesForPointOfService.size());
		return employeesForPointOfService.stream().skip(randomNum).findFirst().get();
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @param b2bUnit
	 * @return
	 */
	private B2BBudgetExceededPermissionModel createBudgetExceededPermission(final B2BRegistrationModel registration,
			final BaseStoreModel baseStore, final B2BUnitModel b2bUnit)
	{
		// Create B2BBudgetExceededPermission with
		//  - code 		= companyCr + _budget
		//  - Unit 		= (the unit created Above)
		//  - active		= true
		final String BUDGET_CODE_POSTFIX = baseStore.getBudgetCodePostfix();

		final B2BBudgetExceededPermissionModel b2bBudgetExceededPermissionModel = getModelService()
				.create(B2BBudgetExceededPermissionModel.class);

		b2bBudgetExceededPermissionModel.setCode(registration.getCompanyCr() + BUDGET_CODE_POSTFIX);
		b2bBudgetExceededPermissionModel.setUnit(b2bUnit);
		b2bBudgetExceededPermissionModel.setActive(true);
		getModelService().save(b2bBudgetExceededPermissionModel);

		registration.setDefaultB2BUnit(b2bUnit);
		getModelService().save(registration);

		return b2bBudgetExceededPermissionModel;
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @param b2bUnit
	 * @return
	 */
	private B2BOrderThresholdTimespanPermissionModel createThresholdTimeSpanPermission(final B2BRegistrationModel registration,
			final BaseStoreModel baseStore, final B2BUnitModel b2bUnit)
	{
		// Create B2BOrderThresholdTimespanPermission with
		//  - code 		= email + _ttsp
		//  - Unit 		= (the unit created Above)
		//  - threshold	= 1000000000
		//  - range		= DAY:B2BPeriodRange

		final String ORDER_THRESHOLD_TIMESPAN_PERMISSION_CODE_POSTFIX = baseStore.getOrderThresholdTimespanPermissionCodePostfix();
		final BigDecimal budget = baseStore.getBudgetAmmount();
		final CurrencyModel currency = baseStore.getAutoRegistrationCurrency();

		final B2BOrderThresholdTimespanPermissionModel b2bOrderThresholdTimespanPermissionModel = getModelService()
				.create(B2BOrderThresholdTimespanPermissionModel.class);
		b2bOrderThresholdTimespanPermissionModel
				.setCode(registration.getCompanyCr() + ORDER_THRESHOLD_TIMESPAN_PERMISSION_CODE_POSTFIX);
		b2bOrderThresholdTimespanPermissionModel.setUnit(b2bUnit);
		b2bOrderThresholdTimespanPermissionModel.setThreshold(budget.doubleValue());
		b2bOrderThresholdTimespanPermissionModel.setRange(B2BPeriodRange.DAY);
		b2bOrderThresholdTimespanPermissionModel.setCurrency(currency);
		getModelService().save(b2bOrderThresholdTimespanPermissionModel);
		return b2bOrderThresholdTimespanPermissionModel;
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @param b2bUnit
	 * @return
	 */
	private B2BOrderThresholdPermissionModel createThresholdPermission(final B2BRegistrationModel registration,
			final BaseStoreModel baseStore, final B2BUnitModel b2bUnit)
	{
		// Create B2BOrderThresholdPermission with
		//  - code 		= email + _otp
		//  - Unit 		= (the unit created Above)
		//  - threshold		= 1000000000
		//  - currency	= SAR

		final String ORDER_THRESHOLD_PERMISSION_CODE_POSTFIX = baseStore.getOrderThresholdPermissionCodePostfix();
		final BigDecimal budget = baseStore.getBudgetAmmount();
		final CurrencyModel currency = baseStore.getAutoRegistrationCurrency();

		final B2BOrderThresholdPermissionModel b2bOrderThresholdPermissionModel = getModelService()
				.create(B2BOrderThresholdPermissionModel.class);

		b2bOrderThresholdPermissionModel.setCode(registration.getCompanyCr() + ORDER_THRESHOLD_PERMISSION_CODE_POSTFIX);
		b2bOrderThresholdPermissionModel.setUnit(b2bUnit);
		b2bOrderThresholdPermissionModel.setThreshold(budget.doubleValue());
		b2bOrderThresholdPermissionModel.setCurrency(currency);
		getModelService().save(b2bOrderThresholdPermissionModel);
		return b2bOrderThresholdPermissionModel;
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @param b2bUnit
	 * @return
	 */
	private B2BCreditLimitModel createCreditLimit(final B2BRegistrationModel registration, final BaseStoreModel baseStore,
			final B2BUnitModel b2bUnit)
	{
		// Create B2BCreditLimit with
		//  - code 			= email + _credit_limit
		//  - budget		= 1000000000
		//  - currency		= SAR
		//  - dateRange	= DAY:B2BPeriodRange
		//  - Unit 			= (the unit created Above)
		final String CREDITLIMMIT_CODE_POSTFIX = baseStore.getCreditLimitCodePostfix();
		final BigDecimal budget = baseStore.getBudgetAmmount();
		final CurrencyModel currency = baseStore.getAutoRegistrationCurrency();

		final B2BCreditLimitModel creditLimit = getModelService().create(B2BCreditLimitModel.class);
		creditLimit.setCode(registration.getCompanyCr() + CREDITLIMMIT_CODE_POSTFIX);
		creditLimit.setAmount(budget);
		creditLimit.setAlertThreshold(budget);
		creditLimit.setCurrency(currency);
		creditLimit.setDateRange(B2BPeriodRange.DAY);
		Collection<B2BUnitModel> units = creditLimit.getUnit();
		if (units == null)
		{
			units = new ArrayList<>();
			units.add(b2bUnit);
			creditLimit.setUnit(units);
		}
		getModelService().save(creditLimit);
		b2bUnit.setCreditLimit(creditLimit);
		return creditLimit;
	}

	private B2BBudgetModel createBudget(final B2BRegistrationModel registration, final BaseStoreModel baseStore,
			final B2BCostCenterModel costCenter, final B2BUnitModel b2bUnit)
	{

		// Create B2BBudget with
		//  - code 		= companyCr + _budget
		//  - name 		= companyCr + Budget
		//  - Unit 		= (the unit created Above)
		//  - budget		= 1000000000
		//  - currency	= SAR
		//  - dateRange  = 01-01-2020 ,01-01-2100
		final String BUDGET_CODE_POSTFIX = baseStore.getBudgetCodePostfix();
		final String BUDGET_NAME_POSTFIX = baseStore.getBudgetNamePostfix();
		final BigDecimal budget = baseStore.getBudgetAmmount();
		final CurrencyModel currency = baseStore.getAutoRegistrationCurrency();
		final StandardDateRange budgetDateRange = baseStore.getBudgetDateRange();

		final B2BBudgetModel budgetModel = getModelService().create(B2BBudgetModel.class);
		budgetModel.setCode(registration.getCompanyCr() + BUDGET_CODE_POSTFIX);
		budgetModel.setName(registration.getCompanyCr() + " " + BUDGET_NAME_POSTFIX);

		Set<B2BCostCenterModel> costCenters = budgetModel.getCostCenters();
		if (costCenters == null)
		{
			costCenters = new HashSet<>();
			costCenters.add(costCenter);
		}
		budgetModel.setCostCenters(costCenters);

		budgetModel.setBudget(budget);
		budgetModel.setCurrency(currency);
		budgetModel.setDateRange(budgetDateRange);
		budgetModel.setUnit(b2bUnit);
		getModelService().save(budgetModel);
		return budgetModel;
	}

	/**
	 * @param registration
	 * @param baseStore
	 * @param b2bUnit
	 * @return
	 */
	private B2BCostCenterModel createCostCenter(final B2BRegistrationModel registration, final BaseStoreModel baseStore,
			final B2BUnitModel b2bUnit)
	{
		// Create B2BCostCenter with
		//  - code 		= companyCr + _cost_center
		//  - name 		= companyCr + Cost Center
		//  - Unit  	= (the unit created Above)
		//  - currency	= SAR (From BaseStore)
		final String COSTCENTER_CODE_POSTFIX = baseStore.getCostCenterCodePostfix();
		final String COSTCENTER_NAME_POSTFIX = baseStore.getCostCenterNamePostfix();
		final CurrencyModel currency = baseStore.getAutoRegistrationCurrency();

		final B2BCostCenterModel costCenter = getModelService().create(B2BCostCenterModel.class);
		costCenter.setCode(registration.getCompanyCr() + COSTCENTER_CODE_POSTFIX);
		costCenter.setName(registration.getCompanyCr() + " " + COSTCENTER_NAME_POSTFIX);
		costCenter.setCurrency(currency);
		costCenter.setUnit(b2bUnit);
		getModelService().save(costCenter);
		return costCenter;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	private String getCountQuery(final String typecode, final String field, final String code)
	{
		return String.format("SELECT {p:pk} FROM {%s AS p} where {p:%s} = '%s'", typecode, field, code);
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @return the customPointOfServiceService
	 */
	public CustomPointOfServiceService getCustomPointOfServiceService()
	{
		return customPointOfServiceService;
	}

}
