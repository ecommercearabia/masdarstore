/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.impl.DefaultOrderService;

import javax.annotation.Resource;

import com.masdar.core.dao.CustomB2COrderDao;
import com.masdar.core.service.CustomB2COrderService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomB2COrderService extends DefaultOrderService implements CustomB2COrderService
{

	@Resource(name = "customB2COrderDao")
	private CustomB2COrderDao customB2COrderDao;

	@Override
	public OrderModel getOrderForCode(final String code)
	{
		return getCustomB2COrderDao().findOrderByCode(code);
	}

	protected CustomB2COrderDao getCustomB2COrderDao()
	{
		return customB2COrderDao;
	}

}
