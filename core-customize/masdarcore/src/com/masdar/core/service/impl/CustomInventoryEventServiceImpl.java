/**
 *
 */
package com.masdar.core.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.warehousing.inventoryevent.service.impl.DefaultInventoryEventService;
import de.hybris.platform.warehousing.model.AllocationEventModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.masdar.core.service.CustomStockService;


/**
 * @author mnasro
 *
 */
public class CustomInventoryEventServiceImpl extends DefaultInventoryEventService
{
	@Resource(name = "stockService")
	private CustomStockService customStockService;

	/**
	 * @return the customStockService
	 */
	protected CustomStockService getCustomStockService()
	{
		return customStockService;
	}

	@Override
	public List<AllocationEventModel> createAllocationEventsForConsignmentEntry(final ConsignmentEntryModel consignmentEntry)
	{
		Preconditions.checkArgument((!consignmentEntry.getConsignment().getWarehouse().isExternal() ? 1 : 0) != 0,
				"External warehouses are not allowed to create AllocationEvent");
		LOGGER.debug("Creating allocation event for ConsignmentEntry :: Product [{}], at Warehouse [{}]: \tQuantity: '{}'",
				new Object[]
				{ consignmentEntry.getOrderEntry().getProduct().getCode(), consignmentEntry.getConsignment().getWarehouse().getCode(),
						consignmentEntry.getQuantity() });
		final Collection stockLevels = this.getStockService().getStockLevels(consignmentEntry.getOrderEntry().getProduct(),
				Collections.singletonList(consignmentEntry.getConsignment().getWarehouse()));
		final Map<StockLevelModel, Long> stockLevelForAllocation = this.getStockLevelSelectionStrategy()
				.getStockLevelsForAllocation(stockLevels, consignmentEntry.getQuantity());
		final List<AllocationEventModel> allocationEvents = stockLevelForAllocation.entrySet().stream().map(stockMapEntry -> {
			final AllocationEventModel allocationEvent = (AllocationEventModel) this.getModelService()
					.create(AllocationEventModel.class);
			allocationEvent.setConsignmentEntry(consignmentEntry);
			allocationEvent.setStockLevel(stockMapEntry.getKey());
			allocationEvent.setEventDate(this.getTimeService().getCurrentTime());
			allocationEvent.setQuantity(stockMapEntry.getValue().longValue());
			this.getModelService().save(allocationEvent);
			final boolean isActive = consignmentEntry.getConsignment() != null
					&& consignmentEntry.getConsignment().getOrder() != null
					&& consignmentEntry.getConsignment().getOrder().getStore() != null
					&& consignmentEntry.getConsignment().getOrder().getStore().isReservedAllocationEventForConsignmentEntry();
			if (isActive)
			{
				try
				{
					getCustomStockService().reserve(stockMapEntry.getKey(), stockMapEntry.getValue().intValue());
				}
				catch (final InsufficientStockLevelException e)
				{
					LOGGER.error(
							"DefaultCustomInventoryEventService ->  createAllocationEventsForConsignmentEntry -> InsufficientStockLevelException"
									+ e.getMessage(),
							e);
				}
			}
			return allocationEvent;
		}).collect(Collectors.toList());
		return allocationEvents;
	}

}

