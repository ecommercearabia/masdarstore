/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomB2CCartService extends CartService
{
	CartModel createCartFromAbstractOrder(final AbstractOrderModel order);
}
