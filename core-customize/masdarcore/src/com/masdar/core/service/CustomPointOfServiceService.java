/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import java.util.List;
import java.util.Set;


/**
 * @author Husam Dababneh (husam.dababneh@erabia.com)
 *
 */
public interface CustomPointOfServiceService extends PointOfServiceService
{
	List<PointOfServiceModel> getPointOfServicesForRegion(final String countryIsoCode, final String regionIsoCode,
			final BaseStoreModel baseStore);

	Set<EmployeeModel> getEmployeesForPointOfService(final PointOfServiceModel model);
}
