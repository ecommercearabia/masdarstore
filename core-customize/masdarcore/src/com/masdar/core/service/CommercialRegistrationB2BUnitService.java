/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.store.BaseStoreModel;

import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.masdarsecureportalcustomaddon.model.B2BRegistrationModel;


/**
 * @author origin
 *
 */
public interface CommercialRegistrationB2BUnitService
{
	B2BUnitModel createB2bUnitFromCr(final B2BRegistrationModel registration, final BaseStoreModel baseStore)
			throws CommercialRegistrationException;


	B2BUnitModel createB2bUnitFromCrForCurrentStore(final B2BRegistrationModel registration)
			throws CommercialRegistrationException;

	boolean validateCr(final String companyCr, final BaseStoreModel baseStore);
}
