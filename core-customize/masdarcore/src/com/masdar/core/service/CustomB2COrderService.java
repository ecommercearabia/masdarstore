/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface CustomB2COrderService extends OrderService
{
	/**
	 * Gets the order based on it's {@link OrderModel#CODE}.
	 *
	 * @param code
	 *           the code
	 * @return the order
	 */
	public abstract OrderModel getOrderForCode(final String code);
}
