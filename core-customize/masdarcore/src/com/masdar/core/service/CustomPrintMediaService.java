/**
 *
 */
package com.masdar.core.service;

import de.hybris.platform.warehousing.labels.service.PrintMediaService;


/**
 * @author mnasro
 *
 */
public interface CustomPrintMediaService extends PrintMediaService
{

}
