/**
 *
 */
package com.masdar.core.area.service.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.area.dao.AreaDao;
import com.masdar.core.area.service.AreaService;
import com.masdar.core.model.AreaModel;


/**
 * The Class DefaultCityService.
 *
 * @author mnasro
 */
public class DefaultAreaService implements AreaService
{

	/** The area dao. */
	@Resource(name = "areaDao")
	private AreaDao areaDao;

	protected AreaDao getAreaDao()
	{
		return areaDao;
	}

	/**
	 * Gets the by city code.
	 *
	 * @param cityCode the city code
	 * @return the by city code
	 */
	@Override
	public Optional<List<AreaModel>> getByCityCode(final String cityCode)
	{
		return getAreaDao().findAreasByCityCode(cityCode);
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public Optional<List<AreaModel>> getAll()
	{
		return getAreaDao().findAll();
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the optional
	 */
	@Override
	public Optional<AreaModel> get(final String code)
	{
		return getAreaDao().find(code);
	}



}
