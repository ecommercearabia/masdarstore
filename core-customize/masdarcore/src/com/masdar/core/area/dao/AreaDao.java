
package com.masdar.core.area.dao;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.AreaModel;


/**
 * @author mnasro
 *
 *         The Interface AreaDao.
 */
public interface AreaDao
{

	/**
	 * Find areas by cityCode.
	 *
	 * @param cityCode the city code
	 * @return the list
	 */
	public Optional<List<AreaModel>> findAreasByCityCode(String cityCode);

	/**
	 * Find.
	 *
	 * @param code the code
	 * @return the optional
	 */
	public Optional<AreaModel> find(String code);

	/**
	 * Find.
	 *
	 * @return the list
	 */
	public Optional<List<AreaModel>> findAll();

}
