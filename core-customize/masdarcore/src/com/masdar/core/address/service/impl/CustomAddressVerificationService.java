package com.masdar.core.address.service.impl;

import de.hybris.platform.commerceservices.address.AddressErrorCode;
import de.hybris.platform.commerceservices.address.AddressFieldType;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.address.data.AddressFieldErrorData;
import de.hybris.platform.commerceservices.address.data.AddressVerificationResultData;
import de.hybris.platform.commerceservices.address.util.AddressVerificationResultUtils;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * The mock implementation of AddressVerificationService to be used in the absence of an external service.
 */
public class CustomAddressVerificationService implements
		com.masdar.core.address.service.CustomAddressVerificationService<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>>
{
	private BaseStoreService baseStoreService;

	public static final String ACCEPT = "accept";
	public static final String REJECT = "reject";

	public static final String TITLE_CODE = "titleCode";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String ADDRESS_LINE_1 = "addressline1";
	public static final String ADDRESS_LINE_2 = "addressline2";
	public static final String REGION = "region";
	public static final String ZIP_CODE = "zipcode";
	public static final String CITY = "city";
	public static final String COUNTRY = "country";
	public static final String ADDRESS_NAME = "addressName";

	public static final String MISSING = "missing";
	public static final String INVALID = "invalid";

	private static final String TITLE = "title";

	@Override
	public AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> verifyAddress(
			final AddressModel addressModel)
	{
		final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> acceptedResult = createVerificationResult();

		validateAddressFields(acceptedResult, addressModel);
		if (AddressVerificationResultUtils.requiresErrorHandling(acceptedResult))
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(REJECT));
		}
		else
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(ACCEPT));
		}
		return acceptedResult;
	}

	@Override
	public AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> verifyB2CAddress(
			final AddressModel addressModel)
	{
		final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> acceptedResult = createVerificationResult();

		validateB2CAddressFields(acceptedResult, addressModel);
		if (AddressVerificationResultUtils.requiresErrorHandling(acceptedResult))
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(REJECT));
		}
		else
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(ACCEPT));
		}
		return acceptedResult;
	}

	@Override
	public boolean isCustomerAllowedToIgnoreSuggestions()
	{
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		return baseStore != null && baseStore.isCustomerAllowedToIgnoreSuggestions();
	}

	/**
	 * Validates each field input in an AddressForm. Field validation is usually left up to the external address
	 * verification service so the mock must perform this function.
	 */
	protected void validateAddressFields(
			final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> result,
			final AddressModel address)
	{

		final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList = new ArrayList<AddressFieldErrorData<AddressFieldType, AddressErrorCode>>();
		// Check required fields not empty.

		if (address.getTitle() == null || StringUtils.isEmpty(address.getTitle().getCode()))
		{
			addErrorToVerificationResult(TITLE, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getFirstname()))
		{
			addErrorToVerificationResult(FIRST_NAME, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getLastname()))
		{
			addErrorToVerificationResult(LAST_NAME, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getLine1()))
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getAddressName()))
		{
			addErrorToVerificationResult(ADDRESS_NAME, MISSING, errorList);
		}
		if (address.getCountry() == null || StringUtils.isEmpty(address.getCountry().getIsocode()))
		{
			addErrorToVerificationResult(COUNTRY, MISSING, errorList);
		}
		if (address.getRegion() == null || StringUtils.isEmpty(address.getRegion().getIsocode()))
		{
			addErrorToVerificationResult(REGION, MISSING, errorList);
		}
		if (address.getCity() == null || StringUtils.isEmpty(address.getCity().getCode()))
		{
			addErrorToVerificationResult(CITY, MISSING, errorList);
		}

		// Check length of fields.
		if (address.getTitle() != null && StringUtils.length(address.getTitle().getCode()) > 255)
		{
			addErrorToVerificationResult(TITLE, INVALID, errorList);
		}
		if (StringUtils.length(address.getFirstname()) > 255)
		{
			addErrorToVerificationResult(FIRST_NAME, INVALID, errorList);
		}
		if (StringUtils.length(address.getLastname()) > 255)
		{
			addErrorToVerificationResult(LAST_NAME, INVALID, errorList);
		}
		if (StringUtils.length(address.getLine1()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, INVALID, errorList);
		}

		if (StringUtils.isNotEmpty(address.getLine2()) && StringUtils.length(address.getLine2()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_2, INVALID, errorList);
		}

		if (StringUtils.isNotEmpty(address.getTown()) && StringUtils.length(address.getTown()) > 255)
		{
			addErrorToVerificationResult(CITY, INVALID, errorList);
		}

		if (address.getRegion() != null && StringUtils.length(address.getRegion().getIsocode()) > 255)
		{
			addErrorToVerificationResult(REGION, INVALID, errorList);
		}
		else if (address.getCountry() != null && StringUtils.length(address.getCountry().getIsocode()) > 255)
		{
			addErrorToVerificationResult(COUNTRY, INVALID, errorList);
		}
		else if (StringUtils.length(address.getAddressName()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_NAME, INVALID, errorList);
		}

		// Check if region belongs to address country
		if (Objects.isNull(address.getRegion()) || !address.getCountry().equals(address.getRegion().getCountry()))
		{
			addErrorToVerificationResult(REGION, INVALID, errorList);
		}
		//Check if city belongs to address region.
		if (address.getCity() == null || !address.getCity().getRegion().equals(address.getRegion()))
		{
			addErrorToVerificationResult(CITY, INVALID, errorList);
		}

		result.setFieldErrors(errorList);
	}

	protected void validateB2CAddressFields(
			final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> result,
			final AddressModel address)
	{

		final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList = new ArrayList<AddressFieldErrorData<AddressFieldType, AddressErrorCode>>();
		// Check required fields not empty.

		if (address.getTitle() != null && StringUtils.isEmpty(address.getTitle().getCode()))
		{
			addErrorToVerificationResult(TITLE, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getFirstname()))
		{
			addErrorToVerificationResult(FIRST_NAME, MISSING, errorList);
		}
		//		if (StringUtils.isEmpty(address.getLastname()))
		//		{
		//			addErrorToVerificationResult(LAST_NAME, MISSING, errorList);
		//		}
		if (StringUtils.isEmpty(address.getLine1()))
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, MISSING, errorList);
		}
		if (StringUtils.isEmpty(address.getAddressName()))
		{
			addErrorToVerificationResult(ADDRESS_NAME, MISSING, errorList);
		}
		if (address.getCountry() != null && StringUtils.isEmpty(address.getCountry().getIsocode()))
		{
			addErrorToVerificationResult(COUNTRY, MISSING, errorList);
		}
		if (address.getRegion() != null && StringUtils.isEmpty(address.getRegion().getIsocode()))
		{
			addErrorToVerificationResult(REGION, MISSING, errorList);
		}
		if (address.getCity() != null && StringUtils.isEmpty(address.getCity().getCode())) //TODO:
		{
			addErrorToVerificationResult(CITY, MISSING, errorList);
		}

		// Check length of fields.
		if (StringUtils.length(address.getTitle().getCode()) > 255)
		{
			addErrorToVerificationResult(TITLE, INVALID, errorList);
		}
		if (StringUtils.length(address.getFirstname()) > 255)
		{
			addErrorToVerificationResult(FIRST_NAME, INVALID, errorList);
		}
		//		if (StringUtils.length(address.getLastname()) > 255)
		//		{
		//			addErrorToVerificationResult(LAST_NAME, INVALID, errorList);
		//		}
		if (StringUtils.length(address.getLine1()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, INVALID, errorList);
		}

		if (StringUtils.isNotEmpty(address.getLine2()) && StringUtils.length(address.getLine2()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_2, INVALID, errorList);
		}

		if (StringUtils.isNotEmpty(address.getTown()) && StringUtils.length(address.getTown()) > 255)
		{
			addErrorToVerificationResult(CITY, INVALID, errorList);
		}

		//		if (address.getRegion() != null && address.getRegion().getIsocode() == null)
		//		{
		//			addErrorToVerificationResult(REGION, MISSING, errorList);
		//		}
		if (address.getRegion() != null && StringUtils.length(address.getRegion().getIsocode()) > 255)
		{
			addErrorToVerificationResult(REGION, INVALID, errorList);
		}
		else if (StringUtils.length(address.getCountry().getIsocode()) > 255)
		{
			addErrorToVerificationResult(COUNTRY, INVALID, errorList);
		}
		else if (StringUtils.length(address.getAddressName()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_NAME, INVALID, errorList);
		}
		// Check if region belongs to address country
		//		if (Objects.isNull(address.getRegion()) || !address.getCountry().equals(address.getRegion().getCountry()))
		//		{
		//			addErrorToVerificationResult(REGION, INVALID, errorList);
		//		}
		//TODO MNasro
		//Check if city belongs to address region.
		//		if (address.getCity() != null && !address.getCity().getRegion().equals(address.getRegion()))
		//		{
		//			addErrorToVerificationResult(CITY, INVALID, errorList);
		//		}

		result.setFieldErrors(errorList);
	}

	protected void addErrorToVerificationResult(final String titleCode, final String missing,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errors)
	{
		final AddressFieldErrorData<AddressFieldType, AddressErrorCode> errorData = createFieldError();
		errorData.setFieldType(AddressFieldType.lookup(titleCode));
		errorData.setErrorCode(AddressErrorCode.lookup(missing));
		errors.add(errorData);
	}

	protected AddressFieldErrorData<AddressFieldType, AddressErrorCode> createFieldError()
	{
		return new AddressFieldErrorData<AddressFieldType, AddressErrorCode>();
	}

	protected AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> createVerificationResult()
	{
		return new AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>>();
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

}
