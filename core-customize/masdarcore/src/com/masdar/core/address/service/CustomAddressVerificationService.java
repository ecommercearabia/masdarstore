/**
 *
 */
package com.masdar.core.address.service;

import de.hybris.platform.commerceservices.address.AddressErrorCode;
import de.hybris.platform.commerceservices.address.AddressFieldType;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.address.AddressVerificationService;
import de.hybris.platform.commerceservices.address.data.AddressFieldErrorData;
import de.hybris.platform.commerceservices.address.data.AddressVerificationResultData;
import de.hybris.platform.core.model.user.AddressModel;


/**
 * @author monzer
 *
 */
public interface CustomAddressVerificationService<DECISION, FIELD_ERROR> extends AddressVerificationService<DECISION, FIELD_ERROR>
{

	public AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> verifyB2CAddress(
			final AddressModel addressModel);

}
