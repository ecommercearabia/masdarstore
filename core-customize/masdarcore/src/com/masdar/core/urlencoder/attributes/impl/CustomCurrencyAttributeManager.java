/**
 *
 */
package com.masdar.core.urlencoder.attributes.impl;

import de.hybris.platform.acceleratorservices.urlencoder.attributes.impl.DefaultCurrencyAttributeManager;
import de.hybris.platform.commerceservices.model.process.QuoteProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.returns.model.ReturnProcessModel;

import java.util.Optional;

import javax.annotation.Resource;


/**
 * @author mnasro
 *
 */
public class CustomCurrencyAttributeManager extends DefaultCurrencyAttributeManager
{
	@Resource(name = "quoteService")
	private QuoteService quoteService;


	/**
	 * @return the quoteService
	 */
	protected QuoteService getQuoteService()
	{
		return quoteService;
	}

	@Override
	public String getAttributeValueForEmail(final BusinessProcessModel businessProcessModel)
	{
		if (businessProcessModel instanceof StoreFrontCustomerProcessModel)
		{
			return ((StoreFrontCustomerProcessModel) businessProcessModel).getCurrency().getIsocode();
		}
		else if (businessProcessModel instanceof OrderProcessModel)
		{
			return ((OrderProcessModel) businessProcessModel).getOrder().getCurrency().getIsocode();
		}
		else if (businessProcessModel instanceof ConsignmentProcessModel)
		{
			return ((ConsignmentProcessModel) businessProcessModel).getConsignment().getOrder().getCurrency().getIsocode();
		}
		else if (businessProcessModel instanceof ReturnProcessModel)
		{
			return ((ReturnProcessModel) businessProcessModel).getReturnRequest().getOrder().getCurrency().getIsocode();
		}
		else if (businessProcessModel instanceof QuoteProcessModel)
		{
			final QuoteModel quote = getQuote(((QuoteProcessModel) businessProcessModel));
			return quote == null ? getDefaultValue() : quote.getCurrency().getIsocode();
		}
		return getDefaultValue();
	}

	protected QuoteModel getQuote(final QuoteProcessModel quoteProcessModel)
	{
		return Optional.of(quoteProcessModel).map(QuoteProcessModel::getQuoteCode).map(getQuoteService()::getCurrentQuoteForCode)
				.orElseThrow();
	}
}
