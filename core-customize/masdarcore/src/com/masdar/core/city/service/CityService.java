/**
 *
 */
package com.masdar.core.city.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.CityModel;


/**
 * The Interface CityService.
 *
 * @author mnasro
 */
public interface CityService
{
	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	public Optional<List<CityModel>> getByRegionIsocode(final String isoCode);


	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	public Optional<List<CityModel>> getAll();


	/**
	 * Gets the city by code
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	public Optional<CityModel> get(final String code);

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	public Optional<List<CityModel>> getByCountryIsocode(final String isoCode);

	/**
	 * Gets the warehouse cities.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the warehouse cities
	 */
	public List<CityModel> getWarehouseCities(BaseStoreModel baseStoreModel);

	/**
	 * Gets the warehouses cities by current base store.
	 *
	 * @return the warehouses cities by current base store
	 */
	public List<CityModel> getWarehousesCitiesByCurrentBaseStore();

}
