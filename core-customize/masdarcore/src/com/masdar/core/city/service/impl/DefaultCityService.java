/**
 *
 */
package com.masdar.core.city.service.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.masdar.core.city.dao.CityDao;
import com.masdar.core.city.service.CityService;
import com.masdar.core.model.CityModel;


/**
 * The Class DefaultCityService.
 *
 * @author mnasro
 */
public class DefaultCityService implements CityService
{
	private static final Logger LOG = Logger.getLogger(DefaultCityService.class);
	/** The city dao. */
	@Resource(name = "cityDao")
	private CityDao cityDao;

	/** The session service. */
	@Resource(name = "sessionService")
	private SessionService sessionService;
	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}


	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}


	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	protected CityDao getCityDao()
	{
		return cityDao;
	}


	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityModel>> getByRegionIsocode(final String isoCode)
	{

		final Optional<List<CityModel>> data = getCityDao().findCitiesByRegionIsocode(isoCode);

		return data.isEmpty() || CollectionUtils.isEmpty(data.get()) ? Optional.empty()
				: Optional.ofNullable(getCities(data.get()));
	}




	/**
	 * Gets the city
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	@Override
	public Optional<CityModel> get(final String code)
	{
		final Optional<CityModel> find = getCityDao().find(code);
		return find.isEmpty() || !checkCity(find.get(), getCurrentSiteChannel()) ? Optional.empty() : find;
	}

	/**
	 * Gets the all city
	 *
	 * @return the all city
	 */
	@Override
	public Optional<List<CityModel>> getAll()
	{

		final Optional<List<CityModel>> data = getCityDao().findAll();

		return data.isEmpty() || CollectionUtils.isEmpty(data.get()) ? Optional.empty()
				: Optional.ofNullable(getCities(data.get()));
	}

	/**
	 * Gets the by country isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the by country isocode
	 */
	@Override
	public Optional<List<CityModel>> getByCountryIsocode(final String isoCode)
	{

		final Optional<List<CityModel>> data = getCityDao().findCitesByIsocode(isoCode);

		return data.isEmpty() || CollectionUtils.isEmpty(data.get()) ? Optional.empty()
				: Optional.ofNullable(getCities(data.get()));
	}


	@Override
	public List<CityModel> getWarehouseCities(final BaseStoreModel baseStoreModel)
	{

		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel is null!");
		}

		if (CollectionUtils.isEmpty(baseStoreModel.getWarehouses()))
		{
			LOG.warn("base Store Model Have No Warehouses");
			return Collections.emptyList();
		}
		final Set<CityModel> cities = new HashSet<>();


		for (final WarehouseModel model : baseStoreModel.getWarehouses())
		{
			if (CollectionUtils.isEmpty(model.getCity()))
			{
				continue;
			}
			cities.addAll(model.getCity());
		}
		return getCities(new ArrayList<>(cities));
	}


	@Override
	public List<CityModel> getWarehousesCitiesByCurrentBaseStore()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

		return getWarehouseCities(currentBaseStore);

	}

	/**
	 * @param list
	 * @return
	 */
	private List<CityModel> getCities(final List<CityModel> list)
	{

		final SiteChannel siteChannel = getCurrentSiteChannel();
		return list.stream().filter(city -> checkCity(city, siteChannel)).collect(Collectors.toList());
	}

	/**
	 * @return
	 */
	private SiteChannel getCurrentSiteChannel()
	{
		return getCmsSiteService().getCurrentSite() == null || getCmsSiteService().getCurrentSite().getChannel() == null ? null
				: getCmsSiteService().getCurrentSite().getChannel();
	}


	private boolean checkCity(final CityModel city, final SiteChannel siteChannel)
	{
		if (siteChannel == null)
		{
			return true;
		}
		if (city == null)
		{
			return false;
		}
		if (SiteChannel.B2C.equals(siteChannel))
		{
			return city.isActiveOnB2C();
		}
		else
		{
			return city.isActiveOnB2B();
		}

	}
}
