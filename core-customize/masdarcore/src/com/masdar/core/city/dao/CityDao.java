
package com.masdar.core.city.dao;

import java.util.List;
import java.util.Optional;

import com.masdar.core.model.CityModel;


/**
 * The Interface CityDao.
 *
 * @author mnasro
 * @author monzer
 */
public interface CityDao
{

	/**
	 * Find cites by isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the list
	 */
	public Optional<List<CityModel>> findCitiesByRegionIsocode(String isoCode);

	/**
	 * Find city
	 *
	 * @param code
	 *           the code
	 * @return the city model
	 */
	public Optional<CityModel> find(String code);

	/**
	 * Find list of city
	 *
	 * @return the list
	 */
	public Optional<List<CityModel>> findAll();

	/**
	 * Find cites by isocode.
	 *
	 * @param isoCode
	 *           the iso code
	 * @return the list
	 */
	public Optional<List<CityModel>> findCitesByIsocode(String isoCode);



}
