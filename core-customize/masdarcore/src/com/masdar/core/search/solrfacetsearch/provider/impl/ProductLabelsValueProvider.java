package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;


/**
 * This ValueProvider will provide a boolean flag to indicate if this product is in stock. This can be used to sort the
 * search results by in stock status.
 */
public class ProductLabelsValueProvider extends AbstractProductShipmentStockValueProvider
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String PRODUCT_LABEL_B2C_Site_Channel_ATTR = "MasdarClassification/1.0/MasdarHiddenClassification.productlabels";
	private static final String PRODUCT_LABEL_B2B_Site_Channel_ATTR = "Masdarb2cClassification/1.0/MasdarHiddenb2cClassification.productlabels";

	@Override
	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();


		addFieldValues(fieldValues, indexedProperty, getAllProductLabels(product));

		return fieldValues;
	}

	private List<String> getAllProductLabels(final ProductModel product)
	{
		final List<ProductModel> products = getVariantsProducts(product);

		final List<String> labels = new ArrayList<>();

		labels.addAll(getProductLabels(product));

		for (final ProductModel productModel : products)
		{
			labels.addAll(getProductLabels(productModel));
		}

		return labels;

	}

	private List<String> getProductLabels(final ProductModel source)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features == null)
		{
			return Collections.emptyList();
		}

		final Feature collectionFeatureLabel = features.getFeatureByCode(PRODUCT_LABEL_B2B_Site_Channel_ATTR);
		if (collectionFeatureLabel == null || CollectionUtils.isEmpty(collectionFeatureLabel.getValues()))
		{
			return Collections.emptyList();
		}
		final List<FeatureValue> values = collectionFeatureLabel.getValues();
		final List<String> labels = new ArrayList<>();
		for (final FeatureValue featureValue : values)
		{

			labels.add(featureValue.getValue().toString());
		}

		return labels;

	}

}
