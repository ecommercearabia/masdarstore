package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * This ValueProvider will provide a boolean flag to indicate if this product is in stock. This can be used to sort the
 * search results by in stock status.
 */
public class ProductWarehouseStockValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{


	private FieldNameProvider fieldNameProvider;
	private CommonI18NService commonI18NService;

	@Resource(name = "stockService")
	private StockService stockService;


	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Autowired
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Autowired
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (!(model instanceof ProductModel))
		{
			throw new FieldValueProviderException("Cannot get WarehouseStock status of non-product item");
		}

		if (Objects.isNull(indexConfig.getBaseSite()) || CollectionUtils.isEmpty(indexConfig.getBaseSite().getStores()))
		{
			throw new FieldValueProviderException("Cannot find store");
		}
		final BaseStoreModel baseStoreModel = indexConfig.getBaseSite().getStores().get(0);
		final ProductModel product = (ProductModel) model;
		final Set<WarehouseModel> warehouses = getWarehousesForStore(baseStoreModel);

		if (CollectionUtils.isEmpty(warehouses))
		{
			throw new FieldValueProviderException("No warehouses for basestore");
		}
		return createFieldValue(product, indexedProperty, warehouses);
	}

	/**
	 * @param baseStoreModel
	 * @return
	 */
	protected Set<WarehouseModel> getWarehousesForStore(final BaseStoreModel baseStoreModel)
	{
		return new HashSet<>(baseStoreModel.getWarehouses());
	}

	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexedProperty indexedProperty,
			final Set<WarehouseModel> warehouses)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();

		final List<ProductModel> products = new ArrayList<>();

		getVariantsProducts(product, products);




		for (final String warehouseCode : getWarehousesCode(products, warehouses))
		{
			addFieldValues(fieldValues, indexedProperty, warehouseCode);
		}

		return fieldValues;

	}


	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty, final Object value)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}


	/**
	 * @return the stockService
	 */
	protected StockService getStockService()
	{
		return stockService;
	}

	protected List<String> getWarehousesCode(final List<ProductModel> products, final Set<WarehouseModel> warehouses)
	{
		final List<String> codes = new LinkedList<>();
		for (final ProductModel product : products)
		{
			codes.addAll(
					getStockService().getAllStockLevels(product).stream()
							.filter(s -> (InStockStatus.FORCEINSTOCK.equals(s.getInStockStatus()) || s.getAvailable() > 0)
									&& warehouses.contains(s.getWarehouse()))
							.map(s -> s.getWarehouse().getCode()).collect(Collectors.toList()));
		}
		return codes;
	}

	protected void getVariantsProducts(final ProductModel product, final List<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);

		}

	}




}
