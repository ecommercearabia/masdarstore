/**
 *
 */
package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author monzer
 *
 */
public abstract class AbstractProductShipmentStockValueProvider extends AbstractProductVariantLastLevelValueProvider
{
	@Resource(name = "stockPopulator")
	private Populator<ProductModel, StockData> stockPopulator;

	/**
	 * @return the stockPopulator
	 */
	public Populator<ProductModel, StockData> getStockPopulator()
	{
		return stockPopulator;
	}

	protected StockData getStockData(final ProductModel product)
	{
		final StockData stock = new StockData();
		getStockPopulator().populate(product, stock);
		return stock;
	}

}
