package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;

import java.util.ArrayList;
import java.util.List;


/**
 * This ValueProvider will provide a boolean flag to indicate if this product is in stock. This can be used to sort the
 * search results by in stock status.
 */
public class ProductDeliveryShipmentStockValueProvider extends AbstractProductShipmentStockValueProvider
{

	@Override
	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();

		final List<ProductModel> products = getVariantsProducts(product);

		addFieldValues(fieldValues, indexedProperty, isDeliveryShipmentStockAvailable(products));

		return fieldValues;
	}

	/**
	 * @param p
	 * @return
	 */
	private boolean isDeliveryShipmentStockAvailable(final List<ProductModel> products)
	{
		for (final ProductModel product : products)
		{
			if (!StockLevelStatus.OUTOFSTOCK.equals(getStockData(product).getStockLevelStatus()))
			{
				return true;
			}
		}
		return false;
	}

}
