package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * This ValueProvider will provide a boolean flag to indicate if this product is in stock. This can be used to sort the
 * search results by in stock status.
 */
public abstract class AbstractProductVariantLastLevelValueProvider extends AbstractPropertyFieldValueProvider
		implements FieldValueProvider
{

	private FieldNameProvider fieldNameProvider;
	private CommonI18NService commonI18NService;

	@Resource(name = "stockService")
	private StockService stockService;

	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Autowired
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Autowired
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (!(model instanceof ProductModel))
		{
			throw new FieldValueProviderException("Cannot get WarehouseStock status of non-product item");
		}

		if (Objects.isNull(indexConfig.getBaseSite()) || CollectionUtils.isEmpty(indexConfig.getBaseSite().getStores()))
		{
			throw new FieldValueProviderException("Cannot find store");
		}
		final ProductModel product = (ProductModel) model;

		return createFieldValue(product, indexedProperty);
	}

	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty,
			final Object available)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, available));
		}
	}

	protected abstract List<FieldValue> createFieldValue(final ProductModel product, final IndexedProperty indexedProperty);

	private void getVariantsProducts(final ProductModel product, final List<ProductModel> list)
	{
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			list.add(product);
			return;
		}

		for (final ProductModel p : product.getVariants())
		{
			getVariantsProducts(p, list);
		}
	}

	protected List<ProductModel> getVariantsProducts(final ProductModel product)
	{
		if (product == null)
		{
			return Collections.emptyList();
		}
		final List<ProductModel> products = new ArrayList<>();

		getVariantsProducts(product, products);
		return products;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the categoryService
	 */
	public CategoryService getCategoryService()
	{
		return categoryService;
	}

}
