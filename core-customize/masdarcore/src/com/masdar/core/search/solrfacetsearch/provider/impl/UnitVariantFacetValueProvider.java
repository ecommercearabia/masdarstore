/**
 *
 */
package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.masdar.masdarproduct.model.UnitVariantProductModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UnitVariantFacetValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{
	private FieldNameProvider fieldNameProvider;
	private CommonI18NService commonI18NService;

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Autowired
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Autowired
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected List<FieldValue> createFieldValue(final UnitVariantProductModel unitVariantProduct, final LanguageModel language,
			final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();

		if (language != null)
		{
			final Object value = unitVariantProduct.getUnitVariant(getCommonI18NService().getLocaleForLanguage(language));

			final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
			for (final String fieldName : fieldNames)
			{
				fieldValues.add(new FieldValue(fieldName, value));
			}
		}
		else
		{
			final Object value = unitVariantProduct.getUnitVariant();
			final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
			for (final String fieldName : fieldNames)
			{
				fieldValues.add(new FieldValue(fieldName, value));
			}
		}

		return fieldValues;
	}

	protected ProductModel getProductModel(final Object model)
	{

		if (model instanceof ProductModel)
		{
			return (ProductModel) model;
		}
		else
		{
			return null;
		}
	}

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final ProductModel productModel = getProductModel(model);
		if (productModel == null)
		{
			return Collections.emptyList();
		}
		final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

		if (!CollectionUtils.isEmpty(productModel.getVariants()))
		{
			// is base
			for (final VariantProductModel firstLevelVariant : productModel.getVariants())
			{
				if (!CollectionUtils.isEmpty(firstLevelVariant.getVariants()))
				{
					if (indexedProperty.isLocalized())
					{
						final Collection<LanguageModel> languages = indexConfig.getLanguages();
						for (final LanguageModel language : languages)
						{
							firstLevelVariant.getVariants().forEach(v -> addFieldValue(fieldValues, language, indexedProperty, v));
						}
					}
					else
					{
						firstLevelVariant.getVariants().forEach(v -> addFieldValue(fieldValues, null, indexedProperty, v));
					}
				}
			}
		}

		return fieldValues;
	}

	private void addFieldValue(final Collection<FieldValue> fieldValues, final LanguageModel language,
			final IndexedProperty indexedProperty, final VariantProductModel variantProduct)
	{
		if (variantProduct instanceof UnitVariantProductModel)
		{
			final UnitVariantProductModel unitVariant = (UnitVariantProductModel) variantProduct;
			fieldValues.addAll(createFieldValue(unitVariant, language, indexedProperty));
		}
	}
}
