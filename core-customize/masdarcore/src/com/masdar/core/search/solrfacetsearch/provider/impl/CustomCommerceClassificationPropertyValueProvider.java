package com.masdar.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CommerceClassificationPropertyValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;


/**
 * This ValueProvider will provide the value for a classification attribute on a product.
 */
@SuppressWarnings("deprecation")
public class CustomCommerceClassificationPropertyValueProvider extends CommerceClassificationPropertyValueProvider
{
	final static Logger LOG = Logger.getLogger(CustomCommerceClassificationPropertyValueProvider.class);

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		if (model instanceof ProductModel)
		{
			final ProductModel productModel = (ProductModel) model;

			final List<ProductModel> lastVariants = getLastVariants(productModel);

			for (final ProductModel lastVariant : lastVariants)
			{
				try
				{

					final Collection<FieldValue> fieldValues2 = super.getFieldValues(indexConfig, indexedProperty, lastVariant);
					if (!CollectionUtils.isEmpty(fieldValues2))
					{
						fieldValues.addAll(fieldValues2);
					}
				}
				catch (final FieldValueProviderException e)
				{
					continue;
				}
			}
		}

		return fieldValues;

	}

	/**
	 *
	 */
	private List<ProductModel> getLastVariants(final ProductModel productModel)
	{

		final List<ProductModel> list = new LinkedList<ProductModel>();
		if (!CollectionUtils.isEmpty(productModel.getVariants()))
		{
			for (final VariantProductModel firstLevelVariant : productModel.getVariants())
			{
				if (!CollectionUtils.isEmpty(firstLevelVariant.getVariants()))
				{
					list.addAll(firstLevelVariant.getVariants());
				}
				else
				{
					list.addAll(productModel.getVariants());
				}
			}
		}
		else
		{
			list.add(productModel);
		}
		return list;
	}

}
