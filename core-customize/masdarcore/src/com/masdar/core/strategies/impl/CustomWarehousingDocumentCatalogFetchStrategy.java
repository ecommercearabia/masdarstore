package com.masdar.core.strategies.impl;

import de.hybris.platform.acceleratorservices.document.strategy.DocumentCatalogFetchStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.model.process.QuoteProcessModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;


public class CustomWarehousingDocumentCatalogFetchStrategy implements DocumentCatalogFetchStrategy
{
	@Resource(name = "quoteService")
	private QuoteService quoteService;


	/**
	 * @return the quoteService
	 */
	protected QuoteService getQuoteService()
	{
		return quoteService;
	}

	protected QuoteModel getQuote(final QuoteProcessModel quoteProcessModel)
	{
		return Optional.of(quoteProcessModel).map(QuoteProcessModel::getQuoteCode).map(getQuoteService()::getCurrentQuoteForCode)
				.orElseThrow();
	}

	public CatalogVersionModel fetch(final BusinessProcessModel businessProcessModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcessModel", businessProcessModel);
		AbstractOrderModel order = null;
		if (businessProcessModel instanceof OrderProcessModel)
		{
			order = ((OrderProcessModel) businessProcessModel).getOrder();
		}
		else if (businessProcessModel instanceof ConsignmentProcessModel)
		{
			order = ((ConsignmentProcessModel) businessProcessModel).getConsignment().getOrder();
		}
		else if (businessProcessModel instanceof ReturnProcessModel)
		{
			order = ((ReturnProcessModel) businessProcessModel).getReturnRequest().getOrder();
		}
		else if (businessProcessModel instanceof QuoteProcessModel)
		{
			order = getQuote(((QuoteProcessModel) businessProcessModel));
		}
		else if (businessProcessModel.getContextParameters().iterator().hasNext())
		{
			final BusinessProcessParameterModel param = businessProcessModel.getContextParameters().iterator().next();
			if ("ConsolidatedConsignmentModels".equals(param.getName()))
			{
				final List<ConsignmentModel> consignmentList = (List) param.getValue();
				if (CollectionUtils.isNotEmpty(consignmentList))
				{
					order = consignmentList.iterator().next().getOrder();
				}
			}
		}

		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		Assert.isTrue(order.getSite() instanceof CMSSiteModel, "No CMSSite found for the order");
		final List<ContentCatalogModel> contentCatalogs = ((CMSSiteModel) order.getSite()).getContentCatalogs();
		Assert.isTrue(CollectionUtils.isNotEmpty(contentCatalogs), "Catalog Version cannot be found for the order");
		return contentCatalogs.iterator().next().getActiveCatalogVersion();
	}
}
