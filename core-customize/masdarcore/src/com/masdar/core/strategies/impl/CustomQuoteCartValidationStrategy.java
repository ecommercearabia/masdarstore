/**
 *
 */
package com.masdar.core.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.strategies.impl.DefaultQuoteCartValidationStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomQuoteCartValidationStrategy extends DefaultQuoteCartValidationStrategy
{

	@Override
	public boolean validate(final AbstractOrderModel source, final AbstractOrderModel target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		if (isFoundOrderEntryQuote(source, target))
		{
			if (isFoundOrderQuote(source, target))
			{
				return validateOrderEntryQuote(source, target)
						&& ObjectUtils.compare(source.getTotalDiscounts(), target.getTotalDiscounts()) == 0;
			}
			else
			{
				return validateOrderEntryQuote(source, target);

			}
		}
		else
		{
			return validateOrderQuote(source, target);
		}
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private boolean validateOrderQuoteWithOrderEntryQuote(final AbstractOrderModel source, final AbstractOrderModel target)
	{
		if (ObjectUtils.compare(source.getTotalDiscounts(), target.getTotalDiscounts()) != 0)
		{
			return false;
		}
		return true;
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private boolean isFoundOrderQuote(final AbstractOrderModel source, final AbstractOrderModel target)
	{

		if (StringUtils.isBlank(source.getQuoteDiscountValuesInternal())
				|| StringUtils.isBlank(target.getQuoteDiscountValuesInternal()))
		{
			return false;
		}

		return true;

	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private boolean isFoundOrderEntryQuote(final AbstractOrderModel source, final AbstractOrderModel target)
	{
		if (CollectionUtils.size(source.getEntries()) != CollectionUtils.size(source.getEntries()))
		{
			return false;
		}

		for (int i = 0; i < source.getEntries().size(); i++)
		{
			final AbstractOrderEntryModel sourceEntry = source.getEntries().get(i);
			final AbstractOrderEntryModel targetEntry = target.getEntries().get(i);


			if (!StringUtils.isBlank(sourceEntry.getQuoteDiscountValuesInternal())
					|| !StringUtils.isBlank(targetEntry.getQuoteDiscountValuesInternal()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private boolean validateOrderEntryQuote(final AbstractOrderModel source, final AbstractOrderModel target)
	{
		return compareQuoteEntries(source.getEntries(), target.getEntries());
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private boolean validateOrderQuote(final AbstractOrderModel source, final AbstractOrderModel target)
	{
		if (ObjectUtils.compare(source.getSubtotal(), target.getSubtotal()) != 0
				|| ObjectUtils.compare(source.getTotalDiscounts(), target.getTotalDiscounts()) != 0)
		{
			return false;
		}

		return compareEntries(source.getEntries(), target.getEntries());
	}

	protected boolean compareQuoteEntries(final List<AbstractOrderEntryModel> sourceEntries,
			final List<AbstractOrderEntryModel> targetEntries)
	{
		if (CollectionUtils.size(sourceEntries) != CollectionUtils.size(targetEntries))
		{
			return false;
		}

		for (int i = 0; i < sourceEntries.size(); i++)
		{
			final AbstractOrderEntryModel sourceEntry = sourceEntries.get(i);
			final AbstractOrderEntryModel targetEntry = targetEntries.get(i);

			if (ObjectUtils.compare(sourceEntry.getEntryNumber(), targetEntry.getEntryNumber()) != 0
					|| !StringUtils.equals(sourceEntry.getProduct().getCode(), targetEntry.getProduct().getCode())
					|| ObjectUtils.compare(sourceEntry.getQuantity(), targetEntry.getQuantity()) != 0
					|| ObjectUtils.compare(sourceEntry.getBasePrice(), targetEntry.getBasePrice()) != 0
			//
			)
			{
				return false;
			}
		}

		return true;
	}

}
