/**
 *
 */
package com.masdar.core.strategies.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.selectivecartservices.strategies.SelectiveCartAddToCartStrategy;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * @author monzer
 *
 */
public class CustomSelectiveCartAddToCartStrategy extends SelectiveCartAddToCartStrategy
{
	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	@Override
	protected long getAvailableStockLevel(final ProductModel productModel, final PointOfServiceModel pointOfServiceModel)
	{
		if (getCmsSiteService().getCurrentSite() == null || !getCmsSiteService().getCurrentSite().isCustomerLocationEnabled())
		{
			if ((pointOfServiceModel == null && productModel.isDeliveryFlag())
					|| (pointOfServiceModel != null && !productModel.isPickupInStoreFlag()))
			{
				return 0l;
			}
			return super.getAvailableStockLevel(productModel, pointOfServiceModel);
		}

		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();

		if (!getCommerceStockService().isStockSystemEnabled(baseStore))
		{
			return getForceInStockMaxQuantity();
		}
		else
		{
			Long availableStockLevel = null;

			if (pointOfServiceModel == null && !productModel.isDeliveryFlag())
			{
				availableStockLevel = getCommerceStockService().getStockLevelForProductAndBaseStore(productModel, baseStore);

				final Long stockLevelForProductAndCityByCurrentCustomer = getCustomCommerceStockService()
						.getStockLevelForProductAndCityByCurrentCustomer(productModel);

				availableStockLevel = availableStockLevel == null
						|| stockLevelForProductAndCityByCurrentCustomer < availableStockLevel
								? stockLevelForProductAndCityByCurrentCustomer
								: availableStockLevel;
			}
			else if (pointOfServiceModel != null && productModel.isPickupInStoreFlag())
			{
				availableStockLevel = getCommerceStockService().getStockLevelForProductAndPointOfService(productModel,
						pointOfServiceModel);
			}
			else
			{
				availableStockLevel = 0l;
			}

			if (availableStockLevel == null)
			{
				return getForceInStockMaxQuantity();
			}
			else
			{
				return availableStockLevel.longValue();
			}
		}
	}


}
