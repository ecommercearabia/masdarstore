/**
 *
 */
package com.masdar.core.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.warehousing.sourcing.context.impl.FirstPosSelectionStrategy;

import com.masdar.core.enums.ShipmentType;


/**
 * @author monzer
 *
 */
public class PosSelectionByNameStrategy extends FirstPosSelectionStrategy
{

	@Override
	public PointOfServiceModel getPointOfService(final AbstractOrderModel abstractOrderModel, final WarehouseModel warehouseModel)
	{
		final boolean pickupCart = ShipmentType.PICKUP_IN_STORE.equals(abstractOrderModel.getShipmentType())
				&& abstractOrderModel.getDeliveryPointOfService() != null;

		final boolean posExistsOnWarehouse = warehouseModel.getPointsOfService().stream()
				.anyMatch(pos -> pos.getName() != null && abstractOrderModel.getDeliveryPointOfService() != null
						&& pos.getName().equals(abstractOrderModel.getDeliveryPointOfService().getName()));

		return pickupCart && posExistsOnWarehouse ? abstractOrderModel.getDeliveryPointOfService()
				: super.getPointOfService(abstractOrderModel, warehouseModel);
	}

}
