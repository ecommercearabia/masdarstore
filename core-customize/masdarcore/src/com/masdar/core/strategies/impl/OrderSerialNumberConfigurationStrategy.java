/**
 *
 */
package com.masdar.core.strategies.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.core.service.AbstractSerialNumberConfigurationService;
import com.masdar.core.strategies.SerialNumberConfigurationStrategy;

/**
 * @author monzer
 *
 */
public class OrderSerialNumberConfigurationStrategy implements SerialNumberConfigurationStrategy
{

	@Resource(name = "orderSerialNumberService")
	private AbstractSerialNumberConfigurationService orderSerialNumberService;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore()
	{
		return orderSerialNumberService.generateSerialNumberByCurrentStore();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		return orderSerialNumberService.generateSerialNumberForBaseStore(baseStore);
	}

}
