package com.masdar.core.strategies.impl;

import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BCartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import com.masdar.core.service.CustomCommerceStockService;


public class CustomB2BCartValidationStrategy extends DefaultB2BCartValidationStrategy
{
	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	@Override
	protected Long getStockLevel(final CartEntryModel cartEntryModel)
	{
		final ProductModel product = cartEntryModel.getProduct();
		final PointOfServiceModel pointOfService = cartEntryModel.getDeliveryPointOfService();

		if (hasPointOfService(cartEntryModel))
		{
			return getCommerceStockService().getStockLevelForProductAndPointOfService(product, pointOfService);
		}
		else
		{
			if (isEnabledCustomerLocation(cartEntryModel))
			{

				return getCustomCommerceStockService().getStockLevelForProductAndCity(product,
						cartEntryModel.getOrder().getSelectedCity());
			}
			else
			{
				return getCommerceStockService().getStockLevelForProductAndBaseStore(product,
						getBaseStoreService().getCurrentBaseStore());
			}
		}
	}

	/**
	 * @param cartEntryModel
	 * @return
	 */
	private boolean isEnabledCustomerLocation(final CartEntryModel cartEntryModel)
	{

		return cartEntryModel == null || cartEntryModel.getOrder() == null || cartEntryModel.getOrder().getSite() == null
				|| !cartEntryModel.getOrder().getSite().isCustomerLocationEnabled()
				|| cartEntryModel.getOrder().getSelectedCity() == null ? false : true;
	}
}
