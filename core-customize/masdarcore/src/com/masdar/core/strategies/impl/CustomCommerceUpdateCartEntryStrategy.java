/**
 *
 */
package com.masdar.core.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import com.masdar.core.service.CustomCommerceStockService;


/**
 * @author monzer
 *
 */
public class CustomCommerceUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy
{

	@Resource(name = "customCommerceStockService")
	private CustomCommerceStockService customCommerceStockService;

	/**
	 * @return the customCommerceStockService
	 */
	protected CustomCommerceStockService getCustomCommerceStockService()
	{
		return customCommerceStockService;
	}

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	@Override
	protected long getAvailableStockLevel(final ProductModel productModel, final PointOfServiceModel pointOfServiceModel)
	{
		if (getCmsSiteService().getCurrentSite() == null || !getCmsSiteService().getCurrentSite().isCustomerLocationEnabled())
		{
			if ((pointOfServiceModel == null && productModel.isDeliveryFlag())
					|| (pointOfServiceModel != null && !productModel.isPickupInStoreFlag()))
			{
				return 0l;
			}
			return super.getAvailableStockLevel(productModel, pointOfServiceModel);
		}

		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();

		if (!getCommerceStockService().isStockSystemEnabled(baseStore))
		{
			return getForceInStockMaxQuantity();
		}
		else
		{
			Long availableStockLevel = null;

			if (pointOfServiceModel == null && !productModel.isDeliveryFlag())
			{
				availableStockLevel = getCommerceStockService().getStockLevelForProductAndBaseStore(productModel, baseStore);

				final Long stockLevelForProductAndCityByCurrentCustomer = getCustomCommerceStockService()
						.getStockLevelForProductAndCityByCurrentCustomer(productModel);

				availableStockLevel = availableStockLevel == null
						|| stockLevelForProductAndCityByCurrentCustomer < availableStockLevel
								? stockLevelForProductAndCityByCurrentCustomer
								: availableStockLevel;
			}
			else if (pointOfServiceModel != null && productModel.isPickupInStoreFlag())
			{
				availableStockLevel = getCommerceStockService().getStockLevelForProductAndPointOfService(productModel,
						pointOfServiceModel);
			}
			else
			{
				availableStockLevel = 0l;
			}

			if (availableStockLevel == null)
			{
				return getForceInStockMaxQuantity();
			}
			else
			{
				return availableStockLevel.longValue();
			}
		}
	}


	@Override
	public CommerceCartModification updatePointOfServiceForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameters.getCart();
		final PointOfServiceModel pointOfServiceModel = parameters.getPointOfService();
		validateParameterNotNull(cartModel, "Cart model cannot be null");
		validateParameterNotNull(pointOfServiceModel, "PointOfService Model cannot be null");

		final AbstractOrderEntryModel entryToUpdate = getEntryForNumber(cartModel, (int) parameters.getEntryNumber());

		if (entryToUpdate == null)
		{
			throw new CommerceCartModificationException("Unknown entry number");
		}

		if (!isOrderEntryUpdatable(entryToUpdate))
		{
			throw new CommerceCartModificationException("Entry is not updatable");
		}

		final AbstractOrderEntryModel mergeTarget = getEntryMergeStrategy().getEntryToMerge(cartModel.getEntries(), entryToUpdate);
		if (mergeTarget != null)
		{
			return mergeEntries(entryToUpdate, mergeTarget, cartModel);
		}
		else
		{
			final CommerceCartModification modification = new CommerceCartModification();
			final long stockLevel = getAvailableStockLevel(entryToUpdate.getProduct(), parameters.getPointOfService());
			if (stockLevel == 0)
			{
				final AbstractOrderEntryModel cloneEntry = getModelService().clone(entryToUpdate);
				getModelService().detach(cloneEntry);
				getModelService().remove(entryToUpdate);
				modification.setEntry(cloneEntry);
				modification.setQuantityAdded(-cloneEntry.getQuantity().longValue());
				modification.setQuantity(0);
				modification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
			}
			else if (entryToUpdate.getProduct() == null || !entryToUpdate.getProduct().isPickupInStoreFlag())
			{
				final AbstractOrderEntryModel cloneEntry = getModelService().clone(entryToUpdate);
				getModelService().detach(cloneEntry);
				getModelService().remove(entryToUpdate);
				modification.setEntry(cloneEntry);
				modification.setQuantityAdded(cloneEntry.getQuantity().longValue());
				modification.setQuantity(0);
				modification.setStatusCode(CommerceCartModificationStatus.MOVED_FROM_POS_TO_STORE);
			}
			else if (stockLevel < entryToUpdate.getQuantity().longValue())
			{
				entryToUpdate.setQuantity(Long.valueOf(stockLevel));
				entryToUpdate.setDeliveryPointOfService(pointOfServiceModel);
				getModelService().save(entryToUpdate);
				modification.setEntry(entryToUpdate);
				modification.setQuantity(stockLevel);
				modification.setStatusCode(CommerceCartModificationStatus.LOW_STOCK);
			}
			else
			{
				entryToUpdate.setDeliveryPointOfService(pointOfServiceModel);
				getModelService().save(entryToUpdate);
				modification.setEntry(entryToUpdate);
				modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			}
			getModelService().refresh(cartModel);
			final CommerceCartParameter parameter = new CommerceCartParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);
			getCommerceCartCalculationStrategy().calculateCart(parameter);
			if (stockLevel != 0 && entryToUpdate.getProduct() != null && entryToUpdate.getProduct().isPickupInStoreFlag())
			{
				getModelService().refresh(entryToUpdate);
			}
			modification.setEntry(entryToUpdate);
			return modification;
		}
	}

	@Override
	public CommerceCartModification updateToShippingModeForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameters.getCart();

		final AbstractOrderEntryModel entryToUpdate = getEntryForNumber(cartModel, (int) parameters.getEntryNumber());
		final CommerceCartModification modification = new CommerceCartModification();

		validateEntryBeforeSetShippingMode(entryToUpdate);

		final AbstractOrderEntryModel matchingShippingModeEntry = getEntryMergeStrategy().getEntryToMerge(cartModel.getEntries(),
				entryToUpdate);
		if (matchingShippingModeEntry != null)
		{
			return mergeEntries(entryToUpdate, matchingShippingModeEntry, cartModel);
		}
		else
		{
			final long quantityBeforeChange = entryToUpdate.getQuantity().longValue();
			final long stockLevel = getAvailableStockLevel(entryToUpdate.getProduct(), parameters.getPointOfService());
			// Now limit that to the total available in stock
			final long newTotalQuantityAfterStockLimit = Math.min(quantityBeforeChange, stockLevel);

			entryToUpdate.setDeliveryPointOfService(null);
			entryToUpdate.setQuantity(Long.valueOf(newTotalQuantityAfterStockLimit));
			getModelService().save(entryToUpdate);
			getModelService().refresh(cartModel);
			final CommerceCartParameter parameter = new CommerceCartParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);
			getCommerceCartCalculationStrategy().calculateCart(parameter);

			if (stockLevel == 0)
			{
				final AbstractOrderEntryModel cloneEntry = getModelService().clone(entryToUpdate);
				getModelService().detach(cloneEntry);
				getModelService().remove(entryToUpdate);
				modification.setEntry(cloneEntry);
				modification.setQuantityAdded(-cloneEntry.getQuantity().longValue());
				modification.setQuantity(0);
				modification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
			}
			else if (entryToUpdate.getProduct() == null || entryToUpdate.getProduct().isDeliveryFlag())
			{
				final AbstractOrderEntryModel cloneEntry = getModelService().clone(entryToUpdate);
				getModelService().detach(cloneEntry);
				getModelService().remove(entryToUpdate);
				modification.setEntry(cloneEntry);
				modification.setQuantityAdded(cloneEntry.getQuantity().longValue());
				modification.setQuantity(0);
				modification.setStatusCode(CommerceCartModificationStatus.MOVED_FROM_POS_TO_STORE);
			}
			else
			{
				getModelService().refresh(cartModel);
				parameter.setEnableHooks(true);
				parameter.setCart(cartModel);
				getCommerceCartCalculationStrategy().calculateCart(parameter);
				if (stockLevel != 0 && entryToUpdate.getProduct() != null && entryToUpdate.getProduct().isPickupInStoreFlag())
				{
					getModelService().refresh(entryToUpdate);
				}
				modification.setEntry(entryToUpdate);
				return modification;
			}
			if (quantityBeforeChange == newTotalQuantityAfterStockLimit)
			{
				modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			}
			else
			{
				modification.setStatusCode(CommerceCartModificationStatus.LOW_STOCK);
			}
			modification.setQuantity(entryToUpdate.getQuantity().longValue());
			modification.setEntry(entryToUpdate);
		}

		return modification;
	}

	@Override
	protected void validateEntryBeforeSetShippingMode(final AbstractOrderEntryModel entryToUpdate)
			throws CommerceCartModificationException
	{
		if (entryToUpdate == null)
		{
			throw new CommerceCartModificationException("Unknown entry number");
		}
		if (!isOrderEntryUpdatable(entryToUpdate))
		{
			throw new CommerceCartModificationException("Entry is not updatable");
		}
	}
}
