package com.masdar.core.strategies.impl;

import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.order.price.DiscountInformation;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.order.strategies.calculation.pdt.FindPDTValueInfoStrategy;
import de.hybris.platform.order.strategies.calculation.pdt.criteria.DiscountValueInfoCriteria;
import de.hybris.platform.order.strategies.calculation.pdt.criteria.PDTCriteriaFactory;
import de.hybris.platform.product.BaseCriteria;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.google.common.math.DoubleMath;


public class DefaultFindOrderEntryQuoteDiscountValuesStrategy implements FindDiscountValuesStrategy
{
	@Resource(name = "findDiscountValueInfoStrategyExecutable")
	private FindPDTValueInfoStrategy<DiscountValue, DiscountInformation, DiscountValueInfoCriteria> findDiscountValueInfoStrategy;
	@Resource(name = "PDTCriteriaFactory")
	private PDTCriteriaFactory pdtCriteriaFactory;

	private static final double EPSILON = 0.01d;

	@Override
	public boolean isSLOnly()
	{
		return true;
	}

	@Override
	public List<DiscountValue> findDiscountValues(final AbstractOrderModel order) throws CalculationException
	{
		return Collections.emptyList();
	}

	@Override
	public List<DiscountValue> findDiscountValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (CollectionUtils.isEmpty(entry.getDiscountValues()))
		{
			return Collections.emptyList();
		}
		final List<DiscountValue> discountValues = new ArrayList<>();

		for (final DiscountValue discount : entry.getDiscountValues())
		{
			final double value = discount.getAppliedValue();
			if (DoubleMath.fuzzyCompare(value, 0, EPSILON) > 0
					&& CommerceServicesConstants.QUOTE_DISCOUNT_CODE.equals(discount.getCode()))
			{
				discountValues.add(discount);
			}
		}
		return discountValues;
	}

	@Override
	public List<DiscountInformation> getDiscountInformation(final BaseCriteria baseCriteria) throws CalculationException
	{
		return findDiscountValueInfoStrategy
				.getPDTInformation(pdtCriteriaFactory.discountInfoCriteriaFromBaseCriteria(baseCriteria));
	}
}
