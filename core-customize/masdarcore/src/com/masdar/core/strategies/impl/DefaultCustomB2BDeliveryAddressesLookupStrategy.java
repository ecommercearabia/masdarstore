package com.masdar.core.strategies.impl;

import de.hybris.platform.b2b.company.B2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BDeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;


public class DefaultCustomB2BDeliveryAddressesLookupStrategy extends DefaultB2BDeliveryAddressesLookupStrategy
{


	@Resource(name = "b2bCommerceUnitService")
	private B2BCommerceUnitService b2bCommerceUnitService;

	public boolean isCashCustomer()
	{
		final B2BUnitModel parentUnit = b2bCommerceUnitService.getParentUnit();

		return parentUnit != null && parentUnit.isCashCustomer();
	}

	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder, final boolean visibleAddressesOnly)
	{
		if (isCashCustomer())
		{
			return getFallbackDeliveryAddressesLookupStrategy().getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
		}
		if (CheckoutPaymentType.ACCOUNT.equals(abstractOrder.getPaymentType())
				|| CheckoutPaymentType.BANK.equals(abstractOrder.getPaymentType())
				|| CheckoutPaymentType.ESAL.equals(abstractOrder.getPaymentType()))
		{
			// Lookup the
			final B2BCostCenterModel costCenter = getCostCenterForOrder(abstractOrder);
			if (costCenter != null)
			{
				final Set<AddressModel> addresses = collectAddressesForCostCenter(costCenter);
				if (addresses != null && !addresses.isEmpty())
				{
					return sortAddresses(addresses);
				}
			}

			// Can't find any pay on account addresses yet - maybe the cost centre is not set yet?
			return Collections.emptyList();
		}
		else
		{
			// Use fallback
			return getFallbackDeliveryAddressesLookupStrategy().getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
		}
	}
}
