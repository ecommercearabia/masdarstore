/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.core.constants.MasdarCoreConstants;
import com.masdar.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class MasdarCoreManager extends GeneratedMasdarCoreManager
{
	public static final MasdarCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarCoreManager) em.getExtension(MasdarCoreConstants.EXTENSIONNAME);
	}
}
