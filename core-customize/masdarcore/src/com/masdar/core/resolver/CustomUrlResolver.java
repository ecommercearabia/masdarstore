/**
 *
 */
package com.masdar.core.resolver;

/**
 * @author mbaker
 *
 */
public interface CustomUrlResolver
{
	public String getUrl();
}
