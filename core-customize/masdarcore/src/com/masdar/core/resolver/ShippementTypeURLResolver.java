/**
 *
 */
package com.masdar.core.resolver;

/**
 * @author mbaker
 *
 */
public interface ShippementTypeURLResolver
{
	public String getSolarSearchQueryByCurrentCustomer(String sortCode);
}
