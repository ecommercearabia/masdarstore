/**
 *
 */
package com.masdar.core.resolver.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;

import com.masdar.core.beans.ShipmentTypeInfo;
import com.masdar.core.model.CityModel;
import com.masdar.core.resolver.ShippementTypeURLResolver;
import com.masdar.core.service.CustomUserService;
import com.masdar.core.service.CustomWarehouseService;


/**
 * @author mbaker
 *
 */
public class DefaultShippmentTypeResolver implements ShippementTypeURLResolver
{

	@Resource(name = "customUserService")
	private CustomUserService customUserService;

	@Resource(name = "customWarehouseService")
	private CustomWarehouseService customWarehouseService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	@Override
	public String getSolarSearchQueryByCurrentCustomer(final String sortCode)
	{


		final Optional<ShipmentTypeInfo> shipmentTypeInfoForCurrentUser = getCustomUserService()
				.getShipmentTypeInfoForCurrentUser();
		if (shipmentTypeInfoForCurrentUser.isEmpty())
		{
			return Strings.EMPTY;
		}
		final CityModel city = shipmentTypeInfoForCurrentUser.get().getCity();
		final Set<WarehouseModel> warehouses = getCustomWarehouseService().getActiveWarehousesForCityAndStore(city,
				getBaseStoreService().getCurrentBaseStore());

		if (Objects.isNull(city) || CollectionUtils.isEmpty(warehouses))
		{
			return Strings.EMPTY;
		}

		final StringBuilder stringBuilder = new StringBuilder();
		if (Strings.isNotBlank(sortCode))
		{
			stringBuilder.append(":").append(sortCode);
		}


		for (final WarehouseModel warehouse : warehouses)
		{
			stringBuilder.append(":warehouseStock:").append(warehouse.getCode());
		}
		return stringBuilder.toString();
	}

	/**
	 * @return the customUserService
	 */
	protected CustomUserService getCustomUserService()
	{
		return customUserService;
	}

	/**
	 * @return the customWareohuseService
	 */
	public CustomWarehouseService getCustomWarehouseService()
	{
		return customWarehouseService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}



}
