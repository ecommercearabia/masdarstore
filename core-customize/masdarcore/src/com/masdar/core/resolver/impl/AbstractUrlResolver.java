/**
 *
 */
package com.masdar.core.resolver.impl;

import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.masdar.core.resolver.CustomUrlResolver;


/**
 * @author mbaker
 *
 */
public abstract class AbstractUrlResolver implements CustomUrlResolver
{

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "urlEncoderService")
	private UrlEncoderService urlEncoderService;

	public abstract String getUrl();

	protected String getBaseUrl()
	{
		final CMSSiteModel baseSite = getCmsSiteService().getCurrentSite();
		if (Objects.isNull(baseSite))
		{
			return Strings.EMPTY;
		}
		final String currentUrlEncodingPattern = getUrlEncoderService().getCurrentUrlEncodingPattern();

		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(baseSite, currentUrlEncodingPattern, true, "");

	}

	/**
	 * @return the siteBaseUrlResolutionService
	 */
	protected SiteBaseUrlResolutionService getSiteBaseUrlResolutionService()
	{
		return siteBaseUrlResolutionService;
	}

	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the urlEncoderService
	 */
	protected UrlEncoderService getUrlEncoderService()
	{
		return urlEncoderService;
	}

}
