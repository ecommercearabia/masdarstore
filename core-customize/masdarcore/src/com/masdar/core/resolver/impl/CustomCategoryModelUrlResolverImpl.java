/**
 *
 */
package com.masdar.core.resolver.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;


/**
 * @author user2
 *
 */
public class CustomCategoryModelUrlResolverImpl extends DefaultCategoryModelUrlResolver
{

	private static final String FORCE_HYPERLINK_PROPERTY = "force.hyperlink.category.name";


	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected boolean isForceHyperlink()
	{
		return getConfigurationService().getConfiguration().getBoolean(FORCE_HYPERLINK_PROPERTY, false);
	}

	@Override
	protected String buildPathString(final List<CategoryModel> path)
	{
		if (path == null || path.isEmpty())
		{
			return "c"; // Default category part of path when missing category
		}

		final StringBuilder result = new StringBuilder();

		for (int i = 0; i < path.size(); i++)
		{
			if (i != 0)
			{
				result.append('/');
			}
			result.append(urlSafe(getSeoURLNameForCategory(path.get(i))));
		}

		return result.toString();
	}


	private String getSeoURLNameForCategory(final CategoryModel categoryModel)
	{
		if (categoryModel == null)
		{
			return "";
		}

		if (isForceHyperlink())
		{
			return StringUtils.isBlank(categoryModel.getSeoName(Locale.ENGLISH)) ? categoryModel.getName(Locale.ENGLISH)
					: categoryModel.getSeoName(Locale.ENGLISH);

		}
		else
		{
			return StringUtils.isBlank(categoryModel.getSeoName()) ? categoryModel.getName() : categoryModel.getSeoName();
		}

	}

	private String getSeoURLNameForProduct(final ProductModel productModel)
	{
		if (productModel == null)
		{
			return "";
		}
		return StringUtils.isBlank(productModel.getSeoName()) ? productModel.getName() : productModel.getSeoName();
	}

}
