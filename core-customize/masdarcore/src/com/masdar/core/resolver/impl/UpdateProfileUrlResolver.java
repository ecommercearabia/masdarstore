/**
 *
 */
package com.masdar.core.resolver.impl;

import org.apache.logging.log4j.util.Strings;

import com.masdar.core.resolver.CustomUrlResolver;


/**
 * @author mbaker
 *
 */
public class UpdateProfileUrlResolver extends AbstractUrlResolver implements CustomUrlResolver
{

	@Override
	public String getUrl()
	{
		final String baseUrl = getBaseUrl();
		if (Strings.isBlank(baseUrl))
		{
			return Strings.EMPTY;
		}
		return baseUrl.concat("/my-account/update-profile");
	}


}
