/**
 *
 */
package com.masdar.core.resolver.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;


/**
 * @author user2
 *
 */
public class CustomProductModelUrlResolverImpl extends DefaultProductModelUrlResolver
{
	private static final String FORCE_HYPERLINK_PROPERTY = "force.hyperlink.product.name";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	protected boolean isForceHyperlink()
	{
		return getConfigurationService().getConfiguration().getBoolean(FORCE_HYPERLINK_PROPERTY, false);
	}

	@Override
	protected String buildPathString(final List<CategoryModel> path)
	{
		if (path == null || path.isEmpty())
		{
			return "c"; // Default category part of path when missing category
		}

		final StringBuilder result = new StringBuilder();

		for (int i = 0; i < path.size(); i++)
		{
			if (i != 0)
			{
				result.append('/');
			}
			result.append(urlSafe(getSeoURLNameForCategory(path.get(i))));
		}

		return result.toString();
	}

	@Override
	protected String resolveInternal(final ProductModel source)
	{
		final ProductModel baseProduct = getProductAndCategoryHelper().getBaseProduct(source);

		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		String url = getPattern();

		if (currentBaseSite != null && url.contains("{baseSite-uid}"))
		{
			url = url.replace("{baseSite-uid}", urlEncode(currentBaseSite.getUid()));
		}
		if (url.contains("{category-path}"))
		{
			url = url.replace("{category-path}", buildPathString(getCategoryPath(baseProduct)));
		}

		if (url.contains("{product-name}"))
		{
			url = url.replace("{product-name}", urlSafe(getSeoURLNameForProduct(baseProduct)));
		}
		if (url.contains("{product-code}"))
		{
			url = url.replace("{product-code}", urlEncode(source.getCode()));
		}

		return url;
	}

	private String getSeoURLNameForCategory(final CategoryModel categoryModel)
	{
		if (categoryModel == null)
		{
			return "";
		}
		if (isForceHyperlink())
		{
			return StringUtils.isBlank(categoryModel.getSeoName(Locale.ENGLISH)) ? categoryModel.getName(Locale.ENGLISH)
					: categoryModel.getSeoName(Locale.ENGLISH);

		}
		else
		{
			return StringUtils.isBlank(categoryModel.getSeoName()) ? categoryModel.getName() : categoryModel.getSeoName();
		}

	}

	private String getSeoURLNameForProduct(final ProductModel productModel)
	{
		if (productModel == null)
		{
			return "";
		}
		if (isForceHyperlink())
		{
			return StringUtils.isBlank(productModel.getSeoName(Locale.ENGLISH)) ? productModel.getName(Locale.ENGLISH)
					: productModel.getSeoName(Locale.ENGLISH);

		}
		else
		{
			return StringUtils.isBlank(productModel.getSeoName()) ? productModel.getName() : productModel.getSeoName();
		}
	}

}
