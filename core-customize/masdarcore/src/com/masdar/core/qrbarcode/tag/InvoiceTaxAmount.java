package com.masdar.core.qrbarcode.tag;

import com.masdar.core.qrbarcode.Tag;


public class InvoiceTaxAmount extends Tag
{

	public InvoiceTaxAmount(final String value)
	{
		super(5, value);
	}

}
