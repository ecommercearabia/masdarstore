package com.masdar.core.qrbarcode.tag;

import com.masdar.core.qrbarcode.Tag;


public class InvoiceTotalAmount extends Tag
{

	public InvoiceTotalAmount(final String value)
	{
		super(4, value);
	}

}
