package com.masdar.core.qrbarcode.tag;

import com.masdar.core.qrbarcode.Tag;


public class TaxNumber extends Tag
{

	public TaxNumber(final String value)
	{
		super(2, value);
	}

}
