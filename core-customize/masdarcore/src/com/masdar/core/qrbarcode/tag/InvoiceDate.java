package com.masdar.core.qrbarcode.tag;

import com.masdar.core.qrbarcode.Tag;


public class InvoiceDate extends Tag
{

	public InvoiceDate(final String value)
	{
		super(3, value);
	}

}
