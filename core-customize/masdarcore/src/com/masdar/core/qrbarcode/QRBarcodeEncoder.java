package com.masdar.core.qrbarcode;

import java.util.Base64;

import com.masdar.core.qrbarcode.tag.InvoiceDate;
import com.masdar.core.qrbarcode.tag.InvoiceTaxAmount;
import com.masdar.core.qrbarcode.tag.InvoiceTotalAmount;
import com.masdar.core.qrbarcode.tag.Seller;
import com.masdar.core.qrbarcode.tag.TaxNumber;



public class QRBarcodeEncoder
{

	private QRBarcodeEncoder()
	{
		//Factory method pattern
	}

	public static String encode(final Seller seller, final TaxNumber taxNumber, final InvoiceDate invoiceDate,
			final InvoiceTotalAmount invoiceTotalAmount, final InvoiceTaxAmount invoiceTaxAmount)
	{
		return toBase64(toTLV(seller, taxNumber, invoiceDate, invoiceTotalAmount, invoiceTaxAmount));
	}

	private static String toTLV(final Seller seller, final TaxNumber taxNumber, final InvoiceDate invoiceDate,
			final InvoiceTotalAmount invoiceTotalAmount, final InvoiceTaxAmount invoiceTaxAmount)
	{
		return seller.toString() + taxNumber.toString() + invoiceDate.toString() + invoiceTotalAmount.toString()
				+ invoiceTaxAmount.toString();
	}

	private static String toBase64(final String tlvString)
	{
		return Base64.getEncoder().encodeToString(tlvString.getBytes());
	}

}
