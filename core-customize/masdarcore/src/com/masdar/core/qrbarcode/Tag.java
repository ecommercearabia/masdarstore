package com.masdar.core.qrbarcode;

public class Tag
{

	private final int tag;
	private final String value;

	public Tag(final int tag, final String value)
	{
		//		if (value == null || value.trim().equals(""))
		//		{
		//			throw new IllegalArgumentException("Value cannot be null or empty");
		//		}
		this.tag = tag;
		this.value = value;
	}

	private int getTag()
	{
		return this.tag;
	}

	private String getValue()
	{
		return this.value;
	}

	private int getLength()
	{
		return this.value == null || value.trim().equals("") ? 0 : this.value.getBytes().length;
	}

	private String toHex(final int value)
	{
		final String hex = String.format("%02X", value);
		final String input = hex.length() % 2 == 0 ? hex : hex + "0";
		final StringBuilder output = new StringBuilder();
		for (int i = 0; i < input.length(); i += 2)
		{
			final String str = input.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return output.toString();
	}

	@Override
	public String toString()
	{
		return this.value == null || value.trim().equals("") ? ""
				: this.toHex(this.getTag()) + this.toHex(this.getLength()) + (this.getValue());
	}

}
