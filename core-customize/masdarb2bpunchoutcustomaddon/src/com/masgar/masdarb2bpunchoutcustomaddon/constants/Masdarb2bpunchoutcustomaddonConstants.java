/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masgar.masdarb2bpunchoutcustomaddon.constants;

/**
 * Global class for all Masdarb2bpunchoutcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bpunchoutcustomaddonConstants extends GeneratedMasdarb2bpunchoutcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarb2bpunchoutcustomaddon";
	public static final String PUNCHOUT_USER = "punchoutUser";
	public static final String VIEW_PAGE_PREFIX = "addon:/masdarb2bpunchoutcustomaddon";
	public static final String SEAMLESS_PAGE = "seamlessPage";

	private Masdarb2bpunchoutcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
