/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masgar.masdarb2bpunchoutcustomaddon.constants;

/**
 * Global class for all Masdarb2bpunchoutcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bpunchoutcustomaddonWebConstants // NOSONAR
{

	private Masdarb2bpunchoutcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
