/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masgar.masdarb2bpunchoutcustomaddon.controllers;

/**
 */
public interface B2BPunchoutAddonControllerConstants
{
	// implement here controller constants used by this extension
}
