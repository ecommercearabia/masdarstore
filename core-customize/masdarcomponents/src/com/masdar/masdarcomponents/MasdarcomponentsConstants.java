/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcomponents;

import com.masdar.masdarcomponents.constants.GeneratedMasdarcomponentsConstants;

/**
 * Global class for all Masdarcomponents constants. You can add global constants for your extension into this class.
 */
public final class MasdarcomponentsConstants extends GeneratedMasdarcomponentsConstants
{
	public static final String EXTENSIONNAME = "masdarcomponents";

	private MasdarcomponentsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarcomponentsPlatformLogo";
}
