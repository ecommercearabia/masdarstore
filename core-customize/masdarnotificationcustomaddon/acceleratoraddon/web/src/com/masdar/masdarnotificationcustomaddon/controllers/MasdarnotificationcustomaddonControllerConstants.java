/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomaddon.controllers;

/**
 */
public interface MasdarnotificationcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
