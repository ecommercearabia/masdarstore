/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomaddon.constants;

/**
 * Global class for all Masdarnotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarnotificationcustomaddonWebConstants // NOSONAR
{
	private MasdarnotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
