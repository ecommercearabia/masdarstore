/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomaddon.constants;

/**
 * Global class for all Masdarnotificationcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class MasdarnotificationcustomaddonConstants extends GeneratedMasdarnotificationcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarnotificationcustomaddon";

	private MasdarnotificationcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
