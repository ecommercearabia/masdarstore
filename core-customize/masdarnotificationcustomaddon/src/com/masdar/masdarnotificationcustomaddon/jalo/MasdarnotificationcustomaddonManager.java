/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.masdarnotificationcustomaddon.constants.MasdarnotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class MasdarnotificationcustomaddonManager extends GeneratedMasdarnotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarnotificationcustomaddonManager.class.getName() );
	
	public static final MasdarnotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarnotificationcustomaddonManager) em.getExtension(MasdarnotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
