/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarassistedservicepromotioncustomaddon.jalo;

import com.masdar.masdarassistedservicepromotioncustomaddon.constants.MasdarassistedservicepromotioncustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarassistedservicepromotioncustomaddonManager extends GeneratedMasdarassistedservicepromotioncustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarassistedservicepromotioncustomaddonManager.class.getName() );
	
	public static final MasdarassistedservicepromotioncustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarassistedservicepromotioncustomaddonManager) em.getExtension(MasdarassistedservicepromotioncustomaddonConstants.EXTENSIONNAME);
	}
	
}
