/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.masdar.masdarcustomersupportbackoffice.services;

/**
 * Hello World MasdarcustomersupportbackofficeService
 */
public class MasdarcustomersupportbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
