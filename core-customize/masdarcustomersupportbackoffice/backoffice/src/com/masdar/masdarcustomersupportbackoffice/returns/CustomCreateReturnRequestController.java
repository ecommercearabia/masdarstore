/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.returns;

import de.hybris.platform.omsbackoffice.widgets.returns.createreturnrequest.CreateReturnRequestController;
import de.hybris.platform.omsbackoffice.widgets.returns.dtos.ReturnEntryToCreateDto;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.zkoss.zul.Row;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCreateReturnRequestController extends CreateReturnRequestController
{

	@Override
	protected void calculateRowAmount(final Row myRow, final ReturnEntryToCreateDto myReturnEntry, final int qtyEntered)
	{
		final BigDecimal newAmount = BigDecimal.valueOf(myReturnEntry.getRefundEntry().getOrderEntry().getBasePrice() * qtyEntered);
		myReturnEntry.setQuantityToReturn(qtyEntered);
		myReturnEntry.getRefundEntry().setAmount(newAmount);
		this.applyToRow(newAmount.setScale(2, RoundingMode.HALF_EVEN).doubleValue(), 7, myRow);
		this.calculateIndividualTaxEstimate(myReturnEntry);
		this.calculateTotalRefundAmount();
	}
}
