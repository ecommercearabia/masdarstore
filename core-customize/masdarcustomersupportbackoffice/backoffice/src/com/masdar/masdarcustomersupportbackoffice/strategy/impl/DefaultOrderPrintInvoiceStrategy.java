/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.strategy.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;
import de.hybris.platform.warehousing.process.WarehousingBusinessProcessService;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.util.Clients;

import com.hybris.cockpitng.labels.LabelUtils;
import com.masdar.masdarcustomersupportbackoffice.strategy.OrderPrintDocumentStrategy;

/**
 * @author monzer
 *
 */
public class DefaultOrderPrintInvoiceStrategy implements OrderPrintDocumentStrategy
{
	protected static final String FRONTEND_TEMPLATENAME = "OrderInvoiceDocumentTemplate";
	protected static final String POPUP_WIDTH = "800";
	protected static final String POPUP_HEIGHT = "800";
	protected static final String BLOCKED_POPUP_MESSAGE = "blockedpopupmessage";
	private PrintMediaService printMediaService;
	private WarehousingBusinessProcessService<AbstractOrderModel> orderBusinessProcessService;

	public void printDocument(final OrderModel orderModel)
	{
		ServicesUtil.validateParameterNotNull(orderModel, "orderModel cannot be null");

		final String processCode = orderBusinessProcessService.getProcessCode(orderModel);

		final MediaModel orderInvoiceMedia = this.getPrintMediaService().getMediaForTemplate(FRONTEND_TEMPLATENAME,
				orderBusinessProcessService.getProcess(processCode));
		final String popup = this.getPrintMediaService().generatePopupScriptForMedia(orderInvoiceMedia, "800", "800",
				this.resolveLabel(BLOCKED_POPUP_MESSAGE));
		Clients.evalJavaScript(popup);
	}

	protected String resolveLabel(final String labelKey)
	{
		final String defaultValue = LabelUtils.getFallbackLabel(labelKey);
		return Labels.getLabel(labelKey, defaultValue);
	}

	protected PrintMediaService getPrintMediaService()
	{
		return this.printMediaService;
	}

	@Required
	public void setPrintMediaService(final PrintMediaService printMediaService)
	{
		this.printMediaService = printMediaService;
	}

	/**
	 * @return the orderBusinessProcessService
	 */
	protected WarehousingBusinessProcessService<AbstractOrderModel> getOrderBusinessProcessService()
	{
		return orderBusinessProcessService;
	}

	/**
	 * @param orderBusinessProcessService
	 *           the orderBusinessProcessService to set
	 */
	@Required
	public void setOrderBusinessProcessService(
			final WarehousingBusinessProcessService<AbstractOrderModel> orderBusinessProcessService)
	{
		this.orderBusinessProcessService = orderBusinessProcessService;
	}

}