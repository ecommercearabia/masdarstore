/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.strategy;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author monzer
 *
 *
 */
public interface OrderPrintDocumentStrategy {
	void printDocument(OrderModel orderModel);
}