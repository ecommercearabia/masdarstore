/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.comments.model.CommentAttachmentModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.media.MediaService;

import javax.annotation.Resource;

import org.zkoss.zkmax.zul.Filedownload;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author monzer
 *
 */
public class POAttachmentAction extends AbstractComponentWidgetAdapterAware implements CockpitAction<OrderModel, OrderModel>
{

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Override
	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();
		if (orderModel == null)
		{
			return false;
		}
		final CommentAttachmentModel paymentTypeAttachment = orderModel.getPaymentTypeAttachment();
		return (paymentTypeAttachment != null ? true : false);
	}

	@Override
	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();

		final CommentAttachmentModel paymentTypeAttachment = orderModel.getPaymentTypeAttachment();
		if (paymentTypeAttachment == null)
		{
			return new ActionResult("fail");
		}

		final MediaModel attachment = (MediaModel) paymentTypeAttachment.getItem();
		if (attachment != null && attachment.getDownloadURL() != null)
		{
			final byte[] data = mediaService.getDataFromMedia(attachment);
			Filedownload.save(data, null, attachment.getRealFileName());
			return new ActionResult("success");
		}
		return new ActionResult("fail");
	}

}
