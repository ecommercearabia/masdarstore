/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.b2b.process.approval.model.B2BApprovalProcessModel;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.masdar.Ordermanagement.constants.MasdarOrdermanagementConstants;



/**
 * @author monzer
 *
 */
public class ESALReCreateFailedOrderAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Override
	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();
		if (orderModel == null)
		{
			return false;
		}

		return (CheckoutPaymentType.ESAL.equals(orderModel.getPaymentType())
				|| CheckoutPaymentType.ESAL_B2C.equals(orderModel.getPaymentType()))
				&& !orderModel.isPaymentB2BInvoiceCreatedSuccessfully();
	}

	@Override
	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();

		if (orderModel != null && CollectionUtils.isNotEmpty(orderModel.getOrderProcess()))
		{
			final Optional<OrderProcessModel> process = orderModel.getOrderProcess().stream()
					.filter(p -> !(p instanceof B2BApprovalProcessModel)).findFirst();
			if (process.isPresent())
			{
				final String eventId = process.get().getCode() + "_" + MasdarOrdermanagementConstants.ORDER_ACTION_EVENT_NAME;

				String action = "createESALInvoice";
				if (CheckoutPaymentType.ESAL_B2C.equals(orderModel.getPaymentType()))
				{
					action = "createESALB2CInvoice";
				}

				final BusinessProcessEvent event = BusinessProcessEvent.builder(eventId).withChoice(action).build();
				businessProcessService.triggerEvent(event);
			}

		}
		return new ActionResult("success");
	}




}
