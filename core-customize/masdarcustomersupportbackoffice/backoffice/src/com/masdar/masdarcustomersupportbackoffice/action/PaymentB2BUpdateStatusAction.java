/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.masdar.masdarb2bpayment.context.PaymentB2BContext;



/**
 * @author monzer
 *
 */
public class PaymentB2BUpdateStatusAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{

	@Resource(name = "paymentB2BContext")
	private PaymentB2BContext paymentB2BContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();
		if (orderModel == null)
		{
			return false;
		}

		return OrderStatus.PENDING.equals(orderModel.getStatus());
	}

	@Override
	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();

		if (orderModel == null)
		{
			return new ActionResult("error");


		}


		paymentB2BContext.updatePaymentStatus(orderModel);
		return new ActionResult("success");
	}




}
