/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.util.resource.Labels;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.labels.LabelUtils;
import com.masdar.core.context.SerialNumberConfigurationContext;
import com.masdar.core.enums.SerialNumberSource;
import com.masdar.masdarcustomersupportbackoffice.strategy.OrderPrintDocumentStrategy;


/**
 * @author monzer
 *
 */
public class PrintOrderInvoiceAction extends AbstractComponentWidgetAdapterAware implements CockpitAction<OrderModel, OrderModel>
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "orderPrintInvoiceStrategy")
	private OrderPrintDocumentStrategy orderPrintDocumentStrategy;

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;

	@Override
	public boolean canPerform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();
		if (orderModel == null)
		{
			return false;
		}
		final boolean isEnabledByStore = orderModel.getStore() != null && orderModel.getStore().isDisplayInvoiceOnCustomerSupport();

		final Optional<ConsignmentModel> findAny = orderModel.getConsignments() == null ? Optional.empty()
				: orderModel.getConsignments().stream().filter(entry -> ConsignmentStatus.SHIPPED.equals(entry.getStatus()))
						.findAny();
		return isEnabledByStore && findAny.isPresent();
	}

	@Override
	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> ctx)
	{
		final OrderModel orderModel = ctx.getData();
		if (StringUtils.isBlank(orderModel.getOrderNumber()))
		{
			final Optional<String> orderNumber = generateOrderNumber(orderModel);
			if (orderNumber.isPresent())
			{
				orderModel.setOrderNumber(orderNumber.get());
				modelService.save(orderModel);
				modelService.refresh(orderModel);
			}
		}
		orderPrintDocumentStrategy.printDocument(orderModel);
		return new ActionResult<>(ActionResult.SUCCESS);
	}

	protected String resolveLabel(final String labelKey)
	{
		final String defaultValue = LabelUtils.getFallbackLabel(labelKey);
		return Labels.getLabel(labelKey, defaultValue);
	}

	/**
	 * @return
	 */
	protected Optional<String> generateOrderNumber(final OrderModel order)
	{
		return getSerialNumberConfigurationContext().generateSerialNumberForBaseStore(order.getStore(),
				SerialNumberSource.ABSTRACT_ORDER);
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the orderPrintDocumentStrategy
	 */
	public OrderPrintDocumentStrategy getOrderPrintDocumentStrategy()
	{
		return orderPrintDocumentStrategy;
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	public SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}

}
