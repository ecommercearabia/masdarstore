/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.omsbackoffice.actions.returns.ApproveReturnAction;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.util.Optional;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomApproveReturnAction extends ApproveReturnAction
{

	@Override
	public boolean canPerform(final ActionContext<ReturnRequestModel> actionContext)
	{
		final boolean superCanperform = super.canPerform(actionContext);
		final Object data = actionContext.getData();
		ReturnRequestModel returnRequest = null;
		boolean decision = false;
		if (data instanceof ReturnRequestModel)
		{
			returnRequest = (ReturnRequestModel) data;
			final Optional<ReturnProcessModel> process = returnRequest.getReturnProcess().stream().findFirst();
			if (process.isPresent())
			{
				final Optional<ProcessTaskModel> processTaskModel = process.get().getCurrentTasks().stream().findFirst();

				if (processTaskModel.isPresent())
				{

					decision = processTaskModel.get().getAction().equals("waitForConfirmOrCancelReturnAction");
				}
			}
		}

		return superCanperform && decision;
	}

}
