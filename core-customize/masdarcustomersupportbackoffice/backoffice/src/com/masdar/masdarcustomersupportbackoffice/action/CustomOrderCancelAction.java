/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.action;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.omsbackoffice.actions.order.cancel.CancelOrderAction;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author mohammed.baker@erabia.com
 *
 */
public class CustomOrderCancelAction extends CancelOrderAction
{

	@Override
	public boolean canPerform(final ActionContext<OrderModel> actionContext)
	{
		final boolean superPerfrom = super.canPerform(actionContext);
		final OrderModel order = actionContext.getData();
		return superPerfrom && order != null && !OrderStatus.PENDING.equals(order.getStatus()) && (order.getVersionID() == null);
	}


}
