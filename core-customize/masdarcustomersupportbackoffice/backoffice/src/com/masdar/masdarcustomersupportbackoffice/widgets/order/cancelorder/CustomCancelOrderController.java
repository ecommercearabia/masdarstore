/**
 *
 */
package com.masdar.masdarcustomersupportbackoffice.widgets.order.cancelorder;

import de.hybris.platform.omsbackoffice.widgets.order.cancelorder.CancelOrderController;

import org.zkoss.zk.ui.event.Event;


/**
 * @author mbaker
 *
 */
public class CustomCancelOrderController extends CancelOrderController
{

	@Override
	protected void processCancellation(final Event obj)
	{
		super.processCancellation(obj);

	}

}
