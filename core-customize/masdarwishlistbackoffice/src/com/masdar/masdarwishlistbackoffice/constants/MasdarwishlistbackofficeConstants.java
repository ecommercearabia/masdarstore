/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.masdar.masdarwishlistbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class MasdarwishlistbackofficeConstants extends GeneratedMasdarwishlistbackofficeConstants
{
	public static final String EXTENSIONNAME = "masdarwishlistbackoffice";

	private MasdarwishlistbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
