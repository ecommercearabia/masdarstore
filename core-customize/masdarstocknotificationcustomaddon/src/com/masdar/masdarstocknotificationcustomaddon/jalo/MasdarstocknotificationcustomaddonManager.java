/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarstocknotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.masdarstocknotificationcustomaddon.constants.MasdarstocknotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class MasdarstocknotificationcustomaddonManager extends GeneratedMasdarstocknotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarstocknotificationcustomaddonManager.class.getName() );
	
	public static final MasdarstocknotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarstocknotificationcustomaddonManager) em.getExtension(MasdarstocknotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
