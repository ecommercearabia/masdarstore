/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarstocknotificationcustomaddon.controllers;

/**
 */
public interface MasdarstocknotificationcustomaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/masdarstocknotificationcustomaddon/";

	interface Pages
	{
		String AddNotificationPage = ADDON_PREFIX + "pages/addStockNotification";
		String AddNotificationFromInterestPage = ADDON_PREFIX + "pages/addStockNotificationFromInterest";
	}
}
