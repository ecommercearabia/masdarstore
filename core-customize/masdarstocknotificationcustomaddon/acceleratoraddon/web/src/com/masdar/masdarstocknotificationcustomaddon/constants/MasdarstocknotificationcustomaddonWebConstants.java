/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarstocknotificationcustomaddon.constants;

/**
 * Global class for all Masdarstocknotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarstocknotificationcustomaddonWebConstants // NOSONAR
{
	private MasdarstocknotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
