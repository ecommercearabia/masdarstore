/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommerceorgcustomaddon.constants;

/**
 * Global class for all Masdarcommerceorgcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class MasdarcommerceorgcustomaddonConstants extends GeneratedMasdarcommerceorgcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarcommerceorgcustomaddon";

	private MasdarcommerceorgcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
