/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommerceorgcustomaddon.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Pojo for 'B2b unit' form.
 */
public class B2BUnitForm
{
	private String uid;
	private String name;
	private String parentUnit;
	private String originalUid;
	private String approvalProcessCode;
	private String iban;
	private String bankName;
	private String accountNumber;
	private String accountHolderName;
	private String vatId;
	

	public String getVatId()
	{
		return vatId;
	}

	public void setVatId(final String vatId)
	{
		this.vatId = vatId;
	}
	

	@NotNull(message = "{unit.uid.invalid}")
	@Size(min = 1, max = 255, message = "{unit.uid.invalid}")
	public String getUid()
	{
		return uid;
	}

	public void setUid(final String uid)
	{
		this.uid = uid;
	}

	@NotNull(message = "{unit.name.invalid}")
	@Size(min = 1, max = 255, message = "{unit.name.invalid}")
	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getParentUnit()
	{
		return parentUnit;
	}

	public void setParentUnit(final String parentUnit)
	{
		this.parentUnit = parentUnit;
	}

	public String getApprovalProcessCode()
	{
		return approvalProcessCode;
	}

	public void setApprovalProcessCode(final String approvalProcessCode)
	{
		this.approvalProcessCode = approvalProcessCode;
	}

	public String getOriginalUid()
	{
		return originalUid;
	}

	public void setOriginalUid(final String originalUid)
	{
		this.originalUid = originalUid;
	}
	

	/**
	 * @return the iban
	 */
	public String getIban()
	{
		return iban;
	}

	/**
	 * @param iban the iban to set
	 */
	public void setIban(String iban)
	{
		this.iban = iban;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName()
	{
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountHolderName
	 */
	public String getAccountHolderName()
	{
		return accountHolderName;
	}

	/**
	 * @param accountHolderName the accountHolderName to set
	 */
	public void setAccountHolderName(String accountHolderName)
	{
		this.accountHolderName = accountHolderName;
	}
	
}
