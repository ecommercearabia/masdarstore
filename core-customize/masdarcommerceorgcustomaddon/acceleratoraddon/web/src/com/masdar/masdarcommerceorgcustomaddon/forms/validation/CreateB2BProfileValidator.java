package com.masdar.masdarcommerceorgcustomaddon.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.commerceservices.user.UserMatchingService;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.masdar.masdarcommerceorgcustomaddon.forms.B2BCustomerForm;

/**
 * Validator for profile forms.
 */
@Component("createB2BProfileValidator")
public class CreateB2BProfileValidator implements Validator
{
	@Resource(name = "commerceorgUserMatchingService")
	private UserMatchingService commerceorgUserMatchingService;
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return B2BCustomerForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final B2BCustomerForm profileForm = (B2BCustomerForm) object;
		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();
		final String email = profileForm.getEmail();

		if (StringUtils.isBlank(title) || StringUtils.length(title) > 255)
		{
			errors.rejectValue("titleCode", "profile.title.invalid");
		}

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}
		else if (StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		else if (StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		
		if(StringUtils.isNotBlank(lastName) && commerceorgUserMatchingService.isUserExisting(email))
		{
			errors.rejectValue("email", "profile.email.is.exist");
			
		}
	}

}
