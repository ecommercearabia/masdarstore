/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcommerceorgcustomaddon.constants;

/**
 * Global class for all Masdarcommerceorgcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class MasdarcommerceorgcustomaddonWebConstants
{
	//Dummy field to avoid pmd error - delete when you add the first real constant!
	public static final String deleteThisDummyField = "DELETE ME";

	private MasdarcommerceorgcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
