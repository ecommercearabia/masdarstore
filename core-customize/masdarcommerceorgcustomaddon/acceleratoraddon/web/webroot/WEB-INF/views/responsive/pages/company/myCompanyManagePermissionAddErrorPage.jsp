<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/addons/masdarcommerceorgcustomaddon/responsive/company" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${empty cancelUrl}">
	<c:choose>
		<c:when test="${not empty b2BPermissionForm.originalCode }">
		    <spring:url value="/my-company/organization-management/manage-permissions/view" var="cancelUrl" htmlEscape="false">
		        <spring:param name="permissionCode" value="${b2BPermissionForm.originalCode}"/>
		    </spring:url>
		</c:when>
		<c:when test="${empty b2BPermissionForm.originalCode }">
			<spring:url value="/my-company/organization-management/manage-permissions" var="cancelUrl" htmlEscape="false"/>
		</c:when>
	</c:choose>
</c:if>
<c:if test="${empty saveUrl}">
    <spring:url value="/my-company/organization-management/manage-permissions/edit" var="saveUrl" htmlEscape="false">
        <spring:param name="permissionCode" value="${b2BPermissionForm.originalCode}"/>
    </spring:url>
</c:if>
<template:page pageTitle="${pageTitle}">
    <div class="account-section">
       
         <div class="row back-link border">
        <div class="container-lg col-md-6">
       
            <a href="${fn:escapeXml(cancelUrl)}">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <span class="label"><spring:theme code="text.company.managePermissions.edit.page.title"/></span>
        </div>
        </div>
      
        <div class="row">
    <div class="container-lg col-md-6">
        <div class="account-section-content">
        
        <company:b2bPermissionForm cancelUrl="${cancelUrl}" saveUrl="${saveUrl}" b2BPermissionForm="${b2BPermissionForm}"/>
    </div></div></div>
    </div>
</template:page>
