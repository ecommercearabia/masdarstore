/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.facades;

import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.exceptions.CustomerAlreadyExistsException;
import com.masdar.masdarsecureportalcustomaddon.exceptions.IncorrectRegistrationDataException;


/**
 * Facade responsible for everything related to B2B registration
 */
public interface B2BRegistrationFacade
{

	/**
	 * Initiates the registration process for B2B. This method will first validate the submitted data, check if a user or
	 * a company to the given name already exists, persist the registration request (as a model) and initiate the
	 * workflow so that the registration request either gets approved OR rejected.
	 *
	 * @param data
	 *           The registration data
	 */
	public void register(B2BRegistrationData data) throws CustomerAlreadyExistsException, IncorrectRegistrationDataException;

}
