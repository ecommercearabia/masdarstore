/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * Thrown when a customer already exists
 */
public class IncorrectRegistrationDataException extends BusinessException
{

	public IncorrectRegistrationDataException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public IncorrectRegistrationDataException(final String message)
	{
		super(message);
	}

	public IncorrectRegistrationDataException(final Throwable cause)
	{
		super(cause);

	}

}
