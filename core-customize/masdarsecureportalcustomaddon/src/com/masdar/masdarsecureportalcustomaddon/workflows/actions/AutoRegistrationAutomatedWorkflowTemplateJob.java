/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.workflows.actions;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.List;

import org.apache.log4j.Logger;

import com.masdar.masdarsecureportalcustomaddon.model.B2BRegistrationModel;




/**
 * Action called when a registration request has been rejected
 */
public class AutoRegistrationAutomatedWorkflowTemplateJob extends AbstractAutomatedWorkflowTemplateJob
{

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(AutoRegistrationAutomatedWorkflowTemplateJob.class);

	/*
	 * In this workflow step, we do nothing to reject registration, the registered customer will be removed in the next
	 * send email workflow step
	 *
	 * @see de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob#perform(de.hybris.platform.workflow.model.
	 * WorkflowActionModel)
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		final B2BRegistrationModel registration = getRegistrationAttachment(workflowAction);
		final BaseStoreModel store = registration.getBaseStore();
		final List<WorkflowDecisionModel> workflowDecisionList = (List<WorkflowDecisionModel>) workflowAction.getDecisions();
		if (workflowDecisionList == null || workflowDecisionList.size() < 2)
		{
			return null;
		}

		if (store.isEnableAutoRegistration() && registration.isCashCustomer())
		{
			return workflowDecisionList.stream().filter(e -> "B2BAutoApprovedEnabled".equalsIgnoreCase(e.getCode())).findFirst()
					.orElse(null);
		}
		else
		{
			return workflowDecisionList.stream().filter(e -> !"B2BAutoApprovedEnabled".equalsIgnoreCase(e.getCode())).findFirst()
					.orElse(null);
		}


	}

}
