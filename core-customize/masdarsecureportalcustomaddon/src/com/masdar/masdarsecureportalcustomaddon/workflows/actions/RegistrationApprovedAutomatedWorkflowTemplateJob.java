/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.workflows.actions;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.util.DateRangeFactory;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.model.CompanyRegistrationInfoModel;
import com.masdar.commercialregistration.populators.CommercialRegistrationModelPopulator;
import com.masdar.commercialregistration.service.CommercialRegistrationService;
import com.masdar.core.service.CommercialRegistrationB2BUnitService;
import com.masdar.masdarsecureportalcustomaddon.model.B2BRegistrationModel;




/**
 * Action called when a registration request has been approved
 */
public class RegistrationApprovedAutomatedWorkflowTemplateJob extends AbstractAutomatedWorkflowTemplateJob
{

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(RegistrationApprovedAutomatedWorkflowTemplateJob.class);


	@Resource(name = "currencyDao")
	private CurrencyDao currencyDao;

	@Resource(name = "defaultDateRangeFactory")
	private DateRangeFactory defaultDateRangeFactory;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "commercialRegistrationService")
	private CommercialRegistrationService commercialRegistrationService;

	@Resource(name = "commercialRegistrationModelPopulator")
	private CommercialRegistrationModelPopulator commercialRegistrationModelPopulator;


	@Resource(name = "commercialRegistrationB2BUnitService")
	private CommercialRegistrationB2BUnitService commercialRegistrationB2BUnitService;



	/**
	 * @return the commercialRegistrationB2BUnitService
	 */
	public CommercialRegistrationB2BUnitService getCommercialRegistrationB2BUnitService()
	{
		return commercialRegistrationB2BUnitService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob#perform(de.hybris.platform.workflow.model.
	 * WorkflowActionModel)
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		final B2BRegistrationModel registration = getRegistrationAttachment(workflowAction);
		final BaseStoreModel baseStore = registration.getBaseStore();
		final CustomerModel customer = getCustomer(registration);
		if (baseStore.isEnableAutoRegistration() && registration.isCashCustomer())
		{
			try
			{
				getCommercialRegistrationB2BUnitService().createB2bUnitFromCr(registration, baseStore);
			}
			catch (final CommercialRegistrationException e)
			{
				throw new RuntimeException(e.getMessage());
			}
			final B2BCustomerModel b2BCustomer = createB2BCustomerModel(customer, registration, baseStore);
			//Delete temporary customer attached to workflow
			getModelService().remove(customer);
			//persist the newly created b2bCustomer
			getModelService().save(b2BCustomer);
		}
		else
		{
			final B2BCustomerModel b2BCustomer = createB2BCreditCustomerModel(customer, registration);
			//Delete temporary customer attached to workflow
			getModelService().remove(customer);
			//persist the newly created b2bCustomer
			getModelService().save(b2BCustomer);
		}
		return defaultDecision(workflowAction);

	}

	/**
	 * @param customer
	 * @param registration
	 * @return
	 */
	private B2BCustomerModel createB2BCreditCustomerModel(final CustomerModel customer, final B2BRegistrationModel registration)
	{
		final B2BCustomerModel b2bCustomer = getModelService().create(B2BCustomerModel.class);

		b2bCustomer.setEmail(customer.getUid());
		b2bCustomer.setName(customer.getName());
		b2bCustomer.setTitle(customer.getTitle());
		b2bCustomer.setUid(customer.getUid());

		b2bCustomer.setCustomerID(customer.getCustomerID());
		b2bCustomer.setSessionCurrency(customer.getSessionCurrency());
		b2bCustomer.setSessionLanguage(customer.getSessionLanguage());

		b2bCustomer.setDefaultB2BUnit(registration.getDefaultB2BUnit());

		return b2bCustomer;
	}

	/**
	 * Creates an instance of {@link B2BCustomerModel} out of {@link CustomerModel}.
	 *
	 * @param customer
	 *           CustomerModel data
	 * @return An instance of {@link B2BCustomerModel}
	 * @throws CompanyDoesNotHaveCommercialRegistrationInfoException
	 * @throws CommercialRegistrationException
	 */

	protected B2BCustomerModel createB2BCustomerModel(final CustomerModel customer, final B2BRegistrationModel registration,
			final BaseStoreModel baseStore)
	{

		final B2BCustomerModel b2bCustomer = getModelService().create(B2BCustomerModel.class);
		final B2BUnitModel b2bUnit = registration.getDefaultB2BUnit();

		b2bCustomer.setEmail(customer.getUid());
		b2bCustomer.setName(customer.getName());
		b2bCustomer.setTitle(customer.getTitle());
		b2bCustomer.setUid(customer.getUid());

		b2bCustomer.setCustomerID(customer.getCustomerID());
		b2bCustomer.setSessionCurrency(customer.getSessionCurrency());
		b2bCustomer.setSessionLanguage(customer.getSessionLanguage());
		b2bCustomer.setPasswordEncoding(customer.getPasswordEncoding());
		b2bCustomer.setEncodedPassword(customer.getEncodedPassword());
		b2bCustomer.setLoginDisabled(false);


		b2bCustomer.setMobileCountry(registration.getMobileCountry());
		b2bCustomer.setMobileNumber(registration.getMobileNumber());


		final Set<PrincipalGroupModel> groups = new HashSet<>();
		groups.addAll(b2bCustomer.getGroups());
		groups.addAll(baseStore.getAutoRegistrationGroups());

		b2bCustomer.setGroups(groups);
		b2bCustomer.setDefaultB2BUnit(registration.getDefaultB2BUnit());
		b2bCustomer.setPermissions(b2bUnit.getPermissions());
		try
		{
			if (baseStore.isCommercialRegistrationRequired() && baseStore.isEnableCrCheck())
			{
				final CompanyRegistrationInfoModel crModel = commercialRegistrationService
						.checkAndCreateCompanyCrInfoModel(registration.getCompanyCr(), baseStore);

				b2bCustomer.setCompanyRegistrationInfo(crModel);
				registration.getDefaultB2BUnit().setCompanyRegistrationInfo(crModel);
				getModelService().save(registration.getDefaultB2BUnit());
				getModelService().refresh(registration.getDefaultB2BUnit());
			}
		}
		catch (final CommercialRegistrationException e)
		{
			throw new RuntimeException(e.getMessage());
		}

		return b2bCustomer;
	}



}
