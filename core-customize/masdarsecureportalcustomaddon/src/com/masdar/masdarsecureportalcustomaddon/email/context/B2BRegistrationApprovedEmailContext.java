/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.masdar.masdarsecureportalcustomaddon.model.B2BRegistrationApprovedProcessModel;


/**
 * Email context used to render B2B approval emails with reset password links
 */
public class B2BRegistrationApprovedEmailContext extends B2BRegistrationEmailContext
{

	private String passwordResetToken;
	private String emailVerificationToken;



	/**
	 * @return the emailVerificationToken
	 */
	public String getEmailVerificationToken()
	{
		return emailVerificationToken;
	}

	/**
	 * @param emailVerificationToken
	 *           the emailVerificationToken to set
	 */
	public void setEmailVerificationToken(final String emailVerificationToken)
	{
		this.emailVerificationToken = emailVerificationToken;
	}

	/**
	 * @return the passwordResetToken
	 */
	public String getPasswordResetToken()
	{
		return passwordResetToken;
	}

	/**
	 * @param passwordResetToken
	 *           the passwordResetToken to set
	 */
	public void setPasswordResetToken(final String passwordResetToken)
	{
		this.passwordResetToken = passwordResetToken;
	}

	/**
	 * @return The url-encoded representation of the reset password token
	 * @throws UnsupportedEncodingException
	 */
	public String getResetPasswordUrlEncodedToken() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(getPasswordResetToken(), "UTF-8");
	}

	/**
	 * @return The full URL used to reset a password
	 * @throws UnsupportedEncodingException
	 */
	public String getSecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/login/pw/change", "token=" + getResetPasswordUrlEncodedToken());
	}

	/**
	 * @return The url-encoded representation of the reset password token
	 * @throws UnsupportedEncodingException
	 */
	public String getEmailVerificationUrlEncodedToken() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(getEmailVerificationToken(), "UTF-8");
	}

	/**
	 * @return The full URL used to reset a password
	 * @throws UnsupportedEncodingException
	 */
	public String getSecureEmailVerificationUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/email/verify", "token=" + getEmailVerificationUrlEncodedToken());
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final StoreFrontCustomerProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		if (businessProcessModel instanceof B2BRegistrationApprovedProcessModel)
		{
			final B2BRegistrationApprovedProcessModel registrationProcessModel = (B2BRegistrationApprovedProcessModel) businessProcessModel;
			setPasswordResetToken(registrationProcessModel.getPasswordResetToken());
			setEmailVerificationToken(registrationProcessModel.getEmailVerificationToken());
		}
	}

}
