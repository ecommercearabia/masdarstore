/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;

import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.forms.RegistrationForm;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.service.CommercialRegistrationService;
import com.masdar.core.service.CommercialRegistrationB2BUnitService;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.masdar.core.service.MobilePhoneService;


/**
 * Validates the secure portal registration form.
 */
@Component("securePortalRegistrationValidator")
public class SecurePortalRegistrationValidator implements Validator
{
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "commercialRegistrationService")
	private CommercialRegistrationService commercialRegistrationService;


	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "commercialRegistrationB2BUnitService")
	private CommercialRegistrationB2BUnitService commercialRegistrationB2BUnitService;

	/**
	 * @return the commercialRegistrationB2BUnitService
	 */
	public CommercialRegistrationB2BUnitService getCommercialRegistrationB2BUnitService()
	{
		return commercialRegistrationB2BUnitService;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegistrationForm.class.equals(aClass);
	}

	public boolean validate(final B2BRegistrationData b2bRegistrationData)
	{
		final String country = b2bRegistrationData.getCompanyAddressCountryIso();
		final String email = b2bRegistrationData.getEmail();
		final String name = b2bRegistrationData.getName();
		final String mobileNumber = b2bRegistrationData.getMobileNumber();
		final String mobileCountry = b2bRegistrationData.getMobileCountry();
		final String companyCr = b2bRegistrationData.getCompanyCr();
		final String companyAddressCity = b2bRegistrationData.getCompanyAddressCity();
		final String password = b2bRegistrationData.getPassword();

		return validateBlankText(companyAddressCity) && validateBlankText(country) && validateBlankText(name)
				&& validateBlankText(mobileNumber) && validateBlankText(mobileCountry) && validateBlankText(password)
				&& validateEmail(email) && validateMobileNumber(mobileCountry, mobileNumber, b2bRegistrationData)
				&& validateCompanyCr(companyCr);

	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegistrationForm registrationForm = (RegistrationForm) object;
		final String companyAddressCity = registrationForm.getCompanyAddressCity();
		final String country = registrationForm.getCompanyAddressCountryIso();
		final String email = registrationForm.getEmail();
		final String name = registrationForm.getName();
		final String mobileNumber = registrationForm.getMobileNumber();
		final String mobileCountry = registrationForm.getMobileCountry();
		final String companyCr = registrationForm.getCompanyCr();
		final String password = registrationForm.getPassword();
		final String vatNumber = registrationForm.getCompanyVATNumber();

		validateBlankText(errors, companyAddressCity, "companyAddressCity");
		validateBlankText(errors, country, "companyAddressCountryIso");
		validateBlankText(errors, name, "name");
		validateBlankText(errors, mobileNumber, "mobileNumber");
		validateTextLength(errors, mobileCountry, "mobileCountry");
		validateTextLength(errors, password, "password");
		validateTextLength(errors, password, "companyVATNumber");
		validateEmail(errors, email, "email");

		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (currentBaseStore.isCommercialRegistrationRequired())
		{
			validateCompanyCr(errors, companyCr, "companyCr", currentBaseStore);
		}

		if (StringUtils.isNotBlank(mobileCountry) && StringUtils.isNotBlank(mobileNumber))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				registrationForm.setTelephone(normalizedPhoneNumber.get());
				registrationForm.setMobileNumber(normalizedPhoneNumber.get());
				registrationForm.setTelephoneExtension(mobileCountry);
			}
			else
			{
				errors.rejectValue("mobileNumber", "text.secureportal.register.mobile.format.invalid");
				errors.rejectValue("mobileCountry", "text.secureportal.register.mobilecountry.invalid");
			}
		}
	}

	protected boolean validateMobileNumber(final String mobileCountry, final String mobileNumber,
			final B2BRegistrationData b2bRegistrationData)
	{
		if (StringUtils.isBlank(mobileCountry) || StringUtils.isBlank(mobileNumber))
		{
			return false;
		}
		final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
				mobileNumber);
		if (normalizedPhoneNumber.isEmpty())
		{
			return false;
		}
		b2bRegistrationData.setTelephone(normalizedPhoneNumber.get());
		b2bRegistrationData.setMobileNumber(normalizedPhoneNumber.get());
		b2bRegistrationData.setTelephoneExtension(mobileCountry);
		return true;
	}

	protected boolean validateEmail(final String email)
	{
		return validateBlankText(email) && validateEmailAddress(email) && !isUserExisting(email);
	}

	protected boolean isUserExisting(final String email)
	{
		return userService.isUserExisting(email);
	}

	protected boolean validateBlankText(final String name)
	{
		return (StringUtils.isNotBlank(name) && StringUtils.length(name) < 255);
	}

	protected void validateBlankText(final Errors errors, final String name, final String propertyName)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
		}
		else
		{
			validateTextLength(errors, name, propertyName);
		}
	}

	protected boolean validateTextLength(final Errors errors, final String name, final String propertyName)
	{
		if (!StringUtils.isBlank(name) && StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.toolong");
			return false;
		}
		return true;
	}

	protected boolean validateCompanyCr(final String companyCr)
	{
		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (!currentBaseStore.isCommercialRegistrationRequired())
		{
			return true;
		}

		if (currentBaseStore == null || StringUtils.isBlank(companyCr) || StringUtils.length(companyCr) != 10
				|| !StringUtils.isNumeric(companyCr) || b2bUnitService.getUnitForUid(companyCr) != null
				|| !getCommercialRegistrationB2BUnitService().validateCr(companyCr, currentBaseStore))
		{
			return false;
		}

		if (!currentBaseStore.isEnableCrCheck())
		{
			return true;
		}
		try
		{
			Optional<CompanyRegistrationInfo> companyCrInfoByCurrentStore = commercialRegistrationService
					.getCompanyCrInfoByCurrentStore(companyCr);
			if (companyCrInfoByCurrentStore.isEmpty())
			{
				return false;
			}
		}
		catch (CommercialRegistrationException e)
		{
			return false;
		}
		return true;
	}

	protected void validateCompanyCr(final Errors errors, final String companyCr, final String propertyName,
			BaseStoreModel currentBaseStore)
	{
		if (StringUtils.isEmpty(companyCr))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
			return;
		}
		// Validate too long
		if (StringUtils.length(companyCr) > 10)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.crnumber.toolong");
			return;
		}

		// Validate too short
		if (StringUtils.length(companyCr) < 10)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.crnumber.tooshort");
			return;
		}

		// Validate all digits are num
		if (!StringUtils.isNumeric(companyCr))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mustbenumiric");
			return;
		}

		B2BUnitModel unit = b2bUnitService.getUnitForUid(companyCr);
		if (unit != null)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.already.registered");
			return;
		}

		if (!getCommercialRegistrationB2BUnitService().validateCr(companyCr, currentBaseStore))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.already.registered.check.customersupport");
			return;
		}

		if (!currentBaseStore.isEnableCrCheck())
		{
			return;
		}

		boolean companyCrInfoValid = true;
		try
		{
			Optional<CompanyRegistrationInfo> companyCrInfoByCurrentStore = commercialRegistrationService
					.getCompanyCrInfoByCurrentStore(companyCr);
			if (companyCrInfoByCurrentStore.isEmpty())
			{
				companyCrInfoValid = false;
			}
		}
		catch (CommercialRegistrationException e)
		{
			companyCrInfoValid = false;

		}
		if (!companyCrInfoValid)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.invalid");
			return;
		}
	}

	protected void validateCompanyVATNo(final Errors errors, final String companyVATNo, final String propertyName)
	{
		if (StringUtils.isEmpty(companyVATNo))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
			return;
		}
		// Validate too long
		if (StringUtils.length(companyVATNo) > 15)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.toolong");
			return;
		}

		// Validate too short
		if (StringUtils.length(companyVATNo) < 15)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.short");
			return;
		}

		// Validate all digits are num
		if (!StringUtils.isNumeric(companyVATNo))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mustbenumiric");
			return;
		}

	}

	protected void validateEmail(final Errors errors, final String email, final String propertyName)
	{
		if (StringUtils.isBlank(email))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
			return;
		}

		if (!validateEmailAddress(email))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.email.invalid");
			return;
		}

		if (!validateTextLength(errors, email, propertyName))
		{
			return;
		}
		final boolean userExists = userService.isUserExisting(email);

		if (userExists)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.account.existing");
			return;
		}

	}

	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = Pattern.compile(configurationService.getConfiguration().getString(WebConstants.EMAIL_REGEX))
				.matcher(email);
		return matcher.matches();
	}

	/**
	 * @return the commercialRegistrationB2BUnitService
	 */
	public UserService getUserService()
	{
		return userService;
	}
}
