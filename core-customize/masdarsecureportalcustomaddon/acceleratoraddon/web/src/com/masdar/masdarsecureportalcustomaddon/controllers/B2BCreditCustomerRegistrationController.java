/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.facades.CommercialRegistrationFacade;
import com.masdar.facades.facade.PointOfServiceFacade;
import com.masdar.masdarsecureportalcustomaddon.constants.MasdarsecureportalcustomaddonWebConstants;
import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.exceptions.CustomerAlreadyExistsException;
import com.masdar.masdarsecureportalcustomaddon.exceptions.IncorrectRegistrationDataException;
import com.masdar.masdarsecureportalcustomaddon.facades.B2BRegistrationFacade;
import com.masdar.masdarsecureportalcustomaddon.forms.CreditCustomerRegistrationForm;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.masdar.facades.country.facade.CountryFacade;
import com.masdar.facades.area.facade.AreaFacade;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;

import com.masdar.masdarotp.context.OTPContext;
import com.masdar.masdarotp.enums.OTPVerificationTokenType;

import com.masdar.masdarotp.model.OTPVerificationTokenModel;

import com.masdar.masdarotp.exception.OTPException;
import java.net.URLEncoder;

import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import com.masdar.masdarsecureportalcustomaddon.forms.validation.SecurePortalRegistrationValidator;
import com.masdar.masdarsecureportalcustomaddon.forms.validation.ChangeCrNumberRegistrationValidator;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;


/**
 * Registration page controller: Handles Get and Post request and dispatches relevant wortkflow facades and necessary
 * services
 */
@RequestMapping(value = MasdarsecureportalcustomaddonWebConstants.RequestMappings.CREDIT_ACCOUNT_REGISTRATION)
public class B2BCreditCustomerRegistrationController extends AbstractB2BRegistrationController
{

	private static final Logger LOG = Logger.getLogger(B2BCreditCustomerRegistrationController.class);

	private static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
	private static final String REGISTER_ERROR_OCCURRED = "text.secureportal.register.submit.error";
	private static final String SCP_LINK_CREATE_ACCOUNT = "text.secureportal.link.createAccount";
	private static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";
	private static final String LOGIN_PAGE = REDIRECT_PREFIX + "/login";
	private static final String HOME_REDIRECT = REDIRECT_PREFIX + ROOT;



	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "b2bCreditCustomerRegistrationFacade")
	private B2BRegistrationFacade b2bRegistrationFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "securePortalCreditCustomerRegistrationValidator")
	private Validator registrationValidator;

	@Resource(name = "assistedServiceFacade")
	private AssistedServiceFacade assistedServiceFacade;

	private boolean isASMAgent()
	{
		return assistedServiceFacade.isAssistedServiceAgentLoggedIn();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showRegistrationPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		if (getCmsSiteService().getCurrentSite().isEnableRegistration() && isASMAgent())
		{
			return getDefaultRegistrationPage(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
		}
		return HOME_REDIRECT;
	}



	@RequestMapping(method = RequestMethod.POST)
	public String submitRegistration(final CreditCustomerRegistrationForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		populateModelCmsContent(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
		model.addAttribute(form);

		getRegistrationValidator().validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			List<ObjectError> allErrors = bindingResult.getAllErrors();
			for(ObjectError error : allErrors)
			{
				LOG.error("B2BCreditCustomerRegistrationController" + error);
			}
			return getRegistrationView();
		}

		try
		{
			B2BRegistrationData registrationData = convertFormToData(form);
			registrationData.setCashCustomer(false);
			b2bRegistrationFacade.register(registrationData);
		}
		catch (final CustomerAlreadyExistsException e) //NOSONAR
		{
			LOG.error("Failed to register account. Account already exists.");
			LOG.error(e.getMessage());
			GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(REGISTER_ACCOUNT_EXISTING));
			return getRegistrationView();
		}
		catch (final IncorrectRegistrationDataException e)
		{
			LOG.error("Failed to register account");
			LOG.error(e.getMessage());
			GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(REGISTER_ERROR_OCCURRED));
			return getRegistrationView();
		}

		GlobalMessages.addInfoMessage(model, Localization.getLocalizedString(REGISTER_SUBMIT_CONFIRMATION));

		return LOGIN_PAGE;
	}


	protected B2BRegistrationData convertFormToData(final CreditCustomerRegistrationForm form)
	{
		final B2BRegistrationData registrationData = new B2BRegistrationData();
		BeanUtils.copyProperties(form, registrationData);
		registrationData.setName(StringUtils.trim(form.getFirstName()) + " " + StringUtils.trim(form.getLastName()));
		return registrationData;
	}


	@Override
	protected String getRegistrationView()
	{
		return MasdarsecureportalcustomaddonWebConstants.Views.CREDIT_CUSTOEMR_REGISTRATION_PAGE;
	}


	@Override
	protected String getView()
	{
		return MasdarsecureportalcustomaddonWebConstants.Views.LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return HOME_REDIRECT;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Override
	protected String getRegistrationCmsPage()
	{
		return MasdarsecureportalcustomaddonWebConstants.CMS_CREDIT_CUSTOMER_REGISTER_PAGE_NAME;
	}

	protected String getDefaultRegistrationPage(final Model model, final ContentPageModel contentPageModel)
	{
		populateModelCmsContent(model, contentPageModel);
		CreditCustomerRegistrationForm creditCustomerRegistrationForm = new CreditCustomerRegistrationForm();
		model.addAttribute(creditCustomerRegistrationForm);
		//		populateRegionsAndPOS(model, registrationForm);
		//		populateCitiesAndDistricts(model, registrationForm);
		return getRegistrationView();
	}


	@Override
	protected void populateModelCmsContent(final Model model, final ContentPageModel contentPageModel)
	{

		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);

		final Breadcrumb registrationBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage(SCP_LINK_CREATE_ACCOUNT, null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(registrationBreadcrumbEntry));
	}

	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}



	public Validator getRegistrationValidator()
	{
		return registrationValidator;
	}



	public ModelService getModelService()
	{
		return modelService;
	}



	public B2BRegistrationFacade getB2bRegistrationFacade()
	{
		return b2bRegistrationFacade;
	}



	public CheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}


}
