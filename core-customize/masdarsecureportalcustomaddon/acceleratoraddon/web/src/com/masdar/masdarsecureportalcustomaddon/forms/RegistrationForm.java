/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.forms;

import javax.validation.constraints.Size;

import org.apache.logging.log4j.util.Strings;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * Secure portal registration form
 */
public class RegistrationForm
{
	private String name;
	private String email;
	private String telephone;
	private String telephoneExtension;
	private String mobileNumber;
	private String mobileCountry;
	private String companyName;
	private String companyCr;
	private String companyAddressRegion;
	private String companyAddressCountryIso;
	private String pointOfService;
	private String password;
	private String companyVATNumber;
	private String district;
	private String companyAddressCity;


	public String getCompanyVATNumber()
	{
		return companyVATNumber;
	}

	public void setCompanyVATNumber(String companyVATNumber)
	{
		this.companyVATNumber = companyVATNumber;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		if (!Strings.isBlank(mobileNumber) && mobileNumber.startsWith("00"))
		{
			return mobileNumber.replaceFirst("^0+(?!$)", "");
		}
		return mobileNumber;
	}

	/**
	 * @param mobile
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}


	/**
	 * @return the pointOfServiceName
	 */
	public String getPointOfService()
	{
		return pointOfService;
	}

	/**
	 * @param pointOfServiceName
	 *           the pointOfServiceName to set
	 */
	public void setPointOfService(final String pointOfService)
	{
		this.pointOfService = pointOfService;
	}

	/**
	 * @return the titleCode
	 */
	public String getCompanyAddressRegion()
	{
		return companyAddressRegion;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setCompanyAddressRegion(final String companyAddressRegion)
	{
		this.companyAddressRegion = companyAddressRegion;
	}

	/**
	 * @return the titleCode
	 */
	public String getCompanyCr()
	{
		return companyCr;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setCompanyCr(final String companyCr)
	{
		this.companyCr = companyCr;
	}

	/**
	 * @deprecated deprecated form 1811
	 * @return the firstLastName
	 */
	@Deprecated(since = "1811", forRemoval = true)
	public String getName()
	{
		return name;
	}

	/**
	 * @deprecated deprecated form 1811
	 * @param firstLastName
	 *           the firstLastName to set
	 */
	@Deprecated(since = "1811", forRemoval = true)
	public void setName(final String firstAndLastName)
	{
		this.name = firstAndLastName;
	}


	/**
	 * @return the email
	 */
	//LOCALISATION REQUIRED
	@NotEmpty(message = "{register.email.invalid}")
	@Size(min = 1, max = 255, message = "{register.email.invalid}")
	@Email(message = "{register.email.invalid}")
	public String getEmail()
	{
		return email == null ? "" : email.toLowerCase();
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}


	/**
	 * @return the telephone
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getTelephone()
	{
		return telephone;
	}

	/**
	 * @param telephone
	 *           the telephone to set
	 */
	public void setTelephone(final String telephone)
	{
		this.telephone = telephone;
	}

	/**
	 * @return the telephoneExtension
	 */
	public String getTelephoneExtension()
	{
		return telephoneExtension;
	}

	/**
	 * @param telephoneExtension
	 *           the telephoneExtension to set
	 */
	public void setTelephoneExtension(final String telephoneExtension)
	{
		this.telephoneExtension = telephoneExtension;
	}

	/**
	 * @return the companyName
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getCompanyName()
	{
		return companyName;
	}

	/**
	 * @param companyName
	 *           the companyName to set
	 */
	public void setCompanyName(final String companyName)
	{
		this.companyName = companyName;
	}


	/**
	 * @return the companyAddressCountryIso
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getCompanyAddressCountryIso()
	{
		return companyAddressCountryIso;
	}

	/**
	 * @param companyAddressCountryIso
	 *           the companyAddressCountryIso to set
	 */
	public void setCompanyAddressCountryIso(final String companyAddressCountryIso)
	{
		this.companyAddressCountryIso = companyAddressCountryIso;
	}

	public String getDistrict()
	{
		return district;
	}

	public void setDistrict(String district)
	{
		this.district = district;
	}

	public String getCompanyAddressCity()
	{
		return companyAddressCity;
	}

	public void setCompanyAddressCity(String companyAddressCity)
	{
		this.companyAddressCity = companyAddressCity;
	}
	
	@Override
	public String toString()
	{
		return "RegistrationForm [name=" + name + ", email=" + email + ", telephone=" + telephone + ", telephoneExtension="
				+ telephoneExtension + ", mobileNumber=" + mobileNumber + ", mobileCountry=" + mobileCountry + ", companyName="
				+ companyName + ", companyCr=" + companyCr + ", companyAddressRegion=" + companyAddressRegion
				+ ", companyAddressCountryIso=" + companyAddressCountryIso + ", pointOfService=" + pointOfService + ", password="
				+ password + ", companyVATNumber=" + companyVATNumber + ", district=" + district + ", companyAddressCity="
				+ companyAddressCity + "]";
	}

}
