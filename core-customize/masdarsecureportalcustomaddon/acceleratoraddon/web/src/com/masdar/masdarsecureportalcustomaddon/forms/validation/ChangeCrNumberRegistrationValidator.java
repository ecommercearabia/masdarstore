/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;

import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.forms.RegistrationCrNumberForm;
import com.masdar.commercialregistration.beans.CompanyRegistrationInfo;
import com.masdar.commercialregistration.exception.CommercialRegistrationException;
import com.masdar.commercialregistration.service.CommercialRegistrationService;
import com.masdar.core.service.CommercialRegistrationB2BUnitService;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.masdar.core.service.MobilePhoneService;


/**
 * Validates the secure portal registration form.
 */
@Component("changeCrNumberRegistrationValidator")
public class ChangeCrNumberRegistrationValidator implements Validator
{
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "commercialRegistrationService")
	private CommercialRegistrationService commercialRegistrationService;


	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "commercialRegistrationB2BUnitService")
	private CommercialRegistrationB2BUnitService commercialRegistrationB2BUnitService;

	/**
	 * @return the commercialRegistrationB2BUnitService
	 */
	public CommercialRegistrationB2BUnitService getCommercialRegistrationB2BUnitService()
	{
		return commercialRegistrationB2BUnitService;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegistrationCrNumberForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegistrationCrNumberForm registrationForm = (RegistrationCrNumberForm) object;
		final String companyCr = registrationForm.getCrNumber();

		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (currentBaseStore.isCommercialRegistrationRequired())
		{
			validateCompanyCr(errors, companyCr, "crNumber", currentBaseStore);
		}
	}

	protected boolean isUserExisting(final String email)
	{
		return userService.isUserExisting(email);
	}

	protected boolean validateBlankText(final String name)
	{
		return (StringUtils.isNotBlank(name) && StringUtils.length(name) < 255);
	}

	protected void validateBlankText(final Errors errors, final String name, final String propertyName)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
		}
		else
		{
			validateTextLength(errors, name, propertyName);
		}
	}

	protected boolean validateTextLength(final Errors errors, final String name, final String propertyName)
	{
		if (!StringUtils.isBlank(name) && StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.toolong");
			return false;
		}
		return true;
	}


	protected void validateCompanyCr(final Errors errors, final String companyCr, final String propertyName,
			BaseStoreModel currentBaseStore)
	{
		if (StringUtils.isEmpty(companyCr))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mandatory");
			return;
		}
		// Validate too long
		if (StringUtils.length(companyCr) > 10)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.crnumber.toolong");
			return;
		}

		// Validate too short
		if (StringUtils.length(companyCr) < 10)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.crnumber.tooshort");
			return;
		}

		// Validate all digits are num
		if (!StringUtils.isNumeric(companyCr))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.mustbenumiric");
			return;
		}

		B2BUnitModel unit = b2bUnitService.getUnitForUid(companyCr);
		if (unit != null)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.already.registered");
			return;
		}

		if (!getCommercialRegistrationB2BUnitService().validateCr(companyCr, currentBaseStore))
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.already.registered.check.customersupport");
			return;
		}

		if (!currentBaseStore.isEnableCrCheck())
		{
			return;
		}

		boolean companyCrInfoValid = true;
		try
		{
			Optional<CompanyRegistrationInfo> companyCrInfoByCurrentStore = commercialRegistrationService
					.getCompanyCrInfoByCurrentStore(companyCr);
			if (companyCrInfoByCurrentStore.isEmpty())
			{
				companyCrInfoValid = false;
			}
		}
		catch (CommercialRegistrationException e)
		{
			companyCrInfoValid = false;

		}
		if (!companyCrInfoValid)
		{
			errors.rejectValue(propertyName, "text.secureportal.register.field.company.invalid");
			return;
		}
	}



	/**
	 * @return the commercialRegistrationB2BUnitService
	 */
	public UserService getUserService()
	{
		return userService;
	}
}
