/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.interceptors;

import javax.servlet.http.HttpServletRequest;


public interface SecurePortalRequestProcessor
{
	/**
	 * get other request parameters
	 * 
	 * @param request
	 * @return request parameters
	 */
	String getOtherRequestParameters(final HttpServletRequest request);

	/**
	 * whether we want to skip secure check in secureportal
	 */
	boolean skipSecureCheck();
}
