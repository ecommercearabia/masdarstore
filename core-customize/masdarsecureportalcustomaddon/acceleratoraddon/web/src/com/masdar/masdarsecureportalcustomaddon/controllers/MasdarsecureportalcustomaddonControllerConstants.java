/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.controllers;

public interface MasdarsecureportalcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
