/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.masdar.commercialregistration.data.CompanyRegistrationInfoData;
import com.masdar.commercialregistration.facades.CommercialRegistrationFacade;
import com.masdar.facades.facade.PointOfServiceFacade;
import com.masdar.masdarsecureportalcustomaddon.constants.MasdarsecureportalcustomaddonWebConstants;
import com.masdar.masdarsecureportalcustomaddon.data.B2BRegistrationData;
import com.masdar.masdarsecureportalcustomaddon.exceptions.CustomerAlreadyExistsException;
import com.masdar.masdarsecureportalcustomaddon.exceptions.IncorrectRegistrationDataException;
import com.masdar.masdarsecureportalcustomaddon.facades.B2BRegistrationFacade;
import com.masdar.masdarsecureportalcustomaddon.forms.RegistrationForm;
import com.masdar.masdarsecureportalcustomaddon.forms.RegistrationCrNumberForm;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.masdar.facades.country.facade.CountryFacade;
import com.masdar.facades.area.facade.AreaFacade;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.masdar.masdarotp.context.OTPContext;
import com.masdar.masdarotp.enums.OTPVerificationTokenType;

import com.masdar.masdarotp.model.OTPVerificationTokenModel;

import com.masdar.masdarotp.exception.OTPException;
import java.net.URLEncoder;

import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import com.masdar.masdarsecureportalcustomaddon.forms.validation.SecurePortalRegistrationValidator;
import com.masdar.storefront.form.UpdateProfileForm;
import com.masdar.masdarsecureportalcustomaddon.forms.validation.ChangeCrNumberRegistrationValidator;


/**
 * Registration page controller: Handles Get and Post request and dispatches relevant wortkflow facades and necessary
 * services
 */
@RequestMapping(value = MasdarsecureportalcustomaddonWebConstants.RequestMappings.ACCOUNT_REGISTRATION)
public class B2BRegistrationController extends AbstractB2BRegistrationController
{

	private static final Logger LOG = Logger.getLogger(B2BRegistrationController.class);

	private static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
	private static final String SCP_LINK_CREATE_ACCOUNT = "text.secureportal.link.createAccount";
	private static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";
	private static final String LOGIN_PAGE = REDIRECT_PREFIX + "/login";
	private static final String REGISTER_PAGE = REDIRECT_PREFIX + "/register";
	private static final String HOME_REDIRECT = REDIRECT_PREFIX + ROOT;
	private static final String PHONENUMBER_VERIFICATION_PAGE_LABEL = "/register/verify";
	private static final String MESSAGE_NOT_SENT_MESSAGE = "otp.message.not.sent.message";
	private static final String REGISTER_ERROR_OCCURRED = "text.secureportal.register.submit.error";
	private static final String CR_INFORMATION_REGISTRATION_CMS_PAGE = "cr-info-registration";
	private static final String CR_INFORMATION_REGISTRATION_PAGE = REGISTER_PAGE + "/cr-info";
	private static final String REGISTRATION_THANK_YOU_PAGE_PATH = REDIRECT_PREFIX + "/thank-you/registration";



	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "b2bRegistrationFacade")
	private B2BRegistrationFacade b2bRegistrationFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "securePortalRegistrationValidator")
	private Validator registrationValidator;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "pointOfServiceFacade")
	private PointOfServiceFacade pointOfServiceFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "secureTokenService")
	private SecureTokenService secureTokenService;

	@Resource(name = "securePortalRegistrationValidator")
	private SecurePortalRegistrationValidator securePortalRegistrationValidator;

	@Resource(name = "changeCrNumberRegistrationValidator")
	private ChangeCrNumberRegistrationValidator changeCrNumberRegistrationValidator;

	@Resource(name = "commercialRegistrationFacade")
	private CommercialRegistrationFacade commercialRegistrationFacade;


	private long tokenValiditySeconds = 1800;

	@RequestMapping(method = RequestMethod.GET)
	public String showRegistrationPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		if (getCmsSiteService().getCurrentSite().isEnableRegistration())
		{
			return getDefaultRegistrationPage(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
		}
		return HOME_REDIRECT;
	}
	


	@RequestMapping(method = RequestMethod.POST)
	public String submitRegistration(final RegistrationForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		populateModelCmsContent(model, getContentPageForLabelOrId(getRegistrationCmsPage()));

		preselectMobileCountryForCurrentSite(form);

		
		getRegistrationValidator().validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			populateCitiesAndDistricts(model, form);
			return getRegistrationView();
		}
		model.addAttribute(form);

		B2BRegistrationData b2bRegistrationData = convertFormToData(form);
		final String token = createTokenFromB2BRegistrationData(b2bRegistrationData);

		String encodeToken = encodeToken(token);
		if (Strings.isBlank(encodeToken))
		{
			return getRegistrationView();
		}

		return CR_INFORMATION_REGISTRATION_PAGE + "?token=" + encodeToken;
	}
	
	private void preselectMobileCountryForCurrentSite(final RegistrationForm form)
	{
		final CMSSiteModel site = getCmsSiteService().getCurrentSite();
		if (site == null)
		{
			return;
		}
		if (site.isMobileCountryPreSelecetd())
		{
			form.setMobileCountry(
					site.getDefaultMobileCountry() == null ? form.getMobileCountry() : site.getDefaultMobileCountry().getIsocode());
		}
	}

	@PostMapping(path = "/cr-info/register")
	public String crInfoRegister(@RequestParam(name = "token", required = true)
	final String token, final Model model, final HttpServletRequest request, final HttpSession session,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		LOG.error("[B2BRegistrationController] : Before isTokenValid");

		if (!isTokenValid(token))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "register.crinfo.token.invalid",
					REGISTER_PAGE);
		}

		LOG.error("[B2BRegistrationController] : After isTokenValid");
		B2BRegistrationData b2bRegistrationData = getB2BRegistrationDataFromToken(token);


		LOG.error("[B2BRegistrationController] : Before isRegistrationValidator");
		if (!isRegistrationValidator(b2bRegistrationData))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"register.crinfo.b2bregistrationdata.invalid", REGISTER_PAGE);
		}
		LOG.error("[B2BRegistrationController] : After isRegistrationValidator");


		final Pair<Boolean, String> haveOTP = isHaveOTP(redirectModel, b2bRegistrationData);

		if (Boolean.TRUE.equals(haveOTP.getKey()))
		{
			return haveOTP.getValue();
		}

		try
		{
			b2bRegistrationData.setCashCustomer(true);
			b2bRegistrationFacade.register(b2bRegistrationData);
		}
		catch (final CustomerAlreadyExistsException e) //NOSONAR
		{
			LOG.error("Failed to register account. Account already exists.");
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REGISTER_ACCOUNT_EXISTING,
					REGISTER_PAGE);
		}
		catch (final IncorrectRegistrationDataException e)
		{
			LOG.error("Failed to register account");
			LOG.error(e.getMessage());
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REGISTER_ERROR_OCCURRED,
					REGISTER_PAGE);
		}
		catch (final Exception e)
		{
			LOG.error("Failed to register account");
			LOG.error(e.getMessage());
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, REGISTER_ERROR_OCCURRED,
					REGISTER_PAGE);
		}
		return getRedirectPageWithMsg(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, REGISTER_SUBMIT_CONFIRMATION,
				REGISTRATION_THANK_YOU_PAGE_PATH);
	}

	protected Pair<Boolean, String> isHaveOTP(final RedirectAttributes redirectAttributes,
			final B2BRegistrationData b2bRegistrationData)
	{


		if (otpContext.isEnabledByCurrentSite(OTPVerificationTokenType.REGISTRATION))
		{
			LOG.info("[B2BRegistrationController] : in isEnabledByCurrentSite");
			try
			{
				LOG.info("[B2BRegistrationController] : Before sendOTPCodeByCurrentSiteAndCustomer");
				final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = otpContext.sendOTPCodeByCurrentSiteAndCustomer(
						OTPVerificationTokenType.REGISTRATION, b2bRegistrationData.getMobileCountry(),
						b2bRegistrationData.getMobileNumber(), b2bRegistrationData);

				LOG.info("[B2BRegistrationController] : After sendOTPCodeByCurrentSiteAndCustomer");

				if (otpVerificationTokenModel.isEmpty())
				{
					return Pair.of(Boolean.TRUE, getRedirectPageWithMsg(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"register.crinfo.token.invalid", CR_INFORMATION_REGISTRATION_PAGE));
				}
				String encodeToken = otpVerificationTokenModel.get().getToken();
				try
				{
					LOG.info("[B2BRegistrationController] : Before URLEncoder.encode");
					encodeToken = URLEncoder.encode(otpVerificationTokenModel.get().getToken(), StandardCharsets.UTF_8.toString());
					LOG.info("[B2BRegistrationController] : After URLEncoder.encode");
				}
				catch (final UnsupportedEncodingException e)
				{
					LOG.info(String.format("token from otpVerificationTokenModel could not be decoded : %s",
							otpVerificationTokenModel.get().getToken()), e);

					return Pair.of(Boolean.TRUE, getRedirectPageWithMsg(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"register.crinfo.token.invalid", REGISTER_PAGE));
				}

				return Pair.of(Boolean.TRUE, getOTPRedirectView() + "?token=" + encodeToken);
			}
			catch (final OTPException ex)
			{
				LOG.info("[B2BRegistrationController] : OTPException " + ex.getMessage());
				return Pair.of(Boolean.TRUE, getRedirectPageWithMsg(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						MESSAGE_NOT_SENT_MESSAGE, CR_INFORMATION_REGISTRATION_PAGE));
			}
		}
		else
		{
			return Pair.of(Boolean.FALSE, null);
		}
	}

	protected String getRedirectPageWithMsg(final RedirectAttributes redirectAttributes, final String globalMessages,
			final String msgKey, final String redirectURL)
	{
		GlobalMessages.addFlashMessage(redirectAttributes, globalMessages, msgKey, null);
		return redirectURL;
	}

	@GetMapping(path = "/cr-info")
	public String crInfo(@RequestParam(name = "token", required = false)
	final String token, final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (!isTokenValid(token))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "register.crinfo.token.invalid",
					REGISTER_PAGE);
		}

		B2BRegistrationData b2bRegistrationData = getB2BRegistrationDataFromToken(token);

		if (!isRegistrationValidator(b2bRegistrationData))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"register.crinfo.b2bregistrationdata.invalid", REGISTER_PAGE);
		}

		final Optional<CompanyRegistrationInfoData> companyCrInfo = commercialRegistrationFacade
				.getCompanyCrInfoByCurrentBaseStore(b2bRegistrationData.getCompanyCr());

		if (companyCrInfo.isEmpty())
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"register.crinfo.companycrinfo.isempty", REGISTER_PAGE);
		}

		RegistrationCrNumberForm crNumberForm = new RegistrationCrNumberForm();
		crNumberForm.setToken(token);
		crNumberForm.setCrNumber(b2bRegistrationData.getCompanyCr());
		model.addAttribute("companyCrInfo", companyCrInfo.get());
		model.addAttribute("crNumberForm", crNumberForm);
		model.addAttribute("token", token);
		final ContentPageModel crInfoRegistrationPage = getContentPageForLabelOrId(CR_INFORMATION_REGISTRATION_CMS_PAGE);
		storeCmsPageInModel(model, crInfoRegistrationPage);

		setUpMetaDataForContentPage(model, crInfoRegistrationPage);
		return getViewForPage(model);
	}


	@PostMapping(path = "/cr-info")
	public String crInfoUpdate(final RegistrationCrNumberForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		String token = form.getToken();
		if (!isTokenValid(form.getToken()))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "register.crinfo.token.invalid",
					REGISTER_PAGE);
		}
		B2BRegistrationData b2bRegistrationData = getB2BRegistrationDataFromToken(token);

		if (!isRegistrationValidator(b2bRegistrationData))
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"register.crinfo.b2bregistrationdata.invalid", REGISTER_PAGE);
		}
		final Optional<CompanyRegistrationInfoData> companyCrInfo = commercialRegistrationFacade
				.getCompanyCrInfoByCurrentBaseStore(b2bRegistrationData.getCompanyCr());

		if (companyCrInfo.isEmpty())
		{
			return getRedirectPageWithMsg(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"register.crinfo.companycrinfo.isempty", REGISTER_PAGE);
		}

		getChangeCrNumberRegistrationValidator().validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "text.secureportal.register.field.company.invalid");

			form.setToken(token);
			form.setCrNumber(b2bRegistrationData.getCompanyCr());
			model.addAttribute("companyCrInfo", companyCrInfo.get());
			model.addAttribute("crNumberForm", form);

			final ContentPageModel updatePasswordPage = getContentPageForLabelOrId("cr-info-registration");
			storeCmsPageInModel(model, updatePasswordPage);
			setUpMetaDataForContentPage(model, updatePasswordPage);

			return getViewForPage(model);
		}
		// Now we are sure that the cr and and token are valid we can finally assign the new cr number with new token
		b2bRegistrationData.setCompanyCr(form.getCrNumber());
		String newToken = createTokenFromB2BRegistrationData(b2bRegistrationData);

		return CR_INFORMATION_REGISTRATION_PAGE + "?token=" + encodeToken(newToken);
	}

	private String encodeToken(final String token)
	{
		String encodeToken = Strings.EMPTY;
		try
		{
			encodeToken = URLEncoder.encode(token, StandardCharsets.UTF_8.toString());
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error(String.format("token from encodeToken could not be encode : %s", encodeToken), e);

			return getRegistrationView();
		}

		return encodeToken;
	}

	protected boolean isTokenValid(String token)
	{
		if (StringUtils.isBlank(token))
		{
			return false;
		}

		SecureToken data = null;

		try
		{
			data = getSecureTokenService().decryptData(token);
		}
		catch (Exception e)
		{
			return false;
		}

		if (getTokenValiditySeconds() > 0L)
		{
			final long delta = new Date().getTime() - data.getTimeStamp();
			if (delta / 1000 > getTokenValiditySeconds())
			{
				return false;
			}
		}

		if (data.getData() == null)
		{
			return false;
		}

		return true;
	}

	protected String createTokenFromB2BRegistrationData(B2BRegistrationData b2bRegistrationData)
	{
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();

		String b2bRegistrationDataJson = gson.toJson(b2bRegistrationData);

		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken data = new SecureToken(b2bRegistrationDataJson, timeStamp);
		return getSecureTokenService().encryptData(data);
	}

	protected B2BRegistrationData getB2BRegistrationDataFromToken(String token)
	{
		final SecureToken data = getSecureTokenService().decryptData(token);
		Gson gson = new GsonBuilder().create();
		B2BRegistrationData b2bRegistrationData = null;
		try
		{
			b2bRegistrationData = gson.fromJson(data.getData(), B2BRegistrationData.class);
		}
		catch (JsonSyntaxException e)
		{
			return null;
		}

		return b2bRegistrationData;
	}

	protected boolean isRegistrationValidator(final B2BRegistrationData b2bRegistrationData)
	{

		return b2bRegistrationData == null ? false : getSecurePortalRegistrationValidator().validate(b2bRegistrationData);
	}

	protected void populateCRPage(final B2BRegistrationData b2bRegistrationData, final Model model)
	{
		String companyCr = b2bRegistrationData.getCompanyCr();

		final Optional<CompanyRegistrationInfoData> companyCrInfo = commercialRegistrationFacade
				.getCompanyCrInfoByCurrentBaseStore(companyCr);
		if (companyCrInfo.isPresent())
		{
			model.addAttribute("companyCrInfo", companyCrInfo.get());
		}


	}

	protected String getOTPRedirectView()
	{
		return REDIRECT_PREFIX + PHONENUMBER_VERIFICATION_PAGE_LABEL;
	}

	@Override
	protected String getDefaultRegistrationPage(final Model model, final ContentPageModel contentPageModel)
	{
		populateModelCmsContent(model, contentPageModel);
		RegistrationForm registrationForm = new RegistrationForm();
		populateCitiesAndDistricts(model, registrationForm);
		return getRegistrationView();
	}

	private void populateCitiesAndDistricts(final Model model, RegistrationForm registrationForm)
	{
		Optional<CountryData> defaultCountry = countryFacade.getDefaultRegistrationCountryForCurrentSite();
		if (!defaultCountry.isPresent() || defaultCountry.get().getRegions() == null
				|| defaultCountry.get().getRegions().size() == 0)
		{
			return;
		}

		if (Strings.isBlank(registrationForm.getCompanyAddressCountryIso()))
		{
			registrationForm.setCompanyAddressCountryIso(defaultCountry.get().getIsocode());
		}

//		List<AreaData> districts = Collections.emptyList();
//
//		if (!Strings.isBlank(registrationForm.getCompanyAddressCity()))
//		{
//			districts = getAreaFacade().getRegistrationAreasByCityCode(registrationForm.getCompanyAddressCity());
//		}
//		model.addAttribute("districts", districts);


		model.addAttribute("defaultCountry", defaultCountry);
		model.addAttribute(registrationForm);
	}

	private void populateRegionsAndPOS(final Model model, RegistrationForm registrationForm)
	{
		Optional<CountryData> defaultCountry = countryFacade.getDefaultRegistrationCountryForCurrentSite();
		if (!defaultCountry.isPresent() || defaultCountry.get().getRegions() == null
				|| defaultCountry.get().getRegions().size() == 0)
		{
			return;
		}

		if (Strings.isBlank(registrationForm.getCompanyAddressCountryIso()))
		{
			registrationForm.setCompanyAddressCountryIso(defaultCountry.get().getIsocode());
		}
		if (Strings.isBlank(registrationForm.getCompanyAddressRegion()))
		{
			registrationForm.setCompanyAddressRegion(defaultCountry.get().getRegions().get(0).getIsocode());
		}
		model.addAttribute("regions", defaultCountry.get().getRegions());

		model.addAttribute("pos", pointOfServiceFacade.getPointOfServicesForRegion(registrationForm.getCompanyAddressCountryIso(),
				registrationForm.getCompanyAddressRegion(), baseStoreService.getCurrentBaseStore()));
		model.addAttribute("defaultCountry", defaultCountry);

		model.addAttribute(registrationForm);
	}

	@ModelAttribute("mobileCountries")
	public Collection<CountryData> getMobileCountries()
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		return mobileCountries.isPresent() ? mobileCountries.get() : null;
	}

	/**
	 * @param form
	 *           Form data as submitted by user
	 * @return A DTO object built from the form instance
	 */
	protected B2BRegistrationData convertFormToData(final RegistrationForm form)
	{
		final B2BRegistrationData registrationData = new B2BRegistrationData();
		BeanUtils.copyProperties(form, registrationData);
		registrationData.setName(StringUtils.trim(form.getName()));
		return registrationData;
	}


	protected Validator getChangeCrNumberRegistrationValidator()
	{
		return changeCrNumberRegistrationValidator;
	}

	@Override
	protected Validator getRegistrationValidator()
	{
		return registrationValidator;
	}

	@Override
	protected String getRegistrationView()
	{
		return MasdarsecureportalcustomaddonWebConstants.Views.REGISTRATION_PAGE;
	}

	@Override
	protected String getRegistrationCmsPage()
	{
		return MasdarsecureportalcustomaddonWebConstants.CMS_REGISTER_PAGE_NAME;
	}

	@Override
	protected void populateModelCmsContent(final Model model, final ContentPageModel contentPageModel)
	{

		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);

		final Breadcrumb registrationBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage(SCP_LINK_CREATE_ACCOUNT, null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(registrationBreadcrumbEntry));
	}

	@Override
	protected String getView()
	{
		return MasdarsecureportalcustomaddonWebConstants.Views.LOGIN_PAGE;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return HOME_REDIRECT;
	}

	/**
	 * @return the commercialRegistrationFacade
	 */
	protected CommercialRegistrationFacade getCommercialRegistrationFacade()
	{
		return commercialRegistrationFacade;
	}


	protected SecurePortalRegistrationValidator getSecurePortalRegistrationValidator()
	{
		return securePortalRegistrationValidator;
	}

	/**
	 * @return the secureTokenService
	 */
	protected SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}


	/**
	 * @return the areaFacade
	 */
	protected AreaFacade getAreaFacade()
	{
		return areaFacade;
	}

	protected long getTokenValiditySeconds()
	{
		return tokenValiditySeconds;
	}

}
