/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsecureportalcustomaddon.forms;


/**
 * Registration Company Registration Number form
 */
public class RegistrationCrNumberForm
{
	private String token;
	private String crNumber;

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public String getCrNumber()
	{
		return crNumber;
	}

	public void setCrNumber(String crNumber)
	{
		this.crNumber = crNumber;
	}

}
