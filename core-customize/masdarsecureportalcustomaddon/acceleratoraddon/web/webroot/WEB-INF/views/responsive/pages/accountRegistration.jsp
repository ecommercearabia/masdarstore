<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spuser" tagdir="/WEB-INF/tags/addons/masdarsecureportalcustomaddon/responsive/spuser"%>

<template:page pageTitle="${pageTitle}">
    <jsp:body>
        <div class="register__container">
            <div data-role="content" class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="register__section box m-t-20">
                        <c:url value="/register" var="submitAction" />
                        <spuser:register actionNameKey="register.submit" action="${submitAction}" />
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</template:page>