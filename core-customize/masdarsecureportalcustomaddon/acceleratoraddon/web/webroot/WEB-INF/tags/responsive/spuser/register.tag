<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true"
			  type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement"
		   tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="login-page__headline">
	<spring:theme code="text.secureportal.register.new.customer"/>
</div>
<p class="PinlineDiv">
	<spring:theme code="text.secureportal.register.description"/>
</p>
<c:url var="b2cRegisterUrl" value="${siteBaseUrl}/register" />
<a href="${b2cRegisterUrl}" class="linkRegPage">
	<spring:theme code="header.link.login.individual" text="To login as a Company, click here" />
</a>


<form:form method="post" id="registerForm"
		   modelAttribute="registrationForm" action="${action}">
	<div class="row">
		<div class="col-sm-6 hidden">
			<formElement:formSelectBox idKey="addresscountrydel"
									   labelKey="address.country" path="companyAddressCountryIso"
									   mandatory="true" skipBlank="true"
									   skipBlankMessageKey="address.selectCountry" items="${countries}"
									   itemValue="isocode" selectCSSClass="form-control"/>
		</div>
		<div class="col-sm-12">
			<formElement:formInputBox idKey="text.secureportal.register.name"
									  labelKey="text.secureportal.register.name" path="name"
									  inputCSS="form-control" mandatory="true"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<formElement:formSelectBoxCity idKey="registrationCityId"
										   labelKey="address.city" path="companyAddressCity" mandatory="true"
										   skipBlank="false" skipBlankMessageKey="address.selectCity"
										   items="${registrationCities}" itemValue="code"
										   selectCSSClass="form-control" datalivesearch="true"/>

			<div class="hidden">
				<formElement:formInputBox idKey="registrationDistrictsId"
					labelKey="text.secureportal.register.district" path="district"
					mandatory="true" inputCSS="form-control" />
			</div>
		</div>
		<div class="col-sm-6">

			<formElement:formInputBox idKey="companyCr"
									  labelKey="text.secureportal.register.companyCr" path="companyCr"
									  inputCSS="form-control" mandatory="true"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2 extension hidden">
			<formElement:formSelectBox
					idKey="text.secureportal.register.mobilecountry"
					labelKey="text.secureportal.register.mobilecountry"
					path="mobileCountry" mandatory="true" skipBlank="true"
					skipBlankMessageKey="address.selectCountry"
					items="${mobileCountries}" itemValue="isocode"
					selectCSSClass="form-control" itemLabel="name"/>
		</div>
		<div class="col-sm-6 phone">
			<formElement:formInputBox idKey="text.secureportal.register.mobile"
									  labelKey="text.secureportal.register.mobile" path="mobileNumber"
									  inputCSS="form-control" mandatory="true"
									  placeholder="text.secureportal.register.mobile.placeholder"/>
		</div>
		<div class="col-sm-6 phone">
			<formElement:formInputBox
					idKey="text.secureportal.register.companyVatNo"
					labelKey="text.secureportal.register.companyVatNo"
					path="companyVATNumber" inputCSS="form-control" mandatory="true"/>
		</div>

	</div>
	<div class="row">
		<div class="col-sm-6 extension">
			<formElement:formInputBox idKey="register.email"
									  labelKey="register.email" path="email"
									  inputCSS="form-control js-secureportal-orignal-register-email"
									  mandatory="true"/>
		</div>
		<div class="col-sm-6 extension">
			<formElement:formPasswordBox idKey="register.password"
										 labelKey="register.password" path="password" inputCSS="form-control"
										 mandatory="true"/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<input type="hidden" id="recaptchaChallangeAnswered"
				   value="${fn:escapeXml(requestScope.recaptchaChallangeAnswered)}"/>

			<div
					class="form_field-elements control-group js-recaptcha-masdarcaptchacustomaddon"></div>
		</div>
	</div>
	<div class="register-form-action row">
		<div class="col-xs-12 col-md-6 pull-right">
			<ycommerce:testId code="register_Register_button">
				<button data-theme="b"
						class="js-secureportal-register-button btn btn-primary btn-block">
					<spring:theme code='${actionNameKey}'/>
				</button>
			</ycommerce:testId>
		</div>
	</div>
</form:form>

