/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpayment.exception;

import com.masdar.masdarpayment.exception.enums.PaymentExceptionType;


/**
 *
 */
public class PaymentException extends Exception
{
	private PaymentExceptionType exceptionType;

	public PaymentException(final String message)
	{
		super(message);
	}

	public PaymentException(final String message, final PaymentExceptionType type)
	{
		super(message);
		this.exceptionType = type;
	}

	/**
	 * @return the type
	 */
	public PaymentExceptionType getExceptionType()
	{
		return exceptionType;
	}
}
