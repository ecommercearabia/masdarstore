/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;

import java.math.BigDecimal;
import java.util.Map;

import com.masdar.masdarpayment.hyperpay.HyperpayStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class AuthReplySubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "source [Map<String, Object>] source cannot be null");

		final AuthReplyData data = new AuthReplyData();

		final Integer ccAuthReplyReasonCode = 1;
		final String ccAuthReplyAuthorizationCode = getResultCode(source);
		final String ccAuthReplyCvCode = null;
		final Boolean cvnDecision = Boolean.TRUE;
		final String ccAuthReplyAvsCodeRaw = "YES";
		final String ccAuthReplyAvsCode = "YES";
		final BigDecimal ccAuthReplyAmount = getAmount(source);
		//		new BigDecimal((Double) source.get(HyperpayStatusReason.AMOUNT.getKey()));
		final String ccAuthReplyProcessorResponse = null;
		final String ccAuthReplyAuthorizedDateTime = String.valueOf(source.get(HyperpayStatusReason.TIMESTAMP.getKey()));


		data.setCcAuthReplyReasonCode(ccAuthReplyReasonCode);
		data.setCcAuthReplyAuthorizationCode(ccAuthReplyAuthorizationCode);
		data.setCcAuthReplyCvCode(ccAuthReplyCvCode);
		data.setCvnDecision(cvnDecision);
		data.setCcAuthReplyAvsCodeRaw(ccAuthReplyAvsCodeRaw);
		data.setCcAuthReplyAvsCode(ccAuthReplyAvsCode);
		data.setCcAuthReplyAmount(ccAuthReplyAmount);
		data.setCcAuthReplyProcessorResponse(ccAuthReplyProcessorResponse);
		data.setCcAuthReplyAuthorizedDateTime(ccAuthReplyAuthorizedDateTime);

		target.setAuthReplyData(data);
	}

	private BigDecimal getAmount(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.AMOUNT.getKey());
		if (object == null)
		{
			return null;
		}
		try
		{
			return BigDecimal.valueOf(Double.valueOf((String) object));
		}
		catch (final java.lang.NumberFormatException e)
		{
			return null;
		}
	}

	private String getResultCode(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.RESULT.getKey());
		if (object == null || !(object instanceof Map)
				|| ((Map<String, Object>) object).get(HyperpayStatusReason.RESULT_CODE.getKey()) == null)
		{
			return null;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		return String.valueOf(cardMap.get(HyperpayStatusReason.RESULT_CODE.getKey()));

	}
}
