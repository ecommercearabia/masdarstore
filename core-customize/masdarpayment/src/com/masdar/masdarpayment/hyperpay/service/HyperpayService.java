package com.masdar.masdarpayment.hyperpay.service;

import java.util.Map;

import com.masdar.masdarpayment.hyperpay.enums.TransactionStatus;
import com.masdar.masdarpayment.hyperpay.exception.HyperpayPaymentException;
import com.masdar.masdarpayment.hyperpay.model.HyperpayPaymentData;


/**
 * The Interface HyperpayService.
 *
 * @author monzer
 */
public interface HyperpayService {

	/**
	 * Prepare checkout (Payment) and return the response data.
	 *
	 * @param data the data
	 * @return the map
	 * @throws HyperpayPaymentException the hyperpay payment exception
	 */
	public Map<String, Object> prepareCheckout(final String accessToken, final String entityId, final String checkoutsURL,
			final HyperpayPaymentData data) throws HyperpayPaymentException;

	/**
	 * Gets the checkout status by providing  transaction Id.
	 *
	 * @param id the id
	 * @return the checkout status
	 * @throws HyperpayPaymentException the hyperpay payment exception
	 */
	public Map<String, Object> getCheckoutStatus(final String accessToken, final String entityId, final String checkoutsURL,
			final String id) throws HyperpayPaymentException;

	/**
	 * Search for transaction by id, and get the transaction report using payment id .
	 *
	 * @param paymentId the payment id
	 * @return the map
	 * @throws HyperpayPaymentException the hyperpay payment exception
	 */
	public Map<String, Object> searchForTransactionById(final String accessToken, final String entityId, final String queryURL,
			final String paymentId) throws HyperpayPaymentException;

	/**
	 * Refund payment,make a refund transaction using payment id and HyperpayPaymentData .
	 *
	 * @param data the data
	 * @param paymentId the payment id
	 * @return the map
	 * @throws HyperpayPaymentException the hyperpay payment exception
	 */
	public Map<String, Object> refundPayment(final String accessToken, final String entityId, final String paymentsURL,
			final HyperpayPaymentData data, final String paymentId) throws HyperpayPaymentException;

	/**
	 * Reverse payment , cancel payment .
	 *
	 * @param data the data
	 * @param paymentId the payment id
	 * @return the map
	 * @throws HyperpayPaymentException the hyperpay payment exception
	 */
	public Map<String, Object> reversePayment(final String accessToken, final String entityId, final String paymentsURL,
			final HyperpayPaymentData data, final String paymentId)  throws HyperpayPaymentException;


	public TransactionStatus getTransactionStatus(Map<String, Object> map);

	public TransactionStatus getTransactionStatus(final String accessToken, final String entityId, final String queryURL,
			final String paymentId);
}
