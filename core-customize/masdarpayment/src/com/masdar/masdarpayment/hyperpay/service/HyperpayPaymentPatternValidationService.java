package com.masdar.masdarpayment.hyperpay.service;

import com.masdar.masdarpayment.hyperpay.enums.HyperpayPaymentDataParameters;


/**
 * The Interface HyperpayPaymentPatternValidationService.
 *
 * @author monzer
 */
public interface HyperpayPaymentPatternValidationService {

	/**
	 * Validate.
	 *
	 * @param param the param
	 * @param pattern the pattern
	 * @return true, if param matches pattern
	 */
	boolean validate(String param, HyperpayPaymentDataParameters hyperpayPaymentDataParameters);

}
