package com.masdar.masdarpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum TransactionCategory {
	EC, MO, TO, RC, IN, PO, PM;
}
