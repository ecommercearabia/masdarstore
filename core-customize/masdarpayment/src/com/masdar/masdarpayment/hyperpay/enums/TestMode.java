package com.masdar.masdarpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum TestMode {
	EXTERNAL, INTERNAL
}
