/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpayment.hyperpay.enums;

/**
 * @author monzer
 */
public enum TransactionStatus
{
	SUCCESS, FAILED;
}