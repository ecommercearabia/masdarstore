package com.masdar.masdarpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum CustomerSex {
	
	M, F;
	
}
