package com.masdar.masdarpayment.hyperpay.exception;

/**
 *
 * @author monzer
 *
 */
public enum ExceptionType {

	BAD_REQUEST,
	INVALID_PARAMETER_FORMAT,
	SERVER_ERROR, ORDER_ALREADY_PAID, ORDER_PENDING;

}
