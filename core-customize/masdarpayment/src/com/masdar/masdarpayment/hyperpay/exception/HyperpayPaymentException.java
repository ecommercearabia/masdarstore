package com.masdar.masdarpayment.hyperpay.exception;

import com.masdar.masdarpayment.exception.PaymentException;


/**
 *
 * @author monzer
 *
 */
public class HyperpayPaymentException extends PaymentException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private final String message;

	@SuppressWarnings("unused")
	private final ExceptionType type;

	public HyperpayPaymentException(final ExceptionType type, final String message) {
		super(message);
		this.message = message;
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public ExceptionType getType()
	{
		return type;
	}



}
