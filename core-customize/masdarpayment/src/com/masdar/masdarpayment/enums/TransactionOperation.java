/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpayment.enums;

/**
 *
 */
public enum TransactionOperation
{
	CAPTURE, CANCEL, REFUND_STANDALONE;
}
