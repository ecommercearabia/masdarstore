/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpayment.dao.impl;

import com.masdar.masdarpayment.dao.PaymentProviderDao;
import com.masdar.masdarpayment.model.MadaPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultMadaPaymentProviderDao.
 */
public class DefaultMadaPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default Mada provider dao.
	 */
	public DefaultMadaPaymentProviderDao()
	{
		super(MadaPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return MadaPaymentProviderModel._TYPECODE;
	}

}
