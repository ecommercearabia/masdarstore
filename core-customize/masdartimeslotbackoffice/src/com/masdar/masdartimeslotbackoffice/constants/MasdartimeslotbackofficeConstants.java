/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.masdar.masdartimeslotbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class MasdartimeslotbackofficeConstants extends GeneratedMasdartimeslotbackofficeConstants
{
	public static final String EXTENSIONNAME = "masdartimeslotbackoffice";

	private MasdartimeslotbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
