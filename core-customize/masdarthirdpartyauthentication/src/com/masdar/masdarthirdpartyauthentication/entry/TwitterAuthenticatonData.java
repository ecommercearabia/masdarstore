/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.entry;

/**
 *
 */
public class TwitterAuthenticatonData extends TwitterCredinatial
{
	private String oauthToken;
	private String oauthTokenSecret;
	private String oauthVerifier;

	/**
	 *
	 */

	/**
	 * @return the oauthToken
	 */
	public String getOauthToken()
	{
		return oauthToken;
	}

	/**
	 *
	 */
	public TwitterAuthenticatonData(final String consumerKey, final String consumerSecret, final String oauthToken,
			final String oauthTokenSecret, final String oauthVerifier)
	{
		super(consumerKey, consumerSecret);
		this.oauthToken = oauthToken;
		this.oauthTokenSecret = oauthTokenSecret;
		this.oauthVerifier = oauthVerifier;
	}

	/**
	 * @param oauthToken
	 *           the oauthToken to set
	 */
	public void setOauthToken(final String oauthToken)
	{
		this.oauthToken = oauthToken;
	}

	/**
	 * @return the oauthTokenSecret
	 */
	public String getOauthTokenSecret()
	{
		return oauthTokenSecret;
	}

	/**
	 * @param oauthTokenSecret
	 *           the oauthTokenSecret to set
	 */
	public void setOauthTokenSecret(final String oauthTokenSecret)
	{
		this.oauthTokenSecret = oauthTokenSecret;
	}

	/**
	 * @return the oauthVerifier
	 */
	public String getOauthVerifier()
	{
		return oauthVerifier;
	}

	/**
	 * @param oauthVerifier
	 *           the oauthVerifier to set
	 */
	public void setOauthVerifier(final String oauthVerifier)
	{
		this.oauthVerifier = oauthVerifier;
	}


}
