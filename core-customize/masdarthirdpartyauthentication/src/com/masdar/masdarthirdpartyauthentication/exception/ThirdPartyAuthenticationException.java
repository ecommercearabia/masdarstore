/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.exception;

import com.masdar.masdarthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;


/**
 * The Class ThirdPartyAuthenticationException.
 *
 * @author monzer
 */
public class ThirdPartyAuthenticationException extends Exception
{

	/** The type. */
	private final ThirdPartyAuthenticationExceptionType type;

	/**
	 * Instantiates a new third party authentication exception.
	 *
	 * @param type
	 *           the type
	 * @param msg
	 *           the msg
	 */
	public ThirdPartyAuthenticationException(final ThirdPartyAuthenticationExceptionType type, final String msg)
	{
		super(msg);
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public ThirdPartyAuthenticationExceptionType getType()
	{
		return type;
	}



}
