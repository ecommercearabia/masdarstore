/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.exception.enums;

/**
 * The Enum ThirdPartyAuthenticationExceptionType.
 */
public enum ThirdPartyAuthenticationExceptionType
{

	/** The bad request. */
	BAD_REQUEST("bad request"),
	/** The non authorized. */
	NON_AUTHORIZED("user not authorized"),
	/** The invalid provider. */
	INVALID_PROVIDER("provider is invalid"),
	/** The callback not authprized. */
	CALLBACK_NOT_AUTHPRIZED("callback url is not authorized"),
	/** The invalid provider type. */
	INVALID_PROVIDER_TYPE("provider type is invalid"),
	/** The method not supported. */
	METHOD_NOT_SUPPORTED("method is not supported"),
	/** The provider not found. */
	PROVIDER_NOT_FOUND("no such provider"),
	/** The redirect error. */
	REDIRECT_ERROR("error in redirect"), INVALID_AUTH_TOKEN("Invalid authentication token");

	/** The msg. */
	private String msg;

	/**
	 * Instantiates a new third party authentication exception type.
	 *
	 * @param msg
	 *           the msg
	 */
	private ThirdPartyAuthenticationExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

}
