/**
 *
 */
package com.masdar.masdarthirdpartyauthentication.facades.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Optional;

import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyUserData;
import com.masdar.masdarthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.masdar.masdarthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.masdar.masdarthirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListData;
import com.masdar.masdarthirdpartyauthentication.provider.apple.AppleAuthenticationProviderData;
import com.masdar.masdarthirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderData;
import com.masdar.masdarthirdpartyauthentication.provider.google.GoogleAuthenticationProviderData;


/**
 * @author monzer
 *
 */
public interface ThirdPartyAuthenticationProviderFacade
{

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderByCurrentSite();

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySiteUid(String siteUid);

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySite(CMSSiteModel site);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderByCurrentSite();

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySiteUid(String siteUid);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderByCurrentSite();

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySiteUid(String siteUid);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersByCurrentSite();

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySiteUid(String siteUid);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySite(CMSSiteModel site);

	Optional<ThirdPartyUserData> getThirdPartyUserData(Object authData, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyToken(Object token, ThirdPartyAuthenticationType type, CMSSiteModel site)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyTokenByCurrentSite(Object token, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	void saveUser(CustomerData customer);

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationData(Object authData, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

}
