/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.strategy.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.masdar.masdarthirdpartyauthentication.entry.TwitterFormData;
import com.masdar.masdarthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.masdar.masdarthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.masdar.masdarthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.masdar.masdarthirdpartyauthentication.model.FacebookAuthenticationProviderModel;
import com.masdar.masdarthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.masdar.masdarthirdpartyauthentication.service.FacebookService;
import com.masdar.masdarthirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;


/**
 *
 */
public class DefaultFacebookThirdPartyAuthenticationStrategy implements ThirdPartyAuthenticationStrategy
{
	@Resource(name = "facebookService")
	private FacebookService facebookService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (provider instanceof FacebookAuthenticationProviderModel)
		{
			final String appId = ((FacebookAuthenticationProviderModel) provider).getAppId();
			final String appSecret = ((FacebookAuthenticationProviderModel) provider).getAppSecret();
			final ThirdPartyAuthenticationProviderData thirdPartyAuthenticationProviderData = new ThirdPartyAuthenticationProviderData();
			thirdPartyAuthenticationProviderData.setId(appId);
			thirdPartyAuthenticationProviderData.setType(ThirdPartyAuthenticationType.FACEBOOK.getCode());
			thirdPartyAuthenticationProviderData.setActive(provider.isActive());
			thirdPartyAuthenticationProviderData.setEnabledForMobile(provider.isActive() && provider.isEnabledForMobile());
			thirdPartyAuthenticationProviderData.setEnabledForStoreFront(provider.isActive() && provider.isEnabledForStoreFront());
			return Optional.ofNullable(thirdPartyAuthenticationProviderData);
		}
		else
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		if (!(provider instanceof FacebookAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final Optional<ThirdPartyAuthenticationUserData> thirdPartyAuthenticationUserData = facebookService.getData((String) data,
				((FacebookAuthenticationProviderModel) provider).getAppSecret());

		if (!thirdPartyAuthenticationUserData.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		return thirdPartyAuthenticationUserData;
	}

	@Override
	public Optional<TwitterFormData> getFormData(final Object data, final ThirdPartyAuthenticationProviderModel provider,
			final String callbackUrl)
			throws ThirdPartyAuthenticationException
	{
		throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED,
				ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED.getMsg());
	}

	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object data, final Object token,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || token == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		if (!(provider instanceof FacebookAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		final FacebookAuthenticationProviderModel facebookProvider = (FacebookAuthenticationProviderModel) provider;
		return facebookService.verifyThirdPartyAccessToken(String.valueOf(data), String.valueOf(token),
				facebookProvider.getAppSecret());
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		return this.getThirdPartyUserData(data, provider);
	}

}
