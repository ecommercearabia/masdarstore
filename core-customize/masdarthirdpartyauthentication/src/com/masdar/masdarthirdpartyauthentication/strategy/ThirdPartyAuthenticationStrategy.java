/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.strategy;

import java.util.Optional;

import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.masdar.masdarthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.masdar.masdarthirdpartyauthentication.entry.TwitterFormData;
import com.masdar.masdarthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.masdar.masdarthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationStrategy
{
	Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(Object data,
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getFormData(Object data, ThirdPartyAuthenticationProviderModel provider, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;
}
