/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.masdar.masdarthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 * The Interface ThirdPartyAuthenticationProviderService.
 */
public interface ThirdPartyAuthenticationProviderService
{

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(String cmsSiteUid, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @param providerClass
	 *           the provider class
	 * @return the active
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActive(CMSSiteModel cmsSiteModel, final Class<?> providerClass);

	/**
	 * Gets the active by current site.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active by current site
	 */
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveByCurrentSite(final Class<?> providerClass);
}