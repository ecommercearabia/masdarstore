/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.service.impl;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.api.client.util.Preconditions;
import com.masdar.core.enums.MaritalStatus;
import com.masdar.core.model.NationalityModel;
import com.masdar.core.nationality.service.NationalityService;
import com.masdar.facades.customer.facade.CustomCustomerFacade;
import com.masdar.masdarthirdpartyauthentication.service.ThirdPartyUserService;


/**
 *
 */
public class DefaultThirdPartyUserService implements ThirdPartyUserService
{

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "titleConverter")
	private Converter<TitleModel, TitleData> titleConverter;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;


	@Resource(name = "nationalityService")
	NationalityService nationalityService;

	@Override
	public Boolean isUserExist(final String cutomerId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(cutomerId), "User is null");
		return customCustomerFacade.isCustomerExists(cutomerId);
	}

	@Override
	public void saveUser(final CustomerData user)
	{
		Preconditions.checkArgument(user != null, "User is null");
		Preconditions.checkArgument(user.getCustomerId() != null, "User ID Is null");
		Preconditions.checkArgument(user.getUid() != null, "User email is null");
		Preconditions.checkArgument(user.getMobileCountry() != null, "User Mobile country is null");
		Preconditions.checkArgument(user.getMobileNumber() != null, "User Mobile Numberis null");
		Preconditions.checkArgument(user.getFirstName() != null, "User First name is null");
		Preconditions.checkArgument(user.getLastName() != null, "User Last Name is null");
		Preconditions.checkArgument(!this.isUserExist(user.getUid()), "User exists!");
		final CustomerModel model = modelService.create(CustomerModel.class);

		final TitleModel title = userService.getTitleForCode(user.getTitleCode());
		model.setTitle(title);
		model.setName(user.getName());
		final CountryModel country = commonI18NService.getCountry(user.getMobileCountry().getIsocode());
		model.setMobileCountry(country);
		model.setMobileNumber(user.getMobileNumber());
		model.setUid(user.getUid());
		model.setCustomerID(user.getCustomerId());
		model.setType(CustomerType.THIRD_PARTY);
		model.setThirdPartyType(user.getThirdPartyType());
		model.setNationalityID(user.getNationalityID());
		model.setBirthOfDate(user.getBirthDate());
		populateNationality(user, model);
		if (user.getMaritalStatus() != null && StringUtils.isNotBlank(user.getMaritalStatus().getCode()))
		{
			model.setMaritalStatus(MaritalStatus.valueOf(user.getMaritalStatus().getCode()));
		}
		modelService.save(model);
		modelService.refresh(model);

		eventService.publishEvent(initializeEvent(new RegisterEvent(), model));
	}

	private void populateNationality(final CustomerData user, final CustomerModel model)
	{
		if (user.getNationality() != null && StringUtils.isNotBlank(user.getNationality().getCode()))
		{
			final Optional<NationalityModel> isNationalityExists = nationalityService.get(user.getNationality().getCode());
			if (isNationalityExists.isPresent())
			{
				model.setNationality(isNationalityExists.get());
			}
		}
	}

	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event, final CustomerModel customerModel)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
