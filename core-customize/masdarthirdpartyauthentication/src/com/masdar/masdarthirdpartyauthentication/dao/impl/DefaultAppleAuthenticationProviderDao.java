/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.dao.impl;

import com.masdar.masdarthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.masdar.masdarthirdpartyauthentication.model.AppleAuthenticationProviderModel;


/**
 * @author monzer
 */
public class DefaultAppleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	*
	*/
	public DefaultAppleAuthenticationProviderDao()
	{
		super(AppleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AppleAuthenticationProviderModel._TYPECODE;
	}

}
