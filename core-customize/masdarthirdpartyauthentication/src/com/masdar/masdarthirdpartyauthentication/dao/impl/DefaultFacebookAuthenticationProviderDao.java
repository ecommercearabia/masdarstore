/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.dao.impl;

import com.masdar.masdarthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.masdar.masdarthirdpartyauthentication.model.FacebookAuthenticationProviderModel;

/**
 *
 */
public class DefaultFacebookAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	 *
	 */
	public DefaultFacebookAuthenticationProviderDao()
	{
		super(FacebookAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return FacebookAuthenticationProviderModel._TYPECODE;
	}

}
