/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarthirdpartyauthentication.dao.impl;

import com.masdar.masdarthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.masdar.masdarthirdpartyauthentication.model.TwitterAuthenticationProviderModel;


/**
 *
 */
public class DefaultTwitterAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultTwitterAuthenticationProviderDao()
	{
		super(TwitterAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return TwitterAuthenticationProviderModel._TYPECODE;
	}


}
