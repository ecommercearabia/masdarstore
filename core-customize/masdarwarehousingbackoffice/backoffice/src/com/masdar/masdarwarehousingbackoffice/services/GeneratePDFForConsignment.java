/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.services;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface GeneratePDFForConsignment
{

	public void generatePDFEInvoice(final ConsignmentModel consignmentModel);
}
