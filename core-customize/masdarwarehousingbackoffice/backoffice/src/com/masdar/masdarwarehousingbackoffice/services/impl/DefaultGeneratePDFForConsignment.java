/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;
import de.hybris.platform.warehousing.process.impl.DefaultConsignmentProcessService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdaritextservices.beans.EmbeddedFileInfo;
import com.masdar.masdaritextservices.beans.EmbeddedFileInfo.FileType;
import com.masdar.masdaritextservices.services.ItextPDFService;
import com.masdar.masdarwarehousingbackoffice.services.GeneratePDFForConsignment;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultGeneratePDFForConsignment implements GeneratePDFForConsignment
{
	private static final String PDF_MIME = "application/pdf";
	private static final String PACK_LABEL_DOCUMENT_TEMPLATE = "PackLabelDocumentTemplate";


	protected static final Logger LOG = LoggerFactory.getLogger(DefaultGeneratePDFForConsignment.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "itextPDFService")
	private ItextPDFService itextPDFService;

	@Resource(name = "printMediaService")
	private PrintMediaService printMediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "consignmentBusinessProcessService")
	private DefaultConsignmentProcessService consignmentBusinessProcessService;

	@Override
	public void generatePDFEInvoice(final ConsignmentModel consignmentModel)
	{
		if (consignmentModel.getEInvoicePDF() != null)
		{
			return;
		}

		final MediaModel packLabelMedia = getPrintMediaService().getMediaForTemplate(PACK_LABEL_DOCUMENT_TEMPLATE,
				getConsignmentBusinessProcessService().getConsignmentProcess(consignmentModel));

		final String generatedHtmlMediaTemplate = getPrintMediaService().generateHtmlMediaTemplate(packLabelMedia);

		final List<EmbeddedFileInfo> embeddedFiles = generateListOfEmbeddedFiles(consignmentModel);
		final byte[] pdfA1ByteArray = getItextPDFService().createPDFA(generatedHtmlMediaTemplate, embeddedFiles);

		final MediaModel createMediaFile = createMediaFile(
				"InvoicePDF-cons-" + consignmentModel.getInvoiceNumber() + "-" + LocalDateTime.now().toString() + ".pdf", PDF_MIME,
				pdfA1ByteArray);

		getModelService().save(createMediaFile);
		consignmentModel.setEInvoicePDF(createMediaFile);
		getModelService().save(consignmentModel);

	}


	private MediaModel createMediaFile(final String mediaName, final String mime, final byte[] byteArray)
	{
		final MediaModel media = getModelService().create(MediaModel.class);
		media.setCode(mediaName);
		media.setMime(mime);
		media.setRealFileName(mediaName);

		try
		{
			media.setCatalogVersion(getCatalogVersionService().getCatalogVersion("Default", "Online"));
			getModelService().save(media);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();

		try (InputStream dataStream = new ByteArrayInputStream(byteArray);)
		{
			getMediaService().setStreamForMedia(media, dataStream, mediaName, mime, mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate Media from byteArray", e);
			return null;
		}
		return media;
	}

	/**
	 * @param consignmentModel
	 * @return
	 */
	private List<EmbeddedFileInfo> generateListOfEmbeddedFiles(final ConsignmentModel consignmentModel)
	{
		final MediaModel eInvoiceEmmbedXML = consignmentModel.getEInvoiceEmmbedXML();
		if (eInvoiceEmmbedXML == null)
		{
			LOG.error("EInvoice XML file cannot be empty");
			return null;
		}
		final EmbeddedFileInfo embeddedFileInfo = new EmbeddedFileInfo();
		final byte[] dataFromMedia = getMediaService().getDataFromMedia(eInvoiceEmmbedXML);
		embeddedFileInfo.setData(dataFromMedia);
		embeddedFileInfo.setDescription(embeddedFileInfo.getDescription());
		embeddedFileInfo.setName(eInvoiceEmmbedXML.getRealFileName());
		embeddedFileInfo.setMimeType(FileType.ApplicationXml);
		return Arrays.asList(embeddedFileInfo);
	}

	/**
	 * Gets the {@link MediaFolderModel} to save the generated Media
	 *
	 * @return the {@link MediaFolderModel}
	 */
	private MediaFolderModel getDocumentMediaFolder()
	{
		return getMediaService().getFolder("documents");
	}

	/**
	 * @return the printMediaService
	 */
	private PrintMediaService getPrintMediaService()
	{
		return printMediaService;
	}

	/**
	 * @return the consignmentBusinessProcessService
	 */
	private DefaultConsignmentProcessService getConsignmentBusinessProcessService()
	{
		return consignmentBusinessProcessService;
	}

	/**
	 * @return the itextPDFService
	 */
	private ItextPDFService getItextPDFService()
	{
		return itextPDFService;
	}

	/**
	 * @return the modelService
	 */
	private ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}


	/**
	 * @return the mediaService
	 */
	public MediaService getMediaService()
	{
		return mediaService;
	}

}
