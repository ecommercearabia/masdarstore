/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.masdar.masdarwarehousingbackoffice.services;

/**
 * Hello World MasdarwarehousingbackofficeService
 */
public class MasdarwarehousingbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
