/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.dtos;

import de.hybris.platform.ordersplitting.model.WarehouseModel;


/**
 * @author mnasro
 *
 */
public class WarehouseLocationsDto
{

	private String code;
	private WarehouseModel warehouseModel;
	private String warehouseCode;




	/**
	 * @param code
	 * @param warehouseModel
	 * @param warehouseCode
	 */
	public WarehouseLocationsDto(final String code, final WarehouseModel warehouseModel, final String warehouseCode)
	{
		super();
		this.code = code;
		this.warehouseModel = warehouseModel;
		this.warehouseCode = warehouseCode;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the warehouseModel
	 */
	public WarehouseModel getWarehouseModel()
	{
		return warehouseModel;
	}

	/**
	 * @param warehouseModel
	 *           the warehouseModel to set
	 */
	public void setWarehouseModel(final WarehouseModel warehouseModel)
	{
		this.warehouseModel = warehouseModel;
	}

	/**
	 * @return the warehouseCode
	 */
	public String getWarehouseCode()
	{
		return warehouseCode;
	}

	/**
	 * @param warehouseCode
	 *           the warehouseCode to set
	 */
	public void setWarehouseCode(final String warehouseCode)
	{
		this.warehouseCode = warehouseCode;
	}


}
