package com.masdar.masdarwarehousingbackoffice.dtos;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.warehousing.enums.DeclineReason;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.zkoss.zul.ListModelArray;


public class ConsignmentEntryToReallocateDto
{
	private ConsignmentEntryModel consignmentEntry = null;
	private Long quantityToReallocate;
	private String declineConsignmentEntryComment;
	private DeclineReason selectedReason;
	private WarehouseLocationsDto selectedLocation;
	private ListModelArray declineReasonsModel = new ListModelArray(new ArrayList());
	private ListModelArray possibleLocationsModel = new ListModelArray(new ArrayList());

	public ConsignmentEntryToReallocateDto(final ConsignmentEntryModel consignmentEntry, final List<String> reasons,
			final Set<WarehouseLocationsDto> locations)
	{
		this.consignmentEntry = consignmentEntry;
		this.quantityToReallocate = 0L;
		this.declineReasonsModel = new ListModelArray(reasons);
		this.possibleLocationsModel = new ListModelArray(locations.toArray());
	}

	public ConsignmentEntryModel getConsignmentEntry()
	{
		return this.consignmentEntry;
	}

	public void setConsignmentEntry(final ConsignmentEntryModel consignmentEntry)
	{
		this.consignmentEntry = consignmentEntry;
	}

	public Long getQuantityToReallocate()
	{
		return this.quantityToReallocate;
	}

	public void setQuantityToReallocate(final Long quantityToReallocate)
	{
		this.quantityToReallocate = quantityToReallocate;
	}

	public ListModelArray getDeclineReasonsModel()
	{
		return this.declineReasonsModel;
	}

	public void setDeclineReasonsModel(final ListModelArray declineReasonsModel)
	{
		this.declineReasonsModel = declineReasonsModel;
	}

	public ListModelArray getPossibleLocationsModel()
	{
		return this.possibleLocationsModel;
	}

	public void setPossibleLocationsModel(final ListModelArray possibleLocationsModel)
	{
		this.possibleLocationsModel = possibleLocationsModel;
	}

	public DeclineReason getSelectedReason()
	{
		return this.selectedReason;
	}

	public void setSelectedReason(final DeclineReason selectedReason)
	{
		this.selectedReason = selectedReason;
	}

	public WarehouseLocationsDto getSelectedLocation()
	{
		return this.selectedLocation;
	}

	public void setSelectedLocation(final WarehouseLocationsDto selectedLocation)
	{
		this.selectedLocation = selectedLocation;
	}

	public String getDeclineConsignmentEntryComment()
	{
		return this.declineConsignmentEntryComment;
	}

	public void setDeclineConsignmentEntryComment(final String declineConsignmentEntryComment)
	{
		this.declineConsignmentEntryComment = declineConsignmentEntryComment;
	}
}
