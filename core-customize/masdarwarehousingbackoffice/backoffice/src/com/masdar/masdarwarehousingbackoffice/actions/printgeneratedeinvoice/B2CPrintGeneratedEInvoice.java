package com.masdar.masdarwarehousingbackoffice.actions.printgeneratedeinvoice;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class B2CPrintGeneratedEInvoice extends AbstractPrintGeneratedEInvoice
{
	private static final Logger LOG = LoggerFactory.getLogger(B2CPrintGeneratedEInvoice.class);

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}

