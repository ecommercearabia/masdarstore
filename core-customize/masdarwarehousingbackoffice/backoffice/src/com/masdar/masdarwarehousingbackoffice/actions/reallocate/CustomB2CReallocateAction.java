package com.masdar.masdarwarehousingbackoffice.actions.reallocate;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CustomB2CReallocateAction extends AbstractCustomReallocateAction
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomB2CReallocateAction.class);


	@Override
	protected Logger getLogger()
	{
		return LOG;
	}


	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}
}

