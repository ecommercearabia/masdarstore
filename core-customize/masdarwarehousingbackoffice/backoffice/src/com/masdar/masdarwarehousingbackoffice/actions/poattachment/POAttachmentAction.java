/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.poattachment;

import de.hybris.platform.commerceservices.enums.SiteChannel;


/**
 * @author Husam
 *
 */
public class POAttachmentAction extends AbstractPOAttachmentAction
{

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

}
