/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printshippinglabel;

import de.hybris.platform.commerceservices.enums.SiteChannel;


/**
 * @author Husam
 *
 */
public class CustomB2CPrintShippingLabelAction extends AbstractCustomPrintShippingLabelAction
{
	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}

}
