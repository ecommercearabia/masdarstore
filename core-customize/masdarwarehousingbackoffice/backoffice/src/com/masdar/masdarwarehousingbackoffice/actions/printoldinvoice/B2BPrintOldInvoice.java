package com.masdar.masdarwarehousingbackoffice.actions.printoldinvoice;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class B2BPrintOldInvoice extends AbstractPrintOldInvoice
{
	private static final Logger LOG = LoggerFactory.getLogger(B2BPrintOldInvoice.class);

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}

