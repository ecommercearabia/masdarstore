package com.masdar.masdarwarehousingbackoffice.actions.printeinvoice;


import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;
import de.hybris.platform.warehousing.process.impl.DefaultConsignmentProcessService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.labels.LabelUtils;
import com.masdar.masdaritextservices.services.ItextPDFService;
import com.masdar.masdarwarehousingbackoffice.services.GeneratePDFForConsignment;



/**
 * @author Husam
 *
 */
public abstract class AbstractPrintEInvoiceAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	private static final String ERROR_MESSAGE = "Error Message: ";

	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "itextPDFService")
	private ItextPDFService itextPDFService;

	@Resource(name = "printMediaService")
	private PrintMediaService printMediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "consignmentBusinessProcessService")
	private DefaultConsignmentProcessService consignmentBusinessProcessService;

	@Resource(name = "generatePDFForConsignment")
	private GeneratePDFForConsignment generatePDFForConsignment;


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();
		if (!getSiteChannel().equals(order.getSite().getChannel()))
		{
			return false;
		}

		return consignment.getEInvoiceEmmbedXML() != null && consignment.getInvoiceQRCode() != null;
	}


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		if (consignment == null)
		{
			Messagebox.show(ERROR_MESSAGE + "Pack request or consignment is empty!");
			return new ActionResult<>(ActionResult.ERROR);
		}

		getGeneratePDFForConsignment().generatePDFEInvoice(consignment);
		getModelService().refresh(consignment);

		if (consignment.getEInvoicePDF() != null)
		{
			printDocumentByMedia(consignment.getEInvoicePDF());
			return new ActionResult<>(ActionResult.SUCCESS);
		}

		return new ActionResult<>(ActionResult.ERROR);
	}


	/**
	 *
	 */
	private void printDocumentByMedia(final MediaModel pdfMedia)
	{

		// TODO: Validate that pdf media is actuallty PDF
		// TODO: Validate that the media have a valid URL
		final String width = "950";
		final String height = "800";
		final StringBuilder sb = new StringBuilder();

		sb.append("var myWindow = window.open('").append(pdfMedia.getURL()).append("','','width=").append(width).append(",height=")
				.append(height).append(",scrollbars=yes');");

		final String popup = sb.toString();

		Clients.evalJavaScript(popup);

	}

	protected String resolveLabel(final String labelKey)
	{
		final String defaultValue = LabelUtils.getFallbackLabel(labelKey);
		return Labels.getLabel(labelKey, defaultValue);
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the generatePDFForConsignment
	 */
	public GeneratePDFForConsignment getGeneratePDFForConsignment()
	{
		return generatePDFForConsignment;
	}

}

