package com.masdar.masdarwarehousingbackoffice.actions.printoldinvoice;


import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousingbackoffice.labels.strategy.ConsignmentPrintDocumentStrategy;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.core.user.CockpitUserService;


/**
 * @author Husam
 *
 */
public abstract class AbstractPrintOldInvoice implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final String ERROR_MESSAGE = "Error Message: ";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "consignmentPrintPackSlipStrategy")
	private ConsignmentPrintDocumentStrategy consignmentPrintPackSlipStrategy;

	@Resource(name = "cockpitUserService")
	private CockpitUserService cockpitUserService;

	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null)
		{
			return false;
		}

		if (!getSiteChannel().equals(consignment.getOrder().getSite().getChannel()))
		{
			return false;
		}

		final String currentUser = getCockpitUserService().getCurrentUser();
		if (Strings.isBlank(currentUser) || !getCockpitUserService().isAdmin(currentUser))
		{
			return false;
		}

		return consignment.getOrder().getStore().isEnablePrintOldInvoice();
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> context)
	{
		this.getConsignmentPrintPackSlipStrategy().printDocument(context.getData());
		return new ActionResult<>(ActionResult.SUCCESS);
	}

	/**
	 * @return the consignmentPrintPackSlipStrategy
	 */
	public ConsignmentPrintDocumentStrategy getConsignmentPrintPackSlipStrategy()
	{
		return consignmentPrintPackSlipStrategy;
	}

	/**
	 * @return the cockpitUserService
	 */
	public CockpitUserService getCockpitUserService()
	{
		return cockpitUserService;
	}


}

