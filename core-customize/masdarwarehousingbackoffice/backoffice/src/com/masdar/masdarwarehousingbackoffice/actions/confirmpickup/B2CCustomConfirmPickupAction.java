/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.confirmpickup;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class B2CCustomConfirmPickupAction extends AbstractCustomConfirmPickUpAction
{
	private static Logger LOG = LoggerFactory.getLogger(B2CCustomConfirmPickupAction.class);

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}
}
