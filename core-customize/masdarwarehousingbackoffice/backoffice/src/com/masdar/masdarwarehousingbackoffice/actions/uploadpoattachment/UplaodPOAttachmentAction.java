/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.uploadpoattachment;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;

import javax.annotation.Resource;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class UplaodPOAttachmentAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		if (consignmentModelActionContext == null || consignmentModelActionContext.getData() == null)
		{
			return new ActionResult<>(ActionResult.ERROR);
		}
		sendOutput("uploadpoattachment", consignmentModelActionContext.getData());
		return new ActionResult<>(ActionResult.SUCCESS);
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		if (consignmentModelActionContext == null || consignmentModelActionContext.getData() == null)
		{
			return false;
		}

		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		return consignment.getOrder() != null
				&& !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
	}


}
