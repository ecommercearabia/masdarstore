/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.ship;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.warehousingbackoffice.actions.ship.ConfirmShippedConsignmentAction;

import java.util.Date;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.masdar.core.context.SerialNumberConfigurationContext;
import com.masdar.core.enums.SerialNumberSource;
import com.masdar.masdareinvoiceservices.context.EInvoiceContext;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;
import com.masdar.masdarwarehousingbackoffice.services.GeneratePDFForConsignment;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public abstract class AbstractConfirmShippedConsignmentAction extends ConfirmShippedConsignmentAction
{
	private static final String ERROR_MESSAGE_DELIVERY_NOTE = "Error Message: Delivery Note Attachment not found on consignment, please upload delivery note attachment.";

	@Resource(name = "scpiService")
	private SCPIService scpiService;

	@Resource(name = "einvoiceContext")
	private EInvoiceContext einvoiceContext;

	@Resource(name = "generatePDFForConsignment")
	private GeneratePDFForConsignment generatePDFForConsignment;

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;




	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		final BaseStoreModel store = getBaseStore(consignment);
		final String failed_message_label = actionContext.getLabel(FAILED_MESSAGE);
		ActionResult<ConsignmentModel> result;

		if (store.isEnableDeliveryNoteAttachmentCheckOnConfirmShip() && consignment.getDeliveryNoteAttachment() == null)
		{
			errorMessage(ERROR_MESSAGE_DELIVERY_NOTE, failed_message_label);
			return new ActionResult<>(ActionResult.ERROR);
		}

		setInvoiceNumberAndDate(consignment);

		if (!processEInovoice(actionContext))
		{
			return new ActionResult<>(ActionResult.ERROR);
		}

		if (!store.isScpiSendOrderActionEnable())
		{
			getLogger().info("SCPI Is not enabled in store [{}]", store.getUid());
			return super.perform(actionContext);
		}


		try
		{

			if (!sendSCPISalesOrder(consignment))
			{
				errorMessage("Sending SalesOrder to SCPI failed", failed_message_label);
				return new ActionResult<>(ActionResult.ERROR);
			}

			result = super.perform(actionContext);
		}
		catch (final SCPIException e)
		{
			errorMessage(e.getMessage(), failed_message_label);
			result = new ActionResult<>(ActionResult.ERROR);
		}


		return result;
	}

	private boolean processEInovoice(final ActionContext<ConsignmentModel> actionContext)
	{

		final ConsignmentModel consignment = actionContext.getData();
		final String failed_message_label = actionContext.getLabel(FAILED_MESSAGE);
		final BaseStoreModel baseStore = getBaseStore(consignment);
		if (baseStore == null)
		{
			return false;
		}


		if (consignment.getInvoiceQRCode() != null && consignment.getEInvoicePDF() != null
				&& consignment.getEInvoiceEmmbedXML() != null)
		{
			return true;
		}


		try
		{
			getEinvoiceContext().generateEInvoiceForConsignment(consignment);
			getGeneratePDFForConsignment().generatePDFEInvoice(consignment);
		}
		catch (final Exception e)
		{
			errorMessage(e.getMessage(), failed_message_label);
			return false;
		}


		if (!baseStore.isSaveEInvoiceFromProvider())
		{
			return true;
		}


		try
		{
			getEinvoiceContext().saveEInvoicePDFOnConsignment(consignment);
		}
		catch (final Exception e)
		{
			errorMessage(e.getMessage(), failed_message_label);
			return false;
		}

		return true;
	}

	private int errorMessage(final String message, final String failMessage)
	{
		return Messagebox.show(message, failMessage, Messagebox.OK, Messagebox.ERROR);
	}


	protected boolean sendSCPISalesOrder(final ConsignmentModel consignment) throws SCPIException
	{

		final BaseStoreModel store = getBaseStore(consignment);

		if (store == null)
		{
			return false;
		}

		if (store.isEnabelCheckSCPISendOrder())
		{
			if (!getOrder(consignment).isScpiSentOrder())
			{
				getScpiService().sendSCPIOrder(consignment);
				if (store.isScpiSaveSuccessSendOrder() && consignment.getOrder() instanceof OrderModel)
				{
					final OrderModel orderModel = (OrderModel) consignment.getOrder();
					getModelService().refresh(orderModel);
					orderModel.setScpiSentOrder(true);
					getModelService().save(orderModel);
				}
			}
		}
		else
		{
			getScpiService().sendSCPIOrder(consignment);
		}

		if (store.isScpiSendConsginmentActionEnable())
		{
			getScpiService().sendSCPIConsignment(consignment);
		}

		return true;
	}


	/**
	 * @param consignment
	 * @return
	 */
	protected Optional<String> generateInvoiceNumber(final ConsignmentModel consignment)
	{
		return getSerialNumberConfigurationContext().generateSerialNumberForBaseStore(consignment.getOrder().getStore(),
				SerialNumberSource.CONSIGNMENT);
	}

	protected void setInvoiceNumberAndDate(final ConsignmentModel consignment)
	{
		if (StringUtils.isBlank(consignment.getInvoiceNumber()))
		{
			final Optional<String> invoiceNumber = generateInvoiceNumber(consignment);
			if (invoiceNumber.isPresent())
			{
				consignment.setInvoiceNumber(invoiceNumber.get());
				consignment.setInvoiceDate(new Date());
				getModelService().save(consignment);
			}
		}


		setDeliveryCostOnConsignment(consignment);

	}

	private void setDeliveryCostOnConsignment(final ConsignmentModel consignment)
	{
		synchronized (AbstractConfirmShippedConsignmentAction.class)
		{
			getModelService().refresh(consignment);
			final AbstractOrderModel order = consignment.getOrder();
			getModelService().refresh(order);
			final Double deliveryCost = order.getDeliveryCost();
			if (deliveryCost == null || deliveryCost.doubleValue() == 0.0d
					|| !Strings.isBlank(order.getDeliveryCostAppliedOnConsignment()))
			{
				return;
			}

			final String code = consignment.getCode();
			order.setDeliveryCostAppliedOnConsignment(code);
			getModelService().save(order);
		}
	}



	protected AbstractOrderModel getOrder(final ConsignmentModel consignment)
	{
		if (consignment == null || consignment.getOrder() == null)
		{
			return null;
		}
		return consignment.getOrder();
	}

	protected BaseStoreModel getBaseStore(final ConsignmentModel consignment)
	{
		final AbstractOrderModel order = getOrder(consignment);
		if (order == null)
		{
			return null;
		}

		if (order.getStore() == null)
		{
			Messagebox.show("Store Is null");
			return null;
		}

		return order.getStore();
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();

		if (!checkNulls(consignment))
		{
			return false;
		}

		if (isBlockedAction(consignment.getWarehouse(), consignment.getOrder().getStore())
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel())
				|| OrderStatus.PENDING.equals(getOrder(consignment).getStatus()))
		{
			return false;
		}

		return super.canPerform(actionContext);
	}

	/**
	 * @param consignment
	 */
	private boolean checkNulls(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			return false;
		}

		if (consignment.getOrder() == null)
		{
			return false;
		}

		if (consignment.getOrder().getStore() == null)
		{
			return false;
		}

		return consignment.getOrder().getSite() != null;
	}


	private boolean isBlockedAction(final WarehouseModel warehouse, final BaseStoreModel baseStore)
	{
		return baseStore.isEnableBlockConsignmentActionsIfNotDefualtStore() && warehouse.getDefault().booleanValue();
	}

	/**
	 * @return
	 */
	private SCPIService getScpiService()
	{
		return scpiService;
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	private SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}

	/**
	 * @return the einvoiceContext
	 */
	private EInvoiceContext getEinvoiceContext()
	{
		return einvoiceContext;
	}

	/**
	 * @return the generatePDFForConsignment
	 */
	private GeneratePDFForConsignment getGeneratePDFForConsignment()
	{
		return generatePDFForConsignment;
	}

}
