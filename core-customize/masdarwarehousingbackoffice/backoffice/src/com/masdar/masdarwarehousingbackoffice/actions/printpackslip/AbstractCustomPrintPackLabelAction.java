package com.masdar.masdarwarehousingbackoffice.actions.printpackslip;


import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;
import de.hybris.platform.warehousing.process.BusinessProcessException;
import de.hybris.platform.warehousingbackoffice.actions.printpacklabel.PrintPackLabelAction;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.labels.LabelUtils;
import com.masdar.masdareinvoiceservices.context.EInvoiceContext;


/**
 * @author Husam
 *
 */
public abstract class AbstractCustomPrintPackLabelAction extends PrintPackLabelAction
{
	private static final String ERROR_MESSAGE = "Error Message: ";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "einvoiceContext")
	private EInvoiceContext einvoiceContext;

	@Resource(name = "printMediaService")
	private PrintMediaService printMediaService;

	@Resource(name = "notificationService")
	private NotificationService notificationService;

	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel()))
		{
			return false;
		}

		if (!ConsignmentStatus.READY.equals(consignment.getStatus()))
		{
			return false;
		}

		return !OrderStatus.PENDING.equals(consignment.getOrder().getStatus()) && super.canPerform(actionContext);
	}


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();

		if (consignment == null)
		{
			Messagebox.show(ERROR_MESSAGE + "Pack request or consignment is empty!");
			return new ActionResult<>(ActionResult.ERROR);
		}

		processWorkflowIfNeeded(consignment);
		getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, "Ready To be Shipped");

		final ActionResult<ConsignmentModel> actionResult = new ActionResult<>(ActionResult.SUCCESS);
		actionResult.getStatusFlags().add(ActionResult.StatusFlag.OBJECT_MODIFIED);
		return actionResult;
	}

	private void processWorkflowIfNeeded(final ConsignmentModel consignment)
	{
		final WorkflowActionModel packWorkflowAction = this.getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode("NPR_Packing", consignment);
		if (packWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals(packWorkflowAction.getStatus()))
		{
			try
			{
				this.getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, "NPR_Packing", "packConsignment");
			}
			catch (final BusinessProcessException var4)
			{
				getLogger().info("Unable to trigger pack consignment process for consignment: {}", consignment.getCode());
			}
		}
	}

	/**
	 *
	 */
	private void printDocumentByMedia(final MediaModel pdfMedia)
	{

		// TODO: Validate that pdf media is actuallty PDF
		// TODO: Validate that the media have a valid URL
		final String width = "950";
		final String height = "800";
		final StringBuilder sb = new StringBuilder();

		sb.append("var myWindow = window.open('").append(pdfMedia.getURL()).append("','','width=").append(width).append(",height=")
				.append(height).append(",scrollbars=yes');");

		final String popup = sb.toString();

		Clients.evalJavaScript(popup);

	}

	protected String resolveLabel(final String labelKey)
	{
		final String defaultValue = LabelUtils.getFallbackLabel(labelKey);
		return Labels.getLabel(labelKey, defaultValue);
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the printMediaService
	 */
	public PrintMediaService getPrintMediaService()
	{
		return printMediaService;
	}

	/**
	 * @return the mediaService
	 */
	public MediaService getMediaService()
	{
		return mediaService;
	}

	/**
	 * @return the notificationService
	 */
	public NotificationService getNotificationService()
	{
		return notificationService;
	}

}

