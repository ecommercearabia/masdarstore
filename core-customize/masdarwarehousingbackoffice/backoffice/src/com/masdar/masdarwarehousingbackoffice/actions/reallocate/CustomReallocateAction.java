package com.masdar.masdarwarehousingbackoffice.actions.reallocate;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CustomReallocateAction extends AbstractCustomReallocateAction
{
	private final Logger LOG = LoggerFactory.getLogger(CustomReallocateAction.class); // NOSONAR

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}
}

