/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.ship;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class CustomB2CConfirmShippedConsignmentAction extends AbstractConfirmShippedConsignmentAction
{

	private static Logger LOG = LoggerFactory.getLogger(CustomB2CConfirmShippedConsignmentAction.class);


	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}


	@Override
	protected Logger getLogger()
	{
		return LOG;
	}
}
