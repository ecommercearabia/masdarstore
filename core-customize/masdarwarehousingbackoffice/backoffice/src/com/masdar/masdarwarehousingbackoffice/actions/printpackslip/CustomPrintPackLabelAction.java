package com.masdar.masdarwarehousingbackoffice.actions.printpackslip;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author Husam
 *
 */
public class CustomPrintPackLabelAction extends AbstractCustomPrintPackLabelAction
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomPrintPackLabelAction.class);

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		if (consignmentModelActionContext.getData() == null)
		{
			return false;
		}
		return !isBlockedAction(consignmentModelActionContext.getData()) && super.canPerform(consignmentModelActionContext);
	}

	private boolean isBlockedAction(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
				&& consignment.getWarehouse().getDefault().booleanValue();
	}

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}

