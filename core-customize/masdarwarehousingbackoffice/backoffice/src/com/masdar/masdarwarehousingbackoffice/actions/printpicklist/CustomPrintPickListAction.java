/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printpicklist;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomPrintPickListAction extends AbstractCustomPrintPickListAction
{
	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		return super.canPerform(consignmentModelActionContext) && !isBlockedAction(consignment);
	}



	private boolean isBlockedAction(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
				&& consignment.getWarehouse().getDefault().booleanValue();
	}



	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}
}
