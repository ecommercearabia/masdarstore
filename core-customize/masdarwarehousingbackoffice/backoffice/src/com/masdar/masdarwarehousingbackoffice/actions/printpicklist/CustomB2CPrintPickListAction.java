/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printpicklist;

import de.hybris.platform.commerceservices.enums.SiteChannel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomB2CPrintPickListAction extends AbstractCustomPrintPickListAction
{

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}
}
