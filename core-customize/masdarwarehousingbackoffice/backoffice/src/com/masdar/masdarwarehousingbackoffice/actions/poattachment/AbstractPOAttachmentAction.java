/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.poattachment;

import de.hybris.platform.comments.model.CommentAttachmentModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;

import javax.annotation.Resource;

import org.zkoss.zkmax.zul.Filedownload;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;


/**
 * @author Husam
 *
 */
public abstract class AbstractPOAttachmentAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	@Resource(name = "mediaService")
	private MediaService mediaService;

	protected abstract SiteChannel getSiteChannel();

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		final CommentAttachmentModel paymentTypeAttachment = consignment.getOrder().getPaymentTypeAttachment();
		if (paymentTypeAttachment == null)
		{
			return new ActionResult<>(ActionResult.ERROR);
		}

		final MediaModel attachment = (MediaModel) paymentTypeAttachment.getItem();
		if (attachment != null && attachment.getDownloadURL() != null)
		{
			final byte[] data = getMediaService().getDataFromMedia(attachment);
			Filedownload.save(data, null, attachment.getRealFileName());
			return new ActionResult<>(ActionResult.SUCCESS);
		}
		return new ActionResult<>(ActionResult.ERROR);

	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		if ((consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel())))
		{
			return false;
		}
		final CommentAttachmentModel paymentTypeAttachment = consignment.getOrder().getPaymentTypeAttachment();
		return (paymentTypeAttachment != null) && consignment.getOrder() != null
				&& !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
	}

	/**
	 * @return the mediaService
	 */
	public MediaService getMediaService()
	{
		return mediaService;
	}
}
