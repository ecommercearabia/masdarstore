/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.reallocate;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.reallocate.ReallocateAction;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;


/**
 * @author core
 *
 */
public abstract class AbstractCustomReallocateAction extends ReallocateAction
{

	protected static final String CAPTURE_PAYMENT_ON_CONSIGNMENT = "warehousing.capturepaymentonconsignment";
	protected static final String SOCKET_OUT_CONTEXT = "customReallocateContext";

	@Resource(name = "scpiService")
	private SCPIService scpiService;

	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel()))
		{
			return false;
		}
		return super.canPerform(actionContext) && !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignmentModel = actionContext.getData();
		ActionResult<ConsignmentModel> actionResult = new ActionResult<>(ActionResult.SUCCESS);
		if (!consignmentModel.getOrder().getStore().isSCPIActive())
		{
			this.sendOutput(SOCKET_OUT_CONTEXT, actionContext.getData());
			return actionResult;
		}

		try
		{
			getScpiService().updateSCPIInventory(consignmentModel);
			this.sendOutput(SOCKET_OUT_CONTEXT, actionContext.getData());
		}
		catch (final SCPIException e)
		{
			Messagebox.show("Error Message: " + e.getMessage());
			actionResult = new ActionResult<>(ActionResult.ERROR);
		}

		return actionResult;

	}

	/**
	 * @return the scpiService
	 */
	protected SCPIService getScpiService()
	{
		return scpiService;
	}
}
