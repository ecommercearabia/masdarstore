/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.returns;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.warehousingbackoffice.actions.returns.acceptgoods.AcceptGoodsAction;

import java.util.Optional;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomAcceptGoodsAction extends AcceptGoodsAction
{

	@Override
	public boolean canPerform(final ActionContext<ReturnRequestModel> actionContext)
	{
		final boolean superCanperform = super.canPerform(actionContext);
		final Object data = actionContext.getData();
		ReturnRequestModel returnRequest = null;
		boolean decision = false;
		boolean checkPaymentType = true;
		if (data instanceof ReturnRequestModel)
		{
			returnRequest = (ReturnRequestModel) data;
			final Optional<ReturnProcessModel> process = returnRequest.getReturnProcess().stream().findFirst();
			if (process.isPresent())
			{
				final Optional<ProcessTaskModel> processTaskModel = process.get().getCurrentTasks().stream().findFirst();

				if (processTaskModel.isPresent())
				{

					decision = processTaskModel.get().getAction().equals("waitForGoodsAction");
				}
			}
		}
		if (data instanceof ConsignmentModel)
		{
			final ConsignmentModel consignment = (ConsignmentModel) data;
			checkPaymentType = consignment.getOrder() != null && !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
		}



		return superCanperform && decision && checkPaymentType;
	}
}
