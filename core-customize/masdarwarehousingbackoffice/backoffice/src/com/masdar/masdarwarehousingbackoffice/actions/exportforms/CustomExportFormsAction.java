/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.exportforms;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.exportforms.ExportFormsAction;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author Husam
 *
 */
public class CustomExportFormsAction extends ExportFormsAction
{

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final boolean superPerfrom = super.canPerform(consignmentModelActionContext);
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		return superPerfrom && checkBlock(consignment) && consignment.getOrder() != null
				&& !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
	}

	private boolean checkBlock(final ConsignmentModel consignment)
	{
		return !(consignment == null || consignment.getOrder() == null || consignment.getOrder().getStore() == null
				|| (consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
						&& consignment.getWarehouse().getDefault().booleanValue()));
	}
}
