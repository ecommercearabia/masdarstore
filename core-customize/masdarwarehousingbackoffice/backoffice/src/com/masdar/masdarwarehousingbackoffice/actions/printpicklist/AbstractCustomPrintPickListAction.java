/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printpicklist;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.printpicklist.PrintPickListAction;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public abstract class AbstractCustomPrintPickListAction extends PrintPickListAction
{
	protected abstract SiteChannel getSiteChannel();

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final boolean superPerfrom = super.canPerform(consignmentModelActionContext);
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		if ((consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel())))
		{
			return false;
		}

		return superPerfrom && !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());
	}

}
