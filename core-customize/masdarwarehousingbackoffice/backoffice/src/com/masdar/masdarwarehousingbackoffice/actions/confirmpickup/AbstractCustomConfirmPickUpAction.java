/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.confirmpickup;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.warehousingbackoffice.actions.confirmpickup.ConfirmPickupAction;

import java.util.Date;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.masdar.core.context.SerialNumberConfigurationContext;
import com.masdar.core.enums.SerialNumberSource;
import com.masdar.masdareinvoiceservices.context.EInvoiceContext;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.service.SCPIService;
import com.masdar.masdarwarehousingbackoffice.services.GeneratePDFForConsignment;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public abstract class AbstractCustomConfirmPickUpAction extends ConfirmPickupAction
{
	private static final String ERROR_MESSAGE = "Error Message: ";


	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;

	@Resource(name = "einvoiceContext")
	private EInvoiceContext einvoiceContext;

	@Resource(name = "scpiService")
	private SCPIService scpiService;

	@Resource(name = "generatePDFForConsignment")
	private GeneratePDFForConsignment generatePDFForConsignment;

	protected abstract SiteChannel getSiteChannel();

	protected abstract Logger getLogger();

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		ActionResult<ConsignmentModel> result;


		setInvoiceNumberAndDate(consignment);

		if (!processEInovoice(actionContext))
		{
			return new ActionResult<>(ActionResult.ERROR);
		}


		if (!getBaseStore(consignment).isScpiSendOrderActionEnable())
		{
			return super.perform(actionContext);
		}

		try
		{

			if (!sendSCPISalesOrder(consignment))
			{
				Messagebox.show("Sending SalesOrder to SCPI failed");
				return new ActionResult<>(ActionResult.ERROR);
			}

			result = super.perform(actionContext);
		}
		catch (final SCPIException e)
		{
			Messagebox.show(ERROR_MESSAGE + e.getMessage());
			result = new ActionResult<>(ActionResult.ERROR);
		}
		return result;
	}

	private boolean processEInovoice(final ActionContext<ConsignmentModel> actionContext)
	{

		final ConsignmentModel consignment = actionContext.getData();
		final String failed_message_label = actionContext.getLabel(FAILED_MESSAGE);
		final BaseStoreModel baseStore = getBaseStore(consignment);
		if (baseStore == null)
		{
			return false;
		}


		if (consignment.getInvoiceQRCode() != null && consignment.getEInvoicePDF() != null
				&& consignment.getEInvoiceEmmbedXML() != null)
		{
			return true;
		}


		try
		{
			getEinvoiceContext().generateEInvoiceForConsignment(consignment);
			getGeneratePDFForConsignment().generatePDFEInvoice(consignment);
		}
		catch (final Exception e)
		{
			errorMessage(e.getMessage(), failed_message_label);
			return false;
		}

		if (!baseStore.isSaveEInvoiceFromProvider())
		{
			return true;
		}


		try
		{
			getEinvoiceContext().saveEInvoicePDFOnConsignment(consignment);
		}
		catch (final Exception e)
		{
			errorMessage(e.getMessage(), failed_message_label);
			return false;
		}

		return true;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();

		if (!checkNulls(consignment))
		{
			return false;
		}

		if (!getSiteChannel().equals(consignment.getOrder().getSite().getChannel())
				|| OrderStatus.PENDING.equals(getOrder(consignment).getStatus()))
		{
			return false;
		}

		return super.canPerform(actionContext);
	}

	private int errorMessage(final String message, final String failMessage)
	{
		return Messagebox.show(message, failMessage, Messagebox.OK, Messagebox.ERROR);
	}


	protected boolean sendSCPISalesOrder(final ConsignmentModel consignment) throws SCPIException
	{

		final BaseStoreModel store = getBaseStore(consignment);

		if (store == null)
		{
			return false;
		}

		if (store.isEnabelCheckSCPISendOrder())
		{
			if (!getOrder(consignment).isScpiSentOrder())
			{
				getScpiService().sendSCPIOrder(consignment);
				if (store.isScpiSaveSuccessSendOrder() && consignment.getOrder() instanceof OrderModel)
				{
					final OrderModel orderModel = (OrderModel) consignment.getOrder();
					getModelService().refresh(orderModel);
					orderModel.setScpiSentOrder(true);
					getModelService().save(orderModel);
				}
			}
		}
		else
		{
			getScpiService().sendSCPIOrder(consignment);
		}

		if (store.isScpiSendConsginmentActionEnable())
		{
			getScpiService().sendSCPIConsignment(consignment);
		}
		return true;
	}




	protected BaseStoreModel getBaseStore(final ConsignmentModel consignment)
	{
		if (getOrder(consignment) == null)
		{
			return null;
		}

		if (getOrder(consignment).getStore() == null)
		{
			Messagebox.show("Store Is null");
			return null;
		}

		return getOrder(consignment).getStore();
	}

	protected AbstractOrderModel getOrder(final ConsignmentModel consignment)
	{
		if (consignment == null || consignment.getOrder() == null)
		{
			Messagebox.show("Order Is null");
			return null;
		}
		return consignment.getOrder();
	}

	protected void setInvoiceNumberAndDate(final ConsignmentModel consignment)
	{
		if (StringUtils.isBlank(consignment.getInvoiceNumber()))
		{
			final Optional<String> invoiceNumber = generateInvoiceNumber(consignment);
			if (invoiceNumber.isPresent())
			{
				consignment.setInvoiceNumber(invoiceNumber.get());
				consignment.setInvoiceDate(new Date());
				getModelService().save(consignment);
			}
		}

	}


	/**
	 * @param consignment
	 */
	private boolean checkNulls(final ConsignmentModel consignment)
	{

		if (consignment == null)
		{
			return false;
		}

		if (consignment.getOrder() == null)
		{
			return false;
		}

		if (consignment.getOrder().getStore() == null)
		{
			return false;
		}

		if (consignment.getOrder().getSite() == null)
		{
			return false;
		}

		return true;
	}

	/**
	 * @param consignment
	 * @return
	 */
	protected Optional<String> generateInvoiceNumber(final ConsignmentModel consignment)
	{
		return getSerialNumberConfigurationContext().generateSerialNumberForBaseStore(consignment.getOrder().getStore(),
				SerialNumberSource.CONSIGNMENT);
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	public SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}


	/**
	 * @return the scpiService
	 */
	public SCPIService getScpiService()
	{
		return scpiService;
	}


	/**
	 * @return the einvoiceContext
	 */
	public EInvoiceContext getEinvoiceContext()
	{
		return einvoiceContext;
	}

	/**
	 * @return the generatePDFForConsignment
	 */
	public GeneratePDFForConsignment getGeneratePDFForConsignment()
	{
		return generatePDFForConsignment;
	}

}
