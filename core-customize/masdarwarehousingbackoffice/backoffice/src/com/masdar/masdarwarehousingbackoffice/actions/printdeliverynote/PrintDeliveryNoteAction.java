/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printdeliverynote;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class PrintDeliveryNoteAction extends AbstractPrintDeliveryNoteAction
{


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{

		final ActionResult<ConsignmentModel> perform = super.perform(consignmentModelActionContext);
		if (ActionResult.SUCCESS.equals(perform.getResultCode()))
		{
			setEmployeeOnConsignment(consignmentModelActionContext.getData());
		}

		return perform;
	}

	/**
	 * @param consignment
	 *
	 */
	private void setEmployeeOnConsignment(final ConsignmentModel consignment)
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		if (currentUser instanceof EmployeeModel)
		{
			consignment.setPrintDeliveryNoteEmployee((EmployeeModel) currentUser);
			getModelService().save(consignment);
			getModelService().refresh(consignment);
		}

	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return !isBlockedAction(consignmentModelActionContext.getData()) && super.canPerform(consignmentModelActionContext);
	}



	private boolean isBlockedAction(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
				&& consignment.getWarehouse().getDefault().booleanValue();
	}

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

}
