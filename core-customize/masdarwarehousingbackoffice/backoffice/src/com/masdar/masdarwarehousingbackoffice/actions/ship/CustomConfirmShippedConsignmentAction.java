/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.ship;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class CustomConfirmShippedConsignmentAction extends AbstractConfirmShippedConsignmentAction
{

	private static Logger LOG = LoggerFactory.getLogger(CustomConfirmShippedConsignmentAction.class);

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}
