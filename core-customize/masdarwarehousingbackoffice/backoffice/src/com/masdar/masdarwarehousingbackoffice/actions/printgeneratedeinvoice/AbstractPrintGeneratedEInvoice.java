package com.masdar.masdarwarehousingbackoffice.actions.printgeneratedeinvoice;


import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.labels.service.PrintMediaService;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.zkoss.zk.ui.util.Clients;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.core.user.CockpitUserService;
import com.masdar.masdareinvoiceservices.context.EInvoiceContext;


/**
 * @author Husam
 *
 */
public abstract class AbstractPrintGeneratedEInvoice implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final String ERROR_MESSAGE = "Error Message: ";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "einvoiceContext")
	private EInvoiceContext einvoiceContext;

	@Resource(name = "printMediaService")
	private PrintMediaService printMediaService;

	@Resource(name = "cockpitUserService")
	private CockpitUserService cockpitUserService;


	protected abstract Logger getLogger();

	protected abstract SiteChannel getSiteChannel();

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null)
		{
			return false;
		}

		if (!getSiteChannel().equals(consignment.getOrder().getSite().getChannel()))
		{
			return false;
		}

		final String currentUser = getCockpitUserService().getCurrentUser();
		if (Strings.isBlank(currentUser) || !getCockpitUserService().isAdmin(currentUser))
		{
			return false;
		}
		return consignment.getOrder().getStore().isEnablePrintProviderGeneratedEInvoice()
				&& consignment.getProviderGeneratedEInvoice() != null;// && consignment.get;
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> context)
	{
		printDocumentByMedia(context.getData().getProviderGeneratedEInvoice());
		return new ActionResult<>(ActionResult.SUCCESS);
	}

	/**
	 *
	 */
	private void printDocumentByMedia(final MediaModel pdfMedia)
	{

		// TODO: Validate that pdf media is actuallty PDF
		// TODO: Validate that the media have a valid URL
		final String width = "950";
		final String height = "800";
		final StringBuilder sb = new StringBuilder();

		sb.append("var myWindow = window.open('").append(pdfMedia.getURL()).append("','','width=").append(width).append(",height=")
				.append(height).append(",scrollbars=yes');");

		final String popup = sb.toString();

		Clients.evalJavaScript(popup);

	}

	/**
	 * @return the cockpitUserService
	 */
	public CockpitUserService getCockpitUserService()
	{
		return cockpitUserService;
	}
}

