/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.confirmpickup;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomConfirmPickupAction extends AbstractCustomConfirmPickUpAction
{

	private static Logger LOG = LoggerFactory.getLogger(CustomConfirmPickupAction.class);

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		return isBlockedAction(actionContext.getData()) && super.canPerform(actionContext);
	}

	private boolean isBlockedAction(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
				&& consignment.getWarehouse().getDefault().booleanValue();
	}

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}
}
