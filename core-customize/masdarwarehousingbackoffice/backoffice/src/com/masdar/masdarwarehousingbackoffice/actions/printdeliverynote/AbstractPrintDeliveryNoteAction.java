/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printdeliverynote;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;


/**
 * @author core
 *
 */
public abstract class AbstractPrintDeliveryNoteAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	protected abstract SiteChannel getSiteChannel();


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		final MediaModel attachment = consignment.getDeliveryNoteAttachment();
		if (attachment == null || attachment.getDownloadURL() == null)
		{
			Messagebox.show("Can not find Delivery Note Attachment or the attachment's download URL.");
			return new ActionResult<>(ActionResult.ERROR);
		}
		try
		{
			final byte[] data = getMediaService().getDataFromMedia(attachment);
			Filedownload.save(data, null, attachment.getRealFileName());
			return new ActionResult<>(ActionResult.SUCCESS);
		}
		catch (final Exception e)
		{
			Messagebox.show("Error occurred while downloading attachment, caused by: " + e.getMessage());
			return new ActionResult<>(ActionResult.ERROR);
		}
	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final ConsignmentModel consignment = consignmentModelActionContext.getData();
		if ((consignment == null || consignment.getOrder() == null || consignment.getOrder().getSite() == null
				|| !getSiteChannel().equals(consignment.getOrder().getSite().getChannel())))
		{
			return false;
		}
		return !OrderStatus.PENDING.equals(consignment.getOrder().getStatus());

	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return "Do you want to download the delivery note attachment?";
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}
}
