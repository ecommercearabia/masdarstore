package com.masdar.masdarwarehousingbackoffice.actions.printeinvoice;


import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class B2CPrintEInvoiceAction extends AbstractPrintEInvoiceAction
{
	private static final Logger LOG = LoggerFactory.getLogger(B2CPrintEInvoiceAction.class);

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}
}

