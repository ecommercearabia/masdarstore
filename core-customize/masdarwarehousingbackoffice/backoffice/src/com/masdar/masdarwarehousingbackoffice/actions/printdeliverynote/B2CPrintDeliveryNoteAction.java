/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printdeliverynote;

import de.hybris.platform.commerceservices.enums.SiteChannel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class B2CPrintDeliveryNoteAction extends AbstractPrintDeliveryNoteAction
{

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}
}
