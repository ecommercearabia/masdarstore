package com.masdar.masdarwarehousingbackoffice.actions.printpackslip;

import de.hybris.platform.commerceservices.enums.SiteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Husam
 *
 */
public class CustomB2CPrintPackLabelAction extends AbstractCustomPrintPackLabelAction
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomB2CPrintPackLabelAction.class);

	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2C;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}

