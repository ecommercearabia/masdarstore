/**
 *
 */
package com.masdar.masdarwarehousingbackoffice.actions.printshippinglabel;

import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.hybris.cockpitng.actions.ActionContext;


/**
 * @author Husam
 *
 */
public class CustomPrintShippingLabelAction extends AbstractCustomPrintShippingLabelAction
{

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		final boolean superPerfrom = super.canPerform(consignmentModelActionContext);
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		return superPerfrom && !isBlockedAction(consignment);
	}

	private boolean isBlockedAction(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getStore().isEnableBlockConsignmentActionsIfNotDefualtStore()
				&& consignment.getWarehouse().getDefault().booleanValue();
	}



	@Override
	protected SiteChannel getSiteChannel()
	{
		return SiteChannel.B2B;
	}
}
