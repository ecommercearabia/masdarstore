/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.google.common.collect.Sets
 *  com.hybris.backoffice.i18n.BackofficeLocaleService
 *  com.hybris.backoffice.widgets.notificationarea.NotificationService
 *  com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent$Level
 *  com.hybris.cockpitng.annotations.SocketEvent
 *  com.hybris.cockpitng.annotations.ViewEvent
 *  com.hybris.cockpitng.core.events.CockpitEvent
 *  com.hybris.cockpitng.core.events.CockpitEventQueue
 *  com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent
 *  com.hybris.cockpitng.util.DefaultWidgetController
 *  de.hybris.platform.core.HybrisEnumValue
 *  de.hybris.platform.core.model.ItemModel
 *  de.hybris.platform.enumeration.EnumerationService
 *  de.hybris.platform.ordersplitting.WarehouseService
 *  de.hybris.platform.ordersplitting.model.ConsignmentEntryModel
 *  de.hybris.platform.ordersplitting.model.ConsignmentModel
 *  de.hybris.platform.ordersplitting.model.ConsignmentProcessModel
 *  de.hybris.platform.ordersplitting.model.WarehouseModel
 *  de.hybris.platform.processengine.model.BusinessProcessModel
 *  de.hybris.platform.processengine.model.BusinessProcessParameterModel
 *  de.hybris.platform.servicelayer.model.ModelService
 *  de.hybris.platform.warehousing.data.allocation.DeclineEntries
 *  de.hybris.platform.warehousing.data.allocation.DeclineEntry
 *  de.hybris.platform.warehousing.enums.DeclineReason
 *  de.hybris.platform.warehousing.process.WarehousingBusinessProcessService
 *  de.hybris.platform.warehousing.sourcing.filter.SourcingFilterProcessor
 *  de.hybris.platform.warehousing.stock.services.impl.DefaultWarehouseStockService
 *  org.apache.commons.collections4.CollectionUtils
 *  org.zkoss.zk.ui.Component
 *  org.zkoss.zk.ui.WrongValueException
 *  org.zkoss.zk.ui.event.Event
 *  org.zkoss.zk.ui.event.Events
 *  org.zkoss.zk.ui.event.InputEvent
 *  org.zkoss.zk.ui.event.SelectEvent
 *  org.zkoss.zk.ui.select.annotation.Wire
 *  org.zkoss.zk.ui.select.annotation.WireVariable
 *  org.zkoss.zk.ui.util.Clients
 *  org.zkoss.zul.Checkbox
 *  org.zkoss.zul.Combobox
 *  org.zkoss.zul.Comboitem
 *  org.zkoss.zul.Grid
 *  org.zkoss.zul.Intbox
 *  org.zkoss.zul.Label
 *  org.zkoss.zul.ListModel
 *  org.zkoss.zul.ListModelArray
 *  org.zkoss.zul.ListModelList
 *  org.zkoss.zul.Row
 *  org.zkoss.zul.Textbox
 *  org.zkoss.zul.impl.InputElement
 */
package com.masdar.masdarwarehousingbackoffice.widgets.consignment;

import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousing.data.allocation.DeclineEntries;
import de.hybris.platform.warehousing.data.allocation.DeclineEntry;
import de.hybris.platform.warehousing.enums.DeclineReason;
import de.hybris.platform.warehousing.process.WarehousingBusinessProcessService;
import de.hybris.platform.warehousing.sourcing.filter.SourcingFilterProcessor;
import de.hybris.platform.warehousing.stock.services.impl.DefaultWarehouseStockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.masdar.masdarwarehousingbackoffice.dtos.ConsignmentEntryToReallocateDto;
import com.masdar.masdarwarehousingbackoffice.dtos.WarehouseLocationsDto;


public class CustomConsignmentToReallocateController extends DefaultWidgetController
{
	private static final long serialVersionUID = 1L;
	protected static final String IN_SOCKET = "consignmentInput";
	protected static final String OUT_CONFIRM = "confirmOutput";
	protected static final Object COMPLETED = "completed";
	protected static final String CONSIGNMENT_ACTION_EVENT_NAME = "ConsignmentActionEvent";
	protected static final String REALLOCATE_CONSIGNMENT_CHOICE = "reallocateConsignment";
	protected static final String DECLINE_ENTRIES = "declineEntries";
	protected static final int COLUMN_INDEX_REALLOCATION_QUANTITY = 4;
	protected static final int COLUMN_INDEX_REALLOCATION_REASON = 5;
	protected static final int COLUMN_INDEX_REALLOCATION_LOCATION = 6;
	protected static final int COLUMN_INDEX_REALLOCATION_COMMENT = 7;
	private transient Set<ConsignmentEntryToReallocateDto> consignmentsEntriesToReallocate;
	private ConsignmentModel consignment;
	@Wire
	private Textbox consignmentCode;
	@Wire
	private Textbox customerName;
	@Wire
	private Combobox globalDeclineReasons;
	@Wire
	private Textbox globalDeclineComment;
	@Wire
	private Grid consignmentEntries;
	@Wire
	private Combobox globalPossibleLocations;
	@Wire
	private Checkbox globalDeclineEntriesSelection;
	private final List<String> declineReasons = new ArrayList<String>();
	private final Set<WarehouseModel> locations = Sets.newHashSet();
	private final Set<WarehouseLocationsDto> warehouseLocations = Sets.newHashSet();

	@WireVariable
	private transient SourcingFilterProcessor sourcingFilterProcessor;
	@WireVariable
	private transient WarehouseService warehouseService;
	@WireVariable
	private transient EnumerationService enumerationService;
	@WireVariable
	private transient WarehousingBusinessProcessService<ConsignmentModel> consignmentBusinessProcessService;
	@WireVariable
	private transient ModelService modelService;
	@WireVariable
	private transient BackofficeLocaleService cockpitLocaleService;
	@WireVariable
	private transient CockpitEventQueue cockpitEventQueue;
	@WireVariable
	private transient DefaultWarehouseStockService warehouseStockService;
	@WireVariable
	private transient NotificationService notificationService;

	@SocketEvent(socketId = "consignmentInput")
	public void initReallocationConsignmentForm(final ConsignmentModel inputObject)
	{
		this.declineReasons.clear();
		this.locations.clear();
		this.warehouseLocations.clear();
		this.globalDeclineEntriesSelection.setChecked(false);
		this.setConsignment(inputObject);
		this.getWidgetInstanceManager().setTitle(
				String.valueOf(this.getWidgetInstanceManager().getLabel("warehousingbackoffice.reallocationconsignment.title")) + " "
						+ this.getConsignment().getCode());
		this.consignmentCode.setValue(this.getConsignment().getCode());
		this.customerName.setValue(this.getConsignment().getOrder().getUser().getDisplayName());
		final Locale locale = this.getCockpitLocaleService().getCurrentLocale();
		this.getEnumerationService().getEnumerationValues(DeclineReason.class).stream()
				.filter(reason -> !reason.equals(DeclineReason.ASNCANCELLATION)).forEach(reason -> {
					final boolean bl = this.declineReasons.add(this.getEnumerationService().getEnumerationName(reason, locale));
				});
		this.sourcingFilterProcessor.filterLocations(inputObject.getOrder(), this.locations);
		if (this.locations.contains(this.getConsignment().getWarehouse()))
		{
			this.locations.remove(this.getConsignment().getWarehouse());
		}
		this.globalDeclineReasons.setModel(new ListModelArray(this.declineReasons));
		//		this.globalPossibleLocations.setModel(new ListModelArray(this.locations.toArray()));
		setLocationsTowarehouseLocations(this.locations);
		this.globalPossibleLocations.setModel(new ListModelArray(this.warehouseLocations.toArray()));
		this.consignmentsEntriesToReallocate = new HashSet<ConsignmentEntryToReallocateDto>();

		this.getConsignment().getConsignmentEntries().stream().filter(entry -> entry.getQuantityPending() > 0L)
				.forEach(this::populateFromWithConsignments);

		this.getConsignmentEntries().setModel(new ListModelList(this.consignmentsEntriesToReallocate));
		this.getConsignmentEntries().renderAll();
		this.addListeners();
	}

	private void populateFromWithConsignments(final ConsignmentEntryModel entry)
	{
		final Set<WarehouseLocationsDto> whlocations = this.getWarehouseLocations(this.warehouseLocations, entry);
		//		if (locations != null && !locations.isEmpty())
		{
			this.consignmentsEntriesToReallocate.add(new ConsignmentEntryToReallocateDto(entry, this.declineReasons, whlocations));
		}
	}

	/**
	 * @param warehouseLocations2
	 * @param entry
	 * @return
	 */
	private Set<WarehouseLocationsDto> getWarehouseLocations(final Set<WarehouseLocationsDto> wlDTO,
			final ConsignmentEntryModel entry)
	{

		final Set<WarehouseLocationsDto> setDTOs = Sets.newHashSet();
		for (final WarehouseLocationsDto warehouseLocationsDto : wlDTO)
		{

			final String availableStock = getAvailableStock(warehouseLocationsDto.getWarehouseModel(),
					entry.getOrderEntry().getProduct().getCode());
			if (!Strings.isNullOrEmpty(availableStock))
			{
				setDTOs.add(new WarehouseLocationsDto(availableStock, warehouseLocationsDto.getWarehouseModel(),
						warehouseLocationsDto.getWarehouseCode()));
			}
		}
		return setDTOs;
	}

	/**
	 * @param locations2
	 * @return
	 * @return
	 */
	private void setLocationsTowarehouseLocations(final Set<WarehouseModel> locations)
	{

		WarehouseLocationsDto warehouseLocationsDto = null;
		String code = null;
		for (final WarehouseModel warehouseModel : locations)
		{
			code = warehouseModel.getCode();
			warehouseLocationsDto = new WarehouseLocationsDto(code, warehouseModel, warehouseModel.getCode());
			this.warehouseLocations.add(warehouseLocationsDto);
		}

	}

	private String getAvailableStock(final WarehouseModel warehouseModel, final String productCode)
	{
		final Long stockLevelForProductCodeAndWarehouse = warehouseStockService.getStockLevelForProductCodeAndWarehouse(productCode,
				warehouseModel);

		String stock = null;

		if (stockLevelForProductCodeAndWarehouse == null)
		{
			stock = "FIS";
		}
		else if (stockLevelForProductCodeAndWarehouse.longValue() <= 0)
		{
			return null;
		}
		else
		{
			stock = stockLevelForProductCodeAndWarehouse.longValue() + "";
		}
		return warehouseModel.getCode() + " (" + stock + ")";
	}

	@ViewEvent(componentID = "confirmreallocation", eventName = "onClick")
	public void confirmReallocation() throws InterruptedException
	{
		this.validateRequest();
		final String consignmentProcessCode = String.valueOf(this.consignment.getCode()) + "_ordermanagement";
		final Optional<ConsignmentProcessModel> myConsignmentProcess = this.consignment.getConsignmentProcesses().stream()
				.filter(consignmentProcess -> consignmentProcess.getCode().equals(consignmentProcessCode)).findFirst();
		final ArrayList<DeclineEntry> entriesToReallocate = new ArrayList<DeclineEntry>();
		if (myConsignmentProcess.isPresent())
		{
			final List<Component> rows = this.consignmentEntries.getRows().getChildren();
			rows.stream().filter(entry -> ((Checkbox) entry.getFirstChild()).isChecked())
					.forEach(entry -> this.createDeclineEntry(entriesToReallocate, entry));
		}
		if (!entriesToReallocate.isEmpty())
		{
			this.buildDeclineParam(myConsignmentProcess.get(), entriesToReallocate);
			this.getConsignmentBusinessProcessService().triggerChoiceEvent(this.getConsignment(), CONSIGNMENT_ACTION_EVENT_NAME,
					REALLOCATE_CONSIGNMENT_CHOICE);
			final ConsignmentModel refreshedConsignment = (ConsignmentModel) this.getModelService()
					.get(this.getConsignment().getPk());
			for (int iterationCount = 0; !this.isDeclineProcessDone(refreshedConsignment, entriesToReallocate)
					&& iterationCount < 500000; ++iterationCount)
			{
				this.getModelService().refresh(refreshedConsignment);
			}
			refreshedConsignment.getConsignmentEntries().forEach(
					entry -> this.getCockpitEventQueue().publishEvent(new DefaultCockpitEvent("objectsUpdated", entry, null)));
			this.setConsignment(refreshedConsignment);
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
			{ this.getLabel("warehousingbackoffice.reallocationconsignment.success.message") });
		}
		else
		{
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
			{ this.getLabel("warehousingbackoffice.reallocationconsignment.error.message") });
		}
		this.sendOutput(OUT_CONFIRM, COMPLETED);
	}

	protected void createDeclineEntry(final Collection<DeclineEntry> entriesToReallocate, final Component component)
	{
		final ConsignmentEntryToReallocateDto consignmentEntryToReallocate = (ConsignmentEntryToReallocateDto) ((Row) component)
				.getValue();
		final Long qtyToReallocate = consignmentEntryToReallocate.getQuantityToReallocate();
		final Long qtyAvailableForReallocation = consignmentEntryToReallocate.getConsignmentEntry().getQuantityPending();
		if (qtyToReallocate > 0L && qtyToReallocate <= qtyAvailableForReallocation)
		{
			final DeclineEntry newEntry = new DeclineEntry();
			newEntry.setQuantity(qtyToReallocate);
			newEntry.setConsignmentEntry(consignmentEntryToReallocate.getConsignmentEntry());
			newEntry.setNotes(consignmentEntryToReallocate.getDeclineConsignmentEntryComment());
			final WarehouseModel warehouse = consignmentEntryToReallocate.getSelectedLocation() != null
					? consignmentEntryToReallocate.getSelectedLocation().getWarehouseModel()
					: null;
			newEntry.setReallocationWarehouse(warehouse);
			newEntry.setReason(consignmentEntryToReallocate.getSelectedReason());
			entriesToReallocate.add(newEntry);
		}
	}

	@ViewEvent(componentID = "undoreallocation", eventName = "onClick")
	public void reset()
	{
		this.globalDeclineReasons.setSelectedItem(null);
		this.globalPossibleLocations.setSelectedItem(null);
		this.globalDeclineComment.setValue("");
		this.initReallocationConsignmentForm(this.getConsignment());
	}

	protected void addListeners()
	{
		final List<Component> rows = this.consignmentEntries.getRows().getChildren();
		for (final Component row : rows)
		{
			for (final Component myComponent : row.getChildren())
			{
				if (myComponent instanceof Checkbox)
				{
					myComponent.addEventListener("onCheck", event -> this.handleRow((Row) event.getTarget().getParent()));
					continue;
				}
				if (myComponent instanceof Combobox)
				{
					myComponent.addEventListener("onSelect", this::handleIndividualLocation);
					myComponent.addEventListener("onCustomChange",
							event -> Events.echoEvent("onLaterCustomChange", myComponent, event.getData()));
					myComponent.addEventListener("onLaterCustomChange", event -> {
						Clients.clearWrongValue(myComponent);
						myComponent.invalidate();
						this.handleIndividualReason(event);
					});
					continue;
				}
				if (myComponent instanceof Intbox)
				{
					myComponent.addEventListener("onChange", event -> {
						this.autoSelect(event);
						((ConsignmentEntryToReallocateDto) ((Row) event.getTarget().getParent()).getValue())
								.setQuantityToReallocate(Long.parseLong(((InputEvent) event).getValue()));
					});
					continue;
				}
				if (!(myComponent instanceof Textbox))
				{
					continue;
				}
				myComponent.addEventListener("onChanging", event -> {
					this.autoSelect(event);
					((ConsignmentEntryToReallocateDto) ((Row) event.getTarget().getParent()).getValue())
							.setDeclineConsignmentEntryComment(((InputEvent) event).getValue());
				});
			}
		}
		this.globalDeclineReasons.addEventListener("onSelect", this::handleGlobalReason);
		this.globalPossibleLocations.addEventListener("onSelect", this::handleGlobalLocation);
		this.globalDeclineComment.addEventListener("onChanging", this::handleGlobalComment);
		this.globalDeclineEntriesSelection.addEventListener("onCheck", event -> this.selectAllEntries());
	}

	protected void applyToGrid(final Object data, final int childrenIndex)
	{
		this.consignmentEntries.getRows().getChildren().stream()
				.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach(entry -> this.applyToRow(data, childrenIndex, entry));
	}

	protected void applyToRow(final Object data, final int childrenIndex, final Component row)
	{
		int index = 0;
		for (final Component myComponent : row.getChildren())
		{
			if (index == childrenIndex)
			{
				this.applyToCheckboxRow(data, myComponent);
				this.applyToComboboxRow(data, myComponent);
				if (myComponent instanceof Intbox)
				{
					((Intbox) myComponent).setValue((Integer) data);
				}
				else if (!(myComponent instanceof Combobox) && myComponent instanceof Textbox)
				{
					((Textbox) myComponent).setValue((String) data);
				}
			}
			++index;
		}
	}

	protected void applyToComboboxRow(final Object data, final Component component)
	{
		if (component instanceof Combobox)
		{
			if (data == null)
			{
				((Combobox) component).setSelectedItem(null);
			}
			else
			{
				if (((Combobox) component).getItems().size() > 0)
				{
					((Combobox) component).setSelectedIndex(((Integer) data).intValue());
				}
			}
		}
	}

	protected void applyToCheckboxRow(final Object data, final Component component)
	{
		if (component instanceof Checkbox)
		{
			if (data == null)
			{
				((Checkbox) component).setChecked(Boolean.FALSE.booleanValue());
			}
			else
			{
				((Checkbox) component).setChecked(((Boolean) data).booleanValue());
			}
		}
	}

	protected void autoSelect(final Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
	}

	protected void buildDeclineParam(final ConsignmentProcessModel processModel,
			final Collection<DeclineEntry> entriesToReallocate)
	{
		this.cleanDeclineParam(processModel);
		final ArrayList<BusinessProcessParameterModel> contextParams = new ArrayList<BusinessProcessParameterModel>();
		contextParams.addAll(processModel.getContextParameters());
		final DeclineEntries declinedEntries = new DeclineEntries();
		declinedEntries.setEntries(entriesToReallocate);
		final BusinessProcessParameterModel declineParam = new BusinessProcessParameterModel();
		declineParam.setName(DECLINE_ENTRIES);
		declineParam.setValue(declinedEntries);
		declineParam.setProcess(processModel);
		contextParams.add(declineParam);
		processModel.setContextParameters(contextParams);
		this.getModelService().save(processModel);
	}

	protected void cleanDeclineParam(final ConsignmentProcessModel processModel)
	{
		Optional<BusinessProcessParameterModel> declineEntriesParamOptional;
		final ArrayList<BusinessProcessParameterModel> contextParams = new ArrayList();
		contextParams.addAll(processModel.getContextParameters());
		if (CollectionUtils.isNotEmpty(contextParams) && (declineEntriesParamOptional = contextParams.stream()
				.filter(param -> param.getName().equals(DECLINE_ENTRIES)).findFirst()).isPresent())
		{
			final BusinessProcessParameterModel declineEntriesParam = declineEntriesParamOptional.get();
			contextParams.remove(declineEntriesParam);
			this.getModelService().remove(declineEntriesParam);
			processModel.setContextParameters(contextParams);
			this.getModelService().save(processModel);
		}
	}

	protected int getLocationIndex(final WarehouseLocationsDto location)
	{
		int index = 0;
		for (final WarehouseLocationsDto warehouseModel : this.warehouseLocations)
		{
			if (location.getCode().equals(warehouseModel.getCode()))
			{
				break;
			}
			++index;
		}
		return index;
	}

	protected int getReasonIndex(final DeclineReason declineReason)
	{
		int index = 0;
		final String myReason = this.getEnumerationService().getEnumerationName(declineReason,
				this.getCockpitLocaleService().getCurrentLocale());
		for (final String reason : this.declineReasons)
		{
			if (myReason.equals(reason))
			{
				break;
			}
			++index;
		}
		return index;
	}

	protected Optional<DeclineReason> getSelectedDeclineReason(final Event event)
	{
		Optional<DeclineReason> result = Optional.empty();
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			final Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			result = this.matchingComboboxDeclineReason(selectedValue.toString());
		}
		return result;
	}

	protected Optional<DeclineReason> getCustomSelectedDeclineReason(final Event event)
	{
		Optional<DeclineReason> reason = Optional.empty();
		if (event.getTarget() instanceof Combobox)
		{
			final Object selectedValue = event.getData();
			reason = this.matchingComboboxDeclineReason(selectedValue.toString());
		}
		return reason;
	}

	protected WarehouseLocationsDto getSelectedLocation(final Event event)
	{
		WarehouseLocationsDto result = null;
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			result = (WarehouseLocationsDto) ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
		}
		return result;
	}

	protected void handleGlobalComment(final Event event)
	{
		this.applyToGrid(((InputEvent) event).getValue(), 7);
		this.consignmentEntries.getRows().getChildren().stream()
				.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach(entry -> ((ConsignmentEntryToReallocateDto) ((Row) entry).getValue())
						.setDeclineConsignmentEntryComment(((InputEvent) event).getValue()));
	}

	protected void handleGlobalLocation(final Event event)
	{
		final WarehouseLocationsDto selectedLocation = this.getSelectedLocation(event);

		if (selectedLocation != null)
		{
			this.applyToGrid(this.getLocationIndex(selectedLocation), 6);
			this.consignmentEntries.getRows().getChildren().stream()
					.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked()).forEach(
							entry -> ((ConsignmentEntryToReallocateDto) ((Row) entry).getValue()).setSelectedLocation(selectedLocation));
		}
	}

	protected void handleGlobalReason(final Event event)
	{
		final Optional<DeclineReason> declineReason = this.getSelectedDeclineReason(event);
		if (declineReason.isPresent())
		{
			this.applyToGrid(this.getReasonIndex(declineReason.get()), 5);
			this.consignmentEntries.getRows().getChildren().stream()
					.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
					.forEach(entry -> ((ConsignmentEntryToReallocateDto) ((Row) entry).getValue())
							.setSelectedReason(declineReason.get()));
		}
	}

	protected void handleIndividualReason(final Event event)
	{
		final Optional<DeclineReason> declineReason = this.getCustomSelectedDeclineReason(event);
		if (declineReason.isPresent())
		{
			this.autoSelect(event);
			((ConsignmentEntryToReallocateDto) ((Row) event.getTarget().getParent()).getValue())
					.setSelectedReason(declineReason.get());
		}
	}

	protected void handleIndividualLocation(final Event event)
	{
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			this.autoSelect(event);
			final Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			if (selectedValue instanceof WarehouseLocationsDto)
			{
				((ConsignmentEntryToReallocateDto) ((Row) event.getTarget().getParent()).getValue())
						.setSelectedLocation((WarehouseLocationsDto) selectedValue);
			}
		}
	}

	protected void handleRow(final Row row)
	{
		final ConsignmentEntryToReallocateDto myEntry = (ConsignmentEntryToReallocateDto) row.getValue();
		if (row.getChildren().iterator().next() instanceof Checkbox)
		{
			if (!((Checkbox) row.getChildren().iterator().next()).isChecked())
			{
				this.applyToRow(0, 4, row);
				this.applyToRow(null, 5, row);
				this.applyToRow(null, 6, row);
				this.applyToRow(null, 7, row);
				myEntry.setQuantityToReallocate(0L);
				myEntry.setSelectedReason(null);
				myEntry.setSelectedLocation(null);
				myEntry.setDeclineConsignmentEntryComment(null);
			}
			else
			{
				this.applyToRow(this.globalDeclineReasons.getSelectedIndex(), 5, row);
				this.applyToRow(this.globalPossibleLocations.getSelectedIndex(), 6, row);
				this.applyToRow(this.globalDeclineComment.getValue(), 7, row);
				final Optional<DeclineReason> reason = this.matchingComboboxDeclineReason(
						this.globalDeclineReasons.getSelectedItem() != null ? this.globalDeclineReasons.getSelectedItem().getLabel()
								: null);
				myEntry.setSelectedReason(reason.isPresent() ? reason.get() : null);
				myEntry.setSelectedLocation(this.globalPossibleLocations.getSelectedItem() != null
						? (WarehouseLocationsDto) this.globalPossibleLocations.getSelectedItem().getValue()
						: null);
				myEntry.setDeclineConsignmentEntryComment(this.globalDeclineComment.getValue());
			}
		}
	}

	protected boolean isDeclineProcessDone(final ConsignmentModel latestConsignmentModel,
			final Collection<DeclineEntry> entriesToReallocate)
	{
		return entriesToReallocate.stream().allMatch(entry -> this.isDeclinedQuantityCorrect(latestConsignmentModel, entry));
	}

	protected boolean isDeclinedQuantityCorrect(final ConsignmentModel latestConsignmentModel, final DeclineEntry declineEntry)
	{
		final Long expectedDeclinedQuantity = declineEntry.getConsignmentEntry().getQuantityDeclined() + declineEntry.getQuantity();
		return latestConsignmentModel.getConsignmentEntries().stream()
				.anyMatch(entry -> entry.getPk().equals(declineEntry.getConsignmentEntry().getPk())
						&& expectedDeclinedQuantity.equals(entry.getQuantityDeclined()));
	}

	protected Optional<DeclineReason> matchingComboboxDeclineReason(final String declineReasonLabel)
	{
		return this.getEnumerationService().getEnumerationValues(DeclineReason.class).stream()
				.filter(reason -> this.getEnumerationService()
						.getEnumerationName(reason, this.getCockpitLocaleService().getCurrentLocale()).equals(declineReasonLabel))
				.findFirst();
	}

	protected void selectAllEntries()
	{
		this.applyToGrid(Boolean.TRUE, 0);
		for (final Component row : this.consignmentEntries.getRows().getChildren())
		{
			final Component firstComponent = row.getChildren().iterator().next();
			if (firstComponent instanceof Checkbox)
			{
				((Checkbox) firstComponent).setChecked(this.globalDeclineEntriesSelection.isChecked());
			}
			this.handleRow((Row) row);
			if (!this.globalDeclineEntriesSelection.isChecked())
			{
				continue;
			}
			final int reallocatableQuantity = Integer.parseInt(((Label) row.getChildren().get(3)).getValue());
			this.applyToRow(reallocatableQuantity, 4, row);
		}
		if (this.globalDeclineEntriesSelection.isChecked())
		{
			this.consignmentsEntriesToReallocate.stream()
					.forEach(entry -> entry.setQuantityToReallocate(entry.getConsignmentEntry().getQuantityPending()));
		}
	}

	protected Component targetFieldToApplyValidation(final String stringToValidate, final int indexLabelToCheck,
			final int indexTargetComponent)
	{
		for (final Component component : this.consignmentEntries.getRows().getChildren())
		{
			final Label label = (Label) component.getChildren().get(indexLabelToCheck);
			if (!label.getValue().equals(stringToValidate))
			{
				continue;
			}
			return component.getChildren().get(indexTargetComponent);
		}
		return null;
	}

	protected void validateConsignmentEntry(final ConsignmentEntryToReallocateDto entry)
	{
		if (entry.getQuantityToReallocate() > entry.getConsignmentEntry().getQuantityPending())
		{
			final InputElement quantity = (InputElement) this
					.targetFieldToApplyValidation(entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(), 1, 4);
			throw new WrongValueException(quantity,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.invalid.quantity"));
		}

		if (entry.getSelectedReason() != null && entry.getQuantityToReallocate() == 0L)
		{
			final InputElement quantity = (InputElement) this
					.targetFieldToApplyValidation(entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(), 1, 4);
			throw new WrongValueException(quantity,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.missing.quantity"));
		}
		if (entry.getSelectedReason() == null && entry.getQuantityToReallocate() > 0L)
		{
			final Combobox reason = (Combobox) this
					.targetFieldToApplyValidation(entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(), 1, 5);
			throw new WrongValueException(reason,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.missing.reason"));
		}
		if (entry.getSelectedLocation() != null
				&& this.getWarehouseStockService().getStockLevelForProductCodeAndWarehouse(
						entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(),
						entry.getSelectedLocation().getWarehouseModel()) != null
				&& this.getWarehouseStockService().getStockLevelForProductCodeAndWarehouse(
						entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(),
						entry.getSelectedLocation().getWarehouseModel()) == 0L)
		{
			final Combobox location = (Combobox) this
					.targetFieldToApplyValidation(entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(), 1, 6);
			throw new WrongValueException(location,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.invalid.stockLevel"));
		}

		if (entry.getSelectedLocation() != null)
		{
			final Long locationStockQuantity = warehouseStockService.getStockLevelForProductCodeAndWarehouse(
					entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(),
					entry.getSelectedLocation().getWarehouseModel());
			if (locationStockQuantity == null || locationStockQuantity < entry.getQuantityToReallocate())
			{
				final InputElement quantity = (InputElement) this
						.targetFieldToApplyValidation(entry.getConsignmentEntry().getOrderEntry().getProduct().getCode(), 1, 4);
				throw new WrongValueException(quantity,
						this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.overflow.quantity"));
			}
		}

	}

	protected void validateRequest()
	{
		for (final Component row : this.getConsignmentEntries().getRows().getChildren())
		{
			InputElement returnQty;
			final Component firstComponent = row.getChildren().iterator().next();
			if (!(firstComponent instanceof Checkbox) || !((Checkbox) firstComponent).isChecked()
					|| !(returnQty = (InputElement) row.getChildren().get(4)).getRawValue().equals(0))
			{
				continue;
			}
			throw new WrongValueException(returnQty,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.missing.quantity"));
		}
		final ListModelList<ConsignmentEntryToReallocateDto> modelList = (ListModelList) this.getConsignmentEntries().getModel();
		if (modelList.stream().allMatch(entry -> entry.getQuantityToReallocate() == 0L))
		{
			throw new WrongValueException(this.globalDeclineEntriesSelection,
					this.getLabel("warehousingbackoffice.reallocationconsignment.decline.validation.missing.selectedLine"));
		}
		modelList.forEach(this::validateConsignmentEntry);
	}

	protected ConsignmentModel getConsignment()
	{
		return this.consignment;
	}

	public void setConsignment(final ConsignmentModel consignment)
	{
		this.consignment = consignment;
	}

	protected EnumerationService getEnumerationService()
	{
		return this.enumerationService;
	}

	protected Grid getConsignmentEntries()
	{
		return this.consignmentEntries;
	}

	protected WarehousingBusinessProcessService<ConsignmentModel> getConsignmentBusinessProcessService()
	{
		return this.consignmentBusinessProcessService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	protected BackofficeLocaleService getCockpitLocaleService()
	{
		return this.cockpitLocaleService;
	}

	protected CockpitEventQueue getCockpitEventQueue()
	{
		return this.cockpitEventQueue;
	}

	protected DefaultWarehouseStockService getWarehouseStockService()
	{
		return this.warehouseStockService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}
}

