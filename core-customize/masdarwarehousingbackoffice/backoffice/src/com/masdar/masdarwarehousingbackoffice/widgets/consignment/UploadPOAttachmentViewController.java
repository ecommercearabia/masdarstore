package com.masdar.masdarwarehousingbackoffice.widgets.consignment;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.sql.Timestamp;
import java.time.Instant;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;


public class UploadPOAttachmentViewController extends DefaultWidgetController
{

	private static final Logger LOG = Logger.getLogger(UploadPOAttachmentViewController.class.getName());

	private static final String MEDIA_FILE_POSTFIX = "_DeliveryNoteAttachment";
	private static final String MEDIA_FOLDER_NAME = "poattachments";
	protected static final String OUT_CONFIRM = "submit";
	protected static final Object COMPLETED = "completed";


	// Services
	@Resource(name = "mediaService")
	private transient MediaService mediaService;

	@Resource(name = "mediaContainerService")
	private transient MediaContainerService mediaContainerService;

	@Resource(name = "modelService")
	private transient ModelService modelService;

	@Resource(name = "mediaStorageConfigService")
	private transient MediaStorageConfigService mediaStorageConfigService;


	@Resource(name = "catalogVersionService")
	private transient CatalogVersionService catalogVersionService;

	// Objects
	private transient Media media;
	private transient ConsignmentModel consignment;

	@Wire
	private Label filenameLabel;

	@ViewEvent(componentID = "uploadButton", eventName = Events.ON_UPLOAD)
	public void doUpload(final UploadEvent event)
	{
		final Media upload = event.getMedia();
		if (!(upload instanceof org.zkoss.image.Image || "pdf".equalsIgnoreCase(upload.getFormat())))
		{
			Messagebox.show("Unsupported Format.\n Supported formats are [PNG, JPEG, JPG, PDF]");
			return;
		}

		if (filenameLabel != null)
		{
			filenameLabel.setValue(event.getMedia().getName());
		}
		this.media = event.getMedia();
	}

	protected MediaFolderModel getDeliveryNoteAttachmentsMediaFolder()
	{
		MediaFolderModel folder = new MediaFolderModel();
		final String folderName = MEDIA_FOLDER_NAME;
		try
		{
			folder = getMediaService().getFolder(folderName);
		}
		catch (final UnknownIdentifierException uie)
		{
			folder.setQualifier(folderName);
			folder.setPath(folderName);
			modelService.save(folder);
		}
		catch (final AmbiguousIdentifierException aie)
		{
			folder = getMediaService().getRootFolder();
		}
		return folder;
	}

	private void setConsignmentPOAttachment()
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("Default", "Staged");

		final MediaModel mediaModel = modelService.create(MediaModel.class);
		final Timestamp ts = Timestamp.from(Instant.now());
		final String finalName = consignment.getCode() + " - " + ts.toString();

		final MediaFolderModel folder = getDeliveryNoteAttachmentsMediaFolder();

		mediaModel.setCode(finalName);
		mediaModel.setCatalogVersion(catalogVersion);
		mediaModel.setRealFileName(finalName + MEDIA_FILE_POSTFIX + "." + media.getFormat());
		mediaModel.setFolder(folder);
		try
		{
			modelService.save(mediaModel);
			mediaService.setStreamForMedia(mediaModel, media.getStreamData());
		}
		catch (final ModelSavingException e)
		{
			LOG.error(e.getMessage());
		}
		modelService.refresh(mediaModel);
		getConsignment().setDeliveryNoteAttachment(mediaModel);
		modelService.save(consignment);
		modelService.refresh(consignment);

	}


	@SocketEvent(socketId = "consignmentInput")
	public void initReallocationConsignmentForm(final ConsignmentModel consignment)
	{
		setConsignment(consignment);
	}

	@ViewEvent(componentID = "confirmupload", eventName = Events.ON_CLICK)
	public void confirmUpload()
	{
		if (this.media != null)
		{
			setConsignmentPOAttachment();
		}
		this.sendOutput(OUT_CONFIRM, COMPLETED);
	}



	public ConsignmentModel getConsignment()
	{
		return consignment;
	}


	protected void setConsignment(final ConsignmentModel consignment)
	{
		this.consignment = consignment;
	}

	protected MediaService getMediaService()
	{
		return mediaService;
	}
}

