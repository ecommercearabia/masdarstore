# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2018 SAP SE or an SAP affiliate company.
# All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# OMS velocity template for printing label

$contentCatalog=masdarContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$lang=ar
$picture=media(code, $contentCV);

# CMS components and Email velocity templates
UPDATE RendererTemplate ; code[unique=true]               ; description[lang=$lang]           ; templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                        ; shipping-label-template         ; "shipping-label-template"         ; jar:/masdarordermanagementcustomaddon/import/documents/shippingLayout.vm                                      
                        ; return-shipping-label-template  ; "return-shipping-label-template"  ; jar:/masdarordermanagementcustomaddon/import/documents/returnShippingLayout.vm                                
                        ; pick-slip-template              ; "pick-slip-template"              ; jar:/masdarordermanagementcustomaddon/import/documents/pickLayout.vm                                          
                        ; consolidated-pick-slip-template ; "consolidated-pick-slip-template" ; jar:/masdarordermanagementcustomaddon/import/documents/consolidatedPickLayout.vm                              
                        ; pack-template                   ; "pack-template"                   ; jar:/masdarordermanagementcustomaddon/import/documents/packLayout.vm                                          
                        ; export-form-template            ; "export-form-template"            ; jar:/masdarordermanagementcustomaddon/import/documents/exportFormLayout.vm                                    
                        ; return-form-template            ; "return-form-template"            ; jar:/masdarordermanagementcustomaddon/import/documents/returnFormLayout.vm                                    
                        ; order-invoice-template          ; "order-invoice-template"          ; jar:/masdarordermanagementcustomaddon/import/documents/orderInvoiceLabel.vm                                   
                        ; quote-invoice-template          ; "quote-invoice-template"          ; jar:/masdarordermanagementcustomaddon/import/documents/quoteInvoiceLabel.vm

# CMS Image Components  
INSERT_UPDATE CMSImageComponent ; $contentCV[unique=true] ; uid[unique=true]      ; $picture[lang=$lang]
                                ;                         ; DocumentSiteLogoImage ; MasdarLogo          ;  

# Content Slots                 
UPDATE ContentSlot ; $contentCV[unique=true] ; uid[unique=true]     ; cmsComponents(uid,$contentCV)
                   ;                         ; DocumentSiteLogoSlot ; DocumentSiteLogoImage        

# Import config properties into impex macros for modulegen
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor] ; pk[unique=true]
$emailResource=$config-emailResourceValue                                                                  

# CMS components and Email velocity templates                                                              
UPDATE RendererTemplate ; code[unique=true]                 ; description[lang=$lang]      ; templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                        ; masdar_Email_Return_Label_Body    ; "Return Label Email Body"    ; jar:/masdarordermanagementcustomaddon/import/documents/email-returnLabelBody.vm                               
                        ; masdar_Email_Return_Label_Subject ; "Return Label Email Subject" ; jar:/masdarordermanagementcustomaddon/import/documents/email-returnLabelSubject.vm                            

# Email Pages           
UPDATE EmailPage ; $contentCV[unique=true] ; uid[unique=true] ; fromEmail[lang=$lang]   ; fromName[lang=$lang]    
                 ;                         ; ReturnLabelEmail ; "care@masdaronline.com" ; "Customer Services Team"
