/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.constants;

/**
 * Global class for all Masdarb2bacceleratorcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bacceleratorcustomaddonConstants extends GeneratedMasdarb2bacceleratorcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarb2bacceleratorcustomaddon";
	public static final String ADDON_PREFIX = "addon:/masdarb2bacceleratorcustomaddon";

	private Masdarb2bacceleratorcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
