/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.AbstractB2BCheckoutStepValidator;
import com.masdar.facades.facade.CustomCheckoutFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;

import javax.annotation.Resource;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public class DefaultB2BPaymentTypeCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{
	@Resource(name = "customB2BCheckoutFacade")
	private CustomCheckoutFacade b2bCheckoutFacade;

	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		return ValidationResults.SUCCESS;
	}

	@Override
	public ValidationResults validateOnExit()
	{
		List<B2BPaymentTypeData> paymentTypes = b2bCheckoutFacade.getSupportedPaymentTypes();

		if (CollectionUtils.isEmpty(paymentTypes))
		{
			return ValidationResults.REDIRECT_TO_CART;
		}

		final B2BPaymentTypeData checkoutPaymentType = getCheckoutFacade().getCheckoutCart().getPaymentType();

		if (checkoutPaymentType != null && CheckoutPaymentType.ACCOUNT.getCode().equals(checkoutPaymentType.getCode())
				&& !getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		if (checkoutPaymentType != null && CheckoutPaymentType.BANK.getCode().equals(checkoutPaymentType.getCode())
				&& !getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		
		return ValidationResults.SUCCESS;
	}

}
