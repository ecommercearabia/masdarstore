/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import com.masdar.facades.facade.CustomCheckoutFacade;
import com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.AbstractB2BCheckoutStepValidator;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public class DefaultB2BSummaryCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{

	@Resource(name = "customB2BCheckoutFacade")
	private CustomCheckoutFacade customCheckoutFacade;
	
	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final CartData checkoutCart = getCheckoutFacade().getCheckoutCart();
		final B2BPaymentTypeData checkoutPaymentType = checkoutCart.getPaymentType();

		if (checkoutPaymentType == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentType.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		if (!getCustomCheckoutFacade().hasValidCr())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.payment.type.unit.invalid.cr");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		final ValidationResults esalResult = checkEsalAttributes(redirectAttributes);
		if (Objects.nonNull(esalResult))
		{
			return esalResult;
		}

		if (CheckoutPaymentType.ACCOUNT.getCode().equals(checkoutPaymentType.getCode())
				&& checkoutCart.getCostCenter() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.costCenter.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}


		if (CheckoutPaymentType.BANK.getCode().equals(checkoutPaymentType.getCode())
				&& getCheckoutFacade().getCheckoutCart().getSelectedBankTransfer() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.bankTransfer.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		
		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		if (!getCustomCheckoutFacade().hasCity())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.city.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}

		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().hasShippingItems())
		{
			checkoutCart.setDeliveryAddress(null);
		}
		
		return ValidationResults.SUCCESS;
	}
	
	protected ValidationResults checkEsalAttributes(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFacade().isCustomerValidForEsal())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.customerMobile.notvalid");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		
		if (!getCustomCheckoutFacade().isAddressValidForEsal())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notvalid");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		
		return null;
	}
	

	/**
	 * @return the customCheckoutFacade
	 */
	protected CustomCheckoutFacade getCustomCheckoutFacade()
	{
		return customCheckoutFacade;
	}
}