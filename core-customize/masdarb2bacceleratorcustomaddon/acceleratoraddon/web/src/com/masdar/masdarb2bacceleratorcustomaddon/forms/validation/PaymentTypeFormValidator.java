/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.forms.validation;

import com.masdar.facades.facade.BankTransferFacade;
import com.masdar.masdarb2bacceleratorcustomaddon.forms.PaymentTypeForm;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCostCenterService;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.services.BaseStoreService;
import com.masdar.core.service.CreditLimitCheckService;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validator for {@link PaymentTypeForm}.
 */
@Component("paymentTypeFormValidator")
public class PaymentTypeFormValidator implements Validator
{
	@Resource(name = "bankTransferFacade")
	private BankTransferFacade bankTransferFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "b2bUnitService")
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource(name = "b2bCustomerService")
	private B2BCustomerService<B2BCustomerModel, B2BUnitModel> b2bCustomerService;

	@Resource(name = "b2bCostCenterService")
	private B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService;

	@Resource(name = "defaultCreditLimitCheckService")
	private CreditLimitCheckService creditLimitCheckService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return PaymentTypeForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		if (object instanceof PaymentTypeForm)
		{
			final PaymentTypeForm paymentTypeForm = (PaymentTypeForm) object;

			if (!CheckoutPaymentType.ESAL.getCode().equals(paymentTypeForm.getPaymentType())
					&& StringUtils.isBlank(paymentTypeForm.getPurchaseOrderNumber()))
			{
				errors.rejectValue("purchaseOrderNumber", "general.required");

			}
			if (CheckoutPaymentType.ACCOUNT.getCode().equals(paymentTypeForm.getPaymentType())
					&& StringUtils.isBlank(paymentTypeForm.getCostCenterId()))
			{
				errors.rejectValue("costCenterId", "general.required");
			}
			else if (CheckoutPaymentType.BANK.getCode().equals(paymentTypeForm.getPaymentType())
					&& (StringUtils.isBlank(paymentTypeForm.getBankTransferCode())
							|| !bankTransferFacade.isSupportedBankTransferForCurrentStore(paymentTypeForm.getBankTransferCode())))
			{
				errors.rejectValue("bankTransferCode", "general.required");
			}
			if ((!CheckoutPaymentType.ESAL.getCode().equals(paymentTypeForm.getPaymentType())
					&& baseStoreService.getCurrentBaseStore() != null
					&& baseStoreService.getCurrentBaseStore().isUploadFileForPaymentTypeRequired() && cartService.hasSessionCart()
					&& cartService.getSessionCart().getPaymentTypeAttachment() == null && fileNotValid(paymentTypeForm)))
			{
				errors.rejectValue("files", "general.required");
			}
			validateCashCustomerPaymentType(errors, (PaymentTypeForm) object);
			validateCreditLimit(errors, (PaymentTypeForm) object);
		}
	}


	private void validateCreditLimit(Errors error, PaymentTypeForm paymentTypeForm)
	{
		if (!CheckoutPaymentType.ACCOUNT.getCode().equals(paymentTypeForm.getPaymentType()))
		{
			return;
		}

		B2BCustomerModel currentCustomer = getB2BCustomerService().getCurrentB2BCustomer();
		CartModel sessionCart = getCartService().getSessionCart();
		B2BCostCenterModel costCenter = getB2BCostCenterService().getCostCenterForCode(paymentTypeForm.getCostCenterId());
		if (currentCustomer == null || sessionCart == null || costCenter == null || costCenter.getUnit() == null )
		{
			error.rejectValue("paymentType", "checkout.payment.type.credit.limit.exceeded");
			return;
		}

		if (!getCreditLimitCheckService().isCreditAmountEnough(sessionCart, currentCustomer, costCenter))
		{
			error.rejectValue("paymentType", "checkout.payment.type.credit.limit.exceeded");
			error.rejectValue("costCenterId", "checkout.payment.type.credit.limit.exceeded");
			return;
		}
	}

	/**
	 * @author Husam Dababneh
	 * @param paymentTypeForm
	 */
	private void validateCashCustomerPaymentType(Errors error, PaymentTypeForm paymentTypeForm)
	{
		CartModel sessionCart = cartService.getSessionCart();
		if (isCashCustomer(sessionCart) && !"ESAL".equals(paymentTypeForm.getPaymentType()))
		{
			error.rejectValue("paymentType", "text.account.paymentType.notSupported");
		}
	}

	private boolean isCashCustomer(CartModel cart)
	{
		B2BCustomerModel customer = (B2BCustomerModel) cart.getUser();
		Optional<B2BUnitModel> findAny = b2bUnitService.getAllUnitsOfOrganization(customer).stream().filter(e -> e.isCashCustomer())
				.findAny();
		if (findAny.isPresent())
		{
			return true;
		}
		return false;
	}

	private boolean fileNotValid(PaymentTypeForm paymentTypeForm)
	{
		return paymentTypeForm.getFiles() == null || paymentTypeForm.getFiles().isEmpty()
				|| paymentTypeForm.getFiles().get(0).getOriginalFilename() == null
				|| paymentTypeForm.getFiles().get(0).getOriginalFilename().isEmpty();
	}


	protected B2BCustomerService<B2BCustomerModel, B2BUnitModel> getB2BCustomerService()
	{
		return b2bCustomerService;
	}

	protected B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> getB2BCostCenterService()
	{
		return b2bCostCenterService;
	}

	protected CartService getCartService()
	{
		return cartService;
	}

	protected CreditLimitCheckService getCreditLimitCheckService()
	{
		return creditLimitCheckService;
	}
}
