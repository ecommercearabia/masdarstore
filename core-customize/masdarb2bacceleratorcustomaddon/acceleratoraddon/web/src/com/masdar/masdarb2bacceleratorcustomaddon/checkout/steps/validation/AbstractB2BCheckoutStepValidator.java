/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.annotation.Resource;

import com.masdar.masdarb2bacceleratorcustomaddon.security.B2BUserGroupProvider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.masdar.core.resolver.CustomUrlResolver;
import com.masdar.facades.facade.CustomCheckoutFacade;


/**
 * Abstract checkout step validator for the B2B accelerator.
 */
public abstract class AbstractB2BCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private B2BUserGroupProvider b2bUserGroupProvider;

	@Resource(name = "customB2BCheckoutFacade")
	private CustomCheckoutFacade customCheckoutFacade;
	
	private static final Logger LOG = Logger.getLogger(AbstractB2BCheckoutStepValidator.class);

	@Resource(name = "updateProfileUrlResolver")
	private CustomUrlResolver updateProfileUrlResolver;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getB2bUserGroupProvider().isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.error.invalid.accountType");
			return ValidationResults.FAILED;
		}

		if (!getCheckoutFlowFacade().hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		return doValidateOnEnter(redirectAttributes);
	}

	/**
	 * Performs implementation specific validation on entering a checkout step after the common validation has been
	 * performed in the abstract implementation.
	 *
	 * @param redirectAttributes
	 * @return {@link ValidationResults}
	 */
	protected abstract ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes);

	protected B2BUserGroupProvider getB2bUserGroupProvider()
	{
		return b2bUserGroupProvider;
	}

	@Required
	public void setB2bUserGroupProvider(final B2BUserGroupProvider b2bUserGroupProvider)
	{
		this.b2bUserGroupProvider = b2bUserGroupProvider;
	}

	protected CustomUrlResolver getUpdateProfileUrlResolver()
	{
		return updateProfileUrlResolver;
	}
	
	protected ValidationResults checkEsalAttributes(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFacade().isCustomerValidForEsal())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.customerMobile.notvalid",new Object[] {getUpdateProfileUrlResolver().getUrl()});
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		
		if (!getCustomCheckoutFacade().isAddressValidForEsal())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notvalid");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		
		return null;
	}
	
	/**
	 * @return the customCheckoutFacade
	 */
	protected CustomCheckoutFacade getCustomCheckoutFacade()
	{
		return customCheckoutFacade;
	}
}
