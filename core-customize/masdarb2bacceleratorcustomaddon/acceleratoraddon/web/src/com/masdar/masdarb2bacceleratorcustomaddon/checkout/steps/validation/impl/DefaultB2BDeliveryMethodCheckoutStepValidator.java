/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.Objects;

import javax.annotation.Resource;

import com.masdar.masdarb2bacceleratorcustomaddon.checkout.steps.validation.AbstractB2BCheckoutStepValidator;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.masdar.facades.facade.CustomCheckoutFacade;


public class DefaultB2BDeliveryMethodCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{

	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		

		if (!getCustomCheckoutFacade().hasValidCr())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.payment.type.unit.invalid.cr");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}

		if (!getCustomCheckoutFacade().hasCity())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.city.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}


		final ValidationResults esalResult = checkEsalAttributes(redirectAttributes);
		if (Objects.nonNull(esalResult))
		{
			return esalResult;
		}

		return ValidationResults.SUCCESS;
	}

}
