/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.forms;

/**
 * Pojo for 'reorder' form.
 */
public class ReorderForm
{
	private String orderCode;

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}
}
