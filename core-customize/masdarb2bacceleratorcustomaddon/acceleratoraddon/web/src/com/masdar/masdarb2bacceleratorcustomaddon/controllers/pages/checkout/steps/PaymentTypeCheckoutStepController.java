/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import com.masdar.masdarb2bacceleratorcustomaddon.controllers.Masdarb2bacceleratorcustomaddonControllerConstants;
import com.masdar.masdarb2bacceleratorcustomaddon.forms.PaymentTypeForm;
import com.masdar.masdarb2bacceleratorcustomaddon.forms.validation.PaymentTypeFormValidator;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.B2BCostCenterFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BCostCenterData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import com.masdar.facades.facade.BankTransferFacade;
import com.masdar.facades.facade.CustomCheckoutFacade;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import com.masdar.masdarfacades.data.BankTransferData;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.http.MediaType;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.google.common.collect.Maps;


@Controller
@RequestMapping(value = "/checkout/multi/payment-type")
public class PaymentTypeCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOGGER = Logger.getLogger(PaymentTypeCheckoutStepController.class);

	private final static String PAYMENT_TYPE = "payment-type";
	public static final String MAX_UPLOAD_SIZE = "maxUploadSize";
	public static final String MAX_UPLOAD_SIZE_PROPERTY = "paymentType.form.max.upload.size.bytes";
	public static final String MAX_UPLOAD_SIZE_MB = "maxUploadSizeMB";
	public static final String ALLOWED_UPLOADED_FORMATS = "allowedUploadedFormats";


	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	public static final String FORM_GLOBAL_ERROR_KEY = "form-global-error";
	@Resource(name = "customB2BCheckoutFacade")
	private CustomCheckoutFacade b2bCheckoutFacade;

	@Resource(name = "bankTransferFacade")
	private BankTransferFacade bankTransferFacade;

	@Resource(name = "costCenterFacade")
	private B2BCostCenterFacade costCenterFacade;

	@Resource(name = "paymentTypeFormValidator")
	private PaymentTypeFormValidator paymentTypeFormValidator;

	@ModelAttribute("paymentTypes")
	public Collection<B2BPaymentTypeData> getAllB2BPaymentTypes()
	{
		return b2bCheckoutFacade.getSupportedPaymentTypes();
	}

	@ModelAttribute("backTransfers")
	public List<BankTransferData> getBankTransfers()
	{
		return b2bCheckoutFacade.getSupportedBankTransfers();
	}

	@ModelAttribute("costCenters")
	public List<? extends B2BCostCenterData> getVisibleActiveCostCenters()
	{
		final List<? extends B2BCostCenterData> costCenterData = costCenterFacade.getActiveCostCenters();
		return costCenterData == null ? Collections.<B2BCostCenterData> emptyList() : costCenterData;
	}

	@Override
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_TYPE)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		b2bCheckoutFacade.resetPaymentTypeForCurrentCart();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		List<B2BPaymentTypeData> paymentTypes = (List<B2BPaymentTypeData>) getAllB2BPaymentTypes();
		if ((cartData.getPaymentType() == null || cartData.getPaymentType().getCode() == null) && paymentTypes != null
				&& !paymentTypes.isEmpty())
		{
			b2bCheckoutFacade.setPaymentTypeForCurrentCart(paymentTypes.get(0).getCode());
			cartData.setPaymentType(paymentTypes.get(0));
		}
		getPaymentTypeFileSize(model);
		model.addAttribute("cartData", cartData);
		model.addAttribute("paymentTypeForm", preparePaymentTypeForm(cartData));
		prepareDataForPage(model);
		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return Masdarb2bacceleratorcustomaddonControllerConstants.Views.Pages.MultiStepCheckout.ChoosePaymentTypePage;
	}

	@RequestMapping(value = "/choose", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@RequireHardLogIn
	public ResponseEntity<Object> choose(@ModelAttribute
	final PaymentTypeForm paymentTypeForm, final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException, CommerceCartModificationException
	{
		paymentTypeFormValidator.validate(paymentTypeForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			final List<Map<String, String>> list = buildErrorMessagesMap(bindingResult);
			list.add(buildMessageMap(FORM_GLOBAL_ERROR_KEY, FORM_GLOBAL_ERROR));
			return new ResponseEntity<>(list, HttpStatus.BAD_REQUEST);
		}

		if (paymentTypeForm.getFiles() != null && !paymentTypeForm.getFiles().isEmpty() && !paymentTypeForm.getFiles().isEmpty()
				&& paymentTypeForm.getFiles().get(0).getOriginalFilename() != null
				&& !paymentTypeForm.getFiles().get(0).getOriginalFilename().isEmpty())
		{
			try
			{
				b2bCheckoutFacade.setPaymentTypeAttachmentForCurrentCart(paymentTypeForm.getFiles());
			}
			catch (de.hybris.platform.ticket.service.UnsupportedAttachmentException e)
			{
				LOGGER.error("PaymentTypeCheckoutStepController: choose -> " + e.getMessage());
				List<Pair<String, String>> errors = new ArrayList<>();

				Pair<String, String> error = Pair.of("attachmentFiles", "files.is.invaild");
				errors.add(error);
				final List<Map<String, String>> list = getErrorMessagesMap(errors);
				list.add(buildMessageMap(FORM_GLOBAL_ERROR_KEY, FORM_GLOBAL_ERROR));
				return new ResponseEntity<>(list, HttpStatus.BAD_REQUEST);
			}
		}
		updateCheckoutCart(paymentTypeForm);

		checkAndSelectDeliveryAddress(paymentTypeForm);
		return new ResponseEntity<>(getCheckoutStep().nextStep(), HttpStatus.OK);

	}

	protected void updateCheckoutCart(final PaymentTypeForm paymentTypeForm)
	{
		final CartData cartData = new CartData();

		// set payment type
		final B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();
		paymentTypeData.setCode(paymentTypeForm.getPaymentType());

		cartData.setPaymentType(paymentTypeData);

		// set cost center
		if (CheckoutPaymentType.ACCOUNT.getCode().equals(cartData.getPaymentType().getCode())
				|| CheckoutPaymentType.BANK.getCode().equals(cartData.getPaymentType().getCode()))
		{
			final B2BCostCenterData costCenter = new B2BCostCenterData();
			costCenter.setCode(paymentTypeForm.getCostCenterId());

			cartData.setCostCenter(costCenter);
		}
		else if (CheckoutPaymentType.ESAL.getCode().equals(cartData.getPaymentType().getCode())
				&& !CollectionUtils.isEmpty(getVisibleActiveCostCenters()))
		{
			cartData.setCostCenter(getVisibleActiveCostCenters().get(0));
		}

		// set purchase order number
		cartData.setPurchaseOrderNumber(paymentTypeForm.getPurchaseOrderNumber());
		if (StringUtils.isNotBlank(paymentTypeForm.getBankTransferCode()))
		{
			Optional<BankTransferData> bank = bankTransferFacade.getByCode(paymentTypeForm.getBankTransferCode());
			cartData.setSelectedBankTransfer(bank.isPresent() ? bank.get() : null);
		}

		b2bCheckoutFacade.updateCheckoutCart(cartData);
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	protected PaymentTypeForm preparePaymentTypeForm(final CartData cartData)
	{
		final PaymentTypeForm paymentTypeForm = new PaymentTypeForm();

		// set payment type
		if (cartData.getPaymentType() != null && StringUtils.isNotBlank(cartData.getPaymentType().getCode()))
		{
			paymentTypeForm.setPaymentType(cartData.getPaymentType().getCode());
		}
		else
		{
			paymentTypeForm.setPaymentType(CheckoutPaymentType.ACCOUNT.getCode());
		}

		// set cost center
		if (cartData.getCostCenter() != null && StringUtils.isNotBlank(cartData.getCostCenter().getCode()))
		{
			paymentTypeForm.setCostCenterId(cartData.getCostCenter().getCode());
		}
		else if (!CollectionUtils.isEmpty(getVisibleActiveCostCenters()) && getVisibleActiveCostCenters().size() == 1)
		{
			paymentTypeForm.setCostCenterId(getVisibleActiveCostCenters().get(0).getCode());
		}

		paymentTypeForm.setBankTransferCode(
				cartData.getSelectedBankTransfer() != null ? cartData.getSelectedBankTransfer().getCode() : null);
		// set purchase order number
		paymentTypeForm.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
		return paymentTypeForm;
	}

	protected void checkAndSelectDeliveryAddress(final PaymentTypeForm paymentTypeForm)
	{
		if (CheckoutPaymentType.ACCOUNT.getCode().equals(paymentTypeForm.getPaymentType())
				|| CheckoutPaymentType.BANK.getCode().equals(paymentTypeForm.getPaymentType())
				|| CheckoutPaymentType.ESAL.getCode().equals(paymentTypeForm.getPaymentType()))
		{
			final List<? extends AddressData> deliveryAddresses = getCheckoutFacade().getSupportedDeliveryAddresses(true);
			if (deliveryAddresses.size() == 1)
			{
				getCheckoutFacade().setDeliveryAddress(deliveryAddresses.get(0));
			}
		}
	}

	private void getPaymentTypeFileSize(final Model model)
	{
		String sizeStr = null;
		Long size = null;
		try
		{
			sizeStr = (String) getConfigurationService().getConfiguration().getProperty(MAX_UPLOAD_SIZE_PROPERTY);
			if (sizeStr == null)
			{
				size = (Long.valueOf(25000));
			}
			else
			{
				size = Long.valueOf(sizeStr);
			}
		}
		catch (final ClassCastException e)
		{
			size = (Long.valueOf(25000));
		}
		model.addAttribute(MAX_UPLOAD_SIZE, size);
		model.addAttribute(MAX_UPLOAD_SIZE_MB, FileUtils.byteCountToDisplaySize(size.longValue()));
		model.addAttribute(ALLOWED_UPLOADED_FORMATS, b2bCheckoutFacade.getAllowedUploadedFormatsForPaymentTypeForCurrentCart());


	}

	/**
	 * Build the error message list with map contains the validation error code and localised message.
	 *
	 * @param bindingResult
	 * @return Map of error code and message
	 */
	protected List<Map<String, String>> buildErrorMessagesMap(final BindingResult bindingResult)
	{
		return bindingResult.getAllErrors().stream().filter(err -> err.getCode() != null && err.getCode().length() > 0).map(err -> {
			final Map<String, String> map = Maps.newHashMap();
			final String keyStr = err.getCodes()[0].substring(err.getCodes()[0].lastIndexOf('.') + 1, err.getCodes()[0].length());
			final String valusStr = getMessageSource().getMessage(err.getCode(), null, getI18nService().getCurrentLocale());
			map.put(keyStr, valusStr);
			return map;
		}).collect(Collectors.toList());
	}

	protected List<Map<String, String>> getErrorMessagesMap(List<Pair<String, String>> errorKeys)
	{
		if (CollectionUtils.isEmpty(errorKeys))
		{
			return Collections.EMPTY_LIST;
		}
		return errorKeys.stream().filter(err -> err != null).map(err -> {
			final Map<String, String> map = Maps.newHashMap();
			final String keyStr = err.getKey();
			final String valusStr = getMessageSource().getMessage(err.getValue(), null, getI18nService().getCurrentLocale());
			map.put(keyStr, valusStr);
			return map;
		}).collect(Collectors.toList());
	}

	/**
	 * Build a map with key and localsed Message.
	 *
	 * @param key
	 *           the render key
	 * @param localisedKey
	 *           the localised message key
	 * @return Map of error code and message
	 */
	protected Map<String, String> buildMessageMap(final String key, final String localisedKey)
	{

		final Map<String, String> map = Maps.newHashMap();
		map.put(key, getMessageSource().getMessage(localisedKey, null, getI18nService().getCurrentLocale()));

		return map;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_TYPE);
	}

}
