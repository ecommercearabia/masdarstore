/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public class PaymentTypeForm
{
	private String paymentType;
	private String costCenterId;
	private String purchaseOrderNumber;
	private String bankTransferCode;
	private List<MultipartFile> files;


	@NotNull(message = "{general.required}")
	@Size(min = 1, max = 255, message = "{general.required}")
	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getCostCenterId()
	{
		return costCenterId;
	}

	public void setCostCenterId(final String costCenterId)
	{
		this.costCenterId = costCenterId;
	}

	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	
	/**
	 * @param bankTransferCode the bankTransferCode to set
	 */
	public void setBankTransferCode(String bankTransferCode)
	{
		this.bankTransferCode = bankTransferCode;
	}
	
	/**
	 * @return the bankTransferCode
	 */
	public String getBankTransferCode()
	{
		return bankTransferCode;
	}
	
	/**
	 * @return the files
	 */
	public List<MultipartFile> getFiles()
	{
		return files;
	}
	
	/**
	 * @param files the files to set
	 */
	public void setFiles(List<MultipartFile> files)
	{
		this.files = files;
	}
}
