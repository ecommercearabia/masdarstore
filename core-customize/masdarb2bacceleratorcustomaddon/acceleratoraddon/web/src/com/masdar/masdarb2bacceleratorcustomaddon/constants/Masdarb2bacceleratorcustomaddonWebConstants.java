/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bacceleratorcustomaddon.constants;

/**
 * Global class for all Masdarb2bacceleratorcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class Masdarb2bacceleratorcustomaddonWebConstants
{
	//Dummy field to avoid pmd error - delete when you add the first real constant!
	public static final String deleteThisDummyField = "DELETE ME";

	private Masdarb2bacceleratorcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
