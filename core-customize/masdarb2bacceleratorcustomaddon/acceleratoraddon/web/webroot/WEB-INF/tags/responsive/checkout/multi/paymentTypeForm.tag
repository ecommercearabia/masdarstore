<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<form:form id="selectPaymentTypeForm" modelAttribute="paymentTypeForm" action="${request.contextPath}/checkout/multi/payment-type/choose" method="post"  enctype="multipart/form-data">
   
    <div class="step-body-form">
    
        <div class="radiobuttons_paymentselection">
            <c:forEach items="${paymentTypes}" var="paymentType">
                        <spring:theme code="checkout.multi.paymentType.label.${paymentType.code}" var="paymentTypelpl" htmlEscape="false" />
            
                <form:radiobutton path="paymentType" id="PaymentTypeSelection_${paymentType.code}" value="${paymentType.code}" label="${paymentTypelpl}" />
                <br>
            </c:forEach>
        </div>
        <div class="alertpaymentType"></div>
        <div class="">
        	<formElement:formInputBox idKey="purchaseOrderNumber" labelKey="checkout.multi.purchaseOrderNumber.label" path="purchaseOrderNumber" inputCSS="text" />

        <div id="costCenter" class="ifesal ${(empty cartData.paymentType || empty cartData.paymentType.code || cartData.paymentType.code eq 'ESAL') ? 'hidden' : ''}" >
            <formElement:formSelectBox idKey="costCenterSelectEroor" labelKey="checkout.multi.costCenter.label" path="costCenterId" skipBlank="false" skipBlankMessageKey="checkout.multi.costCenter.title.pleaseSelect" itemValue="code" itemLabel="name" items="${costCenters}" mandatory="true" selectCSSClass="form-control"/>
           
        </div>
        <c:set var = "allowedUploadedFormatsTemp" value = "${fn:replace(allowedUploadedFormats, ' ', '')}" />
        <c:set var = "allowedUploadedFormatsTemp" value = "${fn:replace(allowedUploadedFormatsTemp, ',', ', .')}" />
		<div id="bankTransfer">
            <formElement:formSelectBox idKey="bankTransferCode" labelKey="checkout.multi.bankTransfer.label" path="bankTransferCode" skipBlank="false" skipBlankMessageKey="checkout.multi.costCenter.title.pleaseSelect" itemValue="code" itemLabel="name" items="${backTransfers}" mandatory="true" selectCSSClass="form-control"/>
        </div>


        		<c:if test="${not empty cartData.paymentTypeAttachment.URL}">
	        		<div id="paymentTypeAttachmentId" >
	        			<spring:theme code="paymentTypeAttachment.paymentTypeAttachment"	text="Attachment:" /> 
	        					<a href="${contextPath}/${cartData.paymentTypeAttachment.URL}" download="${cartData.paymentTypeAttachment.filename}" > ${cartData.paymentTypeAttachment.filename}</a><br>
					</div>
				</c:if>
				<div class="form-group file-upload js-file-upload">
					<label class="control-label file-upload__label" for="files">
						<spring:theme code="customItem.selectFile" text="Select a file" /> 
					</label>
					<div class="file-upload__wrapper btn btn-default btn-small">
						<span> 
						<spring:theme code="customItem.chooseFile"
								text="Choose file" /> <i class="fa fa-paperclip"></i>
						</span> 
						<input type="file" accept="${allowedUploadedFormatsTemp}" name="files" id="attachmentFiles" 
							class="file-upload__input js-file-upload__input"
							data-max-upload-size="${maxUploadSize}" />
					</div>
					<span class="file-upload__file-name js-file-upload__file-name">
						<spring:theme code="customItem.noFileChosen" />
					</span>
				</div>
				<div class="help-file-block">
					<spring:theme code="file.allowed.extension"
						text="Only the following file types are allowed" arguments="" /> ( ${allowedUploadedFormats} )
				</div>
				<div id="filesError" class="help-block" style="display: none;">	<spring:theme code="checkout.multi.paymenttypefile.size.invalid" arguments="${maxUploadSizeMB}"/> </div>
        	
        </div>
            </div>

	<button id="choosePaymentType_continue_button" type="submit" class="btn btn-primary btn-block checkout-next">
		<spring:theme code="checkout.multi.paymentType.continue"/>
	</button>
		
</form:form>
