ACC.paymentType = {
		
		_autoload: [
		    		"bindPaymentTypeSelect",
		    		"showHideCostCenterSelect"
		    	],

	bindPaymentTypeSelect: function ()
	{
		$("input:radio[name='paymentType']").change(function()
		{
			ACC.paymentType.showHideCostCenterSelect();
		});
	},

	showHideCostCenterSelect: function() {
		var paymentTypeSelected =  $("input:radio[name='paymentType']:checked").val();
		$("#filesError").hide();
		
		
		if(paymentTypeSelected == "ACCOUNT") {
			$("#costCenter").show();
			$("#bankTransfer").hide();
		} else if(paymentTypeSelected == "BANK") {
			$("#costCenter").show();
			$("#bankTransfer").show();

		} else if(paymentTypeSelected == "ESAL") {
			$("#costCenter").show();
			$("#bankTransfer").hide();

		}else
		{
			$("#costCenter").hide();
			$("#bankTransfer").hide();
		}
	}
}
	
	
