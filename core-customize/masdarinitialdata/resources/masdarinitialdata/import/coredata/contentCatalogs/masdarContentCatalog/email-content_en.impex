# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import the CMS content for the Masdar site emails
#
$contentCatalog=masdarContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$jarEmailResource=jar:com.masdar.initialdata.constants.MasdarInitialDataConstants&/masdarinitialdata/import/coredata/contentCatalogs/masdarContentCatalog/emails

# Import config properties into impex macros for modulegen
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]
$emailResource=$config-emailResourceValue

# Language
$lang=en

# CMS components and Email velocity templates
UPDATE RendererTemplate;code[unique=true];description[lang=$lang];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 ;masdar-BannerComponent-template;"CMSBannerComponent Template";$emailResource/email-bannerComponentTemplate.vm
 ;masdar-CMSImageComponent-template;"CMSImageComponent Template";$emailResource/email-cmsImageComponentTemplate.vm
 ;masdar-CMSLinkComponent-template;"CMSLinkComponent Template";$emailResource/email-cmsLinkComponentTemplate.vm
 ;masdar-CMSParagraphComponent-template;"CMSParagraphComponent Template";$emailResource/email-cmsParagraphComponentTemplate.vm
 ;masdar-SimpleBannerComponent-template;"CMSSimpleBannerComponent Template";$emailResource/email-bannerComponentTemplate.vm
 ;masdar_Email_Customer_Registration_Body;"Customer Registration Email Body";$emailResource/email-customerRegistrationBody.vm
 ;masdar_Email_Customer_Registration_Subject;"Customer Registration Email Subject";$emailResource/email-customerRegistrationSubject.vm
 ;masdar_Email_Delivery_Sent_Body;"Delivery Sent Email Body";$emailResource/email-deliverySentBody.vm
 ;masdar_Email_Delivery_Sent_Subject;"Delivery Sent Email Subject";$emailResource/email-deliverySentSubject.vm
 ;masdar_Email_Forgotten_Password_Body;"Forgotten Password Email Body";$emailResource/email-forgottenPasswordBody.vm
 ;masdar_Email_Forgotten_Password_Subject;"Forgotten Password Email Subject";$emailResource/email-forgottenPasswordSubject.vm
 ;masdar_Email_NotPickedUpConsignment_Canceled_Body;"Not Picked Up Consignment Canceled Email Body";$emailResource/email-notPickedUpConsignmentCanceledBody.vm
 ;masdar_Email_NotPickedUpConsignment_Canceled_Subject;"Not Picked Up Consignment Canceled Email Subject";$emailResource/email-notPickedUpConsignmentCanceledSubject.vm
 ;masdar_Email_Order_Cancelled_Body;"Order Cancelled Email Body";$emailResource/email-orderCancelledBody.vm
 ;masdar_Email_Order_Cancelled_Subject;"Order Cancelled Email Subject";$emailResource/email-orderCancelledSubject.vm
 ;masdar_Email_Order_Collection_Reminder_Body;"Order Collection Reminder Body";$emailResource/email-orderCollectionReminderBody.vm
 ;masdar_Email_Order_Collection_Reminder_Subject;"Order Collection Reminder Subject";$emailResource/email-orderCollectionReminderSubject.vm
 ;masdar_Email_Order_Confirmation_Body;"Order Confirmation Email Body";$emailResource/email-orderConfirmationBody.vm
 ;masdar_Email_Order_Confirmation_Subject;"Order Confirmation Email Subject";$emailResource/email-orderConfirmationSubject.vm
 ;masdar_Email_Order_Move_To_CS_Body;"Order Move To CS Body";$emailResource/email-orderMoveToCsBody.vm
 ;masdar_Email_Order_Move_To_CS_Subject;"Order Move To CS Subject";$emailResource/email-orderMoveToCsSubject.vm
 ;masdar_Email_Order_Partially_Canceled_Body;"Order Partially Canceled Email Body";$emailResource/email-orderPartiallyCanceledBody.vm
 ;masdar_Email_Order_Partially_Canceled_Subject;"Order Partially Canceled Email Subject";$emailResource/email-orderPartiallyCanceledSubject.vm
 ;masdar_Email_Order_Partially_Refunded_Body;"Order Partially Refunded Email Body";$emailResource/email-orderPartiallyRefundedBody.vm
 ;masdar_Email_Order_Partially_Refunded_Subject;"Order Partially Refunded Email Subject";$emailResource/email-orderPartiallyRefundedSubject.vm
 ;masdar_Email_Order_Refund_Body;"Order Refund Email Body";$emailResource/email-orderRefundBody.vm
 ;masdar_Email_Order_Refund_Subject;"Order Refund Email Subject";$emailResource/email-orderRefundSubject.vm
 ;masdar_Email_Quote_Buyer_Cancellation_Body;"Quote Buyer Cancellation Email Body";$emailResource/email-quoteBuyerCancellationBody.vm
 ;masdar_Email_Quote_Buyer_Cancellation_Subject;"Quote Buyer Cancellation Email Subject";$emailResource/email-quoteBuyerCancellationSubject.vm
 ;masdar_Email_Quote_Buyer_Submission_Body;"Quote Buyer Submission Email Body";$emailResource/email-quoteBuyerSubmissionBody.vm
 ;masdar_Email_Quote_Buyer_Submission_Subject;"Quote Buyer Submission Email Subject";$emailResource/email-quoteBuyerSubmissionSubject.vm
 ;masdar_Email_Quote_Expired_Body;"Quote Expired Body";$emailResource/email-quoteExpiredBody.vm
 ;masdar_Email_Quote_Expired_Subject;"Quote Expired Subject";$emailResource/email-quoteExpiredSubject.vm
 ;masdar_Email_Quote_To_Expire_Soon_Body;"Quote To Expire Soon Body";$emailResource/email-quoteToExpireSoonBody.vm
 ;masdar_Email_Quote_To_Expire_Soon_Subject;"Quote To Expire Soon Subject";$emailResource/email-quoteToExpireSoonSubject.vm
 ;masdar_Email_Ready_For_Pickup_Body;"Ready For Pickup Email Body";$emailResource/email-readyForPickupBody.vm
 ;masdar_Email_Ready_For_Pickup_Subject;"Ready For Pickup Email Subject";$emailResource/email-readyForPickupSubject.vm
 ;masdar_Email_Quote_Buyer_Offer_Body;"Quote Buyer Offer Body";$emailResource/email-quoteBuyerOfferBody.vm
 ;masdar_Email_Quote_Buyer_Offer_Subject;"Quote Buyer Offer Subject";$emailResource/email-quoteBuyerOfferSubject.vm

;masdar_Email_Account_Manager_Quote_Buyer_Submission_Body;"Account Manager Quote Buyer Submission Email Body";$emailResource/email-accountManagerQuoteBuyerSubmissionBody.vm
;masdar_Email_Account_Manager_Quote_Buyer_Submission_Subject;"Account Manager Quote Buyer Submission Email Subject";$emailResource/email-accountManagerQuoteBuyerSubmissionSubject.vm
;masdar_Email_Account_Manager_Quote_Buyer_Cancellation_Body;"Account Manager Quote Buyer Cancellation Email Body";$emailResource/email-accountManagerQuoteBuyerCancellationBody.vm
;masdar_Email_Account_Manager_Quote_Buyer_Cancellation_Subject;"Account Manager Quote Buyer Cancellation Email Subject";$emailResource/email-accountManagerQuoteBuyerCancellationSubject.vm
;masdar_Email_Account_Manager_Quote_Buyer_Offer_Body;"Account Manager Quote Buyer Offer Body";$emailResource/email-accountManagerQuoteBuyerOfferBody.vm
;masdar_Email_Account_Manager_Quote_Buyer_Offer_Subject;"Account Manager Quote Buyer Offer Subject";$emailResource/email-accountManagerQuoteBuyerOfferSubject.vm
;masdar_Email_Order_Confirmation_Account_Manager_Body;"Order Confirmation Account Manager Email Body";$emailResource/email-orderConfirmationAccountManagerBody.vm
;masdar_Email_Order_Confirmation_Account_Manager_Subject;"Order Confirmation Account Manager Email Subject";$emailResource/email-orderConfirmationAccountManagerSubject.vm

;masdar_Email_Sadad_Bill_Id_Subject;"Sadad Bill Id Subject";$jarEmailResource/email-sadadBillIdSubject.vm
;masdar_Email_Sadad_Bill_Id_Body;"Sadad Bill Id Body";$jarEmailResource/email-sadadBillIdBody.vm
;masdar_Email_Payment_Notification_Subject;"Payment Notification Subject";$jarEmailResource/email-paymentNotificationSubject.vm
;masdar_Email_Payment_Notification_Body;"Payment Notification Body";$jarEmailResource/email-paymentNotificationBody.vm
;masdar_Email_Payment_Reminder_Subject;"Payment Reminder Subject";$jarEmailResource/email-paymentReminderSubject.vm
;masdar_Email_Payment_Reminder_Body;"Payment Reminder Body";$jarEmailResource/email-paymentReminderBody.vm

;masdar_Email_Sadad_Bill_Id_Account_Manager_Subject;"Sadad Bill Id Subject";$jarEmailResource/email-accountManagerSadadBillIdSubject.vm
;masdar_Email_Sadad_Bill_Id_Account_Manager_Body;"Sadad Bill Id Body";$jarEmailResource/email-accountManagerSadadBillIdBody.vm
;masdar_Email_Payment_Notification_Account_Manager_Subject;"Payment Notification Subject";$jarEmailResource/email-accountManagerPaymentNotificationSubject.vm
;masdar_Email_Payment_Notification_Account_Manager_Body;"Payment Notification Body";$jarEmailResource/email-accountManagerPaymentNotificationBody.vm
;masdar_Email_Payment_Reminder_Account_Manager_Subject;"Payment Reminder Subject";$jarEmailResource/email-accountManagerPaymentReminderSubject.vm
;masdar_Email_Payment_Reminder_Account_Manager_Body;"Payment Reminder Body";$jarEmailResource/email-accountManagerPaymentReminderBody.vm

;masdar_Email_Full_Payment_Subject;"Full Payment Subject";$jarEmailResource/email-fullPaymentSubject.vm
;masdar_Email_Full_Payment_Body;"Full Payment Body";$jarEmailResource/email-fullPaymentBody.vm
;masdar_Email_Partial_Payment_Subject;"Partial Payment Subject";$jarEmailResource/email-partialPaymentSubject.vm
;masdar_Email_Partial_Payment_Body;"Partial Payment Body";$jarEmailResource/email-partialPaymentBody.vm

;masdar_Email_Full_Payment_Account_Manager_Subject;"Full Payment Subject";$jarEmailResource/email-accountManagerFullPaymentSubject.vm
;masdar_Email_Full_Payment_Account_Manager_Body;"Full Payment Body";$jarEmailResource/email-accountManagerFullPaymentBody.vm
;masdar_Email_Partial_Payment_Account_Manager_Subject;"Partial Payment Subject";$jarEmailResource/email-accountManagerPartialPaymentSubject.vm
;masdar_Email_Partial_Payment_Account_Manager_Body;"Partial Payment Body";$jarEmailResource/email-accountManagerPartialPaymentBody.vm

;masdar_Email_Customer_Verification_Body;"Customer Verification Email Body";$emailResource/email-customerVerificationBody.vm
;masdar_Email_Customer_Verification_Subject;"Customer Verification Email Subject";$emailResource/email-customerVerificationSubject.vm
 
;masdar_Email_ConsignmentShipped_Body;" Consignment Shipped Email Body";$emailResource/email-consignmentShippedBody.vm
;masdar_Email_ConsignmentShipped_Subject;"Consignment Shipped Email Subject";$emailResource/email-consignmentShippedSubject.vm
;masdar_Email_ConsignmentDeliveryCompleted_Body;" Consignment Delivery Completed Email Body";$emailResource/email-consignmentDeliveryCompletedBody.vm
;masdar_Email_ConsignmentDeliveryCompleted_Subject;"Consignment Delivery Completed Email Subject";$emailResource/email-consignmentDeliveryCompletedSubject.vm
;masdar_Email_OrderCompleted_Body;" Order Completed Email Body";$emailResource/email-orderCompletedBody.vm
;masdar_Email_OrderCompleted_Subject;"Order Completed Email Subject";$emailResource/email-orderCompletedSubject.vm

# CMS components and Email velocity templates
UPDATE RendererTemplate;code[unique=true];description[lang=$lang];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 ;masdar_Email_Order_ApprovalRejection_Body;"Buyer Approval Rejection Email Body";$jarEmailResource/email-orderApprovalRejectionBody.vm
 ;masdar_Email_Order_ApprovalRejection_Subject;"Order Approval Rejection Email Subject";$jarEmailResource/email-orderApprovalRejectionSubject.vm
 ;masdar_Email_Order_PendingApproval_Body;"Order Pending Approval Email Body";$jarEmailResource/email-orderPendingApprovalBody.vm
 ;masdar_Email_Order_PendingApproval_Subject;"Order Pending Approval Email Subject";$jarEmailResource/email-orderPendingApprovalSubject.vm
 ;masdar_Email_Replenishment_Order_Confirmation_Body;"";$jarEmailResource/email-replenishmentOrderConfirmationBody.vm
 ;masdar_Email_Replenishment_Order_Confirmation_Subject;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderConfirmationSubject.vm
 ;masdar_Email_Replenishment_Order_Placed_Body;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderPlacedBody.vm
 ;masdar_Email_Replenishment_Order_Placed_Subject;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderPlacedSubject.vm

 # Email Pages
UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];fromEmail[lang=$lang];fromName[lang=$lang]
 ;;CustomerRegistrationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;DeliverySentEmail;"care@masdaronline.com";"Customer Services Team"
 ;;ForgottenPasswordEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderApprovalRejectionEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderCancelledEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderCollectionReminderEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderConfirmationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderMoveToCsEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderPartiallyCanceledEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderPartiallyRefundedEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderPendingApprovalEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderRefundEmail;"care@masdaronline.com";"Customer Services Team"
 ;;QuoteBuyerCancellationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;QuoteBuyerSubmissionEmail;"care@masdaronline.com";"Customer Services Team"
 ;;QuoteExpiredEmail;"care@masdaronline.com";"Customer Services Team"
 ;;QuoteToExpireSoonEmail;"care@masdaronline.com";"Customer Services Team"
 ;;ReadyForPickupEmail;"care@masdaronline.com";"Customer Services Team"
 ;;ReplenishmentOrderConfirmationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;ReplenishmentOrderPlacedEmail;"care@masdaronline.com";"Customer Services Team"
 ;;NotPickedUpConsignmentCanceledEmail;"care@masdaronline.com";"Customer Services Team"
 ;;QuoteBuyerOfferEmail;"care@masdaronline.com";"Customer Services Team"
 
 ;;SadadBillIdEmail;"care@masdaronline.com";"Customer Services Team"
 ;;PaymentNotificationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;PaymentReminderEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerSadadBillIdEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerPaymentNotificationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerPaymentReminderEmail;"care@masdaronline.com";"Customer Services Team"
 ;;FullPaymentEmail;"care@masdaronline.com";"Customer Services Team"
 ;;PartialPaymentEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerFullPaymentEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerPartialPaymentEmail;"care@masdaronline.com";"Customer Services Team"
 
 ;;AccountManagerQuoteBuyerOfferEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerQuoteBuyerCancellationEmail;"care@masdaronline.com";"Customer Services Team"
 ;;AccountManagerQuoteBuyerSubmissionEmail;"care@masdaronline.com";"Customer Services Team"
 ;;OrderConfirmationAccountManagerEmail;"care@masdaronline.com";"Customer Services Team"
 ;;CustomerVerificationEmail;"care@masdaronline.com";"Customer Services Team"
;;ConsignmentShippedEmail;"care@masdaronline.com";"Customer Services Team"
;;ConsignmentDeliveryCompletedEmail;"care@masdaronline.com";"Customer Services Team"
;;OrderCompletedEmail;"care@masdaronline.com";"Customer Services Team"


