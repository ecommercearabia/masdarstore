/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarcmscustomocc.jaxb.adapters.components;

import com.masdar.masdarcmscustomocc.data.ComponentWsDTO;
import com.masdar.masdarcmscustomocc.jaxb.adapters.components.ComponentAdapterUtil.ComponentAdaptedData;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * This adapter is used to convert {@link com.masdar.masdarcmscustomocc.data.ComponentWsDTO} into
 * {@link ComponentAdapterUtil.ComponentAdaptedData}
 */
public class ComponentWsDTOAdapter extends XmlAdapter<ComponentAdaptedData, ComponentWsDTO>
{
	@Override
	public ComponentAdaptedData marshal(final ComponentWsDTO componentDTO)
	{
		if (componentDTO == null)
		{
			return null;
		}

		return ComponentAdapterUtil.convert(componentDTO);
	}

	@Override
	public ComponentWsDTO unmarshal(final ComponentAdaptedData v) throws Exception
	{
		throw new UnsupportedOperationException();
	}
}
