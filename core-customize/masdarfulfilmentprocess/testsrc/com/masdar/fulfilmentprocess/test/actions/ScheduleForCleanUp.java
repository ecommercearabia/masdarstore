/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.fulfilmentprocess.test.actions;

import com.masdar.fulfilmentprocess.actions.order.ScheduleForCleanUpAction;


/**
 * Test counterpart for {@link ScheduleForCleanUpAction}
 */
public class ScheduleForCleanUp extends TestActionTemp
{
	//EMPTY
}
