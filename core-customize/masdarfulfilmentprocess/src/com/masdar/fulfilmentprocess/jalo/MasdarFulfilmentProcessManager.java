/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.fulfilmentprocess.constants.MasdarFulfilmentProcessConstants;

public class MasdarFulfilmentProcessManager extends GeneratedMasdarFulfilmentProcessManager
{
	public static final MasdarFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarFulfilmentProcessManager) em.getExtension(MasdarFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
