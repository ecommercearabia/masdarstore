/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpaymentfacade.constants;

/**
 * Global class for all Masdarpaymentfacade constants. You can add global constants for your extension into this class.
 */
public final class MasdarpaymentfacadeConstants extends GeneratedMasdarpaymentfacadeConstants
{
	public static final String EXTENSIONNAME = "masdarpaymentfacade";

	private MasdarpaymentfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
