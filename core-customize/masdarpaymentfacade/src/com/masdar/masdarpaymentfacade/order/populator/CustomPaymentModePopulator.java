/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarpaymentfacade.order.populator;

import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.masdar.masdarpaymentfacades.data.PaymentModeTypeData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomPaymentModePopulator implements Populator<PaymentModeModel, PaymentModeData>
{

	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final PaymentModeModel source, final PaymentModeData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		final PaymentModeTypeData paymentModeType = new PaymentModeTypeData();
		if (Objects.nonNull(source.getPaymentModeType()))
		{
			paymentModeType.setCode(source.getPaymentModeType().getCode());
			paymentModeType.setName(source.getPaymentModeType().name());
		}
		target.setPaymentModeType(paymentModeType);
		target.setPaymentProviderNeeded(source.isPaymentProviderNeeded());
		if (source.getIcon() != null)
		{
			target.setLogo(imageConverter.convert(source.getIcon()));
		}
	}

}
