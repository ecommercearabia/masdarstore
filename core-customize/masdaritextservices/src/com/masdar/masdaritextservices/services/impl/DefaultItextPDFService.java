/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaritextservices.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.filespec.PdfFileSpec;
import com.itextpdf.layout.Document;
import com.itextpdf.licensing.base.LicenseKey;
import com.masdar.masdaritextservices.beans.EmbeddedFileInfo;
import com.masdar.masdaritextservices.services.ItextPDFService;


/**
 *
 */
public class DefaultItextPDFService implements ItextPDFService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultItextPDFService.class);

	static
	{


	}

	//	@Override
	public byte[] createPDFA(final String body, final String embededFileName, final byte[] embededXML)
	{
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		final PdfDocument pdfDocument = new PdfDocument(new PdfWriter(byteArrayOutputStream));

		final PdfFileSpec spec = PdfFileSpec.createEmbeddedFileSpec(pdfDocument, embededXML, embededFileName, embededFileName, null,
				null, null);
		pdfDocument.addFileAttachment("embededFileName", spec);

		final Document document = HtmlConverter.convertToDocument(body, pdfDocument, null);
		document.close();

		return byteArrayOutputStream.toByteArray().clone();
	}

	@Override
	public byte[] createPDFA(final String body, final List<EmbeddedFileInfo> embeddedFiles)
	{
		final String formatBase = "/masdaritextservices/itext-licenses/%s";

		final List<String> asList = Arrays.asList(
				String.format(formatBase, "b46e3e22aefa3eb4c8ef7857b7238f664015799ca7b0350c240be8eaf90598c4.json"),
				String.format(formatBase, "2f5ab14ec16d1f21c5d16b471cd2f34597fec242b5224c117fe304112b4ce016.json"),
				String.format(formatBase, "9f96cfc2137deda7fd540c749c14cd6d5c97a06f12c48d8b1086ecfbceddc447.json"));


		for (final String file : asList)
		{
			try (final InputStream resourceAsStream = DefaultItextPDFService.class.getResourceAsStream(file))
			{
				LicenseKey.loadLicenseFile(resourceAsStream);
			}
			catch (final NoSuchMethodError e)
			{
				LOG.error(e.getMessage());
			}
			catch (final Exception e)
			{
				LOG.error(e.getMessage());
			}
		}
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		final ConverterProperties props = new ConverterProperties();

		final PdfDocument pdfDocument = new PdfDocument(new PdfWriter(byteArrayOutputStream));
		pdfDocument.setDefaultPageSize(new PageSize(950, 800));
		final Document document = HtmlConverter.convertToDocument(body, pdfDocument, props);
		attachFilesToPdf(embeddedFiles, pdfDocument);

		document.setMargins(0, 0, 0, 0);
		document.close();
		final byte[] extarctBytesFromByteArray = extarctBytesFromByteArray(byteArrayOutputStream);
		pdfDocument.close();

		return extarctBytesFromByteArray;
	}

	private byte[] extarctBytesFromByteArray(final ByteArrayOutputStream byteArrayOutputStream)
	{
		final byte[] byteArray = byteArrayOutputStream.toByteArray().clone();
		try
		{
			byteArrayOutputStream.close();
		}
		catch (final IOException e)
		{
			LOG.error(e.getMessage());
			return null;
		}
		return byteArray;
	}

	private void attachFilesToPdf(final List<EmbeddedFileInfo> embeddedFiles, final PdfDocument pdfDocument)
	{
		if (embeddedFiles == null || embeddedFiles.size() < 1)
		{
			return;
		}

		for (final EmbeddedFileInfo fileInfo : embeddedFiles)
		{
			final PdfFileSpec spec = PdfFileSpec.createEmbeddedFileSpec(pdfDocument, fileInfo.getData(), fileInfo.getDescription(),
					fileInfo.getName(), fileInfo.getMimeType(), null, null);

			pdfDocument.addFileAttachment(fileInfo.getName(), spec);
		}
	}
}
