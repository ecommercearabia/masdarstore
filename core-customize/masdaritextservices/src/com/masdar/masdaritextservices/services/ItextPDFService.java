/**
 *
 */
package com.masdar.masdaritextservices.services;

import java.util.List;

import com.masdar.masdaritextservices.beans.EmbeddedFileInfo;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface ItextPDFService
{
	byte[] createPDFA(final String body, List<EmbeddedFileInfo> embeddedFiles);
}
