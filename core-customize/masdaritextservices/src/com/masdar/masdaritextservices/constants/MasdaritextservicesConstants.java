/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdaritextservices.constants;

/**
 * Global class for all Masdaritextservices constants. You can add global constants for your extension into this class.
 */
public final class MasdaritextservicesConstants extends GeneratedMasdaritextservicesConstants
{
	public static final String EXTENSIONNAME = "masdaritextservices";

	private MasdaritextservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
