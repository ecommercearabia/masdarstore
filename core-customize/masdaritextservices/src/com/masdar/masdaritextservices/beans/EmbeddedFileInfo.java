/**
 *
 */
package com.masdar.masdaritextservices.beans;

import com.itextpdf.kernel.pdf.PdfName;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class EmbeddedFileInfo
{

	public enum FileType
	{
		ApplicationXml
	}

	private String name;
	private String description;
	private byte[] data;
	private PdfName mimeType;



	/**
	 * @return the mimeType
	 */
	public PdfName getMimeType()
	{
		return mimeType;
	}


	/**
	 * @param mimeType
	 *           the mimeType to set
	 */
	public void setMimeType(final FileType mimeType)
	{
		this.mimeType = FileType.ApplicationXml.equals(mimeType) ? PdfName.ApplicationXml : null;
	}


	/**
	 *
	 */
	public EmbeddedFileInfo()
	{
		super();
	}


	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *           the description to set
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * @return the data
	 */
	public byte[] getData()
	{
		return data;
	}

	/**
	 * @param data
	 *           the data to set
	 */
	public void setData(final byte[] data)
	{
		this.data = data;
	}



}
