/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.exception;

import com.masdar.masdarscpiservice.exception.type.SCPIExceptionType;


/**
 *
 */
public class SCPIException extends Exception
{

	private final SCPIExceptionType type;

	/**
	 *
	 */
	public SCPIException(final SCPIExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public SCPIExceptionType getType()
	{
		return type;
	}



}
