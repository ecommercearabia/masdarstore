/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.exception.type;

/**
 *
 */
public enum SCPIExceptionType
{
	CLIENT_ERROR("Client Error"), SERVER_ERROR("Server Error");

	private String message;

	/**
	 *
	 */
	private SCPIExceptionType(final String message)
	{
		this.message = message;
	}
}
