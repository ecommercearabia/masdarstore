/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.dao.impl;

import com.masdar.masdarscpiservice.dao.SCPIProviderDao;
import com.masdar.masdarscpiservice.model.SCPISendConsignmentServiceProviderModel;


/**
 * @author Husam
 *
 */
public class DefaultSendConsignementSCPIProviderDao extends DefaultSCPIProviderDao implements SCPIProviderDao
{
	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultSendConsignementSCPIProviderDao()
	{
		super(SCPISendConsignmentServiceProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return SCPISendConsignmentServiceProviderModel._TYPECODE;
	}

}
