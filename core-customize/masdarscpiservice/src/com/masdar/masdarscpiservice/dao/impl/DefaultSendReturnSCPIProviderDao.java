/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.dao.impl;

import com.masdar.masdarscpiservice.dao.SCPIProviderDao;
import com.masdar.masdarscpiservice.model.SCPIReturnOrderServiceProviderModel;


/**
 * @author Husam
 *
 */
public class DefaultSendReturnSCPIProviderDao extends DefaultSCPIProviderDao implements SCPIProviderDao
{
	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultSendReturnSCPIProviderDao()
	{
		super(SCPIReturnOrderServiceProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return SCPIReturnOrderServiceProviderModel._TYPECODE;
	}

}
