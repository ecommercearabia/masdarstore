/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.dao.impl;

import com.masdar.masdarscpiservice.dao.SCPIProviderDao;
import com.masdar.masdarscpiservice.model.SCPISendOrderServiceProviderModel;


/**
 * @author Husam
 *
 */
public class DefaultSendOrderSCPIProviderDao extends DefaultSCPIProviderDao implements SCPIProviderDao
{
	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultSendOrderSCPIProviderDao()
	{
		super(SCPISendOrderServiceProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return SCPISendOrderServiceProviderModel._TYPECODE;
	}

}
