/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.dao.impl;

import com.masdar.masdarscpiservice.dao.SCPIProviderDao;
import com.masdar.masdarscpiservice.model.SCPISendCartInventoryServiceProviderModel;


/**
 * @author Husam
 *
 */
public class DefaultSendCartInventorySCPIServiceDao extends DefaultSCPIProviderDao implements SCPIProviderDao
{
	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultSendCartInventorySCPIServiceDao()
	{
		super(SCPISendCartInventoryServiceProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return SCPISendCartInventoryServiceProviderModel._TYPECODE;
	}

}
