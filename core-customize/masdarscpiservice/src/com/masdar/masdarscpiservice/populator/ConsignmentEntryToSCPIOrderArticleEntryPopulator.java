/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.populator;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.joda.time.DateTime;

import com.masdar.masdarscpiservice.beans.SCPIOrderArticleEntry;


/**
 *
 */
public class ConsignmentEntryToSCPIOrderArticleEntryPopulator implements Populator<ConsignmentEntryModel, SCPIOrderArticleEntry>
{
	private static final String DATE_FORMAT = "yyyyMMdd";

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Override
	public void populate(final ConsignmentEntryModel consignmentEntryModel, final SCPIOrderArticleEntry target)
			throws ConversionException
	{

		final ConsignmentModel consignment = consignmentEntryModel.getConsignment();
		final ConsignmentEntryData consignmentEntry = consignmentEntryConverter.convert(consignmentEntryModel);


		final OrderModel orderModel = (OrderModel) consignment.getOrder();

		final DateTime dtOrg = new DateTime(orderModel.getDate());
		final DateTime dtPlusN = dtOrg.plusDays(1);

		final OrderEntryData it = consignmentEntry.getOrderEntry();

		target.setCustomerPurchaseOrderNumber(orderModel.getOrderNumber());
		target.setInvoiceCreationDate(dtOrg.toString(DATE_FORMAT));
		target.setRequestedDeliveryDate(dtPlusN.toString(DATE_FORMAT));
		if (it.getProduct().getCode().contains("_"))
		{
			target.setArticleNumber(it.getProduct().getCode().substring(0, it.getProduct().getCode().indexOf('_')));
			target.setUom(it.getProduct().getCode().substring(it.getProduct().getCode().indexOf('_') + 1));
		}
		else
		{
			target.setArticleNumber(it.getProduct().getCode());
			target.setUom(it.getProduct().getCode());
		}
		target.setCurrency(orderModel.getCurrency().getIsocode());
		target.setSalesDocumentItem(it.getEntryNumber() == null ? "" : it.getEntryNumber().toString());

		final int lineItemNumber = (it.getEntryNumber() + 1) * 10;
		target.setItemNumber(String.valueOf(lineItemNumber));

		target.setTargetQuantity(consignmentEntry.getQuantity() == null ? "" : consignmentEntry.getQuantity().toString());



		target.setDiscount(String.valueOf(consignmentEntry.getNetDiscountValue()));

		target.setIncTax(String.valueOf(consignmentEntry.getTotalTaxPriceValue()));

		target.setTotalPrice(String.valueOf(consignmentEntry.getNetTotalPriceValue()));

		target.setTotalPriceWithTax(String.valueOf(consignmentEntry.getTotalPriceValue()));
		target.setBasePriceWithTax(consignmentEntry.getNetBasePriceWithTax() == null ? ""
				: String.valueOf(consignmentEntry.getNetBasePriceWithTax().getValue().doubleValue()));

		target.setBasePrice(String.valueOf(consignmentEntry.getNetBasePriceValue()));
		target.setWarehouseId(consignment.getWarehouse().getCode());

	}

}
