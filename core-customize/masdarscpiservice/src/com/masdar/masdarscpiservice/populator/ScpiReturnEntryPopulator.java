/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import javax.annotation.Resource;

import org.joda.time.DateTime;

import com.masdar.masdarscpiservice.beans.SCPIReturnEntryData;


/**
 *
 */
public class ScpiReturnEntryPopulator implements Populator<ReturnEntryModel, SCPIReturnEntryData>
{

	private static final String DATE_FORMAT = "yyyyMMdd";

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;

	@Override
	public void populate(final ReturnEntryModel source, final SCPIReturnEntryData target) throws ConversionException
	{
		final OrderEntryModel orderEntry = (OrderEntryModel) source.getOrderEntry();

		final int lineItemNumber = (orderEntry.getEntryNumber() + 1) * 10;
		final DateTime dtOrg = new DateTime(orderEntry.getOrder().getDate());

		target.setItemSerialNo(String.valueOf(lineItemNumber));
		target.setRequestedDeliveryDate(dtOrg.toString(DATE_FORMAT));

		double discount = 0.0d;
		if (orderEntry.getDiscountValues() != null)
		{

			discount = orderEntry.getDiscountValues().stream().mapToDouble(DiscountValue::getAppliedValue).sum();
			discount /= orderEntry.getQuantity();
		}

		target.setCurrency(orderEntry.getOrder().getCurrency().getIsocode());
		target.setDiscountLineItem(discount == 0.0d ? "0" : "-" + String.valueOf(discount));
		target.setPriceLineItem("-" + orderEntry.getBasePrice().toString());
		target.setArticleNumber(orderEntry.getProduct().getCode().substring(0, orderEntry.getProduct().getCode().indexOf('_')));
		target.setTargetQuantityUom(
				orderEntry.getProduct().getCode().substring(orderEntry.getProduct().getCode().indexOf('_') + 1));

		final WarehouseModel warehouse = getReturnWarehouse(source.getReturnRequest());
		target.setSite(warehouse.getCode());
		target.setInvoiceNnumberRefrence(((OrderModel) source.getOrderEntry().getOrder()).getOrderNumber());
		target.setTargetQuantity(source.getExpectedQuantity() == null ? "" : source.getExpectedQuantity().toString());


	}

	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}

	protected RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}

}
