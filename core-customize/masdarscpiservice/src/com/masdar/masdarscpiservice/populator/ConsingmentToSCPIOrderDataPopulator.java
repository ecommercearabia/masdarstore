/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.populator;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.constants.GeneratedCommerceServicesConstants.Enumerations.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.joda.time.DateTime;

import com.masdar.masdarscpiservice.beans.SCPIOrderArticleEntry;
import com.masdar.masdarscpiservice.beans.SCPIOrderData;
import com.masdar.masdarscpiservice.model.SCPISendOrderServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPIServiceProviderModel;
import com.masdar.masdarscpiservice.service.SCPIProviderService;


/**
 *
 */
public class ConsingmentToSCPIOrderDataPopulator implements Populator<ConsignmentModel, SCPIOrderData>
{

	private static final String DATE_FORMAT = "yyyyMMdd";

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Resource(name = "ordermanagementConsignmentConverter")
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;


	@Resource(name = "consignmentEntryToSCPIOrderArticleEntryConverter")
	private Converter<ConsignmentEntryModel, SCPIOrderArticleEntry> consignmentEntryToSCPIOrderArticleEntryConverter;


	@Resource(name = "scpiProviderService")
	private SCPIProviderService scpiProviderService;

	private String extractData(final Object obj)
	{
		return obj != null ? obj.toString() : "";
	}

	@Override
	public void populate(final ConsignmentModel consignment, final SCPIOrderData target) throws ConversionException
	{
		final OrderModel orderModel = (OrderModel) consignment.getOrder();
		final String siteChannel = orderModel.getSite() == null ? null : orderModel.getSite().getChannel().toString();

		final OrderData order = orderConverter.convert(orderModel);

		final SCPISendOrderServiceProviderModel sendOrderProvider = getProvider(orderModel);

		target.setSalesDocumentType(extractData(sendOrderProvider.getSalesDocumentType()));
		target.setSalesOrganization(extractData(sendOrderProvider.getSalesOrganization()));
		target.setDistributionChannel(extractData(sendOrderProvider.getDistributionChannel()));
		target.setDivision(extractData(sendOrderProvider.getDivision()));
		target.setConditionType(extractData(sendOrderProvider.getConditionType()));
		target.setConditionRate(extractData(sendOrderProvider.getConditionrate()));
		target.setEmployeeID(
				consignment.getPrintDeliveryNoteEmployee() == null ? "" : consignment.getPrintDeliveryNoteEmployee().getUid());

		final DateTime dtOrg = new DateTime(orderModel.getDate());
		final DateTime dtPlusN = dtOrg.plusDays(1);

		target.setInvoiceNumber(orderModel.getOrderNumber());
		target.setOriginalConsginmentID(consignment.getCode());
		target.setWarehouseId(consignment.getWarehouse().getCode());

		target.setOrderStatus(extractData(order.getStatus()));
		target.setPaymentType(order.getPaymentType() != null ? order.getPaymentType().getCode() : null);
		target.setPatmentStatus(orderModel.getPaymentStatus() != null ? orderModel.getPaymentStatus().getCode() : null);
		target.setSdadBillID(extractData(orderModel.getSadadBillId()));

		target.setConsginmentID(consignment.getInvoiceNumber());
		target.setEsalInvoice(orderModel.getPaymentB2BInvoiceId());
		target.setInvoiceCreationDate(dtOrg.toString(DATE_FORMAT));
		target.setRequestedDeliveryDate(dtPlusN.toString(DATE_FORMAT));
		target.setCustomerPurchaseOrderNumber(order.getOrderNumber());
		if (orderModel.getUnit() != null)
		{
			final String unitUid = extractData(orderModel.getUnit().getUid());
			target.setPartnerFunctionSoldTo(unitUid);
			target.setCustomerNumber(orderModel.getUnit().isCashCustomer() ? "0000006999" : unitUid);
			target.setAccountManager(orderModel.getUnit().getAccountManager() == null
					|| orderModel.getUnit().getAccountManager().getAccountManagerID() == null ? ""
							: orderModel.getUnit().getAccountManager().getAccountManagerID());
			target.setPartnerFunctionShipTo(target.getAccountManager());
		}
		target.setPaymentCost(orderModel.getPaymentCost() == null ? "" : String.valueOf(orderModel.getPaymentCost()));

		populateDeliveryCost(consignment, target, orderModel, order);
		final BigDecimal totalOrderPrice = order.getNetAmount().getValue();

		target.setTotalPrice(String.valueOf(roundPrice(totalOrderPrice, 2)));

		target.setInclTax(order.getTotalTax() == null ? "" : String.valueOf(roundPrice(order.getTotalTax().getValue(), 2)));
		target.setDiscountsIncluded(
				order.getTotalDiscounts() == null ? "" : String.valueOf(order.getTotalDiscounts().getValue().doubleValue()));
		target.setCurrencyIsoKey(orderModel.getCurrency() == null ? "" : orderModel.getCurrency().getIsocode());


		final ConsignmentData consignmentData = consignmentConverter.convert(consignment);
		target.setTotalPriceWithTax(String.valueOf(consignmentData.getTotalPriceWithTaxValue()));


		final UserModel currentUser = getUserService().getCurrentUser();
		if (Strings.isBlank(target.getAccountManager()) && SiteChannel.B2C.equals(siteChannel)
				&& currentUser instanceof EmployeeModel)
		{
			target.setAccountManager(((EmployeeModel) currentUser).getUid());
		}


		final List<SCPIOrderArticleEntry> scpiOrderArticleEntries = consignmentEntryToSCPIOrderArticleEntryConverter
				.convertAll(consignment.getConsignmentEntries().stream().filter(e -> e.getQuantity() != null && e.getQuantity() > 0)
						.collect(Collectors.toList()))
				.stream().filter(e -> Strings.isNotBlank(e.getTargetQuantity())).collect(Collectors.toList());
		target.getEntries().addAll(scpiOrderArticleEntries);

	}

	private void populateDeliveryCost(final ConsignmentModel consignment, final SCPIOrderData target, final OrderModel orderModel,
			final OrderData order)
	{
		if (consignment.getCode().equals(orderModel.getDeliveryCostAppliedOnConsignment()))
		{
			target.setDeliveryCost(order.getDeliveryCost() == null ? "" : order.getDeliveryCost().getValue().toString());
			return;
		}

		target.setDeliveryCost(String.valueOf(0.0d));

	}

	/**
	 *
	 */
	private SCPISendOrderServiceProviderModel getProvider(final OrderModel order)
	{
		final Optional<SCPIServiceProviderModel> scpiProvider = getScpiProviderService().getSCPIProvider(order.getStore(),
				SCPISendOrderServiceProviderModel.class);

		return (SCPISendOrderServiceProviderModel) scpiProvider.get();
	}

	public double roundPrice(final BigDecimal price, final int digits)
	{
		final double digitsWeight1 = Math.pow(10, digits + 1.0d);
		final double rounded = Math.round(price.doubleValue() * digitsWeight1) / digitsWeight1;
		final double digitsWeight2 = Math.pow(10, digits);
		return Math.round(rounded * digitsWeight2) / digitsWeight2;
	}

	public SCPIProviderService getScpiProviderService()
	{
		return scpiProviderService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}



}
