/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.springframework.util.Assert;

import com.masdar.masdarscpiservice.beans.SCPIReturnData;
import com.masdar.masdarscpiservice.beans.SCPIReturnEntryData;


/**
 *
 */
public class ScpiReturnPopulator implements Populator<ReturnRequestModel, SCPIReturnData>
{

	@Resource(name = "scpiReturnEntryConvertor")
	Converter<ReturnEntryModel, SCPIReturnEntryData> entryConvertor;

	private static final String DATE_FORMAT = "yyyyMMdd";

	@Override
	public void populate(final ReturnRequestModel source, final SCPIReturnData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");



		final DateTime dtOrg = new DateTime(source.getOrder().getDate());

		target.setCustomerNumber(source.getOrder().getUnit().getUid() == null ? "" : source.getOrder().getUnit().getUid());
		target.setAccountManager(source.getOrder().getUnit().getAccountManager() == null ? ""
				: source.getOrder().getUnit().getAccountManager().getAccountManagerID());
		target.setInvoiceNumberRefrence(source.getOrder().getOrderNumber());
		target.setRequestedDeliveryDate(dtOrg.toString(DATE_FORMAT));
		target.setEntries(entryConvertor.convertAll(source.getReturnEntries()));
		target.setRmaNumber("R" + source.getRMA());
	}

}
