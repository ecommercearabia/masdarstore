/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;

import com.google.gson.annotations.Expose;


/**
 * @author Husam Dababneh
 */
public class SCPIReturnEntryData implements Serializable
{
	@Expose(serialize = true)
	private String itemSerialNo;
	@Expose(serialize = true)
	private String requestedDeliveryDate;
	@Expose(serialize = true)
	private String currency;
	@Expose(serialize = true)
	private String discountLineItem;
	@Expose(serialize = true)
	private String priceLineItem;
	@Expose(serialize = true)
	private String articleNumber;
	@Expose(serialize = true)
	private String site;
	@Expose(serialize = true)
	private String invoiceNnumberRefrence;
	@Expose(serialize = true)
	private String targetQuantity;
	@Expose(serialize = true)
	private String targetQuantityUom;

	/**
	 * @return the itemSerialNo
	 */
	public String getItemSerialNo()
	{
		return itemSerialNo;
	}

	/**
	 * @param itemSerialNo
	 *           the itemSerialNo to set
	 */
	public void setItemSerialNo(final String itemSerialNo)
	{
		this.itemSerialNo = itemSerialNo;
	}

	/**
	 * @return the requestedDeliveryDate
	 */
	public String getRequestedDeliveryDate()
	{
		return requestedDeliveryDate;
	}

	/**
	 * @param requestedDeliveryDate
	 *           the requestedDeliveryDate to set
	 */
	public void setRequestedDeliveryDate(final String requestedDeliveryDate)
	{
		this.requestedDeliveryDate = requestedDeliveryDate;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}

	/**
	 * @param currency
	 *           the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * @return the discountLineItem
	 */
	public String getDiscountLineItem()
	{
		return discountLineItem;
	}

	/**
	 * @param discountLineItem
	 *           the discountLineItem to set
	 */
	public void setDiscountLineItem(final String discountLineItem)
	{
		this.discountLineItem = discountLineItem;
	}

	/**
	 * @return the priceLineItem
	 */
	public String getPriceLineItem()
	{
		return priceLineItem;
	}

	/**
	 * @param priceLineItem
	 *           the priceLineItem to set
	 */
	public void setPriceLineItem(final String priceLineItem)
	{
		this.priceLineItem = priceLineItem;
	}

	/**
	 * @return the articleNumber
	 */
	public String getArticleNumber()
	{
		return articleNumber;
	}

	/**
	 * @param articleNumber
	 *           the articleNumber to set
	 */
	public void setArticleNumber(final String articleNumber)
	{
		this.articleNumber = articleNumber;
	}

	/**
	 * @return the site
	 */
	public String getSite()
	{
		return site;
	}

	/**
	 * @param site
	 *           the site to set
	 */
	public void setSite(final String site)
	{
		this.site = site;
	}

	/**
	 * @return the invoiceNnumberRefrence
	 */
	public String getInvoiceNnumberRefrence()
	{
		return invoiceNnumberRefrence;
	}

	/**
	 * @param invoiceNnumberRefrence
	 *           the invoiceNnumberRefrence to set
	 */
	public void setInvoiceNnumberRefrence(final String invoiceNnumberRefrence)
	{
		this.invoiceNnumberRefrence = invoiceNnumberRefrence;
	}

	/**
	 * @return the targetQuantity
	 */
	public String getTargetQuantity()
	{
		return targetQuantity;
	}

	/**
	 * @param targetQuantity
	 *           the targetQuantity to set
	 */
	public void setTargetQuantity(final String targetQuantity)
	{
		this.targetQuantity = targetQuantity;
	}

	/**
	 * @return the targetQuantityUom
	 */
	public String getTargetQuantityUom()
	{
		return targetQuantityUom;
	}

	/**
	 * @param targetQuantityUom
	 *           the targetQuantityUom to set
	 */
	public void setTargetQuantityUom(final String targetQuantityUom)
	{
		this.targetQuantityUom = targetQuantityUom;
	}




}
