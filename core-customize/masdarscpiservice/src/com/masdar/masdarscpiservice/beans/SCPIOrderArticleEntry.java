/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;

import com.google.gson.annotations.Expose;


/**
 *
 */
public class SCPIOrderArticleEntry implements Serializable
{
	@Expose(serialize = true)
	private String salesDocumentItem;
	@Expose(serialize = true)
	private String articleNumber;
	@Expose(serialize = true)
	private String targetQuantity;
	@Expose(serialize = true)
	private String basePriceWithTax;
	@Expose(serialize = true)
	private String basePrice;
	@Expose(serialize = true)
	private String totalPriceWithTax;
	@Expose(serialize = true)
	private String totalPrice;
	@Expose(serialize = true)
	private String discount;
	@Expose(serialize = true)
	private String incTax;
	@Expose(serialize = true)
	private String uom;
	@Expose(serialize = true)
	private String currency;
	@Expose(serialize = true)
	private String customerPurchaseOrderNumber;
	@Expose(serialize = true)
	private String requestedDeliveryDate;
	@Expose(serialize = true)
	private String invoiceCreationDate;
	@Expose(serialize = true)
	private String itemNumber;
	@Expose(serialize = true)
	private String warehouseId;




	/**
	 * @return the warehouseID
	 */
	public String getWarehouseId()
	{
		return warehouseId;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseId(final String warehouseID)
	{
		this.warehouseId = warehouseID;
	}

	/**
	 * @return the invoiceCreationDate
	 */
	public String getInvoiceCreationDate()
	{
		return invoiceCreationDate;
	}

	/**
	 * @param invoiceCreationDate
	 *           the invoiceCreationDate to set
	 */
	public void setInvoiceCreationDate(final String invoiceCreationDate)
	{
		this.invoiceCreationDate = invoiceCreationDate;
	}

	/**
	 * @return the itemNumber
	 */
	public String getItemNumber()
	{
		return itemNumber;
	}

	/**
	 * @param itemNumber
	 *           the itemNumber to set
	 */
	public void setItemNumber(final String itemNumber)
	{
		this.itemNumber = itemNumber;
	}

	/**
	 * @return the customerPurchaseOrderNumber
	 */
	public String getCustomerPurchaseOrderNumber()
	{
		return customerPurchaseOrderNumber;
	}

	/**
	 * @param customerPurchaseOrderNumber
	 *           the customerPurchaseOrderNumber to set
	 */
	public void setCustomerPurchaseOrderNumber(final String customerPurchaseOrderNumber)
	{
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
	}

	/**
	 * @return the requestedDeliveryDate
	 */
	public String getRequestedDeliveryDate()
	{
		return requestedDeliveryDate;
	}

	/**
	 * @param requestedDeliveryDate
	 *           the requestedDeliveryDate to set
	 */
	public void setRequestedDeliveryDate(final String requestedDeliveryDate)
	{
		this.requestedDeliveryDate = requestedDeliveryDate;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}

	/**
	 * @param currency
	 *           the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * @return the uom
	 */
	public String getUom()
	{
		return uom;
	}

	/**
	 * @param uom
	 *           the uom to set
	 */
	public void setUom(final String uom)
	{
		this.uom = uom;
	}

	/**
	 * @return the salesDocumentItem
	 */
	public String getSalesDocumentItem()
	{
		return salesDocumentItem;
	}

	/**
	 * @param salesDocumentItem
	 *           the salesDocumentItem to set
	 */
	public void setSalesDocumentItem(final String salesDocumentItem)
	{
		this.salesDocumentItem = salesDocumentItem;
	}

	/**
	 * @return the articleNumber
	 */
	public String getArticleNumber()
	{
		return articleNumber;
	}

	/**
	 * @param articleNumber
	 *           the articleNumber to set
	 */
	public void setArticleNumber(final String articleNumber)
	{
		this.articleNumber = articleNumber;
	}

	/**
	 * @return the targetQuantity
	 */
	public String getTargetQuantity()
	{
		return targetQuantity;
	}

	/**
	 * @param targetQuantity
	 *           the targetQuantity to set
	 */
	public void setTargetQuantity(final String targetQuantity)
	{
		this.targetQuantity = targetQuantity;
	}

	/**
	 * @return the basePriceWithTax
	 */
	public String getBasePriceWithTax()
	{
		return basePriceWithTax;
	}

	/**
	 * @param basePriceWithTax
	 *           the basePriceWithTax to set
	 */
	public void setBasePriceWithTax(final String basePriceWithTax)
	{
		this.basePriceWithTax = basePriceWithTax;
	}

	/**
	 * @return the basePrice
	 */
	public String getBasePrice()
	{
		return basePrice;
	}

	/**
	 * @param basePrice
	 *           the basePrice to set
	 */
	public void setBasePrice(final String basePrice)
	{
		this.basePrice = basePrice;
	}

	/**
	 * @return the totalPriceWithTax
	 */
	public String getTotalPriceWithTax()
	{
		return totalPriceWithTax;
	}

	/**
	 * @param totalPriceWithTax
	 *           the totalPriceWithTax to set
	 */
	public void setTotalPriceWithTax(final String totalPriceWithTax)
	{
		this.totalPriceWithTax = totalPriceWithTax;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice()
	{
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *           the totalPrice to set
	 */
	public void setTotalPrice(final String totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the discount
	 */
	public String getDiscount()
	{
		return discount;
	}

	/**
	 * @param discount
	 *           the discount to set
	 */
	public void setDiscount(final String discount)
	{
		this.discount = discount;
	}

	/**
	 * @return the incTax
	 */
	public String getIncTax()
	{
		return incTax;
	}

	/**
	 * @param incTax
	 *           the incTax to set
	 */
	public void setIncTax(final String incTax)
	{
		this.incTax = incTax;
	}



}
