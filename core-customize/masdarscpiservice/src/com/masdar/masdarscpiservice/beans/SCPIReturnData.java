/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;


/**
 * @author Husam Dababneh
 */
public class SCPIReturnData implements Serializable
{

	@Expose(serialize = true)
	private String rmaNumber;
	@Expose(serialize = true)
	private String requestedDeliveryDate;
	@Expose(serialize = true)
	private String invoiceNumberRefrence;
	@Expose(serialize = true)
	private String customerNumber;
	@Expose(serialize = true)
	private String accountManager;
	@Expose(serialize = true)
	List<SCPIReturnEntryData> entries;




	/**
	 * @param entries
	 *           the entries to set
	 */
	public void setEntries(final List<SCPIReturnEntryData> entries)
	{
		this.entries = entries;
	}

	/**
	 *
	 */
	public SCPIReturnData()
	{
		this.entries = new ArrayList<>();
	}

	/**
	 * @return the rmaNumber
	 */
	public String getRmaNumber()
	{
		return rmaNumber;
	}

	/**
	 * @param rmaNumber
	 *           the rmaNumber to set
	 */
	public void setRmaNumber(final String rmaNumber)
	{
		this.rmaNumber = rmaNumber;
	}

	/**
	 * @return the requestedDeliveryDate
	 */
	public String getRequestedDeliveryDate()
	{
		return requestedDeliveryDate;
	}

	/**
	 * @param requestedDeliveryDate
	 *           the requestedDeliveryDate to set
	 */
	public void setRequestedDeliveryDate(final String requestedDeliveryDate)
	{
		this.requestedDeliveryDate = requestedDeliveryDate;
	}

	/**
	 * @return the invoiceNumberRefrence
	 */
	public String getInvoiceNumberRefrence()
	{
		return invoiceNumberRefrence;
	}

	/**
	 * @param invoiceNumberRefrence
	 *           the invoiceNumberRefrence to set
	 */
	public void setInvoiceNumberRefrence(final String invoiceNumberRefrence)
	{
		this.invoiceNumberRefrence = invoiceNumberRefrence;
	}

	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber()
	{
		return customerNumber;
	}

	/**
	 * @param customerNumber
	 *           the customerNumber to set
	 */
	public void setCustomerNumber(final String customerNumber)
	{
		this.customerNumber = customerNumber;
	}

	/**
	 * @return the accountManager
	 */
	public String getAccountManager()
	{
		return accountManager;
	}

	/**
	 * @param accountManager
	 *           the accountManager to set
	 */
	public void setAccountManager(final String accountManager)
	{
		this.accountManager = accountManager;
	}

	/**
	 * @return the entries
	 */
	public List<SCPIReturnEntryData> getEntries()
	{
		return entries;
	}

}
