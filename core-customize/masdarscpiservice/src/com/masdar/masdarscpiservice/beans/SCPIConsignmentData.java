/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;


/**
 * @author Husam Dababneh
 */
public class SCPIConsignmentData implements Serializable
{

	@Expose(serialize = true)
	private final List<SCPIConsignmentEntryData> entries;



	/**
	 *
	 */
	public SCPIConsignmentData()
	{
		super();
		entries = new ArrayList<SCPIConsignmentEntryData>();
	}



	/**
	 * @return the entries
	 */
	public List<SCPIConsignmentEntryData> getEntries()
	{
		return entries;
	}




}
