/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;


/**
 * @author Husam Dababneh
 */
public class SCPIOrderData implements Serializable
{
	@Expose(serialize = true)
	private String invoiceCreationDate;
	@Expose(serialize = true)
	private String requestedDeliveryDate;
	@Expose(serialize = true)
	private String customerPurchaseOrderNumber;
	@Expose(serialize = true)
	private String partnerFunctionSoldTo;// (Sold to Party)
	@Expose(serialize = true)
	private String customerNumber;
	@Expose(serialize = true)
	private String partnerFunctionShipTo;// (Ship to Party)
	@Expose(serialize = true)
	private String paymentCost;
	@Expose(serialize = true)
	private String deliveryCost;
	@Expose(serialize = true)
	private String totalPrice;
	@Expose(serialize = true)
	private String inclTax;
	@Expose(serialize = true)
	private String discountsIncluded;
	@Expose(serialize = true)
	private String currencyIsoKey;
	@Expose(serialize = true)
	private String accountManager;

	@Expose(serialize = true)
	private String originalConsginmentID;
	@Expose(serialize = true)
	private String consginmentID;
	@Expose(serialize = true)
	private String invoiceNumber;
	@Expose(serialize = true)
	private String warehouseId;


	@Expose(serialize = true)
	private String esalInvoice;
	@Expose(serialize = true)
	private String totalPriceWithTax;

	@Expose(serialize = true)
	private String orderStatus;
	@Expose(serialize = true)
	private String paymentType;
	@Expose(serialize = true)
	private String patmentStatus;
	@Expose(serialize = true)
	private String sdadBillID;

	@Expose(serialize = true)
	private String employeeID;


	@Expose(serialize = true)
	private final List<SCPIOrderArticleEntry> entries;

	@Expose(serialize = false)
	private String salesDocumentType;
	@Expose(serialize = false)
	private String salesOrganization;
	@Expose(serialize = false)
	private String distributionChannel;
	@Expose(serialize = false)
	private String division;
	@Expose(serialize = false)
	private String conditionType;
	@Expose(serialize = false)
	private String conditionRate;






	/**
	 * @return the employeeID
	 */
	public String getEmployeeID()
	{
		return employeeID;
	}


	/**
	 * @param employeeID
	 *           the employeeID to set
	 */
	public void setEmployeeID(final String employeeID)
	{
		this.employeeID = employeeID;
	}


	/**
	 * @return the originalconsginmentID
	 */
	public String getOriginalConsginmentID()
	{
		return originalConsginmentID;
	}


	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus()
	{
		return orderStatus;
	}


	/**
	 * @param orderStatus
	 *           the orderStatus to set
	 */
	public void setOrderStatus(final String orderStatus)
	{
		this.orderStatus = orderStatus;
	}


	/**
	 * @return the paymentType
	 */
	public String getPaymentType()
	{
		return paymentType;
	}


	/**
	 * @param paymentType
	 *           the paymentType to set
	 */
	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}


	/**
	 * @return the patmentStatus
	 */
	public String getPatmentStatus()
	{
		return patmentStatus;
	}


	/**
	 * @param patmentStatus
	 *           the patmentStatus to set
	 */
	public void setPatmentStatus(final String patmentStatus)
	{
		this.patmentStatus = patmentStatus;
	}


	/**
	 * @return the sdadBillID
	 */
	public String getSdadBillID()
	{
		return sdadBillID;
	}


	/**
	 * @param sdadBillID
	 *           the sdadBillID to set
	 */
	public void setSdadBillID(final String sdadBillID)
	{
		this.sdadBillID = sdadBillID;
	}


	/**
	 * @return the totalPriceWithTax
	 */
	public String getTotalPriceWithTax()
	{
		return totalPriceWithTax;
	}


	/**
	 * @param totalPriceWithTax
	 *           the totalPriceWithTax to set
	 */
	public void setTotalPriceWithTax(final String totalPriceWithTax)
	{
		this.totalPriceWithTax = totalPriceWithTax;
	}


	/**
	 * @param originalconsginmentID
	 *           the originalconsginmentID to set
	 */
	public void setOriginalConsginmentID(final String originalconsginmentID)
	{
		this.originalConsginmentID = originalconsginmentID;
	}


	/**
	 * @return the consginmentID
	 */
	public String getConsginmentID()
	{
		return consginmentID;
	}


	/**
	 * @param consginmentID
	 *           the consginmentID to set
	 */
	public void setConsginmentID(final String consginmentID)
	{
		this.consginmentID = consginmentID;
	}


	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}


	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}


	/**
	 * @return the warehouseId
	 */
	public String getWarehouseId()
	{
		return warehouseId;
	}


	/**
	 * @param warehouseId
	 *           the warehouseId to set
	 */
	public void setWarehouseId(final String warehouseId)
	{
		this.warehouseId = warehouseId;
	}


	/**
	 * @return the invoiceCreationDate
	 */
	public String getInvoiceCreationDate()
	{
		return invoiceCreationDate;
	}


	/**
	 * @param invoiceCreationDate
	 *           the invoiceCreationDate to set
	 */
	public void setInvoiceCreationDate(final String invoiceCreationDate)
	{
		this.invoiceCreationDate = invoiceCreationDate;
	}




	/**
	 *
	 */
	public SCPIOrderData()
	{
		super();
		entries = new ArrayList<>();
	}




	/**
	 * @return the esalInvoice
	 */
	public String getEsalInvoice()
	{
		return esalInvoice;
	}


	/**
	 * @param esalInvoice
	 *           the esalInvoice to set
	 */
	public void setEsalInvoice(final String esalInvoice)
	{
		this.esalInvoice = esalInvoice;
	}


	/**
	 * @return the accountManager
	 */
	public String getAccountManager()
	{
		return accountManager;
	}




	/**
	 * @param accountManager
	 *           the accountManager to set
	 */
	public void setAccountManager(final String accountManager)
	{
		this.accountManager = accountManager;
	}




	/**
	 * @return the requestedDeliveryDate
	 */
	public String getRequestedDeliveryDate()
	{
		return requestedDeliveryDate;
	}



	/**
	 * @param requestedDeliveryDate
	 *           the requestedDeliveryDate to set
	 */
	public void setRequestedDeliveryDate(final String requestedDeliveryDate)
	{
		this.requestedDeliveryDate = requestedDeliveryDate;
	}



	/**
	 * @return the customerPurchaseOrderNumber
	 */
	public String getCustomerPurchaseOrderNumber()
	{
		return customerPurchaseOrderNumber;
	}



	/**
	 * @param customerPurchaseOrderNumber
	 *           the customerPurchaseOrderNumber to set
	 */
	public void setCustomerPurchaseOrderNumber(final String customerPurchaseOrderNumber)
	{
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
	}



	/**
	 * @return the partnerFunctionSoldTo
	 */
	public String getPartnerFunctionSoldTo()
	{
		return partnerFunctionSoldTo;
	}



	/**
	 * @param partnerFunctionSoldTo
	 *           the partnerFunctionSoldTo to set
	 */
	public void setPartnerFunctionSoldTo(final String partnerFunctionSoldTo)
	{
		this.partnerFunctionSoldTo = partnerFunctionSoldTo;
	}



	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber()
	{
		return customerNumber;
	}



	/**
	 * @param customerNumber
	 *           the customerNumber to set
	 */
	public void setCustomerNumber(final String customerNumber)
	{
		this.customerNumber = customerNumber;
	}



	/**
	 * @return the partnerFunctionShipTo
	 */
	public String getPartnerFunctionShipTo()
	{
		return partnerFunctionShipTo;
	}



	/**
	 * @param partnerFunctionShipTo
	 *           the partnerFunctionShipTo to set
	 */
	public void setPartnerFunctionShipTo(final String partnerFunctionShipTo)
	{
		this.partnerFunctionShipTo = partnerFunctionShipTo;
	}



	/**
	 * @return the paymentCost
	 */
	public String getPaymentCost()
	{
		return paymentCost;
	}



	/**
	 * @param paymentCost
	 *           the paymentCost to set
	 */
	public void setPaymentCost(final String paymentCost)
	{
		this.paymentCost = paymentCost;
	}



	/**
	 * @return the deliveryCost
	 */
	public String getDeliveryCost()
	{
		return deliveryCost;
	}



	/**
	 * @param deliveryCost
	 *           the deliveryCost to set
	 */
	public void setDeliveryCost(final String deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}



	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice()
	{
		return totalPrice;
	}



	/**
	 * @param totalPrice
	 *           the totalPrice to set
	 */
	public void setTotalPrice(final String totalPrice)
	{
		this.totalPrice = totalPrice;
	}



	/**
	 * @return the inclTax
	 */
	public String getInclTax()
	{
		return inclTax;
	}



	/**
	 * @param inclTax
	 *           the inclTax to set
	 */
	public void setInclTax(final String inclTax)
	{
		this.inclTax = inclTax;
	}



	/**
	 * @return the discountsIncluded
	 */
	public String getDiscountsIncluded()
	{
		return discountsIncluded;
	}



	/**
	 * @param discountsIncluded
	 *           the discountsIncluded to set
	 */
	public void setDiscountsIncluded(final String discountsIncluded)
	{
		this.discountsIncluded = discountsIncluded;
	}



	/**
	 * @return the currencyIsoKey
	 */
	public String getCurrencyIsoKey()
	{
		return currencyIsoKey;
	}



	/**
	 * @param currencyIsoKey
	 *           the currencyIsoKey to set
	 */
	public void setCurrencyIsoKey(final String currencyIsoKey)
	{
		this.currencyIsoKey = currencyIsoKey;
	}




	/**
	 * @return the salesDocumentType
	 */
	public String getSalesDocumentType()
	{
		return salesDocumentType;
	}



	/**
	 * @param salesDocumentType
	 *           the salesDocumentType to set
	 */
	public void setSalesDocumentType(final String salesDocumentType)
	{
		this.salesDocumentType = salesDocumentType;
	}



	/**
	 * @return the salesOrganization
	 */
	public String getSalesOrganization()
	{
		return salesOrganization;
	}



	/**
	 * @param salesOrganization
	 *           the salesOrganization to set
	 */
	public void setSalesOrganization(final String salesOrganization)
	{
		this.salesOrganization = salesOrganization;
	}



	/**
	 * @return the distributionChannel
	 */
	public String getDistributionChannel()
	{
		return distributionChannel;
	}



	/**
	 * @param distributionChannel
	 *           the distributionChannel to set
	 */
	public void setDistributionChannel(final String distributionChannel)
	{
		this.distributionChannel = distributionChannel;
	}



	/**
	 * @return the division
	 */
	public String getDivision()
	{
		return division;
	}



	/**
	 * @param division
	 *           the division to set
	 */
	public void setDivision(final String division)
	{
		this.division = division;
	}



	/**
	 * @return the conditionType
	 */
	public String getConditionType()
	{
		return conditionType;
	}



	/**
	 * @param conditionType
	 *           the conditionType to set
	 */
	public void setConditionType(final String conditionType)
	{
		this.conditionType = conditionType;
	}



	/**
	 * @return the conditionRate
	 */
	public String getConditionRate()
	{
		return conditionRate;
	}



	/**
	 * @param conditionRate
	 *           the conditionRate to set
	 */
	public void setConditionRate(final String conditionRate)
	{
		this.conditionRate = conditionRate;
	}



	/**
	 * @return the entries
	 */
	public List<SCPIOrderArticleEntry> getEntries()
	{
		return entries;
	}



}
