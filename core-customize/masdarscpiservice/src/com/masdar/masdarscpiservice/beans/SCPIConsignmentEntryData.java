/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;

import com.google.gson.annotations.Expose;


/**
 *
 */
public class SCPIConsignmentEntryData implements Serializable
{
	@Expose(serialize = true)
	private String invoiceCreationDate;
	@Expose(serialize = true)
	private String expecteddeliverydate;
	@Expose(serialize = true)
	private String salesordernumber;
	@Expose(serialize = true)
	private String itemNumber;
	@Expose(serialize = true)
	private String article;
	@Expose(serialize = true)
	private String deliveryQty;
	@Expose(serialize = true)
	private String site;
	@Expose(serialize = true)
	private String consginmentID;
	@Expose(serialize = true)
	private String invoiceNumber;
	@Expose(serialize = true)
	private String uom;

	@Expose(serialize = false)
	private String shippingpoint;
	@Expose(serialize = false)
	private String storageLocation;




	/**
	 * @return the invoiceCreationDate
	 */
	public String getInvoiceCreationDate()
	{
		return invoiceCreationDate;
	}

	/**
	 * @param invoiceCreationDate
	 *           the invoiceCreationDate to set
	 */
	public void setInvoiceCreationDate(final String invoiceCreationDate)
	{
		this.invoiceCreationDate = invoiceCreationDate;
	}

	/**
	 * @return the consginmentID
	 */
	public String getConsginmentID()
	{
		return consginmentID;
	}

	/**
	 * @param consginmentID
	 *           the consginmentID to set
	 */
	public void setConsginmentID(final String consginmentID)
	{
		this.consginmentID = consginmentID;
	}

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the uom
	 */
	public String getUom()
	{
		return uom;
	}

	/**
	 * @param uom
	 *           the uom to set
	 */
	public void setUom(final String uom)
	{
		this.uom = uom;
	}

	/**
	 * @return the shippingpoint
	 */
	public String getShippingpoint()
	{
		return shippingpoint;
	}

	/**
	 * @param shippingpoint
	 *           the shippingpoint to set
	 */
	public void setShippingpoint(final String shippingpoint)
	{
		this.shippingpoint = shippingpoint;
	}

	/**
	 * @return the storageLocation
	 */
	public String getStorageLocation()
	{
		return storageLocation;
	}

	/**
	 * @param storageLocation
	 *           the storageLocation to set
	 */
	public void setStorageLocation(final String storageLocation)
	{
		this.storageLocation = storageLocation;
	}

	/**
	 *
	 */
	public SCPIConsignmentEntryData()
	{
		super();
	}

	/**
	 * @return the expecteddeliverydate
	 */
	public String getExpecteddeliverydate()
	{
		return expecteddeliverydate;
	}

	/**
	 * @param expecteddeliverydate
	 *           the expecteddeliverydate to set
	 */
	public void setExpecteddeliverydate(final String expecteddeliverydate)
	{
		this.expecteddeliverydate = expecteddeliverydate;
	}

	/**
	 * @return the salesordernumber
	 */
	public String getSalesordernumber()
	{
		return salesordernumber;
	}

	/**
	 * @param salesordernumber
	 *           the salesordernumber to set
	 */
	public void setSalesordernumber(final String salesordernumber)
	{
		this.salesordernumber = salesordernumber;
	}

	/**
	 * @return the itemNumber
	 */
	public String getItemNumber()
	{
		return itemNumber;
	}

	/**
	 * @param itemNumber
	 *           the itemNumber to set
	 */
	public void setItemNumber(final String itemNumber)
	{
		this.itemNumber = itemNumber;
	}

	/**
	 * @return the article
	 */
	public String getArticle()
	{
		return article;
	}

	/**
	 * @param article
	 *           the article to set
	 */
	public void setArticle(final String article)
	{
		this.article = article;
	}

	/**
	 * @return the deliveryQty
	 */
	public String getDeliveryQty()
	{
		return deliveryQty;
	}

	/**
	 * @param deliveryQty
	 *           the deliveryQty to set
	 */
	public void setDeliveryQty(final String deliveryQty)
	{
		this.deliveryQty = deliveryQty;
	}

	/**
	 * @return the site
	 */
	public String getSite()
	{
		return site;
	}

	/**
	 * @param site
	 *           the site to set
	 */
	public void setSite(final String site)
	{
		this.site = site;
	}



}
