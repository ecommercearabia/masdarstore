/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;

import com.google.gson.annotations.Expose;


/**
 *
 */
public class SCPIUpdateInventoryEntry implements Serializable
{
	@Expose(serialize = true)
	private String product;
	@Expose(serialize = true)
	private String warehouse;



	/**
	 *
	 */
	public SCPIUpdateInventoryEntry(final String product, final String warehouse)
	{
		super();
		this.product = product;
		this.warehouse = warehouse;
	}

	/**
	 * @return the product
	 */
	public String getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *           the product to set
	 */
	public void setProduct(final String product)
	{
		this.product = product;
	}

	/**
	 * @return the warehouse
	 */
	public String getWarehouse()
	{
		return warehouse;
	}

	/**
	 * @param warehouse
	 *           the warehouse to set
	 */
	public void setWarehouse(final String warehouse)
	{
		this.warehouse = warehouse;
	}


}
