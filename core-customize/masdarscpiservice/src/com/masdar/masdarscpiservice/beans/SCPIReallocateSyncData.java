/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;


/**
 * @author Husam Dababneh
 */
public class SCPIReallocateSyncData implements Serializable
{

	@Expose(serialize = true)
	private final List<SCPIReallocateInventoryEntry> entries;



	/**
	 *
	 */
	public SCPIReallocateSyncData()
	{
		this.entries = new ArrayList<>();
	}



	/**
	 * @return the entries
	 */
	public List<SCPIReallocateInventoryEntry> getEntries()
	{
		return entries;
	}


}
