/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.exception.type.SCPIExceptionType;
import com.masdar.masdarscpiservice.service.SCPIServiceClient;
import com.masdar.masdarwebserviceapi.util.WebServiceApiUtil;
import com.masdar.masdarwebserviceapi.util.WebServiceUnirestUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;


/**
 *
 */
public class DefaultSCPIServiceClient implements SCPIServiceClient
{

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSCPIServiceClient.class);

	private static final String SERVER_ERROR = "Server Error: ";
	private static final String AUTHORIZATION = "Authorization";


	@Override
	public String updateSCPIInventory(final String baseURL, final String username, final String password, final String data,
			final long timeout) throws SCPIException
	{
		return sendDataTOSCPI(baseURL, username, password, data, timeout);
	}


	@Override
	public String sendSCPIConsignment(final String baseURL, final String username, final String password, final String data)
			throws SCPIException
	{
		return sendDataTOSCPI(baseURL, username, password, data, 0);
	}


	@Override
	public String sendSCPIOrder(final String baseURL, final String username, final String password, final String data)
			throws SCPIException
	{
		return sendDataTOSCPI(baseURL, username, password, data, 0);
	}


	@Override
	public String sendSCPIApproveReturn(final String baseURL, final String username, final String password, final String data)
			throws SCPIException
	{
		return sendDataTOSCPI(baseURL, username, password, data, 0);
	}

	private String sendDataTOSCPI(final String baseURL, final String username, final String password, final String data,
			final long timeout) throws SCPIException
	{
		final Map<String, String> headers = createRequestHeaders(username, password);
		HttpResponse<?> response = null;
		try
		{
			response = WebServiceUnirestUtil.httpPostJSON(baseURL, data, headers, timeout);
			if (!WebServiceUnirestUtil.is200Status(response))
			{
				throw new SCPIException(SCPIExceptionType.SERVER_ERROR,
						"Return Status: " + response.getStatusText() + "--- Body: " + response.getBody().toString());
			}
		}
		catch (final UnirestException e)
		{

			throw new SCPIException(SCPIExceptionType.SERVER_ERROR,
					Strings.isBlank(e.getMessage()) ? response.getStatusText() : SERVER_ERROR + e.getMessage());
		}
		return response.getBody().toString();
	}

	private Map<String, String> createRequestHeaders(final String username, final String password)
	{
		final Map<String, String> headers = new HashMap<>();
		headers.put(AUTHORIZATION, WebServiceApiUtil.getBasicAuthHeader(username, password));
		return headers;
	}


}
