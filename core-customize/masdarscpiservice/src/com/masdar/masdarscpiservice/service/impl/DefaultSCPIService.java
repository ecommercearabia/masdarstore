/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.service.impl;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masdar.masdarscpiservice.beans.SCPIConsignmentData;
import com.masdar.masdarscpiservice.beans.SCPIConsignmentEntryData;
import com.masdar.masdarscpiservice.beans.SCPIOrderData;
import com.masdar.masdarscpiservice.beans.SCPIReallocateInventoryEntry;
import com.masdar.masdarscpiservice.beans.SCPIReallocateSyncData;
import com.masdar.masdarscpiservice.beans.SCPIReturnData;
import com.masdar.masdarscpiservice.beans.SCPIUpdateInventoryData;
import com.masdar.masdarscpiservice.beans.SCPIUpdateInventoryEntry;
import com.masdar.masdarscpiservice.exception.SCPIException;
import com.masdar.masdarscpiservice.exception.type.SCPIExceptionType;
import com.masdar.masdarscpiservice.model.ReallocateActionHistoryEntryModel;
import com.masdar.masdarscpiservice.model.SCPIActionHistoryEntryModel;
import com.masdar.masdarscpiservice.model.SCPIReturnOrderServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPISendCartInventoryServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPISendConsignmentServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPISendInventoryReallocateServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPISendOrderServiceProviderModel;
import com.masdar.masdarscpiservice.model.SCPIServiceProviderModel;
import com.masdar.masdarscpiservice.service.SCPIProviderService;
import com.masdar.masdarscpiservice.service.SCPIService;
import com.masdar.masdarscpiservice.service.SCPIServiceClient;


/**
 * @author Husam Dababneh
 */
public class DefaultSCPIService implements SCPIService
{
	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSCPIService.class);
	final Gson GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

	private static final String SENDORDER = "SENDORDER";
	private static final String SERVER_ERROR = "SCPI Server Error: ";
	private static final String SENDCONSIGNMENT = "SENDCONSIGNMENT";
	private static final String CLIENT_ERROR_SCPI_PROVIDER_IS_NULL = "Client Error: SCPI Provider is null";
	private static final String CONSIGNEMNT_ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL = "Consignemnt order must be assosiated with a basestore model";
	private static final String CONSIGNMENT_MUST_NOT_BE_EMPTY = "Consignment Must not be null";
	private static final String RETURN_MUST_NOT_BE_EMPTY = "ReturnrequestModel Must not be null";
	private static final String ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL = "Order must be assosiated with a basestore model";
	private static final String ORDER_MUST_NOT_BE_EMPTY = "Order Must not be null";
	private static final String DATE_FORMAT = "yyyyMMdd";

	private static final String CART_MUST_NOT_BE_EMPTY = "Cart Must Not Be Null";
	private static final String CART_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL = "Cart must be assosiated with a basestore model";


	@Resource(name = "scpiProviderService")
	private SCPIProviderService scpiProviderService;

	@Resource(name = "warehouseService")
	private WarehouseService warehouseService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Resource(name = "ordermanagementConsignmentConverter")
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;


	@Resource(name = "scpiReturnConvertor")
	private Converter<ReturnRequestModel, SCPIReturnData> returnConverter;


	@Resource(name = "consingmentToSCPIOrderDataConverter")
	private Converter<ConsignmentModel, SCPIOrderData> consingmentToSCPIOrderDataConverter;


	@Resource(name = "scpiServiceClient")
	private SCPIServiceClient scpiServiceClient;


	@Override
	public void sendSCPIOrder(final ConsignmentModel consignment) throws SCPIException
	{
		Preconditions.checkArgument(consignment != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final AbstractOrderModel order = consignment.getOrder();
		Preconditions.checkArgument(order != null, ORDER_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = order.getStore();
		Preconditions.checkArgument(store != null, ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final var sendOrderProvider = getSCPIProvider(store, SCPISendOrderServiceProviderModel.class);

		final SCPIOrderData data = consingmentToSCPIOrderDataConverter.convert(consignment);

		final String requestBody = GSON.toJson(data);
		try
		{
			final String response = scpiServiceClient.sendSCPIOrder(sendOrderProvider.getBaseUrl(), sendOrderProvider.getUsername(),
					sendOrderProvider.getPassword(), GSON.toJson(data));
			saveActionInHistory(order, SENDORDER, requestBody, response);
		}
		catch (final SCPIException e)
		{
			saveActionInHistory(order, SENDORDER, requestBody, String.valueOf(e.getMessage()));
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}
	}



	@Override
	public void sendSCPIConsignment(final ConsignmentModel consignment) throws SCPIException
	{
		Preconditions.checkArgument(consignment != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = consignment.getOrder().getStore();
		Preconditions.checkArgument(store != null, CONSIGNEMNT_ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);


		final SCPISendConsignmentServiceProviderModel scpiConsignmentProvider = getSCPIProvider(store,
				SCPISendConsignmentServiceProviderModel.class);

		final SCPIConsignmentData data = prepareConsignmentData(consignment, scpiConsignmentProvider);

		final String requestBody = GSON.toJson(data);

		try
		{
			final String response = scpiServiceClient.sendSCPIConsignment(scpiConsignmentProvider.getBaseUrl(),
					scpiConsignmentProvider.getUsername(), scpiConsignmentProvider.getPassword(), requestBody);

			saveActionInHistory(consignment.getOrder(), SENDCONSIGNMENT, requestBody, response);
		}
		catch (final SCPIException e)
		{
			saveActionInHistory(consignment.getOrder(), SENDCONSIGNMENT, requestBody, String.valueOf(e.getMessage()));
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}

	}



	@Override
	public void sendSCPIApproveReturn(final ReturnRequestModel returnRequest) throws SCPIException
	{

		final String APPROVERETURN = "APPROVERETURN";
		Preconditions.checkArgument(returnRequest != null, RETURN_MUST_NOT_BE_EMPTY);
		final AbstractOrderModel order = returnRequest.getOrder();
		Preconditions.checkArgument(order != null, ORDER_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = order.getStore();
		Preconditions.checkArgument(store != null, ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);


		final SCPIReturnOrderServiceProviderModel approveReturnProvider = getSCPIProvider(store,
				SCPIReturnOrderServiceProviderModel.class);

		final SCPIReturnData data = returnConverter.convert(returnRequest);

		final String requestBody = GSON.toJson(data);
		try
		{
			final String response = scpiServiceClient.sendSCPIApproveReturn(approveReturnProvider.getBaseUrl(),
					approveReturnProvider.getUsername(), approveReturnProvider.getPassword(), requestBody);
			saveActionInHistory(order, APPROVERETURN, requestBody, response);
		}
		catch (final SCPIException e)
		{
			saveActionInHistory(order, APPROVERETURN, requestBody, String.valueOf(e.getMessage()));
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}

	}

	@Override
	public void updateSCPIInventory(final ConsignmentModel consignmentModel) throws SCPIException
	{

		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = consignmentModel.getOrder().getStore();
		Preconditions.checkArgument(store != null, CONSIGNEMNT_ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final SCPISendInventoryReallocateServiceProviderModel reallocateProvider = getSCPIProvider(store,
				SCPISendInventoryReallocateServiceProviderModel.class);


		final String requestBody = store.isEnabeInventoryMultipleRequests() ? prepareUpdateInventoryData(consignmentModel)
				: prepareUpdateInventoryBulkData(consignmentModel);

		try
		{
			final String response = scpiServiceClient.sendSCPIOrder(reallocateProvider.getBaseUrl(),
					reallocateProvider.getUsername(), reallocateProvider.getPassword(), requestBody);
			saveActionInHistory(consignmentModel, requestBody, response);
		}
		catch (final SCPIException e)
		{
			saveActionInHistory(consignmentModel, requestBody, String.valueOf(e.getMessage()));
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}

	}

	@Override
	public void updateSCPIInventory(final CartModel cart) throws SCPIException
	{
		Preconditions.checkArgument(cart != null, CART_MUST_NOT_BE_EMPTY);
		Preconditions.checkArgument(cart.getStore() != null, CART_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);


		final SCPISendCartInventoryServiceProviderModel provider = getSCPIProvider(cart.getStore(),
				SCPISendCartInventoryServiceProviderModel.class);

		final SCPIUpdateInventoryData data = prepareCartUpdateInventoryData(cart);

		final String requestBody = GSON.toJson(data.getEntries());
		try
		{
			scpiServiceClient.updateSCPIInventory(provider.getBaseUrl(), provider.getUsername(), provider.getPassword(), requestBody,
					provider.isEnableTimeout() ? provider.getTimeout() : 0l);
		}
		catch (final SCPIException e)
		{

			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}
	}


	@Override
	public void updateInventoryForProduct(final ProductModel product, final BaseStoreModel baseStore) throws SCPIException
	{

		Preconditions.checkArgument(product != null, "product is null");
		Preconditions.checkArgument(baseStore != null, "BaseStore is null");

		final SCPISendCartInventoryServiceProviderModel provider = getSCPIProvider(baseStore,
				SCPISendCartInventoryServiceProviderModel.class);

		final SCPIUpdateInventoryData data = prepareUpdateInventoryForProductData(product, baseStore);

		final String requestBody = GSON.toJson(data.getEntries());
		try
		{
			scpiServiceClient.updateSCPIInventory(provider.getBaseUrl(), provider.getUsername(), provider.getPassword(), requestBody,
					provider.isEnableTimeout() ? provider.getTimeout() : 0l);
		}
		catch (final SCPIException e)
		{

			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, SERVER_ERROR + e.getMessage());
		}
	}


	@Override
	public void updateInventoryForProductByCurrentStore(final ProductModel product) throws SCPIException
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		updateInventoryForProduct(product, currentBaseStore);
	}




	/**
	 *
	 */
	private SCPIUpdateInventoryData prepareUpdateInventoryForProductData(final ProductModel product,
			final BaseStoreModel baseStore)
	{

		final SCPIUpdateInventoryData data = new SCPIUpdateInventoryData();

		final Set<String> warehouses = getCodesForNotDefaultWarehousesForStore(baseStore);

		for (final String warehouse : warehouses)
		{
			data.getEntries().add(new SCPIUpdateInventoryEntry(product.getCode(), warehouse));
		}


		return data;
	}

	/**
	 *
	 */
	private String prepareUpdateInventoryData(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = consignmentModel.getOrder().getStore();
		Preconditions.checkArgument(store != null, CONSIGNEMNT_ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final Set<String> warehouses = getCodesForNotDefaultWarehousesForStore(store);

		final Set<String> products = getConsignmentsProductsCodes(consignmentModel);

		final Map<String, SCPIReallocateSyncData> data = new HashMap<>();

		if (warehouses != null && products != null)
		{
			for (final String product : products)
			{
				for (final String warehouse : warehouses)
				{
					if (!data.containsKey(product))
					{
						data.put(product, new SCPIReallocateSyncData());
					}

					data.get(product).getEntries().add(new SCPIReallocateInventoryEntry(product, warehouse));
				}
			}
		}

		return GSON.toJson(data);
	}

	/**
	 *
	 */
	private String prepareUpdateInventoryBulkData(final ConsignmentModel consignmentModel)
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = consignmentModel.getOrder().getStore();
		Preconditions.checkArgument(store != null, CONSIGNEMNT_ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final Set<String> warehouses = getCodesForNotDefaultWarehousesForStore(store);

		final Set<String> products = getConsignmentsProductsCodes(consignmentModel);

		final SCPIReallocateSyncData data = new SCPIReallocateSyncData();

		if (warehouses != null && products != null)
		{
			for (final String product : products)
			{
				for (final String warehouse : warehouses)
				{
					data.getEntries().add(new SCPIReallocateInventoryEntry(product, warehouse));
				}
			}
		}

		return GSON.toJson(data.getEntries());
	}

	/**
	 *
	 */
	private Set<String> getConsignmentsProductsCodes(final ConsignmentModel consignmentModel)
	{
		if (consignmentModel == null || consignmentModel.getConsignmentEntries() == null
				|| consignmentModel.getConsignmentEntries().isEmpty())
		{
			return Collections.emptySet();
		}
		final Set<String> data = new HashSet<>(consignmentModel.getConsignmentEntries().size());

		for (final ConsignmentEntryModel it : consignmentModel.getConsignmentEntries())
		{
			if (it.getOrderEntry() != null && it.getOrderEntry().getProduct() != null)
			{
				data.add(it.getOrderEntry().getProduct().getCode());
			}
		}

		return data;
	}

	/**
	 *
	 */
	private Set<String> getCodesForNotDefaultWarehousesForStore(final BaseStoreModel baseStore)
	{
		if (baseStore.getWarehouses() == null)
		{
			return Collections.emptySet();
		}
		return baseStore.getWarehouses().stream().filter(w -> !w.getDefault()).map(WarehouseModel::getCode)
				.collect(Collectors.toSet());
	}

	private void saveActionInHistory(final ConsignmentModel consignmentModel, final String request, final String response)
	{
		final List<ReallocateActionHistoryEntryModel> history = new ArrayList<>();
		if (consignmentModel.getReallocationActionHistory() != null)
		{
			history.addAll(consignmentModel.getReallocationActionHistory());
		}

		final ReallocateActionHistoryEntryModel historyEntry = modelService.create(ReallocateActionHistoryEntryModel.class);
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		history.add(historyEntry);


		consignmentModel.setReallocationActionHistory(history);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
	}

	/**
	 * @param string2
	 *
	 */
	private void saveActionInHistory(final AbstractOrderModel order, final String type, final String request,
			final String response)
	{
		final List<SCPIActionHistoryEntryModel> history = new ArrayList<>();
		if (order.getScpiActionHistory() != null)
		{
			history.addAll(order.getScpiActionHistory());
		}

		final SCPIActionHistoryEntryModel historyEntry = createSCPIActionHistory(order, type, request, response);
		history.add(historyEntry);
		order.setScpiActionHistory(history);
		modelService.save(order);
		modelService.refresh(order);
	}



	private SCPIActionHistoryEntryModel createSCPIActionHistory(final AbstractOrderModel order, final String type,
			final String request, final String response)
	{
		final SCPIActionHistoryEntryModel historyEntry = modelService.create(SCPIActionHistoryEntryModel.class);
		historyEntry.setType(type);
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		historyEntry.setOrder(Arrays.asList(order));
		modelService.save(historyEntry);
		modelService.refresh(historyEntry);
		return historyEntry;
	}



	public double roundPrice(final BigDecimal price, final int digits)
	{
		final double digitsWeight1 = Math.pow(10, digits + 1.0d);
		final double rounded = Math.round(price.doubleValue() * digitsWeight1) / digitsWeight1;
		final double digitsWeight2 = Math.pow(10, digits);
		return Math.round(rounded * digitsWeight2) / digitsWeight2;
	}



	/**
	 *
	 */
	private SCPIUpdateInventoryData prepareCartUpdateInventoryData(final CartModel cart)
	{
		final SCPIUpdateInventoryData result = new SCPIUpdateInventoryData();
		final Set<String> warehouses = getCodesForNotDefaultWarehousesForStore(cart.getStore());

		final Set<String> cartProducts = cart.getEntries().stream().map(e -> e.getProduct().getCode()).collect(Collectors.toSet());

		if (CollectionUtils.isEmpty(warehouses))
		{
			throw new IllegalArgumentException(String.format("Could Not Find Warehouses for Store [%s]", cart.getStore().getUid()));
		}
		if (CollectionUtils.isEmpty(cartProducts))
		{
			throw new IllegalArgumentException(String.format("Cart [%s] doesn't have any products", cart.getCode()));
		}

		for (final String product : cartProducts)
		{
			for (final String warehouse : warehouses)
			{
				if (!result.getEntries().contains(result))
				{
					result.getEntries().add(new SCPIUpdateInventoryEntry(product, warehouse));
				}
			}
		}
		return result;
	}


	/**
	 *
	 */
	private SCPIConsignmentData prepareConsignmentData(final ConsignmentModel consignment,
			final SCPISendConsignmentServiceProviderModel scpiConsignmentProvider)
	{
		final SCPIConsignmentData data = new SCPIConsignmentData();

		for (final ConsignmentEntryModel it : consignment.getConsignmentEntries())
		{
			if (it.getQuantity() == null || it.getQuantity().doubleValue() == 0)
			{
				continue;
			}
			final SCPIConsignmentEntryData entry = new SCPIConsignmentEntryData();
			final DateTime dtOrg = new DateTime(consignment.getShippingDate());
			final DateTime dtPlusN = dtOrg.plusDays(1);

			entry.setInvoiceCreationDate(dtOrg.toString(DATE_FORMAT));
			entry.setExpecteddeliverydate(dtPlusN.toString(DATE_FORMAT));
			entry.setSalesordernumber(
					it.getOrderEntry().getOrder().getCode() == null ? "" : it.getOrderEntry().getOrder().getCode());
			entry.setItemNumber(it.getOrderEntry().getEntryNumber() == null ? "" : it.getOrderEntry().getEntryNumber().toString());
			entry.setDeliveryQty(it.getQuantity() == null ? "" : it.getQuantity().toString());
			entry.setSite(consignment.getWarehouse().getCode());

			entry.setStorageLocation(scpiConsignmentProvider.getStorageLocation());
			entry.setShippingpoint(scpiConsignmentProvider.getShippingPoint());

			if (it.getOrderEntry().getProduct().getCode().contains("_"))
			{
				entry.setArticle(
						it.getOrderEntry().getProduct().getCode().substring(0, it.getOrderEntry().getProduct().getCode().indexOf('_')));
				entry.setUom(it.getOrderEntry().getProduct().getCode()
						.substring(it.getOrderEntry().getProduct().getCode().indexOf('_') + 1));
			}
			else
			{
				entry.setArticle(it.getOrderEntry().getProduct().getCode());
				entry.setUom(it.getOrderEntry().getProduct().getCode());
			}
			final int lineItemNumber = (it.getOrderEntry().getEntryNumber() + 1) * 10;
			entry.setItemNumber(String.valueOf(lineItemNumber));
			entry.setConsginmentID(it.getConsignment().getCode());
			if (it.getConsignment().getOrder() != null)
			{
				final OrderModel order = (OrderModel) it.getConsignment().getOrder();
				entry.setInvoiceNumber(order.getOrderNumber());
			}

			data.getEntries().add(entry);
		}
		return data;
	}

	private <T> T getSCPIProvider(final BaseStoreModel store, final Class<T> clazz) throws SCPIException
	{
		final Optional<SCPIServiceProviderModel> scpiProvider = getScpiProviderService().getSCPIProvider(store, clazz);
		if (scpiProvider.isEmpty())
		{
			throw new SCPIException(SCPIExceptionType.CLIENT_ERROR, CLIENT_ERROR_SCPI_PROVIDER_IS_NULL);
		}

		return (T) scpiProvider.get();
	}

	/**
	 * @return the scpiProviderService
	 */
	public SCPIProviderService getScpiProviderService()
	{
		return scpiProviderService;
	}

	/**
	 * @return the warehouseService
	 */
	public WarehouseService getWarehouseService()
	{
		return warehouseService;
	}



	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}



}
