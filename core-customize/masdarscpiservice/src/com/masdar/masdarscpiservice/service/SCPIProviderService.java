/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdarscpiservice.model.SCPIServiceProviderModel;


/**
 * @author Husam Dababneh
 */
public interface SCPIProviderService
{
	public Optional<SCPIServiceProviderModel> getSCPIProvider(BaseStoreModel store, final Class<?> providerClass);

	public Optional<SCPIServiceProviderModel> getSCPIProviderByCurrentStore(final Class<?> providerClass);
}
