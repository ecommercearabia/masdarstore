/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.service;

import com.masdar.masdarscpiservice.exception.SCPIException;


/**
 * @author Husam Dababneh
 */
public interface SCPIServiceClient
{
	public String updateSCPIInventory(final String baseURL, final String username, final String password, final String data,
			final long timeout) throws SCPIException;

	public String sendSCPIOrder(final String baseURL, final String username, final String password, final String data)
			throws SCPIException;

	public String sendSCPIConsignment(final String baseURL, final String username, final String password, final String data)
			throws SCPIException;

	public String sendSCPIApproveReturn(final String baseURL, final String username, final String password, final String data)
			throws SCPIException;
}
