/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarscpiservice.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import com.masdar.masdarscpiservice.exception.SCPIException;


/**
 * @author Husam Dababneh
 */
public interface SCPIService
{
	public void updateSCPIInventory(ConsignmentModel consignment) throws SCPIException;

	public void updateSCPIInventory(CartModel cart) throws SCPIException;

	public void updateInventoryForProduct(ProductModel product, BaseStoreModel baseStore) throws SCPIException;

	public void updateInventoryForProductByCurrentStore(ProductModel product) throws SCPIException;

	public void sendSCPIOrder(ConsignmentModel order) throws SCPIException;

	public void sendSCPIConsignment(ConsignmentModel consignment) throws SCPIException;

	public void sendSCPIApproveReturn(ReturnRequestModel returnRequest) throws SCPIException;
}
