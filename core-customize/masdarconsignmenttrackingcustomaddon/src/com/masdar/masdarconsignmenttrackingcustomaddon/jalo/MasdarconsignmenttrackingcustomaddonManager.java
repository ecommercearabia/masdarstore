/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarconsignmenttrackingcustomaddon.jalo;

import com.masdar.masdarconsignmenttrackingcustomaddon.constants.MasdarconsignmenttrackingcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarconsignmenttrackingcustomaddonManager extends GeneratedMasdarconsignmenttrackingcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarconsignmenttrackingcustomaddonManager.class.getName() );
	
	public static final MasdarconsignmenttrackingcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarconsignmenttrackingcustomaddonManager) em.getExtension(MasdarconsignmenttrackingcustomaddonConstants.EXTENSIONNAME);
	}
	
}
