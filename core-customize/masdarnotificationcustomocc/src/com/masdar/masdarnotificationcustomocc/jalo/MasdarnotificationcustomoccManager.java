/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.masdarnotificationcustomocc.constants.MasdarnotificationcustomoccConstants;
import org.apache.log4j.Logger;

public class MasdarnotificationcustomoccManager extends GeneratedMasdarnotificationcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarnotificationcustomoccManager.class.getName() );
	
	public static final MasdarnotificationcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarnotificationcustomoccManager) em.getExtension(MasdarnotificationcustomoccConstants.EXTENSIONNAME);
	}
	
}
