/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarnotificationcustomocc.constants;

/**
 * Global class for all masdarnotificationcustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class MasdarnotificationcustomoccConstants extends GeneratedMasdarnotificationcustomoccConstants
{
	public static final String EXTENSIONNAME = "masdarnotificationcustomocc"; //NOSONAR

	private MasdarnotificationcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
