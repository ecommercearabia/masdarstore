/**
 *
 */
package com.masdar.masdarorderconfirmation.service;

import java.util.List;

import com.masdar.masdarorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailService
{
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
