/**
 *
 */
package com.masdar.masdarorderconfirmation.dao;

import java.util.List;

import com.masdar.masdarorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailDao
{
	/**
	 * @param storeUid
	 * @return
	 */
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
