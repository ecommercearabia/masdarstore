/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bcustomocctests.setup;

import com.masdar.masdarcommercecustomwebservices.test.setup.MasdarcommercecustomwebservicesTestSetup;


public class B2BOCCTestSetup extends MasdarcommercecustomwebservicesTestSetup
{
	public void loadData()
	{
		getSetupImpexService().importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/essential-data.impex", false);
		getSetupImpexService().importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/products.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/essential-data-user-rights.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standalonePermissionManagementTestData.impex",
						false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneBudgetManagementTestData.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneUnitManagementTestData.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneUserManagementTestData.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneOrderApprovalsTestData.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneUnitGroupsManagementTestData.impex",
						false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneReplenishmentOrderTestData.impex", false);
        getSetupImpexService()
                .importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneCostCentersTestData.impex", false);
		getSetupImpexService()
				.importImpexFile("/masdarb2bcustomocctests/import/sampledata/wsCommerceOrg/standaloneOrdersTestData.impex", false);
		getSetupSolrIndexerService().executeSolrIndexerCronJob(String.format("%sIndex", WS_TEST), true);
	}
}
