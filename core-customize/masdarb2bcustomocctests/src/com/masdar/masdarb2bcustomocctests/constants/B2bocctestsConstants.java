/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bcustomocctests.constants;

@SuppressWarnings({ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public class B2bocctestsConstants extends GeneratedB2bocctestsConstants
{
	public static final String EXTENSIONNAME = "masdarb2bcustomocctests";

	private B2bocctestsConstants()
	{
		//empty
	}


}
