/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarb2bcustomocctests.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.apache.log4j.Logger;

import com.masdar.masdarb2bcustomocctests.constants.B2bocctestsConstants;


@SuppressWarnings("PMD")
public class Masdarb2bcustomocctestsManager extends GeneratedMasdarb2bcustomocctestsManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Masdarb2bcustomocctestsManager.class.getName());

	public static final Masdarb2bcustomocctestsManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Masdarb2bcustomocctestsManager) em.getExtension(B2bocctestsConstants.EXTENSIONNAME);
	}

}
