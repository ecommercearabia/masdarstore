/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarsmarteditcustomaddon.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class MasdarsmarteditcustomaddonConstants extends GeneratedMasdarsmarteditcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarsmarteditcustomaddon";
	
	private MasdarsmarteditcustomaddonConstants()
	{
		//empty
	}
}
