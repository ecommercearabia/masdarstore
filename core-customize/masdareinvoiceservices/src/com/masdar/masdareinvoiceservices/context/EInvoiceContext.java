/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.context;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.masdar.masdareinvoiceservices.exception.EInvoiceException;


/**
 *
 */
public interface EInvoiceContext
{
	public void generateEInvoiceForConsignment(final ConsignmentModel consignment) throws EInvoiceException;


	public byte[] getEInvoicePDF(final ConsignmentModel consignment) throws EInvoiceException;

	public void saveEInvoicePDFOnConsignment(final ConsignmentModel consignment) throws EInvoiceException;
}
