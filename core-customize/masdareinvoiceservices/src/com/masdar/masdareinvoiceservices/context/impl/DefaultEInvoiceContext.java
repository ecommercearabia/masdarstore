/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.context.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.logging.log4j.util.Strings;

import com.google.common.base.Preconditions;
import com.masdar.masdareinvoiceservices.context.EInvoiceContext;
import com.masdar.masdareinvoiceservices.context.EInvoiceProviderContext;
import com.masdar.masdareinvoiceservices.enums.EInvoiceProviderType;
import com.masdar.masdareinvoiceservices.exception.EInvoiceException;
import com.masdar.masdareinvoiceservices.model.ClearTaxEInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.strategy.EInvoiceStrategy;


/**
 *
 */
public class DefaultEInvoiceContext implements EInvoiceContext
{

	@Resource(name = "einvoiceProviderContext")
	public EInvoiceProviderContext einvoiceProviderContext;


	@Resource(name = "einvoiceStrategyMap")
	public Map<Class<?>, EInvoiceStrategy> einvoiceStrategyMap;


	@Override
	public void generateEInvoiceForConsignment(final ConsignmentModel consignment) throws EInvoiceException
	{
		validateConsignment(consignment);

		final ImmutablePair<EInvoiceProviderModel, EInvoiceStrategy> providerAndStrategy = getProviderAndStrategy(consignment);
		final EInvoiceProviderModel activeProvider = providerAndStrategy.getLeft();
		final EInvoiceStrategy eInvoiceStrategy = providerAndStrategy.getRight();

		eInvoiceStrategy.generateEInvoiceForConsigmnet(consignment, activeProvider);
	}



	@Override
	public byte[] getEInvoicePDF(final ConsignmentModel consignment) throws EInvoiceException
	{
		validateConsignmentForPDF(consignment);

		final ImmutablePair<EInvoiceProviderModel, EInvoiceStrategy> providerAndStrategy = getProviderAndStrategy(consignment);
		final EInvoiceProviderModel activeProvider = providerAndStrategy.getLeft();
		final EInvoiceStrategy eInvoiceStrategy = providerAndStrategy.getRight();

		return eInvoiceStrategy.getPDFForEInvoice(consignment, activeProvider);

	}


	@Override
	public void saveEInvoicePDFOnConsignment(final ConsignmentModel consignment) throws EInvoiceException
	{
		validateConsignmentForPDF(consignment);

		final ImmutablePair<EInvoiceProviderModel, EInvoiceStrategy> providerAndStrategy = getProviderAndStrategy(consignment);
		final EInvoiceProviderModel activeProvider = providerAndStrategy.getLeft();
		final EInvoiceStrategy eInvoiceStrategy = providerAndStrategy.getRight();

		eInvoiceStrategy.savePDFForEInvoiceOnConsignment(consignment, activeProvider);
	}


	private ImmutablePair<EInvoiceProviderModel, EInvoiceStrategy> getProviderAndStrategy(final ConsignmentModel consignment)
	{
		final BaseStoreModel store = consignment.getOrder().getStore();

		final EInvoiceProviderType eInvoiceProviderType = store.getEinvoiceProviderType();
		final Class<?> providerClass = getProviderClassByType(eInvoiceProviderType);

		Preconditions.checkNotNull(providerClass, "Provider Class Is null");

		final EInvoiceProviderModel activeProvider = getEinvoiceProviderContext().getActiveProvider(providerClass, store)
				.orElse(null);
		Preconditions.checkNotNull(activeProvider, "Could not Find Active Provider For This service");


		final EInvoiceStrategy eInvoiceStrategy = getEinvoiceStrategyMap().get(providerClass);
		Preconditions.checkNotNull(eInvoiceStrategy, "Could not Find Strategy For This service");
		return new ImmutablePair<>(activeProvider, eInvoiceStrategy);
	}

	/**
	 *
	 */
	private Class<?> getProviderClassByType(final EInvoiceProviderType eInvoiceProviderType)
	{
		if (eInvoiceProviderType == EInvoiceProviderType.CLEARTAX)
		{
			return ClearTaxEInvoiceProviderModel.class;
		}

		return null;
	}

	/**
	 * @return the einvoiceProviderContext
	 */
	public EInvoiceProviderContext getEinvoiceProviderContext()
	{
		return einvoiceProviderContext;
	}

	/**
	 * @return the einvoiceStrategyMap
	 */
	public Map<Class<?>, EInvoiceStrategy> getEinvoiceStrategyMap()
	{
		return einvoiceStrategyMap;
	}

	/**
	 *
	 */
	private void validateConsignment(final ConsignmentModel consignment)
	{
		Preconditions.checkNotNull(consignment, "Consignment cannot be null");
		Preconditions.checkNotNull(consignment.getOrder(), "Order cannot be null");
		Preconditions.checkNotNull(consignment.getOrder().getStore(), "Store cannot be null");

	}

	/**
	 *
	 */
	private void validateConsignmentForPDF(final ConsignmentModel consignment)
	{
		validateConsignment(consignment);
		Preconditions.checkNotNull(consignment, "Consignment cannot be null");
		Preconditions.checkNotNull(consignment.getEInvoiceEmmbedXML(), "EInvoiceEmmbedXML is  null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvoiceNumber()), "EInvoiceNumber is empty or null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvocieDocumentType()),
				"EInvocieDocumentType is empty or null");


		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvocieTaxPayerID()), "EInvocieTaxPayerID is empty or null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvoiceDocumentDate()),
				"EInvoiceDocumentDate is empty or null");
	}
}
