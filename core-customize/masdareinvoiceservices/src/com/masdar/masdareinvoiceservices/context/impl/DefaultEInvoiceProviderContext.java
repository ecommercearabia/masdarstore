/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.masdar.masdareinvoiceservices.context.EInvoiceProviderContext;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.strategy.EInvoiceProviderStrategy;


/**
 *
 */
public class DefaultEInvoiceProviderContext implements EInvoiceProviderContext
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultEInvoiceProviderContext.class);

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";


	@Resource(name = "einvoiceProviderStrategyMap")
	private Map<Class<?>, EInvoiceProviderStrategy> einvoiceProviderMap;

	@Override
	public Optional<EInvoiceProviderModel> getActiveProvider(final Class<?> providerClass, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProvider(baseStoreModel);
	}


	@Override
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProviderByCurrentStore();
	}

	/**
	 * @return the einvoiceProviderMap
	 */
	public Map<Class<?>, EInvoiceProviderStrategy> getEinvoiceProviderMap()
	{
		return einvoiceProviderMap;
	}

	/**
	 *
	 */
	private Optional<EInvoiceProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		return Optional.ofNullable(getEinvoiceProviderMap().get(providerClass));
	}


}
