/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;


/**
 *
 */
public interface EInvoiceProviderContext
{
	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	public Optional<EInvoiceProviderModel> getActiveProvider(Class<?> providerClass, BaseStoreModel baseStoreModel);

	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore(Class<?> providerClass);

}
