package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AccountingParty
{

	@Expose
	@SerializedName("Party")
	private Party party;

	/**
	 * @return the party
	 */
	public Party getParty()
	{
		return party;
	}

	/**
	 * @param party
	 *           the party to set
	 */
	public void setParty(final Party party)
	{
		this.party = party;
	}



}
