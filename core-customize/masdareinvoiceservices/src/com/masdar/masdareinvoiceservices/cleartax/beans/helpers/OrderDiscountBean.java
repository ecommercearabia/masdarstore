/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.cleartax.beans.helpers;

/**
 *
 */
public class OrderDiscountBean
{
	private double discountPercentage;
	private double discountValue;



	/**
	 *
	 */
	public OrderDiscountBean(final double discountPercentage, final double discountValue)
	{
		super();
		this.discountPercentage = discountPercentage;
		this.discountValue = discountValue;
	}

	//	/**
	//	 * @return the discountPercentage
	//	 */
	//	public double getDiscountPercentage()
	//	{
	//		return discountPercentage;
	//	}

	/**
	 * @param discountPercentage
	 *           the discountPercentage to set
	 */
	public void setDiscountPercentage(final double discountPercentage)
	{
		this.discountPercentage = discountPercentage;
	}

	/**
	 * @return the discountValue
	 */
	public double getDiscountValue()
	{
		return discountValue;
	}

	/**
	 * @param discountValue
	 *           the discountValue to set
	 */
	public void setDiscountValue(final double discountValue)
	{
		this.discountValue = discountValue;
	}


}
