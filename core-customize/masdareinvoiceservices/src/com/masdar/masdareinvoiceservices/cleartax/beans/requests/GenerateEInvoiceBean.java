package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GenerateEInvoiceBean
{

	@Expose
	@SerializedName("UBLVersionID")
	private String ublVersionID;
	@Expose
	@SerializedName("ProfileID")
	private String profileID;

	@Expose
	@SerializedName("ID")
	private DualLanguageBean id;

	@Expose
	@SerializedName("InvoiceTypeCode")
	private NameValueBean invoiceTypeCode;

	// NOTE(Husam): Format YYYY-MM-DD
	@Expose
	@SerializedName("IssueDate")
	private String issueDate;

	// NOTE(Husam): Format HH:mm:ss (24 Hour format)
	@Expose
	@SerializedName("IssueTime")
	private String issueTime;

	@Expose
	@SerializedName("Delivery")
	private List<DeliveryBean> delivery;

	@Expose
	@SerializedName("BillingReference")
	private List<BillingReferenceBean> billingReference;

	@Expose
	@SerializedName("OrderReference")
	private IDObject orderReference;

	@Expose
	@SerializedName("ContractDocumentReference")
	private List<IDObject> contractDocumentReference;

	@Expose
	@SerializedName("DocumentCurrencyCode")
	private String documentCurrencyCode;

	@Expose
	@SerializedName("TaxCurrencyCode")
	private String taxCurrencyCode;

	@Expose
	@SerializedName("AccountingSupplierParty")
	private AccountingParty accountingSupplierParty;

	@Expose
	@SerializedName("AccountingCustomerParty")
	private AccountingParty accountingCustomerParty;

	@Expose
	@SerializedName("InvoiceLine")
	private List<InvoiceLine> invoiceLine;

	@Expose
	@SerializedName("AllowanceCharge")
	private List<AllowanceCharge> allowanceCharge;

	@Expose
	@SerializedName("TaxTotal")
	private List<ClearTaxTaxTotal> taxTotal;

	@Expose
	@SerializedName("LegalMonetaryTotal")
	private LegalMonetaryTotal legalMonetaryTotal;

	@Expose
	@SerializedName("PaymentMeans")
	private List<PaymentMeans> paymentMeans;

	@Expose
	@SerializedName("Note")
	private DualLanguageBean note;

	/**
	 * @return the ublVersionID
	 */
	public String getUblVersionID()
	{
		return ublVersionID;
	}

	/**
	 * @param ublVersionID
	 *           the ublVersionID to set
	 */
	public void setUblVersionID(final String ublVersionID)
	{
		this.ublVersionID = ublVersionID;
	}

	/**
	 * @return the profileID
	 */
	public String getProfileID()
	{
		return profileID;
	}

	/**
	 * @param profileID
	 *           the profileID to set
	 */
	public void setProfileID(final String profileID)
	{
		this.profileID = profileID;
	}

	/**
	 * @return the id
	 */
	public DualLanguageBean getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final DualLanguageBean id)
	{
		this.id = id;
	}

	/**
	 * @return the invoiceTypeCode
	 */
	public NameValueBean getInvoiceTypeCode()
	{
		return invoiceTypeCode;
	}

	/**
	 * @param invoiceTypeCode
	 *           the invoiceTypeCode to set
	 */
	public void setInvoiceTypeCode(final NameValueBean invoiceTypeCode)
	{
		this.invoiceTypeCode = invoiceTypeCode;
	}

	/**
	 * @return the issueDate
	 */
	public String getIssueDate()
	{
		return issueDate;
	}

	/**
	 * @param issueDate
	 *           the issueDate to set
	 */
	public void setIssueDate(final String issueDate)
	{
		this.issueDate = issueDate;
	}

	/**
	 * @return the issueTime
	 */
	public String getIssueTime()
	{
		return issueTime;
	}

	/**
	 * @param issueTime
	 *           the issueTime to set
	 */
	public void setIssueTime(final String issueTime)
	{
		this.issueTime = issueTime;
	}

	/**
	 * @return the delivery
	 */
	public List<DeliveryBean> getDelivery()
	{
		return delivery;
	}

	/**
	 * @param delivery
	 *           the delivery to set
	 */
	public void setDelivery(final List<DeliveryBean> delivery)
	{
		this.delivery = delivery;
	}

	/**
	 * @return the billingReference
	 */
	public List<BillingReferenceBean> getBillingReference()
	{
		return billingReference;
	}

	/**
	 * @param billingReference
	 *           the billingReference to set
	 */
	public void setBillingReference(final List<BillingReferenceBean> billingReference)
	{
		this.billingReference = billingReference;
	}

	/**
	 * @return the orderReference
	 */
	public IDObject getOrderReference()
	{
		return orderReference;
	}

	/**
	 * @param orderReference
	 *           the orderReference to set
	 */
	public void setOrderReference(final IDObject orderReference)
	{
		this.orderReference = orderReference;
	}

	/**
	 * @return the contractDocumentReference
	 */
	public List<IDObject> getContractDocumentReference()
	{
		return contractDocumentReference;
	}

	/**
	 * @param contractDocumentReference
	 *           the contractDocumentReference to set
	 */
	public void setContractDocumentReference(final List<IDObject> contractDocumentReference)
	{
		this.contractDocumentReference = contractDocumentReference;
	}

	/**
	 * @return the documentCurrencyCode
	 */
	public String getDocumentCurrencyCode()
	{
		return documentCurrencyCode;
	}

	/**
	 * @param documentCurrencyCode
	 *           the documentCurrencyCode to set
	 */
	public void setDocumentCurrencyCode(final String documentCurrencyCode)
	{
		this.documentCurrencyCode = documentCurrencyCode;
	}

	/**
	 * @return the taxCurrencyCode
	 */
	public String getTaxCurrencyCode()
	{
		return taxCurrencyCode;
	}

	/**
	 * @param taxCurrencyCode
	 *           the taxCurrencyCode to set
	 */
	public void setTaxCurrencyCode(final String taxCurrencyCode)
	{
		this.taxCurrencyCode = taxCurrencyCode;
	}

	/**
	 * @return the accountingSupplierParty
	 */
	public AccountingParty getAccountingSupplierParty()
	{
		return accountingSupplierParty;
	}

	/**
	 * @param accountingSupplierParty
	 *           the accountingSupplierParty to set
	 */
	public void setAccountingSupplierParty(final AccountingParty accountingSupplierParty)
	{
		this.accountingSupplierParty = accountingSupplierParty;
	}

	/**
	 * @return the accountingCustomerParty
	 */
	public AccountingParty getAccountingCustomerParty()
	{
		return accountingCustomerParty;
	}

	/**
	 * @param accountingCustomerParty
	 *           the accountingCustomerParty to set
	 */
	public void setAccountingCustomerParty(final AccountingParty accountingCustomerParty)
	{
		this.accountingCustomerParty = accountingCustomerParty;
	}

	/**
	 * @return the invoiceLine
	 */
	public List<InvoiceLine> getInvoiceLine()
	{
		return invoiceLine;
	}

	/**
	 * @param invoiceLine
	 *           the invoiceLine to set
	 */
	public void setInvoiceLine(final List<InvoiceLine> invoiceLine)
	{
		this.invoiceLine = invoiceLine;
	}

	/**
	 * @return the allowanceCharge
	 */
	public List<AllowanceCharge> getAllowanceCharge()
	{
		return allowanceCharge;
	}

	/**
	 * @param allowanceCharge
	 *           the allowanceCharge to set
	 */
	public void setAllowanceCharge(final List<AllowanceCharge> allowanceCharge)
	{
		this.allowanceCharge = allowanceCharge;
	}

	/**
	 * @return the taxTotal
	 */
	public List<ClearTaxTaxTotal> getTaxTotal()
	{
		return taxTotal;
	}

	/**
	 * @param taxTotal
	 *           the taxTotal to set
	 */
	public void setTaxTotal(final List<ClearTaxTaxTotal> taxTotal)
	{
		this.taxTotal = taxTotal;
	}

	/**
	 * @return the legalMonetaryTotal
	 */
	public LegalMonetaryTotal getLegalMonetaryTotal()
	{
		return legalMonetaryTotal;
	}

	/**
	 * @param legalMonetaryTotal
	 *           the legalMonetaryTotal to set
	 */
	public void setLegalMonetaryTotal(final LegalMonetaryTotal legalMonetaryTotal)
	{
		this.legalMonetaryTotal = legalMonetaryTotal;
	}

	/**
	 * @return the paymentMeans
	 */
	public List<PaymentMeans> getPaymentMeans()
	{
		return paymentMeans;
	}

	/**
	 * @param paymentMeans
	 *           the paymentMeans to set
	 */
	public void setPaymentMeans(final List<PaymentMeans> paymentMeans)
	{
		this.paymentMeans = paymentMeans;
	}

	/**
	 * @return the note
	 */
	public DualLanguageBean getNote()
	{
		return note;
	}

	/**
	 * @param note
	 *           the note to set
	 */
	public void setNote(final DualLanguageBean note)
	{
		this.note = note;
	}



}
