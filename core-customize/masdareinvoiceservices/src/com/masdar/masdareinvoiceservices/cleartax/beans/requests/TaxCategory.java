package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TaxCategory
{

	@Expose
	@SerializedName("ID")
	private String id;

	@Expose
	@SerializedName("Percent")
	private String percent;

	@Expose
	@SerializedName("TaxScheme")
	private TaxScheme taxScheme;

	@Expose
	@SerializedName("TaxExemptionReason")
	private DualLanguageBean taxExemptionReason;

	@Expose
	@SerializedName("TaxExemptionReasonCode")
	private String taxExemptionReasonCode;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * @return the percent
	 */
	public String getPercent()
	{
		return percent;
	}

	/**
	 * @param percent
	 *           the percent to set
	 */
	public void setPercent(final String percent)
	{
		this.percent = percent;
	}

	/**
	 * @return the taxScheme
	 */
	public TaxScheme getTaxScheme()
	{
		return taxScheme;
	}

	/**
	 * @param taxScheme
	 *           the taxScheme to set
	 */
	public void setTaxScheme(final TaxScheme taxScheme)
	{
		this.taxScheme = taxScheme;
	}

	/**
	 * @return the taxExemptionReason
	 */
	public DualLanguageBean getTaxExemptionReason()
	{
		return taxExemptionReason;
	}

	/**
	 * @param taxExemptionReason
	 *           the taxExemptionReason to set
	 */
	public void setTaxExemptionReason(final DualLanguageBean taxExemptionReason)
	{
		this.taxExemptionReason = taxExemptionReason;
	}

	/**
	 * @return the taxExemptionReasonCode
	 */
	public String getTaxExemptionReasonCode()
	{
		return taxExemptionReasonCode;
	}

	/**
	 * @param taxExemptionReasonCode
	 *           the taxExemptionReasonCode to set
	 */
	public void setTaxExemptionReasonCode(final String taxExemptionReasonCode)
	{
		this.taxExemptionReasonCode = taxExemptionReasonCode;
	}


}
