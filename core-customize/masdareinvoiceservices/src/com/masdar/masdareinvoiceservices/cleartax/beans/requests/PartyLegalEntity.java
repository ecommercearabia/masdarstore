package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PartyLegalEntity
{
	@Expose
	@SerializedName("RegistrationName")
	private DualLanguageBean registrationName;

	public PartyLegalEntity(final DualLanguageBean registrationName)
	{
		this.registrationName = registrationName;
	}

	public PartyLegalEntity(final String en, final String ar)
	{
		this.registrationName = new DualLanguageBean(en, ar);
	}

	public PartyLegalEntity(final String bothLanguages)
	{
		this.registrationName = new DualLanguageBean(bothLanguages);
	}

	public DualLanguageBean getRegistrationName()
	{
		return registrationName;
	}

	public void setRegistrationName(final DualLanguageBean registrationName)
	{
		this.registrationName = registrationName;
	}

}
