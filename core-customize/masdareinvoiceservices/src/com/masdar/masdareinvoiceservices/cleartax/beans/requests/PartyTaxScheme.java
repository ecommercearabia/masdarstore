package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PartyTaxScheme
{
	@Expose
	@SerializedName("CompanyID")
	private String companyId;

	@Expose
	@SerializedName("TaxScheme")
	private TaxScheme taxScheme;

	/**
	 * @return the companyId
	 */
	public String getCompanyId()
	{
		return companyId;
	}

	/**
	 * @param companyId
	 *           the companyId to set
	 */
	public void setCompanyId(final String companyId)
	{
		this.companyId = companyId;
	}

	/**
	 * @return the taxScheme
	 */
	public TaxScheme getTaxScheme()
	{
		return taxScheme;
	}

	/**
	 * @param taxScheme
	 *           the taxScheme to set
	 */
	public void setTaxScheme(final TaxScheme taxScheme)
	{
		this.taxScheme = taxScheme;
	}



}
