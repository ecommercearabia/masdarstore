package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DualLanguageBean
{
	@Expose
	@SerializedName("en")
	private String en;

	@Expose
	@SerializedName("ar")
	private String ar;

	/**
	 *
	 */
	public DualLanguageBean(final String bothLanguages)
	{
		this.en = bothLanguages;
		this.ar = bothLanguages;
	}

	/**
	 *
	 */
	public DualLanguageBean(final String en, final String ar)
	{
		this.en = en;
		this.ar = ar;
	}

	/**
	 * @return the en
	 */
	public String getEn()
	{
		return en;
	}

	/**
	 * @param en
	 *           the en to set
	 */
	public void setEn(final String en)
	{
		this.en = en;
	}

	/**
	 * @return the ar
	 */
	public String getAr()
	{
		return ar;
	}

	/**
	 * @param ar
	 *           the ar to set
	 */
	public void setAr(final String ar)
	{
		this.ar = ar;
	}


}
