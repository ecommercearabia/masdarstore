package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxItem
{
	@Expose
	@SerializedName("BuyersItemIdentification")
	private IDObject buyersItemIdentification;

	@Expose
	@SerializedName("SellersItemIdentification")
	private IDObject sellersItemIdentification;

	@Expose
	@SerializedName("StandardItemIdentification")
	private IDObject standardItemIdentification;

	@Expose
	@SerializedName("Name")
	private DualLanguageBean name;

	@Expose
	@SerializedName("ClassifiedTaxCategory")
	private TaxCategory classifiedTaxCategory;

	/**
	 * @return the buyersItemIdentification
	 */
	public IDObject getBuyersItemIdentification()
	{
		return buyersItemIdentification;
	}

	/**
	 * @param buyersItemIdentification
	 *           the buyersItemIdentification to set
	 */
	public void setBuyersItemIdentification(final IDObject buyersItemIdentification)
	{
		this.buyersItemIdentification = buyersItemIdentification;
	}

	/**
	 * @return the sellersItemIdentification
	 */
	public IDObject getSellersItemIdentification()
	{
		return sellersItemIdentification;
	}

	/**
	 * @param sellersItemIdentification
	 *           the sellersItemIdentification to set
	 */
	public void setSellersItemIdentification(final IDObject sellersItemIdentification)
	{
		this.sellersItemIdentification = sellersItemIdentification;
	}

	/**
	 * @return the standardItemIdentification
	 */
	public IDObject getStandardItemIdentification()
	{
		return standardItemIdentification;
	}

	/**
	 * @param standardItemIdentification
	 *           the standardItemIdentification to set
	 */
	public void setStandardItemIdentification(final IDObject standardItemIdentification)
	{
		this.standardItemIdentification = standardItemIdentification;
	}

	/**
	 * @return the name
	 */
	public DualLanguageBean getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final DualLanguageBean name)
	{
		this.name = name;
	}

	/**
	 * @return the classifiedTaxCategory
	 */
	public TaxCategory getClassifiedTaxCategory()
	{
		return classifiedTaxCategory;
	}

	/**
	 * @param classifiedTaxCategory
	 *           the classifiedTaxCategory to set
	 */
	public void setClassifiedTaxCategory(final TaxCategory classifiedTaxCategory)
	{
		this.classifiedTaxCategory = classifiedTaxCategory;
	}



}
