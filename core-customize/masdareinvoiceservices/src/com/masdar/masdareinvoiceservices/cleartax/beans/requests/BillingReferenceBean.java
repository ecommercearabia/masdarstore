package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BillingReferenceBean
{
	@Expose
	@SerializedName("InvoiceDocumentReference")
	private IDObject invoiceDocumentReference;

	/**
	 * @return the invoiceDocumentReference
	 */
	public IDObject getInvoiceDocumentReference()
	{
		return invoiceDocumentReference;
	}

	/**
	 * @param invoiceDocumentReference
	 *           the invoiceDocumentReference to set
	 */
	public void setInvoiceDocumentReference(final IDObject invoiceDocumentReference)
	{
		this.invoiceDocumentReference = invoiceDocumentReference;
	}



}
