/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.cleartax.beans.helpers;

import java.text.DecimalFormat;


/**
 *
 */
public final class ClearTaxUtils
{

	private ClearTaxUtils()
	{
	}

	public static double formatDouble(final double value)
	{
		final DecimalFormat df = new DecimalFormat("#.00");
		return Double.valueOf(df.format(value));
	}


}
