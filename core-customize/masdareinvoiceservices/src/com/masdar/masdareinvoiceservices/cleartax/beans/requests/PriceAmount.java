package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masdar.masdareinvoiceservices.cleartax.beans.helpers.ClearTaxUtils;


public class PriceAmount
{
	@Expose
	@SerializedName("currencyID")
	private String currencyID;

	@Expose
	@SerializedName("value")
	private double value;



	/**
	 *
	 */
	public PriceAmount()
	{
		super();
	}

	/**
	 *
	 */
	public PriceAmount(final String currencyID, final double value)
	{
		super();
		this.currencyID = currencyID;
		this.value = ClearTaxUtils.formatDouble(value);
	}

	public String getCurrencyID()
	{
		return currencyID;
	}

	public void setCurrencyID(final String currencyID)
	{
		this.currencyID = currencyID;
	}


	public double getValue()
	{
		return value;
	}

	public void setValue(final double value)
	{
		this.value = ClearTaxUtils.formatDouble(value);
	}

	public PriceAmount add(final PriceAmount toBeAdded)
	{
		if (!this.getCurrencyID().equals(toBeAdded.getCurrencyID()))
		{
			throw new IllegalArgumentException("Currencies does not match");
		}

		return new PriceAmount(this.currencyID, this.getValue() + toBeAdded.getValue());
	}
}
