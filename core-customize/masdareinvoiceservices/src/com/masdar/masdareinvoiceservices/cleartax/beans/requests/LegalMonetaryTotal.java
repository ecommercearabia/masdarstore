package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LegalMonetaryTotal
{
	@Expose
	@SerializedName("LineExtensionAmount")
	private PriceAmount lineExtensionAmount;

	@Expose
	@SerializedName("AllowanceTotalAmount")
	private PriceAmount allowanceTotalAmount;

	@Expose
	@SerializedName("TaxExclusiveAmount")
	private PriceAmount taxExclusiveAmount;

	@Expose
	@SerializedName("TaxInclusiveAmount")
	private PriceAmount taxInclusiveAmount;

	@Expose
	@SerializedName("PrepaidAmount")
	private PriceAmount prepaidAmount;

	@Expose
	@SerializedName("PayableAmount")
	private PriceAmount payableAmount;

	/**
	 * @return the lineExtensionAmount
	 */
	public PriceAmount getLineExtensionAmount()
	{
		return lineExtensionAmount;
	}

	/**
	 * @param lineExtensionAmount
	 *           the lineExtensionAmount to set
	 */
	public void setLineExtensionAmount(final PriceAmount lineExtensionAmount)
	{
		this.lineExtensionAmount = lineExtensionAmount;
	}

	/**
	 * @return the allowanceTotalAmount
	 */
	public PriceAmount getAllowanceTotalAmount()
	{
		return allowanceTotalAmount;
	}

	/**
	 * @param allowanceTotalAmount
	 *           the allowanceTotalAmount to set
	 */
	public void setAllowanceTotalAmount(final PriceAmount allowanceTotalAmount)
	{
		this.allowanceTotalAmount = allowanceTotalAmount;
	}

	/**
	 * @return the taxExclusiveAmount
	 */
	public PriceAmount getTaxExclusiveAmount()
	{
		return taxExclusiveAmount;
	}

	/**
	 * @param taxExclusiveAmount
	 *           the taxExclusiveAmount to set
	 */
	public void setTaxExclusiveAmount(final PriceAmount taxExclusiveAmount)
	{
		this.taxExclusiveAmount = taxExclusiveAmount;
	}

	/**
	 * @return the taxInclusiveAmount
	 */
	public PriceAmount getTaxInclusiveAmount()
	{
		return taxInclusiveAmount;
	}

	/**
	 * @param taxInclusiveAmount
	 *           the taxInclusiveAmount to set
	 */
	public void setTaxInclusiveAmount(final PriceAmount taxInclusiveAmount)
	{
		this.taxInclusiveAmount = taxInclusiveAmount;
	}

	/**
	 * @return the prepaidAmount
	 */
	public PriceAmount getPrepaidAmount()
	{
		return prepaidAmount;
	}

	/**
	 * @param prepaidAmount
	 *           the prepaidAmount to set
	 */
	public void setPrepaidAmount(final PriceAmount prepaidAmount)
	{
		this.prepaidAmount = prepaidAmount;
	}

	/**
	 * @return the payableAmount
	 */
	public PriceAmount getPayableAmount()
	{
		return payableAmount;
	}

	/**
	 * @param payableAmount
	 *           the payableAmount to set
	 */
	public void setPayableAmount(final PriceAmount payableAmount)
	{
		this.payableAmount = payableAmount;
	}

}
