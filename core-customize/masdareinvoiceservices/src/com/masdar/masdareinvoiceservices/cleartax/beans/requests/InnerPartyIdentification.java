package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InnerPartyIdentification
{
	@Expose
	@SerializedName("schemeID")
	private String schemeID;

	@Expose
	@SerializedName("value")
	private String value;

	/**
	 * @return the schemeID
	 */
	public String getSchemeID()
	{
		return schemeID;
	}

	/**
	 * @param schemeID
	 *           the schemeID to set
	 */
	public void setSchemeID(final String schemeID)
	{
		this.schemeID = schemeID;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	public void setValue(final String value)
	{
		this.value = value;
	}



}
