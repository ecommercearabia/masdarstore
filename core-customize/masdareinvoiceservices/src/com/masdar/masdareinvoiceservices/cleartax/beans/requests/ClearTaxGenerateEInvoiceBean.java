package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxGenerateEInvoiceBean
{

	@Expose
	@SerializedName("DeviceId")
	private String deviceId;

	@Expose
	@SerializedName("EInvoice")
	private GenerateEInvoiceBean eInvoice;

	@Expose
	@SerializedName("CustomFields")
	private Object customFields;

	/**
	 * @return the deviceId
	 */
	public String getDeviceId()
	{
		return deviceId;
	}

	/**
	 * @param deviceId
	 *           the deviceId to set
	 */
	public void setDeviceId(final String deviceId)
	{
		this.deviceId = deviceId;
	}

	/**
	 * @return the eInvoice
	 */
	public GenerateEInvoiceBean geteInvoice()
	{
		return eInvoice;
	}

	/**
	 * @param eInvoice
	 *           the eInvoice to set
	 */
	public void seteInvoice(final GenerateEInvoiceBean eInvoice)
	{
		this.eInvoice = eInvoice;
	}

	/**
	 * @return the customFields
	 */
	public Object getCustomFields()
	{
		return customFields;
	}

	/**
	 * @param customFields
	 *           the customFields to set
	 */
	public void setCustomFields(final Object customFields)
	{
		this.customFields = customFields;
	}



}
