package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxQuantity
{

	@Expose
	@SerializedName("unitCode")
	private String unitCode;

	@Expose
	@SerializedName("value")
	private String value;



	/**
	 *
	 */
	public ClearTaxQuantity()
	{
		super();
	}

	/**
	 *
	 */
	public ClearTaxQuantity(final String unitCode, final String value)
	{
		super();
		this.unitCode = unitCode;
		this.value = value;
	}

	/**
	 * @return the unitCode
	 */
	public String getUnitCode()
	{
		return unitCode;
	}

	/**
	 * @param unitCode
	 *           the unitCode to set
	 */
	public void setUnitCode(final String unitCode)
	{
		this.unitCode = unitCode;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	public void setValue(final String value)
	{
		this.value = value;
	}


}
