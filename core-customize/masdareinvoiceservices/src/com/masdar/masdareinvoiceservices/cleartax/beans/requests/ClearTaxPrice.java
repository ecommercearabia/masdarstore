package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxPrice
{

	@Expose
	@SerializedName("PriceAmount")
	private PriceAmount priceAmount;

	@Expose
	@SerializedName("BaseQuantity")
	private ClearTaxQuantity baseQuantity;

	@Expose
	@SerializedName("AllowanceCharge")
	private AllowanceCharge allowanceCharge;

	/**
	 * @return the priceAmount
	 */
	public PriceAmount getPriceAmount()
	{
		return priceAmount;
	}

	/**
	 * @param priceAmount
	 *           the priceAmount to set
	 */
	public void setPriceAmount(final PriceAmount priceAmount)
	{
		this.priceAmount = priceAmount;
	}

	/**
	 * @return the baseQuantity
	 */
	public ClearTaxQuantity getBaseQuantity()
	{
		return baseQuantity;
	}

	/**
	 * @param baseQuantity
	 *           the baseQuantity to set
	 */
	public void setBaseQuantity(final ClearTaxQuantity baseQuantity)
	{
		this.baseQuantity = baseQuantity;
	}

	/**
	 * @return the allowanceCharge
	 */
	public AllowanceCharge getAllowanceCharge()
	{
		return allowanceCharge;
	}

	/**
	 * @param allowanceCharge
	 *           the allowanceCharge to set
	 */
	public void setAllowanceCharge(final AllowanceCharge allowanceCharge)
	{
		this.allowanceCharge = allowanceCharge;
	}


}
