package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InvoiceLine
{

	@Expose
	@SerializedName("ID")
	private Object id;

	@Expose
	@SerializedName("Item")
	private ClearTaxItem item;

	@Expose
	@SerializedName("Price")
	private ClearTaxPrice price;

	@Expose
	@SerializedName("InvoicedQuantity")
	private ClearTaxQuantity invoicedQuantity;

	@Expose
	@SerializedName("AllowanceCharge")
	private List<AllowanceCharge> allowanceCharge;

	@Expose
	@SerializedName("LineExtensionAmount")
	private PriceAmount lineExtensionAmount;

	@Expose
	@SerializedName("TaxTotal")
	private ClearTaxTaxTotal taxTotal;

	/**
	 * @return the id
	 */
	public Object getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final Object id)
	{
		this.id = id;
	}

	/**
	 * @return the item
	 */
	public ClearTaxItem getItem()
	{
		return item;
	}

	/**
	 * @param item
	 *           the item to set
	 */
	public void setItem(final ClearTaxItem item)
	{
		this.item = item;
	}

	/**
	 * @return the price
	 */
	public ClearTaxPrice getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *           the price to set
	 */
	public void setPrice(final ClearTaxPrice price)
	{
		this.price = price;
	}

	/**
	 * @return the invoicedQuantity
	 */
	public ClearTaxQuantity getInvoicedQuantity()
	{
		return invoicedQuantity;
	}

	/**
	 * @param invoicedQuantity
	 *           the invoicedQuantity to set
	 */
	public void setInvoicedQuantity(final ClearTaxQuantity invoicedQuantity)
	{
		this.invoicedQuantity = invoicedQuantity;
	}

	/**
	 * @return the allowanceCharge
	 */
	public List<AllowanceCharge> getAllowanceCharge()
	{
		return allowanceCharge;
	}

	/**
	 * @param allowanceCharge
	 *           the allowanceCharge to set
	 */
	public void setAllowanceCharge(final List<AllowanceCharge> allowanceCharge)
	{
		this.allowanceCharge = allowanceCharge;
	}

	/**
	 * @return the lineExtensionAmount
	 */
	public PriceAmount getLineExtensionAmount()
	{
		return lineExtensionAmount;
	}

	/**
	 * @param lineExtensionAmount
	 *           the lineExtensionAmount to set
	 */
	public void setLineExtensionAmount(final PriceAmount lineExtensionAmount)
	{
		this.lineExtensionAmount = lineExtensionAmount;
	}

	/**
	 * @return the taxTotal
	 */
	public ClearTaxTaxTotal getTaxTotal()
	{
		return taxTotal;
	}

	/**
	 * @param taxTotal
	 *           the taxTotal to set
	 */
	public void setTaxTotal(final ClearTaxTaxTotal taxTotal)
	{
		this.taxTotal = taxTotal;
	}



}
