package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TaxSubtotal
{
	@Expose
	@SerializedName("TaxableAmount")
	private PriceAmount taxableAmount;

	@Expose
	@SerializedName("TaxAmount")
	private PriceAmount taxAmount;

	@Expose
	@SerializedName("TaxCategory")
	private TaxCategory taxCategory;

	/**
	 * @return the taxableAmount
	 */
	public PriceAmount getTaxableAmount()
	{
		return taxableAmount;
	}

	/**
	 * @param taxableAmount
	 *           the taxableAmount to set
	 */
	public void setTaxableAmount(final PriceAmount taxableAmount)
	{
		this.taxableAmount = taxableAmount;
	}

	/**
	 * @return the taxAmount
	 */
	public PriceAmount getTaxAmount()
	{
		return taxAmount;
	}

	/**
	 * @param taxAmount
	 *           the taxAmount to set
	 */
	public void setTaxAmount(final PriceAmount taxAmount)
	{
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the taxCategory
	 */
	public TaxCategory getTaxCategory()
	{
		return taxCategory;
	}

	/**
	 * @param taxCategory
	 *           the taxCategory to set
	 */
	public void setTaxCategory(final TaxCategory taxCategory)
	{
		this.taxCategory = taxCategory;
	}


}
