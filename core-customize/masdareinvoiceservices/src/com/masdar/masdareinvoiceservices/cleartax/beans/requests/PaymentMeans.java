package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PaymentMeans
{

	@Expose
	@SerializedName("InstructionNote")
	private DualLanguageBean instructionNote;

	@Expose
	@SerializedName("PayeeFinancialAccount")
	private PayeeFinancialAccount payeeFinancialAccount;

	@Expose
	@SerializedName("PaymentMeansCode")
	private String paymentMeansCode;

	/**
	 * @return the instructionNote
	 */
	public DualLanguageBean getInstructionNote()
	{
		return instructionNote;
	}

	/**
	 * @param instructionNote
	 *           the instructionNote to set
	 */
	public void setInstructionNote(final DualLanguageBean instructionNote)
	{
		this.instructionNote = instructionNote;
	}

	/**
	 * @return the payeeFinancialAccount
	 */
	public PayeeFinancialAccount getPayeeFinancialAccount()
	{
		return payeeFinancialAccount;
	}

	/**
	 * @param payeeFinancialAccount
	 *           the payeeFinancialAccount to set
	 */
	public void setPayeeFinancialAccount(final PayeeFinancialAccount payeeFinancialAccount)
	{
		this.payeeFinancialAccount = payeeFinancialAccount;
	}

	/**
	 * @return the paymentMeansCode
	 */
	public String getPaymentMeansCode()
	{
		return paymentMeansCode;
	}

	/**
	 * @param paymentMeansCode
	 *           the paymentMeansCode to set
	 */
	public void setPaymentMeansCode(final String paymentMeansCode)
	{
		this.paymentMeansCode = paymentMeansCode;
	}



}
