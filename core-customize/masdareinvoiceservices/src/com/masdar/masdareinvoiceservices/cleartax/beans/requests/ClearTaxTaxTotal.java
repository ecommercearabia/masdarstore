package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxTaxTotal
{
	@Expose
	@SerializedName("TaxAmount")
	private PriceAmount taxAmount;

	@Expose
	@SerializedName("RoundingAmount")
	private PriceAmount roundingAmount;

	@Expose
	@SerializedName("TaxSubtotal")
	private List<TaxSubtotal> taxSubtotal;

	/**
	 * @return the taxAmount
	 */
	public PriceAmount getTaxAmount()
	{
		return taxAmount;
	}

	/**
	 * @param taxAmount
	 *           the taxAmount to set
	 */
	public void setTaxAmount(final PriceAmount taxAmount)
	{
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the roundingAmount
	 */
	public PriceAmount getRoundingAmount()
	{
		return roundingAmount;
	}

	/**
	 * @param roundingAmount
	 *           the roundingAmount to set
	 */
	public void setRoundingAmount(final PriceAmount roundingAmount)
	{
		this.roundingAmount = roundingAmount;
	}

	/**
	 * @return the taxSubtotal
	 */
	public List<TaxSubtotal> getTaxSubtotal()
	{
		return taxSubtotal;
	}

	/**
	 * @param taxSubtotal
	 *           the taxSubtotal to set
	 */
	public void setTaxSubtotal(final List<TaxSubtotal> taxSubtotal)
	{
		this.taxSubtotal = taxSubtotal;
	}



}
