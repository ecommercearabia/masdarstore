package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PartyIdentification
{

	@Expose
	@SerializedName("ID")
	private InnerPartyIdentification id;

	/**
	 * @return the id
	 */
	public InnerPartyIdentification getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final InnerPartyIdentification id)
	{
		this.id = id;
	}



}
