package com.masdar.masdareinvoiceservices.cleartax.beans.responses;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GenerateEInvoiceResponse
{

	@Expose
	@SerializedName("DeviceId")
	private String deviceId;

	@Expose
	@SerializedName("Status")
	private String status;

	@Expose
	@SerializedName("QrCodeStatus")
	private String qrCodeStatus;

	@Expose
	@SerializedName("InvoiceStatus")
	private String invoiceStatus;

	@Expose
	@SerializedName("QRCode")
	private String qRCode;

	@Expose
	@SerializedName("RawQRCode")
	private String rawQRCode;

	@Expose
	@SerializedName("InvoiceXml")
	private String invoiceXml;

	@Expose
	@SerializedName("UUID")
	private String uUID;

	@Expose
	@SerializedName("ICV")
	private String iCV;

	@Expose
	@SerializedName("PIH")
	private String pIH;

	@Expose
	@SerializedName("InvoiceHash")
	private String invoiceHash;

	@Expose
	@SerializedName("InvoiceType")
	private String invoiceType;

	@Expose
	@SerializedName("InvoiceNumber")
	private String invoiceNumber;

	@Expose
	@SerializedName("IssueDate")
	private String issueDate;

	@Expose
	@SerializedName("SellerVatNumber")
	private String sellerVatNumber;

	@Expose
	@SerializedName("BuyerVatNumber")
	private String buyerVatNumber;

	@Expose
	@SerializedName("ErrorList")
	private List<ClearTaxErrorBean> errorList;

	@Expose
	@SerializedName("WarningList")
	private List<ClearTaxErrorBean> warningList;

	@Expose
	@SerializedName("Message")
	private String message;

	public String getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(final String deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public String getQrCodeStatus()
	{
		return qrCodeStatus;
	}

	public void setQrCodeStatus(final String qrCodeStatus)
	{
		this.qrCodeStatus = qrCodeStatus;
	}

	public String getInvoiceStatus()
	{
		return invoiceStatus;
	}

	public void setInvoiceStatus(final String invoiceStatus)
	{
		this.invoiceStatus = invoiceStatus;
	}

	public String getqRCode()
	{
		return qRCode;
	}

	public void setqRCode(final String qRCode)
	{
		this.qRCode = qRCode;
	}

	public String getRawQRCode()
	{
		return rawQRCode;
	}

	public void setRawQRCode(final String rawQRCode)
	{
		this.rawQRCode = rawQRCode;
	}

	public String getInvoiceXml()
	{
		return invoiceXml;
	}

	public void setInvoiceXml(final String invoiceXml)
	{
		this.invoiceXml = invoiceXml;
	}

	public String getuUID()
	{
		return uUID;
	}

	public void setuUID(final String uUID)
	{
		this.uUID = uUID;
	}

	public String getiCV()
	{
		return iCV;
	}

	public void setiCV(final String iCV)
	{
		this.iCV = iCV;
	}

	public String getpIH()
	{
		return pIH;
	}

	public void setpIH(final String pIH)
	{
		this.pIH = pIH;
	}

	public String getInvoiceHash()
	{
		return invoiceHash;
	}

	public void setInvoiceHash(final String invoiceHash)
	{
		this.invoiceHash = invoiceHash;
	}

	public String getInvoiceType()
	{
		return invoiceType;
	}

	public void setInvoiceType(final String invoiceType)
	{
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	public String getIssueDate()
	{
		return issueDate;
	}

	public void setIssueDate(final String issueDate)
	{
		this.issueDate = issueDate;
	}

	public String getSellerVatNumber()
	{
		return sellerVatNumber;
	}

	public void setSellerVatNumber(final String sellerVatNumber)
	{
		this.sellerVatNumber = sellerVatNumber;
	}

	public String getBuyerVatNumber()
	{
		return buyerVatNumber;
	}

	public void setBuyerVatNumber(final String buyerVatNumber)
	{
		this.buyerVatNumber = buyerVatNumber;
	}

	public List<ClearTaxErrorBean> getErrorList()
	{
		return errorList;
	}

	public void setErrorList(final List<ClearTaxErrorBean> errorList)
	{
		this.errorList = errorList;
	}

	public List<ClearTaxErrorBean> getWarningList()
	{
		return warningList;
	}

	public void setWarningList(final List<ClearTaxErrorBean> warningList)
	{
		this.warningList = warningList;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

}
