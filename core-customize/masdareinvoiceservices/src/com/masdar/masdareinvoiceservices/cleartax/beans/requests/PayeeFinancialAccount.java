package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PayeeFinancialAccount
{
	@Expose
	@SerializedName("PaymentNote")
	private DualLanguageBean paymentNote;




	/**
	 *
	 */
	public PayeeFinancialAccount(final DualLanguageBean paymentNote)
	{
		super();
		this.paymentNote = paymentNote;
	}

	/**
	 * @return the paymentNote
	 */
	public DualLanguageBean getPaymentNote()
	{
		return paymentNote;
	}

	/**
	 * @param paymentNote
	 *           the paymentNote to set
	 */
	public void setPaymentNote(final DualLanguageBean paymentNote)
	{
		this.paymentNote = paymentNote;
	}



}
