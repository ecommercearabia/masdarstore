package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PostalAddress
{

	@Expose
	@SerializedName("StreetName")
	private DualLanguageBean streetName;

	@Expose
	@SerializedName("AdditionalStreetName")
	private DualLanguageBean additionalStreetName;

	@Expose
	@SerializedName("BuildingNumber")
	private DualLanguageBean buildingNumber;

	@Expose
	@SerializedName("PlotIdentification")
	private DualLanguageBean plotIdentification;

	@Expose
	@SerializedName("CityName")
	private DualLanguageBean cityName;

	@Expose
	@SerializedName("CitySubdivisionName")
	private DualLanguageBean citySubdivisionName;

	@Expose
	@SerializedName("PostalZone")
	private String postalZone;

	@Expose
	@SerializedName("CountrySubentity")
	private DualLanguageBean countrySubentity;

	@Expose
	@SerializedName("Country")
	private ClearTaxCountryBean country;

	/**
	 * @return the streetName
	 */
	public DualLanguageBean getStreetName()
	{
		return streetName;
	}

	/**
	 * @param streetName
	 *           the streetName to set
	 */
	public void setStreetName(final DualLanguageBean streetName)
	{
		this.streetName = streetName;
	}

	/**
	 * @return the additionalStreetName
	 */
	public DualLanguageBean getAdditionalStreetName()
	{
		return additionalStreetName;
	}

	/**
	 * @param additionalStreetName
	 *           the additionalStreetName to set
	 */
	public void setAdditionalStreetName(final DualLanguageBean additionalStreetName)
	{
		this.additionalStreetName = additionalStreetName;
	}

	/**
	 * @return the buildingNumber
	 */
	public DualLanguageBean getBuildingNumber()
	{
		return buildingNumber;
	}

	/**
	 * @param buildingNumber
	 *           the buildingNumber to set
	 */
	public void setBuildingNumber(final DualLanguageBean buildingNumber)
	{
		this.buildingNumber = buildingNumber;
	}

	/**
	 * @return the plotIdentification
	 */
	public DualLanguageBean getPlotIdentification()
	{
		return plotIdentification;
	}

	/**
	 * @param plotIdentification
	 *           the plotIdentification to set
	 */
	public void setPlotIdentification(final DualLanguageBean plotIdentification)
	{
		this.plotIdentification = plotIdentification;
	}

	/**
	 * @return the cityName
	 */
	public DualLanguageBean getCityName()
	{
		return cityName;
	}

	/**
	 * @param cityName
	 *           the cityName to set
	 */
	public void setCityName(final DualLanguageBean cityName)
	{
		this.cityName = cityName;
	}

	/**
	 * @return the citySubdivisionName
	 */
	public DualLanguageBean getCitySubdivisionName()
	{
		return citySubdivisionName;
	}

	/**
	 * @param citySubdivisionName
	 *           the citySubdivisionName to set
	 */
	public void setCitySubdivisionName(final DualLanguageBean citySubdivisionName)
	{
		this.citySubdivisionName = citySubdivisionName;
	}

	/**
	 * @return the postalZone
	 */
	public String getPostalZone()
	{
		return postalZone;
	}

	/**
	 * @param postalZone
	 *           the postalZone to set
	 */
	public void setPostalZone(final String postalZone)
	{
		this.postalZone = postalZone;
	}

	/**
	 * @return the countrySubentity
	 */
	public DualLanguageBean getCountrySubentity()
	{
		return countrySubentity;
	}

	/**
	 * @param countrySubentity
	 *           the countrySubentity to set
	 */
	public void setCountrySubentity(final DualLanguageBean countrySubentity)
	{
		this.countrySubentity = countrySubentity;
	}

	/**
	 * @return the country
	 */
	public ClearTaxCountryBean getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final ClearTaxCountryBean country)
	{
		this.country = country;
	}



}
