package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Party
{
	@Expose
	@SerializedName("PartyLegalEntity")
	private PartyLegalEntity partyLegalEntity;

	@Expose
	@SerializedName("PartyTaxScheme")
	private PartyTaxScheme partyTaxScheme;

	@Expose
	@SerializedName("PartyIdentification")
	private PartyIdentification partyIdentification;

	@Expose
	@SerializedName("PostalAddress")
	private PostalAddress postalAddress;

	/**
	 * @return the partyLegalEntity
	 */
	public PartyLegalEntity getPartyLegalEntity()
	{
		return partyLegalEntity;
	}

	/**
	 * @param partyLegalEntity
	 *           the partyLegalEntity to set
	 */
	public void setPartyLegalEntity(final PartyLegalEntity partyLegalEntity)
	{
		this.partyLegalEntity = partyLegalEntity;
	}

	/**
	 * @return the partyTaxScheme
	 */
	public PartyTaxScheme getPartyTaxScheme()
	{
		return partyTaxScheme;
	}

	/**
	 * @param partyTaxScheme
	 *           the partyTaxScheme to set
	 */
	public void setPartyTaxScheme(final PartyTaxScheme partyTaxScheme)
	{
		this.partyTaxScheme = partyTaxScheme;
	}

	/**
	 * @return the partyIdentification
	 */
	public PartyIdentification getPartyIdentification()
	{
		return partyIdentification;
	}

	/**
	 * @param partyIdentification
	 *           the partyIdentification to set
	 */
	public void setPartyIdentification(final PartyIdentification partyIdentification)
	{
		this.partyIdentification = partyIdentification;
	}

	/**
	 * @return the postalAddress
	 */
	public PostalAddress getPostalAddress()
	{
		return postalAddress;
	}

	/**
	 * @param postalAddress
	 *           the postalAddress to set
	 */
	public void setPostalAddress(final PostalAddress postalAddress)
	{
		this.postalAddress = postalAddress;
	}



}
