package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClearTaxCountryBean {
	@Expose
	@SerializedName("IdentificationCode")
	private String identificationCode;

	/**
	 * @return the identificationCode
	 */
	public String getIdentificationCode()
	{
		return identificationCode;
	}

	/**
	 * @param identificationCode
	 *           the identificationCode to set
	 */
	public void setIdentificationCode(final String identificationCode)
	{
		this.identificationCode = identificationCode;
	}


}
