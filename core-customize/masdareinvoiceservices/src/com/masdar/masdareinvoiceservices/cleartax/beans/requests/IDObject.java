package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class IDObject
{
	@Expose
	@SerializedName("ID")
	private DualLanguageBean id;


	public IDObject(final DualLanguageBean id)
	{
		super();
		this.id = id;
	}

	public IDObject(final String englishAndArabic)
	{
		id = new DualLanguageBean(englishAndArabic);
	}

	public IDObject(final String english, final String arabic)
	{
		id = new DualLanguageBean(english, arabic);
	}

	/**
	 * @return the id
	 */
	public DualLanguageBean getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final DualLanguageBean id)
	{
		this.id = id;
	}


}
