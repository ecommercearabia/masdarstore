package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DeliveryBean
{

	// Format: YYYY-MM-DD
	@Expose
	@SerializedName("ActualDeliveryDate")
	private String actualDeliveryDate;

	// Format: YYYY-MM-DD
	@Expose
	@SerializedName("LatestDeliveryDate")
	private String latestDeliveryDate;

	/**
	 *
	 */
	public DeliveryBean(final String actualDeliveryDate, final String latestDeliveryDate)
	{
		super();
		this.actualDeliveryDate = actualDeliveryDate;
		this.latestDeliveryDate = latestDeliveryDate;
	}

	/**
	 * @return the actualDeliveryDate
	 */
	public String getActualDeliveryDate()
	{
		return actualDeliveryDate;
	}

	/**
	 * @param actualDeliveryDate
	 *           the actualDeliveryDate to set
	 */
	public void setActualDeliveryDate(final String actualDeliveryDate)
	{
		this.actualDeliveryDate = actualDeliveryDate;
	}

	/**
	 * @return the latestDeliveryDate
	 */
	public String getLatestDeliveryDate()
	{
		return latestDeliveryDate;
	}

	/**
	 * @param latestDeliveryDate
	 *           the latestDeliveryDate to set
	 */
	public void setLatestDeliveryDate(final String latestDeliveryDate)
	{
		this.latestDeliveryDate = latestDeliveryDate;
	}



}
