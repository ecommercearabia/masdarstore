package com.masdar.masdareinvoiceservices.cleartax.beans.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ClearTaxErrorBean
{
	@Expose
	@SerializedName("ErrorCode")
	private String errorCode;

	@Expose
	@SerializedName("ErrorMessage")
	private String errorMessage;

	@Expose
	@SerializedName("ErrorSource")
	private String errorSource;

	@Expose
	@SerializedName("Path")
	private String path;

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(final String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public String getErrorSource()
	{
		return errorSource;
	}

	public void setErrorSource(final String errorSource)
	{
		this.errorSource = errorSource;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(final String path)
	{
		this.path = path;
	}

}
