package com.masdar.masdareinvoiceservices.cleartax.beans.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.masdar.masdareinvoiceservices.cleartax.beans.helpers.ClearTaxUtils;


public class AllowanceCharge
{
	@Expose
	@SerializedName("ChargeIndicator")
	private boolean chargeIndicator;

	@Expose
	@SerializedName("BaseAmount")
	private PriceAmount baseAmount;

	@Expose
	@SerializedName("MultiplierFactorNumeric")
	private Double multiplierFactorNumeric;

	@Expose
	@SerializedName("Amount")
	private PriceAmount amount;

	@Expose
	@SerializedName("TaxCategory")
	private TaxCategory taxCategory;

	@Expose
	@SerializedName("AllowanceChargeReason")
	private DualLanguageBean allowanceChargeReason;

	@Expose
	@SerializedName("AllowanceChargeReasonCode")
	private String allowanceChargeReasonCode;

	/**
	 * @return the chargeIndicator
	 */
	public boolean isChargeIndicator()
	{
		return chargeIndicator;
	}

	/**
	 * @param chargeIndicator
	 *           the chargeIndicator to set
	 */
	public void setChargeIndicator(final boolean chargeIndicator)
	{
		this.chargeIndicator = chargeIndicator;
	}

	/**
	 * @return the baseAmount
	 */
	public PriceAmount getBaseAmount()
	{
		return baseAmount;
	}

	/**
	 * @param baseAmount
	 *           the baseAmount to set
	 */
	public void setBaseAmount(final PriceAmount baseAmount)
	{
		this.baseAmount = baseAmount;
	}

	/**
	 * @return the multiplierFactorNumeric
	 */
	public double getMultiplierFactorNumeric()
	{
		return multiplierFactorNumeric;
	}

	/**
	 * @param multiplierFactorNumeric
	 *           the multiplierFactorNumeric to set
	 */
	public void setMultiplierFactorNumeric(final double multiplierFactorNumeric)
	{
		this.multiplierFactorNumeric = ClearTaxUtils.formatDouble(multiplierFactorNumeric);
	}

	/**
	 * @return the amount
	 */
	public PriceAmount getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final PriceAmount amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the taxCategory
	 */
	public TaxCategory getTaxCategory()
	{
		return taxCategory;
	}

	/**
	 * @param taxCategory
	 *           the taxCategory to set
	 */
	public void setTaxCategory(final TaxCategory taxCategory)
	{
		this.taxCategory = taxCategory;
	}

	/**
	 * @return the allowanceChargeReason
	 */
	public DualLanguageBean getAllowanceChargeReason()
	{
		return allowanceChargeReason;
	}

	/**
	 * @param allowanceChargeReason
	 *           the allowanceChargeReason to set
	 */
	public void setAllowanceChargeReason(final DualLanguageBean allowanceChargeReason)
	{
		this.allowanceChargeReason = allowanceChargeReason;
	}

	/**
	 * @return the allowanceChargeReasonCode
	 */
	public String getAllowanceChargeReasonCode()
	{
		return allowanceChargeReasonCode;
	}

	/**
	 * @param allowanceChargeReasonCode
	 *           the allowanceChargeReasonCode to set
	 */
	public void setAllowanceChargeReasonCode(final String allowanceChargeReasonCode)
	{
		this.allowanceChargeReasonCode = allowanceChargeReasonCode;
	}



}
