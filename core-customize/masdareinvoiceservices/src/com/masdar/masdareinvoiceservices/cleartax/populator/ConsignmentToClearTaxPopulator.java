/*
d * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.cleartax.populator;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import com.google.common.base.Preconditions;
import com.masdar.core.enums.ShipmentType;
import com.masdar.masdareinvoiceservices.cleartax.beans.helpers.ClearTaxUtils;
import com.masdar.masdareinvoiceservices.cleartax.beans.helpers.OrderDiscountBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.AccountingParty;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.AllowanceCharge;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxCountryBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxGenerateEInvoiceBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxItem;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxPrice;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxQuantity;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxTaxTotal;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.DeliveryBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.DualLanguageBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.GenerateEInvoiceBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.IDObject;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.InnerPartyIdentification;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.InvoiceLine;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.LegalMonetaryTotal;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.NameValueBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.Party;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PartyIdentification;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PartyLegalEntity;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PartyTaxScheme;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PayeeFinancialAccount;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PaymentMeans;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PostalAddress;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.PriceAmount;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.TaxCategory;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.TaxScheme;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.TaxSubtotal;


/**
 *
 */
public class ConsignmentToClearTaxPopulator implements Populator<ConsignmentModel, ClearTaxGenerateEInvoiceBean>
{

	@Resource(name = "taxService")
	private TaxService taxService;

	private static final Logger LOG = LoggerFactory.getLogger(ConsignmentToClearTaxPopulator.class); // NOSONAR

	private static final String SAR = "SAR";
	private static final Locale LOCALE_AR = new Locale("ar");
	private static final Locale LOCALE_EN = Locale.ENGLISH;
	private static final PriceAmount ZERO_PRICE_AMOUNT = new PriceAmount(SAR, 0.0);
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss");

	private static final String DISCOUNT_LABEL = "Discount";
	private static final String DISCOUNT_LABEL_ARABIC = "خصم";

	private static final String DELIVERY_COST_LABEL = "Delivery Cost";
	private static final String DELIVERY_COST_LABEL_ARABIC = "تكلفة التوصيل";

	private static final String VAT_LABEL = "VAT";


	@Override
	public void populate(final ConsignmentModel source, final ClearTaxGenerateEInvoiceBean target) throws ConversionException
	{
		// Validate

		final GenerateEInvoiceBean eInvoice = new GenerateEInvoiceBean();

		populateCommonData(source, eInvoice);

		populateEInvoiceLines(source, eInvoice);

		populatePaymentMeans(source, eInvoice);

		populateCustomFields(source, target);

		target.seteInvoice(eInvoice);
	}

	/**
	 *
	 */
	private void populateCustomFields(final ConsignmentModel source, final ClearTaxGenerateEInvoiceBean eInvoice)
	{
		final Map<String, String> customFields = new HashMap<>();
		final String CONSIGNMENT_CODE = "Consignment Code";
		final String ORDER_CODE = "Order Code";
		customFields.put(CONSIGNMENT_CODE, source.getCode());
		customFields.put(ORDER_CODE, source.getOrder().getCode());
		eInvoice.setCustomFields(customFields);
	}

	private void populatePaymentMeans(final ConsignmentModel consignment, final GenerateEInvoiceBean eInvoice)
	{

		final AbstractOrderModel order = consignment.getOrder();

		final String paymentMeansCode = getPaymentMeansCodeByOrderPaymentType(order);


		final PaymentMeans paymentMeans = new PaymentMeans();
		/*-
		 * Mandatory. The means, expressed as code, for how a payment is expected to be or has been settled.
		 * A Payment instruction (BG-16) shall specify the Payment means type code (BT-81).
		 * Payment means in an invoice MUST be coded using UNCL4461 code list.
		 * Payment means code (BT-81) in an invoice must contain one of the
		 * values:
		 * 	10 - In cash,
		 * 	30 - Credit,
		 * 	42 - Payment to bank account ,
		 * 	48 - Bank card ,
		 * 	1 - Instrument not defined (Free text) .
		 * In the simplified tax invoice and associated credit notes and debit notes (KSA-2, position 1 and 2 = 02) this
		 * value is optional.
		 * InstructionNote ​​ -
		 */
		paymentMeans.setPaymentMeansCode(paymentMeansCode);

		/*-
		 * Conditional. Required in case of Debit and credit note (invoice type code (BT-3) is equal to 383 or 381).
		 * Reasons for issuance of credit / debit note as per Article 40 (paragraph 1) of KSA VAT regulations, a Credit
		 * and Debit Note is issued for these 4 instances:
		 * 	- Cancellation or suspension of the supplies after its occurrence either wholly or partially
		 * 	- In case of essential change or amendment in the supply, which leads to the change of the VAT due
		 * 	- Amendment of the supply value which is pre-agreed upon between the supplier and consumer
		 * 	- In case of goods or services refund.
		 *
		 * Invoices types
		 * 	- 388 Tax Invoice
		 *		- 383 Debit Note
		 *		- 381 Credit Note
		 */
		paymentMeans.setInstructionNote(null);

		//Optional. Payee Financial Account Payment Note object. Default: null
		paymentMeans.setPayeeFinancialAccount(new PayeeFinancialAccount(new DualLanguageBean("")));
		eInvoice.setPaymentMeans(Arrays.asList(paymentMeans));
	}

	private String getPaymentMeansCodeByOrderPaymentType(final AbstractOrderModel order)
	{
		final CheckoutPaymentType orderPaymentType = order.getPaymentType();
		Preconditions.checkNotNull(orderPaymentType, "Order Payment Type Cannot be null");
		Preconditions.checkArgument(Strings.isNotBlank(orderPaymentType.getCode()), "Order Payment Type Cannot be null");
		/*-
		 * values:
		 * 	10 - In cash,
		 * 	30 - Credit,
		 * 	42 - Payment to bank account ,
		 * 	48 - Bank card ,
		 * 	1 - Instrument not defined (Free text) .
		 */
		String paymentMeansCode = null;
		if (CheckoutPaymentType.PAY_IN_STORE.equals(orderPaymentType) || CheckoutPaymentType.COD.equals(orderPaymentType))
		{
			paymentMeansCode = "10";
		}
		if (CheckoutPaymentType.ACCOUNT.equals(orderPaymentType))
		{
			paymentMeansCode = "30";
		}
		if (CheckoutPaymentType.BANK.equals(orderPaymentType))
		{
			paymentMeansCode = "42";
		}
		if (CheckoutPaymentType.CARD.equals(orderPaymentType) || CheckoutPaymentType.CCOD.equals(orderPaymentType))
		{
			paymentMeansCode = "48";
		}
		if (CheckoutPaymentType.ESAL.equals(orderPaymentType) || CheckoutPaymentType.ESAL_B2C.equals(orderPaymentType))
		{
			paymentMeansCode = "42";
		}

		Preconditions.checkArgument(Strings.isNotBlank(paymentMeansCode), "Could not decide Payment Means code");

		return paymentMeansCode;
	}

	/**
	 *
	 */
	private void populateEInvoiceLines(final ConsignmentModel source, final GenerateEInvoiceBean eInvoice)
	{
		final List<InvoiceLine> lines = eInvoice.getInvoiceLine() == null ? new ArrayList<>() : eInvoice.getInvoiceLine();

		int idCounter = 1;
		for (final var entryModel : source.getConsignmentEntries())
		{
			final AbstractOrderEntryModel orderEntry = entryModel.getOrderEntry();

			Preconditions.checkArgument(entryModel.getQuantity() > 0, "ConsingmentEntryModel.quanitity should be more than 0");
			Preconditions.checkArgument(orderEntry.getQuantity() > 0, "ConsingmentEntryModel.quanitity should be more than 0");
			final InvoiceLine line = new InvoiceLine();


			final ClearTaxItem item = createClearTaxItemFromEntry(entryModel);


			/*
			 * Mandatory. Each Invoice line (BG-25) shall have an Invoice line identifier (BT-126). A unique identifier for
			 * the individual line within the Invoice. An Invoice shall have at least one Invoice line (BG-25)
			 */
			line.setId(idCounter);

			/*
			 * Mandatory. Each Invoice line (BG-25) shall contain the Item name (BT-153).
			 */
			line.setItem(item);

			line.setPrice(createClearTaxPriceFromEntry(entryModel));

			/*
			 * Mandatory. Each Invoice line shall have an Invoiced quantity. The quantity of items (goods or services) that
			 * is charged in the Invoice line. All the document amounts and quantities must be positive.
			 */
			final ClearTaxQuantity invoicedQuantity = createClearTaxQuantityForClearTaxItem(entryModel);
			line.setInvoicedQuantity(invoicedQuantity);

			/* Mandatory. Invoice line extension amount object. */
			line.setLineExtensionAmount(createLineExtensionAmountForClearTaxItem(entryModel));
			line.setTaxTotal(createClearTaxTaxBeanForEntry(entryModel));
			line.setAllowanceCharge(createAllowanceChargeForEntry(entryModel));

			lines.add(line);

			idCounter += 1;
		}

		eInvoice.setInvoiceLine(lines);


		populateDeliveryCostIfNeccessery(source, eInvoice, idCounter);

		populateTaxesAndDiscounts(source, eInvoice);
	}


	/**
	 *
	 */
	private void populateDeliveryCostIfNeccessery(final ConsignmentModel consignment, final GenerateEInvoiceBean eInvoice,
			final int idCounter)
	{
		final AbstractOrderModel order = consignment.getOrder();
		if (!consignment.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			return;
		}

		final DualLanguageBean lineItemName = new DualLanguageBean(DELIVERY_COST_LABEL, DELIVERY_COST_LABEL_ARABIC);
		final IDObject buyersItemIdentification = new IDObject(lineItemName);
		final double deliveryCost = order.getDeliveryCost().doubleValue();

		final PriceAmount lineExtensionAmount = new PriceAmount(SAR, deliveryCost);
		final PriceAmount taxAmount = new PriceAmount(SAR, deliveryCost * 15 / 100);
		final PriceAmount roundingAmount = lineExtensionAmount.add(taxAmount);


		final InvoiceLine line = new InvoiceLine();

		final ClearTaxItem item = new ClearTaxItem();
		item.setBuyersItemIdentification(buyersItemIdentification);
		item.setName(lineItemName);
		item.setSellersItemIdentification(buyersItemIdentification);
		item.setStandardItemIdentification(buyersItemIdentification);
		item.setClassifiedTaxCategory(getTaxCategory());

		final ClearTaxPrice price = new ClearTaxPrice();
		price.setBaseQuantity(new ClearTaxQuantity(null, String.valueOf(1)));

		price.setPriceAmount(lineExtensionAmount);
		price.setAllowanceCharge(null);

		final ClearTaxQuantity invoicedQuantity = new ClearTaxQuantity();
		invoicedQuantity.setUnitCode(null);
		invoicedQuantity.setValue(String.valueOf(1));

		final ClearTaxTaxTotal taxTotal = new ClearTaxTaxTotal();
		taxTotal.setTaxAmount(taxAmount);
		taxTotal.setRoundingAmount(roundingAmount);

		line.setId(idCounter);
		line.setItem(item);
		line.setInvoicedQuantity(invoicedQuantity);
		line.setPrice(price);
		line.setLineExtensionAmount(lineExtensionAmount);
		line.setTaxTotal(taxTotal);

		eInvoice.getInvoiceLine().add(line);



	}

	/**
	 *
	 */
	private double populateAllowanceCharges(final GenerateEInvoiceBean eInvoice, final OrderDiscountBean orderDiscount)
	{
		if (orderDiscount == null)
		{
			return 0.0d;
		}

		final List<AllowanceCharge> allowanceCharges = new ArrayList<>();
		final AllowanceCharge allowanceCharge = new AllowanceCharge();
		allowanceCharge.setChargeIndicator(false);
		//		allowanceCharge.setBaseAmount(new PriceAmount(SAR, lineExtensionAmountBeforeOrderDiscount));
		allowanceCharge.setAmount(new PriceAmount(SAR, orderDiscount.getDiscountValue()));
		//		allowanceCharge.setMultiplierFactorNumeric(orderDiscount.getDiscountPercentage());
		allowanceCharge.setAllowanceChargeReason(new DualLanguageBean(DISCOUNT_LABEL, DISCOUNT_LABEL_ARABIC));
		allowanceCharge.setAllowanceChargeReasonCode(null);
		allowanceCharge.setTaxCategory(getTaxCategory());
		allowanceCharges.add(allowanceCharge);
		eInvoice.setAllowanceCharge(allowanceCharges);


		return orderDiscount.getDiscountValue();

	}

	private void populateTaxesAndDiscounts(final ConsignmentModel consignment, final GenerateEInvoiceBean eInvoice)
	{

		final List<InvoiceLine> invoiceLines = eInvoice.getInvoiceLine();
		final LegalMonetaryTotal legalMonetaryTotal = new LegalMonetaryTotal();

		final double lineExtensionAmountBeforeOrderDiscount = invoiceLines.stream()
				.map(e -> Double.valueOf(e.getLineExtensionAmount().getValue())).reduce(0.0d, Double::sum).doubleValue();

		final OrderDiscountBean orderDiscount = getOrderDiscountPercentage(consignment, lineExtensionAmountBeforeOrderDiscount);
		final double totalDiscounts = populateAllowanceCharges(eInvoice, orderDiscount);
		final double lineExtensionTotalAfterDiscount = lineExtensionAmountBeforeOrderDiscount - totalDiscounts;

		final Double taxAmount = lineExtensionTotalAfterDiscount * 15 / 100;

		final PriceAmount taxPriceAmount = new PriceAmount(SAR, taxAmount);
		final PriceAmount allowanceTotalAmount = new PriceAmount(SAR, totalDiscounts);
		final PriceAmount taxablePriceAmount = new PriceAmount(SAR, lineExtensionTotalAfterDiscount);
		final PriceAmount lineExtensionAmount = new PriceAmount(SAR, lineExtensionAmountBeforeOrderDiscount);
		final PriceAmount totalAmountWithTax = taxablePriceAmount.add(taxPriceAmount);


		legalMonetaryTotal.setLineExtensionAmount(lineExtensionAmount);
		legalMonetaryTotal.setAllowanceTotalAmount(allowanceTotalAmount);
		legalMonetaryTotal.setTaxExclusiveAmount(taxablePriceAmount);
		legalMonetaryTotal.setTaxInclusiveAmount(totalAmountWithTax);


		if (isCreditCustomer(consignment))
		{
			legalMonetaryTotal.setPayableAmount(ZERO_PRICE_AMOUNT);
			legalMonetaryTotal.setPrepaidAmount(totalAmountWithTax);
		}
		else
		{
			legalMonetaryTotal.setPayableAmount(totalAmountWithTax);
			legalMonetaryTotal.setPrepaidAmount(ZERO_PRICE_AMOUNT);
		}

		final List<ClearTaxTaxTotal> clearTaxTaxTotals = new ArrayList<>();

		final ClearTaxTaxTotal clearTaxTaxTotal = new ClearTaxTaxTotal();
		clearTaxTaxTotal.setTaxAmount(taxPriceAmount);
		clearTaxTaxTotal.setRoundingAmount(totalAmountWithTax);

		final List<TaxSubtotal> taxSubtotals = new ArrayList<>();
		final TaxSubtotal taxSubTotal = new TaxSubtotal();

		taxSubTotal.setTaxableAmount(taxablePriceAmount);
		taxSubTotal.setTaxAmount(taxPriceAmount);
		final TaxCategory taxCategory = getTaxCategory();
		taxSubTotal.setTaxCategory(taxCategory);
		taxSubtotals.add(taxSubTotal);

		clearTaxTaxTotal.setTaxSubtotal(taxSubtotals);


		clearTaxTaxTotals.add(clearTaxTaxTotal);

		eInvoice.setTaxTotal(clearTaxTaxTotals);

		eInvoice.setLegalMonetaryTotal(legalMonetaryTotal);

	}

	private TaxCategory getTaxCategory()
	{
		final TaxCategory taxCategory = new TaxCategory();
		final TaxScheme taxScheme = new TaxScheme(VAT_LABEL);
		taxCategory.setTaxScheme(taxScheme);
		taxCategory.setId("S");
		taxCategory.setPercent("15.0");
		return taxCategory;
	}

	/**
		 *
		 */
	private OrderDiscountBean getOrderDiscountPercentage(final ConsignmentModel consignment,
			final double lineExtensionBeforeOrderDiscount)
	{
		final AbstractOrderModel order = consignment.getOrder();
		if (Collections.isEmpty(order.getGlobalDiscountValues()))
		{
			return null;
		}


		final double orderDiscount = order.getGlobalDiscountValues().stream().map(e -> e.getAppliedValue())
				.reduce(0.0d, Double::sum).doubleValue();

		if (orderDiscount <= 0.0f)
		{
			return null;
		}
		final double discountPercentage = (orderDiscount / order.getSubtotal()) * 100;

		double totalAmountOFConsignmnet = lineExtensionBeforeOrderDiscount;
		if (consignment.getCode().equals(order.getDeliveryCostAppliedOnConsignment()))
		{
			// The discounts are applied on the order excluding the delivery cost
			totalAmountOFConsignmnet = totalAmountOFConsignmnet - order.getDeliveryCost().doubleValue();
		}

		final double totalDiscount = (totalAmountOFConsignmnet * (discountPercentage / 100));
		final double formatDouble = ClearTaxUtils.formatDouble(totalDiscount);

		return new OrderDiscountBean(discountPercentage, formatDouble);
	}


	/**
	 *
	 */
	private List<AllowanceCharge> createAllowanceChargeForEntry(final ConsignmentEntryModel entryModel)
	{
		if (Collections.isEmpty(entryModel.getOrderEntry().getDiscountValues()))
		{
			return null;
		}

		final AbstractOrderEntryModel orderEntry = entryModel.getOrderEntry();

		//		final Double basePrice = orderEntry.getBasePrice();

		// Discount amount for order entry
		final Double discountAmount = orderEntry.getDiscountValues().stream().map(e -> e.getAppliedValue()).reduce(0.0d,
				Double::sum);

		final double discountAmountPer1Qty = discountAmount / orderEntry.getQuantity();

		//		final double discountPercenteage = (discountAmountPer1Qty / basePrice) * 100.0d;

		final double totalDiscount = discountAmountPer1Qty * entryModel.getQuantity();
		//		final double totalPriceWithoutDiscount = basePrice * entryModel.getQuantity();

		final AllowanceCharge allowanceCharge = new AllowanceCharge();
		allowanceCharge.setChargeIndicator(false);
		//		allowanceCharge.setBaseAmount(new PriceAmount(SAR, totalPriceWithoutDiscount));
		allowanceCharge.setAmount(new PriceAmount(SAR, totalDiscount));
		//		allowanceCharge.setMultiplierFactorNumeric(discountPercenteage);
		allowanceCharge.setAllowanceChargeReason(new DualLanguageBean(DISCOUNT_LABEL, DISCOUNT_LABEL_ARABIC));
		allowanceCharge.setAllowanceChargeReasonCode(null);
		return Arrays.asList(allowanceCharge);
	}

	/**
	 *
	 */
	private ClearTaxTaxTotal createClearTaxTaxBeanForEntry(final ConsignmentEntryModel entryModel)
	{
		final AbstractOrderEntryModel orderEntry = entryModel.getOrderEntry();

		final ClearTaxTaxTotal clearTaxTaxTotal = new ClearTaxTaxTotal();
		final double netBasePriceForWithDiscount = orderEntry.getTotalPrice() / orderEntry.getQuantity();

		final double netBasePriceForConsignmentEntry = netBasePriceForWithDiscount * entryModel.getQuantity();
		final double taxValueAmount = (netBasePriceForConsignmentEntry * 15) / 100;
		final double roundingAmount = netBasePriceForConsignmentEntry + taxValueAmount;

		final PriceAmount taxAmount = new PriceAmount(SAR, taxValueAmount);
		final PriceAmount roundingAmountObj = new PriceAmount(SAR, roundingAmount);

		clearTaxTaxTotal.setRoundingAmount(roundingAmountObj);
		clearTaxTaxTotal.setTaxAmount(taxAmount);

		return clearTaxTaxTotal;
	}



	/**
	 *
	 */
	private PriceAmount createLineExtensionAmountForClearTaxItem(final ConsignmentEntryModel entryModel)
	{
		final PriceAmount priceAmount = new PriceAmount();
		/* Conditional. Required if parent context exists. Currency for invoice line net amount */
		priceAmount.setCurrencyID(SAR);

		/*
		 * Mandatory. Each Invoice line shall have an Invoice line net amount
		 *
		 * The total amount of the Invoice line, including allowances (discounts). It is the item net price multiplied
		 * with the quantity. The amount is “net” without VAT. Invoice line net amount must equal (Invoiced quantity *
		 * (Item net price / item price base quantity))- Sum of invoice line allowance amount The allowed maximum number
		 * of decimals for the Invoice line net amount is 2. All the document amounts and quantities must be positive.
		 */
		final AbstractOrderEntryModel orderEntry = entryModel.getOrderEntry();
		final double amountPer1Qty = orderEntry.getTotalPrice() / orderEntry.getQuantity();
		priceAmount.setValue(amountPer1Qty * entryModel.getQuantity());
		return priceAmount;
	}

	private ClearTaxQuantity createClearTaxQuantityForClearTaxItem(final ConsignmentEntryModel entryModel)
	{

		final ClearTaxQuantity invoicedQuantity = new ClearTaxQuantity();
		/* Optional. The unit of measure that applies to the invoiced quantity. */
		invoicedQuantity.setUnitCode(null);

		/*
		 * Mandatory. Each Invoice line shall have an Invoiced quantity. The quantity of items (goods or services) that is
		 * charged in the Invoice line. All the document amounts and quantities must be positive.
		 */
		invoicedQuantity.setValue(String.valueOf(entryModel.getQuantity()));
		return invoicedQuantity;
	}

	/**
	 *
	 */
	private ClearTaxPrice createClearTaxPriceFromEntry(final ConsignmentEntryModel consignmentEntryModel)
	{
		final AbstractOrderEntryModel orderEntry = consignmentEntryModel.getOrderEntry();
		final ProductModel product = orderEntry.getProduct();
		final ClearTaxPrice clearTaxPrice = new ClearTaxPrice();


		// AllowanceCharge here means discount
		final AllowanceCharge allowanceCharge = new AllowanceCharge();
		final PriceAmount baseAmount = new PriceAmount();
		/*
		 * Conditional. Required if parent context exists. Currency for item gross prices. This is in the context of the
		 * line item price level (not the line item level).
		 */
		baseAmount.setCurrencyID(SAR);
		/*
		 * Conditional. The unit price, exclusive of VAT, before subtracting Item price discount. All the document amounts
		 * and quantities must be positive.
		 */
		baseAmount.setValue(0.0d);

		allowanceCharge.setBaseAmount(baseAmount);
		/*-
		 * Conditional. An indicator that this AllowanceCharge describes a discount. The value of this tag indicating
		 * discount (Allowance) must be ""false"". This is in the context of the line item price level (not the line item
		 * level). Charge on price level (BG-29) is NOT allowed.
		 * Only value 'false' allowed.
		 */
		allowanceCharge.setChargeIndicator(false);
		allowanceCharge.setAmount(baseAmount);


		final PriceAmount priceAmount = new PriceAmount();
		/* Conditional. Required if parent context exists. Currency for item net price */
		priceAmount.setCurrencyID(SAR);

		/*-
		 * Mandatory. The price of an item, exclusive of VAT, after subtracting item price discount.
		 * If the gross price and the discount exist (in price level allowance charge), the Item net price has to be equal with the Item
		 * gross price minus the Item price discount.
		 * Item net price must have a maximum two decimals.
		 * All the document amounts and quantities must be positive.
		 */
		priceAmount.setValue(orderEntry.getBasePrice());


		final ClearTaxQuantity baseQuantity = new ClearTaxQuantity();
		/* Optional. Item price base quantity unit code. */
		baseQuantity.setUnitCode(product.getUnitOfMeasure());
		/*
		 * Optional. The number of item units to which the price applies. All the document amounts and quantities must be
		 * positive.
		 */
		baseQuantity.setValue("1");

		/* Mandatory. Each Invoice line shall contain the Item net price */
		clearTaxPrice.setPriceAmount(priceAmount);

		/* Conditional */
		clearTaxPrice.setAllowanceCharge(null);

		/* Optional. Invoice line price base quantity. */
		clearTaxPrice.setBaseQuantity(baseQuantity);

		return clearTaxPrice;
	}

	private ClearTaxItem createClearTaxItemFromEntry(final ConsignmentEntryModel entryModel)
	{
		final AbstractOrderEntryModel orderEntry = entryModel.getOrderEntry();
		final ProductModel product = orderEntry.getProduct();
		final ClearTaxItem item = new ClearTaxItem();
		/*
		 * Mandatory. Each Invoice line (BG-25) shall contain the Item name (BT-153). The description of goods or services
		 * as per Article 53 of the VAT Implementing Regulation.
		 */
		final DualLanguageBean itemName = new DualLanguageBean(product.getName(LOCALE_EN), product.getName(LOCALE_AR));
		item.setName(itemName);

		/* Optional. An identifier, assigned by the Buyer, for the item. */
		item.setBuyersItemIdentification(new IDObject(itemName));

		/* Optional. An identifier, assigned by the Seller, for the item. */
		final IDObject itemIdentification = new IDObject(product.getCode());
		item.setSellersItemIdentification(itemIdentification);

		/*
		 * "Optional. An item identifier based on a registered scheme. This should include the product code type and the
		 * actual code. This list includes UPC (11 digit, 12 digit, 13 digit EAN), GTIN (14 digit), Customs HS Code and
		 * multiple other codes"
		 */
		item.setStandardItemIdentification(itemIdentification);


		// This whole part needs to be revisited,
		//   				 classifiedTaxCategory.id should be selected after checking the tax row applied on the product
		final TaxCategory classifiedTaxCategory = new TaxCategory();

		//		Mandatory element. Use “VAT”
		final TaxScheme taxScheme = new TaxScheme(VAT_LABEL);

		final ProductTaxGroup productTaxGroup = product.getEurope1PriceFactory_PTG();
		final TaxModel taxForCode = getTaxService().getTaxForCode(productTaxGroup.getCode()); // NOTE(Husam): this will not work unless the tax group and tax row have the same code

		/*-
		 * Mandatory.
		 * Each Invoice line (BG-25) shall be categorized with an Invoiced item VAT category code (BT-151).
		 * The VAT category code for the invoiced item. An Invoice that contains a VAT breakdown group (BG-23) with a VAT
		 * category code (BT-118) ""Not subject to VAT"" shall not contain an Invoice line (BG-25) where the Invoiced item
		 * VAT category code (BT-151) is not ""Not subject to VAT"".
		 * Invoice tax categories MUST be coded using UNCL5305
		 * code list VAT category code must contain one of the values (S, Z, E, O).
		 *
		 * - S : Standard rate
		 * - Z : Zero rated goods
		 * - E : Exempt from Tax
		 * - O : Services outside scope of tax
		 * Note(Husam): For more info https://docs.peppol.eu/poacc/billing/3.0/codelist/UNCL5305/
		 */
		classifiedTaxCategory.setId("S");

		/*
		 * Mandatory. The VAT rate, represented as percentage that applies to the invoiced item as per Article 53 of the
		 * VAT Implementing Regulation In an Invoice line (BG-25) where the Invoiced item VAT category code (BT-151) is
		 * ""Zero rated"" the Invoiced item VAT rate (BT-152) shall be 0 (zero). In an Invoice line (BG-25) where the
		 * Invoiced item VAT category code (BT-151) is ""Exempt from VAT"", the Invoiced item VAT rate (BT-152) shall be 0
		 * (zero). An Invoice line (BG-2525) where the VAT category code (BT-151151) is ""Not subject to VAT"" shall not
		 * contain an Invoiced item VAT rate (BT-152152). The VAT rates (BT-96119, BT-152) must be from 0.00 to 100.00,
		 * with maximum two decimals. Only numerals are accepted, the percentage symbol (%) is not allowed. All the
		 * document amounts and quantities must be positive.
		 */
		classifiedTaxCategory.setPercent(String.valueOf(taxForCode.getValue()));
		classifiedTaxCategory.setTaxScheme(taxScheme);

		item.setClassifiedTaxCategory(classifiedTaxCategory);
		return item;
	}

	/**
	 *
	 */
	private void populateCommonData(final ConsignmentModel source, final GenerateEInvoiceBean eInvoice)
	{

		final LocalDateTime ldt = LocalDateTime.now();
		final LocalDateTime plusDay = ldt.plusDays(1);
		// Optional. Default value: "2.1"
		eInvoice.setUblVersionID("2.1");

		// Mandatory. Business process type. Identifies a user-defined profile of the customization of UBL being used. Default value: "reporting:1.0"
		eInvoice.setProfileID("reporting:1.0");

		// Mandatory. Invoice number. A unique identification of the Invoice.
		eInvoice.setId(new DualLanguageBean(source.getInvoiceNumber()));

		final NameValueBean invoiceTypeCode = createInvoiceTypeCode(source);
		eInvoice.setInvoiceTypeCode(invoiceTypeCode);

		/*-
		 * Mandatory. Invoice issue date (Gregorian calendar). The date when the Invoice was issued as per Article 53 of
		 * the VAT Implementing Regulation. The document issue date must be less or equal to the current date.
		 * Format: YYYY-MM-DD
		 */
		eInvoice.setIssueDate(ldt.format(DATE_FORMAT));

		/*-
		 * Mandatory. Invoice issue time. The time when the invoice was issued.
		 * Format: HH:mm:ss (24 hr format)
		 */
		eInvoice.setIssueTime(ldt.format(TIME_FORMAT));

		// "Conditional. Required in case of Credit Debit Notes. Array of Delivery Objects. Default: []"
		eInvoice.setDelivery(Arrays.asList(new DeliveryBean(ldt.format(DATE_FORMAT), plusDay.format(DATE_FORMAT))));


		/*-
		 * Conditional. Required only in case the invoice type is Debit Note or Credit Note.
		 * Not required if the invoice type is Tax Invoice.
		 *
		 * 	Array of Billing Reference Objects.
		 */
		eInvoice.setBillingReference(null);

		// Optional. Order Reference Object. Default: null
		eInvoice.setOrderReference(null);

		// Optional. Array of Contract Document Reference Objects. Default: null
		eInvoice.setContractDocumentReference(null);

		/*-
		 * Mandatory. Invoice currency code.
		 * The currency in which all Invoice amounts are given, except for the Total VAT amount in accounting currency.
		 * Default: "SAR".
		 */
		eInvoice.setDocumentCurrencyCode(SAR);

		/*-
		 * Mandatory. Tax currency code.
		 * The currency used for VAT accounting and reporting purposes as accepted or required in the country of the Seller.
		 * Shall be used in combination with the Total VAT amount in accounting currency (BT-111).
		 * Default: "SAR"
		 */
		eInvoice.setTaxCurrencyCode(SAR);

		// Mandatory. Accounting Supplier Party Object
		eInvoice.setAccountingSupplierParty(createAccountingSupplierParty(source));

		// Mandatory. Accounting Customer Party Object
		final boolean B2C = SiteChannel.B2C.equals(source.getOrder().getSite().getChannel());
		final AccountingParty createAccountingCustomerParty = B2C ? createAccountingB2CCustomerParty(source)
				: createAccountingB2BCustomerParty(source);
		eInvoice.setAccountingCustomerParty(createAccountingCustomerParty);

		// Mandatory. Array of Invoice Line Item Objects
		eInvoice.setInvoiceLine(null);

		// Conditional. Required in case of invoice level discount. Allowance Charge Object
		eInvoice.setAllowanceCharge(null);

		// Mandatory. Array of Tax Total Objects
		eInvoice.setTaxTotal(null);

		// Mandatory. Legal Monetary Total Object
		eInvoice.setLegalMonetaryTotal(null);


		/*-
		 * Conditional. Required in case of Standard Tax Invoice, Standard Credit Note, Standard Debit Note, Simplified
		 * Credit Note and Simplified Debit Note. Not required in case of Simplified Tax Invoice.
		 * Array of Payment Means Object
		 */
		eInvoice.setPaymentMeans(null);


		/*-
		 * Optional. Invoice note. A textual note that gives unstructured information that is relevant to the Invoice as a
		 * whole.
		 * Default: null
		 */
		eInvoice.setNote(null);

		/*-
		 * Read Only. Optional. Array of Additional Document Reference Objects.
		 * This array should contain:
		 * 	1 Invoice counter value object (ICV both standard and simplified)
		 * 	1 Previous invoice hash object (PIH both standard and simplified)
		 * 	1 QR code object (QR only for simplified)
		 *
		 * Default: Clear will try to generate Invoice counter value,
		 * 	Previous invoice hash and QR Code based on the configuration.
		 *
		 * QR is required in Phase I only for Simplified documents. ICV and PIH are required in Phase II.
		 */

		// Read Only. Optional. Signature Object. Default: Clear will try to generate a cryptographic stamp based on the configuration.
	}

	/**
	 *
	 */
	private AccountingParty createAccountingB2BCustomerParty(final ConsignmentModel source)
	{

		final B2BUnitModel unit = source.getOrder().getUnit();
		Preconditions.checkArgument(Strings.isNotBlank(unit.getId()), "B2BUnit Id (Commercial Regisration Number) Cannot be Empty");
		//		Preconditions.checkArgument(Strings.isNotBlank(unit.getVatID()), "B2BUnit Vat Id Cannot be Empty");

		final AccountingParty customerParty = new AccountingParty();

		// Mandatory. Customer Party Object.
		final Party party = new Party();

		// Mandatory. Seller Postal Address object.
		final AbstractOrderModel order = source.getOrder();


		String unitNameEn = unit.getLocName(LOCALE_EN);
		String unitNameAr = unit.getLocName(LOCALE_AR);
		if (Strings.isBlank(unitNameEn))
		{
			unitNameEn = unit.getUid();
		}
		if (Strings.isBlank(unitNameAr))
		{
			unitNameAr = unit.getUid();
		}

		final PartyLegalEntity partyLegalEntity = new PartyLegalEntity(unitNameEn, unitNameAr);

		final PostalAddress postalAddress = createPostalAddress(order.getDeliveryAddress());

		final PartyTaxScheme partyTaxScheme = new PartyTaxScheme();
		partyTaxScheme.setCompanyId(unit.getVatID());
		partyTaxScheme.setTaxScheme(new TaxScheme(VAT_LABEL));


		final PartyIdentification partyIdentification = new PartyIdentification();
		final InnerPartyIdentification id = new InnerPartyIdentification();
		id.setSchemeID("CRN"); // Commercial Registration number
		id.setValue(unit.getId());

		partyIdentification.setId(id);


		party.setPartyTaxScheme(partyTaxScheme);
		party.setPartyIdentification(partyIdentification);
		party.setPostalAddress(postalAddress);
		party.setPartyLegalEntity(partyLegalEntity);
		customerParty.setParty(party);
		return customerParty;
	}


	private AddressModel getDeliveryAddressForPOS(final ConsignmentModel consignmnet)
	{

		for (final ConsignmentEntryModel entry : consignmnet.getConsignmentEntries())
		{
			if (entry.getOrderEntry() != null && entry.getOrderEntry().getDeliveryPointOfService() != null
					&& entry.getOrderEntry().getDeliveryPointOfService().getAddress() != null)
			{
				return entry.getOrderEntry().getDeliveryPointOfService().getAddress();
			}
		}
		return null;
	}

	/**
	 *
	 */
	private AccountingParty createAccountingB2CCustomerParty(final ConsignmentModel source)
	{
		final AccountingParty supplierParty = new AccountingParty();

		// Mandatory. Customer Party Object.
		final Party party = new Party();

		// Optional
		final PartyLegalEntity partyLegalEntity = new PartyLegalEntity(source.getOrder().getUser().getDisplayName());

		// Mandatory. Seller Postal Address object.
		AddressModel deliveryAddress = source.getOrder().getDeliveryAddress();
		if (ShipmentType.PICKUP_IN_STORE.equals(source.getOrder().getSelectedShipmentType()))
		{
			deliveryAddress = getDeliveryAddressForPOS(source);
		}

		Preconditions.checkNotNull(deliveryAddress, "Delivery Adderss Cannot be null for Simplefied Tax Invoices (B2C)");

		final PostalAddress postalAddress = createPostalAddress(deliveryAddress);

		party.setPostalAddress(postalAddress);
		party.setPartyLegalEntity(partyLegalEntity);
		supplierParty.setParty(party);
		return supplierParty;
	}

	/**
	 *
	 */
	private AccountingParty createAccountingSupplierParty(final ConsignmentModel source)
	{
		final BaseStoreModel store = source.getOrder().getStore();

		final AccountingParty supplierParty = new AccountingParty();

		// Mandatory. Supplier Party Object.
		final Party party = new Party();

		// Mandatory. Seller Party Legal Entity Object
		final PartyLegalEntity partyLegalEntity = new PartyLegalEntity(store.getSupplierName(LOCALE_EN),
				store.getSupplierName(LOCALE_AR));

		// Mandatory. Party Identification object.
		final PartyIdentification partyIdentification = createSupplierPartyIdentification(store);

		// Mandatory. Party Tax Scheme  object.
		final PartyTaxScheme partyTaxScheme = createSupplierPartyTaxScheme(store);

		// Mandatory. Seller Postal Address object.
		final PostalAddress postalAddress = createPostalAddress(store.getSupplierAddressInfo());


		party.setPartyLegalEntity(partyLegalEntity);
		party.setPartyIdentification(partyIdentification);
		party.setPartyTaxScheme(partyTaxScheme);
		party.setPostalAddress(postalAddress);
		supplierParty.setParty(party);
		return supplierParty;
	}

	/**
	 *
	 */
	private PostalAddress createPostalAddress(final AddressModel address)
	{
		Preconditions.checkNotNull(address, "Address Cannot be null");
		/*-
		 * Seller address must contain
		 * 	- additional number
		 *		- street name
		 *		- building
		 * 	- number
		 * 	- postal
		 * 	- code
		 * 	- city
		 * 	- Neighborhood
		 * 	- country code.
		 * For more information please access this link: https://www.address.gov.sa/en/a ddress-format/overview
		 */

		final PostalAddress postalAddress = new PostalAddress();

		// Mandatory. Seller address line 1 - the main address line in an address.
		postalAddress.setStreetName(new DualLanguageBean(address.getStreetname()));

		/*-
		 * Optional. Seller address line 2 - an additional address line in an address that can be used to give further
		 * details supplementing the main line.
		 * Default: null
		 */
		postalAddress.setAdditionalStreetName(new DualLanguageBean(address.getStreetname()));

		/*-
		 * Mandatory. Seller address building number.
		 * The seller address building number must contain 4 digits..
		 */
		final String building = getBuildingNumberFromAddress(address);
		postalAddress.setBuildingNumber(new DualLanguageBean(building));

		/*-
		 * Mandatory. Seller address additional number.
		 * Seller Address Additional number must be 4 digits.
		 */
		postalAddress.setPlotIdentification(new DualLanguageBean(building));

		/* Mandatory. The common name of the city, town or village, where the Seller's address is located. */
		postalAddress.setCityName(new DualLanguageBean(address.getCity().getName(LOCALE_EN), address.getCity().getName(LOCALE_AR)));

		/*
		 * Mandatory. The name of the subdivision of the Seller city, town, or village in which its address is located,
		 * such as the name of its district or borough.
		 */
		postalAddress.setCitySubdivisionName(new DualLanguageBean(address.getTown()));

		/* Mandatory. Seller post code. Seller postal code (BT-38) must be 5 digits. */
		postalAddress.setPostalZone("12211");

		/* Mandatory. Seller country subdivision */
		postalAddress.setCountrySubentity(
				new DualLanguageBean(address.getCountry().getName(LOCALE_EN), address.getCountry().getName(LOCALE_AR)));

		// Mandatory. Seller country object.
		final ClearTaxCountryBean country = new ClearTaxCountryBean();
		/*
		 * Mandatory. Seller country code. Country codes in an invoice MUST be coded using ISO code list 3166-1. The
		 * seller address country code (BT-40) must be “SA”.
		 */
		country.setIdentificationCode("SA");
		postalAddress.setCountry(country);
		return postalAddress;
	}

	private String getBuildingNumberFromAddress(final AddressModel address)
	{
		if (Strings.isBlank(address.getBuilding()))
		{
			return "0000";
		}
		try
		{
			final Integer valueOf = Integer.valueOf(address.getBuilding());
			return String.format("%04d", valueOf);
		}
		catch (final NumberFormatException e)
		{
			throw new IllegalArgumentException("Building Number on address must be integer");
		}
	}

	private PartyTaxScheme createSupplierPartyTaxScheme(final BaseStoreModel store)
	{
		final PartyTaxScheme partyTaxScheme = new PartyTaxScheme();

		/*-
		 * Mandatory. Seller VAT identifier - taxpayer entity Also known as Seller VAT identification number.
		 * Validation Must contain 15 digits. The first and the last digits are “3”.
		 */
		partyTaxScheme.setCompanyId(store.getSupplierVatId());

		/*
		 * Conditional. The invoice must contain the seller VAT registration number (BT-31) and/or seller group VAT
		 * registration number (KSA-18). Seller VAT Object.
		 */
		final TaxScheme taxScheme = new TaxScheme(VAT_LABEL);


		partyTaxScheme.setTaxScheme(taxScheme);

		return partyTaxScheme;
	}

	private PartyIdentification createSupplierPartyIdentification(final BaseStoreModel store)
	{
		final PartyIdentification partyIdentification = new PartyIdentification();
		final InnerPartyIdentification id = new InnerPartyIdentification();
		/*-
		 * Mandatory in Phase 2 -
		 * ONLY one of the Other seller ID from the below list:
		 * 	- Commercial registration number with "CRN" as schemeID
		 * 	- Momra license with "MOM" as schemeID
		 * 	- MLSD license with "MLS" as schemeID
		 * 	- Sagia license with "SAG" as schemeID
		 * 	- Other OD with "OTH" as schemeID
		 */
		id.setValue(store.getCompanyCommercialRegistrationNumber());


		/*-
		 * Mandatory in Phase 2 - Other seller ID is one of the list:
		 * 	"CRN" - Commercial registration number
		 *		"MOM" - Momra license
		 * 	"MLS" - MLSD license
		 * 	"SAG" - Sagia license
		 * 	"OTH" - Other ID
		 */
		id.setSchemeID("CRN");
		partyIdentification.setId(id);
		return partyIdentification;
	}

	public static String formatString(final String format, final Object... params)
	{
		return MessageFormatter.arrayFormat(format, params).getMessage();
	}

	/**
	 *
	 */
	private NameValueBean createInvoiceTypeCode(final ConsignmentModel source)
	{
		final NameValueBean result = new NameValueBean();

		/*-
		 * Mandatory. A code of the invoice subtype and invoices transactions. The invoice transaction code must exist and
		 * respect the following structure: NNPNESB where -
		 * NN (positions 1 and 2) = invoice subtype:
		 * 	- 01 for tax invoice
		 * 	- 02 for simplified tax invoice.
		 * P (position 3) = 3rd Party invoice transaction,
		 * 	- 0 for false,
		 * 	- 1 for true
		 * N (position 4) = Nominal invoice transaction,
		 *    - 0 for false,
		 *    - 1 for true
		 * E (position 5) = Exports invoice transaction,
		 *    - 0 for false,
		 *    - 1 for true
		 * S (position 6) = Summary invoice transaction,
		 *    - 0 for false,
		 *    - 1 for true
		 * B (position 7) = Self billed invoice. Self-billing is not allowed (KSA-2, position 7 cannot be ""1"")
		 *
		 * for export invoices (KSA-2, position 5 = 1).
		 *
		 * For simplified tax invoices and associated credit notes and debit notes (KSA-2, position 1 and 2 = 02),
		 * only the following are accepted:
		 * 	- third party (KSA-2, position 3 = 1),
		 * 	- nominal supply (KSA-2, position 4 = 1)
		 * 	- summary transactions (KSA-2,, position 6 = 1) XC
		 */


		final int NN = SiteChannel.B2C.equals(source.getOrder().getSite().getChannel()) ? 2 : 1;
		final int P = 0;
		final int N = 0;
		final int E = 0;
		final int S = 0;
		final int B = 0;

		/*-
		 * 	1. final String InvoiceTypeCodeNameFormat = "0%d%d%d%d%d%d"; // NOSONAR
		 *	   2. final String format = String.format(InvoiceTypeCodeNameFormat, NN, P, N, E, S, B);// NOSONAR
		 */
		final String formatString = formatString("0{}{}{}{}{}{}", NN, P, N, E, S, B);

		result.setName(formatString);

		/*-
		 * Mandatory. A code specifying the functional type of the Invoice. The invoice type code must be equal to one of
		 * value from the subset of UN/CEFACT code list 1001, D.16B agreed for KSA electronic invoices.
		 * Allowed:
		 * 	- 388 Tax Invoice
		 *		- 383 Debit Note
		 *		- 381 Credit Note
		 */
		result.setValue("388");

		return result;
	}


	private boolean isCreditCustomer(final ConsignmentModel consignment)
	{
		final AbstractOrderModel order = consignment.getOrder();
		if (SiteChannel.B2C.equals(order.getSite().getChannel()))
		{
			return false;
		}

		final B2BUnitModel unit = order.getUnit();

		return !unit.isCashCustomer();
	}

	/**
	 * @return the taxService
	 */
	public TaxService getTaxService()
	{
		return taxService;
	}
}
