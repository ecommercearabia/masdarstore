package com.masdar.masdareinvoiceservices.cleartax.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxGenerateEInvoiceBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.responses.GenerateEInvoiceResponse;
import com.masdar.masdareinvoiceservices.cleartax.exceptions.ClearTaxException;
import com.masdar.masdareinvoiceservices.cleartax.service.ClearTaxService;
import com.masdar.masdareinvoiceservices.cleartax.utils.ClearTaxHttpClientUtil;


@Component
public class DefaultClearTaxService implements ClearTaxService
{

	private static final String GENERATE_E_INVOICE_REQUEST_URL = "v2/einvoices/generate";
	private static final String PRINT_E_INVOICE_REQUEST_URL = "v2/einvoices/print";
	private static final String X_CLEARTAX_AUTH_TOKEN_PARAM = "x-cleartax-auth-token";
	private static final String ACCEPT_LANGUAGE_PARAM = "accept-language";
	private static final String ACCEPT_LANGUAGE_VALUE = "en";
	private static final String VAT_PARAM = "vat";

	private static final Gson GSON = (new GsonBuilder().serializeNulls()).create();

	@Override
	public GenerateEInvoiceResponse generateEInvoice(final ClearTaxGenerateEInvoiceBean generateEInvoiceBean, final String baseURL,
			final String authToken, final String vat) throws ClearTaxException
	{
		final Map<String, String> headersMap = getHeadersMap(authToken, vat);
		headersMap.put("Content-Type", "application/json");

		GenerateEInvoiceResponse response = null;

		response = ClearTaxHttpClientUtil.post(baseURL + GENERATE_E_INVOICE_REQUEST_URL, headersMap, generateEInvoiceBean,
				GenerateEInvoiceResponse.class);

		if (response.getErrorList() != null && response.getErrorList().size() > 0)
		{
			final String responseJson = GSON.toJson(response);
			final String errorJson = GSON.toJson(response.getErrorList());
			final String requestJson = GSON.toJson(generateEInvoiceBean);
			throw ClearTaxException.getClientException(requestJson, responseJson, errorJson);
		}
		return response;
	}



	private Map<String, String> getHeadersMap(final String authToken, final String vat)
	{
		final Map<String, String> headersMap = new HashMap<>();
		headersMap.put(ACCEPT_LANGUAGE_PARAM, ACCEPT_LANGUAGE_VALUE);
		headersMap.put(X_CLEARTAX_AUTH_TOKEN_PARAM, authToken);
		headersMap.put(VAT_PARAM, vat);
		return headersMap;
	}






	@Override
	public byte[] getPDFEInvoice(final String baseURL, final String authToken, final String vat, final String documentNumber,
			final String documentType, final String taxpayerId, final String documentDate, final String templateId)
			throws ClearTaxException
	{
		final Map<String, String> headersMap = getHeadersMap(authToken, vat);
		final Map<String, Object> paramsMap = new HashMap<>();
		paramsMap.put("documentNumber", documentNumber);
		paramsMap.put("documentType", documentType);
		paramsMap.put("taxpayerId", taxpayerId);
		paramsMap.put("documentDate", documentDate);
		paramsMap.put("templateId", templateId);

		final String URL = baseURL + PRINT_E_INVOICE_REQUEST_URL;
		return ClearTaxHttpClientUtil.getPDF(URL, headersMap, paramsMap);
	}
}
