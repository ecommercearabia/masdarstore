package com.masdar.masdareinvoiceservices.cleartax.service;

import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxGenerateEInvoiceBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.responses.GenerateEInvoiceResponse;
import com.masdar.masdareinvoiceservices.cleartax.exceptions.ClearTaxException;


public interface ClearTaxService
{

	public GenerateEInvoiceResponse generateEInvoice(final ClearTaxGenerateEInvoiceBean generateEInvoiceBean, final String baseURL,
			final String authToken, final String vat) throws ClearTaxException;

	public byte[] getPDFEInvoice(final String baseURL, final String authToken, final String vat, final String documentNumber,
			final String documentType, final String taxpayerId, final String documentDate, final String templateId)
			throws ClearTaxException;
}
