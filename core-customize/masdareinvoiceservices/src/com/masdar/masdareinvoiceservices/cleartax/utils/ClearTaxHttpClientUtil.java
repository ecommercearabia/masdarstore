package com.masdar.masdareinvoiceservices.cleartax.utils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.masdar.masdareinvoiceservices.cleartax.exceptions.ClearTaxException;

import kong.unirest.Body;
import kong.unirest.GetRequest;
import kong.unirest.Header;
import kong.unirest.HttpRequest;
import kong.unirest.HttpResponse;
import kong.unirest.RequestBodyEntity;
import kong.unirest.Unirest;


public class ClearTaxHttpClientUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(ClearTaxHttpClientUtil.class);
	private static final Gson GSON = (new GsonBuilder().serializeNulls()).create();

	private ClearTaxHttpClientUtil()
	{

	}

	private static String convertRequestToString(final HttpRequest<?> request)
	{
		final StringBuilder sb = new StringBuilder();
		sb.append(request.getHttpMethod().toString());
		sb.append(" ");
		sb.append(request.getUrl());
		sb.append("\n");

		for (final Header a : request.getHeaders().all())
		{
			sb.append(a.getName() + ":" + a.getValue() + "\n");
		}

		final Object body = request.getBody().orElse(null);
		if (body instanceof Body)
		{

			final Body body2 = (Body) body;
			sb.append(body2.uniPart().getValue());
		}
		return sb.toString();
	}

	public static <T> T post(final String URL, final Map<String, String> headers, final Object body, final Type clazz)
			throws ClearTaxException
	{

		final String json = GSON.toJson(body);
		final RequestBodyEntity request = Unirest.post(URL).headers(headers).body(json);

		HttpResponse<String> response;
		final String requestJSON = convertRequestToString(request);

		try
		{
			response = request.asString();
		}
		catch (final Exception e)
		{
			throw ClearTaxException.getClientException(requestJSON, e.getMessage());
		}

		if (response.isSuccess())
		{
			return GSON.fromJson(response.getBody(), clazz);
		}

		// Try to serialize if the request actually received by the other end
		try
		{
			return GSON.fromJson(response.getBody(), clazz);
		}
		catch (final JsonSyntaxException e)
		{
			LOG.info(response.getBody());
			throw ClearTaxException.getClientException(requestJSON, e.getMessage());
		}

	}

	public static byte[] getPDF(final String URL, Map<String, String> headers, final Map<String, Object> params)
			throws ClearTaxException
	{
		if (headers == null || headers.isEmpty())
		{
			headers = new HashMap<>();
		}
		headers.put("content-type", "application/pdf");
		headers.put("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.put("Pragma", "no-cache");
		headers.put("Expires", "0");

		final GetRequest request = Unirest.get(URL).headers(headers).queryString(params);

		HttpResponse<byte[]> response;
		final String requestJSON = convertRequestToString(request);

		try
		{
			response = request.asBytes();
		}
		catch (final Exception e)
		{
			throw ClearTaxException.getClientException(requestJSON, e.getMessage());
		}

		return response.getBody();
	}
}
