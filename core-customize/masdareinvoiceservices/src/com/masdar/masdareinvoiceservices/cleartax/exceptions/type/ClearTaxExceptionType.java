package com.masdar.masdareinvoiceservices.cleartax.exceptions.type;

/**
 *
 * @author husam.dababneh@erabia.com
 *
 */
public enum ClearTaxExceptionType
{
	// @formatter:off
	UNKNOWN("Unknown Error Code", "Unknown Error Code"),
	CLIENT_ERROR("Client Error", "Client Error");
	// @formatter:on

	private String code;
	private String description;

	private ClearTaxExceptionType(final String code, final String description)
	{
		this.code = code;
		this.description = description;
	}

	public String getCode()
	{
		return code;
	}

	public String getDescription()
	{
		return description;
	}

	public static ClearTaxExceptionType getExceptionTypeByCode(final String code)
	{
		for (int i = 0; i < ClearTaxExceptionType.values().length; i++)
		{
			final ClearTaxExceptionType tamaraExceptionType = ClearTaxExceptionType.values()[i];
			if (tamaraExceptionType.getCode().equals(code))
			{
				return tamaraExceptionType;
			}
		}
		return UNKNOWN;
	}
}
