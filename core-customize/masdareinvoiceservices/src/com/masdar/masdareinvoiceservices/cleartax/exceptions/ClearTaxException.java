package com.masdar.masdareinvoiceservices.cleartax.exceptions;

import com.masdar.masdareinvoiceservices.cleartax.exceptions.type.ClearTaxExceptionType;


/**
 *
 * @author husam.dababneh@erabia.com
 *
 */
public class ClearTaxException extends Exception
{
	private static final long serialVersionUID = -2727878348941788892L;
	private final ClearTaxExceptionType exceptionType;
	private final String exceptionMessage;
	private final String requestData;
	private final String responseData;

	private ClearTaxException(final ClearTaxExceptionType exceptionType, final String exceptionMessage, final String requestData,
			final String responseData)
	{
		super(exceptionMessage);
		this.exceptionType = exceptionType;
		this.responseData = responseData;
		this.requestData = requestData;
		this.exceptionMessage = exceptionMessage;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public ClearTaxExceptionType getExceptionType()
	{
		return exceptionType;
	}

	public String getExceptionMessage()
	{
		return exceptionMessage;
	}

	public String getRequestData()
	{
		return requestData;
	}

	public String getResponseData()
	{
		return responseData;
	}

	public static ClearTaxException getClientException(final String request, final String response)
	{
		return new ClearTaxException(ClearTaxExceptionType.CLIENT_ERROR, ClearTaxExceptionType.CLIENT_ERROR.getDescription(),
				request, response);

	}

	public static ClearTaxException getClientException(final String request, final String response, final String message)
	{
		return new ClearTaxException(ClearTaxExceptionType.CLIENT_ERROR, message, request, response);

	}
}
