/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.exception;

import com.masdar.masdareinvoiceservices.exception.type.EInvoiceExceptionType;


/**
 *
 */
public class EInvoiceException extends Exception
{

	public EInvoiceException(final EInvoiceExceptionType exceptionType, final String exceptionMessage, final String requestData,
			final String responseData)
	{
		super(exceptionMessage);
		//		this.exceptionType = exceptionType;
		//		this.responseData = responseData;
		//		this.requestData = requestData;
		//		this.exceptionMessage = exceptionMessage;
	}

}
