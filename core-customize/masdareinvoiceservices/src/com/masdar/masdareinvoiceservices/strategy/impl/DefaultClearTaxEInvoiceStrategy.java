/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.strategy.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Base64;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.masdar.masdareinvoiceservices.cleartax.beans.requests.ClearTaxGenerateEInvoiceBean;
import com.masdar.masdareinvoiceservices.cleartax.beans.responses.GenerateEInvoiceResponse;
import com.masdar.masdareinvoiceservices.cleartax.exceptions.ClearTaxException;
import com.masdar.masdareinvoiceservices.cleartax.service.ClearTaxService;
import com.masdar.masdareinvoiceservices.enums.EInvoiceProviderType;
import com.masdar.masdareinvoiceservices.exception.EInvoiceException;
import com.masdar.masdareinvoiceservices.model.ClearTaxEInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.model.EInvoicesHistoryEntryModel;
import com.masdar.masdareinvoiceservices.strategy.EInvoiceStrategy;


/**
 *
 */
public class DefaultClearTaxEInvoiceStrategy implements EInvoiceStrategy<ClearTaxEInvoiceProviderModel>
{
	private static final String PNG_MIME = "image/x-png";
	private static final String XML_MIME = "application/xml";
	private static final String PDF_MIME = "application/pdf";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultClearTaxEInvoiceStrategy.class);

	private static final Gson GSON = (new GsonBuilder()).create();

	@Resource(name = "clearTaxService")
	private ClearTaxService clearTaxService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "clearTaxGenerateEInvoiceBeanConvertor")
	private Converter<ConsignmentModel, ClearTaxGenerateEInvoiceBean> clearTaxGenerateEInvoiceBeanConvertor;


	@Override
	public void generateEInvoiceForConsigmnet(final ConsignmentModel consignment,
			final ClearTaxEInvoiceProviderModel providerModel) throws EInvoiceException
	{
		validateProvider(providerModel);

		final ClearTaxGenerateEInvoiceBean clearTaxGenerateEInvoiceBean = getClearTaxGenerateEInvoiceBeanConvertor()
				.convert(consignment);

		clearTaxGenerateEInvoiceBean.setDeviceId(providerModel.getDeviceId());

		final String request = GSON.toJson(clearTaxGenerateEInvoiceBean);

		GenerateEInvoiceResponse generateEInvoiceResponse;
		try
		{
			generateEInvoiceResponse = getClearTaxService().generateEInvoice(clearTaxGenerateEInvoiceBean,
					providerModel.getBaseURL(), providerModel.getAuthToken(), providerModel.getVatNumber());
		}
		catch (final ClearTaxException e)
		{
			saveRequest(request, e.getMessage(), false, consignment);
			throw new EInvoiceException(null, e.getMessage(), request, request);
		}


		final String response = GSON.toJson(generateEInvoiceResponse);

		if (!"GENERATED".equals(generateEInvoiceResponse.getStatus()))
		{
			saveRequest(request, response, false, consignment);
			throw new EInvoiceException(null, "EInvoice Was not generated Please take a look at the logs", request, request);
		}

		saveRequest(request, response, true, consignment);
		cleanUpMediaByAttributeName(consignment, consignment.INVOICEQRCODE);
		cleanUpMediaByAttributeName(consignment, consignment.EINVOICEEMMBEDXML);


		final byte[] qrCodeByteArray = Base64.getDecoder().decode(generateEInvoiceResponse.getqRCode());
		final MediaModel qrCodeMedia = createMediaFile(
				"InvoiceQrCode-cons-" + consignment.getInvoiceNumber() + "-" + LocalDateTime.now().toString() + ".png", PNG_MIME,
				qrCodeByteArray);
		consignment.setInvoiceQRCode(qrCodeMedia);


		final byte[] xmlByteArray = Base64.getDecoder().decode(generateEInvoiceResponse.getInvoiceXml());
		final MediaModel xmlMedia = createMediaFile(
				"InvoiceXml-cons-" + consignment.getInvoiceNumber() + "-" + LocalDateTime.now().toString() + ".xml", XML_MIME,
				xmlByteArray);
		consignment.setEInvoiceEmmbedXML(xmlMedia);


		consignment.setEInvocieDocumentType(generateEInvoiceResponse.getInvoiceType());
		consignment.setEInvocieTaxPayerID(generateEInvoiceResponse.getSellerVatNumber());
		consignment.setEInvoiceNumber(generateEInvoiceResponse.getInvoiceNumber());
		consignment.setEInvoiceDocumentDate(generateEInvoiceResponse.getIssueDate());
		getModelService().save(consignment);

	}

	/**
	 * @param consignment
	 *
	 */
	private void saveRequest(final String request, final String response, final boolean success,
			final ConsignmentModel consignment)
	{
		final EInvoicesHistoryEntryModel entry = getModelService().create(EInvoicesHistoryEntryModel.class);
		entry.setRequest(request);
		entry.setResponse(response);
		entry.setEInvoiceProviderType(EInvoiceProviderType.CLEARTAX);
		entry.setSuccess(success);
		entry.setConsignmentCode(consignment.getCode());
		entry.setConsignment(consignment);
		getModelService().save(entry);
	}

	/**
	 *
	 */
	private void cleanUpMediaByAttributeName(final ConsignmentModel consignment, final String attributeName)
	{
		final Object property = consignment.getProperty(attributeName);
		if (property == null)
		{
			return;
		}

		if (!(property instanceof MediaModel))
		{
			LOG.warn("Property {} on Consinment is not Media Type", attributeName);
			return;
		}

		final MediaModel media = (MediaModel) property;
		getModelService().remove(media);
		getModelService().refresh(consignment);
	}

	@Override
	public byte[] getPDFForEInvoice(final ConsignmentModel consignment, final ClearTaxEInvoiceProviderModel providerModel)
			throws EInvoiceException
	{
		validateProviderForPDF(providerModel);
		validateConsignment(consignment);
		try
		{
			return getClearTaxService().getPDFEInvoice(providerModel.getBaseURL(), providerModel.getAuthToken(),
					providerModel.getVatNumber(), consignment.getEInvoiceNumber(), consignment.getEInvocieDocumentType(),
					consignment.getEInvocieTaxPayerID(), consignment.getEInvoiceDocumentDate(), providerModel.getPdfTemplateId());
		}
		catch (final ClearTaxException e)
		{
			throw new EInvoiceException(null, e.getMessage(), null, null);
		}

	}

	@Override
	public void savePDFForEInvoiceOnConsignment(final ConsignmentModel consignment,
			final ClearTaxEInvoiceProviderModel providerModel) throws EInvoiceException
	{
		final byte[] pdfForEInvoice = this.getPDFForEInvoice(consignment, providerModel);
		final MediaModel pdfMedia = createMediaFile(
				"ProviderGeneratedInvoicePDF-cons-" + consignment.getInvoiceNumber() + "-" + LocalDateTime.now().toString() + ".pdf",
				PDF_MIME, pdfForEInvoice);

		cleanUpMediaByAttributeName(consignment, consignment.PROVIDERGENERATEDEINVOICE);

		consignment.setProviderGeneratedEInvoice(pdfMedia);
		getModelService().save(consignment);
		getModelService().refresh(consignment);
	}

	/**
	 *
	 */
	private void validateConsignment(final ConsignmentModel consignment)
	{
		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvoiceNumber()),
				"EInvoiceNumber For consignment Cannot be null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvocieDocumentType()),
				"EInvocieDocumentType For consignment Cannot be null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvocieTaxPayerID()),
				"EInvocieTaxPayerID For consignment Cannot be null");

		Preconditions.checkArgument(Strings.isNotBlank(consignment.getEInvoiceDocumentDate()),
				"EInvoiceDocumentDate For consignment Cannot be null");
	}

	/**
	 *
	 */
	private void validateProviderForPDF(final ClearTaxEInvoiceProviderModel providerModel)
	{
		validateProvider(providerModel);
		Preconditions.checkArgument(Strings.isNotBlank(providerModel.getPdfTemplateId()),
				"PdfTemplateId For consignment Cannot be null");
	}



	/**
	 * @param byteArray
	 *
	 */
	private MediaModel createMediaFile(final String mediaName, final String mime, final byte[] byteArray)
	{
		final MediaModel media = getModelService().create(MediaModel.class);
		media.setCode(mediaName);
		media.setMime(mime);
		media.setRealFileName(mediaName);

		try
		{
			media.setCatalogVersion(getCatalogVersionService().getCatalogVersion("Default", "Online"));
			getModelService().save(media);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();

		try (InputStream dataStream = new ByteArrayInputStream(byteArray);)
		{
			getMediaService().setStreamForMedia(media, dataStream, mediaName, mime, mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate Media from byteArray", e);
			return null;
		}
		return media;
	}

	/**
	 *
	 */
	private void validateProvider(final ClearTaxEInvoiceProviderModel providerModel)
	{
		Preconditions.checkNotNull(providerModel, "ClearTaxEInvoiceProviderModel Cannot be null");
		Preconditions.checkArgument(Strings.isNotBlank(providerModel.getAuthToken()), "Auth Token Cannot be null");
		Preconditions.checkArgument(Strings.isNotBlank(providerModel.getBaseURL()), "BaseURL Cannot be null");
		Preconditions.checkArgument(Strings.isNotBlank(providerModel.getDeviceId()), "DeviceId Cannot be null");
		Preconditions.checkArgument(Strings.isNotBlank(providerModel.getVatNumber()), "Vat Number Cannot be null");
	}

	/**
	 * @return the clearTaxService
	 */
	public ClearTaxService getClearTaxService()
	{
		return clearTaxService;
	}

	/**
	 * @return the clearTaxGenerateEInvoiceBeanConvertor
	 */
	public Converter<ConsignmentModel, ClearTaxGenerateEInvoiceBean> getClearTaxGenerateEInvoiceBeanConvertor()
	{
		return clearTaxGenerateEInvoiceBeanConvertor;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * Gets the {@link MediaFolderModel} to save the generated Media
	 *
	 * @return the {@link MediaFolderModel}
	 */
	protected MediaFolderModel getDocumentMediaFolder()
	{
		return getMediaService().getFolder("documents");
	}

	/**
	 * @return the mediaService
	 */
	public MediaService getMediaService()
	{
		return mediaService;
	}


}
