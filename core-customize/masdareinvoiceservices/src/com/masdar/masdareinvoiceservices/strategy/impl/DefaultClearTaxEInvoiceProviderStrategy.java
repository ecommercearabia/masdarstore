/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.masdar.masdareinvoiceservices.model.ClearTaxEInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.service.EInvoiceProviderService;
import com.masdar.masdareinvoiceservices.strategy.EInvoiceProviderStrategy;


/**
 *
 */
public class DefaultClearTaxEInvoiceProviderStrategy implements EInvoiceProviderStrategy
{

	@Resource(name = "einvoiceProviderService")
	public EInvoiceProviderService einvoiceProviderService;

	@Override
	public Optional<EInvoiceProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getEinvoiceProviderService().getActive(baseStoreModel, ClearTaxEInvoiceProviderModel.class);
	}

	@Override
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore()
	{
		return getEinvoiceProviderService().getActiveProviderByCurrentStore(ClearTaxEInvoiceProviderModel.class);
	}

	/**
	 * @return the einvoiceProviderService
	 */
	public EInvoiceProviderService getEinvoiceProviderService()
	{
		return einvoiceProviderService;
	}

}
