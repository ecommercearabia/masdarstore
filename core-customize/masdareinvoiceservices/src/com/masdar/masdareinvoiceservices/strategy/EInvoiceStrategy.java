/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.strategy;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.masdar.masdareinvoiceservices.exception.EInvoiceException;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;


/**
 *
 */
public interface EInvoiceStrategy<T extends EInvoiceProviderModel>
{
	/**
	 * Generate EInvoice and saves the response on the consignment
	 *
	 */
	public void generateEInvoiceForConsigmnet(ConsignmentModel consignment, T providerModel) throws EInvoiceException;


	/**
	 * Get EInvoice as PDF
	 *
	 */
	public byte[] getPDFForEInvoice(ConsignmentModel consignment, T providerModel) throws EInvoiceException;

	/**
	 * Get EInvoice as PDF
	 *
	 */
	public void savePDFForEInvoiceOnConsignment(ConsignmentModel consignment, T providerModel) throws EInvoiceException;

}
