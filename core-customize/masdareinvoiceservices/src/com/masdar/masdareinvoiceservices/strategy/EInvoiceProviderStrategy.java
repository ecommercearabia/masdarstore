/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;


/**
 *
 */
public interface EInvoiceProviderStrategy
{
	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active provider
	 */
	public Optional<EInvoiceProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore();
}
