/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;


/**
 *
 */
public interface EInvoiceProviderService
{
	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<EInvoiceProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active
	 */
	public Optional<EInvoiceProviderModel> getActive(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<EInvoiceProviderModel> getActiveByBaseStoreCode(String baseStorecode, final Class<?> providerClass);

	/**
	 * Gets the active provider by current store.
	 *
	 * @return the active provider by current store
	 */
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore(final Class<?> providerClass);
}
