/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.service.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.masdar.masdareinvoiceservices.dao.EInvoiceProviderDao;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;
import com.masdar.masdareinvoiceservices.service.EInvoiceProviderService;


/**
 *
 */
public class DefaultEInvoiceProviderService implements EInvoiceProviderService
{

	/** The otp provider dao map. */
	@Resource(name = "einvoiceProviderDaoMap")
	private Map<Class<?>, EInvoiceProviderDao> einvoiceProviderDaoMap;

	/** The otp provider dao map. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant CMSSITE_UID_MUSTN_T_BE_NULL. */
	private static final String BASE_STORE_UID_MUSTN_T_BE_NULL = "BaseStoreModel  mustn't be null or empty";

	/** The Constant CMSSITE_MUSTN_T_BE_NULL. */
	private static final String BASE_STORE_MUSTN_T_BE_NULL = "BaseStoreModel mustn't be null or empty";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	/** The Constant PROVIDER_DAO_NOT_FOUND. */
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";


	@Override
	public Optional<EInvoiceProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);
		return dao.get().get(code);
	}



	@Override
	public Optional<EInvoiceProviderModel> getActive(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkNotNull(baseStoreModel, BASE_STORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);
		return dao.get().getActive(baseStoreModel);
	}



	@Override
	public Optional<EInvoiceProviderModel> getActiveByBaseStoreCode(final String baseStorecode, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(baseStorecode), BASE_STORE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);
		return dao.get().getActiveByBaseStoreCode(baseStorecode);
	}



	@Override
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EInvoiceProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);
		return dao.get().getActiveProviderByCurrentBaseStore();
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the dao
	 */
	protected Optional<EInvoiceProviderDao> getDao(final Class<?> providerClass)
	{
		final EInvoiceProviderDao dao = getEinvoiceProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}

	/**
	 * @return the einvoiceProviderDaoMap
	 */
	public Map<Class<?>, EInvoiceProviderDao> getEinvoiceProviderDaoMap()
	{
		return einvoiceProviderDaoMap;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


}
