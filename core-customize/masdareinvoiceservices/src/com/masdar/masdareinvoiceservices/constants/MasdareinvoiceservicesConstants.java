/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.constants;

/**
 * Global class for all Masdareinvoiceservices constants. You can add global constants for your extension into this class.
 */
public final class MasdareinvoiceservicesConstants extends GeneratedMasdareinvoiceservicesConstants
{
	public static final String EXTENSIONNAME = "masdareinvoiceservices";

	private MasdareinvoiceservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
