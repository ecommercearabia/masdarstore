/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.masdar.masdareinvoiceservices.model.ClearTaxEInvoiceProviderModel;


/**
 *
 */
public class DefaultClearTaxEInvoiceProviderDao extends DefaultEInvoiceProviderDao<ClearTaxEInvoiceProviderModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultClearTaxEInvoiceProviderDao.class);

	public DefaultClearTaxEInvoiceProviderDao()
	{
		super(ClearTaxEInvoiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ClearTaxEInvoiceProviderModel._TYPECODE;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

}
