/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdareinvoiceservices.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.masdar.masdareinvoiceservices.dao.EInvoiceProviderDao;
import com.masdar.masdareinvoiceservices.model.EInvoiceProviderModel;


/**
 *
 */
public abstract class DefaultEInvoiceProviderDao<T extends EInvoiceProviderModel> extends DefaultGenericDao<T>
		implements EInvoiceProviderDao
{
	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant SITES. */
	private static final String SITES = "sites";

	/** The Constant CODE. */
	private static final String CODE = "code";

	/** The Constant ACTIVE. */
	private static final String ACTIVE = "active";

	/**
	 *
	 */
	public DefaultEInvoiceProviderDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected abstract String getModelName();


	/**
	 * Gets the Logger .
	 *
	 * @return the Logger
	 */
	protected abstract Logger getLogger();

	@Override
	public Optional<EInvoiceProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<EInvoiceProviderModel> find = (List<EInvoiceProviderModel>) find(params);
		final Optional<EInvoiceProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? findFirst : Optional.empty();
	}

	@Override
	public Optional<EInvoiceProviderModel> getActive(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkNotNull(baseStoreModel, "BaseStoreModel must not be null");

		final Set<EInvoiceProviderModel> collect = baseStoreModel.getEInvoiceProviders().stream()
				.filter(EInvoiceProviderModel::isActive).collect(Collectors.toSet());

		if (CollectionUtils.isEmpty(collect))
		{
			getLogger().warn("No Active EInvoiceProvider On BaseStore [{}]", baseStoreModel.getUid());
			return Optional.empty();
		}

		if (collect.size() > 1)
		{
			getLogger().warn("More than 1 Active EInvoiceProvider On BaseStore [{}], returning the first one",
					baseStoreModel.getUid());
		}

		return Optional.ofNullable(collect.iterator().next());
	}

	@Override
	public Optional<EInvoiceProviderModel> getActiveByBaseStoreCode(final String baseStorecode)
	{
		return getActive(getBaseStoreService().getBaseStoreForUid(baseStorecode));
	}

	@Override
	public Optional<EInvoiceProviderModel> getActiveProviderByCurrentBaseStore()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		Preconditions.checkNotNull(currentBaseStore, "Cannot find current BaseStoreModel");

		return getActive(currentBaseStore);
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

}
