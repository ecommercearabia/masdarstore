package com.masdar.masdarwebserviceapi.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;


/**
 * @author husam.dababneh@erabia.com
 */
public class WebServiceUnirestUtil
{
	protected static final Logger LOG = Logger.getLogger(WebServiceUnirestUtil.class);

	private WebServiceUnirestUtil()
	{
	}

	public static HttpResponse<?> httpPostJSON(final String url, final String body, Map<String, String> headers)
			throws UnirestException
	{
		Unirest.setTimeouts(0, 0);
		if (headers == null)
		{
			headers = new HashMap<>();
		}
		addHeaders(headers);

		final RequestBodyEntity entity = Unirest.post(url).headers(headers).body(body);
		return entity.asString();

	}

	public static HttpResponse<?> httpPostJSON(final String url, final String body, Map<String, String> headers,
			final long connectionTimeout) throws UnirestException
	{
		Unirest.setTimeouts(connectionTimeout, 0);
		if (headers == null)
		{
			headers = new HashMap<>();
		}
		addHeaders(headers);

		final RequestBodyEntity entity = Unirest.post(url).headers(headers).body(body);
		return entity.asString();

	}


	public static HttpResponse<?> httpGet(final String url, HashMap<String, String> headers, final Class<?> clazz,
			final int timeout) throws UnirestException
	{
		if (headers == null)
		{
			headers = new HashMap<>();
		}
		addHeaders(headers);

		Unirest.setTimeouts(timeout, timeout);

		return Unirest.get(url).headers(headers).asObject(clazz);
	}

	public static HttpResponse<byte[]> httpGetPdf(final String url, Map<String, String> headers) throws UnirestException
	{
		if (headers == null)
		{
			headers = new HashMap<>();
			headers.put("content-type", "application/pdf");
			headers.put("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.put("Pragma", "no-cache");
			headers.put("Expires", "0");
		}
		else
		{
			addHeaders(headers);
		}


		return Unirest.get(url).headers(headers).asObject(byte[].class);

	}

	public static <T> boolean is200Status(final HttpResponse<T> response)
	{
		return response.getStatus() >= 200 && response.getStatus() < 300;
	}

	/**
	 *
	 */
	private static void addHeaders(final Map<String, String> headers)
	{
		headers.put("Content-Type", "application/json");
		//		headers.put("user-agent", "*");
	}

}
