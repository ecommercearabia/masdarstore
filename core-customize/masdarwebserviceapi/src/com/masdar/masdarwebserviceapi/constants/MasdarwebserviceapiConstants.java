/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwebserviceapi.constants;

/**
 * Global class for all Masdarwebserviceapi constants. You can add global constants for your extension into this class.
 */
public final class MasdarwebserviceapiConstants extends GeneratedMasdarwebserviceapiConstants
{
	public static final String EXTENSIONNAME = "masdarwebserviceapi";

	private MasdarwebserviceapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarwebserviceapiPlatformLogo";
}
