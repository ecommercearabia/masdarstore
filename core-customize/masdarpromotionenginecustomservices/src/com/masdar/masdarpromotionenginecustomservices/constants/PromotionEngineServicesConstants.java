package com.masdar.masdarpromotionenginecustomservices.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class PromotionEngineServicesConstants extends GeneratedPromotionEngineServicesConstants
{
	public static final String EXTENSIONNAME = "masdarpromotionenginecustomservices";
	
	private PromotionEngineServicesConstants()
	{
		//empty
	}
	
	
}
