/**
 *
 */
package com.masdar.masdarpromotionenginecustomservices.calculation.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.ruleengineservices.calculation.impl.DefaultDeliveryCostEvaluationStrategy;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomDeliveryCostEvaluationStrategy extends DefaultDeliveryCostEvaluationStrategy
{

	private final Logger LOG = LoggerFactory.getLogger(CustomDeliveryCostEvaluationStrategy.class);

	@Override
	public BigDecimal evaluateCost(final AbstractOrderModel order, final DeliveryModeModel deliveryMode)
	{
		if (!(deliveryMode instanceof ZoneDeliveryModeModel))
		{
			//LOG.info("deliveryMode is not instance of ZoneDeliveryModeModel, returning ZERO");
			return BigDecimal.ZERO;
		}

		final ZoneDeliveryModeModel mode = (ZoneDeliveryModeModel) deliveryMode;
		if (order == null)
		{
			LOG.warn("AbstractOrderModel is null returning ZERO");
			return BigDecimal.ZERO;
		}

		if (Collections.isEmpty(mode.getValues()))
		{
			//LOG.warn("DeliveryModeModel[{}] values is empty, returning ZERO", mode.getCode());
			return BigDecimal.ZERO;
		}

		final double orderTotal = order.getTotalPrice();

		return getBestMatch(mode, orderTotal);

	}

	/**
	 * @param mode
	 * @param orderTotal
	 * @return
	 */
	private BigDecimal getBestMatch(final ZoneDeliveryModeModel mode, final double orderTotal)
	{
		final List<ZoneDeliveryModeValueModel> values = mode.getValues().stream()
				.sorted((e1, e2) -> e2.getMinimum().compareTo(e1.getMinimum())).collect(Collectors.toList());

		for (final ZoneDeliveryModeValueModel value : values)
		{
			if (orderTotal >= value.getMinimum())
			{
				return BigDecimal.valueOf(value.getValue());
			}
		}


		LOG.warn("Could not find ZoneDeliveryMode value for mode[{]} and order total[{}], returning ZERO", mode.getCode(),
				orderTotal);
		return BigDecimal.ZERO;
	}
}
