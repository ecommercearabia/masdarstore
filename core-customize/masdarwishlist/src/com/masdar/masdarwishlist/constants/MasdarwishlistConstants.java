/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarwishlist.constants;

/**
 * Global class for all Masdarwishlist constants. You can add global constants for your extension into this class.
 */
public final class MasdarwishlistConstants extends GeneratedMasdarwishlistConstants
{
	public static final String EXTENSIONNAME = "masdarwishlist";

	private MasdarwishlistConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "masdarwishlistPlatformLogo";
}
