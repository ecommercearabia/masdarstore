/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarassistedservicecustomerinterestscustomaddon.jalo;

import com.masdar.masdarassistedservicecustomerinterestscustomaddon.constants.MasdarassistedservicecustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class MasdarassistedservicecustomerinterestscustomaddonManager extends GeneratedMasdarassistedservicecustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarassistedservicecustomerinterestscustomaddonManager.class.getName() );
	
	public static final MasdarassistedservicecustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarassistedservicecustomerinterestscustomaddonManager) em.getExtension(MasdarassistedservicecustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
