/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarselectivecartsplitlistcustomaddon.controllers;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import com.masdar.masdarselectivecartsplitlistcustomaddon.model.components.SaveForLaterCMSComponentModel;


/**
 * Masdarselectivecartsplitlistcustomaddon controller constants
 */
public interface MasdarselectivecartsplitlistcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
	String ADDON_PREFIX = "addon:/masdarselectivecartsplitlistcustomaddon";

	/**
	 * Class with action name constants
	 */
	interface Actions
	{
		interface Cms // NOSONAR
		{
			String _Prefix = "/view/"; // NOSONAR
			String _Suffix = "Controller"; // NOSONAR

			/**
			 * Default CMS component controller
			 */
			String SaveForLaterComponent = _Prefix + SaveForLaterCMSComponentModel._TYPECODE + _Suffix; // NOSONAR
			String MiniCartComponent = _Prefix + MiniCartComponentModel._TYPECODE + _Suffix; // NOSONAR
		}
	}

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Cms // NOSONAR
		{
			String ComponentPrefix = "cms/"; // NOSONAR
		}

		interface Fragments
		{
			interface Cart // NOSONAR
			{
				String MiniCartPanel = "fragments/cart/miniCartPanel"; // NOSONAR
				String CartPopup = "fragments/cart/cartPopup"; // NOSONAR
			}
		}
	}
}
