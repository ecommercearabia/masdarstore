/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarselectivecartsplitlistcustomaddon.constants;

/**
 * Masdarselectivecartsplitlistcustomaddon web constants
 */
public final class MasdarselectivecartsplitlistcustomaddonWebConstants // NOSONAR
{
	private MasdarselectivecartsplitlistcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
