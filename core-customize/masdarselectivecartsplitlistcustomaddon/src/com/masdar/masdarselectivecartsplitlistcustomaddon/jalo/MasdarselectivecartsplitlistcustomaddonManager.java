/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarselectivecartsplitlistcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.masdar.masdarselectivecartsplitlistcustomaddon.constants.MasdarselectivecartsplitlistcustomaddonConstants;
import org.apache.log4j.Logger;

public class MasdarselectivecartsplitlistcustomaddonManager extends GeneratedMasdarselectivecartsplitlistcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( MasdarselectivecartsplitlistcustomaddonManager.class.getName() );
	
	public static final MasdarselectivecartsplitlistcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MasdarselectivecartsplitlistcustomaddonManager) em.getExtension(MasdarselectivecartsplitlistcustomaddonConstants.EXTENSIONNAME);
	}
	
}
