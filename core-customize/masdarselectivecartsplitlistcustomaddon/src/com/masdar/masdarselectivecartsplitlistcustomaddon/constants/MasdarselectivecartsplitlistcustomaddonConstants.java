/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.masdar.masdarselectivecartsplitlistcustomaddon.constants;

/**
 * Masdarselectivecartsplitlistcustomaddon constants
 */
public final class MasdarselectivecartsplitlistcustomaddonConstants extends GeneratedMasdarselectivecartsplitlistcustomaddonConstants
{
	public static final String EXTENSIONNAME = "masdarselectivecartsplitlistcustomaddon";

	private MasdarselectivecartsplitlistcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
